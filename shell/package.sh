#!/bin/bash
set -x
git pull

rm -rf ../carrier-ahu-designer/ahu-rest/src/main/resources/static/*

# cd ../carrier-ahu-designer/ahu-portal2/
# rm -rf ./dist/*
# yarn build:production
# cp -rf ./dist/* ../ahu-rest/src/main/resources/static/

cd ../carrier-ahu-designer/ahu-portal4/
yarn build:dist
# mv ./dist/index.html ./dist/ahu.html
# sed -i '.original' "s/\"js/\"..\/js/g" ./dist/ahu.html
# sed -i '.original' "s/\"css/\"..\/css/g" ./dist/ahu.html
# sed -i '.original' "s/\"vendors/\"..\/vendors/g" ./dist/ahu.html

# rm ./dist/ahu.html.original

cp -rf ./dist/* ../ahu-rest/src/main/resources/static/

ls -lh ../ahu-rest/src/main/resources/static/

cat ../ahu-rest/src/main/resources/static/index.htmL


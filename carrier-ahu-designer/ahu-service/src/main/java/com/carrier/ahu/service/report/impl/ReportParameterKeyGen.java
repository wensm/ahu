package com.carrier.ahu.service.report.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.report.common.ReportConstants;

import lombok.Data;

/**
 * The info comes from csv file/coil/:coolingcoildllcalculation.csv This message
 * was used by coolingcoil dll calculation.
 * 
 * @see ICalService#calCoolingCool(com.carrier.ahu.model.calunit.AhuParam)
 * 
 * @author Simon
 *
 */
@Deprecated
@Data
public class ReportParameterKeyGen {
	private static Logger logger = LoggerFactory.getLogger(ReportParameterKeyGen.class);

	private String[][] aruppfContents;
	private String[][] bochengHeaderContents;
	private String[][] bochengContents;
	private String[][] bochengFanContents;
	private String[][] bochengCoolingCoilContents;
	private String[][] bochengHeatingCoilContents;
	private String[][] bochengFilterContents;
	private String[][] bochengWetContents;
	private String[][] bochengContents2;
	private String[][] techAhuContents;
	private String[][] techAhu2Contents;
	private String[][] techCombinedContents;
	private String[][] techCoolingCoilContents;
	private String[][] techCoolingCoilContents1;
	private String[][] techDXCoilContents;
	private String[][] techDXCoilContents1;
	private String[][] techFan1Contents;
	private String[][] techFan2Contents;
	private String[][] techFan3Contents;
	private String[][] techFan4Contents;
	private String[][] techFan5Contents;
	private String[][] techFilterContents;
	private String[][] techMixContents;
	private String[][] techMix2Contents;
	private String[][] techMix3Contents;
	private String[][] techElecContents;
	private String[][] techElec1Contents;
	private String[][] techElec1SContents;
	private String[][] techElec1WContents;
	private String[][] techHepaContents;
	private String[][] techDisContents;
	private String[][] techHeaContents;
	private String[][] techWetContents;
	private String[][] techWetWContents;
	private String[][] techWetSContents;
	private String[][] techSprContents;
	private String[][] techSprSContents;
	private String[][] techSprWContents;
	private String[][] techSteContents;
	private String[][] techSteWContents;
	private String[][] techSteSContents;
	private String[][] techElec2Contents;
	private String[][] techElec2WContents;
	private String[][] techElec2SContents;
	private String[][] techSte1Contents;
	private String[][] techSte1SContents;
	private String[][] techSte1WContents;
	private String[][] techCombContents;
	private String[][] techComb1Contents;
	private String[][] techComb2Contents;
	private String[][] techAtteContents;
	private String[][] techAcceContents;
	private String[][] techWheeContents;
	private String[][] techWhee1Contents;
	private String[][] techPlateContents;
	private String[][] techPlate1Contents;
	private String[][] techCtrContents;

	private String[][] techProjectContents;
	private String[][] techCommonFContents;

	/** 机组铭牌数据 产品装箱单 段连接清单 **/
	private String[][] techUnitnameplatedataContents;
	private String[][] techUnitnameplatedata2Contents;
	
	private String[][] techProductPackingListContents;

	private String[][] docParalistAhuContents;
	private String[][] docParalistCoolingCoilContents;
	private String[][] docParalistHeatingCoilContents;
	private String[][] docParalistFanContents;
	private String[][] docParalistOtherContents;

	private String[][] salerACCEContents;
	private String[][] salerAHUContents;
	private String[][] salerATTEContents;
	private String[][] salerCOMB_1Contents;
	private String[][] salerCOMB_2Contents;
	private String[][] salerCOMBContents;
	private String[][] salerCOMBINEDContents;
	private String[][] salerCOOLINGCOILContents;
	private String[][] salerDISContents;
	private String[][] salerELEC_1Contents;
	private String[][] salerELEC_1SContents;
	private String[][] salerELEC_1WContents;
	private String[][] salerELEC_2Contents;
	private String[][] salerELEC_2SContents;
	private String[][] salerELEC_2WContents;
	private String[][] salerELECContents;
	private String[][] salerFANContents;
	private String[][] salerFILTERContents;
	private String[][] salerHEAContents;
	private String[][] salerHEPAContents;
	private String[][] salerMIXContents;
	private String[][] salerSPRContents;
	private String[][] salerSPRSContents;
	private String[][] salerSPRWContents;
	private String[][] salerSTE_1Contents;
	private String[][] salerSTE_1SContents;
	private String[][] salerSTE_1WContents;
	private String[][] salerSTESContents;
	private String[][] salerSTEWContents;
	private String[][] salerSTEContents;
	private String[][] salerWETContents;
	private String[][] salerWETSContents;
	private String[][] salerWETWContents;
	private String[][] salerWHEEContents;
	private String[][] salerPlateContents;

	private String[][] nsAHUContents;
	private String[][] nsAHU2Contents;
	private String[][] nsCommonContents;
	private String[][] nsCommonMemoContents;
	private String[][] nsWetContents;
	private String[][] nsSprContents;
	private String[][] nsSteContents;
	private String[][] nsFanContents;
	private String[][] nsAccessContents;
	private String[][] nsCoolingCoilContents;
	private String[][] nsHeatingCoilContents;
	public static Map<String, String[][]> getAllKeyArrys() {
		ReportParameterKeyGen gen = new ReportParameterKeyGen();
		Map<String, String[][]> map = new HashMap<>();

		gen.loadAllKey();
		map.put("Aruppf", gen.getAruppfContents());
		map.put("BochengHeader", gen.getBochengHeaderContents());
		map.put("Bocheng", gen.getBochengContents());
		map.put("BochengFan", gen.getBochengFanContents());
		map.put("BochengCoolingCoil", gen.getBochengCoolingCoilContents());
		map.put("BochengHeatingCoil", gen.getBochengHeatingCoilContents());
		map.put("BochengFilter", gen.getBochengFilterContents());
		map.put("BochengWet", gen.getBochengWetContents());
		map.put("Bocheng2", gen.getBochengContents2());
		map.put("TechAhu", gen.getTechAhuContents());
		map.put("TechAhu2", gen.getTechAhu2Contents());
		map.put("TechCombined", gen.getTechCombinedContents());
		map.put("TechCoolingCoil", gen.getTechCoolingCoilContents());
		map.put("TechCoolingCoil1", gen.getTechCoolingCoilContents1());
		map.put("TechDXCoil", gen.getTechDXCoilContents());
		map.put("TechDXCoil1", gen.getTechDXCoilContents1());
		map.put("TechFan1", gen.getTechFan1Contents());
		map.put("TechFan2", gen.getTechFan2Contents());
		map.put("TechFan3", gen.getTechFan3Contents());
		map.put("TechFan4", gen.getTechFan4Contents());
		map.put("TechFan5", gen.getTechFan5Contents());
		map.put("TechFilter", gen.getTechFilterContents());
		map.put("TechMix", gen.getTechMixContents());
		map.put("TechMix2", gen.getTechMix2Contents());
		map.put("TechMix3", gen.getTechMix3Contents());
		map.put("TechElec", gen.getTechElecContents());
		map.put("TechElec1", gen.getTechElec1Contents());
		map.put("TechElec1S", gen.getTechElec1SContents());
		map.put("TechElec1W", gen.getTechElec1WContents());
		map.put("TechHepa", gen.getTechHepaContents());
		map.put("TechDis", gen.getTechDisContents());
		map.put("TechHea", gen.getTechHeaContents());
		map.put("TechWet", gen.getTechWetContents());
		map.put("TechWetS", gen.getTechWetSContents());
		map.put("TechWetW", gen.getTechWetWContents());
		map.put("TechSpr", gen.getTechSprContents());
		map.put("TechSprS", gen.getTechSprSContents());
		map.put("TechSprW", gen.getTechSprWContents());
		map.put("TechSte", gen.getTechSteContents());
		map.put("TechSteW", gen.getTechSteWContents());
		map.put("TechSteS", gen.getTechSteSContents());
		map.put("TechElec2", gen.getTechElec2Contents());
		map.put("TechElec2W", gen.getTechElec2WContents());
		map.put("TechElec2S", gen.getTechElec2SContents());
		map.put("TechSte1", gen.getTechSte1Contents());
		map.put("TechSte1S", gen.getTechSte1SContents());
		map.put("TechSte1W", gen.getTechSte1WContents());
		map.put("TechComb", gen.getTechCombContents());
		map.put("TechComb1", gen.getTechComb1Contents());
		map.put("TechComb2", gen.getTechComb2Contents());
		map.put("TechAtte", gen.getTechAtteContents());
		map.put("TechAcce", gen.getTechAcceContents());
		map.put("TechWhee", gen.getTechWheeContents());
		map.put("TechWhee1", gen.getTechWhee1Contents());
		map.put("TechCtr", gen.getTechCtrContents());
		map.put("TechPlate", gen.getTechPlateContents());
		map.put("TechPlate1", gen.getTechPlate1Contents());

		map.put("TechProject", gen.getTechProjectContents());
		map.put("TechCommonF", gen.getTechCommonFContents());
		map.put("TechUnitnameplatedata", gen.getTechUnitnameplatedataContents());
		map.put("TechUnitnameplatedata2", gen.getTechUnitnameplatedata2Contents());
		
		map.put("TechProductPackingList", gen.getTechProductPackingListContents());

		map.put("DocParalistAhu", gen.getDocParalistAhuContents());
		map.put("DocParalistCoolingCoil", gen.getDocParalistCoolingCoilContents());
		map.put("DocParalistHeatingCoil", gen.getDocParalistHeatingCoilContents());
		map.put("DocParalistFan", gen.getDocParalistFanContents());
		map.put("DocParalistOther", gen.getDocParalistOtherContents());

		map.put("salerACCE", gen.getSalerACCEContents());
		map.put("salerAHU", gen.getSalerAHUContents());
		map.put("salerATTE", gen.getSalerATTEContents());
		map.put("salerCOMB_1", gen.getSalerCOMB_1Contents());
		map.put("salerCOMB_2", gen.getSalerCOMB_2Contents());
		map.put("salerCOMB", gen.getSalerCOMBContents());
		map.put("salerCOMBINED", gen.getSalerCOMBINEDContents());
		map.put("salerCOOLINGCOIL", gen.getSalerCOOLINGCOILContents());
		map.put("salerDIS", gen.getSalerDISContents());
		map.put("salerELEC_1", gen.getSalerELEC_1Contents());
		map.put("salerELEC_1S", gen.getSalerELEC_1SContents());
		map.put("salerELEC_1W", gen.getSalerELEC_1WContents());
		map.put("salerELEC_2", gen.getSalerELEC_2Contents());
		map.put("salerELEC_2S", gen.getSalerELEC_2SContents());
		map.put("salerELEC_2W", gen.getSalerELEC_2WContents());
		map.put("salerELEC", gen.getSalerELECContents());
		map.put("salerFAN", gen.getSalerFANContents());
		map.put("salerFILTER", gen.getSalerFILTERContents());
		map.put("salerHEA", gen.getSalerHEAContents());
		map.put("salerHEPA", gen.getSalerHEPAContents());
		map.put("salerMIX", gen.getSalerMIXContents());
		map.put("salerSPR", gen.getSalerSPRContents());
		map.put("salerSPRS", gen.getSalerSPRSContents());
		map.put("salerSPRW", gen.getSalerSPRWContents());
		map.put("salerSTE_1", gen.getSalerSTE_1Contents());
		map.put("salerSTE_1S", gen.getSalerSTE_1SContents());
		map.put("salerSTE_1W", gen.getSalerSTE_1WContents());
		map.put("salerSTE", gen.getSalerSTEContents());
		map.put("salerSTES", gen.getSalerSTESContents());
		map.put("salerSTEW", gen.getSalerSTEWContents());
		map.put("salerWET", gen.getSalerWETContents());
		map.put("salerWETS", gen.getSalerWETSContents());
		map.put("salerWETW", gen.getSalerWETWContents());
		map.put("salerWHEE", gen.getSalerWHEEContents());
		map.put("salerPlate", gen.getSalerPlateContents());

		map.put("nsAHU", gen.getNsAHUContents());
		map.put("nsAHU2", gen.getNsAHU2Contents());
		map.put("nsCommon", gen.getNsCommonContents());
		map.put("nsCommonMemo", gen.getNsCommonMemoContents());
        map.put("nsWet", gen.getNsWetContents());
        map.put("nsSpr", gen.getNsSprContents());
        map.put("nsSte", gen.getNsSteContents());
        map.put("nsFan", gen.getNsFanContents());
        map.put("nsAccess", gen.getNsAccessContents());
        map.put("nsCoolingCoil", gen.getNsCoolingCoilContents());
        map.put("nsHeatingCoil", gen.getNsHeatingCoilContents());
		return map;
	}

	public void loadAllKey() {
		loadAruppfContents(6);
		loadBochengContents(4);
		loadBochengCoolingCoilContents(4);
		loadBochengHeatingCoilContents(4);
		loadBochengFanContents(4);
		loadBochengFilterContents(4);
		loadBochengWetContents(4);
		loadBochengContents2(4);
		loadBochengHeaderContents(6);
		loadTechAhuContents(7);
		loadTechAhu2Contents(6);
		loadTechCombinedContents(6);
		loadTechCoolingCoilContents(7);
		loadTechCoolingCoilContents1(7);
		loadTechDXCoilContents(7);
		loadTechDXCoilContents1(7);
		loadTechFan1Contents(8);
		loadTechFan2Contents(10);
		loadTechFan3Contents(11);
		loadTechFan4Contents(2);
		loadTechFan5Contents(1);
		loadTechFilterContents(6);
		loadTechMixContents(7);
		loadTechMix2Contents(9);
		loadTechMix3Contents(9);
		loadTechElecContents(6);
		loadTechElec1Contents(6);
		loadTechElec1SContents(6);
		loadTechElec1WContents(6);
		loadTechHepaContents(6);
		loadTechDisContents(6);
		loadTechHeaContents(6);
		loadTechWetContents(6);
		loadTechWetSContents(6);
		loadTechWetWContents(6);
		loadTechSprContents(6);
		loadTechSprSContents(6);
		loadTechSprWContents(6);
		loadTechSteContents(6);
		loadTechSteWContents(6);
		loadTechSteSContents(6);
		loadTechElec2Contents(6);
		loadTechElec2WContents(6);
		loadTechElec2SContents(6);
		loadTechSte1Contents(6);
		loadTechSte1SContents(6);
		loadTechSte1WContents(6);
		loadTechCombContents(6);
		loadTechComb1Contents(9);
		loadTechComb2Contents(9);
		loadTechAtteContents(10);
		loadTechAcceContents(6);
		loadTechWheeContents(6);
		loadTechWhee1Contents(2);
		loadTechCtrContents(6);
		loadTechPlateContents(6);
		loadTechPlate1Contents(2);

		loadTechProjectContents(6);
		loadTechCommonFContents(6);

		loadTechUnitnameplatedataContents(6);
		loadTechUnitnameplatedata2Contents(6);
		
		loadTechProductPackingListContents(5);

		loadDocParalistAhuContents(3);
		loadDocParalistCoolingCoilContents(3);
		loadDocParalistHeatingCoilContents(3);
		loadDocParalistFanContents(3);
		loadDocParalistOtherContents(3);

		loadSalerACCEContents(6);
		loadSalerAHUContents(7);
		loadSalerATTEContents(10);
		loadSalerCOMBContents(6);
		loadSalerCOMB1Contents(9);
		loadSalerCOMB2Contents(9);
		loadSalerCOMBINEDContents(6);
		loadSalerCOOLINGCOILContents(7);
		loadSalerDISContents(6);
		loadSalerELEC_1Contents(6);
		loadSalerELEC_1SContents(6);
		loadSalerELEC_1WContents(6);
		loadSalerELEC_2Contents(6);
		loadSalerELEC_2SContents(6);
		loadSalerELEC_2WContents(6);
		loadSalerELECContents(6);
		loadSalerFANContents(8);
		loadSalerFILTERContents(6);
		loadSalerHEAContents(6);
		loadSalerHEPAContents(6);
		loadSalerMIXContents(7);
		loadSalerSPRContents(6);
		loadSalerSPRSContents(6);
		loadSalerSPRWContents(6);
		loadSalerSTE_1Contents(6);
		loadSalerSTE_1SContents(6);
		loadSalerSTE_1WContents(6);
		loadSalerSTEContents(6);
		loadSalerSTESContents(6);
		loadSalerSTEWContents(6);
		loadSalerWETContents(6);
		loadSalerWETSContents(6);
		loadSalerWETWContents(6);
		loadSalerWHEEContents(6);
		loadSalerPlateContents(6);
		loadNsAHUContents(8);
		loadNsAHU2Contents(8);
		loadNsCommonContents(8);
		loadNsCommonMemoContents(8);
		loadNsWetContents(8);
		loadNsSprContents(8);
		loadNsSteContents(8);
		loadNsFanContents(8);
		loadNsAccessContents(8);
		loadNsCoolingCoilContents(8);
		loadNsHeatingCoilContents(8);
	}

	/**
	 * 奥雅纳工程格式表格
	 * 
	 * @param cols
	 */
	public void loadAruppfContents(int cols) {
		try {
			setAruppfContents(getContents(getReportPath(ReportConstants.REPORT_T_ARUPPF), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report Aruppf Contents Info", e);
		}
	}

    private String getReportPath(String reportName) {
        return String.format(ReportConstants.CONTENT_PATH, reportName);
    }

	/**
	 * 柏诚工程格式表格
	 * 
	 * @param cols
	 */
	public void loadBochengContents(int cols) {
		try {
			setBochengContents(getContents(getReportPath(ReportConstants.REPORT_T_BOCHENG_SPEC), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report Bocheng Contents Info", e);
		}
	}
	/**
	 * 柏诚工程格式冷水盘管表格
	 *
	 * @param cols
	 */
	public void loadBochengCoolingCoilContents(int cols) {
		try {
			setBochengCoolingCoilContents(getContents(getReportPath(ReportConstants.REPORT_T_BOCHENG_COOLINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report Bocheng Contents Info", e);
		}
	}
	/**
	 * 柏诚工程格式热水盘管表格
	 *
	 * @param cols
	 */
	public void loadBochengHeatingCoilContents(int cols) {
		try {
			setBochengHeatingCoilContents(getContents(getReportPath(ReportConstants.REPORT_T_BOCHENG_HEATINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report Bocheng Contents Info", e);
		}
	}
	/**
	 * 柏诚工程格式风机表格
	 *
	 * @param cols
	 */
	public void loadBochengFanContents(int cols) {
		try {
			setBochengFanContents(getContents(getReportPath(ReportConstants.REPORT_T_BOCHENG_FAN), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report Bocheng Contents Info", e);
		}
	}
	/**
	 * 柏诚工程格式过滤器表格
	 *
	 * @param cols
	 */
	public void loadBochengFilterContents(int cols) {
		try {
			setBochengFilterContents(getContents(getReportPath(ReportConstants.REPORT_T_BOCHENG_FILTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report Bocheng Contents Info", e);
		}
	}
	/**
	 * 柏诚工程格式加湿表格
	 *
	 * @param cols
	 */
	public void loadBochengWetContents(int cols) {
		try {
			setBochengWetContents(getContents(getReportPath(ReportConstants.REPORT_T_BOCHENG_WET), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report Bocheng Contents Info", e);
		}
	}
	/**
	 * 柏诚工程格式尾部信息
	 *
	 * @param cols
	 */
	public void loadBochengContents2(int cols) {
		try {
			setBochengContents2(getContents(getReportPath(ReportConstants.REPORT_T_BOCHENG_AHU), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report Bocheng Contents Info", e);
		}
	}
	/**
	 * 柏诚工程格式表格头
	 *
	 * @param cols
	 */
	public void loadBochengHeaderContents(int cols) {
		try {
			setBochengHeaderContents(getContents(getReportPath(ReportConstants.REPORT_T_BOCHENG_HEADER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report Bocheng Contents Info", e);
		}
	}

	/**
	 * 技术说明 - AHU 表格
	 * 
	 * @param cols
	 */
	public void loadTechAhuContents(int cols) {
		try {
			setTechAhuContents(getContents(getReportPath(ReportConstants.REPORT_TECH_AHU_OVERALL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechAhu Contents Info", e);
		}
	}

	/**
	 * 技术说明 - AHU 2 表格
	 * 
	 * @param cols
	 */
	public void loadTechAhu2Contents(int cols) {
		try {
			setTechAhu2Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_AHU_DELIVERY), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechAhu2 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 综合过滤段 表格
	 * 
	 * @param cols
	 */
	public void loadTechCombinedContents(int cols) {
		try {
			setTechCombinedContents(getContents(getReportPath(ReportConstants.REPORT_TECH_COMBINEDFILTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechCombined Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 冷水盘管段 表格
	 * 
	 * @param cols
	 */
	public void loadTechCoolingCoilContents(int cols) {
		try {
			setTechCoolingCoilContents(getContents(getReportPath(ReportConstants.REPORT_TECH_COOLINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechCoolingCoil Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 冷水盘管段 表格
	 *
	 * @param cols
	 */
	public void loadTechCoolingCoilContents1(int cols) {
		try {
			setTechCoolingCoilContents1(getContents(getReportPath(ReportConstants.REPORT_TECH_COOLINGCOIL_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechCoolingCoil Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 冷水盘管段 表格
	 *
	 * @param cols
	 */
	public void loadTechDXCoilContents(int cols) {
		try {
			setTechDXCoilContents(getContents(getReportPath(ReportConstants.REPORT_TECH_DIRECTEXPENSIONCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechDXCoil Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 冷水盘管段 表格
	 *
	 * @param cols
	 */
	public void loadTechDXCoilContents1(int cols) {
		try {
			setTechDXCoilContents1(getContents(getReportPath(ReportConstants.REPORT_TECH_DIRECTEXPENSIONCOIL_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechDXCoil Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 风机段 1 表格
	 * 
	 * @param cols
	 */
	public void loadTechFan1Contents(int cols) {
		try {
			setTechFan1Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_FAN), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechFan1 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 风机段 2 表格
	 * 
	 * @param cols
	 */
	public void loadTechFan2Contents(int cols) {
		try {
			setTechFan2Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_FAN_SOUND), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechFan2 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 风机段 3 表格
	 * 
	 * @param cols
	 */
	public void loadTechFan3Contents(int cols) {
		try {
			setTechFan3Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_FAN_ABSORBER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechFan3 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 风机段 4 表格
	 * 
	 * @param cols
	 */
	public void loadTechFan4Contents(int cols) {
		try {
			setTechFan4Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_FAN_CURVE), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechFan3 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 风机段 5 表格
	 *
	 * @param cols
	 */
	public void loadTechFan5Contents(int cols) {
		try {
			setTechFan5Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_FAN_REMARK), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechFan5 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 静电过滤段 表格
	 * 
	 * @param cols
	 */
	public void loadTechElecContents(int cols) {
		try {
			setTechElecContents(getContents(getReportPath(ReportConstants.REPORT_TECH_ELECTROSTATICFILTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechElec Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 电级加湿段 表格
	 * 
	 * @param cols
	 */
	public void loadTechElec1Contents(int cols) {
		try {
			setTechElec1Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechElec1 Contents Info", e);
		}
	}

	public void loadTechElec1SContents(int cols) {
		try {
			setTechElec1SContents(getContents(getReportPath(ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechElec1 Contents Info", e);
		}
	}

	public void loadTechElec1WContents(int cols) {
		try {
			setTechElec1WContents(getContents(getReportPath(ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechElec1 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 高效过滤段 表格
	 * 
	 * @param cols
	 */
	public void loadTechHepaContents(int cols) {
		try {
			setTechHepaContents(getContents(getReportPath(ReportConstants.REPORT_TECH_HEPAFILTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechHepa Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 出风段 表格
	 * 
	 * @param cols
	 */
	public void loadTechDisContents(int cols) {
		try {
			setTechDisContents(getContents(getReportPath(ReportConstants.REPORT_TECH_DISCHARGE), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechDis Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 热水盘管段 表格
	 * 
	 * @param cols
	 */
	public void loadTechHeaContents(int cols) {
		try {
			setTechHeaContents(getContents(getReportPath(ReportConstants.REPORT_TECH_HEATINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechHea Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 热水盘管段 表格
	 * 
	 * @param cols
	 */
	public void loadTechWetContents(int cols) {
		try {
			setTechWetContents(getContents(getReportPath(ReportConstants.REPORT_TECH_WETFILMHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechWet Contents Info", e);
		}
	}

	public void loadTechWetSContents(int cols) {
		try {
			setTechWetSContents(getContents(getReportPath(ReportConstants.REPORT_TECH_WETFILMHUMIDIFIER_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechWet Contents Info", e);
		}
	}

	public void loadTechWetWContents(int cols) {
		try {
			setTechWetWContents(getContents(getReportPath(ReportConstants.REPORT_TECH_WETFILMHUMIDIFIER_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechWet Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 高压喷雾加湿段 表格
	 * 
	 * @param cols
	 */
	public void loadTechSprContents(int cols) {
		try {
			setTechSprContents(getContents(getReportPath(ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechSpr Contents Info", e);
		}
	}

	public void loadTechSprSContents(int cols) {
		try {
			setTechSprSContents(getContents(getReportPath(ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechSpr Contents Info", e);
		}
	}

	public void loadTechSprWContents(int cols) {
		try {
			setTechSprWContents(getContents(getReportPath(ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechSpr Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 蒸汽盘管段 表格
	 * 
	 * @param cols
	 */
	public void loadTechSteContents(int cols) {
		try {
			setTechSteContents(getContents(getReportPath(ReportConstants.REPORT_TECH_STEAMCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechSte Contents Info", e);
		}
	}

	public void loadTechSteSContents(int cols) {
		try {
			setTechSteSContents(getContents(getReportPath(ReportConstants.REPORT_TECH_STEAMCOIL_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechSte Contents Info", e);
		}
	}

	public void loadTechSteWContents(int cols) {
		try {
			setTechSteWContents(getContents(getReportPath(ReportConstants.REPORT_TECH_STEAMCOIL_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechSte Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 电加热盘管段表格
	 * 
	 * @param cols
	 */
	public void loadTechElec2Contents(int cols) {
		try {
			setTechElec2Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_ELECTRICHEATINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechElec2 Contents Info", e);
		}
	}

	public void loadTechElec2WContents(int cols) {
		try {
			setTechElec2WContents(getContents(getReportPath(ReportConstants.REPORT_TECH_ELECTRICHEATINGCOIL_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechElec2 Contents Info", e);
		}
	}

	public void loadTechElec2SContents(int cols) {
		try {
			setTechElec2SContents(getContents(getReportPath(ReportConstants.REPORT_TECH_ELECTRICHEATINGCOIL_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechElec2 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 干蒸汽加湿段表格
	 * 
	 * @param cols
	 */
	public void loadTechSte1Contents(int cols) {
		try {
			setTechSte1Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_STEAMHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechSte1 Contents Info", e);
		}
	}

	public void loadTechSte1SContents(int cols) {
		try {
			setTechSte1SContents(getContents(getReportPath(ReportConstants.REPORT_TECH_STEAMHUMIDIFIER_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechSte1 Contents Info", e);
		}
	}

	public void loadTechSte1WContents(int cols) {
		try {
			setTechSte1WContents(getContents(getReportPath(ReportConstants.REPORT_TECH_STEAMHUMIDIFIER_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechSte1 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 新回排风段 表格
	 * 
	 * @param cols
	 */
	public void loadTechCombContents(int cols) {
		try {
			setTechCombContents(getContents(getReportPath(ReportConstants.REPORT_TECH_COMBINEDMIXINGCHAMBER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechComb Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 新回排风段-1 表格
	 * 
	 * @param cols
	 */
	public void loadTechComb1Contents(int cols) {
		try {
			setTechComb1Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_COMBINEDMIXINGCHAMBER_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechComb1 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 新回排风段-2 表格
	 * 
	 * @param cols
	 */
	public void loadTechComb2Contents(int cols) {
		try {
			setTechComb2Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_COMBINEDMIXINGCHAMBER_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechComb2 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 消音段 表格
	 * 
	 * @param cols
	 */
	public void loadTechAtteContents(int cols) {
		try {
			setTechAtteContents(getContents(getReportPath(ReportConstants.REPORT_TECH_ATTENUATOR), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechAtte Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 空段 表格
	 * 
	 * @param cols
	 */
	public void loadTechAcceContents(int cols) {
		try {
			setTechAcceContents(getContents(getReportPath(ReportConstants.REPORT_TECH_ACCESS), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechAcce Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 热回收段（转轮） 表格
	 * 
	 * @param cols
	 */
	public void loadTechWheeContents(int cols) {
		try {
			setTechWheeContents(getContents(getReportPath(ReportConstants.REPORT_TECH_HEATRECYCLE), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechWhee Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 热回收段（转轮）-1 表格
	 * 
	 * @param cols
	 */
	public void loadTechWhee1Contents(int cols) {
		try {
			setTechWhee1Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_HEATRECYCLE_DELIVERY), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechWhee1 Contents Info", e);
		}
	}
	
	/**
	 * 技术说明 - 热回收段（板式） 表格
	 * 
	 * @param cols
	 */
	public void loadTechPlateContents(int cols) {
		try {
			setTechPlateContents(getContents(getReportPath(ReportConstants.REPORT_TECH_PLATEHEATRECYCLE), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechWhee1 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 热回收段（板式）-1 表格
	 * 
	 * @param cols
	 */
	public void loadTechPlate1Contents(int cols) {
		try {
			setTechPlate1Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_PLATEHEATRECYCLE_DELIVERY), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechWhee1 Contents Info", e);
		}
	}
	
	/**
	 * 技术说明 - 控制段） 表格
	 * 
	 * @param cols
	 */
	public void loadTechCtrContents(int cols) {
		try {
			setTechCtrContents(getContents(getReportPath(ReportConstants.REPORT_TECH_CTR), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechCtr Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 单层过滤段 表格
	 * 
	 * @param cols
	 */
	public void loadTechFilterContents(int cols) {
		try {
			setTechFilterContents(getContents(getReportPath(ReportConstants.REPORT_TECH_FILTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechFilter Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 混合段 表格
	 * 
	 * @param cols
	 */
	public void loadTechMixContents(int cols) {
		try {
			setTechMixContents(getContents(getReportPath(ReportConstants.REPORT_TECH_MIX), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechMix Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 混合段 2 表格
	 * 
	 * @param cols
	 */
	public void loadTechMix2Contents(int cols) {
		try {
			setTechMix2Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_MIX_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechMix2 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - 混合段 2 表格
	 *
	 * @param cols
	 */
	public void loadTechMix3Contents(int cols) {
		try {
			setTechMix3Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_MIX_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechMix2 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - Project 页眉 表格
	 * 
	 * @param cols
	 */
	public void loadTechProjectContents(int cols) {
		try {
			setTechProjectContents(getContents(getReportPath(ReportConstants.REPORT_TECH_HEADER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechAhu2 Contents Info", e);
		}
	}

	/**
	 * 技术说明 - Project 页眉 表格
	 * 
	 * @param cols
	 */
	public void loadTechCommonFContents(int cols) {
		try {
			setTechCommonFContents(getContents(getReportPath(ReportConstants.REPORT_TECH_NONSTANDARD), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TechAhu2 Contents Info", e);
		}
	}
	public void loadTechUnitnameplatedataContents(int cols) {
		try {
			setTechUnitnameplatedataContents(getContents(getReportPath(ReportConstants.REPORT_TECH_UNITNAMEPLATEDATA), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TECH_UNITNAMEPLATEDATA_JSON Contents Info", e);
		}
	}
	public void loadTechUnitnameplatedata2Contents(int cols) {
		try {
			setTechUnitnameplatedata2Contents(getContents(getReportPath(ReportConstants.REPORT_TECH_UNITNAMEPLATEDATA_SHAPE), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TECH_UNITNAMEPLATEDATA_2_JSON Contents Info", e);
		}
	}
	
	/**
	 * 产品装箱单
	 * @param cols
	 */
	public void loadTechProductPackingListContents(int cols) {
		try {
			setTechProductPackingListContents(getContents(getReportPath(ReportConstants.REPORT_TECH_PRODUCTPACKINGLIST), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report TECH_PRODUCTPACKINGLIST_JSON Contents Info", e);
		}
	}
	
	/**
	 * 主要参数列表-ahu
	 * 
	 * @param cols
	 */
	public void loadDocParalistAhuContents(int cols) {
		try {
			setDocParalistAhuContents(getContents(getReportPath(ReportConstants.REPORT_DOC_PARALIST_AHU), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report DocParalistAhu Contents Info", e);
		}
	}

	/**
	 * 主要参数列表-冷水盘管
	 * 
	 * @param cols
	 */
	public void loadDocParalistCoolingCoilContents(int cols) {
		try {
			setDocParalistCoolingCoilContents(getContents(getReportPath(ReportConstants.REPORT_DOC_PARALIST_COOLINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report DocParalistCoolingCoil Contents Info", e);
		}
	}

	/**
	 * 主要参数列表-热水盘管
	 * 
	 * @param cols
	 */
	public void loadDocParalistHeatingCoilContents(int cols) {
		try {
			setDocParalistHeatingCoilContents(getContents(getReportPath(ReportConstants.REPORT_DOC_PARALIST_HEATINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report DocParalistHeatingCoil Contents Info", e);
		}
	}

	/**
	 * 主要参数列表-风机
	 * 
	 * @param cols
	 */
	public void loadDocParalistFanContents(int cols) {
		try {
			setDocParalistFanContents(getContents(getReportPath(ReportConstants.REPORT_DOC_PARALIST_FAN), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report DocParalistFan Contents Info", e);
		}
	}

	/**
	 * 主要参数列表-其他参数
	 * 
	 * @param cols
	 */
	public void loadDocParalistOtherContents(int cols) {
		try {
			setDocParalistOtherContents(getContents(getReportPath(ReportConstants.REPORT_DOC_PARALIST_OTHER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report DocParalistOther Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 空段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerACCEContents(int cols) {
		try {
			setSalerACCEContents(getContents(getReportPath(ReportConstants.REPORT_SALES_ACCESS), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_ACCE_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - AHU 表格
	 * 
	 * @param cols
	 */
	public void loadSalerAHUContents(int cols) {
		try {
			setSalerAHUContents(getContents(getReportPath(ReportConstants.REPORT_SALES_AHU_OVERALL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_AHU_JSON Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 消音段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerATTEContents(int cols) {
		try {
			setSalerATTEContents(getContents(getReportPath(ReportConstants.REPORT_SALES_ATTENUATOR), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_AHU_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 二次回风段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerCOMBContents(int cols) {
		try {
			setSalerCOMBContents(getContents(getReportPath(ReportConstants.REPORT_SALES_COMBINEDMIXINGCHAMBER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_COMB_JSON Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 二次回风段1 表格
	 * 
	 * @param cols
	 */
	public void loadSalerCOMB1Contents(int cols) {
		try {
			setSalerCOMB_1Contents(getContents(getReportPath(ReportConstants.REPORT_SALES_COMBINEDMIXINGCHAMBER_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_COMB_1_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 二次回风段2 表格
	 * 
	 * @param cols
	 */
	public void loadSalerCOMB2Contents(int cols) {
		try {
			setSalerCOMB_2Contents(getContents(getReportPath(ReportConstants.REPORT_SALES_COMBINEDMIXINGCHAMBER_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_COMB_2_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 综合过滤段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerCOMBINEDContents(int cols) {
		try {
			setSalerCOMBINEDContents(getContents(getReportPath(ReportConstants.REPORT_SALES_COMBINEDFILTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_COMBINED_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 冷水盘管段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerCOOLINGCOILContents(int cols) {
		try {
			setSalerCOOLINGCOILContents(getContents(getReportPath(ReportConstants.REPORT_SALES_COOLINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_COOLINGCOIL_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 出风段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerDISContents(int cols) {
		try {
			setSalerDISContents(getContents(getReportPath(ReportConstants.REPORT_SALES_DISCHARGE), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_DIS_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 电极加湿器 表格
	 * 
	 * @param cols
	 */
	public void loadSalerELEC_1Contents(int cols) {
		try {
			setSalerELEC_1Contents(getContents(getReportPath(ReportConstants.REPORT_SALES_ELECTRODEHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_ELEC_1_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 电极加湿器[夏季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerELEC_1SContents(int cols) {
		try {
			setSalerELEC_1SContents(getContents(getReportPath(ReportConstants.REPORT_SALES_ELECTRODEHUMIDIFIER_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_ELEC_1S_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 电极加湿器[冬季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerELEC_1WContents(int cols) {
		try {
			setSalerELEC_1WContents(getContents(getReportPath(ReportConstants.REPORT_SALES_ELECTRODEHUMIDIFIER_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_ELEC_1W_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 电加热盘管 表格
	 * 
	 * @param cols
	 */
	public void loadSalerELEC_2Contents(int cols) {
		try {
			setSalerELEC_2Contents(getContents(getReportPath(ReportConstants.REPORT_SALES_ELECTRICHEATINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_ELEC_2_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 电加热盘管[夏季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerELEC_2SContents(int cols) {
		try {
			setSalerELEC_2SContents(getContents(getReportPath(ReportConstants.REPORT_SALES_ELECTRICHEATINGCOIL_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_ELEC_2S_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 电加热盘管[冬季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerELEC_2WContents(int cols) {
		try {
			setSalerELEC_2WContents(getContents(getReportPath(ReportConstants.REPORT_SALES_ELECTRICHEATINGCOIL_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_ELEC_2W_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 静电过滤段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerELECContents(int cols) {
		try {
			setSalerELECContents(getContents(getReportPath(ReportConstants.REPORT_SALES_ELECTROSTATICFILTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_ELEC_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 风机 表格
	 * 
	 * @param cols
	 */
	public void loadSalerFANContents(int cols) {
		try {
			setSalerFANContents(getContents(getReportPath(ReportConstants.REPORT_SALES_FAN), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_FAN_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 单层过滤段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerFILTERContents(int cols) {
		try {
			setSalerFILTERContents(getContents(getReportPath(ReportConstants.REPORT_SALES_FILTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_FILTER_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 热水盘管 表格
	 * 
	 * @param cols
	 */
	public void loadSalerHEAContents(int cols) {
		try {
			setSalerHEAContents(getContents(getReportPath(ReportConstants.REPORT_SALES_HEATINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_HEA_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 高效过滤段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerHEPAContents(int cols) {
		try {
			setSalerHEPAContents(getContents(getReportPath(ReportConstants.REPORT_SALES_HEPAFILTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_HEPA_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 混合段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerMIXContents(int cols) {
		try {
			setSalerMIXContents(getContents(getReportPath(ReportConstants.REPORT_SALES_MIX), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_MIX_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 高压喷雾加湿段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerSPRContents(int cols) {
		try {
			setSalerSPRContents(getContents(getReportPath(ReportConstants.REPORT_SALES_SPRAYHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_SPR_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 高压喷雾加湿段[夏季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerSPRSContents(int cols) {
		try {
			setSalerSPRSContents(getContents(getReportPath(ReportConstants.REPORT_SALES_SPRAYHUMIDIFIER_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_SPRS_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 高压喷雾加湿段[冬季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerSPRWContents(int cols) {
		try {
			setSalerSPRWContents(getContents(getReportPath(ReportConstants.REPORT_SALES_SPRAYHUMIDIFIER_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_SPRW_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 干蒸加湿段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerSTE_1Contents(int cols) {
		try {
			setSalerSTE_1Contents(getContents(getReportPath(ReportConstants.REPORT_SALES_STEAMHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_STE_1_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 干蒸加湿段[夏季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerSTE_1SContents(int cols) {
		try {
			setSalerSTE_1SContents(getContents(getReportPath(ReportConstants.REPORT_SALES_STEAMHUMIDIFIER_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_STE_1S_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 干蒸加湿段[冬季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerSTE_1WContents(int cols) {
		try {
			setSalerSTE_1WContents(getContents(getReportPath(ReportConstants.REPORT_SALES_STEAMHUMIDIFIER_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_STE_1W_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 蒸汽盘管段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerSTEContents(int cols) {
		try {
			setSalerSTEContents(getContents(getReportPath(ReportConstants.REPORT_SALES_STEAMCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_STE_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 蒸汽盘管段[夏季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerSTESContents(int cols) {
		try {
			setSalerSTESContents(getContents(getReportPath(ReportConstants.REPORT_SALES_STEAMCOIL_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_STES_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 蒸汽盘管段[冬季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerSTEWContents(int cols) {
		try {
			setSalerSTEWContents(getContents(getReportPath(ReportConstants.REPORT_SALES_STEAMCOIL_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_STEW_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 湿膜加湿段 表格
	 * 
	 * @param cols
	 */
	public void loadSalerWETContents(int cols) {
		try {
			setSalerWETContents(getContents(getReportPath(ReportConstants.REPORT_SALES_WETFILMHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_WET_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 湿膜加湿段[夏季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerWETSContents(int cols) {
		try {
			setSalerWETSContents(getContents(getReportPath(ReportConstants.REPORT_SALES_WETFILMHUMIDIFIER_SUMMER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_WETS_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 湿膜加湿段[冬季] 表格
	 * 
	 * @param cols
	 */
	public void loadSalerWETWContents(int cols) {
		try {
			setSalerWETWContents(getContents(getReportPath(ReportConstants.REPORT_SALES_WETFILMHUMIDIFIER_WINTER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_WETW_JSON Contents Info", e);
		}
	}

	/**
	 * 技术说明[销售] - 热回收 表格
	 * 
	 * @param cols
	 */
	public void loadSalerWHEEContents(int cols) {
		try {
			setSalerWHEEContents(getContents(getReportPath(ReportConstants.REPORT_SALES_WHEELHEATRECYCLE), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_WHEE_JSON Contents Info", e);
		}
	}
	
	/**
	 * 技术说明[销售] - 板式热回收 表格
	 * 
	 * @param cols
	 */
	public void loadSalerPlateContents(int cols) {
		try {
			setSalerPlateContents(getContents(getReportPath(ReportConstants.REPORT_SALES_PLATEHEATRECYCLE), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report SALER_PLATE_JSON Contents Info", e);
		}
	}

	/**
	 * 非标ahu部分
	 * @param cols
	 */
	public void loadNsAHUContents(int cols) {
		try {
			setNsAHUContents(getContents(getReportPath(ReportConstants.REPORT_NS_AHU), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_AHU_JSON Contents Info", e);
		}
	}
	/**
	 * 非标ahu2部分
	 * @param cols
	 */
	public void loadNsAHU2Contents(int cols) {
		try {
			setNsAHU2Contents(getContents(getReportPath(ReportConstants.REPORT_NS_AHU_NONSTANDARD), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_AHU2_JSON Contents Info", e);
		}
	}
	/**
	 * 非标相同段部分
	 * @param cols
	 */
	public void loadNsCommonContents(int cols) {
		try {
			setNsCommonContents(getContents(getReportPath(ReportConstants.REPORT_NS_COMMON), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_COMMON_JSON Contents Info", e);
		}
	}
	/**
	 * 非标相同段部分
	 * @param cols
	 */
	public void loadNsCommonMemoContents(int cols) {
		try {
			setNsCommonMemoContents(getContents(getReportPath(ReportConstants.REPORT_NS_COMMON_MEMO), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_COMMON_JSON Contents Info", e);
		}
	}
	/**
	 * 非标湿膜加湿段
	 * @param cols
	 */
	public void loadNsWetContents(int cols) {
		try {
			setNsWetContents(getContents(getReportPath(ReportConstants.REPORT_NS_WETFILMHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_WET_JSON Contents Info", e);
		}
	}
	/**
	 * 非标高压喷雾加湿段
	 * @param cols
	 */
	public void loadNsSprContents(int cols) {
		try {
			setNsSprContents(getContents(getReportPath(ReportConstants.REPORT_NS_SPRAYHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_SPR_JSON Contents Info", e);
		}
	}
	/**
	 * 非标干蒸加湿段
	 * @param cols
	 */
	public void loadNsSteContents(int cols) {
		try {
			setNsSteContents(getContents(getReportPath(ReportConstants.REPORT_NS_STEAMHUMIDIFIER), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_STE_JSON Contents Info", e);
		}
	}
	/**
	 * 非标风机段
	 * @param cols
	 */
	public void loadNsFanContents(int cols) {
		try {
			setNsFanContents(getContents(getReportPath(ReportConstants.REPORT_NS_FAN), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_FAN_JSON Contents Info", e);
		}
	}
	/**
	 * 非标空段
	 * @param cols
	 */
	public void loadNsAccessContents(int cols) {
		try {
			setNsAccessContents(getContents(getReportPath(ReportConstants.REPORT_NS_ACCESS), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_FAN_JSON Contents Info", e);
		}
	}
	/**
	 * 非标风机段
	 * @param cols
	 */
	public void loadNsCoolingCoilContents(int cols) {
		try {
			setNsCoolingCoilContents(getContents(getReportPath(ReportConstants.REPORT_NS_COOLINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_COOLINGCOIL_JSON Contents Info", e);
		}
	}
	/**
	 * 非标风机段
	 * @param cols
	 */
	public void loadNsHeatingCoilContents(int cols) {
		try {
			setNsHeatingCoilContents(getContents(getReportPath(ReportConstants.REPORT_NS_HEATINGCOIL), cols));
		} catch (Exception e) {
			logger.error("Failed to Load Report NS_HEATINGCOIL_JSON Contents Info", e);
		}
	}
	private String[][] getContents(String filePath, int cols) throws Exception {
		InputStreamReader ireader = getJsonReader(filePath);
		BufferedReader br = new BufferedReader(ireader);
		String s = null;
		StringBuffer sb = new StringBuffer();
		while ((s = br.readLine()) != null) {
			sb.append(s);
		}
		JSONArray array = JSONArray.parseArray(sb.toString());

		String[][] contents = new String[array.size()][cols];

		for (int i = 0; i < array.size(); i++) {
			JSONArray array2 = array.getJSONArray(i);
			for (int j = 0; j < array2.size(); j++) {
				contents[i][j] = String.valueOf(array2.get(j));
			}
		}
		// print(contents);
		return contents;
	}

    /**
     * Load report json for export or default.
     * 
     * @param filePath
     * @return
     */
    private InputStreamReader getJsonReader(String filePath) {
        InputStream jsonStream = AHUContext.getJsonInputStream(filePath);
        InputStreamReader isReader = new InputStreamReader(jsonStream, Charset.forName("UTF-8"));
        return isReader;
    }

}

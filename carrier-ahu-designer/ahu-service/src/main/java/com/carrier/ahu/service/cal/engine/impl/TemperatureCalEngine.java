package com.carrier.ahu.service.cal.engine.impl;

import static com.carrier.ahu.common.intl.I18NConstants.RESISTANCE_CALCULATION_FAILED;
import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;
import static com.carrier.ahu.vo.SystemCalculateConstants.seasonS;
import static com.carrier.ahu.vo.SystemCalculateConstants.seasonW;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.common.exception.calculation.TemperatureCalcException;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.param.EngineConvertParam;
import com.carrier.ahu.param.TemperatureCalParam;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.validate.ValidateService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.AirVolumeUtil;
import com.carrier.ahu.util.InvokeTool;

/**
 * Created by LIANGD4 on 2017/12/13.
 * 混合段、回风、新风计算出风
 */
@Component
public class TemperatureCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(TemperatureCalEngine.class.getName());
    @Autowired
    private ValidateService validateService;

    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        logger.info("TemperatureCalEngine calculator line begin");
        int airVolume = 0;
        try {
            Map<String, Object> mapAll = new HashMap<String, Object>();
            TemperatureCalParam temperatureCalParam = new InvokeTool<TemperatureCalParam>().genInParamFromAhuParam(ahu, section, seasonS, new TemperatureCalParam());
            boolean isWinter=temperatureCalParam.isEnableWinter();
              if(true) {      
                    double newNAVolume = temperatureCalParam.getNAVolume();
                    if (AirDirectionEnum.RETURNAIR.getCode().equals(temperatureCalParam.getAirDirection())) {
                          airVolume = temperatureCalParam.getEairvolume();//回风
                    } else {
                          airVolume = AirVolumeUtil.packageSAirVolumeToInt(temperatureCalParam.getSairvolume(),temperatureCalParam.getAppendAirVolume());//送风
                    }
                    if(newNAVolume != 0) {
                        temperatureCalParam.setNARatio(BaseDataUtil.decimalConvert((newNAVolume/ airVolume), 2) * 100);
                    }
                    //计算前温度条件校验
                    //1:湿球不能大于干球
                    validateService.compareT(temperatureCalParam.getInDryBulbT(), temperatureCalParam.getInWetBulbT(), null);
                    validateService.compareT(temperatureCalParam.getNewDryBulbT(), temperatureCalParam.getNewWetBulbT(), null);

                    if(newNAVolume == 0) {
                        newNAVolume = airVolume * temperatureCalParam.getNARatio() / 100.0;//如果页面新风量为空，根据新风比计算
                    }
                    logger.info("TemperatureCalEngine calculator paramS :" + JSONArray.toJSONString(temperatureCalParam));
                    AirConditionBean airConditionBean = AirConditionUtils.mixAirCalculate(BaseDataUtil.integerConversionDouble(airVolume),
                            temperatureCalParam.getNARatio(), temperatureCalParam.getNewDryBulbT(), temperatureCalParam.getNewWetBulbT(),
                            temperatureCalParam.getInDryBulbT(), temperatureCalParam.getInWetBulbT());

                    double outputF = AirConditionUtils.FAirParmCalculate1(temperatureCalParam.getInDryBulbT(), temperatureCalParam.getInWetBulbT()).getParmF();
                    double outputNewF = AirConditionUtils.FAirParmCalculate1(temperatureCalParam.getNewDryBulbT(), temperatureCalParam.getNewWetBulbT()).getParmF();
                    mapAll.putAll(getMap(airConditionBean, seasonS, section, temperatureCalParam.getNARatio(), newNAVolume, outputF, outputNewF));
                    logger.info("TemperatureCalEngine calculator airConditionBeanS :" + JSONArray.toJSONString(airConditionBean));
              }
                    if(isWinter) {
                    temperatureCalParam = new InvokeTool<TemperatureCalParam>().genInParamFromAhuParam(ahu, section, seasonW, new TemperatureCalParam());
                    double newNAVolume = temperatureCalParam.getNAVolume();
                    if(newNAVolume != 0) {
                        temperatureCalParam.setNARatio(BaseDataUtil.decimalConvert((newNAVolume / airVolume), 2) * 100);
                    }
                    //计算前温度条件校验
                    //1:湿球不能大于干球
                    validateService.compareT(temperatureCalParam.getInDryBulbT(), temperatureCalParam.getInWetBulbT(), null);
                    validateService.compareT(temperatureCalParam.getNewDryBulbT(), temperatureCalParam.getNewWetBulbT(), null);

                    if(newNAVolume == 0) {
                        newNAVolume = airVolume * temperatureCalParam.getNARatio() / 100.0;//如果页面新风量为空，根据新风比计算
                    }

                    logger.info("TemperatureCalEngine calculator paramW :" + JSONArray.toJSONString(temperatureCalParam));
                    AirConditionBean airConditionBean = AirConditionUtils.mixAirCalculate(BaseDataUtil.integerConversionDouble(airVolume),
                            temperatureCalParam.getNARatio(), temperatureCalParam.getNewDryBulbT(), temperatureCalParam.getNewWetBulbT(),
                            temperatureCalParam.getInDryBulbT(), temperatureCalParam.getInWetBulbT());

                    double outputF = AirConditionUtils.FAirParmCalculate1(temperatureCalParam.getInDryBulbT(), temperatureCalParam.getInWetBulbT()).getParmF();
                    double outputNewF = AirConditionUtils.FAirParmCalculate1(temperatureCalParam.getNewDryBulbT(), temperatureCalParam.getNewWetBulbT()).getParmF();
                    mapAll.putAll(getMap(airConditionBean, seasonW, section, temperatureCalParam.getNARatio(), newNAVolume, outputF, outputNewF));
                    logger.info("TemperatureCalEngine calculator airConditionBeanW :" + JSONArray.toJSONString(airConditionBean));
                }
            section.getParams().putAll(mapAll);
        } catch (TempCalErrorException e) {
            String message = getIntlString(RESISTANCE_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("TemperatureCalEngine debug massage :" + message, e.getMessage());
            logger.error("TemperatureCalEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new TemperatureCalcException(RESISTANCE_CALCULATION_FAILED);
        }
        context.setSuccess(true);
        ICalContext subc = super.cal(ahu, section, context);
        subc.setSuccess(true);
        logger.info("TemperatureCalEngine calculator line end");
        return subc;
    }

    private Map<String, Object> getMap(AirConditionBean airConditionBean, String season, PartParam section, double nARatio, double nAVolume, double outputF, double outputNewF) {
        EngineConvertParam engineConvertParam = new EngineConvertParam();
        engineConvertParam.setInRelativeT(outputF);
        engineConvertParam.setNewRelativeT(outputNewF);
        engineConvertParam.setNARatio(BaseDataUtil.decimalConvert(nARatio, 1));//新风比
        engineConvertParam.setNAVolume(BaseDataUtil.decimalConvert(nAVolume, 1));//新风量
        engineConvertParam.setOutDryBulbT(BaseDataUtil.decimalConvert(airConditionBean.getParmT(), 1));//出风干球温度
        engineConvertParam.setOutWetBulbT(BaseDataUtil.decimalConvert(airConditionBean.getParmTb(), 1));//出风湿球温度
        engineConvertParam.setOutRelativeT(BaseDataUtil.decimalConvert(airConditionBean.getParmF(), 0));//出风相对湿度
        Map<String, Object> map = new InvokeTool<EngineConvertParam>().reInvoke(engineConvertParam, season, section.getKey());
        return map;
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{
                SectionTypeEnum.TYPE_MIX,
                SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER
        };
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_TEMPERATURE;
    }
}

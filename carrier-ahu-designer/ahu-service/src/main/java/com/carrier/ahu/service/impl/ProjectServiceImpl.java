package com.carrier.ahu.service.impl;

import static com.carrier.ahu.constant.CommonConstant.SYS_STRING_RECORDSTATUS_ARCHIEVED;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.AhuGroupBind;
import com.carrier.ahu.common.entity.GroupInfo;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.util.BeanUtilities;
import com.carrier.ahu.dao.ProjectDao;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.specification.GroupSpecifications;
import com.carrier.ahu.specification.PartitionSpecifications;
import com.carrier.ahu.specification.SectionSpecifications;
import com.carrier.ahu.specification.UnitSpecifications;
import com.carrier.ahu.unit.KeyGenerator;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by Wen zhengtao on 2017/3/17.
 */
@Service
public class ProjectServiceImpl extends AbstractService implements ProjectService {

	@Autowired
	ProjectDao projectDao;

	@Override
	public String addProject(Project project,String userName) {
		project.setCreateInfo(userName);
		projectDao.save(project);
		return project.getPid(); 
	}

	@Override
	public boolean exists(String no) {
		List<Project> list = projectDao.findAll(new Specification<Project>() {
			@Override
			public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> noPath = root.get(ServiceConstant.SYS_MAP_NO);
				query.where(cb.equal(noPath,no));
				return null;
			}
		});
		return CollectionUtils.isNotEmpty(list);
	}

    @Override
    public Project getProjectById(String pid) {
        if (EmptyUtil.isEmpty(pid)) {
            return null;
        }
        return projectDao.findOne(pid);
    }

	@Override
	public List<Project> findProjectList() {
		List<Project> list = (List<Project>)projectDao.findAll();
		return list;
	}
	@Override
	public List<Project> findProjectListArchieved() {
		List<Project> list = projectDao.findAll(new Specification<Project>() {
			@Override
			public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_RECORDSTATUS), SYS_STRING_RECORDSTATUS_ARCHIEVED);
				query.where(p1);
				return null;
			}
		});
		return list;
	}
	@Override
	public List<Project> findProjectListUnArchieved() {
		List<Project> list = projectDao.findAll(new Specification<Project>() {
			@Override
			public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.notEqual(root.get(ServiceConstant.SYS_STRING_RECORDSTATUS), SYS_STRING_RECORDSTATUS_ARCHIEVED);
				query.where(p1);
				return null;
			}
		});
		return list;
	}

	@Override
	public String updateProject(Project project,String userName) {
		project.setUpdateInfo(userName);
		projectDao.save(project);
		return project.getPid();
	}

	@Override
	public void deleteProject(String pid) {
		Project project = projectDao.findOne(pid);;
		projectDao.delete(pid);
	}

    @Override
    public String saveAs(Project fromProject, String projectName) {
        User user = AHUContext.getUser();
        Project project = BeanUtilities.clone(fromProject);

        List<Unit> fromUnits = this.ahuDao.findAll(UnitSpecifications.unitsOfProject(project.getPid()));
        List<GroupInfo> groups = this.groupDao.findAll(GroupSpecifications.groupsOfProject(project.getPid()));

        project.setPid(KeyGenerator.genProjectId(user.getUnitPreferCode(), user.getUserId()));
        project.setName(projectName);
        project.setCreateInfo(user.getUserName());
        this.projectDao.save(project);

        List<Unit> newUnits = Lists.newArrayList();
        
        for (Unit fromUnit : fromUnits) {
            Unit unit = BeanUtilities.clone(fromUnit);
            newUnits.add(unit);

            List<Part> sections = this.sectionDao.findAll(SectionSpecifications.sectionsOfUnit(unit.getUnitid()));
            List<Partition> partitions = this.partitionDao
                    .findAll(PartitionSpecifications.partitionsOfUnit(unit.getUnitid()));
            AhuGroupBind fromGroupBind = this.ahuGroupBindDao.findOne(unit.getUnitid());

            String newUnitId = KeyGenerator.genUnitId(user.getUnitPreferCode(), user.getUserId());
            unit.setUnitid(newUnitId);
            unit.setPid(project.getPid());
            unit.setCreateInfo(user.getUserName());
            this.ahuDao.save(unit);

            if (fromGroupBind != null) {
                AhuGroupBind groupBind = BeanUtilities.clone(fromGroupBind);
                groupBind.setUnitid(newUnitId);
                groupBind.setCreateInfo(user.getUserName());
                this.ahuGroupBindDao.save(groupBind);
            }

            for (Part fromSection : sections) {
                Part section = BeanUtilities.clone(fromSection);
                section.setPid(project.getPid());
                section.setUnitid(newUnitId);
                section.setPartid(KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId()));
                section.setCreateInfo(user.getUserName());
                this.sectionDao.save(section);
            }

            for (Partition fromPartition : partitions) {
                Partition partition = BeanUtilities.clone(fromPartition);
                partition.setPartitionid(null);
                partition.setUnitid(newUnitId);
                partition.setPid(project.getPid());
                partition.setCreateInfo(user.getUserName());
                this.partitionDao.save(partition);
            }
        }

        for (GroupInfo fromGroup : groups) {
            String newGroupId = KeyGenerator.genGroupId(user.getUnitPreferCode(), user.getUserId());

            GroupInfo group = BeanUtilities.clone(fromGroup);
            group.setProjectId(project.getPid());
            for (Unit unit : newUnits) {
                if (group.getGroupId().equals(unit.getGroupId())) {
                    unit.setGroupId(newGroupId);
                    AhuGroupBind groupBind = this.ahuGroupBindDao.findOne(unit.getUnitid());
                    if (groupBind != null) {
                        groupBind.setGroupid(newGroupId);
                        this.ahuGroupBindDao.save(groupBind);
                    }
                }
            }
            group.setGroupId(newGroupId);
            group.setCreateInfo(user.getUserName());
            this.groupDao.save(group);
            this.ahuDao.save(newUnits);
        }

        return project.getPid();
    }
}

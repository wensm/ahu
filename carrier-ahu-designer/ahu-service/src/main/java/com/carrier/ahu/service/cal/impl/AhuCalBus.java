package com.carrier.ahu.service.cal.impl;

import static com.carrier.ahu.common.intl.I18NConstants.AHU_CALCULATION_FINISHED;
import static com.carrier.ahu.common.intl.I18NConstants.AHU_CALCULATION_STARTED;
import static com.carrier.ahu.common.intl.I18NConstants.ENGIN_CALCULATION_FAILED;
import static com.carrier.ahu.common.intl.I18NConstants.ENGIN_CALCULATION_STARTED;
import static com.carrier.ahu.common.intl.I18NConstants.ENGIN_CALCULATION_SUCCESS;
import static com.carrier.ahu.common.intl.I18NConstants.GET_ENGINE_LIST;
import static com.carrier.ahu.common.intl.I18NConstants.SECTION_CALCULATION_STARTED;
import static com.carrier.ahu.common.intl.I18NConstants.SECTION_CALCULATION_SUCCESS;
import static com.carrier.ahu.common.intl.I18NConstants.TEMPERATURE_TRANSFER_FINISH;
import static com.carrier.ahu.common.intl.I18NConstants.TEMPERATURE_TRANSFER_START;
import static com.carrier.ahu.vo.SystemCalculateConstants.UNDERSCORE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.CalcTypeEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.UserMetaEnum;
import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.service.cal.CalContextUtil;
import com.carrier.ahu.service.cal.IAhuCalBus;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AhuCalBus implements IAhuCalBus {

	private CalOperationEnum calOperation = CalOperationEnum.BATCH;
	@SuppressWarnings("unused")
	private CalcTypeEnum calcType = CalcTypeEnum.ECONOMICAL;
	private static AhuCalBus instance = new AhuCalBus();

	private List<ICalEngine> engineList = new ArrayList<>();

	public static AhuCalBus getInstance() {
		return instance;
	}
	/**
	 * 注册一个新的计算引擎
	 * 
	 * @param calEngine
	 */
	public void registerCalEngine(ICalEngine calEngine) {

		this.engineList.add(calEngine);
		Collections.sort(this.engineList, new Comparator<ICalEngine>() {

			@Override
			public int compare(ICalEngine o1, ICalEngine o2) {
				return o1.getOrder() - o2.getOrder();
			}
		});

	}

	/**
	 * 输入段的类型，返回顺序的计算引擎
	 * 
	 * 过滤掉机组系列在Disabled列表里的引擎。
	 *
	 * @param sectionKey
	 * @param unitModel '39CQ'
	 * @param isNS
	 * @return
	 */
    public List<ICalEngine> getOrderedEngineArr(String sectionKey, String unitModel, String isNS) {
        String unitSeries = AhuUtil.getUnitSeries(unitModel);
        List<ICalEngine> list = new ArrayList<>();
        Iterator<ICalEngine> it = this.engineList.iterator();
        while (it.hasNext()) {
            ICalEngine engine = it.next();

			//点击前端非标计算按钮只进行非标计算引擎调用，其他引擎跳过
			if("true".equals(isNS)){
				//非标价格引擎
				if(ICalEngine.ID_NS_PRICE.equals(engine.getKey())) {
					list.add(engine);
					break;
				}
            }else{
				if (!ICalEngine.ID_NS_PRICE.equals(engine.getKey()) //非标价格引擎，不参与其他计算线路
						&& ArrayUtils.contains(engine.getEnabledSectionIds(), sectionKey)
						&& !ArrayUtils.contains(engine.getDisabledUnitSeries(), unitSeries)) {
					list.add(engine);
				}
            }
        }
        return list;
    }

	@Override
	public void calculate(AhuParam ahu, LanguageEnum language, ICalContext context) throws Exception {

        CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                I18NBundle.getString(AHU_CALCULATION_STARTED, language, ahu.getUnitNo(), ahu.getDrawingNo()));
		DefaultCalContext subContext = new DefaultCalContext();
		subContext.setText(ICalContext.NAME_AHU);
		subContext.setName(ahu.getName());
		subContext.setCalOperation(context.getCalOperation());
		subContext.setCalcTypeEnum(context.getCalType());
		subContext.setUserConfigParam(context.getUserConfigParam());
		context.enter(subContext);
		if (this.calOperation == CalOperationEnum.BATCH) {
			List<List<PartParam>> list = CalLineSupport.sortedParts(ahu);
			for (List<PartParam> plist : list) {
				Iterator<PartParam> it = plist.iterator();
				PartParam lastPart = null;
				while (it.hasNext()) {
					PartParam param = it.next();
                    CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                            I18NBundle.getString(TEMPERATURE_TRANSFER_START, language, ahu.getUnitNo()));
					UserMeta userMeta = AHUContext.getUserMeta(UserMetaEnum.TEMPERATURE_TRANSMIT);
					if (!EmptyUtil.isEmpty(userMeta)) {//根据用户设置是否启用温度传递
						if(BaseDataUtil.stringConversionBoolean(userMeta.getMetaValue())){
							CalLineSupport.transferTempreture(lastPart, param);
						}
					}
                    CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                            I18NBundle.getString(TEMPERATURE_TRANSFER_FINISH, language, ahu.getUnitNo()));
					innerCallEngine(ahu, param, language, subContext);
					lastPart = param;
				}
			}
            CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                    I18NBundle.getString(AHU_CALCULATION_FINISHED, language, ahu.getUnitNo(), ahu.getDrawingNo()));
			subContext.setSuccess(true);
			return;
		}

		Iterator<PartParam> it = ahu.getPartParams().iterator();
		while (it.hasNext()) {
			PartParam param = it.next();
			innerCallEngine(ahu, param, language, subContext);
		}
	}

	private void innerCallEngine(AhuParam ahu, PartParam param, LanguageEnum language, ICalContext context) throws Exception {


		param.getParams().put(SystemCalculateConstants.META_SECTION_COMPLETED, true);// 每次计算重置状态
		DefaultCalContext subContext = new DefaultCalContext();
		subContext.setText(ICalContext.NAME_PART);
		subContext.setName(param.getKey());
		subContext.setCalcType(context.getCalType());
		subContext.setCalOperation(context.getCalOperation());
		subContext.setUserConfigParam(context.getUserConfigParam());
		context.enter(subContext);
        CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                I18NBundle.getString(SECTION_CALCULATION_STARTED, language, ahu.getUnitNo(), param.getKey(), param.getOrders()));
        CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                I18NBundle.getString(GET_ENGINE_LIST, language, ahu.getUnitNo()));
		Iterator<ICalEngine> it = getOrderedEngineArr(String.valueOf(param.getKey()), ahu.getSeries(),ahu.getIsNS()).iterator();
		boolean successful = true;

		while (it.hasNext()) {
			ICalEngine engine = it.next();
			try {
				// comfirm按钮时，不能进行的操作不加进计算bus
				if (this.calOperation.getId() < engine.getCalLevel()) {
					log.debug("Ignore batch calculation engine " + engine.getKey());
					continue;
				}
                CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                        I18NBundle.getString(ENGIN_CALCULATION_STARTED, language, ahu.getUnitNo(), engine.getKey()));
                engine.cal(ahu, param, subContext);
				CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                        I18NBundle.getString(ENGIN_CALCULATION_SUCCESS, language, ahu.getUnitNo(), engine.getKey()));
			} catch (AhuException e) {
				log.error(String.format("Failed to call %s", engine.getKey()), e);
				// 当一个engine计算失败时，退出当前的段的计算，因为前后引擎之间存在依赖关系
                successful = false;
                if (e.getMessage().contains(UNDERSCORE)) {
                    String message = I18NBundle.getString(e.getMessage(), language, e.getParams());
                    CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(), message);
                    if (this.calOperation.getId() == CalOperationEnum.COMFIRM.getId()) {
                        throw new AhuException(message);
                    }
                } else {
                    String message = I18NBundle.getString(ENGIN_CALCULATION_FAILED, language, ahu.getUnitNo(),
                            engine.getKey());
                    CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(), message);
                    if (this.calOperation.getId() == CalOperationEnum.COMFIRM.getId()) {
                        throw new AhuException(message);
                    }
                }
                break;// 批量计算遇到报错的引擎，后面的所有已经不用计算。
            }
		}
		// 完成状态到已完成
		subContext.setSuccess(successful);
        CalContextUtil.getInstance().refreshQueueIncrementally(ahu.getPid(),
                I18NBundle.getString(SECTION_CALCULATION_SUCCESS, language, ahu.getUnitNo(), param.getKey(), param.getOrders()));
	}

	@Override
	public void setCalOperation(CalOperationEnum operation) {
		this.calOperation = operation;

	}

	@Override
	public void setCalType(CalcTypeEnum calType) {
		this.calcType = calType;
	}

}

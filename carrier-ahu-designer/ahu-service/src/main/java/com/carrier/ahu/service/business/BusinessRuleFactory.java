package com.carrier.ahu.service.business;

import org.springframework.stereotype.Component;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.service.business.damper.DamperBusinessRule;
import com.carrier.ahu.service.business.damper.DischargeDamperBusinessRule;
import com.carrier.ahu.service.business.damper.MixDamperBusinessRule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class BusinessRuleFactory {

    public DamperBusinessRule getDamperBusinessRule(Unit unit, Part section) {
        if (SectionTypeEnum.TYPE_MIX.getId().equals(section.getSectionKey())) {
            return new MixDamperBusinessRule(unit, section);
        } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(section.getSectionKey())) {
            return new DischargeDamperBusinessRule(unit, section);
        }

        log.error("the section [{}] has no damper.", section.getSectionKey());
        throw new IllegalArgumentException();
    }

}

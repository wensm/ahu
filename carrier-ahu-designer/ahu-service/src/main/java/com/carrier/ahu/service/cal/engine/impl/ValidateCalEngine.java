package com.carrier.ahu.service.cal.engine.impl;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.calculation.ValidateCalcException;
import com.carrier.ahu.engine.cad.IniUtils2;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.param.ValidateParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.InvokeTool;
import com.carrier.ahu.util.section.SectionRelationValidatorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;

/**
 * confirm 按钮校验引擎
 */
@Component
public class ValidateCalEngine extends AbstractCalEngine {
	private static Logger logger = LoggerFactory.getLogger(ValidateCalEngine.class.getName());

	@Override
	public int getOrder() {
		return 0;
	}

	@Override
	public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
		logger.info("validate line begin");
		ValidateParam validateParam = new InvokeTool<ValidateParam>().genInParamFromAhuParam(ahu, section, null, new ValidateParam());
		validateParam.setParams(section.getParams());
		SectionTypeEnum sectionTypeEnum = SectionTypeEnum.getSectionTypeFromId(section.getKey());
		validateParam.setSectionType(sectionTypeEnum.getCode());
		validateParam.setSectionId(sectionTypeEnum.getId());
		try {
			logger.info("validate param :" + JSONArray.toJSONString(validateParam));

			//获取左右段key
			PartParam prePartParam = IniUtils2.getPrePartParam(ahu, section.getPosition());
			PartParam nextPartParam = IniUtils2.getNextPartParam(ahu, section.getPosition());

			AhuLayout ahuLayout = ahu.getLayout();
			/*if (context.getCalOperation() == CalOperationEnum.BATCH
					&& ahuLayout.getStyle() >20) {//双层机组批量选型，现有架构设置上下段关系会出现不准确问题。屏蔽上下段
				prePartParam = null;
				nextPartParam = null;
			}*/

			//前后端都为空，则为选型段conform 操作；处理前后端
			if (null == prePartParam && null == nextPartParam) {
				if (null != section.getNextParams()) {
					validateParam.setNextPartKey(String.valueOf(section.getNextParams().get(ServiceConstant.SYS_MAP_AHU_KEY)));
					validateParam.setNextParams(section.getNextParams());
				}
				if (null != section.getPreParams()) {
					validateParam.setPrePartKey(String.valueOf(section.getPreParams().get(ServiceConstant.SYS_MAP_AHU_KEY)));
					validateParam.setPreParams(section.getPreParams());
				}
			} else {
				if (null != prePartParam) {
					validateParam.setPrePartKey(prePartParam.getKey());
					validateParam.setPreParams(prePartParam.getParams());
				}
				if (null != nextPartParam) {
					validateParam.setNextPartKey(nextPartParam.getKey());
					validateParam.setNextParams(nextPartParam.getParams());
				}
			}

			SectionRelationValidatorUtil.confirmValidate(validateParam);

			context.setSuccess(true);
			ICalContext subc = super.cal(ahu, section, context);
			subc.setSuccess(true);

			logger.info("validate line end");
			return subc;
		} catch (ValidateCalcException e) {
			String message = getIntlString(e.getMessage(), AHUContext.getLanguage());
			logger.debug("ValidateCalEngine debug massage :" + message, e.getMessage());
			logger.error("ValidateCalEngine error massage :" + e.getMessage());
			ICalContext subc = super.cal(ahu, section, context);
			subc.setSuccess(false);
			subc.error(message);
			context.setSuccess(false);
			section.getParams().put(META_SECTION_COMPLETED, false);
			throw new ValidateCalcException(e.getMessage());
		}
	}

	@Override
	public String[] getEnabledSectionIds() {
		SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{
				SectionTypeEnum.TYPE_FAN
				,SectionTypeEnum.TYPE_WWKFAN
				,SectionTypeEnum.TYPE_HEPAFILTER
				,SectionTypeEnum.TYPE_SINGLE
				,SectionTypeEnum.TYPE_COMPOSITE
				,SectionTypeEnum.TYPE_ELECTROSTATICFILTER
				,SectionTypeEnum.TYPE_ACCESS
		};
		String[] result = new String[sectionEs.length];
		for (int i = 0; i < sectionEs.length; i++) {
			result[i] = sectionEs[i].getId();
		}
		return result;
	}

	@Override
	public String getKey() {
		return ICalEngine.ID_VALIDATE;
	}

}

package com.carrier.ahu.service.impl;

import com.carrier.ahu.common.entity.GroupType;
import com.carrier.ahu.service.GroupTypeService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.EmptyUtil;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created by Simon on 2018-01-27.
 */
@Service
public class GroupTypeServiceImpl extends AbstractService implements GroupTypeService {

	@Override
	public String add(GroupType type, String userName) {
		type.setCreateInfo(userName);
		groupTypeDao.save(type);
		return type.getId();
	}

	@Override
	public String update(GroupType type, String userName) {
		type.setUpdateInfo(userName);
		groupTypeDao.save(type);
		return type.getId();
	}

	@Override
	public void deleteType(String id) {
		groupTypeDao.delete(id);
	}

	@Override
	public GroupType getById(String id) {
		GroupType type = groupTypeDao.findOne(id);
		return type;
	}

	@Override
	public List<GroupType> findListByProjectId(String projectId) {
		List<GroupType> list = groupTypeDao.findAll(new Specification<GroupType>() {
			@Override
			public Predicate toPredicate(Root<GroupType> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> projectIdPath = root.get(ServiceConstant.SYS_STRING_PROJECTID);// 项目ID
				query.where(cb.equal(projectIdPath, projectId));
				return null;
			}
		});
		return list;
	}

	@Override
	public Iterable<GroupType> findAll() {
		return groupTypeDao.findAll();
	}

	@Override
	public boolean checkName(String projectId, String name) {
		List<GroupType> list = groupTypeDao.findAll(new Specification<GroupType>() {
			@Override
			public Predicate toPredicate(Root<GroupType> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> projectIdPath = root.get(ServiceConstant.SYS_STRING_PROJECTID);// 项目ID
				Path<String> namePath = root.get(ServiceConstant.SYS_MAP_NAME);// 名称
				Predicate p1 = cb.equal(projectIdPath, projectId);
				Predicate p2 = cb.equal(namePath, name);
				query.where(cb.and(p1, p2));
				return null;
			}
		});
		if (EmptyUtil.isEmpty(list)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<GroupType> findListByCode(String projectId, String code) {
		List<GroupType> list = groupTypeDao.findAll(new Specification<GroupType>() {
			@Override
			public Predicate toPredicate(Root<GroupType> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> projectIdPath = root.get(ServiceConstant.SYS_STRING_PROJECTID);// 项目ID
				Path<String> codePath = root.get(ServiceConstant.SYS_STRING_CODE);// 名称
				Predicate p1 = cb.equal(projectIdPath, projectId);
				Predicate p2 = cb.equal(codePath, code);
				if (EmptyUtil.isEmpty(projectId)) {
					query.where(p2);
				} else {
					query.where(cb.and(p1, p2));
				}
				return null;
			}
		});
		return list;
	}

}

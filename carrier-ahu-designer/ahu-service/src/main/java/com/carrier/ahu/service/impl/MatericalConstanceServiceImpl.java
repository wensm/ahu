package com.carrier.ahu.service.impl;

import com.carrier.ahu.common.entity.MatericalConstance;
import com.carrier.ahu.dao.MatericalConstanceDao;
import com.carrier.ahu.service.MatericalConstanceService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.EmptyUtil;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

/**
 * Created by Wen zhengtao on 2017/3/17.
 */
@Service
public class MatericalConstanceServiceImpl extends AbstractService implements MatericalConstanceService {
	@Autowired
	MatericalConstanceDao matericalConstanceDao;

	@Override
	public String addConstance(MatericalConstance constance, String userName) {
		constance.setCreateInfo(userName);
		matericalConstanceDao.save(constance);
		return constance.getPid();
	}

	@Override
	public boolean exists(String projid, String groupid) {
		List<MatericalConstance> list = matericalConstanceDao.findAll(new Specification<MatericalConstance>() {
			@Override
			public Predicate toPredicate(Root<MatericalConstance> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJID), projid);
				Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID1), groupid);
				if (EmptyUtil.isEmpty(groupid)) {
					p2 = cb.isNull(root.get(ServiceConstant.SYS_STRING_GROUPID1));
				}
				query.where(cb.and(p1, p2));
				return null;
			}
		});
		return CollectionUtils.isNotEmpty(list);
	}

	@Override
	public MatericalConstance getConstanceById(String pid) {
		return matericalConstanceDao.findOne(pid);
	}

	@Override
	public MatericalConstance getConstanceByProjidAndGroupid(String projid, String groupid) {
		List<MatericalConstance> list = matericalConstanceDao.findAll(new Specification<MatericalConstance>() {
			@Override
			public Predicate toPredicate(Root<MatericalConstance> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJID), projid);
				Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID1), groupid);
				if (EmptyUtil.isEmpty(groupid)) {
					p2 = cb.isNull(root.get(ServiceConstant.SYS_STRING_GROUPID1));
				}
				query.where(cb.and(p1, p2));
				return null;
			}
		});
		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<MatericalConstance> findByProjectid(String projid) {
		List<MatericalConstance> list = matericalConstanceDao.findAll(new Specification<MatericalConstance>() {
			@Override
			public Predicate toPredicate(Root<MatericalConstance> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJID), projid);
				query.where(p1);
				return null;
			}
		});
		return list;
	}

	@Override
	public String updateConstance(MatericalConstance constance, String userName) {
		constance.setUpdateInfo(userName);
		matericalConstanceDao.save(constance);
		return constance.getPid();
	}

	@Override
	public void deleteConstance(String pid) {
		MatericalConstance constance = matericalConstanceDao.findOne(pid);
		matericalConstanceDao.delete(constance);
	}

	@Override
	public List<String> getAllIds(String pid) {
		List<String> idList = new ArrayList<>();
		if (EmptyUtil.isEmpty(pid)) {
			return idList;
		}
		List<MatericalConstance> list = matericalConstanceDao.findAll(new Specification<MatericalConstance>() {
			@Override
			public Predicate toPredicate(Root<MatericalConstance> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJID), pid);
				query.where(p1);
				return null;
			}
		});
		if (EmptyUtil.isNotEmpty(list)) {
			for (MatericalConstance bean : list) {
				idList.add(bean.getPid());
			}
		}
		return idList;
	}
}

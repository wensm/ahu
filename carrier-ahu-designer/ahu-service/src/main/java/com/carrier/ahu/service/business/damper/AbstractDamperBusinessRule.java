package com.carrier.ahu.service.business.damper;

import static com.carrier.ahu.util.meta.SectionMetaUtils.getMetaSectionKey;
import static com.carrier.ahu.util.meta.SectionMetaUtils.getSectionMetaValueInt;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_MODE_A;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_MODE_B;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_MODE_C;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_MODE_D;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_POS_SIZE_A;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_POS_SIZE_B;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_POS_SIZE_C;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_POS_SIZE_D;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_SIZE_X;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_SIZE_Y;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_SECTIONL;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39CQ;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39G;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39XT;

import java.util.Map;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.DamperMaterialEnum;
import com.carrier.ahu.common.enums.DamperPosEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.engine.service.damper.DamperSizeService;
import com.carrier.ahu.engine.service.damper.DamperSizeServiceImpl;
import com.carrier.ahu.service.business.section.AbstractSectionBusinessRule;
import com.carrier.ahu.vo.DamperPosSizeInputVO;
import com.carrier.ahu.vo.DamperPosSizeResultVO;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

public abstract class AbstractDamperBusinessRule extends AbstractSectionBusinessRule implements DamperBusinessRule {

    // 风阀叶片长度/宽度最小尺寸需要大于等于3
    private static final int DAMPER_BLADE_MIN_SIZE = 3;

    // 风量/风阀改变后的面积计算风速，风速限定于 <=8m/s
    private static final int DAMPER_FAN_SPEED_THRESHOLD = 8;

    // 39G, 2M 39CQ/39XT, 1M
    private static final Map<String, Integer> DAMPER_PANEL_MARGIN_MIN_SIZES = ImmutableMap.of(AHU_PRODUCT_39CQ, 1,
            AHU_PRODUCT_39XT, 1, AHU_PRODUCT_39G, 2);

    private DamperSizeService damperSizeService = new DamperSizeServiceImpl();

    public AbstractDamperBusinessRule(Unit unit, Part section) {
        super(unit, section);
    }

    @Override
    public Map<String, Object> adjustDamperSize(DamperPosEnum damperPos) {
        if (!sectionType().getId().equals(section.getSectionKey())) {
            throw new IllegalArgumentException();
        }

        DamperPosSizeInputVO inputVo = new DamperPosSizeInputVO();
        inputVo.setProduct(unit.getProduct());
        inputVo.setDamperMaterial(getDamperMaterial(sectionMeta));
        inputVo.setUnitLengthMode(getUnitLengthMode(unit, getSectionLength(sectionMeta), damperPos));
        inputVo.setModeA(getDamperModeA(sectionMeta, damperPos));
        inputVo.setModeB(getDamperModeB(sectionMeta, damperPos));
        inputVo.setUnitWidthMode(getUnitWidthMode(unit, damperPos));
        inputVo.setModeC(getDamperModeC(sectionMeta, damperPos));
        inputVo.setModeD(getDamperModeD(sectionMeta, damperPos));

        if (!validateDamperPanelMarginSize(inputVo.getProduct(), inputVo.getModeA())) {
            throw new ApiException(ErrorCode.INVALID_DAMPER_PANEL_MARGIN,
                    DAMPER_PANEL_MARGIN_MIN_SIZES.get(inputVo.getProduct()).toString());
        }
        if (!validateDamperPanelMarginSize(inputVo.getProduct(), inputVo.getModeB())) {
            throw new ApiException(ErrorCode.INVALID_DAMPER_PANEL_MARGIN,
                    DAMPER_PANEL_MARGIN_MIN_SIZES.get(inputVo.getProduct()).toString());
        }
        if (!validateDamperBladeSize(inputVo.getModeA(), inputVo.getModeB(), inputVo.getUnitLengthMode())) {
            throw new ApiException(ErrorCode.INVALID_DAMPER_BLADE_SIZE, String.valueOf(DAMPER_BLADE_MIN_SIZE));
        }
        if (!validateDamperPanelMarginSize(inputVo.getProduct(), inputVo.getModeC())) {
            throw new ApiException(ErrorCode.INVALID_DAMPER_PANEL_MARGIN,
                    DAMPER_PANEL_MARGIN_MIN_SIZES.get(inputVo.getProduct()).toString());
        }
        if (!validateDamperPanelMarginSize(inputVo.getProduct(), inputVo.getModeD())) {
            throw new ApiException(ErrorCode.INVALID_DAMPER_PANEL_MARGIN,
                    DAMPER_PANEL_MARGIN_MIN_SIZES.get(inputVo.getProduct()).toString());
        }
        if (!validateDamperBladeSize(inputVo.getModeC(), inputVo.getModeD(), inputVo.getUnitWidthMode())) {
            throw new ApiException(ErrorCode.INVALID_DAMPER_BLADE_SIZE, String.valueOf(DAMPER_BLADE_MIN_SIZE));
        }

        DamperPosSizeResultVO resultVo = this.damperSizeService.adjustDamperSize(inputVo);
        calculateFanSpeed(damperPos, resultVo);

        if (resultVo.getFanSpeed() > DAMPER_FAN_SPEED_THRESHOLD) {
            throw new ApiException(ErrorCode.DAMPER_FAN_SPEED_EXCEED_THRESHOLD,
                    String.valueOf(DAMPER_FAN_SPEED_THRESHOLD));
        }
        return convertToDamperMetadata(resultVo, damperPos);
    }

    private int getSectionLength(Map<String, Object> sectionMeta) {
        return getSectionMetaValueInt(sectionType(), KEY_SECTIONL, sectionMeta);
    }

    protected abstract DamperMaterialEnum getDamperMaterial(Map<String, Object> sectionMeta);

    private int getUnitLengthMode(Unit unit, int sectionLength, DamperPosEnum damperPos) {
        int width = AhuUtil.getWidthOfAHU(unit.getSeries());
        switch (damperPos) {
        case Top:
        case Bottom:
        case Left:
        case Right:
            return sectionLength;
        case Back:
            return width;
        default:
            throw new IllegalArgumentException();
        }
    }

    private int getDamperModeA(Map<String, Object> sectionMeta, DamperPosEnum damperPos) {
        return getSectionMetaValueInt(sectionType(), KEY_DAMPER_MODE_A.apply(damperPos), sectionMeta);
    }

    private int getDamperModeB(Map<String, Object> sectionMeta, DamperPosEnum damperPos) {
        return getSectionMetaValueInt(sectionType(), KEY_DAMPER_MODE_B.apply(damperPos), sectionMeta);
    }

    private int getUnitWidthMode(Unit unit, DamperPosEnum damperPos) {
        int width = AhuUtil.getWidthOfAHU(unit.getSeries());
        int height = AhuUtil.getHeightOfAHU(unit.getSeries());
        switch (damperPos) {
        case Top:
        case Bottom:
            return width;
        case Left:
        case Right:
            return height;
        case Back:
            return height;
        default:
            throw new IllegalArgumentException();
        }
    }

    private int getDamperModeC(Map<String, Object> sectionMeta, DamperPosEnum damperPos) {
        return getSectionMetaValueInt(sectionType(), KEY_DAMPER_MODE_C.apply(damperPos), sectionMeta);
    }

    private int getDamperModeD(Map<String, Object> sectionMeta, DamperPosEnum damperPos) {
        return getSectionMetaValueInt(sectionType(), KEY_DAMPER_MODE_D.apply(damperPos), sectionMeta);
    }

    private boolean validateDamperBladeSize(int a, int b, int mode) {
        // a + b + MDI_X = 机组模数
        // MDI_X = (mode - a - b)
        return (mode - a - b) >= DAMPER_BLADE_MIN_SIZE;
    }

    /* 面板长度设置的话，需要满足最小长度限制 */
    private boolean validateDamperPanelMarginSize(String product, int a) {
        return a == 0 || a >= DAMPER_PANEL_MARGIN_MIN_SIZES.get(product);
    }

    protected abstract SectionTypeEnum sectionType();

    protected abstract void calculateFanSpeed(DamperPosEnum damperPos, DamperPosSizeResultVO resultVo);

    private Map<String, Object> convertToDamperMetadata(DamperPosSizeResultVO resultVo, DamperPosEnum damperPos) {
        Map<String, Object> metadata = Maps.newTreeMap();
        metadata.put(getMetaSectionKey(sectionType(), KEY_DAMPER_POS_SIZE_A.apply(damperPos)), resultVo.getPosSizeA());
        metadata.put(getMetaSectionKey(sectionType(), KEY_DAMPER_POS_SIZE_B.apply(damperPos)), resultVo.getPosSizeB());
        metadata.put(getMetaSectionKey(sectionType(), KEY_DAMPER_POS_SIZE_C.apply(damperPos)), resultVo.getPosSizeC());
        metadata.put(getMetaSectionKey(sectionType(), KEY_DAMPER_POS_SIZE_D.apply(damperPos)), resultVo.getPosSizeD());
        metadata.put(getMetaSectionKey(sectionType(), KEY_DAMPER_SIZE_X.apply(damperPos)), resultVo.getSizeX());
        metadata.put(getMetaSectionKey(sectionType(), KEY_DAMPER_SIZE_Y.apply(damperPos)), resultVo.getSizeY());
        return metadata;
    }

}

package com.carrier.ahu.service.business.section;

import static com.carrier.ahu.common.enums.AirDirectionEnum.RETURNAIR;
import static com.carrier.ahu.common.enums.AirDirectionEnum.SUPPLYAIR;
import static com.carrier.ahu.constant.CommonConstant.METAHU_EAIRVOLUME;
import static com.carrier.ahu.constant.CommonConstant.METAHU_SAIRVOLUME;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_AIRDIRECTION;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.service.business.AbstractBusinessRule;
import com.carrier.ahu.util.meta.SectionMetaUtils;

public abstract class AbstractSectionBusinessRule extends AbstractBusinessRule implements SectionBusinessRule {

    public AbstractSectionBusinessRule(Unit unit, Part section) {
        super(unit, section);
    }

    @Override
    public boolean isSupplierAir() {
        String airDirection = sectionMeta.get(METASEXON_AIRDIRECTION).toString();
        return SUPPLYAIR.getCode().equals(airDirection);
    }

    @Override
    public boolean isReturnAir() {
        String airDirection = sectionMeta.get(METASEXON_AIRDIRECTION).toString();
        return RETURNAIR.getCode().equals(airDirection);
    }

    @Override
    public double getSupplierAirVolume() {
        return SectionMetaUtils.getSectionMetaValueDouble(METAHU_SAIRVOLUME, unitMeta);
    }

    @Override
    public double getReturnAirVolume() {
        return SectionMetaUtils.getSectionMetaValueDouble(METAHU_EAIRVOLUME, unitMeta);
    }

}

package com.carrier.ahu.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.carrier.ahu.dao.AhuDao;
import com.carrier.ahu.dao.AhuGroupBindDao;
import com.carrier.ahu.dao.AhuTemplateDao;
import com.carrier.ahu.dao.BatchCfgProgressDao;
import com.carrier.ahu.dao.FactoryDao;
import com.carrier.ahu.dao.GroupDao;
import com.carrier.ahu.dao.GroupTypeDao;
import com.carrier.ahu.dao.PartitionDao;
import com.carrier.ahu.dao.ProjectDao;
import com.carrier.ahu.dao.SectionDao;
import com.carrier.ahu.dao.UserDao;
import com.carrier.ahu.dao.UserMetaDao;

/**
 * 业务层基类 Created by Wen zhengtao on 2017/3/12.
 */
public abstract class AbstractService {
    protected static Logger logger = LoggerFactory.getLogger(AbstractService.class);
    @Autowired
    ProjectDao projectDao;
    @Autowired
    GroupDao groupDao;
    @Autowired
    AhuDao ahuDao;
    @Autowired
    SectionDao sectionDao;
    @Autowired
    AhuTemplateDao ahuTemplateDao;
    @Autowired
    AhuGroupBindDao ahuGroupBindDao;
    @Autowired
    PartitionDao partitionDao;
    @Autowired
    BatchCfgProgressDao progressDao;
    @Autowired
    UserDao userDao;
    @Autowired
    FactoryDao factoryDao;
    @Autowired
    GroupTypeDao groupTypeDao;
    @Autowired
    UserMetaDao userMetaDao;

}

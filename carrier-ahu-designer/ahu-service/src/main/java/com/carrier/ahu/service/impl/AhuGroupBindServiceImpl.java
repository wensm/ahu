package com.carrier.ahu.service.impl;

import com.carrier.ahu.common.entity.AhuGroupBind;
import com.carrier.ahu.service.AhuGroupBindService;
import com.carrier.ahu.service.constant.ServiceConstant;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

/**
 * Created by Zhang Zhiqiang on 2017/6/30.
 */
@Service
public class AhuGroupBindServiceImpl extends AbstractService implements AhuGroupBindService {

	@Override
	public String addBind(AhuGroupBind bind, String username) {
		bind.setCreateInfo(username);

		if (StringUtils.isBlank(bind.getUnitid())) {
			return null;
		} else {
			boolean b = ahuGroupBindDao.exists(bind.getUnitid());
			if (b) {
				ahuGroupBindDao.delete(bind.getUnitid());
			}
			ahuGroupBindDao.save(bind);
		}

		return bind.getUnitid();
	}

	@Override
	public void deleteBind(String ahuId) {
		boolean b = ahuGroupBindDao.exists(ahuId);
		if (b) {
			ahuGroupBindDao.delete(ahuId);
		}
	}

	@Override
	public void deleteGroup(String groupId) {
		List<AhuGroupBind> list = findByGroup(groupId);
		if (!list.isEmpty()) {
			ahuGroupBindDao.delete(list);
		}
	}

	@Override
	public AhuGroupBind getByAhuId(String ahuId) {
		return ahuGroupBindDao.findOne(ahuId);
	}

	@Override
	public List<AhuGroupBind> findByGroup(String groupId) {
		List<AhuGroupBind> list = ahuGroupBindDao.findAll(new Specification<AhuGroupBind>() {
			@Override
			public Predicate toPredicate(Root<AhuGroupBind> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				query.where(cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID1), groupId));
				return null;
			}
		});
		return list;
	}

	@Override
	public List<String> addBinds(List<AhuGroupBind> binds, String username) {
		List<String> l = new ArrayList<String>();
		Iterable<AhuGroupBind> list = ahuGroupBindDao.save(binds);
		for (AhuGroupBind b : list) {
			l.add(b.getUnitid());
		}
		return l;
	}

}

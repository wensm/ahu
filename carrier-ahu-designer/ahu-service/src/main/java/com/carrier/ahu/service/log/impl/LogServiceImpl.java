package com.carrier.ahu.service.log.impl;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carrier.ahu.common.entity.LoginoutLog;
import com.carrier.ahu.dao.LogDao;
import com.carrier.ahu.service.log.LogService;
import com.carrier.ahu.vo.SysConstants;

/**
 * Created by Wen zhengtao on 2017/6/20.
 */
@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogDao logDao;

    @Override
    public String saveLog(LoginoutLog log) {
        logDao.save(log);
        return log.getLogId();
    }

    @Override
    public List<File> getSystemLogFiles() {
        File logDir = new File(SysConstants.LOG_DIR);
        if (logDir!=null & logDir.exists() && logDir.isDirectory()) {
            return Arrays.asList(logDir.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().endsWith(SysConstants.LOG_EXTENSION);
                }
            }));
        }
        return new ArrayList<>();
    }

}

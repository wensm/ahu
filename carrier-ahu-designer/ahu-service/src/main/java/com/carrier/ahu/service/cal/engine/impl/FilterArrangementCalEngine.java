package com.carrier.ahu.service.cal.engine.impl;

import static com.carrier.ahu.common.intl.I18NConstants.FILTER_ARRANGEMENT_CALCULATION_FAILED;
import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.calculator.PerformanceCalculator;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.calculation.FilterArrangementCalcException;
import com.carrier.ahu.length.param.FilterPanelBean;
import com.carrier.ahu.length.param.FilterPanelParam;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.InvokeTool;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * 过滤器布置计算 单层过滤段 综合过滤段 静电过滤段 高效过滤段
 */
@Component
public class FilterArrangementCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(FilterArrangementCalEngine.class.getName());

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        try {
            logger.info("FilterArrangementCalEngine calculator line begin");
            FilterPanelParam filterPanelParam = new InvokeTool<FilterPanelParam>().genInParamFromAhuParam(ahu, section,
                    null, new FilterPanelParam());
            SectionTypeEnum sectionTypeEnum = SectionTypeEnum.getSectionTypeFromId(section.getKey());
            filterPanelParam.setSectionType(sectionTypeEnum.getCode());
            logger.info("FilterArrangementCalEngine calculator param :" + JSONArray.toJSONString(filterPanelParam));
            List<FilterPanelBean> filterPanelBeanList = PerformanceCalculator.filterPanelBeanList(filterPanelParam);
            if (!EmptyUtil.isEmpty(filterPanelBeanList)) {
                String filterArrange = JSONArray.toJSONString(filterPanelBeanList).toString();
                logger.info("FilterArrangementCalEngine calculator filterPanelBeanList :" + JSONArray.toJSONString(filterPanelBeanList));
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_FILTERARRANGE, String.valueOf(filterArrange));
            } else {
                if(SystemCalculateConstants.HEPAFILTER_SUPPLIER_NONE.equals(filterPanelParam.getSupplier())) {
                    logger.info("supplier is none the panel layout should be empty ");
                }else if (SystemCalculateConstants.COMBINEDFILTER_LMATERIAL_NO.equals(filterPanelParam.getLMaterial()) || 
                		SystemCalculateConstants.COMBINEDFILTER_RMATERIAL_NO_FILTER.equals(filterPanelParam.getRMaterial())) {
					logger.info("Lmaterial or Rmaterial is none, the panel layout should be empty ");
				}else{
                    logger.error("FilterArrangementCalEngine calculator error filterPanelBeanList is null");
                    throw new FilterArrangementCalcException("FilterArrangementCalEngine calculator error filterPanelBeanList is null");
                }
            }
            context.setSuccess(true);
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(true);
            logger.info("FilterArrangementCalEngine calculator line end");
            return subc;
        } catch (Exception e) {
            e.printStackTrace();
            String message = getIntlString(FILTER_ARRANGEMENT_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("FilterArrangementCalEngine debug massage :" + message, e.getMessage());
            logger.error("FilterArrangementCalEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new FilterArrangementCalcException(FILTER_ARRANGEMENT_CALCULATION_FAILED);
        }
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{
                SectionTypeEnum.TYPE_SINGLE,
                SectionTypeEnum.TYPE_COMPOSITE,
                SectionTypeEnum.TYPE_ELECTROSTATICFILTER,
                SectionTypeEnum.TYPE_HEPAFILTER
        };
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_FILTERARRANGEMENT;
    }

}

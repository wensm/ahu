package com.carrier.ahu.service.impl;

import static com.carrier.ahu.constant.CommonConstant.SYS_BLANK;
import static com.carrier.ahu.util.meta.SectionMetaUtils.getMetaSectionKey;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_POSITIVE_PRESSURE_DOOR;
import static com.carrier.ahu.util.partition.AhuPartitionGenerator.AHU_PRODUCT_CASING_WIDTH;
import static com.google.common.base.Preconditions.checkArgument;

import java.io.File;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.carrier.ahu.report.ExportNsFile;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.metadata.entity.AhuSizeDetail;
import com.carrier.ahu.po.UnitType;
import com.carrier.ahu.report.ExportPart;
import com.carrier.ahu.report.ExportPartition;
import com.carrier.ahu.report.ExportUnit;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.specification.PartitionSpecifications;
import com.carrier.ahu.specification.SectionSpecifications;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ValueFormatUtil;
import com.carrier.ahu.util.ahu.AhuProductUtils;
import com.carrier.ahu.util.meta.AhuMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.vo.SysConstants;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.carrier.ahu.vo.UpdateUnitVO;
import com.google.gson.Gson;

/**
 * Created by Wen zhengtao on 2017/3/17.
 */
@Service
public class AhuServiceImpl extends AbstractService implements AhuService {
    @Override
    public String addAhu(Unit unit, String userName) {
        unit.setCreateInfo(userName);
        try {
            String newUnitNO = ValueFormatUtil.getUsefulUnitNOWhenImport(findAllUnitNOInuse(unit.getPid()),
                    Integer.parseInt(unit.getDrawingNo()));
            if (EmptyUtil.isEmpty(newUnitNO)) {
                return null;
            }
            unit.setUnitNo(newUnitNO);
            ahuDao.save(unit);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return unit.getUnitid();
    }

    @Override
    public String updateAhu(Unit unit, String userName) {
        unit.setUpdateInfo(userName);
        ahuDao.save(unit);
        return unit.getUnitid();
    }


    @Override
    public Unit findAhuListByPIdAndUnitNo(String projectId, String unitNo) {
        Unit unit = ahuDao.findOne(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
                if (StringUtils.isNotBlank(unitNo)) {
                    Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_UNITNO), unitNo);
                    query.where(cb.and(p1, p2));
                } else {
                    query.where(p1);
                }
                return null;
            }
        });
        return unit;
    }

    @Override
    public List<String> addOrUpdateAhus(List<Unit> units) {
        ahuDao.save(units);
        List<String> list = new ArrayList<>();
        for (Unit u : units) {
            list.add(u.getUnitid());
        }
        return list;
    }

    @Override
    public void deleteAhu(String ahuId) {
        ahuDao.delete(ahuId);
    }

    @Override
    public Unit findAhuById(String ahuId) {
        Unit ahu = ahuDao.findOne(ahuId);
        return ahu;
    }

    @Override
    public Page<Unit> findAhuList(String projectId, String groupId, Pageable pageable) {
        Page<Unit> list = ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
                Predicate p2 = null;
                if (StringUtils.isEmpty(groupId)) {
                    Predicate p = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), ServiceConstant.SYS_BLANK);
                    Predicate p0 = cb.isNull(root.get(ServiceConstant.SYS_STRING_GROUPID));
                    p2 = cb.or(p, p0);
                } else {
                    p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), groupId);
                }

                query.where(cb.and(p1, p2));
                return null;
            }
        }, pageable);
        return list;
    }

    @Override
    public List<Unit> findAhuList(String projectId, String groupId) {
        List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
                if (StringUtils.isNotBlank(groupId)) {
                    Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), groupId);
                    query.where(cb.and(p1, p2));
                } else {
                    query.where(p1);
                }
                return null;
            }
        });
        return list;
    }

    @Override
    public List<Unit> findAhuListByRecordStatus(String projectId, String recordStatus) {
        List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
                if (StringUtils.isNotBlank(recordStatus)) {
                    Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_RECORD_STATUS), recordStatus);
                    query.where(cb.and(p1, p2));
                } else {
                    query.where(p1);
                }
                query.orderBy(cb.asc(root.get(ServiceConstant.SYS_STRING_UNITNO).as(String.class)));
                return null;
            }
        });
        return list;
    }

    @Override
    public List<Unit> findAhuListByCustomerName(String customerName, String ahuId) {
        List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_CUSTOMERNAME), customerName);
                Predicate p2 = cb.notEqual(root.get(ServiceConstant.SYS_MSG_REQUEST_UNITID), ahuId);
                query.where(cb.and(p1, p2));
                return null;
            }
        });
        return list;
    }

    @Override
    public List<UnitType> getUnitTypes() {
        List<UnitType> unitTypes = new ArrayList<UnitType>();
        UnitType unitType1 = new UnitType();
        unitType1.setTypeName("39G1317");
        unitType1.setVelocity("2.97");
        unitTypes.add(unitType1);
        UnitType unitType2 = new UnitType();
        unitType2.setTypeName("39G1418");
        unitType2.setVelocity("2.63");
        unitTypes.add(unitType2);
        UnitType unitType3 = new UnitType();
        unitType3.setTypeName("39G1420");
        unitType3.setVelocity("2.34");
        unitTypes.add(unitType3);
        UnitType unitType4 = new UnitType();
        unitType4.setTypeName("39G1621");
        unitType4.setVelocity("1.91");
        unitTypes.add(unitType4);
        UnitType unitType5 = new UnitType();
        unitType5.setTypeName("39G1822");
        unitType5.setVelocity("1.53");
        unitTypes.add(unitType5);
        UnitType unitType6 = new UnitType();
        unitType6.setTypeName("39G1825");
        unitType6.setVelocity("1.33");
        unitTypes.add(unitType6);
        UnitType unitType7 = new UnitType();
        unitType7.setTypeName("39G2025");
        unitType7.setVelocity("1.24");
        unitTypes.add(unitType7);
        UnitType unitType8 = new UnitType();
        unitType8.setTypeName("39G2125");
        unitType8.setVelocity("1.15");
        unitTypes.add(unitType8);
        UnitType unitType9 = new UnitType();
        unitType9.setTypeName("39G2226");
        unitType9.setVelocity("1.04");
        unitTypes.add(unitType9);
        UnitType unitType10 = new UnitType();
        unitType10.setTypeName("39G2328");
        unitType10.setVelocity("0.93");
        unitTypes.add(unitType10);
        UnitType unitType11 = new UnitType();
        unitType11.setTypeName("39G2330");
        unitType11.setVelocity("0.88");
        unitTypes.add(unitType11);
        UnitType unitType12 = new UnitType();
        unitType12.setTypeName("39G2333");
        unitType12.setVelocity("0.78");
        unitTypes.add(unitType12);
        return unitTypes;
    }

    @Override
    public String getMaxAhuId() {
        List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                return null;
            }
        });
        String max = "000";
        for (Unit u : list) {
            if (!ValueFormatUtil.isUnitNoLegal(u.getUnitNo())) {
                continue;
            }
            if (max.compareTo(u.getUnitNo()) < 0) {
                max = u.getUnitNo();
            }
        }
        return max;
    }

    @Override
    public String getMaxdrawingNo() {
        return Integer.parseInt(getMaxAhuId()) + ServiceConstant.SYS_BLANK;
    }

    public int getMaxDrawingNo(String projectId) {
        checkArgument(EmptyUtil.isNotEmpty(projectId));

        List<Unit> units = findAhuList(projectId, null);
        return units.stream().mapToInt(u -> Integer.valueOf(u.getDrawingNo())).max().orElse(0);
    }

    @Override
    public Unit findAhuByDrawingNO(String projectid, String drawingNO) {
        List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectid);
                Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_MSG_RESPONSE_RESULT_DRAWINGNO), drawingNO);
                query.where(cb.and(p1, p2));
                return null;
            }
        });
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }

    @Override
    public Unit findAhuByDrawingNOAndGroupId(String projectid, String drawingNO, String groupId) {
        List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectid);
                Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_MSG_RESPONSE_RESULT_DRAWINGNO), drawingNO);
                Predicate p3 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), groupId);
                query.where(cb.and(p1, p2, p3));
                return null;
            }
        });
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }

    /**
     * 获取所有在用的机组编号
     * 
     * @throws Exception
     */
    @Override
    public List<String> findAllUnitNOInuse(String projectId) throws Exception {
        if (EmptyUtil.isEmpty(projectId)) {
            throw new Exception("查询机组编号，projectId Is null");
        }
        List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
                query.where(p1);
                return null;
            }
        });
        List<String> noList = new ArrayList<>();
        for (Unit u : list) {
            noList.add(u.getUnitNo());
        }
        return noList;
    }

    @Override
    public List<String> fixUnitNo(String projectId) {
        List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
                query.where(p1);
                return null;
            }
        });
        Collections.sort(list, new Comparator<Unit>() {
            public int compare(Unit o1, Unit o2) {
                return o1.getUnitNo().compareTo(o2.getUnitNo());
            }
        });
        List<Unit> toUpdateList = new ArrayList<>();
        for (int i = 1; i < list.size(); i++) {
            Unit unit = list.get(i - 1);
            unit.setUnitNo(ValueFormatUtil.transNumberic2String(i, 3));
            toUpdateList.add(unit);
        }
        List<String> lists = addOrUpdateAhus(toUpdateList);
        return lists;
    }

    @Override
    public List<String> getAllIds(String pid) {
        List<String> idList = new ArrayList<>();
        if (EmptyUtil.isEmpty(pid)) {
            return idList;
        }
        List<Unit> list = ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), pid);
                query.where(p1);
                return null;
            }
        });
        if (EmptyUtil.isNotEmpty(list)) {
            for (Unit bean : list) {
                idList.add(bean.getUnitid());
            }
        }
        return idList;
    }

    @Override
    public File exportUnit(Unit unit, String version, String ahuPatchVersion) throws Exception {
        ExportUnit exportUnit = new ExportUnit();
        BeanUtils.copyProperties(exportUnit, unit);
        List<Part> partsOfUnit = sectionDao.findAll(SectionSpecifications.sectionsOfUnit(unit.getUnitid()));
        List<ExportPart> exportParts = new ArrayList<>();
        for (Part aPartOfUnit : partsOfUnit) {
            ExportPart exportPart = new ExportPart();
            BeanUtils.copyProperties(exportPart, aPartOfUnit);
            exportParts.add(exportPart);
        }
        exportUnit.setExportParts(exportParts);

        List<Partition> partitionsOfUnit = partitionDao
                .findAll(PartitionSpecifications.partitionsOfUnit(unit.getUnitid()));
        Partition partitionOfUnit = partitionsOfUnit.isEmpty() ? null : partitionsOfUnit.get(0);

        if (EmptyUtil.isNotEmpty(partitionOfUnit)) {
            ExportPartition exportPartition = new ExportPartition();
            BeanUtils.copyProperties(exportPartition, partitionOfUnit);
            exportUnit.setPartition(exportPartition);
        }
        exportUnit.setVersion(version);
        exportUnit.setPatchVersion(ahuPatchVersion);
        String fileName = MessageFormat.format(SysConstants.NAME_EXPORT_AHU,
                new String(unit.getUnitid().getBytes(Charset.forName(ServiceConstant.SYS_ENCODING_UTF8_UP)),
                        ServiceConstant.SYS_ENCODING_ISO_8859_1));
        return FileUtil.writeFile(SysConstants.DIR_EXPORT + fileName, JSONObject.toJSONString(exportUnit));
    }

    @Override
    public List<Unit> findUngoupedAhuList(String projectId) {
        return ahuDao.findAll(new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), projectId);
                Predicate p = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), ServiceConstant.SYS_BLANK);
                Predicate p0 = cb.isNull(root.get(ServiceConstant.SYS_STRING_GROUPID));
                Predicate p2 = cb.or(p, p0);
                query.where(cb.and(p1, p2));
                query.orderBy(cb.asc(root.get(ServiceConstant.SYS_STRING_UNITNO).as(String.class)));
                return null;
            }
        });
    }

    @Override
    public String switchProduct(Unit unit, UpdateUnitVO unitVo) {
        String oldProduct = unit.getProduct();
        String newProduct = unitVo.getProduct();

        unit.setProduct(newProduct);
        if (StringUtils.isNotEmpty(unit.getSeries())) {
            unit.setSeries(newProduct + AhuUtil.getUnitNo(unit.getSeries()));
        }

        JSONObject ahuJson = (JSONObject) JSON.parse(unit.getMetaJson());
        ahuJson.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_PRODUCT), newProduct);
        ahuJson.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_SERIAL), unit.getSeries());
        AhuProductUtils.resetXTCasingMaterials(ahuJson, oldProduct, newProduct);

        // clear result if unit model is not supported
        if (!isUnitModelAvailable(newProduct, AhuUtil.getUnitNo(unit.getSeries()))) {
            unit.setSeries(SYS_BLANK);
            unit.setRecordStatus(AhuStatusEnum.SELECTING.getId());
            ahuJson.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_WIDTH), SYS_BLANK);
            ahuJson.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_HEIGHT), SYS_BLANK);
        } else {
            AhuSizeDetail ahuSizeDetail = AhuProductUtils.getAhuSizeDetail(newProduct, unit.getSeries());
            ahuJson.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_WIDTH), ahuSizeDetail.getWidth());
            ahuJson.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_HEIGHT), ahuSizeDetail.getHeight());
        }

        List<Part> parts = this.sectionDao.findAll(SectionSpecifications.sectionsOfUnit(unit.getUnitid()));
        // 机组系列修改后，针对选型完成机组自动再选型一遍
        if (AhuStatusEnum.SELECT_COMPLETE.getId().equals(unit.getRecordStatus())) {
            boolean needConfirm = false;
            for (int i = 0; i < parts.size(); i++) {
                Part part = parts.get(i);
                if (AhuProductUtils.needsClearPositivePressureDoor(part, oldProduct, newProduct)) {
                    clearPositivePressureDoor(part);
                    clearSectionCalculationResultAndSave(part);
                    needConfirm = true;
                } else if (AhuProductUtils.needsConfirmIfSwitchProduct(part, newProduct)) {
                    clearSectionCalculationResultAndSave(part);
                    needConfirm = true;
                }
            }

            if (needConfirm) {
                clearUnitCalculationResultAndSave(ahuJson, unit);
            } else {
                unit.setPrice(0d);//潘洁要求修改：已经选型完成的修改系列，需要清空价格防止销售走价差漏洞
                unit.setMetaJson(JSON.toJSONString(ahuJson));
                unit.setUpdateInfo(AHUContext.getUserName());
                this.ahuDao.save(unit);
            }
        } else {
            clearSectionCalculationResultAndSave(parts, oldProduct, newProduct);
            clearUnitCalculationResultAndSave(ahuJson, unit);
        }

        List<Partition> partitions = partitionDao.findAll(PartitionSpecifications.partitionsOfUnit(unit.getUnitid()));
        for (Partition partition : partitions) {
            clearPartitionPanelResultAndSave(partition,unit);
        }
        return unit.getUnitid();
    }
    @Override
    public String switchProductNoSection(Unit unit, UpdateUnitVO unitVo) {
        String newProduct = unitVo.getProduct();
        unit.setProduct(newProduct);
        JSONObject ahuJson = (JSONObject) JSON.parse(unit.getMetaJson());
        ahuJson.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_PRODUCT), newProduct);
        unit.setMetaJson(JSON.toJSONString(ahuJson));
        unit.setUpdateInfo(AHUContext.getUserName());
        this.ahuDao.save(unit);

        return unit.getUnitid();
    }

    private void clearPositivePressureDoor(Part part) {
        JSONObject partJson = (JSONObject) JSON.parse(part.getMetaJson());
        partJson.put(getMetaSectionKey(part.getSectionKey(), KEY_POSITIVE_PRESSURE_DOOR), false);
        part.setMetaJson(JSON.toJSONString(partJson));
    }

    private void clearSectionCalculationResultAndSave(Part part) {
        JSONObject partJson = (JSONObject) JSON.parse(part.getMetaJson());
        partJson.put(SectionMetaUtils.getMetaSectionKey(part.getSectionKey(), MetaKey.KEY_PRICE), SYS_BLANK);
        partJson.put(SectionMetaUtils.getMetaSectionKey(part.getSectionKey(), MetaKey.KEY_WEIGHT), SYS_BLANK);
        partJson.put(SystemCalculateConstants.META_SECTION_COMPLETED, false);
        part.setMetaJson(JSON.toJSONString(partJson));

        part.setRecordStatus(AhuStatusEnum.SELECTING.getId());
        part.setUpdateInfo(AHUContext.getUserName());
        this.sectionDao.save(part);
    }

    private void clearSectionCalculationResultAndSave(List<Part> parts, String oldProduct, String newProduct) {
        parts.forEach(p -> {
            if (AhuProductUtils.needsClearPositivePressureDoor(p, oldProduct,newProduct)) {
                clearPositivePressureDoor(p);
            }
            this.clearSectionCalculationResultAndSave(p);
        });
    }

    private void clearUnitCalculationResultAndSave(JSONObject ahuJson, Unit unit) {
        ahuJson.put(SystemCalculateConstants.META_SECTION_COMPLETED, false);

        unit.setPrice(0.0);
        unit.setWeight(0.0);
        unit.setRecordStatus(AhuStatusEnum.SELECTING.getId());
        unit.setMetaJson(JSON.toJSONString(ahuJson));
        unit.setUpdateInfo(AHUContext.getUserName());
        this.ahuDao.save(unit);
    }

    private void clearPartitionPanelResultAndSave(Partition partition,Unit unit) {
        List<AhuPartition> ahuPartitions = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
        ahuPartitions.forEach(p -> p.setPanels(null));
        ahuPartitions.forEach(p -> p.setCasingWidth(AHU_PRODUCT_CASING_WIDTH.get(unit.getProduct())));
        partition.setPartitionJson(new Gson().toJson(ahuPartitions));
        partition.setUpdateInfo(AHUContext.getUserName());
        this.partitionDao.save(partition);
    }

    @Override
    public boolean isUnitModelAvailable(String series, String model) {
        String unitModel = series + model;
        List<AhuSizeDetail> ahuSizeDetails = AhuMetaUtils.getAhuSizeDetails(series);
        for (AhuSizeDetail ahuSizeDetail : ahuSizeDetails) {
            if (ahuSizeDetail.getAhu().equals(unitModel)) {
                return true;
            }
        }
        return false;
    }
}

package com.carrier.ahu.service.business.damper;

import static com.carrier.ahu.common.enums.DamperMaterialEnum.AL;
import static com.carrier.ahu.common.enums.DamperMaterialEnum.FD;
import static com.carrier.ahu.common.enums.DamperMaterialEnum.GI;
import static com.carrier.ahu.common.enums.DamperTypeEnum.Fresh;
import static com.carrier.ahu.common.enums.DamperTypeEnum.Return;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_MIX;
import static com.carrier.ahu.util.meta.SectionMetaUtils.getSectionMetaValueString;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_OUTLET;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_TYPE;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_NARATIO;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_dAMPER_METERIAL;
import static com.carrier.ahu.vo.SystemCalculateConstants.MIX_DAMPERMETERIAL_AL;
import static com.carrier.ahu.vo.SystemCalculateConstants.MIX_DAMPERMETERIAL_GI;
import static com.carrier.ahu.vo.SystemCalculateConstants.MIX_DAMPEROUTLET_FD;

import java.util.Map;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.DamperMaterialEnum;
import com.carrier.ahu.common.enums.DamperPosEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.vo.DamperPosSizeResultVO;

public class MixDamperBusinessRule extends AbstractDamperBusinessRule {

    public MixDamperBusinessRule(Unit unit, Part section) {
        super(unit, section);
    }

    @Override
    protected DamperMaterialEnum getDamperMaterial(Map<String, Object> sectionMeta) {
        String damperOutlet = getSectionMetaValueString(sectionType(), KEY_DAMPER_OUTLET, sectionMeta);
        if (MIX_DAMPEROUTLET_FD.equals(damperOutlet)) {
            return FD;
        }

        String damperMaterial = getSectionMetaValueString(sectionType(), KEY_dAMPER_METERIAL, sectionMeta);
        if (MIX_DAMPERMETERIAL_AL.equals(damperMaterial)) {
            return AL;
        } else if (MIX_DAMPERMETERIAL_GI.equals(damperMaterial)) {
            return GI;
        }

        throw new IllegalArgumentException("illegal damper material meta value: " + damperMaterial);
    }

    @Override
    protected SectionTypeEnum sectionType() {
        return TYPE_MIX;
    }

    @Override
    protected void calculateFanSpeed(DamperPosEnum damperPos, DamperPosSizeResultVO resultVo) {
        double airVolume = isSupplierAir() ? getSupplierAirVolume() : getReturnAirVolume();
        double damperArea = (resultVo.getSizeX() / 1000) * (resultVo.getSizeY() / 1000);
        double naRatio = SectionMetaUtils.getSectionMetaValueDouble(sectionType(), KEY_NARATIO, sectionMeta);
        String damperType = SectionMetaUtils.getSectionMetaValueString(sectionType(),
                KEY_DAMPER_TYPE.apply(damperPos), sectionMeta);

        if (Fresh.name().equals(damperType)) {
            resultVo.setFanSpeed((airVolume / 3600) * (naRatio / 100) / damperArea);
        } else if (Return.name().equals(damperType)) {
            resultVo.setFanSpeed((airVolume / 3600) * (1 - naRatio / 100) / damperArea);
        } else {
            throw new IllegalArgumentException("unknown damper type: " + damperType);
        }
    }

}

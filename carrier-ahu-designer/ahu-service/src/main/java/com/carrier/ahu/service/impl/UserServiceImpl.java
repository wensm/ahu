package com.carrier.ahu.service.impl;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.common.entity.UserMetaId;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.license.LicenseInfo;
import com.carrier.ahu.service.UserService;
import com.carrier.ahu.service.constant.ServiceConstant;

/**
 * @author Simon
 */
@Service
public class UserServiceImpl extends AbstractService implements UserService {
    @Override
    public String addUser(User user, String userName) {
        user.setCreateInfo(userName);
        userDao.save(user);
        return user.getUserId();
    }

    @Override
    public String updateUser(User user, String userName) {
        user.setUpdateInfo(userName);
        userDao.save(user);
        return user.getUserId();
    }

    @Override
    public void deleteUser(String userId) {
        userDao.delete(userId);
    }

    @Override
    public User findUserById(String userId) {
        User user = userDao.findOne(userId);
        return user;
    }

    @Override
    public boolean checkName(String userName) {
        List<User> list = userDao.findAll(new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Path<String> userNamePath = root.get(ServiceConstant.SYS_MAP_USERNAME);
                query.where(cb.equal(userNamePath, userName));
                return null;
            }
        });
        if (list.isEmpty()) {
            return true;
        }
        return false;
    }

    @Override
    public Iterable<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public void saveUserMeta(UserMeta userMeta) {
        userMetaDao.save(userMeta);
    }

    @Override
    public void saveUserMetas(List<UserMeta> userMetas) {
        if (userMetas != null && !userMetas.isEmpty()) {
            for (UserMeta userMeta : userMetas) {
                userMetaDao.save(userMeta);
            }
        }
    }

    @Override
    public void deleteUserMeta(UserMetaId userMetaId) {
        userMetaDao.delete(userMetaId);
    }

    @Override
    public UserMeta findUserMetaById(UserMetaId userMetaId) {
        return userMetaDao.findOne(userMetaId);
    }

    @Override
    public List<UserMeta> findAllUserMeta() {
        return (List<UserMeta>) userMetaDao.findAll();
    }

    @Override
    public List<UserMeta> findUserMetaByUserAndFactory(String userId, String factoryId) {
        List<UserMeta> userMetas = userMetaDao.findAll(new Specification<UserMeta>() {
            @Override
            public Predicate toPredicate(Root<UserMeta> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Path<String> userIdPath = root.get(ServiceConstant.SYS_STRING_USERMETAID).get(ServiceConstant.SYS_STRING_USERID);
                Path<String> factoryIdPath = root.get(ServiceConstant.SYS_STRING_FACTORYID);
                return cb.and(cb.equal(userIdPath, userId), cb.equal(factoryIdPath, factoryId));
            }
        });
        return userMetas;
    }

    @Override
    public User getLocalUser(LicenseInfo licenseInfo) {
        User user = this.userDao.findOne(licenseInfo.getUserName());
        if (user == null) {
            user = getInitUser(licenseInfo);
            userDao.save(user);
        }
        return user;
    }

    @Override
    public User installUser(LicenseInfo licenseInfo) {
        User user = this.userDao.findOne(licenseInfo.getUserName());
        if (user == null) {
            user = getInitUser(licenseInfo);
        } else {
            user.setUserFactory(licenseInfo.getFactory());
            user.setUserRole(licenseInfo.getRole());
            user.setExpireDate(licenseInfo.getDate());
            this.userDao.delete(user);
        }
        userDao.save(user);
        return user;
    }

    private User getInitUser(LicenseInfo licenseInfo) {
        User user = new User();
        if (Locale.getDefault().getLanguage() == Locale.CHINESE.getLanguage()) {
            user.setPreferredLocale(LanguageEnum.Chinese.getId());
        } else {
            user.setPreferredLocale(LanguageEnum.English.getId());
        }
        user.setUnitPreferCode(UnitSystemEnum.M.getId());
        user.setUserId(licenseInfo.getUserName()); // need upgrade when required
        user.setUserName(licenseInfo.getUserName());
        user.setUserFactory(licenseInfo.getFactory());
        user.setUserRole(licenseInfo.getRole());
        user.setExpireDate(licenseInfo.getDate());
        user.setCreateInfo(ServiceConstant.SYS_STRING_DEFAULT);
        return user;
    }

}

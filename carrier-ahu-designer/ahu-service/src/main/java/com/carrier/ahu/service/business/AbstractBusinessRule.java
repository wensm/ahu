package com.carrier.ahu.service.business;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Map;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.util.meta.SectionMetaUtils;

public abstract class AbstractBusinessRule implements BusinessRule {

    protected Unit unit;
    protected Part section;

    protected Map<String, Object> unitMeta;
    protected Map<String, Object> sectionMeta;

    public AbstractBusinessRule(Unit unit, Part section) {
        this.unit = checkNotNull(unit);
        this.section = checkNotNull(section);
        this.unitMeta = SectionMetaUtils.getUnitMeta(unit);
        this.sectionMeta = SectionMetaUtils.getSectionMeta(section);
    }

}

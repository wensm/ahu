package com.carrier.ahu.service.impl;

import com.carrier.ahu.common.entity.Factory;
import com.carrier.ahu.service.FactoryService;
import com.carrier.ahu.service.constant.ServiceConstant;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.util.List;

/**
 * @author Simon
 */
@Service
public class FactoryServiceImpl extends AbstractService implements FactoryService {
	@Override
	public String addFactory(Factory factory, String factoryName) {
		factory.setCreateInfo(factoryName);
		factoryDao.save(factory);
		return factory.getId();
	}

	@Override
	public String updateFactory(Factory factory, String factoryName) {
		factory.setUpdateInfo(factoryName);
		factoryDao.save(factory);
		return factory.getId();
	}

	@Override
	public void deleteFactory(String factoryId) {
		factoryDao.delete(factoryId);
	}

	@Override
	public Factory findFactoryById(String factoryId) {
		Factory factory = factoryDao.findOne(factoryId);
		return factory;
	}

	@Override
	public boolean checkName(String factoryName) {
		List<Factory> list = factoryDao.findAll(new Specification<Factory>() {
			@Override
			public Predicate toPredicate(Root<Factory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> factoryNamePath = root.get(ServiceConstant.SYS_STRING_FACTORYNAME);
				query.where(cb.equal(factoryNamePath, factoryName));
				return null;
			}
		});
		if (list.isEmpty()) {
			return true;
		}
		return false;
	}

	@Override
	public Iterable<Factory> findAll() {
		return factoryDao.findAll();
	}

}

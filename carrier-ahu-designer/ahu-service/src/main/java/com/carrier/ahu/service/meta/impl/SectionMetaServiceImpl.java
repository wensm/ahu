package com.carrier.ahu.service.meta.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.po.meta.SectionMeta;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.meta.SectionMetaService;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wen zhengtao on 2017/3/18.
 */
@Service
public class SectionMetaServiceImpl implements SectionMetaService {
	@Override
	public List<SectionMeta> getSectionMetas(File[] files) {
		List<SectionMeta> sectionMetas = new ArrayList<SectionMeta>();
		for (File file : files) {
			try {
				BufferedReader in = new BufferedReader(
						new InputStreamReader(new FileInputStream(file), Charset.forName(ServiceConstant.SYS_ENCODING_UTF8_UP)));
				StringBuffer buffer = new StringBuffer();
				String line = ServiceConstant.SYS_BLANK;
				while ((line = in.readLine()) != null) {
					buffer.append(line);
				}
				JSONObject jsonObject = JSON.parseObject(buffer.toString());
				SectionMeta sectionMeta = JSONObject.toJavaObject(jsonObject, SectionMeta.class);
				sectionMetas.add(sectionMeta);
				in.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sectionMetas;
	}
}

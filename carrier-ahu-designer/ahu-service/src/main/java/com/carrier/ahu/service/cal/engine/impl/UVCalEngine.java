package com.carrier.ahu.service.cal.engine.impl;

import static com.carrier.ahu.common.intl.I18NConstants.FILTER_ARRANGEMENT_CALCULATION_FAILED;
import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.calculator.PerformanceCalculator;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.calculation.FilterArrangementCalcException;
import com.carrier.ahu.length.param.AddtionalParam;
import com.carrier.ahu.length.param.ElectricHeatingParam;
import com.carrier.ahu.length.param.FilterPanelBean;
import com.carrier.ahu.length.param.FilterPanelParam;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.calc.SUVCLight;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.InvokeTool;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * 过滤器布置计算 单层过滤段 综合过滤段 静电过滤段 高效过滤段
 */
@Component
public class UVCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(UVCalEngine.class.getName());

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        try {
            logger.info("UV Engine calculator line begin");
            
            AddtionalParam addtionalParam = new InvokeTool<AddtionalParam>().genInParamFromAhuParam(ahu, section, null, new AddtionalParam());
            
            if(addtionalParam.isUvLamp()) {
            	SUVCLight suvclight = AhuMetadata.findOne(SUVCLight.class, addtionalParam.getSerial());
                String uv = "";
                if (null != suvclight) {
                    uv = suvclight.getType() + "*" + suvclight.getQuan();
                    if(section.getKey().equals(SectionTypeEnum.TYPE_MIX.getId())){
                        section.getParams().put(ServiceConstant.METASEXON_MIX_UVLAMPSERIAL, String.valueOf(uv));
                    }else if(section.getKey().equals(SectionTypeEnum.TYPE_ACCESS.getId())){
                        section.getParams().put(ServiceConstant.METASEXON_ACCESS_UVMODEL, String.valueOf(uv));
                    }
                    
                }
            }else{
                if(section.getKey().equals(SectionTypeEnum.TYPE_MIX.getId())){
                    section.getParams().put(ServiceConstant.METASEXON_MIX_UVLAMPSERIAL, "");
                }
            }
            context.setSuccess(true);
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(true);
            logger.info("UV Engine calculator line end");
            return subc;
        } catch (Exception e) {
            e.printStackTrace();
            String message = getIntlString(FILTER_ARRANGEMENT_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("UV engine debug massage :" + message, e.getMessage());
            logger.error("UV engine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new FilterArrangementCalcException(FILTER_ARRANGEMENT_CALCULATION_FAILED);
        }
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{
                SectionTypeEnum.TYPE_ACCESS,SectionTypeEnum.TYPE_MIX
        };
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_UV;
    }

}

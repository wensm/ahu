package com.carrier.ahu.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.carrier.ahu.common.entity.BatchCfgProgress;
import com.carrier.ahu.service.BatchCfgProgressService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.util.EmptyUtil;

import javax.persistence.criteria.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Simon
 * @since 2017-12-09
 */
@Service
public class BatchCfgProgressServiceImpl extends AbstractService implements BatchCfgProgressService {
	@Override
	public String addProgress(BatchCfgProgress progress, String userName) {
		progress.setCreateInfo(userName);
		String projectId = progress.getProjectId();
		String groupId = progress.getGroupId();
		List<BatchCfgProgress> list = progressDao.findAll(new Specification<BatchCfgProgress>() {
			@Override
			public Predicate toPredicate(Root<BatchCfgProgress> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJECTID), projectId);
				Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), groupId);
				query.where(cb.and(p1, p2));
				return null;
			}
		});
		if (list.isEmpty()) {
			progressDao.save(progress);
		} else {
			BatchCfgProgress progress0 = list.get(0);
			progress.setProgressId(progress0.getProgressId());
			progressDao.save(progress);

		}
		return progress.getProgressId();
	}

	@Override
	public String updateProgress(BatchCfgProgress progress, String userName) {
		progress.setUpdateInfo(userName);
		progressDao.save(progress);
		return progress.getProgressId();
	}

	@Override
	public void deleteProgress(String progressId) {
		progressDao.delete(progressId);
	}

	@Override
	public BatchCfgProgress findProgressById(String progressId) {
		BatchCfgProgress progress = progressDao.findOne(progressId);
		return progress;
	}

	@Override
	public List<BatchCfgProgress> findProgressList(String projectId) {
		List<BatchCfgProgress> list = progressDao.findAll(new Specification<BatchCfgProgress>() {
			@Override
			public Predicate toPredicate(Root<BatchCfgProgress> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> projectIdPath = root.get(ServiceConstant.SYS_STRING_PROJECTID);// 项目ID
				query.where(cb.equal(projectIdPath, projectId));
				return null;
			}
		});
		return list;
	}

	@Override
	public Iterable<BatchCfgProgress> findAll() {
		return progressDao.findAll();
	}

	@Override
	public List<BatchCfgProgress> findProgressListByRecordStatus(String projectId, String recordStatus) {
		List<BatchCfgProgress> list = progressDao.findAll(new Specification<BatchCfgProgress>() {
			@Override
			public Predicate toPredicate(Root<BatchCfgProgress> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJECTID), projectId);
				Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_RECORDSTATUS), recordStatus);
				if (StringUtils.isNotBlank(recordStatus)) {
					query.where(cb.and(p1, p2));
				} else {
					query.where(p1);
				}
				return null;
			}
		});
		return list;
	}

	@Override
	public List<BatchCfgProgress> findProgressListByParams(String projectId, String groupId) {
		List<BatchCfgProgress> list = progressDao.findAll(new Specification<BatchCfgProgress>() {
			@Override
			public Predicate toPredicate(Root<BatchCfgProgress> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJECTID), projectId);
				Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_GROUPID), groupId);
				if (StringUtils.isNotBlank(projectId)) {
					return null;
				} else {
					if (StringUtils.isNotBlank(groupId)) {
						query.where(cb.and(p1, p2));
					} else {
						query.where(p1);
					}
				}
				return null;
			}
		});
		return list;
	}

	@Override
	public List<String> getAllIds(String pid) {
		List<String> idList = new ArrayList<>();
		if (EmptyUtil.isEmpty(pid)) {
			return idList;
		}
		List<BatchCfgProgress> list = progressDao.findAll(new Specification<BatchCfgProgress>() {
			@Override
			public Predicate toPredicate(Root<BatchCfgProgress> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_STRING_PROJECTID), pid);
				query.where(p1);
				return null;
			}
		});
		if (EmptyUtil.isNotEmpty(list)) {
			for (BatchCfgProgress bean : list) {
				idList.add(bean.getProgressId());
			}
		}
		return idList;
	}
}

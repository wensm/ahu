package com.carrier.ahu.service.cal.engine.impl;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.RoleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.calculation.PriceCalcException;
import com.carrier.ahu.common.exception.calculation.SectionLengthCalcException;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.param.NSPriceParam;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.service.PriceService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.InvokeTool;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;

import static com.carrier.ahu.common.intl.I18NConstants.PRICE_NS_CALCULATION_FAILED;
import static com.carrier.ahu.common.intl.I18NConstants.PRICE_NS_CALCULATION_LIMIT;
import static com.carrier.ahu.constant.CommonConstant.SYS_ASSERT_TRUE;
import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;

/**
 * 非标价格计算引擎
 */
@Component
public class NSPriceCalEngine extends AbstractCalEngine {
	private static Logger logger = LoggerFactory.getLogger(NSPriceCalEngine.class.getName());

	@Override
	public int getOrder() {
		return 9;
	}
	@Autowired
	private PriceService priceService;

	@Override
	public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
		logger.info("NSPriceCalEngine calculator line begin");
		NSPriceParam nsPriceParam = new InvokeTool<NSPriceParam>().genInParamFromAhuParam(ahu, section, null, new NSPriceParam());
		SectionTypeEnum sectionTypeEnum = SectionTypeEnum.getSectionTypeFromId(section.getKey());
		nsPriceParam.setSectionType(sectionTypeEnum.getCode());
		nsPriceParam.setSectionId(sectionTypeEnum.getId());
		try {
			logger.info("NSPriceCalEngine calculator param :" + JSONArray.toJSONString(nsPriceParam));

			//开启非标功能，销售不能填写非标价格
			if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsPriceParam.getEnable().toLowerCase()) && RoleEnum.Sales.name().equalsIgnoreCase(AHUContext.getUserRole())) {
				String nsPriceKey = SectionMetaUtils.getNSSectionKey(section.getKey(), SectionMetaUtils.MetaKey.KEY_PRICE);
				double partNsPrice = NumberUtils.toDouble(String.valueOf(section.getParams().get(nsPriceKey)));
				if(partNsPrice!=0){
					section.getParams().put(nsPriceKey, "");
				}
			}

			//开启非标功能，并且是非销售人员可以计算非标价格。
			if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsPriceParam.getEnable().toLowerCase())) {
				// 获取非标价格
				Map<String, String> nsPriceMap = priceService.getNSPrice(nsPriceParam);

				Iterator<Map.Entry<String, String>> nsPriceIterator = nsPriceMap.entrySet().iterator();
				// 覆盖价格结果到前台
				while (nsPriceIterator.hasNext()) {
					Map.Entry<String, String> entry = nsPriceIterator.next();
					String mapKey = String.valueOf(entry.getKey());
					String mapValue = String.valueOf(entry.getValue());

					section.getParams().put(SectionMetaUtils.getNSSectionKey(section.getKey(), mapKey), convert2Int(mapValue));
					logger.info("NSPriceCalEngine calculator  :" + SectionMetaUtils.getNSSectionKey(section.getKey(), mapKey) + ServiceConstant.SYS_PUNCTUATION_COLON + mapValue);
				}
			}
			context.setSuccess(true);
			ICalContext subc = super.cal(ahu, section, context);
			subc.setSuccess(true);

			logger.info("NSPriceCalEngine calculator line end");
			return subc;
		} catch (Exception e) {
			String message = getIntlString(PRICE_NS_CALCULATION_FAILED, AHUContext.getLanguage());
			String tempMsg = EmptyUtil.isNotEmpty(e.getMessage())?getIntlString(e.getMessage(), AHUContext.getLanguage()):null;
			if(EmptyUtil.isNotEmpty(tempMsg)){
				message = tempMsg;
			}
			logger.debug("NSPriceCalEngine debug massage :" + message, e.getMessage());
			logger.error("NSPriceCalEngine error massage :" + e.getMessage());
			ICalContext subc = super.cal(ahu, section, context);
			subc.setSuccess(false);
			subc.error(message);
			context.setSuccess(false);
			section.getParams().put(META_SECTION_COMPLETED, false);
			if(EmptyUtil.isNotEmpty(tempMsg)){
				throw new SectionLengthCalcException(e.getMessage());
			}else{
				throw new SectionLengthCalcException(PRICE_NS_CALCULATION_FAILED);
			}
		}
	}

	@Override
	public String[] getEnabledSectionIds() {
		SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{
				SectionTypeEnum.TYPE_WETFILMHUMIDIFIER,
				SectionTypeEnum.TYPE_SPRAYHUMIDIFIER,
				SectionTypeEnum.TYPE_STEAMHUMIDIFIER,
				//SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER, 暂不支持
				SectionTypeEnum.TYPE_FAN
		};
		String[] result = new String[sectionEs.length];
		for (int i = 0; i < sectionEs.length; i++) {
			result[i] = sectionEs[i].getId();
		}
		return result;
	}

	@Override
	public String getKey() {
		return ICalEngine.ID_NS_PRICE;
	}

}

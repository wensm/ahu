package com.carrier.ahu.service.cal.engine.impl;


import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.NumberUtil;
import org.springframework.stereotype.Component;

import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.cal.impl.AhuCalBus;
import com.carrier.ahu.service.cal.impl.DefaultCalContext;

@Component
public abstract class AbstractCalEngine implements ICalEngine {
	
	public AbstractCalEngine() {
		AhuCalBus.getInstance().registerCalEngine(this);
	}

	@Override
	public ICalContext cal(AhuParam ahuParam, PartParam partParam, ICalContext context) throws Exception {
		DefaultCalContext subContext = new DefaultCalContext();
		subContext.setText(ICalContext.NAME_ENGINE);
		subContext.setName(getKey());
		context.enter(subContext);
		return subContext;
	}

	@Override
	public String[] getEnabledSectionIds() {
		return new String[0];
	}

    @Override
    public String[] getDisabledUnitSeries() {
        return new String[0];
    }

	@Override
	public int getOrder() {
		return 99;
	}
	
	@Override
	public int getCalLevel() {
		return CalOperationEnum.COMFIRM.getId();
	}
	
    protected String getIntlString(String key, LanguageEnum language, Object... objects) {
        return I18NBundle.getString(key, language, objects);
    }
	/**
	 * 转换计算后的阻力为int
	 * @param resistance
	 */
	protected String convert2Int(String resistance) {
		if(EmptyUtil.isNotEmpty(resistance)){
			int res = NumberUtil.convertStringToDoubleInt(resistance);
			return ""+res;
		}else{
			return resistance;
		}
	}
}

package com.carrier.ahu.service.service.impl;

import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.calculation.PriceCalcException;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.metadata.entity.price.*;
import com.carrier.ahu.metadata.entity.report.SKAbsorber;
import com.carrier.ahu.param.NSPriceParam;
import com.carrier.ahu.price.NSPriceUtil;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.service.PriceService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static com.carrier.ahu.common.intl.I18NConstants.PRICE_NS_ACCESS_4_SECTIONL_CALCULATION_FAILED;
import static com.carrier.ahu.common.intl.I18NConstants.PRICE_NS_ACCESS_SECTIONL_CALCULATION_FAILED;
import static com.carrier.ahu.constant.CommonConstant.*;
import static com.carrier.ahu.util.partition.AhuPartitionGenerator.HB_39;

@Slf4j
@Service
public class PriceServiceImpl implements PriceService {

    private static Logger logger = LoggerFactory.getLogger(PriceServiceImpl.class.getName());

    @Autowired
    private AhuService ahuService;

    @Autowired
    private PartitionService partitionService;

    @Override
    public double getPrice(LengthParam lengthParam) {
        return 0;
    }

    /**
     * 获取非标价格
     *
     * @param nsPriceParam
     * @return
     */
    @Override
    public Map<String, String> getNSPrice(NSPriceParam nsPriceParam) {
        Map<String, String> nsPriceMap = new HashMap<String, String>();
        String ahuId = nsPriceParam.getUnitid();
        String sectionType = nsPriceParam.getSectionType();
        String serial = AhuUtil.getUnitSeries(nsPriceParam.getSerial());
        NSPriceUtil nsPriceUtil = new NSPriceUtil();
        if (SectionTypeEnum.TYPE_ACCESS.getCode().equals(sectionType)) {
            if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsPriceParam.getEnable())) {
                nsPriceMap.put(ServiceConstant.SYS_STRING_NS_ACCESS_SHUIPAN_PRICE, String.valueOf(calAccessNsPrice(nsPriceParam)));
            }
        } else if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode().equals(sectionType)) {
            if (ServiceConstant.SYS_ASSERT_TRUE
                    .equals(nsPriceParam.getNs_section_wetfilmHumidifier_nsChangeSupplier())) {
                int thinkness = new Double(nsPriceParam.getThickness()).intValue();
                NonstdWet nsWet = nsPriceUtil.getWetNsPrice(SystemCountUtil.getUnit(nsPriceParam.getSerial()),
                        ServiceConstant.SYS_BLANK + thinkness);
                if (null != nsWet) {
                    double cj = Double.parseDouble(nsWet.getTp()) - Double.parseDouble(nsWet.getOldPrice());
                    nsPriceMap.put(ServiceConstant.SYS_STRING_NSSUPPLIERPRICE, String.valueOf(cj));
                }
            }
        } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getCode().equals(sectionType)) {
            if (ServiceConstant.SYS_ASSERT_TRUE
                    .equals(nsPriceParam.getNs_section_sprayHumidifier_nsChangeSupplier())) {
                List<NonstdHigh> nshighs = nsPriceUtil.getAllHighNsPrice();
                Collections.sort(nshighs, new Comparator<NonstdHigh>() {
                    public int compare(NonstdHigh o1, NonstdHigh o2) {
                        return new Integer(o1.getSprayV()).compareTo(new Integer(o2.getSprayV()));
                    }
                });
                for (NonstdHigh nshigh : nshighs) {
                    if (nshigh.getSprayV() >= nsPriceParam.getHq()) {
                        double cj = Double.parseDouble(nshigh.getTp()) - Double.parseDouble(nshigh.getOldPrice());
                        nsPriceMap.put(ServiceConstant.SYS_STRING_NSSUPPLIERPRICE, String.valueOf(cj));
                        break;
                    }
                }
            }
        } else if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getCode().equals(sectionType)) {

        } else if (SectionTypeEnum.TYPE_FAN.getCode().equals(sectionType)) {
            double nsFanPrice = 0;
            if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsPriceParam.getNs_section_fan_nsChangeFan())) {
                String oldUnit = nsPriceParam.getFanModel()
                        .replace(ServiceConstant.JSON_FAN_FANMODEL_FC, ServiceConstant.JSON_FAN_FANMODEL_KTQ)
                        .replace(ServiceConstant.JSON_FAN_FANMODEL_BC, ServiceConstant.JSON_FAN_FANMODEL_KTH);// 前弯fc后弯bc替换为ktq,kth;
                String supplier = nsPriceParam.getNs_section_fan_nsFanSupplier();
                String nsFanModel = nsPriceParam.getNs_section_fan_nsFanModel();
                NonstdFan nonstdFan = AhuMetadata.findOne(NonstdFan.class, oldUnit, supplier, nsFanModel);
                LpRate lpRate = AhuMetadata.findOne(LpRate.class, serial);
                if (EmptyUtil.isNotEmpty(nonstdFan)) {
                    if (EmptyUtil.isNotEmpty(lpRate)) {
                        nsFanPrice = this.calculateNsPrice(nonstdFan, lpRate);
                    } else {
                        log.error("LPRate for - [{}] has not found.", serial);
                        throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                    }
                } else {
                    log.error("Non-std fan price for - [{}, {}, {}] has not found.", oldUnit, supplier, nsFanModel);
                    throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                }
            }
            nsPriceMap.put(ServiceConstant.SYS_STRING_NS_FAN_PRICE, String.valueOf(nsFanPrice));

            double nsMotorPrice = 0;
            if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsPriceParam.getNs_section_fan_nsChangeMotor())) {
                String gl = nsPriceParam.getNs_section_fan_nsMotorPower();
                String js = nsPriceParam.getNs_section_fan_nsMmotorPole();
                String type = this.getMotorTypeCode(nsPriceParam.getType());
                String supplier = this.getFanSupplierCode(nsPriceParam.getSupplier());
                NonstdMotor nonstdMotor = null;
                LpRate lpRate = AhuMetadata.findOne(LpRate.class, serial);
                if (ServiceConstant.SYS_ALPHABET_C.equals(supplier)
                        && ServiceConstant.SYS_ALPHABET_D.equals(type)) {
                    nonstdMotor = AhuMetadata.findOne(NonstdMotor.class, gl, js, "IP55", "F",
                            ServiceConstant.SYS_ALPHABET_C, ServiceConstant.SYS_ALPHABET_B);
                } else {
                    nonstdMotor = AhuMetadata.findOne(NonstdMotor.class, gl, js, "IP55", "F", supplier, type);
                }
                if (EmptyUtil.isNotEmpty(nonstdMotor)) {
                    if (EmptyUtil.isNotEmpty(lpRate)) {
                        nsMotorPrice = this.calculateNsPrice(nonstdMotor, lpRate);
                    } else {
                        log.error("LPRate for - [{}] has not found.", serial);
                        throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                    }
                } else {
                    log.error("Non-std Motor price for - [{}, {}, {}, {}] has not found.", gl, js, type, supplier);
                    throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                }
            }
            nsPriceMap.put(ServiceConstant.SYS_STRING_NS_MOTOR_PRICE, String.valueOf(nsMotorPrice));

            // ns spring price calculation
            double nsSpringtransformPrice = 0;
            String nsSpringtransform = nsPriceParam.getNs_section_fan_nsSpringtransform();
            if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsSpringtransform)) {
                String absorberType = getAbsorberType(nsPriceParam.getFanModel());
                String motorBaseNo = nsPriceParam.getMotorBaseNo();

                String unit = SystemCountUtil.getUnit(String.valueOf(nsPriceParam.getSerial()));
                if (SystemCountUtil.ltBigUnit(BaseDataUtil.stringConversionInteger(unit))) {
                    List<SKFanMotor> skFanMotorList = AhuMetadata.findAll(SKFanMotor.class);
                    if (!EmptyUtil.isEmpty(skFanMotorList)) {
                        for (SKFanMotor skFanMotor : skFanMotorList) {
                            if (skFanMotor.getMachineSiteNo().equals(motorBaseNo)) {
                                motorBaseNo = skFanMotor.getMachineSiteNo1();
                                break;
                            }
                        }
                    }
                }

//                    SKAbsorber skAbsorber = AhuMetadata.findOne(SKAbsorber.class, absorberType, motorBaseNo);
                List<SKAbsorber> absorberList = AhuMetadata.findAll(SKAbsorber.class);
                SKAbsorber skAbsorber = null;
                for (SKAbsorber skAbsorberTemp : absorberList) {
                    if (absorberType.equals(skAbsorberTemp.getFan()) && skAbsorberTemp.getMachineSiteNo().contains(motorBaseNo)) {
                        skAbsorber = skAbsorberTemp;
                        break;
                    }
                }
                if(null==skAbsorber){
                    skAbsorber = new SKAbsorber();
                    skAbsorber.setA("HD60");
                    skAbsorber.setB("HD60");
                    skAbsorber.setC("HD60");
                    skAbsorber.setD("HD60");
                    skAbsorber.setE("HD60");
                    skAbsorber.setF("HD60");
                }
                if (EmptyUtil.isNotEmpty(skAbsorber)) {
                    List<String> absorbers = Arrays.asList(skAbsorber.getA(), skAbsorber.getB(), skAbsorber.getC(),
                            skAbsorber.getD(), skAbsorber.getE(), skAbsorber.getF());
                    double jdPrice = 0;
                    double hdPrice = 0;
                    for (String absorber : absorbers) {
                        if (EmptyUtil.isNotEmpty(absorber)) {
                            CsPrice csPrice = AhuMetadata.findOne(CsPrice.class, absorber);
                            if (EmptyUtil.isNotEmpty(csPrice)) {
                                jdPrice += csPrice.getCost();
                            } else {
                                log.error("Absorber price for code [{}] has not found", absorber);
                                throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                            }
                            if (absorber.contains(ServiceConstant.SYS_STRING_SPRING_JD)) {
                                String hdAbsorber = absorber.replace(ServiceConstant.SYS_STRING_SPRING_JD,
                                        ServiceConstant.SYS_STRING_SPRING_HD);
                                hdAbsorber = reBuildStrByA(hdAbsorber);

                                csPrice = AhuMetadata.findOne(CsPrice.class, hdAbsorber);
                                if (EmptyUtil.isNotEmpty(csPrice)) {
                                    hdPrice += csPrice.getCost();
                                } else {
                                    log.error("Absorber price for code [{}] has not found", hdAbsorber);
                                    throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                                }
                            }
                        }
                    }
                    LpRate lpRate = AhuMetadata.findOne(LpRate.class, serial);
                    if (EmptyUtil.isNotEmpty(lpRate)) {
                        nsSpringtransformPrice = (hdPrice - jdPrice) / lpRate.getMargin() / lpRate.getRate();
                    } else {
                        log.error("LPRate for - [{}] has not found.", serial);
                        throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                    }
                } else {
                    log.error("S_K_Absorber for - [{}, {}] has not found.", absorberType, motorBaseNo);
                    throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                }
            }
            nsPriceMap.put(ServiceConstant.SYS_STRING_NS_SPRING_TRANSFORM_PRICE,
                    String.valueOf(nsSpringtransformPrice));
        } else if (SectionTypeEnum.TYPE_SINGLE.getCode().equals(sectionType)
                || SectionTypeEnum.TYPE_HEPAFILTER.getCode().equals(sectionType)
                || SectionTypeEnum.TYPE_COMPOSITE.getCode().equals(sectionType)) {
            String nsPressureGuage = ServiceConstant.SYS_BLANK;
            String nsPressureGuageModel = ServiceConstant.SYS_BLANK;
            int nsPressureGuageCount = 1;
            if (SectionTypeEnum.TYPE_SINGLE.getCode().equals(sectionType)) {
                nsPressureGuage = nsPriceParam.getNs_section_filter_nsPressureGuage();
                nsPressureGuageModel = nsPriceParam.getNs_section_filter_nsPressureGuageModel();
            } else if (SectionTypeEnum.TYPE_HEPAFILTER.getCode().equals(sectionType)) {
                nsPressureGuage = nsPriceParam.getNs_section_HEPAFilter_nsPressureGuage();
                nsPressureGuageModel = nsPriceParam.getNs_section_HEPAFilter_nsPressureGuageModel();
            } else if (SectionTypeEnum.TYPE_COMPOSITE.getCode().equals(sectionType)) {
                nsPressureGuage = nsPriceParam.getNs_section_combinedFilter_nsPressureGuage();
                nsPressureGuageModel = nsPriceParam.getNs_section_combinedFilter_nsPressureGuageModel();
                nsPressureGuageCount = nsPriceParam.getNs_section_combinedFilter_nsPressureGuageCount();
            }
            double nsPressureGuagePrice = 0;
            if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsPressureGuage)) {
                NonstdPressure nonstdPressure = AhuMetadata.findOne(NonstdPressure.class, nsPressureGuageModel);
                LpRate lpRate = AhuMetadata.findOne(LpRate.class, serial);
                if (EmptyUtil.isNotEmpty(nonstdPressure)) {
                    if (EmptyUtil.isNotEmpty(lpRate)) {
                        nsPressureGuagePrice = this.calculateNsPrice(nonstdPressure, lpRate);
                        //综合过段非标价格，如果有两个压差计价格 * 2
                        if (SectionTypeEnum.TYPE_COMPOSITE.getCode().equals(sectionType) && nsPressureGuageCount == 2) {
                            nsPressureGuagePrice += nsPressureGuagePrice;
                        }
                    } else {
                        log.error("LPRate for - [{}] has not found.", serial);
                        throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                    }
                } else {
                    log.error("Non-std pressure price for - [{}] has not found.", nsPressureGuageModel);
                    throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                }
            }
            nsPriceMap.put(ServiceConstant.SYS_STRING_NS_PRESSURE_GUAGE_PRICE,
                    String.valueOf(nsPressureGuagePrice));
        } else if (EmptyUtil.isNotEmpty(ahuId)) { // calculate ahu ns price
            // calculate AHU channel steel price
            String nsChannelSteelBase = nsPriceParam.getNs_ahu_nsChannelSteelBase();
            String nsChannelSteelBaseModel = nsPriceParam.getNs_ahu_nsChannelSteelBaseModel();
            double nsChannelSteelBasePrice = 0;
            if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsChannelSteelBase)) {
                Unit unit = ahuService.findAhuById(ahuId);
                Partition partition = partitionService.findPartitionByAHUId(ahuId);
                List<AhuPartition> ahuPartitions = AhuPartitionUtils.parseAhuPartition(unit, partition);
                for (AhuPartition ahuPartition : ahuPartitions) {
                    String baseCode = AhuPartitionGenerator.generateBaseForPrice(ahuPartition);
                    //非标底座水盘个数大于2的处理为2
                    baseCode = baseCode.replace(HB_39+"3",HB_39+"2").replace(HB_39+"4",HB_39+"2");



                    int ahuPartitionSectionL = ahuPartition.getLength();
                    if (ahuPartitionSectionL > 100) {
                        ahuPartitionSectionL = ahuPartitionSectionL / 100;
                    }

                    if (EmptyUtil.isNotEmpty(baseCode)) {
                        CsPrice csPrice = AhuMetadata.findOne(CsPrice.class, baseCode);

                        //30模以上按照30模价格*系数所得
                        //31模 ： 31/30*(30模价格)
                        //32模 ： 32/30*(30模价格)
                        if(EmptyUtil.isEmpty(csPrice) && ahuPartitionSectionL>30){
                            csPrice = AhuMetadata.findOne(CsPrice.class, baseCode.substring(0,baseCode.length()-3)+"30R");
                            if(EmptyUtil.isNotEmpty(csPrice)){
                                csPrice.setCost(Double.parseDouble(""+ahuPartitionSectionL)/30*csPrice.getTp());
                            }
                        }

                        CsPriceRate csPriceRate = AhuMetadata.findOne(CsPriceRate.class, nsChannelSteelBaseModel
                                .replaceAll(ServiceConstant.SYS_PUNCTUATION_POUND, ServiceConstant.SYS_BLANK));
                        LpRate lpRate = AhuMetadata.findOne(LpRate.class, serial);
                        if (EmptyUtil.isNotEmpty(csPrice)) {
                            if (EmptyUtil.isNotEmpty(csPriceRate) && EmptyUtil.isNotEmpty(lpRate)) {
                                //槽钢从底座系数
                                nsChannelSteelBasePrice += this.calculateNsCGPrice(csPrice, csPriceRate, lpRate);
                            } else {
                                log.error("Channel steel price rate for model [{}] has not found",
                                        nsChannelSteelBaseModel);
                                throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                            }
                        } else {
                            log.error("Channel steel price rate for model [{}] has not found",
                                    nsChannelSteelBaseModel);
                        }
                    } else {
                        log.error("Channel steel base code has not found");
                        throw new PriceCalcException("Section calNsPrice calculation error " + sectionType);
                    }
                }
            }
            nsPriceMap.put(ServiceConstant.SYS_STRING_NS_CHANNEL_STEEL_BASE_PRICE,
                    String.valueOf(nsChannelSteelBasePrice));

            double nsDeformationPrice = 0;
            if(EmptyUtil.isNotEmpty(nsPriceParam.getDeformationSerial()) && nsPriceParam.getDeformationSerial().length() == 4){
                if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsPriceParam.getNs_ahu_deformation())) {
                    int heightOfAhu = AhuUtil.getHeightOfAHU(nsPriceParam.getSerial());
                    int widthOfAhu = AhuUtil.getWidthOfAHU(nsPriceParam.getSerial());
                    nsDeformationPrice = nsPriceParam.getBoxPrice()
                            * (((nsPriceParam.getNsheight() * nsPriceParam.getNswidth()) / (heightOfAhu * widthOfAhu))
                            * 1.02 - 1);
                    nsPriceMap.put(ServiceConstant.SYS_STRING_DEFORMATION_PRICE, String.valueOf(nsDeformationPrice));
                }
            }
            nsPriceMap.put(ServiceConstant.SYS_STRING_DEFORMATION_PRICE, String.valueOf(nsDeformationPrice));
        }
        return nsPriceMap;
    }

    private String reBuildStrByA(String hdAbsorber) {
        if (hdAbsorber.endsWith("A")) {
            hdAbsorber = hdAbsorber.substring(0, hdAbsorber.length() - 1);
        }
        return hdAbsorber;
    }

    private String getMotorTypeCode(String type) {
        if (ServiceConstant.SYS_STRING_NUMBER_1.equals(type)) { // 二级能效电机, 高效电机
            return ServiceConstant.SYS_ALPHABET_C;
        } else if (ServiceConstant.SYS_STRING_NUMBER_2.equals(type)) { // 三级能效电机, 普通电机
            return ServiceConstant.SYS_ALPHABET_D;
        } else if (ServiceConstant.SYS_STRING_NUMBER_3.equals(type)) { // 防爆电机
            return ServiceConstant.SYS_ALPHABET_B;
        } else if (ServiceConstant.SYS_STRING_NUMBER_4.equals(type)) { // 双速电机
            return ServiceConstant.SYS_ALPHABET_A;
        } else if (ServiceConstant.SYS_STRING_NUMBER_5.equals(type)
                || ServiceConstant.SYS_STRING_NUMBER_6.equals(type)) { // 变频电机
            return ServiceConstant.SYS_ALPHABET_C;
        }
        log.debug("Motor type code for [{}] has not found.", type);
        return null;
    }

    private String getFanSupplierCode(String supplier) {
        if (ServiceConstant.JSON_FAN_SUPPLIER_DEFAULT.equals(supplier)) { // 卧龙
            return ServiceConstant.SYS_ALPHABET_C;
        } else if (ServiceConstant.JSON_FAN_SUPPLIER_ABB.equals(supplier)) { // ABB
            return ServiceConstant.SYS_ALPHABET_A;
        } else if (ServiceConstant.JSON_FAN_SUPPLIER_DONGYUAN.equals(supplier)) { // 东元 TECO
            return ServiceConstant.SYS_ALPHABET_E;
        } else if (ServiceConstant.JSON_FAN_SUPPLIER_SIMENS.equals(supplier)) { // GE
            return ServiceConstant.SYS_ALPHABET_A;
        }
        return null;
    }

    private String getAbsorberType(String fanModel) {
        String fanModelNo = NumberUtil.onlyNumbers(fanModel);
        if (fanModel.contains(ServiceConstant.JSON_FAN_FANMODEL_FC)) {
            return fanModelNo + ServiceConstant.JSON_FAN_FANMODEL_KTQ;
        } else if (fanModel.contains(ServiceConstant.JSON_FAN_FANMODEL_BC)) {
            return fanModelNo + ServiceConstant.JSON_FAN_FANMODEL_KTH;
        }
        return fanModel;
    }

    private double calculateNsCGPrice(NonstdPrice nonstdPrice, LpRate csRate, LpRate lpRate) {
        return (nonstdPrice.getTp() - nonstdPrice.getOldPrice()) / lpRate.getMargin() / lpRate.getRate() * csRate.getRate();
    }

    private double calculateNsPrice(NonstdPrice nonstdPrice, LpRate lpRate) {
        return (nonstdPrice.getTp() - nonstdPrice.getOldPrice()) / lpRate.getMargin() / lpRate.getRate();
    }

    private String calAccessNsPrice(NSPriceParam nsPriceParam) {
        double price = 0.00;
        double sectionL = nsPriceParam.getSectionL();
        //5、水盘模数比段长模数少1，比如6M段长配置的时5M水盘；
        int dripSectionL = NumberUtil.convertStringToDoubleInt("" + (nsPriceParam.getSectionL() - 1));
        String serial = nsPriceParam.getSerial();

        String nsDrainpan = ServiceConstant.SYS_BLANK;//水盘
        nsDrainpan = nsPriceParam.getNs_section_access_nsDrainpan();
        String nsDrainpanMaterial = ServiceConstant.SYS_BLANK;//水盘材质
        nsDrainpanMaterial = nsPriceParam.getNs_section_access_nsDrainpanMaterial();
        String nsDrainpanType = ServiceConstant.SYS_BLANK;//水盘形式
        nsDrainpanType = nsPriceParam.getNs_section_access_nsDrainpanType();

        if (ServiceConstant.SYS_ASSERT_TRUE.equals(nsDrainpan)) {
            //6、安装斜水盘时，段长必须为双数。
            if (JSON_ACCESS_DRAINPANTYPE_SG.equals(nsDrainpanType) && sectionL % 2 != 0) {
                log.error("Non-std Access price sectionL must be even numbers.", nsDrainpanMaterial + serial + sectionL);
                throw new PriceCalcException(PRICE_NS_ACCESS_SECTIONL_CALCULATION_FAILED);
            }
            //4M 以下段长空段，不允许使用控股单非标水盘
            if(sectionL<4){
                throw new PriceCalcException(PRICE_NS_ACCESS_4_SECTIONL_CALCULATION_FAILED);
            }

            NsAccessFlatGlPrice nsAccessFlatGlPrice = AhuMetadata.findOne(NsAccessFlatGlPrice.class, serial);
            NsAccessFlatSlPrice nsAccessFlatSlPrice = AhuMetadata.findOne(NsAccessFlatSlPrice.class, serial);

            if (JSON_ACCESS_DRAINPANMATERIAL_GISTEEL.equals(nsDrainpanMaterial)) {
                if(EmptyUtil.isNotEmpty(nsAccessFlatGlPrice)){
                    //4、延长3M水盘可以按7M-4M计算；7M以上水盘按比例计算，比如9M=7M价格/7*9
                    if (dripSectionL > 9) {
                        price = nsAccessFlatGlPrice.getM7() / 7 * dripSectionL;
                    } else {
                        price = Double.parseDouble(getValueByColumnAndClass("m" + dripSectionL, nsAccessFlatGlPrice));
                    }
                }else{
                    throw new PriceCalcException(ErrorCode.PRICE_NS_DRAINPANMATERIAL_CALCULATION_FAILED);
                }
                

            } else if (JSON_ACCESS_DRAINPANMATERIAL_STAINLESSSTEEL.equals(nsDrainpanMaterial)) {
                if(EmptyUtil.isNotEmpty(nsAccessFlatSlPrice)){
                    //4、延长3M水盘可以按7M-4M计算；7M以上水盘按比例计算，比如9M=7M价格/7*9
                    if (dripSectionL > 9) {
                        price = nsAccessFlatSlPrice.getM7() / 7 * dripSectionL;
                    } else {
                        price = Double.parseDouble(getValueByColumnAndClass("m" + dripSectionL, nsAccessFlatSlPrice));
                    }
                }else{
                    throw new PriceCalcException(ErrorCode.PRICE_NS_DRAINPANMATERIAL_CALCULATION_FAILED);
                }
                
            } else {
                log.error("Non-std Access price for - [{}] has not found.", nsDrainpanMaterial + serial + sectionL);
                throw new PriceCalcException("Section calAccessNsPrice calculation error " + nsDrainpanMaterial + serial + sectionL);
            }

            //7、斜水盘价格在平水盘价格基础上*1.05
            if (JSON_ACCESS_DRAINPANTYPE_SG.equals(nsDrainpanType)) {
                price *= 1.05;
            }
        }

        return String.valueOf(price);
    }

    public String getValueByColumnAndClass(String s, Object t) {//根据列取值
        Class<?> aClass = t.getClass();
        Field[] fields = aClass.getDeclaredFields();
        Map<Object, Object> map = new HashMap<>();
        for (Field field : fields) {
            map.put(field.getName(), getResult(field.getName(), t));
        }
        return map.get(s).toString();
    }

    private static Object getResult(Object fieldName, Object t) {
        try {
            Class<?> aClass = t.getClass();
            Field declaredField = aClass.getDeclaredField(fieldName.toString());
            declaredField.setAccessible(true);
            PropertyDescriptor pd = new PropertyDescriptor(declaredField.getName(), aClass);
            Method readMethod = pd.getReadMethod();
            return readMethod.invoke(t);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

}

package com.carrier.ahu.service.cal.impl;

import com.carrier.ahu.common.entity.UserConfigParam;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.CalcTypeEnum;
import com.carrier.ahu.service.cal.ICalContext;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liujianfeng
 */
@Data
public class DefaultCalContext implements ICalContext {
    private List<ICalContext> children = new ArrayList<ICalContext>();
    private String name;
    private String text;
    private boolean success = false;
    private double value;
    private String error;
    private CalOperationEnum calOperation;
    private CalcTypeEnum calcType;
    private UserConfigParam userConfigParam;

    @Override
    public void completed(double d) {
        this.success = true;
        this.value = d;
    }

    @Override
    public void error(String s) {
        this.setError(s);
        this.setSuccess(false);
    }

    @Override
    public void enter(ICalContext subContext) {
        children.add(subContext);
    }

    public void setCalOperation(CalOperationEnum operation) {
        this.calOperation = operation;
    }

    public CalOperationEnum getCalOperation() {
        return this.calOperation;
    }

    @Override
    public CalcTypeEnum getCalType() {
        return this.calcType;
    }

    public void setCalcTypeEnum(CalcTypeEnum calcType) {
        this.calcType = calcType;
    }

    public UserConfigParam getUserConfigParam() {
        return userConfigParam;
    }

    public void setUserConfigParam(UserConfigParam userConfigParam) {
        this.userConfigParam = userConfigParam;
    }


}

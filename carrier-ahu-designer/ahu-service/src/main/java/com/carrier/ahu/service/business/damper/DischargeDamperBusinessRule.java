package com.carrier.ahu.service.business.damper;

import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_DISCHARGE;
import static com.carrier.ahu.util.meta.SectionMetaUtils.getSectionMetaValueString;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_AINTERFACE;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_METERIAL;

import java.util.Map;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.DamperMaterialEnum;
import com.carrier.ahu.common.enums.DamperPosEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.vo.DamperPosSizeResultVO;
import com.carrier.ahu.vo.SystemCalculateConstants;

public class DischargeDamperBusinessRule extends AbstractDamperBusinessRule {

    public DischargeDamperBusinessRule(Unit unit, Part section) {
        super(unit, section);
    }

    @Override
    protected DamperMaterialEnum getDamperMaterial(Map<String, Object> sectionMeta) {
        String aInterface = getSectionMetaValueString(sectionType(), KEY_AINTERFACE, sectionMeta);
        if (SystemCalculateConstants.DISCHARGE_AINTERFACE_FLANGE.equals(aInterface)) {
            return DamperMaterialEnum.FD;
        }

        String damperMaterial = getSectionMetaValueString(sectionType(), KEY_DAMPER_METERIAL, sectionMeta);
        if (SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_ALUMINUM.equals(damperMaterial)) {
            return DamperMaterialEnum.AL;
        } else if (SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_GL.equals(damperMaterial)) {
            return DamperMaterialEnum.GI;
        }

        throw new IllegalArgumentException("illegal damper material meta value");
    }

    @Override
    protected SectionTypeEnum sectionType() {
        return TYPE_DISCHARGE;
    }

    @Override
    protected void calculateFanSpeed(DamperPosEnum damperPos, DamperPosSizeResultVO resultVo) {
        double airVolume = isSupplierAir() ? getSupplierAirVolume() : getReturnAirVolume();
        double damperArea = (resultVo.getSizeX() / 1000) * (resultVo.getSizeY() / 1000);
        resultVo.setFanSpeed((airVolume / 3600) / damperArea);
    }

}

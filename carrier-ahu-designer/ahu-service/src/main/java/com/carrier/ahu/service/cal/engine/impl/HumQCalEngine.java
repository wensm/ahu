package com.carrier.ahu.service.cal.engine.impl;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.calculator.PerformanceCalculator;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.common.exception.calculation.HeatQCalcException;
import com.carrier.ahu.common.exception.calculation.HumQCalcException;
import com.carrier.ahu.length.param.HumidificationBean;
import com.carrier.ahu.metadata.entity.humidifier.S4HHumiQ;
import com.carrier.ahu.metadata.entity.humidifier.SJModel;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.param.EngineConvertParam;
import com.carrier.ahu.param.HumQCalParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.validate.ValidateService;
import com.carrier.ahu.util.AirVolumeUtil;
import com.carrier.ahu.util.DefaultValueValidationUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.InvokeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.carrier.ahu.common.exception.ErrorCode.HUMIDIFICATION_TOO_MUCH_WITH_MAX_HUMIDIFICATION_FAILED;
import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

/**
 * Created by LIANGD4 on 2017/12/13.
 * 计算出风温度
 * 包含:加湿段
 * 条件加湿量、相对湿度（加湿比）二选一
 */
@Component
public class HumQCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(HumQCalEngine.class.getName());
    @Autowired
    private ValidateService validateService;

    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        logger.info("HumQCalEngine calculator line begin");
        Map<String, Object> mapAll = new HashMap<String, Object>();
        int airVolume = 0;
        String sectionKey = section.getKey();
        double sHumidificationQ = 0.00;
        double wHumidificationQ = 0.00;
        SJModel ssjModel = null;
        SJModel wsjModel = null;
        String supplier = ServiceConstant.SYS_BLANK;
        try {
            HumQCalParam humQCalParam = new InvokeTool<HumQCalParam>().genInParamFromAhuParam(ahu, section, seasonS, new HumQCalParam());
            boolean isSummer = humQCalParam.isEnableSummer();
            boolean isWinter = humQCalParam.isEnableWinter();

            if (AirDirectionEnum.RETURNAIR.getCode().equals(humQCalParam.getAirDirection())) {
                airVolume = humQCalParam.getEairvolume();//回风
            } else {
                airVolume = AirVolumeUtil.packageSAirVolumeToInt(humQCalParam.getSairvolume(),humQCalParam.getAppendAirVolume());//送风
            }
            
            if (isSummer) {
                sHumidificationQ = humQCalParam.getHumidificationQ();//夏季加湿量
                ssjModel = PerformanceCalculator.getActualHumidificationQ(sHumidificationQ, humQCalParam.getInDryBulbT(),
                        humQCalParam.getJSBRelativeT());

                if(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionKey) && sHumidificationQ <= 0) {//干蒸加湿加湿量为空，使用蒸汽压力推导出加湿量
                    List<S4HHumiQ> newS4HHumiQPoList = new ArrayList<S4HHumiQ>();
                    PerformanceCalculator.getHumiQPoList(humQCalParam.getInputVaporPressure(), newS4HHumiQPoList);
                    sHumidificationQ = newS4HHumiQPoList.get(0).getHumidificationQ();
                    humQCalParam.setHumidificationQ(sHumidificationQ);
                }else if(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionKey) && humQCalParam.getJSBRelativeT() > 0 && context.getCalOperation().equals(CalOperationEnum.BATCH)) {//批量选型，湿膜加湿有相对湿度推导出加湿量
                    HumidificationBean humidificationBean = PerformanceCalculator.IsoenthalpyRHConvert(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), humQCalParam.getJSBRelativeT(), airVolume);
                    sHumidificationQ = humidificationBean.getHumidificationQ();
                    humQCalParam.setHumidificationQ(sHumidificationQ);
                }else{
                    DefaultValueValidationUtil.zeroHumQ(sHumidificationQ);
                }

                supplier = humQCalParam.getSupplier();
                //计算前温度条件校验
                //1:湿球不能大于干球
                validateService.compareT(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), null);
                //2:加湿量不能 小于 0
                validateService.compareHumidificationQ(sHumidificationQ, null);


                logger.info("HumQCalEngine calculator paramS :" + JSONArray.toJSONString(humQCalParam));
                if (SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(sectionKey)
                        || SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionKey)) {//（等温加湿）电极加湿段(Electrode Humidifier)/干蒸汽加湿段(Steam Humidifier)

                    HumidificationBean humidificationBean = PerformanceCalculator.IsothermalHumidificationConvert(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), sHumidificationQ, airVolume);
                    mapAll.putAll(getMap(ahu, humidificationBean, seasonS, section));
                    logger.info("HumQCalEngine calculator humidificationBeanS :" + JSONArray.toJSONString(humidificationBean));

                } else if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionKey)
                        || SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(sectionKey)) {//（等焓加湿）湿膜加湿段(Wetfilm Humidifier)  /高压喷雾加湿段(Spray Humidifier)

                    HumidificationBean humidificationBean = PerformanceCalculator.IsoenthalpyHumidificationConvert(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), sHumidificationQ, airVolume);
                    mapAll.putAll(getMap(ahu, humidificationBean, seasonS, section));
                    logger.info("HumQCalEngine calculator humidificationBeanS :" + JSONArray.toJSONString(humidificationBean));

                }
            }

            if (isWinter) {
                humQCalParam = new InvokeTool<HumQCalParam>().genInParamFromAhuParam(ahu, section, seasonW, new HumQCalParam());
                wHumidificationQ = humQCalParam.getHumidificationQ();//冬季加湿量
                wsjModel = PerformanceCalculator.getActualHumidificationQ(wHumidificationQ, humQCalParam.getInDryBulbT(),
                        humQCalParam.getJSBRelativeT());

                if(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionKey) && wHumidificationQ <= 0) {//干蒸加湿加湿量为空，使用蒸汽压力推导出加湿量
                    List<S4HHumiQ> newS4HHumiQPoList = new ArrayList<S4HHumiQ>();
                    PerformanceCalculator.getHumiQPoList(humQCalParam.getInputVaporPressure(), newS4HHumiQPoList);
                    wHumidificationQ = newS4HHumiQPoList.get(0).getHumidificationQ();
                    humQCalParam.setHumidificationQ(wHumidificationQ);
                }else if(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionKey) && humQCalParam.getJSBRelativeT() > 0 && context.getCalOperation().equals(CalOperationEnum.BATCH)) {//批量选型，湿膜加湿有相对湿度推导出加湿量
                    HumidificationBean humidificationBean = PerformanceCalculator.IsoenthalpyRHConvert(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), humQCalParam.getJSBRelativeT(), airVolume);
                    wHumidificationQ = humidificationBean.getHumidificationQ();
                    humQCalParam.setHumidificationQ(wHumidificationQ);
                }else {
                    DefaultValueValidationUtil.zeroHumQ(wHumidificationQ);
                }
                //计算前温度条件校验
                //1:湿球不能大于干球
                validateService.compareT(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), null);
                //2:加湿量不能 小于 0
                validateService.compareHumidificationQ(wHumidificationQ, null);


                logger.info("HumQCalEngine calculator paramW :" + JSONArray.toJSONString(humQCalParam));


                if (SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(sectionKey)
                        || SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionKey)) {//（等温加湿）电极加湿段(Electrode Humidifier)/干蒸汽加湿段(Steam Humidifier)

                    HumidificationBean humidificationBean = PerformanceCalculator.IsothermalHumidificationConvert(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), wHumidificationQ, airVolume);
                    mapAll.putAll(getMap(ahu, humidificationBean, seasonW, section));
                    logger.info("HumQCalEngine calculator humidificationBeanW :" + JSONArray.toJSONString(humidificationBean));

                } else if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionKey)
                        || SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(sectionKey)) {//（等焓加湿）湿膜加湿段(Wetfilm Humidifier)  /高压喷雾加湿段(Spray Humidifier)

                    HumidificationBean humidificationBean = PerformanceCalculator.IsoenthalpyHumidificationConvert(humQCalParam.getInDryBulbT(), humQCalParam.getInWetBulbT(), wHumidificationQ, airVolume);
                    mapAll.putAll(getMap(ahu, humidificationBean, seasonW, section));
                    logger.info("HumQCalEngine calculator humidificationBeanW :" + JSONArray.toJSONString(humidificationBean));
                }
            }
            /*根据加湿段类型计算水量*/
            if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionKey)) {//湿膜加湿段
                double waterQuantity = 0.00;
                //封装水量字段属性 水量=冬季加湿量*3 如果 冬季加湿量为0 使用夏季加湿量
                if (wHumidificationQ > 0.00) {
                    waterQuantity = wHumidificationQ * 3;
                } else {
                    waterQuantity = sHumidificationQ * 3;
                }
                mapAll.put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_WATERQUANTITY, waterQuantity);
            } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(sectionKey)) {//高压喷雾加湿段
                double waterQuantity = 0.00;
                //封装水量字段属性 水量=冬季加湿量*4 如果 冬季加湿量为0 使用夏季加湿量
                if (wHumidificationQ > 0.00) {
                    waterQuantity = wHumidificationQ * 4;
                } else {
                    waterQuantity = sHumidificationQ * 4;
                }
                mapAll.put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_WATERQUANTITY, waterQuantity);
                mapAll.put(
                        MetaCodeGen.calculateAttributePrefix(section.getKey())
                                + ServiceConstant.METACOMMON_POSTFIX_WATERFLOW,
                        ServiceConstant.SYS_STRING_NUMBER_3 + ServiceConstant.SYS_STRING_NUMBER_0);

                // 计算实际额定加湿量，取最大加湿量
                SJModel sjModel = ssjModel;
                if (sJModelIsNull(sjModel)) {
                    sjModel = wsjModel;
                } else if (!sJModelIsNull(wsjModel) && sjModel.getHumidificationQ() < wsjModel.getHumidificationQ()) {
                    sjModel = wsjModel;
                }
                if (!sJModelIsNull(sjModel)) {
                    mapAll.put(MetaCodeGen.calculateAttributePrefix(section.getKey())
                            + ServiceConstant.METACOMMON_POSTFIX_MODEL, sjModel.getModel());
                    mapAll.put(MetaCodeGen.calculateAttributePrefix(section.getKey())
                            + ServiceConstant.METACOMMON_POSTFIX_ACTUALHUMIDIFICATIONQ, sjModel.getHumidificationQ());
                }else{
                    mapAll.put(MetaCodeGen.calculateAttributePrefix(section.getKey())
                            + ServiceConstant.METACOMMON_POSTFIX_MODEL, "");
                    mapAll.put(MetaCodeGen.calculateAttributePrefix(section.getKey())
                            + ServiceConstant.METACOMMON_POSTFIX_ACTUALHUMIDIFICATIONQ, "");
                    section.getParams().putAll(mapAll);
                }

                if(sJModelIsNull(sjModel)){
                    /*section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
                            + ServiceConstant.METACOMMON_POSTFIX_MODEL, "");
                    section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
                            + ServiceConstant.METACOMMON_POSTFIX_ACTUALHUMIDIFICATIONQ, "");*/

                    if(null != sjModel && sjModel.getMaxHumidificationQ()>0){
                        throw new HeatQCalcException(HUMIDIFICATION_TOO_MUCH_WITH_MAX_HUMIDIFICATION_FAILED, String.valueOf(sjModel.getMaxHumidificationQ()));
                    }else{
                        throw new HeatQCalcException(HUMIDIFICATION_TOO_MUCH);
                    }
                }
            }
        } catch (TempCalErrorException e) {
            String message = getIntlString(HUMIDIFICATION_VOLUME_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("HumQCalEngine TempCal debug massage :" + message, e.getMessage());
            logger.error("HumQCalEngine TempCal error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new HumQCalcException(HUMIDIFICATION_VOLUME_CALCULATION_FAILED);
        } catch (HumQCalcException e){
            String message = getIntlString(HUMIDIFICATION_CAPACITY_GREATER_THAN_ZERO, AHUContext.getLanguage());
            logger.debug("HumQCalEngine TempCal debug massage :" + message, e.getMessage());
            logger.error("HumQCalEngine TempCal error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new HumQCalcException(HUMIDIFICATION_CAPACITY_GREATER_THAN_ZERO);
        } catch (HeatQCalcException e){
            String message = e.getMessage();
            logger.debug("HumQCalEngine TempCal debug massage :" + message, e.getMessage());
            logger.error("HumQCalEngine TempCal error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw e;
        }catch (Exception e) {
            String message = getIntlString(HUMIDIFICATION_VOLUME_CALCULATION_FAILED, AHUContext.getLanguage());
            String intlMsg = getIntlString(e.getMessage(), AHUContext.getLanguage());
            if(EmptyUtil.isNotEmpty(intlMsg)){
                message = intlMsg;
            }
            logger.debug("HumQCalEngine debug massage :" + message, e.getMessage());
            logger.error("HumQCalEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            if(EmptyUtil.isNotEmpty(intlMsg)) {
                throw new HumQCalcException(e.getMessage());
            }else{
                throw new HumQCalcException(HUMIDIFICATION_VOLUME_CALCULATION_FAILED);
            }
        }
        //返回结果更新到数据库中
        section.getParams().putAll(mapAll);
        context.setSuccess(true);
        ICalContext subc = super.cal(ahu, section, context);
        subc.setSuccess(true);
        logger.info("HumQCalEngine calculator line end");
        return subc;
    }

    /**
     * 判断是否为有效model
     * @param sjModel
     * @return
     */
    private boolean sJModelIsNull(SJModel sjModel){
        if(null == sjModel){
            return true;
        }else if(EmptyUtil.isNotEmpty(sjModel) && sjModel.getMaxHumidificationQ()>0){//如果型号、实际加湿量为空则为无效model，但是需要将最大加湿量返回页面作为提醒参数。
            return true;
        }else{
            return false;
        }

    }
    private Map<String, Object> getMap(AhuParam ahu, HumidificationBean humidificationBean, String season, PartParam section) {
        EngineConvertParam engineConvertParam = new InvokeTool<EngineConvertParam>().genInParamFromAhuParam(ahu, section, season, new EngineConvertParam());
        engineConvertParam.setInRelativeT(humidificationBean.getInRelativeT());//进风相对湿度
        engineConvertParam.setHumidificationQ(humidificationBean.getHumidificationQ());//加湿量
        engineConvertParam.setJSBRelativeT(humidificationBean.getOutRelativeT());//相对湿度
        engineConvertParam.setOutDryBulbT(humidificationBean.getOutDryBulbT());//出风干球温度
        engineConvertParam.setOutWetBulbT(humidificationBean.getOutWetBulbT());//出风湿球温度
        engineConvertParam.setOutRelativeT(humidificationBean.getOutRelativeT());//出风相对湿度
        Map<String, Object> map = new InvokeTool<EngineConvertParam>().reInvoke(engineConvertParam, season, section.getKey());
        return map;
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{
                SectionTypeEnum.TYPE_STEAMHUMIDIFIER,
                SectionTypeEnum.TYPE_WETFILMHUMIDIFIER,
                SectionTypeEnum.TYPE_SPRAYHUMIDIFIER,
                SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER
        };
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_HUMQ;
    }
}

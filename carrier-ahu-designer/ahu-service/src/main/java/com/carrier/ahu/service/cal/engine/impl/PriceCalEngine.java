package com.carrier.ahu.service.cal.engine.impl;

import org.springframework.stereotype.Component;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;

@Component
public class PriceCalEngine extends AbstractCalEngine {

	@Override
	public int getOrder() {
		return 7;
	}

	@Override
	public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
		ICalContext subc = super.cal(ahu, section, context);
		// TODO 增加计算逻辑
		subc.setSuccess(true);
		return subc;
	}

	@Override
	public String[] getEnabledSectionIds() {
		SectionTypeEnum[] sectionEs = AhuSectionMetas.SECTIONTYPES;
		String[] result = new String[sectionEs.length];
		for (int i = 0; i < sectionEs.length; i++) {
			result[i] = sectionEs[i].getId();
		}
		return result;
	}

	@Override
	public String getKey() {
		return ICalEngine.ID_PRICE;
	}

}

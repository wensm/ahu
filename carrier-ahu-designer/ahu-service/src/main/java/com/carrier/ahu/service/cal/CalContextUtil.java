package com.carrier.ahu.service.cal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.carrier.ahu.service.cal.impl.DefaultCalContext;
import com.carrier.ahu.util.EmptyUtil;

public class CalContextUtil {
	public static final String OVER_STR = "#OVER#";
	public static final String RESULT_STR = "RESULT:";

	public static CalContextUtil getInstance() {
		if (null == instance) {
			reload();
		}
		return instance;
	}

	public static void reload() {
		instance = new CalContextUtil();
	}

	private CalContextUtil() {
		defaultCalContextMap = new HashMap<>();
		allQueueMap = new HashMap<>();
		dynamicQueueMap = new HashMap<>();
//		calculatorKeySet = new HashSet<>();
	}

	/** 树形结构的批量计算上下文Map，Key:batchId */
	private Map<String, DefaultCalContext> defaultCalContextMap;
	/** 批量计算进度全量队列Map，Key:batchId */
	private Map<String, ConcurrentLinkedQueue<String>> allQueueMap;
	/** 批量计算进度增量队列Map，Key:batchId */
	private Map<String, ConcurrentLinkedQueue<String>> dynamicQueueMap;
	/** 批量计算已经结束的batchId集合 */
//	private Set<String> calculatorKeySet;
	/** 内存中是否存在正在执行的运算 */
	private boolean calculatorExist = false;

	/**
	 * 更新批量计算上下文
	 * 
	 * @param projectId
	 * @param defaultCalContext
	 */
	public void refreshContext(String projectId, DefaultCalContext defaultCalContext) {
		defaultCalContextMap.put(genBatchId(projectId), defaultCalContext);
	}

	/**
	 * 增量增加批量计算进度内容
	 * 
	 * @param projectId
	 * @param queueMessage
	 */
	public void refreshQueueIncrementally(String projectId, String queueMessage) {
		System.out.println("------------------------" + queueMessage);
		String batchId = genBatchId(projectId);
		if (allQueueMap.containsKey(batchId)) {
			ConcurrentLinkedQueue<String> queue = allQueueMap.get(batchId);
			queue.offer(queueMessage);
			allQueueMap.put(batchId, queue);
		} else {
			ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<>();
			queue.offer(queueMessage);
			allQueueMap.put(batchId, queue);
		}
		if (dynamicQueueMap.containsKey(batchId)) {
			ConcurrentLinkedQueue<String> queue = dynamicQueueMap.get(batchId);
			queue.offer(queueMessage);
			dynamicQueueMap.put(batchId, queue);
		} else {
			ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<>();
			queue.offer(queueMessage);
			dynamicQueueMap.put(batchId, queue);
		}
//		if (OVER_STR.equalsIgnoreCase(queueMessage)) {
//			calculatorKeySet.add(batchId);
//		} else {
//			if (isBatchOver(projectId)) {
//				calculatorKeySet.remove(batchId);
//			}
//		}
	}

	/**
	 * 获取全量的树形结构的批量计算上下文信息
	 * 
	 * @param projectId
	 * @return
	 */
	public DefaultCalContext getDefaultCalContext(String projectId) {
		return defaultCalContextMap.get(genBatchId(projectId));
	}

	/**
	 * 获取增量的队列结构的批量计算上下文信息
	 * 
	 * @param projectId
	 * @return
	 */
	public ConcurrentLinkedQueue<String> getDynamicQueueContext(String projectId) {
		ConcurrentLinkedQueue<String> queue = dynamicQueueMap.get(genBatchId(projectId));
		if (EmptyUtil.isEmpty(queue)) {
			return null;
		}
		ConcurrentLinkedQueue<String> queueReturn = new ConcurrentLinkedQueue<>(queue);
		dynamicQueueMap.remove(genBatchId(projectId));
		return queueReturn;
	}

	/**
	 * 获取全量的队列结构的批量计算上下文信息
	 * 
	 * @param projectId
	 * @return
	 */
	public ConcurrentLinkedQueue<String> getAllQueueContext(String projectId) {
		return allQueueMap.get(genBatchId(projectId));
	}

	/**
	 * 创建批量运算ID
	 * 
	 * @return
	 */
	private static String genBatchId(String projectId) {
		return "BACTH-" + projectId;
	}

	/**
	 * 判断进程是否结束
	 * 
	 * @param projectid
	 * @return
	 */
//	public boolean isBatchOver(String projectId) {
//		return calculatorKeySet.contains(genBatchId(projectId));
//	}

	/**
	 * 获取计算器开关
	 * 
	 * @return
	 */
	public boolean isCalculatorExist() {
		return calculatorExist;
	}

	/**
	 * 设置计算器开关
	 * 
	 * @param calculatorExist
	 */
	public void setCalculatorExist(boolean calculatorExist) {
		this.calculatorExist = calculatorExist;
	}

	/** 单例模式的机组、段属性配置类 */
	private static CalContextUtil instance = new CalContextUtil();

    public static void sendMessage(String projectId, String message) {
        getInstance().refreshQueueIncrementally(projectId, message);
    }

    public static void sendResultMessage(String projectId, String message) {
        getInstance().refreshQueueIncrementally(projectId, RESULT_STR + message);
    }

    public static void sendFinishMessage(String projectId) {
        getInstance().refreshQueueIncrementally(projectId, OVER_STR);
    }

}

package com.carrier.ahu.service.cal.engine.impl;

import static com.carrier.ahu.common.intl.I18NConstants.SECTION_MIX_FAN_INVERTICALDIRECTION_ERROR;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_HEATINGCOIL_ROWS;
import static com.carrier.ahu.constant.CommonConstant.SITUATION_S3;
import static com.carrier.ahu.vo.SystemCalculateConstants.META_SECTION_COMPLETED;

import java.util.Map;

import com.carrier.ahu.common.exception.calculation.ValidateCalcException;
import com.carrier.ahu.length.CoilLen;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.param.PositivePressureDoorParam;
import com.carrier.ahu.positivepressuredoor.PositivePressureDoorUtil;
import com.carrier.ahu.util.NumberUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.calculation.SectionLengthCalcException;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.engine.cad.IniUtils2;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.InvokeTool;
import com.carrier.ahu.vo.SystemCalculateConstants;

@Component
public class LengthCalEngine extends AbstractCalEngine {

	private static Logger logger = LoggerFactory.getLogger(FilterArrangementCalEngine.class.getName());

	@Autowired
	private LengthService lengthService;

    @Autowired
    private PositivePressureDoorUtil positivePressureDoorUtil;

	@Override
	public int getOrder() {
		return 5;
	}

	@Override
	public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
		logger.info("LengthCalEngine calculator line begin");
		LengthParam lengthParam = new InvokeTool<LengthParam>().genInParamFromAhuParam(ahu, section, null,
				new LengthParam());
		SectionTypeEnum sectionTypeEnum = SectionTypeEnum.getSectionTypeFromId(section.getKey());
		lengthParam.setSectionType(sectionTypeEnum.getCode());
		lengthParam.setSectionId(sectionTypeEnum.getId());
		AhuLayout layout = ahu.getLayout();
		if (layout != null) {
			lengthParam.setVerticalUnit((layout.getStyle() == LayoutStyleEnum.VERTICAL_UNIT_1.style())
					|| (layout.getStyle() == LayoutStyleEnum.VERTICAL_UNIT_2.style()));
		}
		try {
			logger.info("LengthCalEngine calculator param :" + JSONArray.toJSONString(lengthParam));

			// 获取左右段key
			PartParam prePartParam = IniUtils2.getPrePartParam(ahu, section.getPosition());
			PartParam nextPartParam = IniUtils2.getNextPartParam(ahu, section.getPosition());
			PartParam nextNextPartParam = IniUtils2.getNextNextPartParam(ahu, section.getPosition());
			// 前后端都为空，则为选型段conform 操作；处理前后端
			if (null == prePartParam && null == nextPartParam) {
				if (null != section.getNextParams())
					lengthParam.setNextPartKey(
							String.valueOf(section.getNextParams().get(ServiceConstant.SYS_MAP_AHU_KEY)));

				if (null != section.getNextNextParams())
					lengthParam.setNextNextPartKey(
							String.valueOf(section.getNextNextParams().get(ServiceConstant.SYS_MAP_AHU_KEY)));

				if (null != section.getPreParams())
					lengthParam
							.setPrePartKey(String.valueOf(section.getPreParams().get(ServiceConstant.SYS_MAP_AHU_KEY)));
			} else {
				if (null != prePartParam) {
					lengthParam.setPrePartKey(prePartParam.getKey());
					section.setPreParams(prePartParam.getParams());
				}
				if (null != nextPartParam) {
					lengthParam.setNextPartKey(nextPartParam.getKey());
					section.setNextParams(nextPartParam.getParams());
				}
				if (null != nextNextPartParam) {
					lengthParam.setNextNextPartKey(nextNextPartParam.getKey());
					section.setNextNextParams(nextNextPartParam.getParams());
				}
			}

			if (SectionTypeEnum.TYPE_ACCESS.getId().equals(lengthParam.getSectionId())) {
				lengthParam.setOutlet(ServiceConstant.SYS_BLANK);
				if (SectionTypeEnum.TYPE_FAN.getId().equals(lengthParam.getNextPartKey())) {
					Map<String, Object> nextParams = section.getNextParams();
					String outlet = ServiceConstant.METASEXON_FAN_OUTLET;
					if (!EmptyUtil.isEmpty(nextParams) && nextParams.containsKey(outlet)) {
						String value = nextParams.get(outlet).toString();
						if (SystemCalculateConstants.FAN_OPTION_OUTLET_WWK.equals(value)) {
							lengthParam.setOutlet(SystemCalculateConstants.FAN_OPTION_OUTLET_WWK);
						}
					}
					String fanModel = ServiceConstant.METASEXON_FAN_FANMODEL;
					if (!EmptyUtil.isEmpty(nextParams) && nextParams.containsKey(fanModel)) {
						if (!EmptyUtil.isEmpty(nextParams.get(fanModel))) {
							String value = nextParams.get(fanModel).toString();
							if (!EmptyUtil.isEmpty(value)) {
								lengthParam.setFanModel(value);
							}
						}
					}
				}
			}

			if (null != lengthParam.getNextPartKey() && SectionTypeEnum
					.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)) {
				String hRows = section.getNextParams().get(METASEXON_HEATINGCOIL_ROWS).toString();
				lengthParam.setHRows(hRows);
			}

			int minLength = lengthService.getCalLength(lengthParam);// 计算最小段长
			int defaultLength = lengthService.getDefaultLength(lengthParam);// 计算默认段长
			lengthParam.setCalSectionL(minLength);
			lengthParam.setDefaultSectionL(defaultLength);
			int showLength = lengthService.getShowLength(lengthParam);// 最终显示段长

			section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
					+ ServiceConstant.METACOMMON_POSTFIX_CALSECTIONL, minLength);// 最小段长
			section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
					+ ServiceConstant.METACOMMON_POSTFIX_DEFAULTSECTIONL, defaultLength);// 默认段长（最优段长）
			section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
					+ ServiceConstant.METACOMMON_POSTFIX_SECTIONL, showLength);// 最终显示段长

			// 补丁程序，
			if (SectionTypeEnum.TYPE_COLD.equals(sectionTypeEnum)) {
				//1、记录冷加热是否合并段长，sap冷水热水段长码需要使用（如果冷+热合并段长:冷=冷+热；热=0） //放上去只为存入数据库真正的使用放到com/carrier/ahu/calculator/ExcelForPriceCodeUtils.java:566处理，但此代码有用保留以备后用
				CoilLen coilLen = new CoilLen();
				if(coilLen.isMergeLen(lengthParam,sectionTypeEnum.getId())){
					section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
							+ ServiceConstant.METACOMMON_POSTFIX_ENABLEMERGELEN, true);// 记录冷加热是否合并段长
				}else{
					section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
							+ ServiceConstant.METACOMMON_POSTFIX_ENABLEMERGELEN, false);// 记录冷加热是否合并段长
				}

				//2、如果冷水盘管段+热水盘管段长在特定范围
				Integer ser = SystemCountUtil.getUnitNoInt(lengthParam.getSerial());
				//小型机组:如果冷水盘管段+热水盘管段长在特定范围6M
				if (null != lengthParam.getNextPartKey()
						&& SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)
						&& SystemCountUtil.ltBigUnit(ser)) {
					if (-1 == minLength) {
						section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
								+ ServiceConstant.METACOMMON_POSTFIX_CALSECTIONL, defaultLength);// 最小段长（最优段长）
					}
					section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
							+ ServiceConstant.METACOMMON_POSTFIX_MAXSECTIONL, 6);// 最大段长
				} else {
					// 大型机组、冷水+非热水盘管：段长范围默认段长
					section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
							+ ServiceConstant.METACOMMON_POSTFIX_CALSECTIONL, defaultLength);// 最小段长
					section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
							+ ServiceConstant.METACOMMON_POSTFIX_MAXSECTIONL, defaultLength);// 最大段长（最优段长）
				}

			}
			// 补丁程序，如果冷水盘管段长不为6，不能是斜水盘，如果是斜水盘，强制将值修改为平水盘
			if (SectionTypeEnum.TYPE_COLD.equals(sectionTypeEnum)) {
				String drainPanType = lengthParam.getDrainpanType();
				if (defaultLength != 6 && CommonConstant.JSON_AHU_COIL_DRAINPANTYPE_SLOPING.equals(drainPanType)) {
					section.getParams()
							.put(MetaCodeGen.calculateAttributePrefix(section.getKey())
									+ ServiceConstant.METACOMMON_POSTFIX_DRAINPANTYPE,
									CommonConstant.JSON_AHU_COIL_DRAINPANTYPE_FLAT);
				}

			}
			/* 如果当前段为无蜗壳风机，重新计算无蜗壳前的空段段长 */
			if (SectionTypeEnum.TYPE_WWKFAN.equals(sectionTypeEnum) && null != lengthParam.getPrePartKey()
					&& SectionTypeEnum.TYPE_ACCESS
							.equals(SectionTypeEnum.getSectionTypeFromId(lengthParam.getPrePartKey()))) {
				LengthParam preLengthParam = new InvokeTool<LengthParam>().genInParamFromAhuParam(ahu, prePartParam,
						null, new LengthParam());
				// 重新计算前一个空段的默认段长
				int preDefaultLength = lengthService.getDefaultLength(preLengthParam);// 计算默认段长
				// 空段段长重新放入机组内 TODO
			}

			Map map = section.getParams();
			//如果为无蜗壳风机段且选中安装正压门，则判断是否需要增加段长
            PositivePressureDoorParam positivePressureDoorParam = positivePressureDoorUtil.getPositivePressureDoorSituation(ahu.getSeries(),map);
			if(EmptyUtil.isNotEmpty(positivePressureDoorParam)){
                String situation = positivePressureDoorParam.getSituation();
                if(SITUATION_S3.equals(situation)){//如果为S3则说明需要重新计算段长
                    defaultLength = positivePressureDoorParam.getSegL();
                    lengthParam.setDefaultSectionL(defaultLength);
                    showLength = lengthService.getShowLength(lengthParam);// 最终显示段长
                    section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
                            + ServiceConstant.METACOMMON_POSTFIX_DEFAULTSECTIONL, defaultLength);// 默认段长（最优段长）
                    section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey())
                            + ServiceConstant.METACOMMON_POSTFIX_SECTIONL, showLength);// 最终显示段长
                }
            }
			/**
			 * 立式机组上层的风机，和下层的顶部出风的功能段垂直方向重叠，需要提醒。
			 */

			int theLenMixofFanInVerticalDirection = NumberUtil.convertStringToDoubleInt(section.getLenMixofFanInVerticalDirection());
			if(SectionTypeEnum.getSectionTypeFromId(lengthParam.getSectionId()).equals(SectionTypeEnum.TYPE_FAN)
					&& theLenMixofFanInVerticalDirection>0
					&& showLength > theLenMixofFanInVerticalDirection){
				throw new ValidateCalcException(SECTION_MIX_FAN_INVERTICALDIRECTION_ERROR);
			}

			logger.info("LengthCalEngine calculator length :" + showLength);
			context.setSuccess(true);
			ICalContext subc = super.cal(ahu, section, context);
			subc.setSuccess(true);
			logger.info("LengthCalEngine calculator line end");
			return subc;
		} catch (Exception e) {
			e.printStackTrace();
			String message = getIntlString(e.getMessage(), AHUContext.getLanguage());
			logger.debug("LengthCalEngine debug massage :" + message, e.getMessage());
			logger.error("LengthCalEngine error massage :" + e.getMessage());
			ICalContext subc = super.cal(ahu, section, context);
			subc.setSuccess(false);
			subc.error(message);
			context.setSuccess(false);
			section.getParams().put(META_SECTION_COMPLETED, false);
			throw new SectionLengthCalcException(e.getMessage());
		}
	}

	@Override
	public String[] getEnabledSectionIds() {
		SectionTypeEnum[] sectionEs = AhuSectionMetas.SECTIONTYPES;
		String[] result = new String[sectionEs.length];
		for (int i = 0; i < sectionEs.length; i++) {
			result[i] = sectionEs[i].getId();
		}
		return result;
	}

	@Override
	public String getKey() {
		return ICalEngine.ID_LENGTH;
	}

}

package com.carrier.ahu.service.cal;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.service.constant.ServiceConstant;

@Data
public class CoolingCoilCalResultBean {
	private List<String> attributeList;
	private Map<String, Map<String, String>> results;
	public CoolingCoilCalResultBean() {
		attributeList = new ArrayList<String>();
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_COOLINGDETAIL_ROWS);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_COOLINGDETAIL_FINDENSITY);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_COOLINGDETAIL_CIRCUIT);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_VELOCITY);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_AREA);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_ENTERINGAIRDB);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_ENTERINGAIRWB);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_ENTERINGRH);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_RETURNAIRDB);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_RETURNAIRWB);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_RETURNRH);
		//风侧
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_AIRRESISTANCE);
		//水侧
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_MAXWPD);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_VOLUMETRICFLOWRATE);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_ENTERINGFLUIDTEMPERATURE);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_TEMPRETURECHANGE);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_FLUIDVELOCITY);
		//性能
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_TOTALCAPACITY);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_SENSIBLECAPACITY);
		attributeList.add(ServiceConstant.METASEXON_COOLINGCOIL_LEAVINGAIRDB);
		
	}
}

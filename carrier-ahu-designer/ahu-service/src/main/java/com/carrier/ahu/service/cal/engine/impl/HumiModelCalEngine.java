package com.carrier.ahu.service.cal.engine.impl;

import com.carrier.ahu.calculator.PerformanceCalculator;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.calculation.ModelSelectionCalcException;
import com.carrier.ahu.length.param.ModelSelectionParam;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.humidifier.S4HHumiQ;
import com.carrier.ahu.metadata.entity.humidifier.SJSupplier;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.InvokeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.carrier.ahu.common.intl.I18NConstants.MODEL_CALCULATION_FAILED;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

/**
 * Created by LIANGD4 on 2017/12/17.
 */
@Component
public class HumiModelCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(HumiModelCalEngine.class.getName());

//    private static String type = "meta.section.steamHumidifier.HTypes";
//    private static String vaporPressure = "meta.section.steamHumidifier.VaporPressure";
//    private static String hq = "meta.section.sprayHumidifier.hq";
//    private static String nozzleN = "meta.section.sprayHumidifier.nozzleN";
//    private static String aperture = "meta.section.sprayHumidifier.aperture";

    @Override
    public int getOrder() {
        return 3;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        try {
            logger.info("ModelSelectionEngine calculator line begin");
            Map<String, Object> mapAll = new HashMap<String, Object>();
            ModelSelectionParam modelSelectionParam = new InvokeTool<ModelSelectionParam>().genInParamFromAhuParam(ahu, section, null, new ModelSelectionParam());
            boolean isSummer = modelSelectionParam.isEnableSummer();
            boolean isWinter = modelSelectionParam.isEnableWinter();
            String season = ServiceConstant.SYS_BLANK;
            if (isSummer && isWinter) {
                season = seasonAll;
            } else {
                if (isSummer) {
                    season = seasonS;
                } else if (isWinter) {
                    season = seasonW;
                }
            }
            /*根据对应条件计算*/
            if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(section.getKey())) {//干蒸加湿
//                List<S4HHumiQ> s4HHumiQPoList = PerformanceCalculator.getPerformanceH(season, modelSelectionParam.getSHumidificationQ(), modelSelectionParam.getWHumidificationQ(), modelSelectionParam.getSupplier());
                List<S4HHumiQ> newS4HHumiQPoList = new ArrayList<S4HHumiQ>();
                PerformanceCalculator.getHumiQPoList(modelSelectionParam.getInputVaporPressure(), newS4HHumiQPoList);
                getMapByH(newS4HHumiQPoList, section);
            } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(section.getKey())) {//高压喷雾加湿
                List<SJSupplier> sjSupplierPoList = PerformanceCalculator.getPerformanceJ(season, modelSelectionParam.getSHumidificationQ(), modelSelectionParam.getWHumidificationQ(), modelSelectionParam.getSupplier());
                getMapByJ(sjSupplierPoList, section);
            }
            section.getParams().putAll(mapAll);
            context.setSuccess(true);
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(true);
            logger.info("ModelSelectionEngine calculator line end");
            return subc;
        } catch (Exception e) {
            String message = getIntlString(MODEL_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("ModelSelectionEngine debug massage :" + message, e.getMessage());
            logger.error("ModelSelectionEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new ModelSelectionCalcException(MODEL_CALCULATION_FAILED);
        }
    }

    private void getMapByH(List<S4HHumiQ> s4HHumiQPoList, PartParam section) {
        for (S4HHumiQ s4HHumiQ : s4HHumiQPoList) {
            section.getParams().put(ServiceConstant.METASEXON_STEAMHUMIDIFIER_HTYPES, s4HHumiQ.getHTypes());
            section.getParams().put(ServiceConstant.METASEXON_STEAMHUMIDIFIER_VAPORPRESSURE, s4HHumiQ.getVaporPressure());
            return;
        }
    }

    private void getMapByJ(List<SJSupplier> sjSupplierPoList, PartParam section) {
        for (SJSupplier sjSupplierPo : sjSupplierPoList) {
            section.getParams().put(ServiceConstant.METASEXON_SPRAYHUMIDIFIER_HQ, sjSupplierPo.getHq());
            section.getParams().put(ServiceConstant.METASEXON_SPRAYHUMIDIFIER_NOZZLEN, sjSupplierPo.getNozzleN());
            section.getParams().put(ServiceConstant.METASEXON_SPRAYHUMIDIFIER_APERTURE, sjSupplierPo.getAperture());
            return;
        }
    }

    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{
                SectionTypeEnum.TYPE_STEAMHUMIDIFIER,
                SectionTypeEnum.TYPE_SPRAYHUMIDIFIER
        };
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_HUM;
    }

    @Override
    public int getCalLevel() {
        return CalOperationEnum.BATCH.getId();
    }
}

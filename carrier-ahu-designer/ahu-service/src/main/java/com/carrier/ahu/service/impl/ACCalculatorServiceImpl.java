package com.carrier.ahu.service.impl;

import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_PLATEHEATRECYCLE;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_WHEELHEATRECYCLE;
import static com.carrier.ahu.vo.SystemCalculateConstants.seasonS;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carrier.ahu.calculator.PerformanceCalculator;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.engine.service.heatRecycle.HeatRecycleService;
import com.carrier.ahu.length.param.HumidificationBean;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.service.ACCalculatorService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.validate.ValidateService;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.InvokeTool;
import com.carrier.ahu.vo.HeatRecycleResultVO;
import com.google.common.collect.Lists;

/**
 * 
 * @ClassName: ACCalculatorServiceImpl
 * @author dongxiangxiang <dong.xiangxiang@carries.utc.com>
 * @date 2018年1月29日 下午7:41:18
 *
 */
@Service
public class ACCalculatorServiceImpl extends AbstractService implements ACCalculatorService {
    @Autowired
    private ValidateService validateService;

    @Autowired
    private HeatRecycleService heatRecycleService;

    @Override
    public AirConditionBean FAirParmCalculate1(double ParmT, double ParmTB, LanguageEnum languageEnum)
            throws TempCalErrorException {
        // 湿球不能大于干球
        validateService.compareT(ParmT, ParmTB, languageEnum);

        AirConditionBean bean = AirConditionUtils.FAirParmCalculate1(ParmT,
                ParmTB);
        return bean;
    }

    @Override
    public HumidificationBean IsoenthalpyRHConvert(double E_Db, double E_Wb,
            double RH, int airV, LanguageEnum languageEnum) throws TempCalErrorException {
        HumidificationBean humidificationBean = PerformanceCalculator.IsoenthalpyRHConvert(E_Db, E_Wb,
                RH, airV);
        return humidificationBean;
    }

    @Override
    public HumidificationBean IsothermalRHConvert(double E_Db, double E_Wb,
            double RH, int airV, LanguageEnum languageEnum) throws TempCalErrorException {
        HumidificationBean humidificationBean = PerformanceCalculator.IsothermalRHConvert(E_Db, E_Wb,
                RH, airV);
        return humidificationBean;
    }

    @Override
    public HumidificationBean IsoenthalpyHumidificationConvert(double E_Db, double E_Wb,
            double humidification, int airV, LanguageEnum languageEnum) throws TempCalErrorException {
        HumidificationBean humidificationBean = PerformanceCalculator.IsoenthalpyHumidificationConvert(E_Db, E_Wb,
                humidification, airV);
        return humidificationBean;
    }

    @Override
    public HumidificationBean IsothermalHumidificationConvert(double E_Db, double E_Wb,
            double humidification, int airV, LanguageEnum languageEnum) throws TempCalErrorException {
        HumidificationBean humidificationBean = PerformanceCalculator.IsothermalHumidificationConvert(E_Db, E_Wb,
                humidification, airV);
        return humidificationBean;
    }

    @Override
    public HeatRecycleResultVO calculateHeatRecycleInfo(AhuParam ahuParam,
            PartParam partParam) throws Exception {
        HeatRecycleParam heatRecycleParam = new InvokeTool<HeatRecycleParam>().genInParamFromAhuParam(ahuParam,
                partParam, seasonS, new HeatRecycleParam());
        if (TYPE_WHEELHEATRECYCLE.getId().equals(partParam.getKey())) {
            heatRecycleParam.setHeatRecoveryType(ServiceConstant.METASEXON_WHEELHEATRECYCLE_HEATWHEEL);
        } else if (TYPE_PLATEHEATRECYCLE.getId().equals(partParam.getKey())) {
            heatRecycleParam.setHeatRecoveryType(ServiceConstant.METASEXON_PLATEHEATRECYCLE_HEATPLATE);
        }
        if (CommonConstant.SYS_ALPHABET_Z_UP.equals(heatRecycleParam.getBrand())) {
            heatRecycleParam.setNSEnable(true);
        }

        List<List<HeatRecycleInfo>>  heatRecycleInfos=heatRecycleService.getHeatRecycleEngineByRule2(heatRecycleParam,
                    null, AHUContext.getAhuVersion());
        return new HeatRecycleResultVO(heatRecycleInfos);
    }

}

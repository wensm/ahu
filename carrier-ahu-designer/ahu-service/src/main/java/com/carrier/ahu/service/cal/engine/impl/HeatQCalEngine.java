package com.carrier.ahu.service.cal.engine.impl;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.common.exception.calculation.HeatQCalcException;
import com.carrier.ahu.engine.service.coil.CoilService;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.coil.S4FArea;
import com.carrier.ahu.metadata.entity.coil.S4FTHPress;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.param.EngineConvertParam;
import com.carrier.ahu.param.HeatQCalParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.ICalEngine;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.validate.ValidateService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.carrier.ahu.common.intl.I18NConstants.HEATING_CAPACITY_GREATER_THAN_ZERO;
import static com.carrier.ahu.common.intl.I18NConstants.HEATING_VOLUME_CALCULATION_FAILED;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

/**
 * Created by LIANGD4 on 2017/12/13.
 * 计算出风温度
 * 包含：蒸汽盘管、电加热盘管
 * 条件：干球温度、热量 二选一
 */
@Component
public class HeatQCalEngine extends AbstractCalEngine {

    private static Logger logger = LoggerFactory.getLogger(HeatQCalEngine.class.getName());
    @Autowired
    private ValidateService validateService;
    @Autowired
    private CoilService coilService;

    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public ICalContext cal(AhuParam ahu, PartParam section, ICalContext context) throws Exception {
        Map<String, Object> mapAll = new HashMap<String, Object>();
        logger.info("HeatQCalEngine calculator line begin");
        double airVolume = 0;
        try {
            HeatQCalParam sHeatQCalParam = new InvokeTool<HeatQCalParam>().genInParamFromAhuParam(ahu, section, seasonS, new HeatQCalParam());
            HeatQCalParam wHeatQCalParam = new InvokeTool<HeatQCalParam>().genInParamFromAhuParam(ahu, section, seasonW, new HeatQCalParam());
            boolean isSummer = sHeatQCalParam.isEnableSummer();
            boolean isWinter = sHeatQCalParam.isEnableWinter();
            if (AirDirectionEnum.RETURNAIR.getCode().equals(sHeatQCalParam.getAirDirection())) {
                airVolume = sHeatQCalParam.getEairvolume();//回风
            } else {
                airVolume = AirVolumeUtil.packageSAirVolume(sHeatQCalParam.getSairvolume(),sHeatQCalParam.getAppendAirVolume());//送风
            }

            if (SectionTypeEnum.TYPE_STEAMCOIL.getId().equals(section.getKey())) {//如果是蒸汽盘管段单独计算蒸汽压力
                double heatQ = 0;
                double inputVaporPressure = sHeatQCalParam.getInputVaporPressure();
                section.getParams().put(MetaCodeGen.calculateAttributePrefix(section.getKey()) + ServiceConstant.METACOMMON_POSTFIX_VAPORPRESSURE, String.valueOf(inputVaporPressure));
                S4FTHPress s4HHumiQ= AhuMetadata.findOne(S4FTHPress.class, String.valueOf(inputVaporPressure));
                if(EmptyUtil.isNotEmpty(s4HHumiQ)){
                    if(isSummer) {
                        heatQ = calHeatQ(sHeatQCalParam.getInDryBulbT(), airVolume, sHeatQCalParam.getSerial(), s4HHumiQ.getTh());
                    }
                    if (isWinter) {
                        heatQ = calHeatQ(wHeatQCalParam.getInDryBulbT(), airVolume, sHeatQCalParam.getSerial(), s4HHumiQ.getTh());
                    }
                }
                sHeatQCalParam.setHeatQ(heatQ/1000);
                wHeatQCalParam.setHeatQ(heatQ/1000);
            }

            if(isSummer) {
                DefaultValueValidationUtil.zeroHeatQ(sHeatQCalParam.getHeatQ());
                validateService.compareT(sHeatQCalParam.getInDryBulbT(), sHeatQCalParam.getInWetBulbT(), null);//计算前温度条件校验 1:湿球不能大于干球
                logger.info("HeatQCalEngine calculator paramS :" + JSONArray.toJSONString(sHeatQCalParam));
                CoilHeatAirConditionBean coilHeatAirConditionBean = CoilHeatCalUtil.calHeatByHeatQ(airVolume, sHeatQCalParam.getInDryBulbT(), sHeatQCalParam.getInWetBulbT(), sHeatQCalParam.getHeatQ());
                coilHeatAirConditionBean.setOutputF(AirConditionUtils.FAirParmCalculate1(sHeatQCalParam.getInDryBulbT(), sHeatQCalParam.getInWetBulbT()).getParmF());
                mapAll.putAll(getMap(coilHeatAirConditionBean, seasonS, section));
                logger.info("HeatQCalEngine calculator coilHeatAirConditionBeanS :" + JSONArray.toJSONString(coilHeatAirConditionBean));
            }
            if (isWinter) {
                DefaultValueValidationUtil.zeroHeatQ(wHeatQCalParam.getHeatQ());
                validateService.compareT(wHeatQCalParam.getInDryBulbT(), wHeatQCalParam.getInWetBulbT(), null);//计算前温度条件校验 1:湿球不能大于干球
                logger.info("HeatQCalEngine calculator paramW :" + JSONArray.toJSONString(wHeatQCalParam));
                CoilHeatAirConditionBean wCoilHeatAirConditionBean = CoilHeatCalUtil.calHeatByHeatQ(airVolume, wHeatQCalParam.getInDryBulbT(), wHeatQCalParam.getInWetBulbT(), wHeatQCalParam.getHeatQ());
                wCoilHeatAirConditionBean.setOutputF(wHeatQCalParam.getInRelativeT());
                mapAll.putAll(getMap(wCoilHeatAirConditionBean, seasonW, section));
                logger.info("HeatQCalEngine calculator coilHeatAirConditionBeanW :" + JSONArray.toJSONString(wCoilHeatAirConditionBean));
            }
        } catch (TempCalErrorException e) {
            String message = getIntlString(HEATING_VOLUME_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("HeatQCalEngine TempCal debug massage :" + message, e.getMessage());
            logger.error("HeatQCalEngine TempCal error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new HeatQCalcException(HEATING_VOLUME_CALCULATION_FAILED);
        } catch (HeatQCalcException e) {
            e.printStackTrace();
            String message = getIntlString(HEATING_CAPACITY_GREATER_THAN_ZERO, AHUContext.getLanguage());
            logger.debug("HeatQCalEngine TempCal debug massage :" + message, e.getMessage());
            logger.error("HeatQCalEngine TempCal error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new HeatQCalcException(HEATING_CAPACITY_GREATER_THAN_ZERO);
        }catch (Exception e) {
            String message = getIntlString(HEATING_VOLUME_CALCULATION_FAILED, AHUContext.getLanguage());
            logger.debug("HeatQCalEngine debug massage :" + message, e.getMessage());
            logger.error("HeatQCalEngine error massage :" + e.getMessage());
            ICalContext subc = super.cal(ahu, section, context);
            subc.setSuccess(false);
            subc.error(message);
            context.setSuccess(false);
            section.getParams().put(META_SECTION_COMPLETED, false);
            throw new HeatQCalcException(HEATING_VOLUME_CALCULATION_FAILED);
        }
        section.getParams().putAll(mapAll);
        context.setSuccess(true);
        ICalContext subc = super.cal(ahu, section, context);
        subc.setSuccess(true);
        logger.info("HeatQCalEngine calculator line end");
        return subc;
    }

    private double calHeatQ(double parmTin, double parmAirFlow, String serial,double THMin) {
        double ldVarFF = 0.00;
        double ldVarF = 0.00;
        double ldVarTH = 0.00;
        double ldVarQ=0.0;
        double max=197.0;
        parmAirFlow = parmAirFlow / 3600.0;
        S4FArea s4FArea = AhuMetadata.findOne(S4FArea.class, serial);
        if (!EmptyUtil.isEmpty(s4FArea)) {
            ldVarF = s4FArea.getFArea();
            ldVarFF = s4FArea.getFfArea();
        }

        for(double parmTout=parmTin+20;parmTout<200.0;parmTout=parmTout+1.0) {
        	double ldVarDensity = AirConditionUtils.FFormulaDensity(parmTin, parmTout);
            double ldVarVr = parmAirFlow * ldVarDensity / ldVarFF;
            ldVarQ = parmAirFlow * 1.01 * (parmTout - parmTin) * ldVarDensity * 1000.0;
            double ldVarK = 17.0 * Math.pow(ldVarVr, 0.608);
            ldVarTH = ldVarQ * 1.2 / (ldVarF * ldVarK) + (parmTin + parmTout) / 2.0;

            if(ldVarTH>=max) {
            	break;
            }
            if(ldVarTH>THMin) {
            	double dinMin=parmTout-1.0;
            	while(parmTout>=dinMin) {
            		    ldVarDensity = AirConditionUtils.FFormulaDensity(parmTin, parmTout);
                        ldVarVr = parmAirFlow * ldVarDensity / ldVarFF;
                        ldVarQ = parmAirFlow * 1.01 * (parmTout - parmTin) * ldVarDensity * 1000.0;
                        ldVarK = 17.0 * Math.pow(ldVarVr, 0.608);
                        ldVarTH = ldVarQ * 1.2 / (ldVarF * ldVarK) + (parmTin + parmTout) / 2.0;

                        if(BaseDataUtil.doubleConversionInteger(ldVarTH)<=THMin) {
            			   break;
            			}
                        parmTout-=0.1;
            		}
            	break;
            	
            }
        	
        }
        
        
        return ldVarQ;
    }

    private Map<String, Object> getMap(CoilHeatAirConditionBean coilHeatAirConditionBean, String season, PartParam section) {
        EngineConvertParam engineConvertParam = new EngineConvertParam();
        engineConvertParam.setInRelativeT(coilHeatAirConditionBean.getOutputF());
        engineConvertParam.setHeatQ(coilHeatAirConditionBean.getHeatQ());//热量
        engineConvertParam.setHumDryBulbT(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getOutputT(), 1));//目标干球温度
        engineConvertParam.setOutDryBulbT(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getParmT(), 1));//出风干球温度
        engineConvertParam.setOutWetBulbT(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getParmTb(), 1));//出风湿球温度
        engineConvertParam.setOutRelativeT(coilHeatAirConditionBean.getParmF());//出风相对湿度
        Map<String, Object> map = new InvokeTool<EngineConvertParam>().reInvoke(engineConvertParam, season, section.getKey());
        return map;
    }


    @Override
    public String[] getEnabledSectionIds() {
        SectionTypeEnum[] sectionEs = new SectionTypeEnum[]{
                SectionTypeEnum.TYPE_STEAMCOIL,
                SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL
        };
        String[] result = new String[sectionEs.length];
        for (int i = 0; i < sectionEs.length; i++) {
            result[i] = sectionEs[i].getId();
        }
        return result;
    }

    @Override
    public String getKey() {
        return ICalEngine.ID_HEATQ;
    }
}

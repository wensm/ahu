package com.carrier.ahu.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.carrier.ahu.common.entity.Unit;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.specification.SectionSpecifications;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by Wen zhengtao on 2017/3/17.
 */
@Service
public class SectionServiceImpl extends AbstractService implements SectionService {

	@Override
	public Part findAhuListByUnitIdAndPosition(String unitId, Short position) {
		Part part = sectionDao.findOne(new Specification<Part>() {
			@Override
            public Predicate toPredicate(Root<Part> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_MSG_REQUEST_UNITID), unitId);
                Predicate p2 = cb.equal(root.get(ServiceConstant.SYS_STRING_POSITION), position);
                query.where(cb.and(p1, p2));
                return null;
            }
		});
		return part;
	}

	@Override
	public String addSection(Part part, String userName) {
		part.setCreateInfo(userName);
		sectionDao.save(part);
		return part.getPartid();
	}

	@Override
	public List<String> addSections(List<Part> parts) {
		Iterable<Part> pp = sectionDao.save(parts);
		List<String> lll = new ArrayList<>();
		for (Part p : pp) {
			lll.add(p.getPartid());
		}
		return lll;
	}

	@Override
	public List<String> updateSections(List<Part> parts) {
		sectionDao.save(parts);
		List<String> list = new ArrayList<>();
		for (Part u : parts) {
			list.add(u.getPartid());
		}
		return list;
	}

	@Override
	public String updateSection(Part part, String userName) {
		part.setUpdateInfo(userName);
		sectionDao.save(part);
		return part.getPartid();
	}

	@Override
	public void deleteSection(String sectionId) {
		sectionDao.delete(sectionId);
	}

	@Override
	public void deleteAHUSections(String ahuId) {
		List<Part> list = sectionDao.findAll(new Specification<Part>() {
			@Override
			public Predicate toPredicate(Root<Part> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> ahuIdPath = root.get(ServiceConstant.SYS_MSG_REQUEST_UNITID);// ahuID
				query.where(cb.equal(ahuIdPath, ahuId));
				return null;
			}
		});
		for (Part p : list) {
			sectionDao.delete(p);
		}
	}

	@Override
	public Part findSectionById(String sectionId) {
		Part section = sectionDao.findOne(sectionId);
		return section;
	}

    @Override
    public List<Part> findSectionList(String ahuId) {
        return sectionDao.findAll(SectionSpecifications.sectionsOfUnit(ahuId));
    }

	@Override
	public List<PartPO> findLinkedSectionsList(String ahuId) {
		Sort sort = new Sort(Sort.Direction.ASC,ServiceConstant.SYS_STRING_POSITION);
		List<Part> list = sectionDao.findAll(new Specification<Part>() {
			@Override
			public Predicate toPredicate(Root<Part> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> ahuIdPath = root.get(ServiceConstant.SYS_MSG_REQUEST_UNITID);// ahuID
				query.where(cb.equal(ahuIdPath, ahuId));
				return null;
			}
		},sort);

		List<PartPO> parts = new LinkedList<PartPO>();

		for (int i = 0; i < list.size(); i++) {
			PartPO po = new PartPO();
			po.setCurrentPart(list.get(i));
			if (i == 0) {
				po.setPrePart(new Part());
				if (list.size() > 1) {
					Part nextPart = list.get(i + 1);
					po.setNextPart(nextPart == null ? new Part() : nextPart);
				} else {
					po.setNextPart(new Part());
				}
			} else if (i == list.size() - 1) {
				Part prePart = list.get(i - 1);
				po.setPrePart(prePart == null ? new Part() : prePart);
				po.setNextPart(new Part());
			} else {
				Part prePart = list.get(i - 1);
				po.setPrePart(prePart == null ? new Part() : prePart);
				Part nextPart = list.get(i + 1);
				po.setNextPart(nextPart == null ? new Part() : nextPart);
			}
			parts.add(po);
		}
		return parts;
	}

	@Override
	public List<String> getAllIds(String pid) {
		List<String> idList = new ArrayList<>();
		if (EmptyUtil.isEmpty(pid)) {
			return idList;
		}
		List<Part> list = sectionDao.findAll(new Specification<Part>() {
			@Override
			public Predicate toPredicate(Root<Part> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), pid);
				query.where(p1);
				return null;
			}
		});
		if (EmptyUtil.isNotEmpty(list)) {
			for (Part bean : list) {
				idList.add(bean.getPartid());
			}
		}
		return idList;
	}
}

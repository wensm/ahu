package com.carrier.ahu.service.impl;

import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.specification.PartitionSpecifications;
import com.carrier.ahu.util.EmptyUtil;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liujianfeng on 2017/9/20.
 */
@Service
public class PartitionServiceImpl extends AbstractService implements PartitionService {

    @Override
    public Partition savePartition(Partition partition, String userName) {
        Partition partition0 = this.findPartitionByAHUId(partition.getUnitid());
        if (EmptyUtil.isEmpty(partition0)) {
            partition.setCreateInfo(userName);
            return this.addPartition(partition);
        } else {
            partition0.setPartitionJson(partition.getPartitionJson());
            partition0.setUpdateInfo(userName);
            return this.updatePartition(partition0);
        }
    }

	@Override
	public Partition addPartition(Partition partition) {
		Partition newOne = partitionDao.save(partition);
		return newOne;
	}

	@Override
	public Partition updatePartition(Partition partition) {
		Partition newOne = partitionDao.save(partition);
		return newOne;
	}

	@Override
	public void deletePartition(String partitionId) {

		partitionDao.delete(partitionId);
	}

	@Override
	public Partition findPartitionById(String partitionId) {
		return partitionDao.findOne(partitionId);
	}

	// TODO to be implemented
	@Override
	public Partition findPartitionByAHUId(String ahuId) {
		List<Partition> partitionList = partitionDao.findAll(PartitionSpecifications.partitionsOfUnit(ahuId));
		if (EmptyUtil.isNotEmpty(partitionList)) {
			return partitionList.get(0);
		}
		return null;
	}

	@Override
	public List<String> addOrUpdateAhus(List<Partition> partitionList) {
        partitionList.forEach(partition -> {
            savePartition(partition, null);
        });
		List<String> list = new ArrayList<>();
		for (Partition u : partitionList) {
			list.add(u.getPartitionid());
		}
		return list;
	}

	@Override
	public List<String> getAllIds(String pid) {
		List<String> idList = new ArrayList<>();
		if (EmptyUtil.isEmpty(pid)) {
			return idList;
		}
		List<Partition> list = partitionDao.findAll(new Specification<Partition>() {
			@Override
			public Predicate toPredicate(Root<Partition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p1 = cb.equal(root.get(ServiceConstant.SYS_SESSION_PATH_PID), pid);
				query.where(p1);
				return null;
			}
		});
		if (EmptyUtil.isNotEmpty(list)) {
			for (Partition bean : list) {
				idList.add(bean.getUnitid());
			}
		}
		return idList;
	}
}

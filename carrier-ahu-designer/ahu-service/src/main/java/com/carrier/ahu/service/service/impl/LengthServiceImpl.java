package com.carrier.ahu.service.service.impl;

import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.calculation.SectionLengthCalcException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.length.*;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.length.util.ParamConvertFactory;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.AirVolumeUtil;
import com.carrier.ahu.util.EmptyUtil;

import org.apache.catalina.Engine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.carrier.ahu.common.intl.I18NConstants.SECTION_LENGTH_CALCULATION_FAILED;

/**
 * Created by liangd4 on 2017/9/28.
 * 计算段长实现类
 */
@Service
public class LengthServiceImpl implements LengthService {

    private static Logger logger = LoggerFactory.getLogger(LengthServiceImpl.class.getName());
    private static double MIXLENGTHCAL = 0;

    /**
     * 最优段长：确认后显示在页面的段长
     * @param lengthParam
     * @return
     * @throws SectionLengthCalcException
     */
    @Override
    public int getDefaultLength(LengthParam lengthParam) throws SectionLengthCalcException {
        double length = 0;
        String sectionType = lengthParam.getSectionType();
        String sectionId = lengthParam.getSectionId();
        try {
            int airVolume = 0;
            if (AirDirectionEnum.RETURNAIR.getCode().equals(lengthParam.getAirDirection())) {
                airVolume = lengthParam.getEairvolume();
            } else {
                airVolume = AirVolumeUtil.packageSAirVolumeToInt(lengthParam.getSairvolume(),lengthParam.getAppendAirVolume());
            }
            if (SectionTypeEnum.TYPE_MIX.getCode().equals(sectionType)) {//A:混合段
                MixedLen mixedLen = new MixedLen();
                lengthParam = ParamConvertFactory.lengthParam(lengthParam);
                length = mixedLen.getLength(lengthParam.getSerial(), lengthParam.getMixType(), lengthParam.isUvLamp(), airVolume);
            } else if (SectionTypeEnum.TYPE_COLD.getId().equals(sectionId)) {//D:冷水盘管段
                CoilLen coilLen = new CoilLen();
                length = coilLen.getLength(lengthParam, SectionTypeEnum.TYPE_COLD.getId());
            } else if (SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId().equals(sectionId)) {//D:直接蒸发式盘管
                CoilLen coilLen = new CoilLen();
                length = coilLen.getLength(lengthParam, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId());
            } else if (SectionTypeEnum.TYPE_HEATINGCOIL.getCode().equals(sectionType)) {//E:热水盘管段
                CoilLen coilLen = new CoilLen();
                length = coilLen.getHLength(lengthParam);
            } else if (SectionTypeEnum.TYPE_STEAMCOIL.getCode().equals(sectionType)) {//F:蒸汽盘管段
                SteamLen steamLen = new SteamLen();
                length = steamLen.getLength();
            } else if (SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getCode().equals(sectionType)) {//G:电加热盘管段
                ElectricityLen electricityLen = new ElectricityLen();
                length = electricityLen.getLength();
            } else if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getCode().equals(sectionType)) {//H:干蒸汽加湿段
                DrySteamLen drySteamLen = new DrySteamLen();
                length = drySteamLen.getLength();
            } else if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode().equals(sectionType)) {//I:湿膜加湿段
                WetFilmLen wetFilmLen = new WetFilmLen();
                length = wetFilmLen.getLength();
            } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getCode().equals(sectionType)) {//J:高压喷雾加湿段
                HighPresueLen highPresueLen = new HighPresueLen();
                length = highPresueLen.getLength();
            } else if (SectionTypeEnum.TYPE_FAN.getCode().equals(sectionType)) {//K:风机段
                String ID = ServiceConstant.SYS_STRING_NUMBER_1;
                if (lengthParam.isVerticalUnit()) {
                    ID = ServiceConstant.SYS_STRING_NUMBER_2;
                }
                length = SectionCount.sectionLengthFan(lengthParam.getSerial(), lengthParam.getFanModel(),
                        lengthParam.getOutletDirection(), lengthParam.getOutlet(), lengthParam.getMotorPosition(),
                        lengthParam.isStandbyMotor(), ID, lengthParam.getMotorBaseNo(),lengthParam.getType());
            } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getCode().equals(sectionType)) {//L:新回排风段
                RepeatingWindLen repeatingWindLen = new RepeatingWindLen();
                length = repeatingWindLen.getLength(lengthParam.getSerial());
            } else if (SectionTypeEnum.TYPE_ATTENUATOR.getCode().equals(sectionType)) {//M:消音段
                ClearSoundLen clearSoundLen = new ClearSoundLen();
                length = clearSoundLen.getLength(lengthParam);
            } else if (SectionTypeEnum.TYPE_DISCHARGE.getCode().equals(sectionType)) {//N:出风段
                WindOutLen windOutLen = new WindOutLen();
                length = windOutLen.getLength(lengthParam.getSerial(), lengthParam.getOutletDirection(), lengthParam.getODoor());
            } else if (SectionTypeEnum.TYPE_ACCESS.getCode().equals(sectionType)) {//O:空段
                NullLen nullLen = new NullLen();
                length = nullLen.getLength(lengthParam);
            } else if (SectionTypeEnum.TYPE_HEATRECYCLE.getCode().equals(sectionType)) {//W:热回收段
                //TODO 热回收分为板式和转轮
            } else if (SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getCode().equals(sectionType)) {//X:电极加湿
                ElectricityHumidifierLen electricityHumidifierLen = new ElectricityHumidifierLen();
                length = electricityHumidifierLen.getLength();
            } else if (SectionTypeEnum.TYPE_CTR.getCode().equals(sectionType)) {//Z:控制段
                ControlLen controlLen = new ControlLen();
                length = controlLen.getLength(lengthParam.getSerial());
			} else if ((ServiceConstant.SYS_SEXON_ALIAS_FILTER + ServiceConstant.SYS_SEXON_ALIAS_COMBINEDFILTER
					+ ServiceConstant.SYS_SEXON_ALIAS_HEPAFILTER + ServiceConstant.SYS_SEXON_ALIAS_ELECTROSTATICFILTER)
							.contains(sectionType)) {// B:过滤段 C:综合过滤段 V:高效过滤段 Y:静电过滤器
				if (ServiceConstant.SYS_SEXON_ALIAS_HEPAFILTER.equals(sectionType)) {
					length = 3;
				} else if (ServiceConstant.SYS_SEXON_ALIAS_ELECTROSTATICFILTER.equals(sectionType)) {
					length = 3;//静电过滤段段长更新为3M Ye, XinJun 2020/09/27
				} else if (ServiceConstant.SYS_SEXON_ALIAS_COMBINEDFILTER.equals(sectionType)) {
					length = SectionCount.sectionLengthCombineFilter(
							lengthParam.getMediaLoading(), lengthParam.getRMaterialE(),lengthParam.getPrePartKey());
					
				} else {
					length = SectionCount.sectionLengthSimpleFilter(lengthParam.getFitetF(),
							lengthParam.getMediaLoading(), lengthParam.getFilterEfficiency(),lengthParam.getPrePartKey());
				}
			} else if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getCode().equals(sectionType) || SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getCode().equals(sectionType)) {
                HeatRecycleLen heatRecycleLen = new HeatRecycleLen();
                length = heatRecycleLen.getLength(lengthParam.getSerial(), sectionType,lengthParam.getSectionL(),lengthParam.getBrand(),lengthParam.getExchangeModel(),lengthParam.getSectionSideL());
                return BaseDataUtil.doubleConversionInteger(length);//如果是热回收段计算的段长是使用段长，不允许修改。
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.debug("Exception in fan seciton length calculation:", lengthParam);
            logger.error("Exception in fan seciton length calculation", e);
            throw new SectionLengthCalcException(SECTION_LENGTH_CALCULATION_FAILED);
        }
        return BaseDataUtil.doubleConversionInteger(length);
    }


    /**
     * 最小段长：范围校验用的validateJson
     * @param lengthParam
     * @return
     * @throws SectionLengthCalcException
     */
    @Override
    public int getCalLength(LengthParam lengthParam) throws SectionLengthCalcException {
        double calLength = 0;
        String sectionType = lengthParam.getSectionType();
        try {
            int airVolume = 0;
            if (AirDirectionEnum.RETURNAIR.getCode().equals(lengthParam.getAirDirection())) {
                airVolume = lengthParam.getEairvolume();
            } else {
            	airVolume = AirVolumeUtil.packageSAirVolumeToInt(lengthParam.getSairvolume(),lengthParam.getAppendAirVolume());

            }
            /*混合段 空段 出风段 段长可以编辑*/
            if (SectionTypeEnum.TYPE_MIX.getCode().equals(sectionType)) {//A:混合段
                MixedLen mixedLen = new MixedLen();
                lengthParam = ParamConvertFactory.lengthParam(lengthParam);
                calLength = mixedLen.getCalLength(lengthParam.getSerial(), lengthParam.getMixType(), lengthParam.isUvLamp(), airVolume);
                MIXLENGTHCAL = calLength;
            } else if (SectionTypeEnum.TYPE_ACCESS.getCode().equals(sectionType)) {//O:空段
                NullLen nullLen = new NullLen();
                calLength = nullLen.getCalLength(lengthParam);
            } else if (SectionTypeEnum.TYPE_DISCHARGE.getCode().equals(sectionType)) {//N:出风段
            	int height = SystemCountUtil.getUnitHeight(lengthParam.getSerial());
                int width = SystemCountUtil.getUnitWidth(lengthParam.getSerial());
                int serialV = height * 100 + width;
                if(SystemCountUtil.gtSmallUnitHeight(height)) {//大型机组2330-4750的出风段最小段长与混合段一致
                	WindOutLen windOutLen = new WindOutLen();
                	calLength = windOutLen.getCalLength(lengthParam.getSerial(), lengthParam.getOutletDirection(), lengthParam.getODoor(), airVolume);
                }else {
                    
                    MixedLen mixedLen = new MixedLen();
                    
                    String outletType=lengthParam.getOutletDirection();
                    //1："顶部出风" 2："后出风" 3："左侧出风" 3："右侧出风" 1："底部出风"
                    if("T".equals(outletType)) {
                    	outletType="1";
                    }else if("A".equals(outletType)) {
                    	outletType="2";
                    }else if("L".equals(outletType)) {
                    	outletType="3";
                    }else if("R".equals(outletType)) {
                    	outletType="3";
                    }else if("F".equals(outletType)) {
                    	outletType="1";
                    }
                    
                    calLength = mixedLen.getCalLength(lengthParam.getSerial(), outletType, lengthParam.isUvLamp(), airVolume);
                }
            } else if(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getCode().equals(sectionType)){//转轮
                if("Z".equals(lengthParam.getBrand())){//如果是无转轮，则最小段长为6
                    return 6;
                }else{
                    return -1;
                }
                
            } else if(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getCode().equals(sectionType)){//板换
                if("Z".equals(lengthParam.getBrand())){//如果是无板换，则最小段长为7
                    return 7;
                }else{
                    return -1;
                }

            } else{
                return -1;
            }
        } catch (Exception e) {
            logger.debug("Exception in fan seciton callength calculation:", lengthParam);
            logger.error("Exception in fan seciton callength calculation", e);
            throw new SectionLengthCalcException(SECTION_LENGTH_CALCULATION_FAILED);
        }
        return BaseDataUtil.doubleConversionInteger(calLength);
    }

    /**
     * 显示段长：当页面有用户手动选择的段长且大于最小保留页面选择的段长。
     * @param lengthParam
     * @return
     * @throws SectionLengthCalcException
     */
    @Override
    public int getShowLength(LengthParam lengthParam) throws SectionLengthCalcException {
        String sectionType = lengthParam.getSectionType();
        boolean isCool = false;
        if(SectionTypeEnum.TYPE_COLD.getCode().equals(sectionType)
                && null!=lengthParam.getNextPartKey()
                && SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)){
            isCool = true;
        }

        if (SectionTypeEnum.TYPE_MIX.getCode().equals(sectionType)
            || SectionTypeEnum.TYPE_ACCESS.getCode().equals(sectionType)
            || SectionTypeEnum.TYPE_DISCHARGE.getCode().equals(sectionType)) {//A:混合段

            boolean isBootom = false;
            if(SectionTypeEnum.TYPE_MIX.getCode().equals(sectionType) && lengthParam.isReturnButtom()){
                isBootom = true;
            }
            if(SectionTypeEnum.TYPE_DISCHARGE.getCode().equals(sectionType) && "F".equals(lengthParam.getOutletDirection())){
                isBootom = true;
            }
            if(isBootom){//混合段、出风段：底部出风最小段长用默认段长即可
                lengthParam.setCalSectionL(lengthParam.getDefaultSectionL());
            }

            if (lengthParam.getSectionL() >= lengthParam.getCalSectionL() && lengthParam.getSectionL() > 0 && lengthParam.getCalSectionL() > 0) {
                return BaseDataUtil.doubleConversionInteger(lengthParam.getSectionL());//如果页面输入的段长大于等于最小段长，应该保留页面输入的段长
            }
        }
        if(SectionTypeEnum.TYPE_SINGLE.getCode().equals(sectionType)
                || SectionTypeEnum.TYPE_COMPOSITE.getCode().equals(sectionType)
                || isCool) {
        	if (lengthParam.getSectionL() > 0) {
                return BaseDataUtil.doubleConversionInteger(lengthParam.getSectionL());//如果页面输入的段长大于等于最小段长，应该保留页面输入的段长
            }
        }
        if(SectionTypeEnum.TYPE_COMPOSITE.getCode().equals(sectionType)) {
        	if (lengthParam.getSectionL() > 0) {
                return BaseDataUtil.doubleConversionInteger(lengthParam.getSectionL());//如果页面输入的段长大于等于最小段长，应该保留页面输入的段长
            }
        }
        if(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getCode().equals(sectionType)) {
            if (lengthParam.getSectionL() > 0) {
                return BaseDataUtil.doubleConversionInteger(lengthParam.getSectionL());//如果页面输入的段长大于等于最小段长，应该保留页面输入的段长
            }
        }
        if(SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getCode().equals(sectionType) && lengthParam.getSectionL()>0){
            return BaseDataUtil.doubleConversionInteger(lengthParam.getSectionL());
        }
        if(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getCode().equals(sectionType)){//转轮
            if(lengthParam.getSectionL() >= lengthParam.getCalSectionL() && lengthParam.getSectionL() > 0 && lengthParam.getCalSectionL() > 0){
                return BaseDataUtil.doubleConversionInteger(lengthParam.getSectionL());
            }
        }
        if(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getCode().equals(sectionType)){//板换
            if(lengthParam.getSectionL() >= lengthParam.getCalSectionL() && lengthParam.getSectionL() > 0 && lengthParam.getCalSectionL() > 0){
                return BaseDataUtil.doubleConversionInteger(lengthParam.getSectionL());
            }

        }
        return BaseDataUtil.doubleConversionInteger(lengthParam.getDefaultSectionL());
    }
}

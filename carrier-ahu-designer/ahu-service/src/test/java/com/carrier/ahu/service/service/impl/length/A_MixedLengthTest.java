package com.carrier.ahu.service.service.impl.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.service.service.impl.LengthServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/11.
 * 混合段
 */
public class A_MixedLengthTest implements SystemCalculateConstants {

    LengthService lengthService = new LengthServiceImpl();

    @Test
    public void getMixedLength0608() throws Exception {//混合段段长计算，条件：机组型号、
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_MIX.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ0608");
        lengthParam.setReturnTop(true);//顶部出风
        lengthParam.setReturnButtom(true);//底部出风
        lengthParam.setUv(false);//不安装检修灯
        double length = lengthService.getDefaultLength(lengthParam);
        Assert.assertEquals("0608_MixedLength_Error", length, 5, 0);
    }

    @Test
    public void getMixedLength1117() throws Exception {//混合段段长计算，条件：机组型号、
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_MIX.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ1117");
        lengthParam.setReturnRight(true);//右侧出风
        lengthParam.setUv(false);//不安装检修灯
        double length = lengthService.getDefaultLength(lengthParam);
        Assert.assertEquals("1117_MixedLength_Error", length, 11, 0);
    }

    @Test
    public void getMixedLength2226() throws Exception {//混合段段长计算，条件：机组型号、
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_MIX.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2226");
        lengthParam.setReturnBack(true);//后出风
        lengthParam.setUv(false);//安装检修灯
        double length = lengthService.getDefaultLength(lengthParam);
        Assert.assertEquals("2226_MixedLength_Error", length, 6, 0);
    }


    @Test
    public void getMixedLength2333() throws Exception {//混合段段长计算，条件：机组型号、
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_MIX.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ2333");
        lengthParam.setReturnButtom(true);//底部出风
        lengthParam.setUv(false);//不安装检修灯
        double length = lengthService.getDefaultLength(lengthParam);
        Assert.assertEquals("2333_MixedLength_Error", length, 11, 0);
    }

    @Test
    public void getMixedLength3438() throws Exception {//混合段段长计算，条件：机组型号、
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_MIX.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ3438");
        lengthParam.setReturnLeft(true);//左侧出风
        lengthParam.setUv(false);//不安装检修灯
        double length = lengthService.getDefaultLength(lengthParam);
        Assert.assertEquals("3438_MixedLength_Error", length, 15, 0);
    }

}

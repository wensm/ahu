package com.carrier.ahu.service.service.impl.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.service.service.impl.LengthServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 */
public class K_FanLengthTest implements SystemCalculateConstants {

    LengthService lengthService = new LengthServiceImpl();

    @Test
    public void getFanLengthWwk() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_FAN.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ0608");
        lengthParam.setFanModel("SYW250R");//风机型号
        lengthParam.setOutlet(FAN_OPTION_OUTLET_WWK);//无蜗壳风机
        lengthParam.setStandbyMotor(false);//备用电机
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }

    @Test
    public void getFanLengthSide() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_FAN.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ0608");
        lengthParam.setFanModel("FC180");
        lengthParam.setStandbyMotor(false);
        lengthParam.setMotorPosition(FAN_MOTORPOSITION_SIDE);//电机位置 侧置
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }

    @Test
    public void getFanLengthBack() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_FAN.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ0608");
        lengthParam.setFanModel("FC180");
        lengthParam.setStandbyMotor(false);
        lengthParam.setMotorPosition(FAN_MOTORPOSITION_BACK);//电机位置 后置
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }

    @Test
    public void getFanLength3132() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_FAN.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ3132");
        lengthParam.setFanModel("KHF1120K");
        lengthParam.setStandbyMotor(false);
        lengthParam.setMotorPosition(FAN_MOTORPOSITION_BACK);//电机位置 后置
        lengthParam.setOutletDirection(FAN_OUTLETDIRECTION_THF);
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }
}

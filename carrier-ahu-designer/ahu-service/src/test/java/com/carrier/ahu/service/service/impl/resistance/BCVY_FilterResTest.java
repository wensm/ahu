package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 */
public class BCVY_FilterResTest implements SystemCalculateConstants {

    //TODO 新旧软件有差异、段长有9
    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getFilterRes1317Out() throws Exception {//单程过滤 外抽
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_SINGLE.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ1317");
        resistanceParam.setSairvolume(18000);
        resistanceParam.setSectionL(0);
        resistanceParam.setFitetF(FILTER_FITETF_X);//过滤形式 外抽
        resistanceParam.setFitlerStandard(FILTER_FITLERSTANDARD_J_GB);//过滤效率参考标准 GB/T14295
        resistanceParam.setRimThickness(FILTER_RIMTHICKNESS_1);//边框厚度 50
        resistanceParam.setFilterEfficiency(FILTER_FILTEREFFICIENCY_C2);//过滤器效率C2
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

    @Test
    public void getFilterRes1317Bag() throws Exception {//单程过滤 袋式
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_SINGLE.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ1317");
//        resistanceParam.setAirVolume(18000);
//        resistanceParam.setSectionLength(6);
        resistanceParam.setFitetF(FILTER_FITETF_B);//过滤形式 袋式
        resistanceParam.setFitlerStandard(FILTER_FITLERSTANDARD_J_EN);//过滤效率参考标准 EN779
        resistanceParam.setRimThickness(FILTER_RIMTHICKNESS_1);//边框厚度 50
        resistanceParam.setFilterEfficiency(FILTER_FILTEREFFICIENCY_G3);//过滤器效率G3
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

    @Test
    public void getFilterRes1() throws Exception {//单程过滤 袋式
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_SINGLE.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ1317");
//        resistanceParam.setAirVolume(18000);
//        resistanceParam.setSectionLength(6);
        resistanceParam.setRimThickness(FILTER_RIMTHICKNESS_1);//边框厚度 50
        resistanceParam.setFitetF(FILTER_FITETF_B);//过滤形式 袋式
        resistanceParam.setFitlerStandard(FILTER_FITLERSTANDARD_J_EN);//过滤效率参考标准 EN779
        resistanceParam.setFilterEfficiency(FILTER_FILTEREFFICIENCY_G3);//过滤器效率G3
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

    @Test
    public void getFilterRes() throws Exception {//单程过滤 板式
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_SINGLE.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ1317");
//        resistanceParam.setAirVolume(18000);
//        resistanceParam.setSectionLength(0);
        resistanceParam.setRimThickness(FILTER_RIMTHICKNESS_1);//边框厚度 50
        resistanceParam.setFitetF(FILTER_FITETF_P);//过滤形式 板式
        resistanceParam.setFitlerStandard(FILTER_FITLERSTANDARD_J_EN);//过滤效率参考标准 EN779
        resistanceParam.setFilterEfficiency(FILTER_FILTEREFFICIENCY_G3);//过滤器效率G3
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }


    @Test
    public void getFilterResV0608() throws Exception {//高效过滤
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_HEPAFILTER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ0608");
//        resistanceParam.setAirVolume(2000);
        resistanceParam.setFitetF("V");//过滤形式
        resistanceParam.setFitlerStandard("EN1822");
        resistanceParam.setFilterEfficiency("H11");
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

    @Test
    public void getFilterResV2328() throws Exception {//高效过滤
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_HEPAFILTER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ2328");
//        resistanceParam.setAirVolume(50000);
        resistanceParam.setFitetF("V");//过滤形式
        resistanceParam.setFitlerStandard("EN1822");
        resistanceParam.setFilterEfficiency("H11");
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

    @Test
    public void getFilterResV3438() throws Exception {//高效过滤
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_HEPAFILTER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ3438");
//        resistanceParam.setAirVolume(100000);
        resistanceParam.setFitetF("V");//过滤形式
        resistanceParam.setFitlerStandard("EN1822");
        resistanceParam.setFilterEfficiency("H11");
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

    @Test
    public void getFilterResY() throws Exception {//静电过滤
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ3438");
//        resistanceParam.setAirVolume(100000);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }

}

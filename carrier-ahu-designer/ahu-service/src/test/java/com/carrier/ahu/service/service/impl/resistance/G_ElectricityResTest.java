package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/11.
 * 电加热盘管段
 */
public class G_ElectricityResTest {

    private static final String version = "CN-HTC-1.0";
    ResistanceService resistanceService = new ResistanceServiceImpl();


    @Test
    public void getElectricityResi() throws Exception {//电加热盘管段
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ0608");
//        resistanceParam.setAirVolume(2000);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }


}

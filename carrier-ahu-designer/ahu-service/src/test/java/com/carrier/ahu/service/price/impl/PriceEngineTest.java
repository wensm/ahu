package com.carrier.ahu.service.price.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.util.List;

import org.junit.Test;

import com.carrier.ahu.calculator.PriceCalculator;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;

public class PriceEngineTest {
	
	@Test
	public void testReadTemplateFile() {
		String templateCsvName = "price.csv";
		try {
			InputStream is = this.getClass().getClassLoader().getResourceAsStream(templateCsvName);
			List<CalculatorSpec> templates = new PriceCalculator().getModel().getSpec();
			assertEquals("Get template size", 37, templates.size());
			assertEquals("First element is ahu template", "ahu.package", templates.get(0).getName());
			assertEquals("First element is ahu support", "ahu.support", templates.get(2).getName());
		} catch (Exception e) {
			fail("Faile to resolve csv file " + templateCsvName);
		}

	}

}

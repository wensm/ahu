package com.carrier.ahu.service.service.impl.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.service.service.impl.LengthServiceImpl;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/11.
 * 蒸汽盘管段段逻辑
 * 段长固定为3M
 */
public class F_SteamLengthTest {

    private static final String version = "CN-HTC-1.0";
    LengthService lengthService = new LengthServiceImpl();

    @Test
    public void getSteamLength() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_STEAMCOIL.getCode());
        lengthParam.setVersion(version);
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }

}

package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 */
public class O_NullResTest implements SystemCalculateConstants {

    private static final String version = "CN-HTC-1.0";
    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getNullRes() throws Exception {//空段 计算结果和旧选型软件不同，阻力较小忽略
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_ACCESS.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ0608");
//        resistanceParam.setAirVolume(2000);
//        resistanceParam.setSectionLength(6);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 0, 1.5);
    }

    @Test
    public void getNullResCurrent() throws Exception {//空段 均流段
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_ACCESS.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ0608");
//        resistanceParam.setAirVolume(2000);
        resistanceParam.setFunction(SystemCalculateConstants.ACCESS_FUNCTION_DIFFUSER);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 29, 1);
    }

}

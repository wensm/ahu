package com.carrier.ahu.service.service.impl.resistance;


import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 * 控制端
 */
public class Z_ControlResTest implements SystemCalculateConstants {

    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getControlRes() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_CTR.getCode());
        resistanceParam.setVersion(version);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
    }
}

package com.carrier.ahu.service.meta.impl;

import org.junit.Test;

import com.carrier.ahu.section.meta.MetaCodeGen;

import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

public class MetaCodeGenTest {
	@Test
	public void testCoilInfoSize() {
		Short s = MetaCodeGen.getSectionType("ahu.filter");
		assertEquals(Short.valueOf((short) 2), s);
	}

	@Test
	public void testSectionTypeCode() {
		String s = MetaCodeGen.getSectionTypeCode("ahu.filter");
		assertEquals("002", s);
	}

	@Test
	public void testGetAhuGroupCode() {
		LinkedList<String> keys = new LinkedList<>();
		// keys.add("ahu.mix");
		// keys.add("ahu.filter");
		// keys.add("ahu.coolingCoil");
		// keys.add("ahu.fan");
		String s = MetaCodeGen.getAhuGroupCode(keys);
		assertEquals("001002004011", s);
	}
}

package com.carrier.ahu.service.service.impl.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.service.service.impl.LengthServiceImpl;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 */
public class W_HeatRecoveryLengthTest {

    private static final String version = "CN-HTC-1.0";
    LengthService lengthService = new LengthServiceImpl();

    @Test
    public void getHeatRecoveryLengthW1() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_HEATRECYCLE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ0608");
        lengthParam.setHeatRecovery("W1");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("W1="+length);
    }

    @Test
    public void getHeatRecoveryLengthW2() throws Exception {
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_HEATRECYCLE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setSerial("39CQ0608");
        lengthParam.setHeatRecovery("W2");
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println("W2="+length);
    }
}

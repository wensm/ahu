package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 * 电极加湿段
 */
public class X_ElectricityHumidifierResTest implements SystemCalculateConstants {

    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getElectricityHumidifierRes0608() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ0608");
//        resistanceParam.setAirVolume(2000);
//        resistanceParam.setSectionLength(6);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 1, 0.2);
    }

    @Test
    public void getElectricityHumidifierRes1317() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ1317");
//        resistanceParam.setAirVolume(18000);
//        resistanceParam.setSectionLength(6);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 2, 0.4);
    }

    @Test
    public void getElectricityHumidifierRes3438() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getCode());
        resistanceParam.setVersion(version);
        resistanceParam.setSerial("39CQ3438");
//        resistanceParam.setAirVolume(100000);
//        resistanceParam.setSectionLength(6);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        Assert.assertEquals(resistance, 1, 0.2);
    }
}

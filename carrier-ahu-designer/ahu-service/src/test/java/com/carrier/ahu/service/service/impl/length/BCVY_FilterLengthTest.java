package com.carrier.ahu.service.service.impl.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.service.service.LengthService;
import com.carrier.ahu.service.service.impl.LengthServiceImpl;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 */
public class BCVY_FilterLengthTest implements SystemCalculateConstants {

    LengthService lengthService = new LengthServiceImpl();

    @Test
    public void getFilterLengthV() throws Exception { //高效过滤段
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_HEPAFILTER.getCode());
        lengthParam.setVersion(version);
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }

    @Test
    public void getFilterLengthY() throws Exception {//静电过滤段
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getCode());
        lengthParam.setVersion(version);
        double length = lengthService.getDefaultLength(lengthParam);
        System.out.println(length);
    }

    @Test
    public void getFilterLengthOut() throws Exception {//单层过滤段
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_SINGLE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setFitetF(FILTER_FITETF_X);//外抽
        double length = lengthService.getDefaultLength(lengthParam);
        Assert.assertEquals(length, 0, 0);
    }

    @Test
    public void getFilterLengthPanelF() throws Exception {//单层过滤段
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_SINGLE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setFitetF(FILTER_FITETF_P);//板式
        lengthParam.setMediaLoading(FILTER_MEDIALOADING_FRONTLOADING);//正抽
        double length = lengthService.getDefaultLength(lengthParam);
        Assert.assertEquals(length, 0, 0);
    }

    @Test
    public void getFilterLengthPanelS() throws Exception {//单层过滤段
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_SINGLE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setFitetF(FILTER_FITETF_P);//板式
        lengthParam.setMediaLoading(FILTER_MEDIALOADING_SIDELOADING);//侧面抽
        double length = lengthService.getDefaultLength(lengthParam);
        Assert.assertEquals(length, 3, 0);

    }

    @Test
    public void getFilterLengthBagS() throws Exception {//单层过滤段
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_SINGLE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setFitetF(FILTER_FITETF_B);//袋式
        lengthParam.setMediaLoading(FILTER_MEDIALOADING_SIDELOADING);//侧面抽
        double length = lengthService.getDefaultLength(lengthParam);
        Assert.assertEquals(length, 6, 0);
    }

    @Test
    public void getFilterLengthBagF5() throws Exception {//单层过滤段
        LengthParam lengthParam = new LengthParam();
        lengthParam.setSectionType(SectionTypeEnum.TYPE_SINGLE.getCode());
        lengthParam.setVersion(version);
        lengthParam.setFitetF(FILTER_FITETF_B);//袋式
        lengthParam.setMediaLoading("");//其它
        lengthParam.setFilterEfficiency(FILTER_FILTEREFFICIENCY_F5);
        double length = lengthService.getDefaultLength(lengthParam);
        Assert.assertEquals(length, 6, 0);
    }

}

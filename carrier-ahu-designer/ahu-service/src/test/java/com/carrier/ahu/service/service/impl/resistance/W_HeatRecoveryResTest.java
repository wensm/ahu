package com.carrier.ahu.service.service.impl.resistance;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.service.service.ResistanceService;
import com.carrier.ahu.service.service.impl.ResistanceServiceImpl;
import org.junit.Test;

/**
 * Created by liangd4 on 2017/10/12.
 */
public class W_HeatRecoveryResTest {

    private static final String version = "CN-HTC-1.0";
    ResistanceService resistanceService = new ResistanceServiceImpl();

    @Test
    public void getHeatRecoveryRes() throws Exception {
        ResistanceParam resistanceParam = new ResistanceParam();
        resistanceParam.setSectionType(SectionTypeEnum.TYPE_HEATRECYCLE.getCode());
        resistanceParam.setVersion(version);
        double resistance = resistanceService.getResistance(resistanceParam);
        System.out.println(resistance);
        //TODO
    }
}

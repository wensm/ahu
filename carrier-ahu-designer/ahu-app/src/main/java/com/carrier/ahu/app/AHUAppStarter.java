package com.carrier.ahu.app;

import java.lang.reflect.Method;
import java.util.List;

import com.carrier.ahu.app.util.AhuUpgradeUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * AHUApplication starter, load ahu libraries and start AHUApplication.
 * 
 * Created by Braden Zhou on 2018/05/25.
 */
@Slf4j
public class AHUAppStarter {

    public static void main(String[] args) {
        try {
            // load dynamic jar files
            List<String> jars = AhuUpgradeUtils.getLibraries();
            for (String jar : jars) {
                AhuUpgradeUtils.loadJar(jar);
            }

            // load AHUApplication
            Class<?> app = Class.forName("com.carrier.ahu.AHUApplication");
            Method m = app.getMethod("main", String[].class);
            m.invoke(null, (Object) args);
        } catch (Exception e) {
            log.error("Failed to start AHU application.", e);
        }
    }

}
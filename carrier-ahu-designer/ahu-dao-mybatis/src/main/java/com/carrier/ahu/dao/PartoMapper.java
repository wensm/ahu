package com.carrier.ahu.dao;

import com.carrier.ahu.po.Parto;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartoMapper {
    int insert(Parto record);

    int insertSelective(Parto record);
}
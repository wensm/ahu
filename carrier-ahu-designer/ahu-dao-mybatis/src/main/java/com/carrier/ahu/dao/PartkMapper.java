package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partk;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartkMapper {
    int insert(Partk record);

    int insertSelective(Partk record);
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partm;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartmMapper {
    int insert(Partm record);

    int insertSelective(Partm record);
}
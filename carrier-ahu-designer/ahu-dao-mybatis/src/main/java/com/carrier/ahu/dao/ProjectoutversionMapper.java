package com.carrier.ahu.dao;

import com.carrier.ahu.po.Projectoutversion;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProjectoutversionMapper {
    int insert(Projectoutversion record);

    int insertSelective(Projectoutversion record);
}
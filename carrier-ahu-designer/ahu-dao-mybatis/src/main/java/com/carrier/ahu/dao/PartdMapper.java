package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partd;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartdMapper {
    int insert(Partd record);

    int insertSelective(Partd record);
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Singlefilter4b;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Singlefilter4bMapper {
    int insert(Singlefilter4b record);

    int insertSelective(Singlefilter4b record);
}
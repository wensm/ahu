package com.carrier.ahu.dao;

import com.carrier.ahu.po.Panelline;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PanellineMapper {
    int insert(Panelline record);

    int insertSelective(Panelline record);
}
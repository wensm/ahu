package com.carrier.ahu.dao;

import com.carrier.ahu.po.Ahuinfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AhuinfoMapper {
    int insert(Ahuinfo record);

    int insertSelective(Ahuinfo record);
}
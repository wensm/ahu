package com.carrier.ahu.dao;

import com.carrier.ahu.po.Casinglist;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CasinglistMapper {
    int insert(Casinglist record);

    int insertSelective(Casinglist record);
}
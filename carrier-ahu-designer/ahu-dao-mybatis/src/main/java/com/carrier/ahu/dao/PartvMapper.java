package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partv;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartvMapper {
    int insert(Partv record);

    int insertSelective(Partv record);
}
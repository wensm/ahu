package com.carrier.ahu.dao;

import com.carrier.ahu.po.Party;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartyMapper {
    int insert(Party record);

    int insertSelective(Party record);
}
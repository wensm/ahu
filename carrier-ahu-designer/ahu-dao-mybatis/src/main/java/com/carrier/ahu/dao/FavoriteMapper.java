package com.carrier.ahu.dao;

import com.carrier.ahu.po.Favorite;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FavoriteMapper {
    int insert(Favorite record);

    int insertSelective(Favorite record);
}
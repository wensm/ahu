package com.carrier.ahu.dao;

import com.carrier.ahu.po.Stream4f;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Stream4fMapper {
    int insert(Stream4f record);

    int insertSelective(Stream4f record);
}
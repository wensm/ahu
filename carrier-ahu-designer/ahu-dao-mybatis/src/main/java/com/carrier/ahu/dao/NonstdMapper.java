package com.carrier.ahu.dao;

import com.carrier.ahu.po.Nonstd;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface NonstdMapper {
    int insert(Nonstd record);

    int insertSelective(Nonstd record);
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Standardunit;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StandardunitMapper {
    int insert(Standardunit record);

    int insertSelective(Standardunit record);
}
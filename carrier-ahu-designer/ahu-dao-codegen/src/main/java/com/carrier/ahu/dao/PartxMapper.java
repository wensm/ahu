package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partx;

public interface PartxMapper {
    int insert(Partx record);

    int insertSelective(Partx record);
}
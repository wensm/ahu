package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partv;

public interface PartvMapper {
    int insert(Partv record);

    int insertSelective(Partv record);
}
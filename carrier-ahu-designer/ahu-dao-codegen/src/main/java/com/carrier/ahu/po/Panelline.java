package com.carrier.ahu.po;

public class Panelline {
	private String pid;

	private String unitid;

	private Short sideid;

	private String groupid;

	private Short vh;

	private Short x1;

	private Short y1;

	private Short x2;

	private Short y2;

	private Short ioption;

	private Short ipaneloption;

	private Short sideindex;

	private Short sidetype;

	private String smemo;

	private String sindex;

	private Short iislastv;

	private Short iturn;

	private String panelproperties;

	private String lineoption;

	private Integer seq;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid == null ? null : pid.trim();
	}

	public String getUnitid() {
		return unitid;
	}

	public void setUnitid(String unitid) {
		this.unitid = unitid == null ? null : unitid.trim();
	}

	public Short getSideid() {
		return sideid;
	}

	public void setSideid(Short sideid) {
		this.sideid = sideid;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	public Short getVh() {
		return vh;
	}

	public void setVh(Short vh) {
		this.vh = vh;
	}

	public Short getX1() {
		return x1;
	}

	public void setX1(Short x1) {
		this.x1 = x1;
	}

	public Short getY1() {
		return y1;
	}

	public void setY1(Short y1) {
		this.y1 = y1;
	}

	public Short getX2() {
		return x2;
	}

	public void setX2(Short x2) {
		this.x2 = x2;
	}

	public Short getY2() {
		return y2;
	}

	public void setY2(Short y2) {
		this.y2 = y2;
	}

	public Short getIoption() {
		return ioption;
	}

	public void setIoption(Short ioption) {
		this.ioption = ioption;
	}

	public Short getIpaneloption() {
		return ipaneloption;
	}

	public void setIpaneloption(Short ipaneloption) {
		this.ipaneloption = ipaneloption;
	}

	public Short getSideindex() {
		return sideindex;
	}

	public void setSideindex(Short sideindex) {
		this.sideindex = sideindex;
	}

	public Short getSidetype() {
		return sidetype;
	}

	public void setSidetype(Short sidetype) {
		this.sidetype = sidetype;
	}

	public String getSmemo() {
		return smemo;
	}

	public void setSmemo(String smemo) {
		this.smemo = smemo == null ? null : smemo.trim();
	}

	public String getSindex() {
		return sindex;
	}

	public void setSindex(String sindex) {
		this.sindex = sindex == null ? null : sindex.trim();
	}

	public Short getIislastv() {
		return iislastv;
	}

	public void setIislastv(Short iislastv) {
		this.iislastv = iislastv;
	}

	public Short getIturn() {
		return iturn;
	}

	public void setIturn(Short iturn) {
		this.iturn = iturn;
	}

	public String getPanelproperties() {
		return panelproperties;
	}

	public void setPanelproperties(String panelproperties) {
		this.panelproperties = panelproperties == null ? null : panelproperties.trim();
	}

	public String getLineoption() {
		return lineoption;
	}

	public void setLineoption(String lineoption) {
		this.lineoption = lineoption == null ? null : lineoption.trim();
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}
}
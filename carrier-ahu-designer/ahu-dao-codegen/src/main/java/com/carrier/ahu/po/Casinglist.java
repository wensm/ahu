package com.carrier.ahu.po;

public class Casinglist {
    private String partname;

    private Short partlm;

    private Short partwm;

    private Double quantity;

    private String memo;

    private String pid;

    private String unitid;

    private Short groupid;

    private Short valid;

    private String panelpro;

    private Integer igroup;

    public String getPartname() {
        return partname;
    }

    public void setPartname(String partname) {
        this.partname = partname == null ? null : partname.trim();
    }

    public Short getPartlm() {
        return partlm;
    }

    public void setPartlm(Short partlm) {
        this.partlm = partlm;
    }

    public Short getPartwm() {
        return partwm;
    }

    public void setPartwm(Short partwm) {
        this.partwm = partwm;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getUnitid() {
        return unitid;
    }

    public void setUnitid(String unitid) {
        this.unitid = unitid == null ? null : unitid.trim();
    }

    public Short getGroupid() {
        return groupid;
    }

    public void setGroupid(Short groupid) {
        this.groupid = groupid;
    }

    public Short getValid() {
        return valid;
    }

    public void setValid(Short valid) {
        this.valid = valid;
    }

    public String getPanelpro() {
        return panelpro;
    }

    public void setPanelpro(String panelpro) {
        this.panelpro = panelpro == null ? null : panelpro.trim();
    }

    public Integer getIgroup() {
        return igroup;
    }

    public void setIgroup(Integer igroup) {
        this.igroup = igroup;
    }
}
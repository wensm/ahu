package com.carrier.ahu.dao;

import com.carrier.ahu.po.Unit;

public interface UnitMapper {
    int deleteByPrimaryKey(String unitid);

    int insert(Unit record);

    int insertSelective(Unit record);

    Unit selectByPrimaryKey(String unitid);

    int updateByPrimaryKeySelective(Unit record);

    int updateByPrimaryKeyWithBLOBs(Unit record);

    int updateByPrimaryKey(Unit record);
}
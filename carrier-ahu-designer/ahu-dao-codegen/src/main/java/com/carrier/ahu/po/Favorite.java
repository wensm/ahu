package com.carrier.ahu.po;

public class Favorite {
    private String pid;

    private String unitid;

    private String ttreecode;

    private String ftreename;

    private String ftreememo;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getUnitid() {
        return unitid;
    }

    public void setUnitid(String unitid) {
        this.unitid = unitid == null ? null : unitid.trim();
    }

    public String getTtreecode() {
        return ttreecode;
    }

    public void setTtreecode(String ttreecode) {
        this.ttreecode = ttreecode == null ? null : ttreecode.trim();
    }

    public String getFtreename() {
        return ftreename;
    }

    public void setFtreename(String ftreename) {
        this.ftreename = ftreename == null ? null : ftreename.trim();
    }

    public String getFtreememo() {
        return ftreememo;
    }

    public void setFtreememo(String ftreememo) {
        this.ftreememo = ftreememo == null ? null : ftreememo.trim();
    }
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.ProjectMemo;
import org.apache.ibatis.annotations.Param;

public interface ProjectMemoMapper {
    int deleteByPrimaryKey(@Param("unitid") String unitid, @Param("pid") String pid);

    int insert(ProjectMemo record);

    int insertSelective(ProjectMemo record);

    ProjectMemo selectByPrimaryKey(@Param("unitid") String unitid, @Param("pid") String pid);

    int updateByPrimaryKeySelective(ProjectMemo record);

    int updateByPrimaryKey(ProjectMemo record);
}
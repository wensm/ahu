package com.carrier.ahu.dao;

import com.carrier.ahu.po.Favorite;

public interface FavoriteMapper {
    int insert(Favorite record);

    int insertSelective(Favorite record);
}
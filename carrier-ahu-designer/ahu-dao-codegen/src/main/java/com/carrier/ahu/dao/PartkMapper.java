package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partk;

public interface PartkMapper {
    int insert(Partk record);

    int insertSelective(Partk record);
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partn;

public interface PartnMapper {
    int insert(Partn record);

    int insertSelective(Partn record);
}
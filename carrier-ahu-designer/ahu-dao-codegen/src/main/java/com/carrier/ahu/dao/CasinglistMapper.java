package com.carrier.ahu.dao;

import com.carrier.ahu.po.Casinglist;

public interface CasinglistMapper {
    int insert(Casinglist record);

    int insertSelective(Casinglist record);
}
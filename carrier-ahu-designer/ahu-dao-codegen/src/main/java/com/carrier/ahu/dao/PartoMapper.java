package com.carrier.ahu.dao;

import com.carrier.ahu.po.Parto;

public interface PartoMapper {
    int insert(Parto record);

    int insertSelective(Parto record);
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Singlefilter4b;

public interface Singlefilter4bMapper {
    int insert(Singlefilter4b record);

    int insertSelective(Singlefilter4b record);
}
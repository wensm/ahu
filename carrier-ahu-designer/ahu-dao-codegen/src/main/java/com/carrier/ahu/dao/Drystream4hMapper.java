package com.carrier.ahu.dao;

import com.carrier.ahu.po.Drystream4h;

public interface Drystream4hMapper {
    int insert(Drystream4h record);

    int insertSelective(Drystream4h record);
}
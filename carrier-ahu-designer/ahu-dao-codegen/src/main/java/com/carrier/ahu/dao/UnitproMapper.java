package com.carrier.ahu.dao;

import com.carrier.ahu.po.Unitpro;
import org.apache.ibatis.annotations.Param;

public interface UnitproMapper {
    int deleteByPrimaryKey(@Param("pid") String pid, @Param("unitid") String unitid);

    int insert(Unitpro record);

    int insertSelective(Unitpro record);

    Unitpro selectByPrimaryKey(@Param("pid") String pid, @Param("unitid") String unitid);

    int updateByPrimaryKeySelective(Unitpro record);

    int updateByPrimaryKey(Unitpro record);
}
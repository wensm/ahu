package com.carrier.ahu.po;

public class Part {
    private String partid;

    private String pid;

    private String unitid;

    private Short orders;

    private Short types;

    private Short position;

    private Short setup;

    private String pic;

    private Short groupi;

    private Double cost;

    private Short avdirection;

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getUnitid() {
        return unitid;
    }

    public void setUnitid(String unitid) {
        this.unitid = unitid == null ? null : unitid.trim();
    }

    public Short getOrders() {
        return orders;
    }

    public void setOrders(Short orders) {
        this.orders = orders;
    }

    public Short getTypes() {
        return types;
    }

    public void setTypes(Short types) {
        this.types = types;
    }

    public Short getPosition() {
        return position;
    }

    public void setPosition(Short position) {
        this.position = position;
    }

    public Short getSetup() {
        return setup;
    }

    public void setSetup(Short setup) {
        this.setup = setup;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }

    public Short getGroupi() {
        return groupi;
    }

    public void setGroupi(Short groupi) {
        this.groupi = groupi;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Short getAvdirection() {
        return avdirection;
    }

    public void setAvdirection(Short avdirection) {
        this.avdirection = avdirection;
    }
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Party;

public interface PartyMapper {
    int insert(Party record);

    int insertSelective(Party record);
}
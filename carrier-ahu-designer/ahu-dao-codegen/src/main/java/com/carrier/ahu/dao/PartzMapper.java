package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partz;

public interface PartzMapper {
    int insert(Partz record);

    int insertSelective(Partz record);
}
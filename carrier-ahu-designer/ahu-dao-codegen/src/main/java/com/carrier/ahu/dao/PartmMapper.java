package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partm;

public interface PartmMapper {
    int insert(Partm record);

    int insertSelective(Partm record);
}
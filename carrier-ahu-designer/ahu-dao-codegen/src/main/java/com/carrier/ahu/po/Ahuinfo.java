package com.carrier.ahu.po;

public class Ahuinfo {
    private String infoname;

    private String infovalue;

    public String getInfoname() {
        return infoname;
    }

    public void setInfoname(String infoname) {
        this.infoname = infoname == null ? null : infoname.trim();
    }

    public String getInfovalue() {
        return infovalue;
    }

    public void setInfovalue(String infovalue) {
        this.infovalue = infovalue == null ? null : infovalue.trim();
    }
}
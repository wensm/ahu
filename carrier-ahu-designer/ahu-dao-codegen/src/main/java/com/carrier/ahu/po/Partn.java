package com.carrier.ahu.po;

public class Partn {
    private String pid;

    private String partid;

    private String airflowo;

    private String ainterface;

    private Double ainterfacer;

    private String odoor;

    private Short sectionl;

    private Double price;

    private Double weight;

    private Double resistance;

    private String nonstandard;

    private String memo;

    private String doortype;

    private String doordirection;

    private String valvem;

    private Boolean uvc;

    private String actuator;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public String getAirflowo() {
        return airflowo;
    }

    public void setAirflowo(String airflowo) {
        this.airflowo = airflowo == null ? null : airflowo.trim();
    }

    public String getAinterface() {
        return ainterface;
    }

    public void setAinterface(String ainterface) {
        this.ainterface = ainterface == null ? null : ainterface.trim();
    }

    public Double getAinterfacer() {
        return ainterfacer;
    }

    public void setAinterfacer(Double ainterfacer) {
        this.ainterfacer = ainterfacer;
    }

    public String getOdoor() {
        return odoor;
    }

    public void setOdoor(String odoor) {
        this.odoor = odoor == null ? null : odoor.trim();
    }

    public Short getSectionl() {
        return sectionl;
    }

    public void setSectionl(Short sectionl) {
        this.sectionl = sectionl;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getResistance() {
        return resistance;
    }

    public void setResistance(Double resistance) {
        this.resistance = resistance;
    }

    public String getNonstandard() {
        return nonstandard;
    }

    public void setNonstandard(String nonstandard) {
        this.nonstandard = nonstandard == null ? null : nonstandard.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getDoortype() {
        return doortype;
    }

    public void setDoortype(String doortype) {
        this.doortype = doortype == null ? null : doortype.trim();
    }

    public String getDoordirection() {
        return doordirection;
    }

    public void setDoordirection(String doordirection) {
        this.doordirection = doordirection == null ? null : doordirection.trim();
    }

    public String getValvem() {
        return valvem;
    }

    public void setValvem(String valvem) {
        this.valvem = valvem == null ? null : valvem.trim();
    }

    public Boolean getUvc() {
        return uvc;
    }

    public void setUvc(Boolean uvc) {
        this.uvc = uvc;
    }

    public String getActuator() {
        return actuator;
    }

    public void setActuator(String actuator) {
        this.actuator = actuator == null ? null : actuator.trim();
    }
}
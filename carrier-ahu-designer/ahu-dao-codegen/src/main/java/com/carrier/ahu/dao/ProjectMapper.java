package com.carrier.ahu.dao;

import com.carrier.ahu.po.Project;

public interface ProjectMapper {
    int deleteByPrimaryKey(String pid);

    int insert(Project record);

    int insertSelective(Project record);

    Project selectByPrimaryKey(String pid);

    int updateByPrimaryKeySelective(Project record);

    int updateByPrimaryKey(Project record);
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Eheat4g;

public interface Eheat4gMapper {
    int insert(Eheat4g record);

    int insertSelective(Eheat4g record);
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Partd;

public interface PartdMapper {
    int insert(Partd record);

    int insertSelective(Partd record);
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Projectoutversion;

public interface ProjectoutversionMapper {
    int insert(Projectoutversion record);

    int insertSelective(Projectoutversion record);
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Parte;

public interface ParteMapper {
    int insert(Parte record);

    int insertSelective(Parte record);
}
package com.carrier.ahu.dao;

import com.carrier.ahu.po.Nonstd;

public interface NonstdMapper {
    int insert(Nonstd record);

    int insertSelective(Nonstd record);
}
package com.carrier.ahu.po;

public class Partz {
    private String pid;

    private String partid;

    private String odoor;

    private Integer sectionl;

    private String chkdoor;

    private Double price;

    private Double weight;

    private Integer resistance;

    private Boolean co2;

    private Boolean tempsensor;

    private Boolean staticsensor;

    private Boolean frost;

    private Boolean lessair;

    private Boolean controlbox;

    private Boolean controller;

    private Boolean panel;

    private Boolean sasensor;

    private Boolean rasensor;

    private Boolean fasensor;

    private Boolean actuators;

    private Boolean pressuregage;

    private Boolean vfd;

    private Boolean asb;

    private Boolean wvd;

    private Integer wvdnum;

    private String mpower;

    private String memo;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public String getOdoor() {
        return odoor;
    }

    public void setOdoor(String odoor) {
        this.odoor = odoor == null ? null : odoor.trim();
    }

    public Integer getSectionl() {
        return sectionl;
    }

    public void setSectionl(Integer sectionl) {
        this.sectionl = sectionl;
    }

    public String getChkdoor() {
        return chkdoor;
    }

    public void setChkdoor(String chkdoor) {
        this.chkdoor = chkdoor == null ? null : chkdoor.trim();
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getResistance() {
        return resistance;
    }

    public void setResistance(Integer resistance) {
        this.resistance = resistance;
    }

    public Boolean getCo2() {
        return co2;
    }

    public void setCo2(Boolean co2) {
        this.co2 = co2;
    }

    public Boolean getTempsensor() {
        return tempsensor;
    }

    public void setTempsensor(Boolean tempsensor) {
        this.tempsensor = tempsensor;
    }

    public Boolean getStaticsensor() {
        return staticsensor;
    }

    public void setStaticsensor(Boolean staticsensor) {
        this.staticsensor = staticsensor;
    }

    public Boolean getFrost() {
        return frost;
    }

    public void setFrost(Boolean frost) {
        this.frost = frost;
    }

    public Boolean getLessair() {
        return lessair;
    }

    public void setLessair(Boolean lessair) {
        this.lessair = lessair;
    }

    public Boolean getControlbox() {
        return controlbox;
    }

    public void setControlbox(Boolean controlbox) {
        this.controlbox = controlbox;
    }

    public Boolean getController() {
        return controller;
    }

    public void setController(Boolean controller) {
        this.controller = controller;
    }

    public Boolean getPanel() {
        return panel;
    }

    public void setPanel(Boolean panel) {
        this.panel = panel;
    }

    public Boolean getSasensor() {
        return sasensor;
    }

    public void setSasensor(Boolean sasensor) {
        this.sasensor = sasensor;
    }

    public Boolean getRasensor() {
        return rasensor;
    }

    public void setRasensor(Boolean rasensor) {
        this.rasensor = rasensor;
    }

    public Boolean getFasensor() {
        return fasensor;
    }

    public void setFasensor(Boolean fasensor) {
        this.fasensor = fasensor;
    }

    public Boolean getActuators() {
        return actuators;
    }

    public void setActuators(Boolean actuators) {
        this.actuators = actuators;
    }

    public Boolean getPressuregage() {
        return pressuregage;
    }

    public void setPressuregage(Boolean pressuregage) {
        this.pressuregage = pressuregage;
    }

    public Boolean getVfd() {
        return vfd;
    }

    public void setVfd(Boolean vfd) {
        this.vfd = vfd;
    }

    public Boolean getAsb() {
        return asb;
    }

    public void setAsb(Boolean asb) {
        this.asb = asb;
    }

    public Boolean getWvd() {
        return wvd;
    }

    public void setWvd(Boolean wvd) {
        this.wvd = wvd;
    }

    public Integer getWvdnum() {
        return wvdnum;
    }

    public void setWvdnum(Integer wvdnum) {
        this.wvdnum = wvdnum;
    }

    public String getMpower() {
        return mpower;
    }

    public void setMpower(String mpower) {
        this.mpower = mpower == null ? null : mpower.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }
}
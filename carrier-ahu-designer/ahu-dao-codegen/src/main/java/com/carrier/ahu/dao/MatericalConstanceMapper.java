/**
 * Title: MatericalConstanceMapper <br>
 * @Description:[] <br>
 * @date 2017-06-22 <br>
 * Copyright (c) 2016 ZXZL YCT.  <br>
 * 
 * @author zhiqiang1.zhang@dhc.com.cn
 * @version V1.0
 */
package com.carrier.ahu.dao;

import com.carrier.ahu.po.MatericalConstance;

public interface MatericalConstanceMapper {
	int deleteByPrimaryKey(String pid);

	int insertSelective(MatericalConstance record);

	MatericalConstance selectByPrimaryKey(String pid);

	int updateByPrimaryKeySelective(MatericalConstance record);

}
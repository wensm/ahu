package com.carrier.ahu.dao;

import com.carrier.ahu.po.Parti;

public interface PartiMapper {
    int insert(Parti record);

    int insertSelective(Parti record);
}
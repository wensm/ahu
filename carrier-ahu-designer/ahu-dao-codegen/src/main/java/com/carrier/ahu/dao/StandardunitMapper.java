package com.carrier.ahu.dao;

import com.carrier.ahu.po.Standardunit;

public interface StandardunitMapper {
    int insert(Standardunit record);

    int insertSelective(Standardunit record);
}
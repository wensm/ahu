package com.carrier.ahu.security.handler;

import com.carrier.ahu.common.entity.LoginoutLog;
import com.carrier.ahu.common.enums.LoginoutEnum;
import com.carrier.ahu.common.enums.YOrNEnum;
import com.carrier.ahu.common.util.IPUtil;
import com.carrier.ahu.service.log.LogService;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 记录登录成功日志
 * Created by Wen zhengtao on 2017/6/20.
 */
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    private static Logger logger = LoggerFactory.getLogger(LoginSuccessHandler.class);

    @Autowired
    private LogService logService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws ServletException, IOException {
        //用户名
        String userName = authentication.getName();
        //用户ip
        String userIp = IPUtil.getIpAddress(request);
        //用户ua
        String userAgent = request.getHeader("User-Agent");
        //日志类型
        String logType = LoginoutEnum.LOGIN.toString();
        //错误信息
        String errMsg = null;
        //请求url
        String requestUrl = request.getRequestURI();
        //请求方式
        String requestMethod = request.getMethod();
        //请求cookie
        Cookie[] cookies = request.getCookies();
        StringBuffer sb = new StringBuffer();
        if(ArrayUtils.isNotEmpty(cookies)){
            for(Cookie cookie:cookies){
                String name = cookie.getName();
                String value = cookie.getValue();
                sb.append(name).append("=").append(value).append(";");
            }
        }

        //组装实体类
        LoginoutLog log = new LoginoutLog();
        log.setUserName(userName);
        log.setUserIp(userIp);
        log.setUserAgent(userAgent);
        log.setLogType(logType);
        log.setErrMsg(errMsg);
        log.setRequestUrl(requestUrl);
        log.setRequestMethod(requestMethod);
        log.setCookies(sb.toString());
        log.setRecordStatus(YOrNEnum.Y.toString());
        log.setCreateInfo(userName);

        //保存到数据库
        String id = logService.saveLog(log);

        logger.debug("id::::"+id);
    }
}

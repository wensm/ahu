package com.carrier.ahu.security.common;

/**
 * Created by Wen zhengtao on 2017/6/20.
 */
public interface SecurityConstants {

    String LOGIN_URL = "/login";// 登录请求
    String LOGIN_FAILURE_URL = "/failure";// 登录失败请求
    String LOGIN_SUCCESS_URL = "/success";// 登录成功请求
    String ACCOUNT_KEY = "security.account";// 用户名标识
    String PASSWORD_KEY = "security.password";// 密码标识
    String DEFAULT_ROLE = "USER";// 系统使用者

}

package com.carrier.ahu.security.permission;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Permissions are configured in json file.
 * 
 * Created by Braden Zhou on 2018/04/26.
 */
public class PermissionProvider {

    private static final String PERMISSION_FILE = "permission.json";
    private static final String ROLE_ANONYMOUS = "anonymous";

    private static List<RolePermission> rolePermissions;
    private static RolePermission anonymousPermission;

    static {
        loadRolePermissions();
        initAnonymousPermission();
    }

    private static void loadRolePermissions() {
        InputStream is = PermissionProvider.class.getClassLoader().getResourceAsStream(PERMISSION_FILE);
        rolePermissions = new Gson().fromJson(new InputStreamReader(is, Charset.forName("UTF-8")),
                new TypeToken<List<RolePermission>>() {
                }.getType());
    }

    public static RolePermission getRolePermission(String role) {
        if (rolePermissions != null) {
            for (RolePermission rolePermission : rolePermissions) {
                if (rolePermission.getRole().equals(role)) {
                    return rolePermission;
                }
            }
        }
        return anonymousPermission;
    }

    private static void initAnonymousPermission() {
        anonymousPermission = getRolePermission(ROLE_ANONYMOUS);
        if (anonymousPermission == null) {
            Ability denyAll = new Ability();
            denyAll.setAction(Ability.ALL);
            denyAll.setSubject(Ability.ALL);
            denyAll.setResource(Ability.RESOURCE_ALL);

            List<Ability> deny = new ArrayList<Ability>();
            deny.add(denyAll);

            Permission permission = new Permission();
            permission.setDeny(deny);
            permission.setAllow(new ArrayList<Ability>());

            anonymousPermission = new RolePermission();
            anonymousPermission.setRole(ROLE_ANONYMOUS);
            anonymousPermission.setPermission(permission);
        }
    }

}

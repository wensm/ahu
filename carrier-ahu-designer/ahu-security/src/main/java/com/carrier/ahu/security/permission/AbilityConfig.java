package com.carrier.ahu.security.permission;

import org.springframework.security.access.ConfigAttribute;

/**
 * Created by Braden Zhou on 2018/04/26.
 */
@SuppressWarnings("serial")
public class AbilityConfig implements ConfigAttribute {

    private String attribute;
    private boolean inverted;

    public AbilityConfig(String attribute) {
        this(attribute, false);
    }

    public AbilityConfig(String attribute, boolean inverted) {
        this.attribute = attribute;
        this.inverted = inverted;
    }

    @Override
    public String getAttribute() {
        return this.attribute;
    }

    public boolean isInverted() {
        return inverted;
    }

}

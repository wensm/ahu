CREATE TABLE `p_project` (
  `pid` varchar(38) NOT NULL,
  `no` varchar(16) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(80) DEFAULT NULL,
  `contract` varchar(20) DEFAULT NULL,
  `saler` varchar(20) DEFAULT NULL,
  `inputdate` timestamp NULL DEFAULT NULL,
  `memo` varchar(250) DEFAULT NULL,
  `pricebase` varchar(50) DEFAULT NULL,
  `customername` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  UNIQUE KEY `pid_UNIQUE` (`pid`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


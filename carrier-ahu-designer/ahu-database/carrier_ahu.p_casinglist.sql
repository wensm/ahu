CREATE TABLE `p_casinglist` (
  `PartName` varchar(30) DEFAULT NULL,
  `PartLM` smallint(5) DEFAULT NULL,
  `PartWM` smallint(5) DEFAULT NULL,
  `Quantity` double(24,2) DEFAULT NULL,
  `Memo` varchar(20) DEFAULT NULL,
  `PID` varchar(38) DEFAULT NULL,
  `UnitID` varchar(38) DEFAULT NULL,
  `GroupID` smallint(5) DEFAULT NULL,
  `Valid` smallint(5) DEFAULT NULL,
  `PANELPRO` varchar(255) DEFAULT NULL,
  `IGROUP` int(10) DEFAULT NULL,
  KEY `GroupID` (`GroupID`),
  KEY `pid` (`PID`),
  KEY `unitid` (`UnitID`),
  KEY `Valid` (`Valid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `p_standardunit` (
  `Pid` varchar(38) DEFAULT NULL,
  `UnitId` varchar(38) NOT NULL,
  `FTreeCode` varchar(2) DEFAULT NULL,
  `STreeCode` varchar(4) DEFAULT NULL,
  `TTreeCode` varchar(4) DEFAULT NULL,
  `FTreeName` varchar(50) DEFAULT NULL,
  `FTreeMemo` varchar(255) DEFAULT NULL,
  KEY `FTreeCode` (`FTreeCode`),
  KEY `Pid` (`Pid`),
  KEY `STreeCode` (`STreeCode`),
  KEY `TTreeCode` (`TTreeCode`),
  KEY `UnitId` (`UnitId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


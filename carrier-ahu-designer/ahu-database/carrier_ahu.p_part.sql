CREATE TABLE `p_part` (
  `pid` varchar(38) DEFAULT NULL,
  `unitid` varchar(38) DEFAULT NULL,
  `partid` varchar(38) NOT NULL,
  `orders` smallint(5) DEFAULT NULL,
  `types` smallint(5) DEFAULT NULL,
  `position` smallint(5) DEFAULT NULL,
  `setup` smallint(5) DEFAULT NULL,
  `pic` varchar(10) DEFAULT NULL,
  `groupi` smallint(5) DEFAULT NULL,
  `COST` double(24,2) DEFAULT NULL,
  `AVDIRECTION` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`partid`),
  KEY `partid` (`partid`),
  KEY `pid` (`pid`),
  KEY `unitid` (`unitid`),
  CONSTRAINT `f_pid` FOREIGN KEY (`pid`) REFERENCES `p_project` (`pid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `f_uid` FOREIGN KEY (`unitid`) REFERENCES `p_unit` (`unitid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `p_unit` (
  `pid` varchar(38) DEFAULT NULL,
  `unitid` varchar(38) NOT NULL,
  `series` varchar(3) DEFAULT NULL,
  `product` varchar(20) DEFAULT NULL,
  `mount` smallint(5) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `weight` double(24,2) DEFAULT NULL,
  `price` double(24,2) DEFAULT NULL,
  `ModifiedNo` varchar(10) DEFAULT NULL,
  `paneltype` varchar(1) DEFAULT NULL,
  `nsprice` double(24,2) DEFAULT NULL,
  `COST` double(24,2) DEFAULT NULL,
  `LB` double(24,2) DEFAULT NULL,
  `DRAWING` longblob,
  PRIMARY KEY (`unitid`),
  KEY `pid` (`pid`),
  KEY `unitid` (`unitid`),
  CONSTRAINT `project` FOREIGN KEY (`pid`) REFERENCES `p_project` (`pid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


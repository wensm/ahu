CREATE TABLE `p_panelline` (
  `pid` varchar(38) DEFAULT NULL,
  `unitid` varchar(38) DEFAULT NULL,
  `sideid` smallint(5) DEFAULT NULL,
  `groupid` smallint(5) DEFAULT NULL,
  `vh` smallint(5) DEFAULT NULL,
  `x1` smallint(5) DEFAULT NULL,
  `y1` smallint(5) DEFAULT NULL,
  `x2` smallint(5) DEFAULT NULL,
  `y2` smallint(5) DEFAULT NULL,
  `ioption` smallint(5) DEFAULT NULL,
  `ipaneloption` smallint(5) DEFAULT NULL,
  `sideindex` smallint(5) DEFAULT NULL,
  `sidetype` smallint(5) DEFAULT NULL,
  `sMemo` varchar(50) DEFAULT NULL,
  `sIndex` varchar(5) DEFAULT NULL,
  `iIsLastV` smallint(5) DEFAULT NULL,
  `iTurn` smallint(5) DEFAULT NULL,
  `PANELPROPERTIES` varchar(3) DEFAULT NULL,
  `LINEOPTION` varchar(1) DEFAULT NULL,
  `SEQ` int(10) DEFAULT NULL,
  KEY `groupid` (`groupid`),
  KEY `pid` (`pid`),
  KEY `sideid` (`sideid`),
  KEY `unitid` (`unitid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


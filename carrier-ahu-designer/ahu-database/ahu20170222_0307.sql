-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: 20170222
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `p_casinglist`
--

DROP TABLE IF EXISTS `p_casinglist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_casinglist` (
  `PartName` varchar(30) DEFAULT NULL,
  `PartLM` smallint(5) DEFAULT NULL,
  `PartWM` smallint(5) DEFAULT NULL,
  `Quantity` double(24,2) DEFAULT NULL,
  `Memo` varchar(20) DEFAULT NULL,
  `PID` varchar(38) DEFAULT NULL,
  `UnitID` varchar(38) DEFAULT NULL,
  `GroupID` smallint(5) DEFAULT NULL,
  `Valid` smallint(5) DEFAULT NULL,
  `PANELPRO` varchar(255) DEFAULT NULL,
  `IGROUP` int(10) DEFAULT NULL,
  KEY `GroupID` (`GroupID`),
  KEY `pid` (`PID`),
  KEY `unitid` (`UnitID`),
  KEY `Valid` (`Valid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_casinglist`
--

LOCK TABLES `p_casinglist` WRITE;
/*!40000 ALTER TABLE `p_casinglist` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_casinglist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_combinedfilter4c`
--

DROP TABLE IF EXISTS `p_combinedfilter4c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_combinedfilter4c` (
  `PID` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `FilterF` varchar(1) DEFAULT NULL,
  `LMaterialE` varchar(1) DEFAULT NULL,
  `RMaterialE` varchar(1) DEFAULT NULL,
  `LRimMaterial` varchar(1) DEFAULT NULL,
  `RRimMaterial` varchar(1) DEFAULT NULL,
  `RimThick` varchar(1) DEFAULT NULL,
  `PlankM` varchar(1) DEFAULT NULL,
  `FrameM` varchar(1) DEFAULT NULL,
  `LSupplier` varchar(1) DEFAULT NULL,
  `RSupplier` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `LResistance` double DEFAULT NULL,
  `RResistance` double DEFAULT NULL,
  `ResiFlag` varchar(1) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `NoStandard` varchar(1) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `LNoSFilter` varchar(1) DEFAULT NULL,
  `RNoSFilter` varchar(1) DEFAULT NULL,
  `YCJCHK` bit(1) NOT NULL,
  `YCJTYPE` varchar(1) DEFAULT NULL,
  `YCJCHK1` bit(1) NOT NULL,
  `YCJTYPE1` varchar(1) DEFAULT NULL,
  `LOADING` varchar(1) DEFAULT NULL,
  `STANDARD` varchar(1) DEFAULT NULL,
  KEY `PID` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_combinedfilter4c`
--

LOCK TABLES `p_combinedfilter4c` WRITE;
/*!40000 ALTER TABLE `p_combinedfilter4c` DISABLE KEYS */;
INSERT INTO `p_combinedfilter4c` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{431EC6ED-9003-49E1-A591-5C99C26FF7CE}','1','4','8','1','1','1','2','2','A','A',6,161.3,233.2,'1',52.891438,7981.719908,'0','','0','0','\0','X','\0','X','F','E');
/*!40000 ALTER TABLE `p_combinedfilter4c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_drystream4h`
--

DROP TABLE IF EXISTS `p_drystream4h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_drystream4h` (
  `PID` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SHumidificationQ` double DEFAULT NULL,
  `WHumidificationQ` double DEFAULT NULL,
  `ControlM` varchar(1) DEFAULT NULL,
  `hTypes` varchar(1) DEFAULT NULL,
  `Supplier` varchar(1) DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `SVaporPressure` double DEFAULT NULL,
  `WVaporPressure` double DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `NoStandard` varchar(1) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `Season` varchar(1) DEFAULT NULL,
  `DRAINPAN` varchar(255) DEFAULT NULL,
  KEY `PID` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_drystream4h`
--

LOCK TABLES `p_drystream4h` WRITE;
/*!40000 ALTER TABLE `p_drystream4h` DISABLE KEYS */;
INSERT INTO `p_drystream4h` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{C14B5B42-FFAE-41DA-89DE-3FF2BC48A56C}',15.5,14.8,92.881928,35,20.5,25.98162,0,156,'A','3','B',27,19.511719,50,35,25.1,45.088032,0,0.2,6,1,32.5,8286.031251,'0','A*%$@#!1*A01                              0                                                                                                                                                                                                                    ','2','0');
/*!40000 ALTER TABLE `p_drystream4h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_eheat4g`
--

DROP TABLE IF EXISTS `p_eheat4g`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_eheat4g` (
  `PID` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SHeatQ` double DEFAULT NULL,
  `WHeatQ` double DEFAULT NULL,
  `RowNum` varchar(1) DEFAULT NULL,
  `GroupC` varchar(1) DEFAULT NULL,
  `BracketM` varchar(1) DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `NoStandard` varchar(1) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `Season` varchar(1) DEFAULT NULL,
  `COILM` varchar(1) DEFAULT NULL,
  KEY `PID` (`PID`),
  KEY `RowNum` (`RowNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_eheat4g`
--

LOCK TABLES `p_eheat4g` WRITE;
/*!40000 ALTER TABLE `p_eheat4g` DISABLE KEYS */;
INSERT INTO `p_eheat4g` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{2A68CC1F-B92F-49AE-A410-EE794E9BA99D}',30,19.5,37.329058,15,6.7,25.67916,0,81.2048,'2','3','2',27,19.5,49.962483,27,12.1,12.275812,3,33,98.473857,13768,'0','A*%$@#!1*A0                                                                                                                                                                                                                                                    ','2','2');
/*!40000 ALTER TABLE `p_eheat4g` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_favorite`
--

DROP TABLE IF EXISTS `p_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_favorite` (
  `Pid` varchar(38) DEFAULT NULL,
  `UnitId` varchar(38) NOT NULL,
  `TTreeCode` varchar(4) DEFAULT NULL,
  `FTreeName` varchar(50) DEFAULT NULL,
  `FTreeMemo` varchar(255) DEFAULT NULL,
  KEY `Pid` (`Pid`),
  KEY `TTreeCode` (`TTreeCode`),
  KEY `UnitId` (`UnitId`),
  CONSTRAINT `pid` FOREIGN KEY (`Pid`) REFERENCES `p_project` (`pid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `uid` FOREIGN KEY (`UnitId`) REFERENCES `p_unit` (`unitid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_favorite`
--

LOCK TABLES `p_favorite` WRITE;
/*!40000 ALTER TABLE `p_favorite` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_mixed4a`
--

DROP TABLE IF EXISTS `p_mixed4a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_mixed4a` (
  `PID` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `MixF` varchar(5) DEFAULT NULL,
  `AirH` varchar(1) DEFAULT NULL,
  `AInterfaceR` double DEFAULT NULL,
  `DoorO` varchar(1) DEFAULT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SNewDryBulbT` double DEFAULT NULL,
  `SNewWetBulbT` double DEFAULT NULL,
  `SNewRelativeT` double DEFAULT NULL,
  `WNewDryBulbT` double DEFAULT NULL,
  `WNewWetBulbT` double DEFAULT NULL,
  `WNewRelativeT` double DEFAULT NULL,
  `NARatio` double DEFAULT NULL,
  `NAVolume` double DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `NoStandard` varchar(1) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `Season` varchar(1) DEFAULT NULL,
  `DOORTYPE` varchar(1) DEFAULT NULL,
  `DOORDIRECTION` varchar(1) DEFAULT NULL,
  `VAVLEM` varchar(1) DEFAULT NULL,
  `UVC` bit(1) NOT NULL,
  `ACTUATOR` varchar(1) DEFAULT NULL,
  `FAC` bit(1) NOT NULL,
  KEY `PID` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_mixed4a`
--

LOCK TABLES `p_mixed4a` WRITE;
/*!40000 ALTER TABLE `p_mixed4a` DISABLE KEYS */;
INSERT INTO `p_mixed4a` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{18B6875A-2011-456C-AB4E-C895C3887A45}','11000','A',10,'1',25,17.9,49.987171,27,19.5,49.962483,34,28.2,64.700312,-4,-7.1,30.487298,20,4000,26.8,20.3,55.594884,27,19.5,49.962483,8,11,80.68,4264.9598,'0','','1','1','2','1','\0','1','\0');
/*!40000 ALTER TABLE `p_mixed4a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_nonstd`
--

DROP TABLE IF EXISTS `p_nonstd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_nonstd` (
  `PID` varchar(38) NOT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `SEQ` int(10) NOT NULL,
  KEY `pid` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_nonstd`
--

LOCK TABLES `p_nonstd` WRITE;
/*!40000 ALTER TABLE `p_nonstd` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_nonstd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_panelline`
--

DROP TABLE IF EXISTS `p_panelline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_panelline` (
  `pid` varchar(38) DEFAULT NULL,
  `unitid` varchar(38) DEFAULT NULL,
  `sideid` smallint(5) DEFAULT NULL,
  `groupid` smallint(5) DEFAULT NULL,
  `vh` smallint(5) DEFAULT NULL,
  `x1` smallint(5) DEFAULT NULL,
  `y1` smallint(5) DEFAULT NULL,
  `x2` smallint(5) DEFAULT NULL,
  `y2` smallint(5) DEFAULT NULL,
  `ioption` smallint(5) DEFAULT NULL,
  `ipaneloption` smallint(5) DEFAULT NULL,
  `sideindex` smallint(5) DEFAULT NULL,
  `sidetype` smallint(5) DEFAULT NULL,
  `sMemo` varchar(50) DEFAULT NULL,
  `sIndex` varchar(5) DEFAULT NULL,
  `iIsLastV` smallint(5) DEFAULT NULL,
  `iTurn` smallint(5) DEFAULT NULL,
  `PANELPROPERTIES` varchar(3) DEFAULT NULL,
  `LINEOPTION` varchar(1) DEFAULT NULL,
  `SEQ` int(10) DEFAULT NULL,
  KEY `groupid` (`groupid`),
  KEY `pid` (`pid`),
  KEY `sideid` (`sideid`),
  KEY `unitid` (`unitid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_panelline`
--

LOCK TABLES `p_panelline` WRITE;
/*!40000 ALTER TABLE `p_panelline` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_panelline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_part`
--

DROP TABLE IF EXISTS `p_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_part` (
  `pid` varchar(38) DEFAULT NULL,
  `unitid` varchar(38) DEFAULT NULL,
  `partid` varchar(38) NOT NULL,
  `orders` smallint(5) DEFAULT NULL,
  `types` smallint(5) DEFAULT NULL,
  `position` smallint(5) DEFAULT NULL,
  `setup` smallint(5) DEFAULT NULL,
  `pic` varchar(10) DEFAULT NULL,
  `groupi` smallint(5) DEFAULT NULL,
  `COST` double(24,2) DEFAULT NULL,
  `AVDIRECTION` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`partid`),
  KEY `partid` (`partid`),
  KEY `pid` (`pid`),
  KEY `unitid` (`unitid`),
  CONSTRAINT `f_pid` FOREIGN KEY (`pid`) REFERENCES `p_project` (`pid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `f_uid` FOREIGN KEY (`unitid`) REFERENCES `p_unit` (`unitid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_part`
--

LOCK TABLES `p_part` WRITE;
/*!40000 ALTER TABLE `p_part` DISABLE KEYS */;
INSERT INTO `p_part` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{180E3250-98EA-4CCC-8F98-0945EE6D6200}',11,13,0,1,'M',4,3679.76,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{18B6875A-2011-456C-AB4E-C895C3887A45}',1,1,0,1,'a11000',1,1044.83,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{19F5A474-3397-4A52-BA10-3CA3DCF2ABC3}',4,6,0,1,'F',2,0.00,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{2A68CC1F-B92F-49AE-A410-EE794E9BA99D}',5,7,0,1,'G',2,0.00,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{32236410-03AA-4B2C-8040-0E9547A8CD92}',6,15,0,1,'O',2,0.00,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{431EC6ED-9003-49E1-A591-5C99C26FF7CE}',2,3,0,1,'C',1,1977.09,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{5EB75305-E93B-49CB-A7F7-2F3837F438B2}',14,14,0,1,'n2',5,522.42,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{6981254A-A6FA-4B88-9EA4-5551E4AF557E}',7,5,0,1,'E',3,3381.21,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{A14CC26E-718F-4A15-8FEE-B6CB11E324EF}',12,18,0,1,'R',4,0.00,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{A36D4D38-2D9F-4EAA-A8E0-1881426C9705}',13,22,0,1,'vv',5,6631.12,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{B17725E0-CB5B-42ED-9D8D-4A012288151C}',3,16,0,1,'P',2,0.00,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{C14B5B42-FFAE-41DA-89DE-3FF2BC48A56C}',8,8,0,1,'H',3,1917.14,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{D466D8AE-87F8-489E-9D2E-044A558B8E24}',10,15,0,1,'O',4,0.00,1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','{FAD6E04E-D7B6-47B3-932C-68819BB07887}',9,11,0,1,'k_BHF',3,6435.73,1);
/*!40000 ALTER TABLE `p_part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_partd`
--

DROP TABLE IF EXISTS `p_partd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_partd` (
  `PID` varchar(38) DEFAULT NULL,
  `partID` varchar(38) NOT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `season` varchar(1) DEFAULT NULL,
  `RunWeight` double DEFAULT NULL,
  `WTray` varchar(1) DEFAULT NULL,
  `BreakwaterResi` double DEFAULT NULL,
  `AluminumFM` varchar(1) DEFAULT NULL,
  `HeaderM` varchar(1) DEFAULT NULL,
  `Flange` varchar(1) DEFAULT NULL,
  `BracketM` varchar(1) DEFAULT NULL,
  `BreakwaterC` varchar(1) DEFAULT NULL,
  `SColdQ` double DEFAULT NULL,
  `WColdQ` double DEFAULT NULL,
  `SInflowT` double DEFAULT NULL,
  `WInflowT` double DEFAULT NULL,
  `SWTAscend` double DEFAULT NULL,
  `WWTAscend` double DEFAULT NULL,
  `RowNum` varchar(2) DEFAULT NULL,
  `Loop` varchar(1) DEFAULT NULL,
  `SegmentP` varchar(2) DEFAULT NULL,
  `SWaterResistance` double DEFAULT NULL,
  `WWaterResistance` double DEFAULT NULL,
  `SWaterflow` double DEFAULT NULL,
  `WWaterflow` double DEFAULT NULL,
  `SAirResistance` double DEFAULT NULL,
  `WAirResistance` double DEFAULT NULL,
  `SSensibleHeat` double DEFAULT NULL,
  `WSensibleHeat` double DEFAULT NULL,
  `FluidIDC` smallint(5) DEFAULT NULL,
  `CONCC` double(24,2) DEFAULT NULL,
  `FluidIDH` smallint(5) DEFAULT NULL,
  `CONCH` double(24,2) DEFAULT NULL,
  `COILFRAME` varchar(1) DEFAULT NULL,
  `WATERSAVE` varchar(1) DEFAULT NULL,
  `WATERSAVENUM` smallint(5) DEFAULT NULL,
  `DRAINPANTYPE` varchar(1) DEFAULT NULL,
  `COILTYPE` varchar(1) DEFAULT NULL,
  `DIA` varchar(1) DEFAULT NULL,
  `CONDEAT` double DEFAULT NULL,
  `SCT` double DEFAULT NULL,
  `SST` double DEFAULT NULL,
  `ACP` double DEFAULT NULL,
  `SUPERHEAT` double DEFAULT NULL,
  `REG` smallint(5) DEFAULT NULL,
  `CDUTYPE` varchar(255) DEFAULT NULL,
  `SBRAVE` double DEFAULT NULL,
  `WBRAVE` double DEFAULT NULL,
  KEY `FluidID` (`FluidIDC`),
  KEY `FluidIDC` (`FluidIDH`),
  KEY `PID` (`PID`),
  KEY `RowNum` (`RowNum`),
  KEY `WATERSAVENUM` (`WATERSAVENUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_partd`
--

LOCK TABLES `p_partd` WRITE;
/*!40000 ALTER TABLE `p_partd` DISABLE KEYS */;
INSERT INTO `p_partd` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{A14CC26E-718F-4A15-8FEE-B6CB11E324EF}',5,0,138,'0',27,19.5,50,15,6.6,25,15.2,14.3,91,48.5,19.7,4,'','3',166,'2',0,'0','0','0','2','2',103.4,205.89,7,60,5,10,'4','F','10',12.7,12.2,4.94,5,103.5,83.7,81.7,0,1,1.00,1,0.00,'1','0',0,'1','1','1',0,0,0,0,0,1,'',1.1,1.1),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{17AA6FEA-BB4C-45FE-8074-C75D9FC95140}',5,0,0,'0',27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,'','1',0,'2',0,'0','0','0','1','',0,0,0,0,0,0,'A','A','A',0,0,0,0,0,0,0,0,0,0.00,0,0.00,'1','0',0,'','1','1',0,0,0,0,0,1,'',0,0),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{4ED2ADF4-DF70-4970-9A39-5E754CC8C6E7}',5,0,0,'0',27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,'','1',0,'2',0,'0','0','0','1','',0,0,0,0,0,0,'A','A','A',0,0,0,0,0,0,0,0,0,0.00,0,0.00,'1','0',0,'','1','1',0,0,0,0,0,1,'',0,0),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{75412EE5-4D15-4687-9452-FB1EFAF921DE}',5,0,0,'0',27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,'','1',0,'2',0,'0','0','0','1','',0,0,0,0,0,0,'A','A','A',0,0,0,0,0,0,0,0,0,0.00,0,0.00,'1','0',0,'','1','1',0,0,0,0,0,1,'',0,0),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{B17725E0-CB5B-42ED-9D8D-4A012288151C}',6,0,143,'0',26.8,20.3,56,15,6.7,26,15.5,14.8,93,48.5,19.8,4,'','3',172,'2',0,'0','0','0','2','2',113.2,205.89,7,60,5,10,'4','F','10',15.1,12.2,5.39,5,108.1,83.7,78.7,0,1,1.00,1,0.00,'1','0',0,'1','1','1',0,0,0,0,0,1,'',1.2,1.1);
/*!40000 ALTER TABLE `p_partd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_parte`
--

DROP TABLE IF EXISTS `p_parte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_parte` (
  `PID` varchar(38) DEFAULT NULL,
  `partID` varchar(38) NOT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `season` varchar(1) DEFAULT NULL,
  `RunWeight` double DEFAULT NULL,
  `SHeatQ` double DEFAULT NULL,
  `WHeatQ` double DEFAULT NULL,
  `SInflowT` double DEFAULT NULL,
  `WInflowT` double DEFAULT NULL,
  `SWTAscend` double DEFAULT NULL,
  `WWTAscend` double DEFAULT NULL,
  `RowNum` varchar(1) DEFAULT NULL,
  `Loop` varchar(1) DEFAULT NULL,
  `SegmentP` varchar(2) DEFAULT NULL,
  `SWaterResistance` double DEFAULT NULL,
  `WWaterResistance` double DEFAULT NULL,
  `SWaterflow` double DEFAULT NULL,
  `WWaterflow` double DEFAULT NULL,
  `AirResistance` double DEFAULT NULL,
  `AluminumFM` varchar(1) DEFAULT NULL,
  `HeaderM` varchar(1) DEFAULT NULL,
  `Flange` varchar(1) DEFAULT NULL,
  `BracketM` varchar(1) DEFAULT NULL,
  `FluidIDC` smallint(5) DEFAULT NULL,
  `CONCC` double(24,2) DEFAULT NULL,
  `FluidIDH` smallint(5) DEFAULT NULL,
  `CONCH` double(24,2) DEFAULT NULL,
  `COILFRAME` varchar(1) DEFAULT NULL,
  `WATERSAVE` varchar(1) DEFAULT NULL,
  `WATERSAVENUM` smallint(5) DEFAULT NULL,
  `FROSTP` bit(1) NOT NULL,
  `DIA` varchar(1) DEFAULT NULL,
  `SBRAVE` double DEFAULT NULL,
  `WBRAVE` double DEFAULT NULL,
  KEY `PID` (`PID`),
  KEY `RowNum` (`RowNum`),
  KEY `WATERSAVENUM` (`WATERSAVENUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_parte`
--

LOCK TABLES `p_parte` WRITE;
/*!40000 ALTER TABLE `p_parte` DISABLE KEYS */;
INSERT INTO `p_parte` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{6981254A-A6FA-4B88-9EA4-5551E4AF557E}',5,16070.51577,139,'0',15.5,14.8,93,8,3.5,46,15.5,14.8,93,50,20.5,4,'','2',167,0,257.1,7,60,0,10,'4','F','12',0,18.8,0,6.25,99.2,'0','0','0','2',1,1.00,1,1.00,'1','0',0,'\0','1',0,1.36),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{4BB5C2C0-43F8-4B57-BEE0-35632942CC90}',3,0,0,'0',27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,'','2',0,0,0,0,0,0,0,'A','A','A',0,0,0,0,0,'0','0','0','1',0,0.00,0,0.00,'1','0',0,'\0','1',0,0),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{75412EE5-4D15-4687-9452-FB1EFAF921DE}',3,0,0,'0',27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,'','2',0,0,0,0,0,0,0,'A','A','A',0,0,0,0,0,'0','0','0','1',0,0.00,0,0.00,'1','0',0,'\0','1',0,0);
/*!40000 ALTER TABLE `p_parte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_parti`
--

DROP TABLE IF EXISTS `p_parti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_parti` (
  `pid` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `SHumidificationQ` double DEFAULT NULL,
  `WHumidificationQ` double DEFAULT NULL,
  `Supplier` varchar(1) DEFAULT NULL,
  `Typer` varchar(1) DEFAULT NULL,
  `RunWeight` double DEFAULT NULL,
  `Material` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `season` varchar(1) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_parti`
--

LOCK TABLES `p_parti` WRITE;
/*!40000 ALTER TABLE `p_parti` DISABLE KEYS */;
INSERT INTO `p_parti` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8418208D-9FE0-4A9A-AC06-1209551D3800}',27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,0,0,'B','2',0,'2',0,0,0,5,'0','1',''),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{B17725E0-CB5B-42ED-9D8D-4A012288151C}',15.5,14.8,92.881928,48.5,29.394836,25,0,0,0,38.82605,29.299639,50,0,94,'B','4',92,'2',0,66,20257,53,'0','2','');
/*!40000 ALTER TABLE `p_parti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_partj`
--

DROP TABLE IF EXISTS `p_partj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_partj` (
  `pid` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `SHumidificationQ` double DEFAULT NULL,
  `WHumidificationQ` double DEFAULT NULL,
  `Supplier` varchar(1) DEFAULT NULL,
  `RunWeight` double DEFAULT NULL,
  `Material` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `season` varchar(1) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `ControlM` varchar(1) DEFAULT NULL,
  `NozzleN` varchar(1) DEFAULT NULL,
  `aperture` varchar(1) DEFAULT NULL,
  `Eliminator` varchar(1) DEFAULT NULL,
  `EResistance` double DEFAULT NULL,
  `FrameM` varchar(1) DEFAULT NULL,
  `vSpray` double DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_partj`
--

LOCK TABLES `p_partj` WRITE;
/*!40000 ALTER TABLE `p_partj` DISABLE KEYS */;
INSERT INTO `p_partj` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{A14CC26E-718F-4A15-8FEE-B6CB11E324EF}',15.2,14.3,91.048534,48.5,19.7,3.581357,0,0,0,26.967468,19.475042,50,0,206,'B',66,'2',6,44,1241,5,'0','3','','A','5','3','0',89,'2',550),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{DD72777E-0410-4E67-BCFE-8F091AFA1BD2}',27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,0,0,'B',0,'2',6,0,0,5,'0','1','','A','1','1','D',5,'2',0);
/*!40000 ALTER TABLE `p_partj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_partk`
--

DROP TABLE IF EXISTS `p_partk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_partk` (
  `pid` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `WMStatus` varchar(1) DEFAULT NULL,
  `WMType` varchar(1) DEFAULT NULL,
  `AirFlowO` varchar(1) DEFAULT NULL,
  `BeltWP` varchar(1) DEFAULT NULL,
  `MLevel` varchar(1) DEFAULT NULL,
  `InsulationL` varchar(1) DEFAULT NULL,
  `ProtectL` varchar(1) DEFAULT NULL,
  `StartT` varchar(1) DEFAULT NULL,
  `Power` varchar(1) DEFAULT NULL,
  `Typer` varchar(10) DEFAULT NULL,
  `WMVelocity` int(10) DEFAULT NULL,
  `DoorO` varchar(1) DEFAULT NULL,
  `OtherPressure` double DEFAULT NULL,
  `SPressure` double DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `CHKcheck` varchar(1) DEFAULT NULL,
  `Controltype` varchar(50) DEFAULT NULL,
  `Fansupplier` varchar(1) DEFAULT NULL,
  `FanFrame` varchar(2) DEFAULT NULL,
  `MOTORTYPE` varchar(1) DEFAULT NULL,
  `VOLTAGE` varchar(1) DEFAULT NULL,
  `CHKDoor` varchar(1) DEFAULT NULL,
  `DUALMOTOR` varchar(1) DEFAULT NULL,
  `SEISMIC` varchar(1) DEFAULT NULL,
  `BELTBRAND` varchar(1) DEFAULT NULL,
  `OLOCATION` varchar(1) DEFAULT NULL,
  `OUTLET` varchar(1) DEFAULT NULL,
  `MOTORLOCATION` varchar(1) DEFAULT NULL,
  `SALTSPRAY` varchar(1) DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_partk`
--

LOCK TABLES `p_partk` WRITE;
/*!40000 ALTER TABLE `p_partk` DISABLE KEYS */;
INSERT INTO `p_partk` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{FAD6E04E-D7B6-47B3-932C-68819BB07887}','4','L','B','0','4','F','C','A','J','Y160L',1821,'1',100,1307,13,415,0,0,'0','A*%$@#!1*A001                              01                              000                                                                                                                                                                                 ','B','0','0','A','1','E','0','1','0','0','1','X','A','R','0'),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{0BAE314D-C41F-4F03-A638-29C386C1DB47}','0','A','A','0','4','B','A','A','A','A',0,'1',0,0,15,0,0,0,'0','A*%$@#!1*A001                              01                              000                                                                                                                                                                                 ','B','','0','A','1','E','','0','0','0','1','X','A','R','0'),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{E803293A-75B4-48F2-9A66-337C301D87EC}','0','A','A','0','4','B','A','A','A','',0,'1',0,0,15,0,0,0,'0','A*%$@#!1*A001                              01                              000                                                                                                                                                                                 ','B','','0','A','1','E','','0','0','0','1','X','A','R','0');
/*!40000 ALTER TABLE `p_partk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_partl`
--

DROP TABLE IF EXISTS `p_partl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_partl` (
  `pid` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `SNewDryBulbT` double DEFAULT NULL,
  `SNewWetBulbT` double DEFAULT NULL,
  `SNewRelativeT` double DEFAULT NULL,
  `WNewDryBulbT` double DEFAULT NULL,
  `WNewWetBulbT` double DEFAULT NULL,
  `WNewRelativeT` double DEFAULT NULL,
  `NAVolume` double DEFAULT NULL,
  `AInterface` varchar(1) DEFAULT NULL,
  `AInterfaceR` double DEFAULT NULL,
  `OSDoor` varchar(1) DEFAULT NULL,
  `ISDoor` varchar(1) DEFAULT NULL,
  `RResistance` double DEFAULT NULL,
  `IResistance` double DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `season` varchar(1) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `ACTUATOR` varchar(1) DEFAULT NULL,
  `VALVEM` varchar(1) DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_partl`
--

LOCK TABLES `p_partl` WRITE;
/*!40000 ALTER TABLE `p_partl` DISABLE KEYS */;
INSERT INTO `p_partl` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{3A6AC028-AE35-4195-97C0-72C0DA709025}',27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,27,19.5,49.962483,34,28.2,64.700312,-4,-7.1,30.487298,0,'A',5,'1','1',0,0,12,0,0,5,'0','1','','1','');
/*!40000 ALTER TABLE `p_partl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_partm`
--

DROP TABLE IF EXISTS `p_partm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_partm` (
  `pid` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `deadening` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `CLEARSTYPE` varchar(1) DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_partm`
--

LOCK TABLES `p_partm` WRITE;
/*!40000 ALTER TABLE `p_partm` DISABLE KEYS */;
INSERT INTO `p_partm` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{180E3250-98EA-4CCC-8F98-0945EE6D6200}','1',6,83,15904.214717,22,'0','','2');
/*!40000 ALTER TABLE `p_partm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_partn`
--

DROP TABLE IF EXISTS `p_partn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_partn` (
  `PID` varchar(38) DEFAULT NULL,
  `partID` varchar(38) NOT NULL,
  `AirFlowO` varchar(1) DEFAULT NULL,
  `AInterface` varchar(1) DEFAULT NULL,
  `AInterfaceR` double DEFAULT NULL,
  `ODoor` varchar(1) DEFAULT NULL,
  `SectionL` smallint(5) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `DOORTYPE` varchar(1) DEFAULT NULL,
  `DOORDIRECTION` varchar(1) DEFAULT NULL,
  `VALVEM` varchar(1) DEFAULT NULL,
  `UVC` bit(1) NOT NULL,
  `ACTUATOR` varchar(1) DEFAULT NULL,
  KEY `partID` (`partID`),
  KEY `pid` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_partn`
--

LOCK TABLES `p_partn` WRITE;
/*!40000 ALTER TABLE `p_partn` DISABLE KEYS */;
INSERT INTO `p_partn` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{5EB75305-E93B-49CB-A7F7-2F3837F438B2}','2','A',5,'1',8,2257.940615,41,6,'0','','1','2','1','\0','1');
/*!40000 ALTER TABLE `p_partn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_parto`
--

DROP TABLE IF EXISTS `p_parto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_parto` (
  `pid` varchar(38) DEFAULT NULL,
  `partID` varchar(38) NOT NULL,
  `ODoor` varchar(1) DEFAULT NULL,
  `functionS` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `resistance` int(10) DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `season` varchar(1) DEFAULT NULL,
  `CHKDOOR` varchar(1) DEFAULT NULL,
  `UVC` bit(1) NOT NULL,
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_parto`
--

LOCK TABLES `p_parto` WRITE;
/*!40000 ALTER TABLE `p_parto` DISABLE KEYS */;
INSERT INTO `p_parto` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{D466D8AE-87F8-489E-9D2E-044A558B8E24}','1','1',6,996,21,63,'0',27,19.5,50,27,19.5,50,27,19.5,50,27,19.5,50,'A*%$@#!1*A00                                                                                                                                                                                                                                                   ','0','0','\0'),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{32236410-03AA-4B2C-8040-0E9547A8CD92}','1','0',6,0.01,1,1,'0',15.5,14.8,93,27,19.5,50,15.5,14.8,93,27,19.5,50,'A*%$@#!1*A00                                                                                                                                                                                                                                                   ','1','0','\0');
/*!40000 ALTER TABLE `p_parto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_partv`
--

DROP TABLE IF EXISTS `p_partv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_partv` (
  `PID` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `FilterF` varchar(1) DEFAULT NULL,
  `MaterialE` varchar(2) DEFAULT NULL,
  `RimMaterial` varchar(1) DEFAULT NULL,
  `PlankM` varchar(1) DEFAULT NULL,
  `FrameM` varchar(1) DEFAULT NULL,
  `Supplier` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `ResiFlag` varchar(1) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `NoStandard` varchar(1) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `NoSFilter` varchar(1) DEFAULT NULL,
  `BeginResi` double DEFAULT NULL,
  `AverResi` double DEFAULT NULL,
  `EndResi` double DEFAULT NULL,
  `YCJCHK` bit(1) NOT NULL,
  `YCJTYPE` varchar(1) DEFAULT NULL,
  `STANDARD` varchar(1) DEFAULT NULL,
  KEY `PID` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_partv`
--

LOCK TABLES `p_partv` WRITE;
/*!40000 ALTER TABLE `p_partv` DISABLE KEYS */;
INSERT INTO `p_partv` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{A36D4D38-2D9F-4EAA-A8E0-1881426C9705}','1','3','2','2','2','A',9,335.7,'1',173.121438,28660.227265,'0','','0',221.5,335.7,450,'\0','A','E');
/*!40000 ALTER TABLE `p_partv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_partw`
--

DROP TABLE IF EXISTS `p_partw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_partw` (
  `pid` varchar(38) DEFAULT NULL,
  `partID` varchar(38) NOT NULL,
  `WheelModel` varchar(1) DEFAULT NULL,
  `WheelType` varchar(1) DEFAULT NULL,
  `WheelEfficiency` double DEFAULT NULL,
  `BracketMaterial` varchar(50) DEFAULT NULL,
  `NARatio` double DEFAULT NULL,
  `NAVolume` double DEFAULT NULL,
  `SectionL` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `resistance` double DEFAULT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` int(10) DEFAULT NULL,
  `SIndoorDryBulbT` double DEFAULT NULL,
  `SIndoorWetBulbT` double DEFAULT NULL,
  `SIndoorRelativeT` double DEFAULT NULL,
  `WIndoorDryBulbT` double DEFAULT NULL,
  `WIndoorWetBulbT` double DEFAULT NULL,
  `WIndoorRelativeT` double DEFAULT NULL,
  `SExhaustDryBulbT` double DEFAULT NULL,
  `SExhaustWetBulbT` double DEFAULT NULL,
  `SExhaustRelativeT` double DEFAULT NULL,
  `WExhaustDryBulbT` double DEFAULT NULL,
  `WExhaustWetBulbT` double DEFAULT NULL,
  `WExhaustRelativeT` double DEFAULT NULL,
  `memo` varchar(50) DEFAULT NULL,
  `season` varchar(1) DEFAULT NULL,
  `EfficiencyType` varchar(50) DEFAULT NULL,
  `SQt` double DEFAULT NULL,
  `SQs` double DEFAULT NULL,
  `SQl` double DEFAULT NULL,
  `WQt` double DEFAULT NULL,
  `WQs` double DEFAULT NULL,
  `WQl` double DEFAULT NULL,
  `NotMount` varchar(50) DEFAULT NULL,
  `Wheeldia` int(10) DEFAULT NULL,
  `WHEELDEPTH` varchar(1) DEFAULT NULL,
  `PLATEWIDTH` int(10) DEFAULT NULL,
  `FIELDINSTALL` varchar(1) DEFAULT NULL,
  `COUNTRY` varchar(2) DEFAULT NULL,
  `SUPPLIER` varchar(1) DEFAULT NULL,
  `SExhaustDrop` double DEFAULT NULL,
  `WExhaustDrop` double DEFAULT NULL,
  `SOutDrop` double DEFAULT NULL,
  `WOutDrop` double DEFAULT NULL,
  `WExhaustEfficiency` double DEFAULT NULL,
  `SExhaustEfficiency` double DEFAULT NULL,
  `WOutEfficiency` double DEFAULT NULL,
  `SOutEfficiency` double DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_partw`
--

LOCK TABLES `p_partw` WRITE;
/*!40000 ALTER TABLE `p_partw` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_partw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_partx`
--

DROP TABLE IF EXISTS `p_partx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_partx` (
  `PID` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `SHumidificationQ` double DEFAULT NULL,
  `WHumidificationQ` double DEFAULT NULL,
  `Supplier` varchar(1) DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `HTypes` smallint(5) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `NoStandard` varchar(1) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `Season` varchar(1) DEFAULT NULL,
  `Material` varchar(1) DEFAULT NULL,
  KEY `PID` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_partx`
--

LOCK TABLES `p_partx` WRITE;
/*!40000 ALTER TABLE `p_partx` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_partx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_party`
--

DROP TABLE IF EXISTS `p_party`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_party` (
  `PID` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `FilterF` varchar(1) DEFAULT NULL,
  `MaterialE` varchar(2) DEFAULT NULL,
  `RimMaterial` varchar(1) DEFAULT NULL,
  `RimThick` varchar(1) DEFAULT NULL,
  `PlankM` varchar(1) DEFAULT NULL,
  `FrameM` varchar(1) DEFAULT NULL,
  `Supplier` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `ResiFlag` varchar(1) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `BeginResi` double DEFAULT NULL,
  `AverResi` double DEFAULT NULL,
  `EndResi` double DEFAULT NULL,
  KEY `PID` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_party`
--

LOCK TABLES `p_party` WRITE;
/*!40000 ALTER TABLE `p_party` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_party` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_partz`
--

DROP TABLE IF EXISTS `p_partz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_partz` (
  `pid` varchar(38) DEFAULT NULL,
  `partID` varchar(38) NOT NULL,
  `ODoor` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `CHKDOOR` varchar(1) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `resistance` int(10) DEFAULT NULL,
  `CO2` bit(1) NOT NULL,
  `TEMPSENSOR` bit(1) NOT NULL,
  `STATICSENSOR` bit(1) NOT NULL,
  `FROST` bit(1) NOT NULL,
  `LESSAIR` bit(1) NOT NULL,
  `CONTROLBOX` bit(1) NOT NULL,
  `CONTROLLER` bit(1) NOT NULL,
  `PANEL` bit(1) NOT NULL,
  `SASENSOR` bit(1) NOT NULL,
  `RASENSOR` bit(1) NOT NULL,
  `FASENSOR` bit(1) NOT NULL,
  `ACTUATORS` bit(1) NOT NULL,
  `PRESSUREGAGE` bit(1) NOT NULL,
  `VFD` bit(1) NOT NULL,
  `ASB` bit(1) NOT NULL,
  `WVD` bit(1) NOT NULL,
  `WVDNUM` int(10) DEFAULT NULL,
  `MPOWER` varchar(1) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  KEY `pid` (`pid`),
  KEY `WVDNUM` (`WVDNUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_partz`
--

LOCK TABLES `p_partz` WRITE;
/*!40000 ALTER TABLE `p_partz` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_partz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_project`
--

DROP TABLE IF EXISTS `p_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_project` (
  `pid` varchar(38) NOT NULL,
  `no` varchar(16) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(80) DEFAULT NULL,
  `contract` varchar(20) DEFAULT NULL,
  `saler` varchar(20) DEFAULT NULL,
  `inputdate` timestamp NULL DEFAULT NULL,
  `memo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  UNIQUE KEY `pid_UNIQUE` (`pid`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_project`
--

LOCK TABLES `p_project` WRITE;
/*!40000 ALTER TABLE `p_project` DISABLE KEYS */;
INSERT INTO `p_project` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','','20170227','','','','2017-02-26 16:00:00','');
/*!40000 ALTER TABLE `p_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_project_memo`
--

DROP TABLE IF EXISTS `p_project_memo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_project_memo` (
  `Pid` varchar(38) NOT NULL,
  `UnitID` varchar(38) NOT NULL,
  `Value1` varchar(50) DEFAULT NULL,
  `Memo1` varchar(50) DEFAULT NULL,
  `Value2` varchar(50) DEFAULT NULL,
  `Memo2` varchar(50) DEFAULT NULL,
  `Value3` varchar(50) DEFAULT NULL,
  `Memo3` varchar(50) DEFAULT NULL,
  `Value4` varchar(50) DEFAULT NULL,
  `Memo4` varchar(50) DEFAULT NULL,
  `Value5` varchar(50) DEFAULT NULL,
  `Memo5` varchar(50) DEFAULT NULL,
  `Value6` varchar(50) DEFAULT NULL,
  `Memo6` varchar(50) DEFAULT NULL,
  `Value7` varchar(50) DEFAULT NULL,
  `Memo7` varchar(50) DEFAULT NULL,
  `Value8` varchar(50) DEFAULT NULL,
  `Memo8` varchar(50) DEFAULT NULL,
  `Value9` varchar(50) DEFAULT NULL,
  `Memo9` varchar(50) DEFAULT NULL,
  `Value10` varchar(50) DEFAULT NULL,
  `Memo10` varchar(50) DEFAULT NULL,
  `Value11` varchar(50) DEFAULT NULL,
  `Memo11` varchar(50) DEFAULT NULL,
  `Value12` varchar(50) DEFAULT NULL,
  `Memo12` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`UnitID`,`Pid`),
  KEY `Pid` (`Pid`),
  KEY `UnitID` (`UnitID`),
  CONSTRAINT `fmpid` FOREIGN KEY (`Pid`) REFERENCES `p_project` (`pid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fmuid` FOREIGN KEY (`UnitID`) REFERENCES `p_unit` (`unitid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_project_memo`
--

LOCK TABLES `p_project_memo` WRITE;
/*!40000 ALTER TABLE `p_project_memo` DISABLE KEYS */;
INSERT INTO `p_project_memo` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','B','Unit Boxing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'30373.37','Boxing total Cost','139477.7','Boxing total price');
/*!40000 ALTER TABLE `p_project_memo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_projectoutversion`
--

DROP TABLE IF EXISTS `p_projectoutversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_projectoutversion` (
  `PID` varchar(38) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL,
  KEY `PID` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_projectoutversion`
--

LOCK TABLES `p_projectoutversion` WRITE;
/*!40000 ALTER TABLE `p_projectoutversion` DISABLE KEYS */;
INSERT INTO `p_projectoutversion` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','Version 6.4.0 (20160630)');
/*!40000 ALTER TABLE `p_projectoutversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_singlefilter4b`
--

DROP TABLE IF EXISTS `p_singlefilter4b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_singlefilter4b` (
  `PID` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `FilterF` varchar(1) DEFAULT NULL,
  `MaterialE` varchar(2) DEFAULT NULL,
  `RimMaterial` varchar(1) DEFAULT NULL,
  `RimThick` varchar(1) DEFAULT NULL,
  `PlankM` varchar(1) DEFAULT NULL,
  `FrameM` varchar(1) DEFAULT NULL,
  `Supplier` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `ResiFlag` varchar(1) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `NoStandard` varchar(1) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `NoSFilter` varchar(1) DEFAULT NULL,
  `BeginResi` double DEFAULT NULL,
  `AverResi` double DEFAULT NULL,
  `EndResi` double DEFAULT NULL,
  `YCJCHK` bit(1) NOT NULL,
  `YCJTYPE` varchar(1) DEFAULT NULL,
  `LOADING` varchar(1) DEFAULT NULL,
  `STANDARD` varchar(1) DEFAULT NULL,
  KEY `PID` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_singlefilter4b`
--

LOCK TABLES `p_singlefilter4b` WRITE;
/*!40000 ALTER TABLE `p_singlefilter4b` DISABLE KEYS */;
INSERT INTO `p_singlefilter4b` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{DF34492B-7AED-47AE-915B-6498BD1957F4}','1','F','1','1','2','2','A',6,0,'1',0,0,'0','','0',0,0,0,'\0','X','F','G');
/*!40000 ALTER TABLE `p_singlefilter4b` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_standardunit`
--

DROP TABLE IF EXISTS `p_standardunit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_standardunit` (
  `Pid` varchar(38) DEFAULT NULL,
  `UnitId` varchar(38) NOT NULL,
  `FTreeCode` varchar(2) DEFAULT NULL,
  `STreeCode` varchar(4) DEFAULT NULL,
  `TTreeCode` varchar(4) DEFAULT NULL,
  `FTreeName` varchar(50) DEFAULT NULL,
  `FTreeMemo` varchar(255) DEFAULT NULL,
  KEY `FTreeCode` (`FTreeCode`),
  KEY `Pid` (`Pid`),
  KEY `STreeCode` (`STreeCode`),
  KEY `TTreeCode` (`TTreeCode`),
  KEY `UnitId` (`UnitId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_standardunit`
--

LOCK TABLES `p_standardunit` WRITE;
/*!40000 ALTER TABLE `p_standardunit` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_standardunit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_stream4f`
--

DROP TABLE IF EXISTS `p_stream4f`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_stream4f` (
  `PID` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `SInDryBulbT` double DEFAULT NULL,
  `SInWetBulbT` double DEFAULT NULL,
  `SInRelativeT` double DEFAULT NULL,
  `WInDryBulbT` double DEFAULT NULL,
  `WInWetBulbT` double DEFAULT NULL,
  `WInRelativeT` double DEFAULT NULL,
  `Flange` varchar(1) DEFAULT NULL,
  `BracketM` varchar(1) DEFAULT NULL,
  `SOutDryBulbT` double DEFAULT NULL,
  `SOutWetBulbT` double DEFAULT NULL,
  `SOutRelativeT` double DEFAULT NULL,
  `WOutDryBulbT` double DEFAULT NULL,
  `WOutWetBulbT` double DEFAULT NULL,
  `WOutRelativeT` double DEFAULT NULL,
  `SHeatQ` double DEFAULT NULL,
  `WHeatQ` double DEFAULT NULL,
  `SVaporPressure` double DEFAULT NULL,
  `WVaporPressure` double DEFAULT NULL,
  `RowNum` varchar(1) DEFAULT NULL,
  `Material` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `NoStandard` varchar(1) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `Season` varchar(1) DEFAULT NULL,
  KEY `PID` (`PID`),
  KEY `RowNum` (`RowNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_stream4f`
--

LOCK TABLES `p_stream4f` WRITE;
/*!40000 ALTER TABLE `p_stream4f` DISABLE KEYS */;
INSERT INTO `p_stream4f` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{19F5A474-3397-4A52-BA10-3CA3DCF2ABC3}',15.5,14.8,92.881928,15,6.7,25.67916,'0','1',30,19.5,37.329058,27,12.1,12.275812,0,80.8083,0,0,'2','1',3,71,346.36496,16532,'0','A*%$@#!1*A0                                                                                                                                                                                                                                                    ','2');
/*!40000 ALTER TABLE `p_stream4f` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_unit`
--

DROP TABLE IF EXISTS `p_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_unit` (
  `pid` varchar(38) DEFAULT NULL,
  `unitid` varchar(38) NOT NULL,
  `series` varchar(3) DEFAULT NULL,
  `product` varchar(20) DEFAULT NULL,
  `mount` smallint(5) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `weight` double(24,2) DEFAULT NULL,
  `price` double(24,2) DEFAULT NULL,
  `ModifiedNo` varchar(10) DEFAULT NULL,
  `paneltype` varchar(1) DEFAULT NULL,
  `nsprice` double(24,2) DEFAULT NULL,
  `COST` double(24,2) DEFAULT NULL,
  `LB` double(24,2) DEFAULT NULL,
  `DRAWING` longblob,
  PRIMARY KEY (`unitid`),
  KEY `pid` (`pid`),
  KEY `unitid` (`unitid`),
  CONSTRAINT `project` FOREIGN KEY (`pid`) REFERENCES `p_project` (`pid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_unit`
--

LOCK TABLES `p_unit` WRITE;
/*!40000 ALTER TABLE `p_unit` DISABLE KEYS */;
INSERT INTO `p_unit` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','001','39CQ1420',1,'AHU-5-01',0.00,0.00,'59','1',0.00,0.00,0.00,NULL),('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{CF008BEC-37BD-465E-9739-D894094FAB9E}','002','AHU',1,'',0.00,0.00,'0','0',0.00,0.00,0.00,NULL);
/*!40000 ALTER TABLE `p_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_unitpro`
--

DROP TABLE IF EXISTS `p_unitpro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `p_unitpro` (
  `pid` varchar(38) NOT NULL,
  `unitid` varchar(38) NOT NULL,
  `product` varchar(20) NOT NULL,
  `sairvolume` double(24,2) DEFAULT NULL,
  `sexternalstatic` double(24,2) DEFAULT NULL,
  `smodel` varchar(1) DEFAULT NULL,
  `eairvolume` double(24,2) DEFAULT NULL,
  `eexternalstatic` double(24,2) DEFAULT NULL,
  `emodel` varchar(1) DEFAULT NULL,
  `unitconfig` varchar(1) DEFAULT NULL,
  `pipeorientation` varchar(1) DEFAULT NULL,
  `doororientation` varchar(1) DEFAULT NULL,
  `inskinm` varchar(1) DEFAULT NULL,
  `inskinw` varchar(1) DEFAULT NULL,
  `inskincolor` varchar(1) DEFAULT NULL,
  `exskinm` varchar(1) DEFAULT NULL,
  `exskinw` varchar(1) DEFAULT NULL,
  `exskincolor` varchar(1) DEFAULT NULL,
  `panelinsulation` varchar(1) DEFAULT NULL,
  `delivery` varchar(1) DEFAULT NULL,
  `voltage` varchar(1) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `groups` smallint(5) DEFAULT NULL,
  `isgroup` varchar(1) DEFAULT NULL,
  `isemodel` smallint(5) DEFAULT NULL,
  `indirection` varchar(1) DEFAULT NULL,
  `ispanel` varchar(1) DEFAULT NULL,
  `isV1` varchar(1) DEFAULT NULL,
  `BASETYPE` varchar(1) DEFAULT NULL,
  `ISPRERAIN` bit(1) NOT NULL,
  `ATTITUDE` double DEFAULT NULL,
  PRIMARY KEY (`pid`,`unitid`),
  KEY `pid` (`pid`),
  KEY `unitid` (`unitid`),
  CONSTRAINT `fpid` FOREIGN KEY (`pid`) REFERENCES `p_project` (`pid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fuid` FOREIGN KEY (`unitid`) REFERENCES `p_unit` (`unitid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_unitpro`
--

LOCK TABLES `p_unitpro` WRITE;
/*!40000 ALTER TABLE `p_unitpro` DISABLE KEYS */;
INSERT INTO `p_unitpro` VALUES ('{46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}','{8999461E-20AC-44DB-AE04-472D7E9855EE}','AHU39CBF',5.56,400.00,'l',5.56,400.00,'l','H','R','R','1','3','W','4','3','W','3','1','0','',5,'1',0,'0','0','','2','',0);
/*!40000 ALTER TABLE `p_unitpro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_ahuinfo`
--

DROP TABLE IF EXISTS `s_ahuinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_ahuinfo` (
  `InfoName` varchar(50) DEFAULT NULL,
  `InfoValue` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_ahuinfo`
--

LOCK TABLES `s_ahuinfo` WRITE;
/*!40000 ALTER TABLE `s_ahuinfo` DISABLE KEYS */;
INSERT INTO `s_ahuinfo` VALUES ('language','0'),('sn','VNOKBGHSKBKO'),('passport',''),('wintertempfdb','27'),('wintertempfwb','19.5'),('wintertempfr','49.9624830891145'),('summertempfdb','27'),('summertempfwb','19.5'),('summertempfr','49.9624830891145'),('newwintertempfdb','-4'),('newwintertempfwb','-7.1'),('newwintertempfr','30.487298470811'),('newsummertempfdb','34'),('newsummertempfwb','28.2'),('newsummertempfr','64.7003116203103'),('regdate','20080701'),('maxwaterdrop','89'),('avunit','0'),('SUSAGE','0'),('DRAINPAN','2'),('HEADER','0'),('VOLTAGE','0'),('INSKINW','1'),('EXSKINW','1'),('DIANJI','B'),('S_IMPERIAL','0');
/*!40000 ALTER TABLE `s_ahuinfo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-07  0:05:36

CREATE TABLE `p_projectoutversion` (
  `PID` varchar(38) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL,
  KEY `PID` (`PID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


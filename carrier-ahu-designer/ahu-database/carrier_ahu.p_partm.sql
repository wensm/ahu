CREATE TABLE `p_partm` (
  `pid` varchar(38) DEFAULT NULL,
  `PartID` varchar(38) NOT NULL,
  `deadening` varchar(1) DEFAULT NULL,
  `SectionL` int(10) DEFAULT NULL,
  `Weight` double DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Resistance` double DEFAULT NULL,
  `nonstandard` varchar(1) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `CLEARSTYPE` varchar(1) DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


package com.carrier.ahu.metadata.entity.report;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SPaneldXtDoor extends DataEntity {

    @EntityId
    private String fanType;
    @EntityId(sequence = 1)
    private String partType;
    private String door;
    private String doorH;
    private String doorW;
    private String minSectionL;

}

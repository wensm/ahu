package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/8/29.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Noise extends DataEntity {

    @EntityId
    private String fanType;
    private int noise;

}

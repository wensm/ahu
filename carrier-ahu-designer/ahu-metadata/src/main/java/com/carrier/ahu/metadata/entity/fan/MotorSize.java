package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.ToString;

/**
 * 电机安装尺寸数据实体类
 */
@Data
@ToString
public class MotorSize extends DataEntity {

    //机座号
    @EntityId
    private String motorBaseNo;
    //电机品牌
    @EntityId(sequence = 1)
    private String supplier;
    //电机类型
    @EntityId(sequence = 2)
    private String type;
    //电机宽度
    private int motorW;
    //电机直联安装长度
    private int motorL;
}

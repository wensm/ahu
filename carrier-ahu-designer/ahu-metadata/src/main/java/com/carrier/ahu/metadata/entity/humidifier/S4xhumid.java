package com.carrier.ahu.metadata.entity.humidifier;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class S4xhumid extends DataEntity {
	
	@EntityId
    private int humidq;
    private String types;
    private int htypes;
    private double rpower;

}

package com.carrier.ahu.metadata.entity.section;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SectionLength extends DataEntity {

    @EntityId
    private String type;
    private int A;
    private int N;
    private int L;
    private int K;
    @AllowNull
    private int W1;
    private int W2;
    private int D;
    @AllowNull
    private int Z;
    @AllowNull
    private int AMIN;
    @AllowNull
    private int DIF;

}

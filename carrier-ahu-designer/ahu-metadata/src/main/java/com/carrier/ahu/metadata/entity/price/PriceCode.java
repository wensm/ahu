package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.ExcelEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class PriceCode extends ExcelEntity {

    private String no;
    private String name;
    private String description;
    private String valueType;
    private String value;
    private String valueDesp;
    private String code;
    private String group;
    private String metaKey;
    private String metaValue;

}

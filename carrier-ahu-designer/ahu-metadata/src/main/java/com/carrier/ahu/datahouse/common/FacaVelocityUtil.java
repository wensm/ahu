package com.carrier.ahu.datahouse.common;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.section.SectionArea;
import com.carrier.ahu.vo.SystemCalculateConstants;

public class FacaVelocityUtil {

    /**
     * 冷水盘管、热水盘管、直接蒸发式盘管
     *
     * @param airVolume    风量
     * @param serial       机组型号
     * @param tubeDiameter 管径
     * @param sectionType  功能段类型
     * @return 迎面风速
     */
    public static double getCoil(double airVolume, String serial, String tubeDiameter, String sectionType) {
        SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class, serial);
        double area = 0.0;
        if (SectionTypeEnum.TYPE_COLD.getId().equals(sectionType) || SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId().equals(sectionType)) {
            if (SystemCalculateConstants.COOLINGCOIL_TUBEDIAMETER_1_2.equals(tubeDiameter)) {//四分管
                area = sectionArea.getD();
            } else {
                area = sectionArea.getD1();
            }
        } else if (SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(sectionType)) {
            if (SystemCalculateConstants.COOLINGCOIL_TUBEDIAMETER_1_2.equals(tubeDiameter)) {//四分管
                area = sectionArea.getE();
            } else {
                area = sectionArea.getE1();
            }
        }
        return airVolume / 3600.0 / area;
    }

    /**
     * 电加热盘管、蒸汽盘管
     *
     * @param airVolume   风量
     * @param serial      机组型号
     * @param sectionType 功能段类型
     * @return 迎面风速
     */
    public static double getOtherCommon(double airVolume, String serial, String sectionType) {
        SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class, serial);
        double area = 0.0;
        if (SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId().equals(sectionType)) {//电加热盘管
            area = sectionArea.getG();
        } else if (SectionTypeEnum.TYPE_STEAMCOIL.getId().equals(sectionType)) {//蒸汽盘管
            area = sectionArea.getF();
        }
        return airVolume / 3600.0 / area;
    }

}

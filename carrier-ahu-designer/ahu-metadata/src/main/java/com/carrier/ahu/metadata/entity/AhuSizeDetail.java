package com.carrier.ahu.metadata.entity;

import org.apache.commons.lang3.StringUtils;

import com.carrier.ahu.util.EmptyUtil;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Ahu Size Detail data model.
 * 
 * Created by Braden Zhou on 2018/04/27.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AhuSizeDetail extends AhuSize {

    private double totalHeightOnTop;
    private String baseHeightStr;
    private int[] baseHeights;
    private double coilFinHeight;
    private double coilFinWidth;
    private double coilFaceArea;
    private double filterFaceArea;
    private double hepaFaceArea;
    private double totalDoorHeight;
    private double totalDoorWidth;
    private double clearDoorOpeningHeight;
    private double clearDoorOpeningWidth;

    public int[] getBaseHeights() {
        if (EmptyUtil.isNotEmpty(baseHeights)) {
            return baseHeights;
        }
        if (StringUtils.isEmpty(baseHeightStr)) {
            baseHeights = new int[0];
            return baseHeights;
        }
        String[] arr = baseHeightStr.trim().split("or");
        baseHeights = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            String string = arr[i];
            try {
                baseHeights[i] = Integer.parseInt(string.trim());
            } catch (Exception e) {
                baseHeights[i] = 0;
            }
        }
        return baseHeights;
    }

    @Override
    public double getArea() {
        return this.getCoilFaceArea();
    }

}

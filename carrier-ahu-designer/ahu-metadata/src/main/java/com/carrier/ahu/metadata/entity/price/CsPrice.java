package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 槽钢底座价格
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class CsPrice extends DataEntity implements NonstdPrice {

    @EntityId
    private String partNo;
    private double cost;

    @Override
    public double getTp() {
        return this.cost;
    }

    @Override
    public double getOldPrice() {
        return 0;
    }

}

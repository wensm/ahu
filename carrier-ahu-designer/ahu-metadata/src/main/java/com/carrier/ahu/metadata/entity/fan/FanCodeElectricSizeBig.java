package com.carrier.ahu.metadata.entity.fan;


import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FanCodeElectricSizeBig extends DataEntity {
    @EntityId
    private String FANTYPE;
    @EntityId(sequence = 1)
    private String FANCODE;
    private Double SIDEDISTANCESIZE;
    private Double P;
    private Integer BOTTOMLENGTH;
    private Integer FANSIZE;
    private Integer D;
    private Integer W;
}

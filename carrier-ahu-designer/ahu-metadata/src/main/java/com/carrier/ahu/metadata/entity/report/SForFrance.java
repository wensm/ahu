package com.carrier.ahu.metadata.entity.report;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2018/3/29.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SForFrance extends DataEntity {

    @EntityId
    private String unit;
    @EntityId(sequence = 1)
    private String item;
    private double value;

}

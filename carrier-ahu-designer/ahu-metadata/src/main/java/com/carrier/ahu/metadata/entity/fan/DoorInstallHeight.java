package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.ToString;

/**
 * 检修门安装高度实体类
 */
@Data
@ToString
public class DoorInstallHeight extends DataEntity {

    //风机
    @EntityId
    private String fanModel;
    //底座高度
    private int baseH;
    //减震器高度
    private int shockAbsorptionH;
    //底槽铁高度
    private int bottomChannelIronH;
    //电机底座安全距离
    private int motorBaseSafeDistance;
    //内框架距离开门位置
    private int openDoorPositon;
    //检修门安装高度
    private int doorInstallH;


}

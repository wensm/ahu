package com.carrier.ahu.metadata.entity.report;

import java.util.List;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoilInfo extends DataEntity {

    @EntityId(sequence = 0)
    private String unitModel;
    private String tId;
    private String cirNo;
    private String tLen;
    private String tNo;
    @EntityId(sequence = 1)
    private String row;
    @EntityId(sequence = 2)
    private String circuit;
    private String waterFlowrate;
    private boolean hasOther;
    private List<CoilInfo> others;

}

package com.carrier.ahu.metadata.entity.heatrecycle;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class WheelInfo extends DataEntity {

    @EntityId
    private String brand;
    @EntityId(sequence = 1)
    private int wheelDia;
    private int wheelType;

}

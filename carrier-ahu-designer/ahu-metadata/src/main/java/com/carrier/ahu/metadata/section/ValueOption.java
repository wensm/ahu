package com.carrier.ahu.metadata.section;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.util.ReflectionUtils;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.gson.annotations.Expose;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 描述一个段属性的额外信息，包含选项的列表（值，中文，英文），以及属性之间的对应关系
 */
@Data
@Slf4j
public class ValueOption implements Serializable {
	@Expose(serialize = true, deserialize = true)
	private String[] options;
	@Expose(serialize = true, deserialize = true)
	private String[] labels;
	@Expose(serialize = true, deserialize = true)
	private String[] clabels;
	// @Expose(serialize = false, deserialize = false)
	@Expose(serialize = true, deserialize = true)
	private List<InnerPair> optionPairs;
	@Expose(serialize = true, deserialize = true)
	private String[] relatedKeys;
	@Expose(serialize = true, deserialize = true)
	private Map<String, int[]> valueOptionMap;
	@Expose(serialize = true, deserialize = true)
	private String[] enableRelatedKeys;
	@Expose(serialize = true, deserialize = true)
	private Map<String, Boolean> enableValueMap;

	/*多规则属性配置*/
	@Expose(serialize = true, deserialize = true)
	private String[] relationKeys;
	@Expose(serialize = true, deserialize = true)
	private List<Map<String, int[]>> relationMap;
	@Expose(serialize = true, deserialize = true)
	private Map<String, int[]> relatedSectionOptionMap;

	public ValueOption(String paraKey, String[] options, String[] labels, String[] clabels) {
		this.options = options;
		this.labels = labels;
		this.clabels = clabels;
		this.relatedKeys = new String[0];
		this.valueOptionMap = new HashMap<String, int[]>();
		initValuePair(paraKey);
	}

	public ValueOption(String paraKey, String[] options, String[] labels, String[] clabels, String[] relatedKeys,
			Map<String, int[]> valueOptionMap) {
		this.options = options;
		this.labels = labels;
		this.clabels = clabels;
		this.relatedKeys = relatedKeys;
		this.valueOptionMap = valueOptionMap;
		initValuePair(paraKey);
	}

    public ValueOption(String paraKey, String[] options, String[] labels, String[] clabels, String[] relatedKeys,
            Map<String, int[]> valueOptionMap, String[] enableRelatedKeys, Map<String, Boolean> enableValueMap,
            String[] relationKeys, List<Map<String, int[]>> relationMap) {
        this.options = options;
        this.labels = labels;
        this.clabels = clabels;
        this.relatedKeys = relatedKeys;
        this.valueOptionMap = valueOptionMap;
        this.enableRelatedKeys = enableRelatedKeys;
        this.enableValueMap = enableValueMap;
        this.relationKeys = relationKeys;
        this.relationMap = relationMap;
        initValuePair(paraKey);
    }

    public ValueOption(String paraKey, String[] options, String[] labels, String[] clabels, String[] relatedKeys,
            Map<String, int[]> valueOptionMap, String[] enableRelatedKeys, Map<String, Boolean> enableValueMap,
            String[] relationKeys, List<Map<String, int[]>> relationMap, Map<String, int[]> relatedSectionOptionMap) {
        this.options = options;
        this.labels = labels;
        this.clabels = clabels;
        this.relatedKeys = relatedKeys;
        this.valueOptionMap = valueOptionMap;
        this.enableRelatedKeys = enableRelatedKeys;
        this.enableValueMap = enableValueMap;
        this.relationKeys = relationKeys;
        this.relationMap = relationMap;
        this.relatedSectionOptionMap = relatedSectionOptionMap;
        initValuePair(paraKey);
    }

	public void resolveValueFromConstance(String paraKey) {
		// Translate option array with SystemCalculateConstants field
		String[] ooptions = getOptions();
		String[] noptions = new String[ooptions.length];
		for (int i = 0; i < ooptions.length; i++) {
			String str = ooptions[i];
			String fieldName = MetaCodeGen.getFieldName(paraKey, str);
			String nstr = (String)ReflectionUtils.getFieldValueByName(fieldName, SystemCalculateConstants.class);
			if (EmptyUtil.isEmpty(nstr)) {
				nstr = str;
				log.warn(String.format("Failed to resolve %s in SystemCalculateConstants", fieldName));
			}
			noptions[i] = nstr;
		}
		setOptions(noptions);
	}

	public void initValuePair(String paraKey) {
		
		if (EmptyUtil.isEmpty(this.optionPairs)) {
			resolveValueFromConstance(paraKey);
			List<InnerPair> list = new ArrayList<InnerPair>();
			for (int i = 0; i < options.length; i++) {
				InnerPair pair = new InnerPair();
				pair.setClabel(clabels[i]);
				pair.setLabel(labels[i]);
				pair.setOption(options[i]);
				list.add(pair);
			}
			this.optionPairs = list;
		}
	}
}
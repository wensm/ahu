package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.ToString;

/**
 * 无蜗壳风机安装尺寸实体类
 */
@Data
@ToString
public class WwkFanSize extends DataEntity {

    //风机型号
    @EntityId
    private String fanModel;
    //风机长度Lf
    private double fanL;
    //风机底座宽度Wp
    private int fanBaseW;
}

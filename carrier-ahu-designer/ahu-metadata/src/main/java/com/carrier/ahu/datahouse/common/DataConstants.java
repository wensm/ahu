package com.carrier.ahu.datahouse.common;

/**
 * Created by Braden Zhou on 2018/04/27.
 */
public class DataConstants {

    public static final String METADATA_PACKAGE = "com.carrier.ahu.metadata";

    public static final String GET_CELL_PROCERSSOR_METHOD = "getCellProcessors";

}

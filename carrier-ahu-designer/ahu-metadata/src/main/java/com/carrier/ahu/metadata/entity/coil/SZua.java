package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/8/14.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SZua extends DataEntity {

    @EntityId
    private int row;
    private double zuaCR;
    private double zuaCF;
    private double zuaCO;
    private int tubeDia;
    private double zuaH;
    private double zua0711;

}
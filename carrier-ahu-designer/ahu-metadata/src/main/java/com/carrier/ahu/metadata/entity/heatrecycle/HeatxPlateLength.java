package com.carrier.ahu.metadata.entity.heatrecycle;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HeatxPlateLength extends DataEntity {

    @EntityId
    private String unit;
    @EntityId(sequence = 1)
    private String type;
    private int length;

}
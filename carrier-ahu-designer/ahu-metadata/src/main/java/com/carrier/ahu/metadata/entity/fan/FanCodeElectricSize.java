package com.carrier.ahu.metadata.entity.fan;


import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FanCodeElectricSize extends DataEntity {
    @EntityId
    private String FANTYPE;
    @EntityId(sequence = 1)
    private String FANCODE;
    private Integer INNERHEIGHT;
    private Integer INNERWIDTH;
    private Integer A;
    private Integer P;
    private Integer AP;
    private Integer M;
    private Integer N;
    private Integer D;
    private Integer W;
}

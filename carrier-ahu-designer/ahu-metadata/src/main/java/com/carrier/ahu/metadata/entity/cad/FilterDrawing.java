package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FilterDrawing extends DataEntity {

    @EntityId
    private String fanType;
    @EntityId(sequence = 1)
    private String filterFormat;
    private String frontDrawing;
    private String frontDimension;
    private String topDrawing;
    private String topDimension;

}

package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import com.carrier.ahu.metadata.entity.Ahu;
import com.carrier.ahu.util.EmptyUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

@Data
public class CoilAirResistance extends DataEntity {
    @EntityId
    private String season;

    @EntityId(sequence = 1)
    private String tubeDiameter;

    @EntityId(sequence = 2)
    private String finType;

    private double surf;
}

package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class NonstdCoilframe extends DataEntity {

    @EntityId
    private String serial;
    @EntityId(sequence = 1)
    private String sectionTypeNo;
    @EntityId(sequence = 2)
    private String w;
    @AllowNull
    private String h;
}

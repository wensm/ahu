package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ConverterMotorBig extends DataEntity {

    @EntityId
    private String FANTYPE;
    private String FANCODE;
    @EntityId(sequence = 1)
    private String MOTORBASENO;
    private int ABB;
    private int WOLONG;
    private int DONGYUAN;
    private int SIEMENS;
    private int GE;
    private int PTABB;
    private int PTWOLONG;
    private int PTDONGYUAN;
    private int PTGE;

}

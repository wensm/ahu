package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class STakeoversize extends DataEntity {

	@EntityId
    private String fantype;	
	@EntityId(sequence=1)
    private String section;
	@EntityId(sequence=2)
	private int flange;
	private String sizevalue;
	
	@AllowNull
    private String headsize;

}

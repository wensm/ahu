package com.carrier.ahu.metadata.entity.coil;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/7/26.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SCoilInfo1 extends SCoilInfo {
}

package com.carrier.ahu.metadata.entity.panel;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SPanelDBigOperate extends SPanelD {

}
package com.carrier.ahu.metadata;

import com.carrier.ahu.common.enums.FileType;
import com.carrier.ahu.constant.MetadataConstant;
import com.carrier.ahu.metadata.common.MetadataException;
import com.carrier.ahu.metadata.common.ReportType;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ExcelUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.carrier.ahu.constant.CommonConstant.*;

/**
 * Report metadata for each type of report.
 *
 * Created by Braden Zhou on 2018/05/10.
 */
@Slf4j
public class ReportMetadata {

	private static Map<ReportType, String[][]> reportMetadata = new HashMap<>();
	private static Map<String, String[][]> sectionConnectionMetadata = new HashMap<>();
	private static final String REPORT_PARAM_METADATA_PATH = "report/param/";
	private static final String REPORT_TECH_SPEC_METADATA_PATH = "asserts/input/reportTechspec/";
	private static final String SECTION_CONNECTION_DLJ = "DLJ";
	private static final String SECTION_CONNECTION_PREFIX = "section_connection_";

	private static final String[] INPUT_CELLS = { "D2", "E2" };//非39CQ立式
	private static final String[] INPUT_CELLS_CQ_V = { "D3", "E3" };//39CQ立式
	private static final String[] INPUT_CELLS_Q2= { "Q2" };//Q2 inskinm


	public static String[][] getReportMetadata(ReportType reportType) {
		String[][] metadata = null;
		if (reportMetadata.containsKey(reportType)) {
			metadata = reportMetadata.get(reportType);
		} else {
			metadata = loadReportMetadata(reportType);
			reportMetadata.put(reportType, metadata);
		}
		return metadata;
	}

	/**
	 * Report metadata are two dimensional string array.
	 *
	 * @param reportType
	 * @return
	 */
	private static String[][] loadReportMetadata(ReportType reportType) {
		InputStream inputStream = ReportMetadata.class
				.getResourceAsStream(REPORT_PARAM_METADATA_PATH + reportType.getFileName());
		InputStreamReader fileReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
		JsonReader reader = new JsonReader(fileReader);

		Gson gson = new GsonBuilder().create();
		return gson.fromJson(reader, String[][].class);
	}

	/**
	 * Return double string array of technique specification of section connection
	 * for unit series and unit model.
	 *
	 * Metadata file name: section_connection_&lt;series&gt;.xlsx, if for more series metadata, e.g.
	 * section_connection_39CQ.xlsx, section_connection_39XT.xlsx, and so on
	 *
	 * @param unitSeries '39CQ'
	 * @param unitModel '39CQ1234'
	 * @param unitWidth
	 * @param unitHeight
	 * @return
	 */
	public static String[][] getTechSpecSectionConnection(String unitSeries, String unitModel, int unitWidth, int unitHeight, String paneltype,String inskinm) {
		String templateName = unitModel + paneltype.toLowerCase() + inskinm;
		if (sectionConnectionMetadata.containsKey(templateName)) {
			return sectionConnectionMetadata.get(templateName);
		} else {
			String[][] sectionConnection = loadTechSpecSectionConnection(unitSeries, unitModel,unitWidth,unitHeight, paneltype,inskinm);
			sectionConnectionMetadata.put(templateName, sectionConnection);
			return sectionConnection;
		}
	}

	private static String[][] loadTechSpecSectionConnection(String unitSeries, String unitModel, int unitWidth, int unitHeight, String paneltype,String inskinm) {
		String sectionConnectionFile = REPORT_TECH_SPEC_METADATA_PATH + SECTION_CONNECTION_DLJ+ FileType.XLSX.extension();
		String sheetName = unitSeries+paneltype;
		if (!MetadataConstant.JSON_UNIT_PANELTYPE_STANDARD.equals(paneltype)) {//盘管前段连接、正压、立式：section_connection_39CQ_forepart.xlsx
			if (MetadataConstant.JSON_UNIT_PANELTYPE_FOREPART.equals(paneltype)
					&& MetadataConstant.SYS_UNIT_SERIES_39XT.equals(unitSeries)) {//39XT系列没有盘管前段连接
				return null;
			}
		}

		try (InputStream is = new FileInputStream(sectionConnectionFile)) {
			Workbook book = new XSSFWorkbook(is);
			// 执行计算公式
			FormulaEvaluator evaluator = book.getCreationHelper().createFormulaEvaluator();

			//写入型号
			Sheet sheet = book.getSheet(sheetName);
			//CQ立式
			if(unitSeries.contains(SYS_UNIT_SERIES_CQ) && MetadataConstant.JSON_UNIT_PANELTYPE_VERTICAL.equals(paneltype)){
				ExcelUtils.setCellValue(sheet, INPUT_CELLS_CQ_V[0], unitHeight);
				ExcelUtils.setCellValue(sheet, INPUT_CELLS_CQ_V[1], unitWidth);
			}else{
				ExcelUtils.setCellValue(sheet, INPUT_CELLS[0], unitHeight);
				ExcelUtils.setCellValue(sheet, INPUT_CELLS[1], unitWidth);
			}

			if (!MetadataConstant.JSON_UNIT_PANELTYPE_VERTICAL.equals(paneltype)) {//非Vertical处理inskinm
				ExcelUtils.setCellValue(sheet, INPUT_CELLS_Q2[0], inskinm);
			}

			List<List<String>> list = new ArrayList<>();


			/** 得到Excel的行数 */
			int totalRows = sheet.getPhysicalNumberOfRows();

			/** 得到Excel的列数 */
			int totalCells=0;
			if (totalRows >= 1 && EmptyUtil.isNotEmpty(sheet.getRow(0))) {
				totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
			}

			/** 循环Excel的行 */
			for (int r = 0; r < totalRows; r++) {
				Row row = sheet.getRow(r);
				if (EmptyUtil.isEmpty(row)) {
					continue;
				}

				List<String> rowLst = new ArrayList<String>();
				/** 循环Excel的列 */

				for (int c = 0; c < totalCells; c++) {
					Cell cell = row.getCell(c);
					String retCellValue = "";


					CellValue cellValue = evaluator.evaluate(cell);
					if(EmptyUtil.isNotEmpty(cellValue)){
						switch (cellValue.getCellType()) {
							case Cell.CELL_TYPE_BOOLEAN:
								retCellValue = ""+cellValue.getBooleanValue();
								break;
							case Cell.CELL_TYPE_NUMERIC:
								retCellValue = ""+cellValue.getNumberValue();
								if(retCellValue.endsWith(".0")){
									retCellValue = retCellValue.substring(0,retCellValue.length()-2);
								}
								break;
							case Cell.CELL_TYPE_STRING:
								retCellValue = ""+cellValue.getStringValue();
								break;
							case Cell.CELL_TYPE_BLANK:
								break;
							case Cell.CELL_TYPE_ERROR:
								break;

							// CELL_TYPE_FORMULA will never happen
							case Cell.CELL_TYPE_FORMULA:
								break;
						}
					}

					rowLst.add(retCellValue);
				}
				/** 保存第r行的第c列 */
				list.add(rowLst);
			}




			if (EmptyUtil.isNotEmpty(list)) {
				int rows = list.size();
				if(SYS_BLANK.equals(list.get(list.size()-1).get(0))) {
					rows--;
				}
				if(SYS_BLANK.equals(list.get(list.size()-2).get(0))) {
					rows--;
				}

				String[][] sectionConnection = new String[rows][10];
				for (int i = 0; i < rows; i++) {
					for (int j = 0; j < 10; j++) {
						sectionConnection[i][j] = list.get(i).get(j).toString();
					}
				}
				return sectionConnection;
			}
		} catch (Exception e) {
			log.error("Failed to load section connection file for " + unitModel, e);
			throw new MetadataException("Failed to load section connection file for " + unitModel);
		}
		return null;
	}

}
package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/8/29.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Bds extends DataEntity {

    @EntityId
    private String pn;
    private String n1;
    private String n2;
    private String n3;
    private String n4;
    private String n5;
    private String n6;
    private String n7;
    private String n8;
    private String n9;
    private String n10;
    private String n11;
    private String n12;
    private String n13;
    private String n14;
    private String n15;
    private String n16;
    private String n17;
    private String n18;
    private String n19;
    private String n20;
    private String n21;
    private String n22;
    private String n23;
    private String n24;
    private String n25;
    private String n26;
    @AllowNull
    private String n27;
    private String n28;
    private String n29;
    @AllowNull
    private String n30;
    @AllowNull
    private String n31;
    @AllowNull
    private String n32;
    private String field2;
    private String field1;

}

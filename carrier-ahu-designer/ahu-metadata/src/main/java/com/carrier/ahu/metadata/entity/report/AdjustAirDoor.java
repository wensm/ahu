package com.carrier.ahu.metadata.entity.report;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AdjustAirDoor extends DataEntity {

    @EntityId(sequence = 1)
    private String section;
    @EntityId(sequence = 2)
    private String whichFlag;
    @EntityId(sequence = 0)
    private String fanType;
    private String size;
    private String wringq;
    private String wringqAl;

}

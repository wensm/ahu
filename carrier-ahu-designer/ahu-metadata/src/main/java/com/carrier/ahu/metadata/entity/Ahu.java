package com.carrier.ahu.metadata.entity;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Ahu Id related data.
 * 
 * Created by Braden Zhou on 2018/04/27.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Ahu extends DataEntity {

    @EntityId
    private String ahu;

}

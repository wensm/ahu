package com.carrier.ahu.metadata.entity.heatrecycle;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class WheelPrice extends DataEntity {

    @EntityId
    private String priceCode;
    private String originalCode;
    private int wheelDia;
    private double materialPrice;
    private double labourPrice;
    private double labourPriceExport;

}

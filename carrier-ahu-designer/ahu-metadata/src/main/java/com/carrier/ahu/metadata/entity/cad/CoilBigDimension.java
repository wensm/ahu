package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoilBigDimension extends DataEntity {

    @EntityId
    private String unit;
    private String b;
    private String c;
    private String g;
    private String d4r;
    private String d6r;
    private String d8r;
    private String e4r;
    private String e6r;
    private String e8r;
    private String coilh;
    private String coilw;
    private String dia;

}

package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FanEmElectricSizeBig extends DataEntity {
    @EntityId
    private String MOTORBASENO;
    private Integer BPABB;
    private Integer BPWOLONG;
    private Double BPDONGYUAN;
    private Integer BPGE;
    private Integer BPSIMENS;
    private Integer PTABB;
    private Integer PTWOLONG;
    private Integer PTDONGYUAN;
    private Integer PTGE;
    private Integer PTGE2;
    private Integer PTSIMENS3;
    private Integer PTSIMENS2;
    private Double SSDONGYUAN;
    private Integer SSWOLONG;
    private Integer FBWOLONG;
    private Integer C;
}

package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SFan extends DataEntity {

    private String id;
    @EntityId
    private String type;
    private int h;
    private int w;
    private double s;
    private int sCoff;
    private int seq;

}

package com.carrier.ahu.metadata.entity.report;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2018/1/10.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class STakeoverSize extends DataEntity {

    @EntityId
    private String fanType;
    @EntityId(sequence = 1)
    private String section;
    @EntityId(sequence = 2)
    private int flange;
    private String sizeValue;
    @AllowNull
    private String headSize;

    public String getSizeValue() {
        return sizeValue != null ? sizeValue.replace("''", "\"") : sizeValue;
    }

    public String getHeadSize() {
        return headSize != null ? headSize.replace("''", "\"") : headSize;
    }

}

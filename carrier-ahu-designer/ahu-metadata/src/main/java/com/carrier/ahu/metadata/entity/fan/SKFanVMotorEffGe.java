package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SKFanVMotorEffGe extends DataEntity {
    @EntityId
    private double power;
    @EntityId(sequence = 1)
    private int level;
    private String machinesiteNo;
    private double ge;
}

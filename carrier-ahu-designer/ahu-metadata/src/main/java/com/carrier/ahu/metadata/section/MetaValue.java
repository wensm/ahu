package com.carrier.ahu.metadata.section;

import java.util.Map;

import com.carrier.ahu.model.calunit.CalUnit;
import com.google.gson.annotations.Expose;

import lombok.Data;

/**
 * Describe the value of a meta data parameter
 * 
 * @author JL
 *
 */
@Data
public class MetaValue {
	@Expose(serialize = true, deserialize = true)
	private String key;
	@Expose(serialize = true, deserialize = true)
	private ValueOption option;
	@Expose(serialize = true, deserialize = true)
	private String defaultValue;
	@Expose(serialize = true, deserialize = true)
	private Map<String, String> defaultValueMap;

	public MetaValue(String key, ValueOption option,String defaultValue, CalUnit unit,Map<String, String> defaultValueMap) {
		this.key = key;
		this.option = option;
		this.defaultValue = defaultValue;
		this.defaultValueMap = defaultValueMap;
	}
}

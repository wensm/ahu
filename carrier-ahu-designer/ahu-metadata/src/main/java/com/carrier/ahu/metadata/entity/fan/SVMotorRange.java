package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SVMotorRange extends DataEntity {

    @EntityId(sequence = 1)
    private String motorSupplier;
    private String HZ50;
    private String HZ60;
    @EntityId
    private int js;

}

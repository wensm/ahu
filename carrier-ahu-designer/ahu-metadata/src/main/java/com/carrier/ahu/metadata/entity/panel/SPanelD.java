package com.carrier.ahu.metadata.entity.panel;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class SPanelD extends DataEntity {

    @EntityId
    private String serial;
    @EntityId(sequence = 1)
    private String sectionTypeNo;
    private String h;
    @EntityId(sequence = 2)
    private String w;
    private String hr;
    private String hp;
    private String wr;
    private String wp;

}
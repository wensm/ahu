package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import lombok.Data;
import lombok.ToString;

/**
 * 无蜗壳风机段长实体类
 */
@Data
@ToString
public class WwkFanSegmentLength extends DataEntity {
    //机组型号
    private String serial;
    private String id;
    //风机型号
    private String fanModel;
    private String typeLen;
    private String typeLen1;
    //备注
    private String remark;
    //段长
    private String inletLen;
}

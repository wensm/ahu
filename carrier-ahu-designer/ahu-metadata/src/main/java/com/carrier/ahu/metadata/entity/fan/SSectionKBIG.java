package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SSectionKBIG extends DataEntity {

    @EntityId
    private String unitType;
    @EntityId(sequence = 1)
    private String fanType;
    @EntityId(sequence = 2)
    private String airDir;
    private int len;

}

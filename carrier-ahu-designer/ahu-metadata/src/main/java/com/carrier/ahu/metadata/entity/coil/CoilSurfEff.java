package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author gaok2
 * @PackageName:com.carrier.ahu.metadata.entity.coil
 * @className: CoilSurfEff
 * @Description:
 * @date 2021/3/12 13:15
 */

/**
 * 盘管系数实体类
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CoilSurfEff extends DataEntity {

        //盘管类型
        @EntityId
        private String coilType;
        
        //片距
        @EntityId(sequence = 1)
        private String finDensity;

        //翅片材质
        @EntityId(sequence = 2)
        private String finType;

        //排数
        @EntityId(sequence = 3)
        private String rows;

        //管径
        @EntityId(sequence = 4)
        private String tubeDiameter;

        //季节
        @EntityId(sequence = 5)
        private String season;

        //盘管系数
        private String surfEff;

        //进水温度100度盘管系数
        private String surfEff100;
}

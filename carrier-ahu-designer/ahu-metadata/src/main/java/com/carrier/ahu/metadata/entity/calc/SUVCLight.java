package com.carrier.ahu.metadata.entity.calc;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2018/4/26.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SUVCLight extends DataEntity {

    @EntityId
    private String fanType;
    private String type;
    private int quan;

}

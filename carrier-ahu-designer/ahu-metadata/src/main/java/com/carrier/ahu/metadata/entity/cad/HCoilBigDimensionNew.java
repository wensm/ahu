package com.carrier.ahu.metadata.entity.cad;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HCoilBigDimensionNew extends DataEntity {

    @EntityId
    private String unit;
    private String b;
    private String c;
    private String g;
    private String d2r;
    private String e2r;
    private String coilh;
    private String coilw;
    private String dia;

}

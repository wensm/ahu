package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.metadata.entity.Ahu;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CoilDllCalculation extends Ahu {

    private int TID;
    private int cirNo;
    private int tLen;
    private int tNo;
    private int row;
    private String circuit;
    private double waterFlowRate;

}

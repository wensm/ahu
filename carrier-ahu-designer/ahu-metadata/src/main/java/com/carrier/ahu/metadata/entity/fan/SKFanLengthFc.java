package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SKFanLengthFc extends DataEntity {


    @EntityId
    private String TYPE;
    @EntityId(sequence = 1)
    private Integer FANMODE;
    private String MACHINESITENO;
    private Integer THFBHF;
    private Integer UBFUBR;
    private Double FBTHFBHF;
    private Double FBUBFUBR;

}

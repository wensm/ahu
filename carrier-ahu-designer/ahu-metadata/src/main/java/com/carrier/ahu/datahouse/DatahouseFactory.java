package com.carrier.ahu.datahouse;

import com.carrier.ahu.datahouse.csv.CsvDatahouse;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.excel.ExcelDatahouse;

/**
 * It provides data house for csv, excel files.
 * 
 * Created by Braden Zhou on 2018/04/27.
 */
public class DatahouseFactory {

    public static <T extends DataEntity, K> Datahouse<T, K> csv(Class<T> clazz, String fileName) {
        return new CsvDatahouse<T, K>(clazz, fileName);
    }

    public static <T extends DataEntity, K> Datahouse<T, K> excel(Class<T> clazz, String fileName) {
        return new ExcelDatahouse<T, K>(clazz, fileName);
    }

    public static <T extends DataEntity, K> Datahouse<T, K> excel(Class<T> clazz, String fileName,
            boolean isClassified) {
        return new ExcelDatahouse<T, K>(clazz, fileName, isClassified);
    }

}

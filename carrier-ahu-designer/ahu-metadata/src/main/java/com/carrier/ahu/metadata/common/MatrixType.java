package com.carrier.ahu.metadata.common;

/**
 * Created by Braden Zhou on 2018/05/11.
 */
public enum MatrixType {

    SECTION_RELATION("entity/section/section_relation.csv");

    private String fileName;

    private MatrixType(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return this.fileName;
    }

}

package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class NonstdMotor extends DataEntity implements NonstdPrice {

    @EntityId
    private String gl;// 电机功率
    @EntityId(sequence = 1)
    private String js;// 电机级数
    @EntityId(sequence = 2)
    private String fhdj;// 防护等级
    @EntityId(sequence = 3)
    private String jjdj;// 绝缘等级

    /*
     * ABB A 默认 B 东元 C 西门子 S
     */
    @EntityId(sequence = 4)
    private String supplier;// 供应商
    @AllowNull
    private String partNo;// ?
    @EntityId(sequence = 5)
    private String memo;// ?
    private double tp;
    private double oldPrice;
    @AllowNull
    private String supplierNo;// null

}

package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 湿膜加湿段
 * 查询条件为:（老unit 型号） + （加湿量 >= 系统加适量） 的第一条记录
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class NonstdWet extends DataEntity {
    @EntityId
    private String unit;//老型号
    private String tp;//新价格
    private String oldPrice;//老价格
    @EntityId(sequence = 1)
    private String sprayV;//类型厚度
}

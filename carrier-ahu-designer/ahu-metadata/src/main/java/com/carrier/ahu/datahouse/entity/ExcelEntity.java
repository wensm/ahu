package com.carrier.ahu.datahouse.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * Handle the same data structure in different sheets.
 * 
 * Created by Braden Zhou on 2018/05/31.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ExcelEntity extends DataEntity {

    @EntityId
    private String sheetName;

}

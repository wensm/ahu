package com.carrier.ahu.metadata.common;

import com.carrier.ahu.common.exception.AhuException;

/**
 * Exception for metadata.
 * 
 * Created by Braden Zhou on 2018/05/29.
 */
@SuppressWarnings("serial")
public class MetadataException extends AhuException {

    public MetadataException(String message) {
        super(message);
    }

    public MetadataException(String message, Exception e) {
        super(message, e);
    }

}

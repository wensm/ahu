package com.carrier.ahu.metadata.entity.humidifier;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import com.carrier.ahu.util.EmptyUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SJModel extends DataEntity {

    @EntityId
    private int humidificationQ;
    private String model;
    private int maxHumidificationQ;
}

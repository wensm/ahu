package com.carrier.ahu.metadata.entity.cad;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoilBigDimensionL extends CoilBigDimension {

}

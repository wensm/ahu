package com.carrier.ahu.metadata;

import static com.carrier.ahu.metadata.common.MetadataConstants.METADATA_PACKAGE;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ExcelUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * Provide weight metadata from excel files.
 * 
 * Created by Braden Zhou on 2018/06/15.
 */
@Slf4j
public class WeightMetadata {

    private static final String UNIT_SERIES_WEIGHT_FILE_FORMAT = METADATA_PACKAGE + "weight/{0}/_{1}/{2}.xlsx";

    private static Map<String, Map<String, Map<String, Map<String, String>>>> unitSeriesSpecDatas = new HashMap<>();

    public static Map<String, Map<String, String>> getWeightSpecData(String factory, String unitSeries,
            CalculatorSpec spec) {
        Map<String, Map<String, String>> specData = null;
        String specDataKey = spec.getSpecKey();
        if (unitSeriesSpecDatas.containsKey(unitSeries)) {
            Map<String, Map<String, Map<String, String>>> unitSeriesSpecData = unitSeriesSpecDatas.get(unitSeries);
            if (unitSeriesSpecData.containsKey(specDataKey)) {
                return unitSeriesSpecData.get(specDataKey);
            } else {
                specData = loadWeightSpecData(factory, unitSeries, spec);
                unitSeriesSpecData.put(specDataKey, specData);
            }
        } else {
            Map<String, Map<String, Map<String, String>>> unitSeriesSpecData = new HashMap<>();
            specData = loadWeightSpecData(factory, unitSeries, spec);
            unitSeriesSpecData.put(specDataKey, specData);
            unitSeriesSpecDatas.put(unitSeries, unitSeriesSpecData);
        }
        return specData == null ? new HashMap<>() : specData;
    }

    private static Map<String, Map<String, String>> loadWeightSpecData(String factory, String unitSeries,
            CalculatorSpec spec) {
        Map<String, Map<String, String>> weightMap = new HashMap<String, Map<String, String>>();
        InputStream is = null;
        Workbook wb = null;
        try {
            is = getSpecDataStream(factory, unitSeries, spec.getFile());
            wb = ExcelUtils.openExcelByFactory(is);
            if (wb.getSheetIndex(spec.getTab()) == -1) {
                log.warn(String.format("Failed to find spec %s", spec.getTab()));
            }

            Sheet sheet = wb.getSheet(spec.getTab());
            Row row0 = sheet.getRow(0);
            int totalRows = sheet.getPhysicalNumberOfRows();
            int totalCells = 0;
            if (totalRows >= 1 && null != row0) {
                totalCells = row0.getPhysicalNumberOfCells();
            }

            for (int r = 1; r < totalRows; r++) {
                Row row = sheet.getRow(r);
                if (EmptyUtil.isEmpty(row)) {
                    continue;
                }
                Cell cell = row.getCell(0);
                String weightKey = ExcelUtils.getStringCellValueNoDot(cell);
                if (EmptyUtil.isEmpty(cell)) {
                    continue;
                }

                Map<String, String> itemWeightMap = new HashMap<String, String>();
                for (int c = 1; c < totalCells; c++) {
                    String cellKey = ExcelUtils.getStringCellValueNoDot(row0.getCell(c));
                    String cellValue = ExcelUtils.getWeightStringCellValue(row.getCell(c));
                    itemWeightMap.put(cellKey, cellValue);
                }
                if (weightMap.containsKey(weightKey)) {
                    Map<String, String> tempMap = weightMap.get(weightKey);
                    tempMap.putAll(itemWeightMap);
                    weightMap.put(weightKey, tempMap);
                } else {
                    weightMap.put(weightKey, itemWeightMap);
                }
            }
        } catch (Exception e) {
            log.error("Failed to load spec data", e);
        } finally {
            if (EmptyUtil.isNotEmpty(is)) {
                try {
                    is.close();
                } catch (IOException e) {
                    is = null;
                    log.error("Failed to close file", e);
                }
            }
        }
        return weightMap;
    }

    private static InputStream getSpecDataStream(String factory, String unitSeries, String sectionId) {
        String specFilePath = getSectionWeightFile(factory, unitSeries, sectionId);
        InputStream is = WeightMetadata.class.getClassLoader().getResourceAsStream(specFilePath);
        if (is == null) {
            // use 39CQ as default
            specFilePath = getSectionWeightFile(factory, SystemCalculateConstants.AHU_PRODUCT_39CQ, sectionId);
            is = WeightMetadata.class.getClassLoader().getResourceAsStream(specFilePath);
        }
        return is;
    }

    private static String getSectionWeightFile(String factory, String unitSeries, String sectionId) {
        return MessageFormat.format(UNIT_SERIES_WEIGHT_FILE_FORMAT, factory, unitSeries, sectionId);
    }

}

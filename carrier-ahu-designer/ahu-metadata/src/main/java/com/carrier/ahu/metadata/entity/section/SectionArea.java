package com.carrier.ahu.metadata.entity.section;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SectionArea extends DataEntity {

    @EntityId
    private String fanType;
    private double A;
    private double Bin;
    private double Bout;
    private double C;
    private double D;
    private double E;
    private double F;
    private double G;
    private double H;
    private double I;
    private double J;
    private double K;
    private double L;
    private double M;
    private double N;
    private double O;
    private double V;
    private double X;
    private double Y;
    @AllowNull
    private double D1;
    @AllowNull
    private double E1;

}

package com.carrier.ahu.metadata.entity.heatrecycle;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2017/11/28.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PartWWheel extends DataEntity {

    @EntityId
    private String product;
    private int wheelDiaByAir39CQ;
    private int wheelTypeByAir39CQ;
    private int wheelDiaHeatX;
    @EntityId(sequence = 1)
    private int efficiencyType;
    private int wheelDiaYuFeng;
    private int wheelDiaByAir39G;
    private int wheelTypeByAir39G;

}

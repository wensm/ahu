package com.carrier.ahu.metadata.entity.resistance;

import com.carrier.ahu.datahouse.entity.DataEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Resistance base class.
 * 
 * Created by Braden Zhou on 2018/05/21.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SResistance extends DataEntity {

    private double velocity;
    private double resistance;
    private double resistance2;
}

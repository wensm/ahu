package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/7/17.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SKFanMotor extends DataEntity {

    @EntityId
    private double power;
    @EntityId(sequence = 1)
    private int level;
    @EntityId(sequence = 2)
    private String machineSiteNo;// 机器位置标号 Y80M1
    private String machineSite;// 机器位置 804
    private double weight;
    private double inputPower;
    private String machineSiteNo1;
    private double motorCode;
    private int seq;

}

package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/7/17.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SKFanType extends DataEntity {

    @EntityId
    private String type;
    @EntityId(sequence = 3)
    private int bend;
    @EntityId(sequence = 2)
    @AllowNull
    private String fanQuery;
    private String fan;
    private double maxPower;
    private double maxMachineSite;
    @EntityId(sequence = 1)
    private String supplier;

}

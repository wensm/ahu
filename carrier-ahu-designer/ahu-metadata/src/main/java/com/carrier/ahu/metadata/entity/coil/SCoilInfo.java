package com.carrier.ahu.metadata.entity.coil;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/7/26.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SCoilInfo extends DataEntity {

    @EntityId
    private String unitModel;
    private int tId;
    private int cirNo;
    private int tLen;
    private int tNo;
    @EntityId(sequence = 1)
    private int row;
    @EntityId(sequence = 2)
    private String circuit;
    private double waterFlowRate;

}

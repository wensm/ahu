package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SPlugfandimension extends DataEntity {

    @EntityId
    private String fanType;
    private String fanModel;
    private String topHeight;
    private String frontHeight;
    private String h1;
    private String frontLength;
    private String tmax;
    private String x1;
    private String x2;
    private String x3;
    private String d2;
    private String plugFanStart;


}

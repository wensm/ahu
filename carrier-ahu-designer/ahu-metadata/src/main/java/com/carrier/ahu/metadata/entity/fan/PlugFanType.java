package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.metadata.entity.Ahu;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PlugFanType extends Ahu {

    private int bend;
    private String fan;
    private double maxPower;
    private String maxMachineSite;
    private String supplier;
    @AllowNull
    private String fanQuery;

}

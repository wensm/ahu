package com.carrier.ahu.metadata.entity.filter;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HEPAFilterComposing extends FilterComposing {

}

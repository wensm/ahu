package com.carrier.ahu.metadata.entity.resistance;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by liangd4 on 2017/10/18.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SJunliuqiResi extends SResistance {

}

package com.carrier.ahu.metadata.entity.heatrecycle;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HeatxPlate extends DataEntity {

    @EntityId
    private String unit;
    private int width;
    private int height;
    private String type;
    private String corrugatedChannel;
    private String model;
    private double materailPrice;
    private double labourPrice;
    private double labourPriceExport;
    @AllowNull
    private String isLt;

}
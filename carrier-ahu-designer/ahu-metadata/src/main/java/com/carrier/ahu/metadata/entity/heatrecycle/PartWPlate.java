package com.carrier.ahu.metadata.entity.heatrecycle;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2017/11/30.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PartWPlate extends DataEntity {

    @EntityId
    private String product;
    private String wheelModel;
    private int desiccatorType;
    private int plateWidth;
    private int plateLength;

}

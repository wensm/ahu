package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SFanMotorBaseDimension extends DataEntity {

    @EntityId(sequence = 1)
    private int type;
    private int baseStart;
    private int baseEnd;
    private int a;
    private int q;
    private int c;
    private int inlet;
    private int maxMotor;
}

package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Pressure-Diff price.
 * 
 * Option 1: Dwyer </br>
 * Option 2: Digital
 * 
 * Created by Braden Zhou on 2019/03/11.
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class NonstdPressure extends DataEntity implements NonstdPrice {

    @EntityId
    private String option;
    private double tp;
    private double oldPrice;

}

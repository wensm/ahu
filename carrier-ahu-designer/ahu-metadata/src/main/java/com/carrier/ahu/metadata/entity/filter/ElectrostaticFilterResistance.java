package com.carrier.ahu.metadata.entity.filter;

import com.carrier.ahu.datahouse.entity.DataEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ElectrostaticFilterResistance extends DataEntity{

    private double windSpeed;
    private double averResi;
    private double averResi1;

}

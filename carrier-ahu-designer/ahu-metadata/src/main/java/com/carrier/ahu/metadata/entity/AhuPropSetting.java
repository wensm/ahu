package com.carrier.ahu.metadata.entity;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AhuPropSetting extends DataEntity {

    @EntityId
    private String key;
    private String b;
    private String m;

}

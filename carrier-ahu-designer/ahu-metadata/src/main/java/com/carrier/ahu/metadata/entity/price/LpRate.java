package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Product price rate.
 * 
 * Created by Braden Zhou on 2019/03/11.
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class LpRate extends DataEntity {

    @EntityId
    private String product; // 系列号，39CQ...
    private double margin;
    private double rate;

}

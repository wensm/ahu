package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class NsAccessFlatSlPrice extends DataEntity {
    @EntityId
    private String type;
    private Integer m3;
    private Integer m4;
    private Integer m5;
    private Integer m6;
    private Integer m7;
    private Integer m8;
    private Integer m9;

}

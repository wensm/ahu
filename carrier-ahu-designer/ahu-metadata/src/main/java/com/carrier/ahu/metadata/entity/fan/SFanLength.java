package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SFanLength extends DataEntity {

    @EntityId
    private String id;
    @EntityId(sequence = 1)
    private String type;
    private int typeLen;
    private int typeLen1;
    @AllowNull
    private int typeLen2;
    @AllowNull
    private int typeLen3;
    @AllowNull
    private String remark;

}

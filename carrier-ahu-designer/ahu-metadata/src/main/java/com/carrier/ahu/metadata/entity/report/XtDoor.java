package com.carrier.ahu.metadata.entity.report;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class XtDoor extends DataEntity {
    @EntityId
    private String unitType;
    @EntityId(sequence = 1)
    private String sectionl;
    private String xtDoorw;
    private String xtDoorh;
    private String xtDoorwSize;
    private String xtDoorhSize;
}

package com.carrier.ahu.metadata.entity.fan;

import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FrequencySmall extends DataEntity {

    @EntityId
    private String FANTYPE;
    @EntityId(sequence = 1)
    private String FANMODEL;
    @EntityId(sequence = 2)
    private String MOTORBASENO;
    private int ABB;
    private int WOLONG;
    private int DONGYUAN;
    private int SIEMENS;
}

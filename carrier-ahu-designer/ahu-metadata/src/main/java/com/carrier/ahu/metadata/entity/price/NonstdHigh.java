package com.carrier.ahu.metadata.entity.price;

import com.carrier.ahu.datahouse.entity.AllowNull;
import com.carrier.ahu.datahouse.entity.DataEntity;
import com.carrier.ahu.datahouse.entity.EntityId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
/**
 * 高压喷雾加湿段
 * 查询条件为:（最大喷雾量 >= 系统加适量） 的第一条记录
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class NonstdHigh extends DataEntity {
    @AllowNull
    private String unit;
    private String tp;//新价格
    private String oldPrice;//老价格
    @EntityId
    private int sprayV;//最大喷雾量

}

package com.carrier.ahu.metadata;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.carrier.ahu.datahouse.entity.MatrixEntity;
import com.carrier.ahu.metadata.common.MatrixType;

/**
 * 
 * Created by Braden Zhou on 2018/05/11.
 */
@RunWith(Parameterized.class)
public class MatrixMetadataTest {

    private String before;
    private String after;
    private String expected;
    private String firstValue;

    public MatrixMetadataTest(String before, String after, String expected, String firstValue) {
        this.before = before;
        this.after = after;
        this.expected = expected;
        this.firstValue = firstValue;
    }

    @Test
    public void testMatrixMetadata() {
        MatrixEntity matrixEntity = MatrixMetadata.findMatrix(MatrixType.SECTION_RELATION);
        String relation = matrixEntity.findMatrixValue(before, after);
        assertEquals(relation, expected);

        relation = matrixEntity.findFirstMatrixValue(after);
        assertEquals(relation, firstValue);

        relation = MatrixMetadata.findMatrixValue(MatrixType.SECTION_RELATION, before, after);
        assertEquals(relation, expected);

        relation = MatrixMetadata.findFirstMatrixValue(MatrixType.SECTION_RELATION, after);
        assertEquals(relation, firstValue);
    }

    @SuppressWarnings("rawtypes")
    @Parameters
    public static Collection prepareData() {
        String[][] object = { { "ahu.mix", "ahu.mix", null, "1" }, { "ahu.mix", "ahu.discharge", null, null },
                { "ahu.mix", "ahu.combinedMixingChamber", null, null }, { "ahu.mix", "ahu.filter", "1", "1" },
                { "ahu.mix", "ahu.combinedFilter", "1", null }, { "ahu.mix", "ahu.HEPAFilter", null, null },
                { "ahu.mix", "ahu.electrostaticFilter", null, null }, { "ahu.mix", "ahu.coolingCoil", "1", "1" },
                { "ahu.mix", "ahu.directExpensionCoil", "1", "1" }, { "ahu.mix", "ahu.heatingCoil", "1", "1" },
                { "ahu.mix", "ahu.steamCoil", "1", "1" }, { "ahu.mix", "ahu.electricHeatingCoil", "1", "1" },
                { "ahu.mix", "ahu.steamHumidifier", "1", null }, { "ahu.mix", "ahu.wetfilmHumidifier", null, null },
                { "ahu.mix", "ahu.sprayHumidifier", null, null }, { "ahu.mix", "ahu.electrodeHumidifier", "1", "1" },
                { "ahu.mix", "ahu.fan", "1", "1" }, { "ahu.mix", "ahu.noVoluteFan", "1", "1" },
                { "ahu.mix", "ahu.attenuator", null, null }, { "ahu.mix", "ahu.access", "1", null },
                { "ahu.mix", "ahu.diffuser", null, null }, { "ahu.mix", "ahu.heatRecycle", "1", null },
                { "ahu.mix", "ahu.ctr", "1", null }, { "ahu.ctr", "ahu.mix", null, "1" },
                { "ahu.ctr", "ahu.discharge", null, null }, { "ahu.ctr", "ahu.combinedMixingChamber", null, null },
                { "ahu.ctr", "ahu.filter", null, "1" }, { "ahu.ctr", "ahu.combinedFilter", null, null },
                { "ahu.ctr", "ahu.HEPAFilter", null, null }, { "ahu.ctr", "ahu.electrostaticFilter", null, null },
                { "ahu.ctr", "ahu.coolingCoil", null, "1" }, { "ahu.ctr", "ahu.directExpensionCoil", null, "1" },
                { "ahu.ctr", "ahu.heatingCoil", null, "1" }, { "ahu.ctr", "ahu.steamCoil", null, "1" },
                { "ahu.ctr", "ahu.electricHeatingCoil", null, "1" }, { "ahu.ctr", "ahu.steamHumidifier", null, null },
                { "ahu.ctr", "ahu.wetfilmHumidifier", null, null }, { "ahu.ctr", "ahu.sprayHumidifier", null, null },
                { "ahu.ctr", "ahu.electrodeHumidifier", null, "1" }, { "ahu.ctr", "ahu.fan", "3", "1" },
                { "ahu.ctr", "ahu.noVoluteFan", null, "1" }, { "ahu.ctr", "ahu.attenuator", null, null },
                { "ahu.ctr", "ahu.access", null, null }, { "ahu.ctr", "ahu.diffuser", null, null },
                { "ahu.ctr", "ahu.heatRecycle", null, null }, { "ahu.ctr", "ahu.ctr", null, null }, };
        return Arrays.asList(object);
    }

}

package com.carrier.ahu.metadata;

import static com.carrier.ahu.metadata.AhuMetadata.startsWith;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;

import com.carrier.ahu.metadata.entity.AhuSizeDetail;

/**
 * Test special condition when search for metadata.
 * 
 * Created by Braden Zhou on 2018/06/15.
 */
public class AhuMetadataQueryTest {

    @Test
    public void testAhuSizeDetail() {
        String unitSeries = "39CQ";
        List<AhuSizeDetail> ahuSizeDetails = AhuMetadata.findList(AhuSizeDetail.class, startsWith(unitSeries));
        assertNotNull(ahuSizeDetails);
        assertFalse(ahuSizeDetails.isEmpty());
    }

}

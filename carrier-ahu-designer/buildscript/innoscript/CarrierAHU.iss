; 脚本用 Inno Setup 脚本向导 生成。
; 查阅文档获取创建 INNO SETUP 脚本文件的详细资料！

#define MyAppName "CarrierAHU"
#define MyAppVersion "2.0.0"
#define MyAppPublisher "Carrier"
#define MyAppExeName "CarrierAHU.exe"
#define MyAppPublisher "Carrier SRDC"

[Setup]
; 注意: AppId 的值是唯一识别这个程序的标志。
; 不要在其他程序中使用相同的 AppId 值。
; (在编译器中点击菜单“工具 -> 产生 GUID”可以产生一个新的 GUID)
AppId={{9C06D89C-1E1A-41D4-BD2E-F2B723C8E057}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName=D:\{#MyAppName}
DefaultGroupName={#MyAppName}
OutputDir=D:\package\Installation\package\AHU-TMP\chs
OutputBaseFilename=CarrierAHU_ZH_V{#MyAppVersion}
SetupIconFile=D:\package\Installation\package\AHU-TMP\logo.ico
Compression=lzma
SolidCompression=yes

[Languages]
Name: "default"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkedonce

[Files]
Source: "D:\package\Installation\package\AHU-TMP\CarrierAHU.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\lib\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\package\Installation\package\AHU-TMP\asserts\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs uninsneveruninstall
Source: "D:\package\Installation\package\AHU-TMP\Refrig\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\package\Installation\package\AHU-TMP\regdll\*.dll"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs onlyifdoesntexist regserver 32bit
Source: "D:\package\Installation\package\AHU-TMP\regdll\*.mdb"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs
;Source: "D:\package\Installation\package\AHU-TMP\coil_w.out"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\EvapAir1.sif"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\EvapAir1Dll.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\libifcoremdd.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\libmmd.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\libmmdd.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\msvcr100d.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\plate"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\shutdown.bat"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\shutdown.pid"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\Rdci6993.eng"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\tube_constants"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\jre1.8.0_131\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\package\Installation\package\AHU-TMP\carrier-ahu.jar"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\input - Copy.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\input.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\input1.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\package\Installation\package\AHU-TMP\input-normal.txt"; DestDir: "{app}"; Flags: ignoreversion

; 注意: 不要在任何共享的系统文件使用 "Flags: ignoreversion"

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}{#MyAppVersion}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

[Run]
Filename: "{app}\asserts\input\bin\AutoDWG\Bin\reg.bat"; Flags: nowait runminimized;

[ISFormDesigner]
WizardForm=FF0A005457495A415244464F524D0030104F01000054504630F10B5457697A617264466F726D0A57697A617264466F726D0C436C69656E74486569676874034C010B436C69656E74576964746803F1010D4578706C6963697457696474680301020E4578706C696369744865696768740373010D506978656C73506572496E636802600A54657874486569676874020C00F10C544E65774E6F7465626F6F6B0D4F757465724E6F7465626F6F6B00F110544E65774E6F7465626F6F6B5061676509496E6E65725061676500F10C544E65774E6F7465626F6F6B0D496E6E65724E6F7465626F6F6B00F110544E65774E6F7465626F6F6B506167650D53656C6563744469725061676500F10E544E6577537461746963546578740E4469736B53706163654C6162656C0743617074696F6E1214000000F381115C00978189200033003000300020004D004200200084767A7AF295C178D8767A7AF495023000000000000000

[Code]
{ RedesignWizardFormBegin } // Don't remove this line!
// Don't modify this section. It is generated automatically.
procedure RedesignWizardForm;
begin
  with WizardForm.DiskSpaceLabel do
  begin
    Caption := '至少需要 300 MB 的空闲磁盘空间。';
  end;

{ ReservationBegin }
  // This part is for you. Add your specialized code here.

{ ReservationEnd }
end;
// Don't modify this section. It is generated automatically.
{ RedesignWizardFormEnd } // Don't remove this line!

procedure InitializeWizard();
begin
  RedesignWizardForm;
end;





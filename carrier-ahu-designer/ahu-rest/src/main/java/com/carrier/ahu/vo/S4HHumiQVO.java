package com.carrier.ahu.vo;

import lombok.Data;

@Data
public class S4HHumiQVO {

    private int Type;
    private double VaporPressure;

}

package com.carrier.ahu.product;

import java.util.Map;

import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.report.ExportUnit;

/**
 * 校验器接口（工厂模式-产品接口）
 * @author wangd1
 *
 */
public interface ValidatorProduct {
	
	public boolean validate(ExportUnit eUnit, Project project, String groupId);
	
	public Map<String, Object> validate1(ExportUnit eUnit, Project project, String groupId);

	public Map<String, Object> validate2(ExportUnit eUnit, Project project, String groupId);
}
package com.carrier.ahu.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.VersionVO;

import io.swagger.annotations.Api;

/**
 * Created by liangd4 on 2018/4/4.
 */
@Api(description = "帮助(help)文档")
@RestController
public class HelpController extends AbstractController {

    @RequestMapping(value = "help/document", method = RequestMethod.GET)
    public ApiResult<String> getDocument() throws Exception {
        LanguageEnum languageEnum = getLanguage();
        String documentPath = "";

        if (languageEnum != null) {
            if (languageEnum.getId().equals(RestConstant.SYS_LANGUAGE_EN_US)) {// 英文环境下
                documentPath = RestConstant.SYS_PATH_MANUAL_EN;
            } else {
                documentPath = RestConstant.SYS_PATH_MANUAL_CN;
            }
        }
        return ApiResult.success(documentPath);
    }

    @RequestMapping(value = "help/getSysVersion", method = RequestMethod.GET)
    public ApiResult<VersionVO> getSysVersion() throws Exception {
        String ahuVersion = AHUContext.isExportVersion() ? AHUContext.getAhuVersion() + RestConstant.SYS_VERSION_EXPORT
                : AHUContext.getAhuVersion();
        VersionVO versionVo = new VersionVO();
        versionVo.setAhuVersion(ahuVersion);
        versionVo.setAhuPatchVersion(AHUContext.getAhuPatchVersion());
        return ApiResult.success(versionVo);
    }

    @RequestMapping(value = "help/isExport", method = RequestMethod.GET)
    public ApiResult<String> isExport() throws Exception {
        return ApiResult.success(String.valueOf(AHUContext.isExportVersion()));
    }

}

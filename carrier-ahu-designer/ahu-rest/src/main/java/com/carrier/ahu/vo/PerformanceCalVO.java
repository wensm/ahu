package com.carrier.ahu.vo;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/8.
 */
@Data
public class PerformanceCalVO {

    private String season;
    private String fEfficiencyS;
    private String fEfficiencyW;
    private String supplier;
    private String inputVaporPressure;
}

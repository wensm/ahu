package com.carrier.ahu.factory;

import com.carrier.ahu.product.TemperatureValidatorProduct;
import com.carrier.ahu.product.ValidatorProduct;
import com.carrier.ahu.service.AhuService;

/**
 * 温度校验器（工厂模式-工厂）
 * @author wangd1
 *
 */
public class TemperatureValidatorFactory implements ValidatorFactory {

	@Override
	public ValidatorProduct getValidator(AhuService ahuService) {
		return new TemperatureValidatorProduct(ahuService);
	}
	
	@Override
	public ValidatorProduct getValidator() {
		return new TemperatureValidatorProduct();
	}
	
}
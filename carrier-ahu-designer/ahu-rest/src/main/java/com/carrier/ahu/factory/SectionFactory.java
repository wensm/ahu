package com.carrier.ahu.factory;

import java.util.ArrayList;
import java.util.List;

import com.carrier.ahu.common.entity.Part;

public class SectionFactory {
	
	public static List<String> getAhuSectionKeys(List<Part> parts){
		List<String> keys = new ArrayList<String>();
		for (Part part : parts) {
			keys.add(part.getSectionKey());
		}
		return keys;
	}
	
}

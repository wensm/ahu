package com.carrier.ahu.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.DamperPosEnum;
import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.service.business.BusinessRuleFactory;
import com.carrier.ahu.service.business.damper.DamperBusinessRule;
import com.carrier.ahu.vo.AdjustDamperVO;
import com.carrier.ahu.vo.ApiResult;
import com.google.common.collect.Maps;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(description = "风阀相关接口")
@RestController
public class DamperController extends AbstractController {

    @Autowired
    private BusinessRuleFactory bizRuleFactory;

    @RequestMapping(value = "dampers/adjustSize/{positions}", method = RequestMethod.POST)
    public ApiResult<Map<String, Object>> adjustDamperSize(@RequestBody AdjustDamperVO adjustDamperVo,
            @PathVariable("positions") String[] positions) throws Exception {
        try {
            Map<String, Object> result = Maps.newHashMap();
            for (String position : positions) {
                try {
                    DamperPosEnum damperPos = DamperPosEnum.valueOf(position);
                    DamperBusinessRule businessRule = bizRuleFactory.getDamperBusinessRule(adjustDamperVo.getUnit(),
                            adjustDamperVo.getSection());
                    Map<String, Object> metadata = businessRule.adjustDamperSize(damperPos);
                    result.put(position, metadata);
                } catch (AhuException e) {
                    log.error("failed to adjust damper size", e);
                    result.put(position, AHUContext.getErrorMessage(e));
                }
            }
            return ApiResult.success(result);
        } catch (Exception e) {
            log.error("failed to adjust damper size", e);
            throw new ApiException(ErrorCode.ADJUST_DAMPER_SIZE_FAILED);
        }
    }

}

package com.carrier.ahu.rest;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.entity.BatchCfgProgress;
import com.carrier.ahu.common.entity.GroupInfo;
import com.carrier.ahu.common.entity.MatericalConstance;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.CalcTypeEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.BatchCfgProgressService;
import com.carrier.ahu.service.GroupService;
import com.carrier.ahu.service.MatericalConstanceService;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.service.cal.CalContextUtil;
import com.carrier.ahu.service.cal.impl.DefaultCalContext;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.Excel4ModelInAndOutUtils;
import com.carrier.ahu.util.UnitUtil;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.BatchCfgProgressVO;
import com.carrier.ahu.vo.MatericalConstanceRequestVo;
import com.carrier.ahu.vo.SystemCalculateConstants;

import io.swagger.annotations.Api;

@Api(description = "材料批量配置接口")
@RestController
public class MatericalConstanceController extends AbstractController {

	@Autowired
	MatericalConstanceService service;
	@Autowired
	ProjectService projectService;
	@Autowired
	AhuService ahuService;
	@Autowired
	SectionService sectionService;
	@Autowired
	GroupService groupService;
	@Autowired
	BatchCfgProgressService progressService;
	@Autowired
	private AhuStatusTool ahuStatusTool;
	@Autowired
	private BatchCalculateAndConfigTask task;

	/**
	 * 查询(初始化)项目批量配置
	 *
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "mc/getpc/{projid}", method = RequestMethod.GET)
	public ApiResult<MatericalConstance> getProjectConstance(@PathVariable("projid") String projid) throws Exception {
		MatericalConstance con = service.getConstanceByProjidAndGroupid(projid, null);
		if (EmptyUtil.isEmpty(con)) {
			con = new MatericalConstance();
			con.setPid(null);
			con.setProjid(projid);
			con.setGroupid(null);
			con.setProkey(TemplateUtil.getAllAhuProTemplateAsString());
		}
		return ApiResult.success(con);
	}

	/**
	 * 查询(初始化)分组批量配置
	 *
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "mc/getgc/{projid}/{groupid}", method = RequestMethod.GET)
    public ApiResult<MatericalConstance> getGroupConstance(@PathVariable("projid") String projid,
            @PathVariable("groupid") String groupid) throws Exception {
        MatericalConstance con = service.getConstanceByProjidAndGroupid(projid, groupid);
        if (EmptyUtil.isEmpty(con)) {
            List<Unit> units = this.ahuService.findAhuList(projid, groupid);
            if (units != null && !units.isEmpty()) {
                Unit firstUnit = units.get(0);
                List<Part> parts = this.sectionService.findSectionList(firstUnit.getUnitid());
                if (parts != null && !parts.isEmpty()) {
                    con = new MatericalConstance();
                    con.setPid(null);
                    con.setProjid(projid);
                    con.setGroupid(groupid);
                    con.setProkey(TemplateUtil.getAllMaterialDefaultValueString(parts));
                }
            } else {
                throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
            }
        }
        return ApiResult.success(con);
    }

	/**
	 * 修改保存项目批量配置
	 *
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "mc/savepc", method = RequestMethod.POST)
	public ApiResult<String> saveProjectConstance(MatericalConstanceRequestVo vo) throws Exception {
		String projid = vo.getProjid();
		if (EmptyUtil.isEmpty(projid)) {
			return ApiResult.error(getIntlString(I18NConstants.PROJECT_ID_IS_EMPTY)); // should be threw out ???
		}
		String pid = RestConstant.SYS_BLANK;
		MatericalConstance con = service.getConstanceByProjidAndGroupid(projid, null);
		if (EmptyUtil.isEmpty(con)) {
			con = new MatericalConstance();
			con.setPid(UUID.randomUUID().toString().replaceAll(RestConstant.SYS_PUNCTUATION_HYPHEN, RestConstant.SYS_BLANK));
			con.setProjid(projid);
			con.setGroupid(null);
			con.setProkey(vo.getProkey());
			pid = service.addConstance(con, getUserName());
		} else {
			con.setProkey(vo.getProkey());
			pid = service.updateConstance(con, getUserName());
		}
		formatByMatericalConstanceVo(vo, projid, null);
		return ApiResult.success(pid);
	}

	private static Map<String, Object> mergeMeta(Map<String, Object> sourceMap, Map<String, Object> newDataMap) {
		if (EmptyUtil.isEmpty(sourceMap)) {
			sourceMap = new HashMap<>();
		}
		if (EmptyUtil.isEmpty(sourceMap)) {
			return sourceMap;
		}
		sourceMap.put(SystemCalculateConstants.META_SECTION_COMPLETED, false);
		for (Entry<String, Object> e : newDataMap.entrySet()) {
			try {
				sourceMap.put(e.getKey(), Excel4ModelInAndOutUtils.getStringValue(e.getValue()));
			} catch (Exception e1) {
				logger.error(e1.getMessage());
				continue;
			}
		}
		return sourceMap;
	}

	/**
	 * 修改保存分组批量配置
	 *
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "mc/savegc", method = RequestMethod.POST)
	public ApiResult<String> saveGroupConstance(MatericalConstanceRequestVo vo) throws Exception {
		String projid = vo.getProjid();
		String groupid = vo.getGroupid();
		if (EmptyUtil.isEmpty(projid)) {
			ApiResult.error(getIntlString(I18NConstants.PROJECT_ID_IS_EMPTY)); // should be threw out ???
		}
		if (EmptyUtil.isEmpty(groupid)) {
			ApiResult.error(getIntlString(I18NConstants.GROUP_ID_IS_EMPTY)); // should be threw out ???
		}
		String pid = RestConstant.SYS_BLANK;
		MatericalConstance con = service.getConstanceByProjidAndGroupid(projid, groupid);
		if (EmptyUtil.isEmpty(con)) {
			con = new MatericalConstance();
			con.setPid(UUID.randomUUID().toString().replaceAll(RestConstant.SYS_PUNCTUATION_HYPHEN, RestConstant.SYS_BLANK));
			con.setProjid(projid);
			con.setGroupid(groupid);
			con.setProkey(vo.getProkey());
			pid = service.addConstance(con, null);
		} else {
			con.setProkey(vo.getProkey());
			pid = service.updateConstance(con, null);
		}
		formatByMatericalConstanceVo(vo, projid, groupid);
		return ApiResult.success(pid);
	}

	/**
	 * * 批量选型设置<br>
	 * 1.最经济 <br>
	 * 2.性能最优
	 *
	 * @param projectid
	 * @param groupid
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "mcset/{projectid}/{groupid}/{type}/{ahuId}", method = RequestMethod.POST)
	public ApiResult<String> matericalConstanceSetting(@PathVariable("projectid") String projectid,
			@PathVariable("groupid") String groupid, @PathVariable("type") String type, @PathVariable("ahuId") String ahuId, String userConfig) throws Exception {

		/* 1. 获取当前分组下所有的AHU */
		/* 2.取得不同分类设置下的数据（ 最经济：风机和盘管最经济的；性能最优：）赋值给含有对应段的AHU */
		/* 3.更新AHU状态为选型完成 */
		/* 4.保存AHU */
		CalcTypeEnum calcTypeEnum = CalcTypeEnum.ECONOMICAL;// 默认最经济
		if (type.equals(CalcTypeEnum.OPTIMUM.getValue())) {// 最经济
			calcTypeEnum = CalcTypeEnum.OPTIMUM;
		}else if (type.equals(CalcTypeEnum.PERFORMANCE_KEEP.getValue())){
			calcTypeEnum = CalcTypeEnum.PERFORMANCE_KEEP;
		}
		// 获取计算器开关
		boolean calculatorExist = CalContextUtil.getInstance().isCalculatorExist();
		if (calculatorExist) {
			return ApiResult.success(MessageFormat.format(
					getIntlString(I18NConstants.HAS_RUNNING_CALCULATION), calcTypeEnum.getName()));
		} else {
			// 异步执行计算
			task.runCalulator(projectid, groupid, calcTypeEnum, getLanguage(), getUserName(),ahuId,userConfig);
		}
		return ApiResult.success(MessageFormat
				.format(getIntlString(I18NConstants.BATCH_CONFIG_START_SUCCESS_TYPE), calcTypeEnum.getName()));
	}

	@SuppressWarnings("unchecked")
	private void formatByMatericalConstanceVo(MatericalConstanceRequestVo vo, String projid, String groupid)
			throws Exception {
		// 过滤批量选型的参数，以便于分别置换到对应的段信息或者AHU信息中去
		Map<String, Object> cfgMap = JSON.parseObject(vo.getProkey(), Map.class);
		Map<String, Object> ahuMap = new HashMap<>();
		Map<String, Object> mixMap = new HashMap<>();
		Map<String, Object> fanMap = new HashMap<>();
		Map<String, Object> coldMap = new HashMap<>();
		Map<String, Object> filterMap = new HashMap<>();
		Map<String, Object> compositeMap = new HashMap<>();

		Map<String, Object> dxMap = new HashMap<>();
		Map<String, Object> heatingCoilMap = new HashMap<>();
		Map<String, Object> steamCoilMap = new HashMap<>();
		Map<String, Object> electricHeatingCoilMap = new HashMap<>();
		Map<String, Object> steamHumidifierMap = new HashMap<>();
		Map<String, Object> wetfilmHumidifierMap = new HashMap<>();
		Map<String, Object> sprayHumidifierMap = new HashMap<>();
		Map<String, Object> combinedMixingChamberMap = new HashMap<>();
		Map<String, Object> attenuatorMap = new HashMap<>();
		Map<String, Object> dischargeMap = new HashMap<>();
		Map<String, Object> accessMap = new HashMap<>();
		Map<String, Object> HEPAFilterMap = new HashMap<>();
		Map<String, Object> electrodeHumidifierMap = new HashMap<>();
		Map<String, Object> electrostaticFilterMap = new HashMap<>();
		Map<String, Object> CTRMap = new HashMap<>();
		Map<String, Object> wheelHeatRecycleMap = new HashMap<>();
		Map<String, Object> plateHeatRecycleMap = new HashMap<>();

		for (Entry<String, Object> e : cfgMap.entrySet()) {
			String key = e.getKey();
			System.out.println(key);
			if (!AhuSectionMetas.getInstance().isMaterialMetaKey(key)) {
				logger.warn(MessageFormat.format("Meta [Key:{0}] is not a material parameter.", key));
				continue;
			}
			Object value = Excel4ModelInAndOutUtils.valueFilter(String.valueOf(e.getValue()));
			String sectionKey = MetaCodeGen.calculateSectionTypeIdByMetaKey(key);
			if (EmptyUtil.isEmpty(sectionKey)) {
				continue;
			}
			SectionTypeEnum ste = SectionTypeEnum.getSectionTypeFromId(sectionKey);
			if (EmptyUtil.isEmpty(ste)) {
				continue;
			}
			switch (ste) {
			case TYPE_AHU: {
				ahuMap.put(key, value);
				continue;
			}
			case TYPE_MIX: {
				mixMap.put(key, value);
				continue;
			}
			case TYPE_SINGLE: {
				filterMap.put(key, value);
				continue;
			}
			case TYPE_COMPOSITE: {
				compositeMap.put(key, value);
				continue;
			}
			case TYPE_COLD: {
				coldMap.put(key, value);
				continue;
			}
			case TYPE_FAN: {
				fanMap.put(key, value);
				continue;
			}
			case TYPE_DIRECTEXPENSIONCOIL: {
				dxMap.put(key, value);
				continue;
			}
			case TYPE_HEATINGCOIL: {
				heatingCoilMap.put(key, value);
				continue;
			}
			case TYPE_STEAMCOIL: {
				steamCoilMap.put(key, value);
				continue;
			}
			case TYPE_ELECTRICHEATINGCOIL: {
				electricHeatingCoilMap.put(key, value);
				continue;
			}
			case TYPE_STEAMHUMIDIFIER: {
				steamHumidifierMap.put(key, value);
				continue;
			}
			case TYPE_WETFILMHUMIDIFIER: {
				wetfilmHumidifierMap.put(key, value);
				continue;
			}
			case TYPE_SPRAYHUMIDIFIER: {
				sprayHumidifierMap.put(key, value);
				continue;
			}
			case TYPE_COMBINEDMIXINGCHAMBER: {
				combinedMixingChamberMap.put(key, value);
				continue;
			}
			case TYPE_ATTENUATOR: {
				attenuatorMap.put(key, value);
				continue;
			}
			case TYPE_DISCHARGE: {
				dischargeMap.put(key, value);
				continue;
			}
			case TYPE_ACCESS: {
				accessMap.put(key, value);
				continue;
			}
			case TYPE_HEPAFILTER: {
				HEPAFilterMap.put(key, value);
				continue;
			}
			case TYPE_ELECTRODEHUMIDIFIER: {
				electrodeHumidifierMap.put(key, value);
				continue;
			}
			case TYPE_ELECTROSTATICFILTER: {
				electrostaticFilterMap.put(key, value);
				continue;
			}
			case TYPE_CTR: {
				CTRMap.put(key, value);
				continue;
			}
			case TYPE_WHEELHEATRECYCLE: {
				wheelHeatRecycleMap.put(key, value);
				continue;
			}
			case TYPE_PLATEHEATRECYCLE: {
				plateHeatRecycleMap.put(key, value);
				continue;
			}
			default:
			}
		}

		List<Part> toUpdatePartList = new ArrayList<>();

		List<Unit> units = ahuService.findAhuList(projid, groupid);
		for (Unit unit : units) {
			unit.setRecordStatus(AhuStatusEnum.SELECTING.getId());
			if (!ahuMap.isEmpty()) {
				Map<String, Object> m = JSON.parseObject(unit.getMetaJson(), Map.class);
				unit.setMetaJson(JSON.toJSONString(mergeMeta(m, ahuMap)));
				UnitUtil.genSerialAndProduct(unit);
			}
			List<Part> l = sectionService.findSectionList(unit.getUnitid());
			for (Part p : l) {
				p.setRecordStatus(AhuStatusEnum.SELECTING.getId());
				Map<String, Object> m = JSON.parseObject(p.getMetaJson(), Map.class);
				switch (SectionTypeEnum.getSectionTypeFromId(p.getSectionKey())) {
				case TYPE_MIX: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, mixMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_SINGLE: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, filterMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_COMPOSITE: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, compositeMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_COLD: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, coldMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_FAN: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, fanMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_DIRECTEXPENSIONCOIL: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, dxMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_HEATINGCOIL: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, heatingCoilMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_STEAMCOIL: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, steamCoilMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_ELECTRICHEATINGCOIL: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, electricHeatingCoilMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_STEAMHUMIDIFIER: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, steamHumidifierMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_WETFILMHUMIDIFIER: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, wetfilmHumidifierMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_SPRAYHUMIDIFIER: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, sprayHumidifierMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_COMBINEDMIXINGCHAMBER: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, combinedMixingChamberMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_ATTENUATOR: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, attenuatorMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_DISCHARGE: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, dischargeMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_ACCESS: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, accessMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_HEPAFILTER: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, HEPAFilterMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_ELECTRODEHUMIDIFIER: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, electrodeHumidifierMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_ELECTROSTATICFILTER: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, electrostaticFilterMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_CTR: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, CTRMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_WHEELHEATRECYCLE: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, wheelHeatRecycleMap)));
					toUpdatePartList.add(p);
					continue;
				}
				case TYPE_PLATEHEATRECYCLE: {
					p.setMetaJson(JSON.toJSONString(mergeMeta(m, plateHeatRecycleMap)));
					toUpdatePartList.add(p);
					continue;
				}
				default:
				}
			}

		}
		ahuService.addOrUpdateAhus(units);
		sectionService.updateSections(toUpdatePartList);
		ahuStatusTool.syncStatus(projid, getUserName());
	}

	@RequestMapping(value = "project/getcalstatus/{projectId}", method = RequestMethod.POST)
	public ApiResult<Map<String, Object>> getCalStatus(@PathVariable("projectId") String projectId) {
		Map<String, Object> result = new HashMap<>();
		result.put(RestConstant.SYS_MSG_RESPONSE_TEXT, RestConstant.SYS_ASSERT_YES);
		List<BatchCfgProgress> list = progressService.findProgressList(projectId);
		Project project = projectService.getProjectById(projectId);
		List<BatchCfgProgressVO> list2Update = new ArrayList<>();
		Map<String, Object> data = new HashMap<>();
		data.put(RestConstant.SYS_SESSION_PATH_PID, project.getPid());
		data.put(RestConstant.SYS_STRING_PNAME, project.getName());
		for (BatchCfgProgress progress : list) {
			GroupInfo group = groupService.findGroupById(progress.getGroupId());
			BatchCfgProgressVO vo = new BatchCfgProgressVO();
			Map<String, Object> datao = new HashMap<>();
			datao.put(RestConstant.SYS_STRING_GROUPID, group.getGroupId());
			datao.put(RestConstant.SYS_STRING_GROUPNAME, group.getGroupName());
			datao.put(RestConstant.SYS_STRING_GROUPCODE, group.getGroupCode());
			vo.setData(datao);
			String s = progress.getJobJson();
			Object o = JSON.parse(s);
			vo.setChildren(o);
			list2Update.add(vo);
		}
		result.put(RestConstant.SYS_MSG_RESPONSE_CHILDREN, list2Update);
		result.put(RestConstant.SYS_STRING_DATA, data);
		return ApiResult.success(result);
	}

	@RequestMapping(value = "ttttttt/{projectid}", method = RequestMethod.GET)
	public ApiResult<DefaultCalContext> matericalConstanceSetting(@PathVariable("projectid") String projectid)
			throws Exception {
		DefaultCalContext ctxt = CalContextUtil.getInstance().getDefaultCalContext(projectid);
		return ApiResult.success(ctxt);
	}
}

package com.carrier.ahu.rest;

import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_PLATEHEATRECYCLE;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_WHEELHEATRECYCLE;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.calculator.PerformanceCalculator;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.engine.fan.KrugerFanLib;
import com.carrier.ahu.engine.fan.KrugerSelectInfo;
import com.carrier.ahu.engine.heatrecycle.SG200Lib;
import com.carrier.ahu.length.param.FilterPanelBean;
import com.carrier.ahu.length.param.HeatCoilQParam;
import com.carrier.ahu.length.param.HeatCoilTParam;
import com.carrier.ahu.length.param.HumidificationBean;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.calc.SUVCLight;
import com.carrier.ahu.metadata.entity.humidifier.S4HHumiQ;
import com.carrier.ahu.metadata.entity.humidifier.SJSupplier;
import com.carrier.ahu.metadata.entity.validation.S4GRow;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.ACCalculatorService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.BeanTransferUtil;
import com.carrier.ahu.util.CoilHeatAirConditionBean;
import com.carrier.ahu.util.CoilHeatCalUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.MixAirConditionBean;
import com.carrier.ahu.vo.AhuVO;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.FilterPanelVO;
import com.carrier.ahu.vo.HeatQVO;
import com.carrier.ahu.vo.HeatRecycleResultVO;
import com.carrier.ahu.vo.HumidifierCalculationResultVO;
import com.carrier.ahu.vo.MixAirConditionParam;
import com.carrier.ahu.vo.PerformanceCalVO;

import io.swagger.annotations.Api;

/**
 * ahu单元业务接口 Created by Wen zhengtao on 2017/3/16.
 */
@Api(description = "空调段配置计算数据相关接口")
@RestController
public class ACCalculatorController extends AbstractController {

    private static Logger logger = LoggerFactory.getLogger(ACCalculatorController.class.getName());

    @Autowired
    private ACCalculatorService acCalculatorService;

    /**
     * 已知干球温度，湿球温度，求湿度
     *
     * @param ParmT  干球温度
     * @param ParmTB 湿球温度
     * @return
     * @throws TempCalErrorException
     * @throws NumberFormatException
     */
    @RequestMapping(value = "calc/FAirParm1/{ParmT}/{ParmTB}", method = RequestMethod.GET)
    public ApiResult<AirConditionBean> FAirParmCalculate1(@PathVariable("ParmT") String ParmT,
                                                          @PathVariable("ParmTB") String ParmTB) throws TempCalErrorException {
        AirConditionBean bean = acCalculatorService.FAirParmCalculate1(Double.parseDouble(ParmT),
                Double.parseDouble(ParmTB), getLanguage());

        return ApiResult.success(bean);
    }

    /**
     * 已知干球温度，湿度，求湿球温度
     *
     * @param ParmT 干球温度
     * @param ParmF 湿度
     * @return
     */
    @RequestMapping(value = "calc/FAirParm3/{ParmT}/{ParmF}", method = RequestMethod.GET)
    public ApiResult<AirConditionBean> FAirParmCalculate3(@PathVariable("ParmT") String ParmT, @PathVariable("ParmF") String ParmF) throws TempCalErrorException {
        AirConditionBean bean = AirConditionUtils.FAirParmCalculate3(Double.parseDouble(ParmT),
                Double.parseDouble(ParmF));
        return ApiResult.success(bean);
    }

    /**
     * 已知新风，回风，求混合后温度
     *
     * @param airFlow      机组风量
     * @param freshAirRate 新风比
     * @param freshAirTIN  新风干球温度
     * @param freshAirTbIN 新风湿球温度
     * @param returnAirTIN 回风干球温度
     * @param returnTbIN   回风湿球温度
     * @return
     */
    @RequestMapping(value = "calc/mixAir/{airFlow}/{freshAirRate}/{freshAirTIN}/{freshAirTbIN}/{returnAirTIN}/{returnTbIN}", method = RequestMethod.GET)
    public ApiResult<AirConditionBean> mixAirCalculate(@PathVariable("airFlow") String airFlow,
                                                       @PathVariable("freshAirRate") String freshAirRate, @PathVariable("freshAirTIN") String freshAirTIN,
                                                       @PathVariable("freshAirTbIN") String freshAirTbIN, @PathVariable("returnAirTIN") String returnAirTIN,
                                                       @PathVariable("returnTbIN") String returnTbIN) throws TempCalErrorException {
        AirConditionBean bean = AirConditionUtils.mixAirCalculate(Double.parseDouble(airFlow),
                Double.parseDouble(freshAirRate), Double.parseDouble(freshAirTIN), Double.parseDouble(freshAirTbIN),
                Double.parseDouble(returnAirTIN), Double.parseDouble(returnTbIN));

        return ApiResult.success(bean);
    }

    /**
     * 回风+新风
     * 干球温度+湿球温度+新风比
     *
     * @param mixAirConditionParam
     * @return
     * @throws TempCalErrorException
     */
    @RequestMapping(value = "calc/mixAir1", method = RequestMethod.POST)
    public ApiResult<MixAirConditionBean> mixAir1(MixAirConditionParam mixAirConditionParam) throws TempCalErrorException {
//    	if (mixAirConditionParam.getInWetBulbT() > mixAirConditionParam.getInDryBulbT()
//    			||mixAirConditionParam.getNewWetBulbT()>mixAirConditionParam.getNewDryBulbT()) {//湿球不能大于干球
//			throw new TempCalErrorException(ControllerI18N.getInstance().getIntlString("ctrl_intl_1036", getLanguage()));
//		}
        MixAirConditionBean mixAirConditionBean = new MixAirConditionBean();
        mixAirConditionBean.setNARatio(BaseDataUtil.decimalConvert(mixAirConditionParam.getNARatio(), 2));//新风比
        mixAirConditionBean.setInWetBulbT(BaseDataUtil.decimalConvert(mixAirConditionParam.getInWetBulbT(), 1));//回风湿球温度
        mixAirConditionBean.setNewWetBulbT(BaseDataUtil.decimalConvert(mixAirConditionParam.getNewWetBulbT(), 1));//新风湿球温度
        AirConditionBean airConditionBeanMix = AirConditionUtils.mixAirCalculate(mixAirConditionParam.getAirVolume(), mixAirConditionParam.getNARatio(),
                mixAirConditionParam.getNewDryBulbT(), mixAirConditionBean.getNewWetBulbT(), mixAirConditionParam.getInDryBulbT(), mixAirConditionParam.getInWetBulbT());
        AirConditionBean IRT = AirConditionUtils.FAirParmCalculate1(mixAirConditionParam.getInDryBulbT(), mixAirConditionParam.getInWetBulbT());
        AirConditionBean NRT = AirConditionUtils.FAirParmCalculate1(mixAirConditionParam.getNewDryBulbT(), mixAirConditionParam.getNewWetBulbT());
        mixAirConditionBean.setNAVolume(mixAirConditionParam.getAirVolume() * mixAirConditionParam.getNARatio() / 100.0);//新风量
        mixAirConditionBean.setInRelativeT(BaseDataUtil.decimalConvert(IRT.getParmF(), 0));//回风相对湿度
        mixAirConditionBean.setNewRelativeT(BaseDataUtil.decimalConvert(NRT.getParmF(), 0));//新风相对温度
        mixAirConditionBean.setOutDryBulbT(BaseDataUtil.decimalConvert(airConditionBeanMix.getParmT(), 1));//出风干球温度
        mixAirConditionBean.setOutWetBulbT(BaseDataUtil.decimalConvert(airConditionBeanMix.getParmTb(), 1));//出风湿球温度
        mixAirConditionBean.setOutRelativeT(BaseDataUtil.decimalConvert(airConditionBeanMix.getParmF(), 0));//出风相对湿度

        return ApiResult.success(mixAirConditionBean);
    }

    /**
     * 回风+新风
     * 干球温度+相对湿度+新风比
     *
     * @param mixAirConditionParam
     * @return
     * @throws TempCalErrorException
     */
    @RequestMapping(value = "calc/mixAir2", method = RequestMethod.POST)
    public ApiResult<MixAirConditionBean> mixAir2(MixAirConditionParam mixAirConditionParam) throws TempCalErrorException {
        MixAirConditionBean mixAirConditionBean = new MixAirConditionBean();
        mixAirConditionBean.setNARatio(BaseDataUtil.decimalConvert(mixAirConditionParam.getNARatio(), 2));//新风比
        AirConditionBean ITB = AirConditionUtils.FAirParmCalculate3(mixAirConditionParam.getInDryBulbT(), mixAirConditionParam.getInRelativeT());//回风
        AirConditionBean NTB = AirConditionUtils.FAirParmCalculate3(mixAirConditionParam.getNewDryBulbT(), mixAirConditionParam.getNewRelativeT());//新风
        mixAirConditionBean.setInWetBulbT(BaseDataUtil.decimalConvert(ITB.getParmTb(), 1));//回风湿球温度
        mixAirConditionBean.setNewWetBulbT(BaseDataUtil.decimalConvert(NTB.getParmTb(), 1));//新风湿球温度
        AirConditionBean airConditionBeanMix = AirConditionUtils.mixAirCalculate(mixAirConditionParam.getAirVolume(), mixAirConditionParam.getNARatio(),
                mixAirConditionParam.getNewDryBulbT(), mixAirConditionBean.getNewWetBulbT(), mixAirConditionParam.getInDryBulbT(), mixAirConditionBean.getInWetBulbT());
        mixAirConditionBean.setNAVolume(mixAirConditionParam.getAirVolume() * mixAirConditionParam.getNARatio() / 100.0);//新风量
        mixAirConditionBean.setInRelativeT(BaseDataUtil.decimalConvert(mixAirConditionParam.getInRelativeT(), 0));//回风相对湿度
        mixAirConditionBean.setNewRelativeT(BaseDataUtil.decimalConvert(mixAirConditionParam.getNewRelativeT(), 0));//新风相对温度
        mixAirConditionBean.setOutDryBulbT(BaseDataUtil.decimalConvert(airConditionBeanMix.getParmT(), 1));//出风干球温度
        mixAirConditionBean.setOutWetBulbT(BaseDataUtil.decimalConvert(airConditionBeanMix.getParmTb(), 1));//出风湿球温度
        mixAirConditionBean.setOutRelativeT(BaseDataUtil.decimalConvert(airConditionBeanMix.getParmF(), 0));//出风相对湿度
        return ApiResult.success(mixAirConditionBean);
    }

    /**
     * 回风+新风
     * 干球温度+湿球温度+新风量
     *
     * @param mixAirConditionParam
     * @return
     * @throws TempCalErrorException
     */
    @RequestMapping(value = "calc/mixAir4", method = RequestMethod.POST)
    public ApiResult<MixAirConditionBean> mixAir4(MixAirConditionParam mixAirConditionParam) throws TempCalErrorException {
        MixAirConditionBean mixAirConditionBean = new MixAirConditionBean();
        //新风比公式
        double NARatio = BaseDataUtil.decimalConvert(mixAirConditionParam.getNAVolume() / mixAirConditionParam.getAirVolume() * 100, 2);
        mixAirConditionBean.setNARatio(NARatio);//新风比
        mixAirConditionBean.setInWetBulbT(BaseDataUtil.decimalConvert(mixAirConditionParam.getInWetBulbT(), 1));//回风湿球温度
        mixAirConditionBean.setNewWetBulbT(BaseDataUtil.decimalConvert(mixAirConditionParam.getNewWetBulbT(), 1));//新风湿球温度
        AirConditionBean airConditionBeanMix = AirConditionUtils.mixAirCalculate(mixAirConditionParam.getAirVolume(), NARatio,
                mixAirConditionParam.getNewDryBulbT(), mixAirConditionBean.getNewWetBulbT(), mixAirConditionParam.getInDryBulbT(), mixAirConditionParam.getInWetBulbT());
        AirConditionBean IRT = AirConditionUtils.FAirParmCalculate1(mixAirConditionParam.getInDryBulbT(), mixAirConditionParam.getInWetBulbT());
        AirConditionBean NRT = AirConditionUtils.FAirParmCalculate1(mixAirConditionParam.getNewDryBulbT(), mixAirConditionParam.getNewWetBulbT());
        mixAirConditionBean.setNAVolume(BaseDataUtil.decimalConvert(mixAirConditionParam.getNAVolume(), 0));//新风量
        mixAirConditionBean.setInRelativeT(BaseDataUtil.decimalConvert(IRT.getParmF(), 0));//回风相对湿度
        mixAirConditionBean.setNewRelativeT(BaseDataUtil.decimalConvert(NRT.getParmF(), 0));//新风相对湿度
        mixAirConditionBean.setOutDryBulbT(BaseDataUtil.decimalConvert(airConditionBeanMix.getParmT(), 1));//出风干球温度
        mixAirConditionBean.setOutWetBulbT(BaseDataUtil.decimalConvert(airConditionBeanMix.getParmTb(), 1));//出风湿球温度
        mixAirConditionBean.setOutRelativeT(BaseDataUtil.decimalConvert(airConditionBeanMix.getParmF(), 0));//出风相对湿度
        return ApiResult.success(mixAirConditionBean);
    }

    /**
     * 求(等焓加湿段)加湿量
     * 根据干球温度、湿球温度、相对湿度 求出风：干球温度、湿球温度、相对湿度、加湿量
     * inDryBulbT：干球温度
     * inWetBulbT：湿球温度
     * RH:相对湿度（加湿比）
     * eairvolume：风量
     */
    @RequestMapping(value = "calc/IsoenthalpyHumidificationRH/{inDryBulbT}/{inWetBulbT}/{RH}/{eairvolume}", method = RequestMethod.GET)
    public ApiResult<HumidificationBean> IsoenthalpyRHConvert(@PathVariable("inDryBulbT") String inDryBulbT, @PathVariable("inWetBulbT") String inWetBulbT,
                                                              @PathVariable("RH") String RH, @PathVariable("eairvolume") String eairvolume) throws TempCalErrorException {
        HumidificationBean humidificationBean = acCalculatorService.IsoenthalpyRHConvert(BaseDataUtil.stringConversionDouble(inDryBulbT), BaseDataUtil.stringConversionDouble(inWetBulbT),
                BaseDataUtil.stringConversionDouble(RH), BaseDataUtil.stringConversionInteger(eairvolume), getLanguage());
        return ApiResult.success(humidificationBean);
    }

    /**
     * 求(等温加湿段)加湿量
     * 根据干球温度、湿球温度、相对湿度 求出风：干球温度、湿球温度、相对湿度、加湿量
     * inDryBulbT：干球温度
     * inWetBulbT：湿球温度
     * RH:相对湿度（加湿比）
     * eairvolume：风量
     */
    @RequestMapping(value = "calc/IsothermalHumidificationRH/{inDryBulbT}/{inWetBulbT}/{RH}/{eairvolume}", method = RequestMethod.GET)
    public ApiResult<HumidificationBean> IsothermalRHConvert(@PathVariable("inDryBulbT") String inDryBulbT, @PathVariable("inWetBulbT") String inWetBulbT,
                                                             @PathVariable("RH") String RH, @PathVariable("eairvolume") String eairvolume) throws TempCalErrorException {
        HumidificationBean humidificationBean = acCalculatorService.IsothermalRHConvert(BaseDataUtil.stringConversionDouble(inDryBulbT), BaseDataUtil.stringConversionDouble(inWetBulbT),
                BaseDataUtil.stringConversionDouble(RH), BaseDataUtil.stringConversionInteger(eairvolume), getLanguage());
        return ApiResult.success(humidificationBean);
    }

    /**
     * 求(等焓加湿段)相对湿度
     * 根据干球温度、湿球温度 加湿量 求出风：干球温度、湿球温度、相对湿度、加湿量
     * inDryBulbT：干球温度
     * inWetBulbT：湿球温度
     * humidification:加湿量（加湿比）
     * eairvolume：风量
     */
    @RequestMapping(value = "calc/IsoenthalpyHumidification/{inDryBulbT}/{inWetBulbT}/{humidification}/{eairvolume}", method = RequestMethod.GET)
    public ApiResult<HumidificationBean> IsoenthalpyHumidificationConvert(@PathVariable("inDryBulbT") String inDryBulbT, @PathVariable("inWetBulbT") String inWetBulbT,
                                                                          @PathVariable("humidification") String humidification, @PathVariable("eairvolume") String eairvolume) throws TempCalErrorException {
        HumidificationBean humidificationBean = acCalculatorService.IsoenthalpyHumidificationConvert(BaseDataUtil.stringConversionDouble(inDryBulbT), BaseDataUtil.stringConversionDouble(inWetBulbT),
                BaseDataUtil.stringConversionDouble(humidification), BaseDataUtil.stringConversionInteger(eairvolume), getLanguage());
        return ApiResult.success(humidificationBean);
    }

    /**
     * 求(等温加湿段)相对湿度
     * 根据干球温度、湿球温度 加湿量 求出风：干球温度、湿球温度、相对湿度、加湿量
     * inDryBulbT：干球温度
     * inWetBulbT：湿球温度
     * humidification:加湿量（加湿比）
     * eairvolume：风量
     */
    @RequestMapping(value = "calc/IsothermalHumidification/{inDryBulbT}/{inWetBulbT}/{humidification}/{eairvolume}", method = RequestMethod.GET)
    public ApiResult<HumidificationBean> IsothermalHumidificationConvert(@PathVariable("inDryBulbT") String inDryBulbT, @PathVariable("inWetBulbT") String inWetBulbT,
                                                                         @PathVariable("humidification") String humidification, @PathVariable("eairvolume") String eairvolume) throws TempCalErrorException {
        HumidificationBean humidificationBean = acCalculatorService.IsothermalHumidificationConvert(BaseDataUtil.stringConversionDouble(inDryBulbT), BaseDataUtil.stringConversionDouble(inWetBulbT),
                BaseDataUtil.stringConversionDouble(humidification), BaseDataUtil.stringConversionInteger(eairvolume), getLanguage());
        return ApiResult.success(humidificationBean);
    }

    /**
     * 高压喷雾加湿段页面选择型号
     * season:季节
     * fEfficiencyS：夏季加湿量
     * fEfficiencyW：冬季加湿量
     * supplier：供应商 默认B
     */
    @RequestMapping(value = "calc/performanceJ", method = RequestMethod.POST)
    public ApiResult<HumidifierCalculationResultVO<SJSupplier>> performanceJ(PerformanceCalVO performanceCalVO) throws TempCalErrorException {
        List<SJSupplier> sjSupplierPoList = PerformanceCalculator.getPerformanceJ(
                performanceCalVO.getSeason(), BaseDataUtil.stringConversionDouble(performanceCalVO.getFEfficiencyS()),
                BaseDataUtil.stringConversionDouble(performanceCalVO.getFEfficiencyW()), performanceCalVO.getSupplier());
        HumidifierCalculationResultVO<SJSupplier> vo = new HumidifierCalculationResultVO<SJSupplier>();
        vo.setSeason(performanceCalVO.getSeason());
        vo.setResultData(sjSupplierPoList);
        vo.setSectionKey(MetaCodeGen.calculateAttributePrefix(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId()));
        return ApiResult.success(vo);
    }

    /**
     * 干蒸汽加湿段
     * season:季节
     * humidificationS：夏季加湿量
     * humidificationW：冬季加湿量
     * supplier：供应商 默认B
     */
    @RequestMapping(value = "calc/performanceH", method = RequestMethod.POST)
    public ApiResult<HumidifierCalculationResultVO<S4HHumiQ>> performanceH(PerformanceCalVO performanceCalVO) {
        if (EmptyUtil.isEmpty(performanceCalVO.getFEfficiencyS())) {
            performanceCalVO.setFEfficiencyS("0");
        }
        if (EmptyUtil.isEmpty(performanceCalVO.getFEfficiencyW())) {
            performanceCalVO.setFEfficiencyW("0");
        }
//        List<S4HHumiQ> s4HHumiQPoList = PerformanceCalculator.getPerformanceH(performanceCalVO.getSeason(), BaseDataUtil.stringConversionDouble(performanceCalVO.getFEfficiencyS()),
//                BaseDataUtil.stringConversionDouble(performanceCalVO.getFEfficiencyW()), performanceCalVO.getSupplier());
        List<S4HHumiQ> s4HHumiQPoList = AhuMetadata.findList(S4HHumiQ.class, "B");
        List<S4HHumiQ> newS4HHumiQPoList = new ArrayList<S4HHumiQ>();
        double inputVaporPressure = BaseDataUtil.stringConversionDouble(performanceCalVO.getInputVaporPressure());
        for (S4HHumiQ s4HHumiQ : s4HHumiQPoList) {
            if (inputVaporPressure == s4HHumiQ.getVaporPressure()) {
                newS4HHumiQPoList.add(s4HHumiQ);
            }
        }
        HumidifierCalculationResultVO<S4HHumiQ> vo = new HumidifierCalculationResultVO<S4HHumiQ>();
        vo.setSeason(performanceCalVO.getSeason());
        vo.setResultData(newS4HHumiQPoList);
        vo.setSectionKey(MetaCodeGen.calculateAttributePrefix(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId()));
        return ApiResult.success(vo);
    }

    /**
     * 蒸汽盘管、电加热盘管
     *
     * @param heatCoilQParam
     * @return 根据热量计算
     */
    @RequestMapping(value = "calc/heatCoilQ", method = RequestMethod.POST)
    public ApiResult<HeatQVO> calHeatByHeatQ(HeatCoilQParam heatCoilQParam) throws TempCalErrorException {
        int maxHeatQ = 0;
        if (!EmptyUtil.isEmpty(heatCoilQParam.getSerial())) {
            S4GRow s4gRow = AhuMetadata.findOne(S4GRow.class, heatCoilQParam.getSerial());
            maxHeatQ = (s4gRow != null) ? s4gRow.getRow3() : 0;
        }
        if (maxHeatQ < heatCoilQParam.getHeatQ()) {//超过最大加热量热量
            logger.error("ACCalculatorController calHeatByHeatQ maxHeatQ :" + maxHeatQ);
//            throw new CalculationException(ErrorCode.HEAT_EXCEEDS_THE_MAXIMUM);//TODO 异常消息整理暂时没有定义 批量中也需要添加异常消息
        }
        CoilHeatAirConditionBean coilHeatAirConditionBean = CoilHeatCalUtil.calHeatByHeatQ(heatCoilQParam.getAirVolume(), heatCoilQParam.getInDryBulbT(), heatCoilQParam.getInWetBulbT(), heatCoilQParam.getHeatQ());
        HeatQVO heatQVO = new HeatQVO();
        heatQVO.setHeatQ(heatCoilQParam.getHeatQ());//热量
        heatQVO.setHumDryBulbT(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getOutputT(), 1));//加湿比干球温度
        heatQVO.setOutDryBulbT(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getParmT(), 1));//干球温度
        heatQVO.setOutWetBulbT(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getParmTb(), 1));//湿球温度
        heatQVO.setOutRelativeT(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getParmF(), 0));//相对湿度
        heatQVO.setSectionKey(heatCoilQParam.getSectionKey());//页面标识
        return ApiResult.success(heatQVO);
    }

    /**
     * 蒸汽盘管、电加热盘管
     *
     * @param heatCoilTParam
     * @return 根据加湿比的干球温度计算
     */
    @RequestMapping(value = "calc/heatCoilT", method = RequestMethod.POST)
    public ApiResult<HeatQVO> calHeatByHeatT(HeatCoilTParam heatCoilTParam) throws TempCalErrorException {
        CoilHeatAirConditionBean coilHeatAirConditionBean = CoilHeatCalUtil.calHeatByOutT(heatCoilTParam.getAirVolume(), heatCoilTParam.getInDryBulbT(), heatCoilTParam.getInWetBulbT(), heatCoilTParam.getHumDryBulbT());
        HeatQVO heatQVO = new HeatQVO();
        Double humDryBulbT = BaseDataUtil.decimalConvert(heatCoilTParam.getHumDryBulbT(), 1);
        heatQVO.setHumDryBulbT(humDryBulbT);//加湿比干球温度
        heatQVO.setHeatQ(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getHeatQ(), 2));//热量
        heatQVO.setOutDryBulbT(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getParmT(), 1));//干球温度
        heatQVO.setOutWetBulbT(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getParmTb(), 1));//湿球温度
        heatQVO.setOutRelativeT(BaseDataUtil.decimalConvert(coilHeatAirConditionBean.getParmF(), 0));//相对湿度
        heatQVO.setSectionKey(heatCoilTParam.getSectionKey());//页面标识
        return ApiResult.success(heatQVO);
    }


    /**
     * 过滤段计算面板布置
     *
     * @param filterPanelVO
     * @return
     */
    @RequestMapping(value = "calc/filterPanel", method = RequestMethod.POST)
    public ApiResult<HumidifierCalculationResultVO<FilterPanelBean>> filterPanel(FilterPanelVO filterPanelVO) {
        if ("No Filter".equals(filterPanelVO.getRMaterial()) && "No".equals(filterPanelVO.getLMaterial())) {
            return null;
        }
        List<FilterPanelBean> filterPanelBeanList = PerformanceCalculator.filterPanelBeanList(filterPanelVO.getSerial(), filterPanelVO.getSectionType(), filterPanelVO.getMediaLoading());
        HumidifierCalculationResultVO<FilterPanelBean> vo = new HumidifierCalculationResultVO<FilterPanelBean>();
        vo.setResultData(filterPanelBeanList);
        if (SectionTypeEnum.TYPE_SINGLE.getCode().equals(filterPanelVO.getSectionType())) {//单层过滤
            vo.setSectionKey(MetaCodeGen.calculateAttributePrefix(SectionTypeEnum.TYPE_SINGLE.getId()));
        } else if (SectionTypeEnum.TYPE_COMPOSITE.getCode().equals(filterPanelVO.getSectionType())) {//综合过滤
            vo.setSectionKey(MetaCodeGen.calculateAttributePrefix(SectionTypeEnum.TYPE_COMPOSITE.getId()));
        } else if (SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getCode().equals(filterPanelVO.getSectionType())) {//静电过滤
            vo.setSectionKey(MetaCodeGen.calculateAttributePrefix(SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getId()));
        } else if (SectionTypeEnum.TYPE_HEPAFILTER.getCode().equals(filterPanelVO.getSectionType())) {//高效过滤
            vo.setSectionKey(MetaCodeGen.calculateAttributePrefix(SectionTypeEnum.TYPE_HEPAFILTER.getId()));
        }

        return ApiResult.success(vo);
    }

    @RequestMapping(value = "calc/NAVolume/{aVolume}/{naVolume}", method = RequestMethod.GET)
    public ApiResult<Integer> sNAVolume(@PathVariable("aVolume") String aVolume, @PathVariable("naVolume") String naVolume) throws TempCalErrorException {
        //根据新风风量计算回风风量
        double raVolume = BaseDataUtil.stringConversionDouble(aVolume) - BaseDataUtil.stringConversionDouble(naVolume);
        return ApiResult.success(BaseDataUtil.doubleConversionInteger(raVolume));
    }

    @RequestMapping(value = "calc/RAVolume/{aVolume}/{raVolume}", method = RequestMethod.GET)
    public ApiResult<Integer> rNAVolume(@PathVariable("aVolume") String aVolume, @PathVariable("raVolume") String raVolume) throws TempCalErrorException {
        //根据回风风量计算新风风量
        double naVolume = BaseDataUtil.stringConversionDouble(aVolume) - BaseDataUtil.stringConversionDouble(raVolume);
        return ApiResult.success(BaseDataUtil.doubleConversionInteger(naVolume));
    }

    /*杀菌灯灯型号计算*/
    @RequestMapping(value = "calc/uv/{serial}", method = RequestMethod.GET)
    public ApiResult<String> getUV(@PathVariable("serial") String serial) throws Exception {
        SUVCLight suvclight = AhuMetadata.findOne(SUVCLight.class, serial);
        String uv = "";
        if (null != suvclight) {
            uv = suvclight.getType() + "*" + suvclight.getQuan();
        }
        return ApiResult.success(uv);
    }

    @RequestMapping(value = "test_kruger", method = RequestMethod.GET)
    public ApiResult<KrugerSelectInfo> getKruger() throws Exception {
        SG200Lib.getInstance().calculate(true, 36.0, 36.0, 36.0, 36.0, 36.0, 36.0, true, 36.0);
        KrugerSelectInfo krugerSelectInfo = KrugerFanLib.getInstance().calculate(new KrugerSelectInfo());
        return ApiResult.success(krugerSelectInfo);
    }

    /**
     * 转轮热回收段页面计算
     */
    @RequestMapping(value = "calc/heatrecycle", method = RequestMethod.POST)
    public ApiResult<HeatRecycleResultVO> calcHeatRecycle(AhuVO ahuVO) throws Exception {
        AhuParam ahuParam = BeanTransferUtil.getAhuParam(ahuVO);
        List<PartParam> partParams = ahuParam.getPartParams().stream()
                .filter(part -> TYPE_WHEELHEATRECYCLE.getId().equals(part.getKey())
                        || TYPE_PLATEHEATRECYCLE.getId().equals(part.getKey()))
                .collect(Collectors.toList());
        PartParam partParam = partParams.get(0);
        return ApiResult.success(this.acCalculatorService.calculateHeatRecycleInfo(ahuParam, partParam));
    }

}

package com.carrier.ahu.listener;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.carrier.ahu.AHUSpringContextUtil;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.license.LicenseInfo;
import com.carrier.ahu.license.LicenseManager;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.UserService;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Init AHUContext content, User, UserMeta and etc.
 * 
 * Created by Braden Zhou on 2018/08/07.
 */
@Component
public class AhuContextListener implements ApplicationListener<ContextRefreshedEvent> {

    private static Logger logger = LoggerFactory.getLogger(AhuContextListener.class);

    @Autowired
    private UserService userService;

    @Value("${ahu.version}")
    private String ahuVersion;

    @Value("${ahu.patch.version}")
    private String ahuPatchVersion;

    public AhuContextListener() {
        logger.debug(RestConstant.LOG_AHU_CONTEXT_LISTENER_INIT);
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        List<UserMeta> userMetas = new ArrayList<>();
        LicenseInfo licenseInfo = LicenseManager.getLicenseInfo();
        try {
            boolean isDevUser = AHUSpringContextUtil.isDevProfile();
            if (LicenseManager.validateLicenseInfo(licenseInfo, ahuVersion, isDevUser)
                    || EmptyUtil.isNotEmpty(licenseInfo)) {
                User user = this.userService.getLocalUser(licenseInfo);
                user.setDefaultParaPrefer(TemplateUtil.getDefaultParameterFromMeta());
                AHUContext.setUser(user);
                userMetas = this.userService.findUserMetaByUserAndFactory(licenseInfo.getUserName(),
                        licenseInfo.getFactory());
            }
        } catch (Exception e) {
            logger.error(RestConstant.LOG_FAILED_TO_VALIDATE_LICENSE_INFO);
        }
        AHUContext.setAhuVersion(ahuVersion);
        AHUContext.setAhuPatchVersion(ahuPatchVersion);
        AHUContext.setUserMeta(userMetas);
    }

}

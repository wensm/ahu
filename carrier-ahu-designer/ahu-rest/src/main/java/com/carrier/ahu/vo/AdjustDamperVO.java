package com.carrier.ahu.vo;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;

import lombok.Data;

@Data
public class AdjustDamperVO {

    private Unit unit;
    private Part section;

}

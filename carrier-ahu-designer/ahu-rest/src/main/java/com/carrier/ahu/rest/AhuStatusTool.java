package com.carrier.ahu.rest;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * AHU的初始状态为：正在选型（after 新建项目），导入完成（after 导入项目）</br>
 * 后三步依次为：选型完成，分段完成，面板布置完成 </br>
 * 最终为已归档 </br>
 * <br>
 * 机组状态为其拥有的所有段的最低状态<br>
 * 项目状态为其拥有的所有机组的最低状态<br>
 * <br>
 * <br>
 */
@Slf4j
@Component
public class AhuStatusTool {
	@Autowired
	private ProjectService projectService;
	@Autowired
	private AhuService ahuService;
	@Autowired
	private SectionService sectionService;

	public void syncStatus(String projectid, String userName) {
		log.info(MessageFormat.format("Project [id:{0}] all status synchronous starting ......", projectid));
		Project project = projectService.getProjectById(projectid);
		String projectStatus = project.getRecordStatus();
		if (EmptyUtil.isEmpty(project)) {
			log.warn(MessageFormat.format("Project [id:{0}] all status synchronous exit for the project is not exist",
					projectid));
			return;
		}
		List<Unit> unitList = ahuService.findAhuList(projectid, null);
		if (EmptyUtil.isEmpty(unitList)) {
			log.warn(MessageFormat.format("Project [id:{0}] all status synchronous exit for the project has no units",
					projectid));
			return;
		}
		List<String> unitStatusList = new ArrayList<String>();
		List<Unit> toUpdateUnitList = new ArrayList<>();
		for (Unit unit : unitList) {
			String ahuStatus = unit.getRecordStatus();
			List<Part> partList = sectionService.findSectionList(unit.getUnitid());
			if (EmptyUtil.isEmpty(partList)) {
				unitStatusList.add(unit.getRecordStatus());
				continue;
			}
			List<String> partStatusList = new ArrayList<String>();
			for (Part part : partList) {
				if (EmptyUtil.isEmpty(part.getRecordStatus())) {
					partStatusList.add(AhuStatusEnum.SELECTING.getId());
					continue;
				}
				partStatusList.add(part.getRecordStatus());
			}
			String partLowestStatus = getLowestStatusSection(ahuStatus, partStatusList);
			if (null == partLowestStatus) {
				unitStatusList.add(ahuStatus);
			} else {
				if (!partLowestStatus.equals(unit.getRecordStatus())) {
					unit.setRecordStatus(partLowestStatus);
					toUpdateUnitList.add(unit);
				}
				unitStatusList.add(partLowestStatus);
			}
		}
		ahuService.addOrUpdateAhus(toUpdateUnitList);
		log.info(MessageFormat.format("Project [id:{0}] all status synchronous [All AHUs] success", projectid));
		String unitLowestStatus = getLowestStatusAhu(projectStatus, unitStatusList);
		if (EmptyUtil.isNotEmpty(unitLowestStatus)) {
			if (!unitLowestStatus.equals(project.getRecordStatus())) {
				project.setRecordStatus(unitLowestStatus);
				projectService.updateProject(project, userName);
				log.info(MessageFormat.format("Project [id:{0}] all status synchronous [Project] success.", projectid));
				return;
			}
		}
		log.info(MessageFormat.format("Project [id:{0}] all status synchronous end.", projectid));
	}

    private static String getLowestStatusAhu(String projectStatus, List<String> ahuStatusList) {
        if (EmptyUtil.isEmpty(ahuStatusList)) {
            return null;
        }
        String temp = ahuStatusList.get(0);
        boolean allSameFlag = true;
        for (String sts : ahuStatusList) {
            if (EmptyUtil.isNotEmpty(sts) && !sts.equals(temp)) {
                allSameFlag = false;
                break;
            }
        }

        if (allSameFlag) {
            if (projectStatus.equals(AhuStatusEnum.ARCHIVED.getId())) {
                return projectStatus;
            } else {
                return temp;
            }
        } else {
            if (ahuStatusList.contains(AhuStatusEnum.SELECTING.getId())) {
                if (ahuStatusList.contains(AhuStatusEnum.IMPORT_COMPLETE.getId())) {
                    return AhuStatusEnum.IMPORT_COMPLETE.getId();
                }
                return AhuStatusEnum.SELECTING.getId();
            }
            if (ahuStatusList.contains(AhuStatusEnum.IMPORT_COMPLETE.getId())) {
                return AhuStatusEnum.IMPORT_COMPLETE.getId();
            }
            if (ahuStatusList.contains(AhuStatusEnum.SELECT_COMPLETE.getId())) {
                return AhuStatusEnum.SELECT_COMPLETE.getId();
            }
            if (ahuStatusList.contains(AhuStatusEnum.SPLIT_COMPLETE.getId())) {
                return AhuStatusEnum.SPLIT_COMPLETE.getId();
            }
            if (ahuStatusList.contains(AhuStatusEnum.PANEL_COMPLETE.getId())) {
                return AhuStatusEnum.PANEL_COMPLETE.getId();
            }
            if (ahuStatusList.contains(AhuStatusEnum.ARCHIVED.getId())) {
                return AhuStatusEnum.ARCHIVED.getId();
            }
            log.warn("All AHUs' status synchronous return null for the List has no status in AhuStatusEnum");
            return null;
        }
    }

    /**
     * 获取段的最低状态<br>
     * 段只有两种状态：正在选型 & 选型完成
     * 
     * @param statusList
     * @return
     */
    private static String getLowestStatusSection(String ahuStatus, List<String> sectionStatusListList) {
        if (EmptyUtil.isEmpty(sectionStatusListList)) {
            return null;
        }

        String temp = sectionStatusListList.get(0);
        boolean allSameFlag = true;
        for (String sts : sectionStatusListList) {
            if (!sts.equals(temp)) {
                allSameFlag = false;
                break;
            }
        }

        if (allSameFlag) {
            if (temp.equals(AhuStatusEnum.SELECTING.getId())) {
                return AhuStatusEnum.SELECTING.getId();
            } else {
                return ahuStatus;
            }
        } else {
            if (sectionStatusListList.contains(AhuStatusEnum.SELECTING.getId())) {
                return AhuStatusEnum.SELECTING.getId();
            }
            if (sectionStatusListList.contains(AhuStatusEnum.SELECT_COMPLETE.getId())) {
                return AhuStatusEnum.SELECT_COMPLETE.getId();
            }
            log.warn("All sections' status synchronous return null for the List has no status in AhuStatusEnum");
            return null;
        }
    }


    public void resetStatus(String projectId, String userName) {
        Project project = projectService.getProjectById(projectId);
        project.setRecordStatus(AhuStatusEnum.SELECTING.getId());

        List<Unit> unitList = ahuService.findAhuList(projectId, null);
        unitList.forEach(this::clearWeightAndPrice);
        ahuService.addOrUpdateAhus(unitList);
        projectService.updateProject(project, userName);
    }

    private void clearWeightAndPrice(Unit unit) {
        Map<String, Object> metadata = new Gson().fromJson(unit.getMetaJson(), new TypeToken<Map<String, Object>>() {
        }.getType());

        String weightKey = SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_WEIGHT);
        if (metadata.containsKey(weightKey)) {
            metadata.put(weightKey, CommonConstant.SYS_BLANK);
        }

        String priceKey = SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_PRICE);
        if (metadata.containsKey(priceKey)) {
            metadata.put(priceKey, CommonConstant.SYS_BLANK);
        }
        unit.setMetaJson(new Gson().toJson(metadata));
        unit.setWeight(null);
        unit.setPrice(null);
        unit.setRecordStatus(AhuStatusEnum.SELECTING.getId());

        List<Part> partList = sectionService.findSectionList(unit.getUnitid());
        partList.forEach(this::clearPartStatus);
        sectionService.updateSections(partList);
    }

    private void clearPartStatus(Part part) {
        Map<String, Object> metadata = new Gson().fromJson(part.getMetaJson(), new TypeToken<Map<String, Object>>() {
        }.getType());

        if (metadata.containsKey(SystemCalculateConstants.META_SECTION_COMPLETED)) {
            metadata.put(SystemCalculateConstants.META_SECTION_COMPLETED, false);
        }
        part.setMetaJson(new Gson().toJson(metadata));
        part.setRecordStatus(AhuStatusEnum.SELECTING.getId());
    }

}

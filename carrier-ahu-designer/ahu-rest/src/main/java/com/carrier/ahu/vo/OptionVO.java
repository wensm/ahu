package com.carrier.ahu.vo;

import lombok.Data;

/**
 * For select options.
 * 
 * Created by Braden Zhou on 2018/11/12.
 */
@Data
public class OptionVO {

    private String key;
    private String value;

    public OptionVO(String key, String value) {
        this.key = key;
        this.value = value;
    }

}

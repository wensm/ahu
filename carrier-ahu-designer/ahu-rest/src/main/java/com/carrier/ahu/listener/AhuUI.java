package com.carrier.ahu.listener;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.boot.SpringApplication;

import com.carrier.ahu.browser.WebBrowserPanel;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.VersionTypeEnum;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.util.EmptyUtil;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserAdapter;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserCommandEvent;
import lombok.extern.slf4j.Slf4j;

/**
 * Extract from AhuFrontendListener.
 * 
 * Created by Braden Zhou on 2018/11/22.
 */
@Slf4j
public class AhuUI {

    public interface AhuUIListener {

        public void uiStarted();

    }

    public static WebBrowserPanel wPanel;
    public static JFrame frame = new JFrame(RestConstant.SYS_INDEX_TITLE);

    public static void showAhuUI(int port) {
        showAhuUI(port, null);
    }

    public static void showAhuUI(int port, AhuUIListener ahuUIListener) {
        try {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (EmptyUtil.isEmpty(AhuUI.wPanel)) {
                        log.info(RestConstant.LOG_QUIT_WITHOUT_CLIENT);
                    } else {
                        String indexUrl = RestConstant.SYS_INDEX_URL.replace(RestConstant.SYS_KEY_PORT,
                                String.valueOf(port));
                        final String url = String.format("%s?_=%s", indexUrl, System.currentTimeMillis());
                        log.info(RestConstant.LOG_CLIENT_BROWSER_URL + url);

                        AhuUI.showVersionSelection();
                        AhuUI.wPanel.addWebBrowserListener(new WebBrowserAdapter() {
                            @Override
                            public void commandReceived(WebBrowserCommandEvent event) {
                                String command = event.getCommand();
                                // receive sendNSCommand from JWebBrowser
                                if (VersionTypeEnum.DOMESTIC.name().equalsIgnoreCase(command)) {
                                    AHUContext.setVersionType(VersionTypeEnum.DOMESTIC);
                                    AhuUI.wPanel.startChrome(url);
                                } else if (VersionTypeEnum.EXPORT.name().equalsIgnoreCase(command)) {
                                    AHUContext.setVersionType(VersionTypeEnum.EXPORT);
                                    AhuUI.wPanel.startChrome(url);
                                }
                            }
                        });
                    }
                    if (ahuUIListener != null) {
                        ahuUIListener.uiStarted();
                    }
                }
            };
            SwingUtilities.invokeLater(runnable);
        } catch (Exception e) {
            log.error(RestConstant.LOG_BLANK, e);
        }
    }

    public static void openFrontendWindow() {
        NativeInterface.open();
        UIUtils.setPreferredLookAndFeel();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                wPanel = new WebBrowserPanel("");
                wPanel.setContent(getHtmlContent(RestConstant.SYS_STARTING_URL));
                InputStream is = AhuUI.class.getResourceAsStream(RestConstant.SYS_RESOURCE_CARRIER_LOGO);
                byte[] bytes = null;
                try {
                    bytes = new byte[is.available()];
                    is.read(bytes);
                } catch (IOException e) {
                    log.error("failed to read logo", e);
                }
                frame.setIconImage(frame.getToolkit().createImage(bytes));
                frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                frame.getContentPane().add(wPanel, BorderLayout.CENTER);
                // 默认全屏
                // frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
                // 设置窗口最小大小
                // frame.setUndecorated(true);
                frame.setMinimumSize(new Dimension(750, 650));
                // 设置居中
                frame.setLocationRelativeTo(null);

                frame.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosed(WindowEvent e) {
                        if (EmptyUtil.isNotEmpty(AhuFrontendListener.context)) {
                            SpringApplication.exit(AhuFrontendListener.context);
                        }
                    }

                    @Override
                    public void windowClosing(WindowEvent e) {
                        int selected = JOptionPane.showConfirmDialog(frame,
                                AHUContext.getIntlString(I18NConstants.EXIT_OR_NOT));
                        if (JOptionPane.OK_OPTION == selected) {
                            System.exit(0);
                        } else {
                            return;
                        }
                    }
                });
                frame.setVisible(true);
            }
        });
    }

    public static void showVersionSelection() {
        String content = getHtmlContent(RestConstant.SYS_VERSION_URL);
        String fileFolder = System.getProperty(RestConstant.SYS_PROPERTY_USER_DIR)
                .concat(RestConstant.SYS_PATH_WELCOMPAGE_IMG);
        content = content.replace(RestConstant.SYS_STATIC_FOLDER, fileFolder);
        wPanel.setContent(content);
    }

    public static String getHtmlContent(String resource) {
        try {
            byte[] bytes;
            InputStream is = AhuUI.class.getResourceAsStream(resource);
            bytes = new byte[is.available()];
            IOUtils.readFully(is, bytes);
            return new String(bytes, RestConstant.SYS_ENCODING_UTF8);
        } catch (IOException e) {
            log.error("failed to load html", e);
        }
        return StringUtils.EMPTY;
    }

    public static void showMessageAndExit(String message) {
        JOptionPane.showMessageDialog(frame, message);
        System.exit(0);
    }

}

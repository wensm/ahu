package com.carrier.ahu.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by LIANGD4 on 2017/12/9.
 */
@Data
public class FilterPanelVO implements Serializable {

    private String sectionType;//段code B:单层过滤段 c:综合过滤段 Y静电过滤段 V:高效过滤段
    private String serial;//机组型号
    private String MediaLoading;//抽取方式

    /*单层过滤器*/
    private String FilterEfficiency;//过滤器选项 无过滤器：No

    /*综合过滤器*/
    private String RMaterial;//袋式 过滤器选项 无过滤器：No TODO No Filter
    private String LMaterial;//板式 过滤器选项 无过滤器：No

    /*静电过滤器：无选项*/

    /*高效过滤器*/
    private String Supplier;//供应商 无过滤器:none


}

package com.carrier.ahu.rest;

import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.vo.ApiResult;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统日志用户分组
 * Created by Wen zhengtao on 2017/3/16.
 */
@RestController
public class LoggerController extends AbstractController {

    @RequestMapping(value="/log/{userName}",method = RequestMethod.GET)
    public ApiResult<String> log(@PathVariable("userName") String userName) {
        MDC.put(RestConstant.SYS_USER_NAME,userName);
        logger.debug("logger by user "+userName);
        return ApiResult.success(RestConstant.SYS_MSG_RESPONSE_OK);
    }
}

package com.carrier.ahu.rest.websocket;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.springframework.stereotype.Component;

import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.service.cal.CalContextUtil;
import com.carrier.ahu.util.EmptyUtil;

@ServerEndpoint(value = "/websocket/{pid}")
@Component
public class MyWebSocket {
	// 静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
	private static int onlineCount = 0;

	// concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
	private static CopyOnWriteArraySet<MyWebSocket> webSocketSet = new CopyOnWriteArraySet<MyWebSocket>();

	// 与某个客户端的连接会话，需要通过它来给客户端发送数据
	private Session session;

	private static Map<String, Session> sessionMap = new HashMap<>();

	/**
	 * 连接建立成功调用的方法
	 */
	@OnOpen
	public void onOpen(Session session) {
		this.session = session;
		String projectid = session.getPathParameters().get(RestConstant.SYS_SESSION_PATH_PID);
		System.out.println(projectid);
		sessionMap.put(projectid, session);
		webSocketSet.add(this); // 加入set中
		addOnlineCount(); // 在线数加1
		System.out.println("有新连接加入！当前在线人数为" + getOnlineCount());

		// try {
		// // sendMessage("有新连接加入！当前在线人数为" + getOnlineCount());
		// sendMessage(projectid,
		// CalContextUtil.getInstance().getAllQueueContext(projectid));
		// } catch (IOException e) {
		// System.out.println("IO异常");
		// } catch (Exception e) {
		// System.out.println("异常");
		// }
	}

	/**
	 * 连接关闭调用的方法
	 */
	@OnClose
	public void onClose() {
		String projectid = session.getPathParameters().get(RestConstant.SYS_SESSION_PATH_PID);
		sessionMap.remove(projectid);
		webSocketSet.remove(this); // 从set中删除
		subOnlineCount(); // 在线数减1
		System.out.println("有一连接关闭！当前在线人数为" + getOnlineCount());
	}

	/**
	 * 收到客户端消息后调用的方法
	 *
	 * @param message
	 *            客户端发送过来的消息
	 */
	@OnMessage
	public void onMessage(String message, Session session) {
		String projectid = session.getPathParameters().get(RestConstant.SYS_SESSION_PATH_PID);
		System.out.println(projectid);
		System.out.println("来自客户端[" + projectid + "]的消息:" + message);

		if ("@heart".equalsIgnoreCase(message)) {
			try {
//				if (CalContextUtil.getInstance().isBatchOver(projectid)) {
//					sendMessage(projectid, CalContextUtil.OVER_STR);
//				} else {
					sendMessage(projectid, CalContextUtil.getInstance().getDynamicQueueContext(projectid));
//				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// 群发消息
		// try {
		// sendInfo(message);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
	}

	/**
	 * 发生错误时调用
	 */
	@OnError
	public void onError(Session session, Throwable error) {
		System.out.println("发生错误");
		error.printStackTrace();
	}

	public void sendMessage(String projectId, ConcurrentLinkedQueue<String> queue) throws Exception {
		if (EmptyUtil.isEmpty(queue)) {
			sendMessage(projectId, RestConstant.SYS_MSG_NO_DATA);
			return;
		}
		for (String msg : queue) {
			sendMessage(projectId, msg);
		}
	}

	public void sendMessage(String message) throws IOException {
		this.session.getBasicRemote().sendText(message);
		// this.session.getAsyncRemote().sendText(message);
	}

	/**
	 * 给指定的session发送消息
	 * 
	 * @param projectId
	 * @param message
	 * @throws IOException
	 */
	public static void sendMessage(String projectId, String message) throws IOException {
		if (sessionMap.containsKey(projectId)) {
			sessionMap.get(projectId).getBasicRemote().sendText(message);
		}
	}

	/**
	 * 给指定的session发送消息
	 * 
	 * @param projectId
	 * @param session
	 * @throws IOException
	 */
	public static void sendMessage(String message, Session session) throws IOException {
		session.getBasicRemote().sendText(message);
	}

	/**
	 * 群发自定义消息
	 */
	public static void sendInfo(String message) throws IOException {
		for (MyWebSocket item : webSocketSet) {
			try {
				item.sendMessage(message);
			} catch (IOException e) {
				continue;
			}
		}
	}

	public static synchronized int getOnlineCount() {
		return onlineCount;
	}

	public static synchronized void addOnlineCount() {
		MyWebSocket.onlineCount++;
	}

	public static synchronized void subOnlineCount() {
		MyWebSocket.onlineCount--;
	}
}

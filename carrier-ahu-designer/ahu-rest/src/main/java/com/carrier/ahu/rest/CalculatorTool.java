package com.carrier.ahu.rest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.calculator.CalculatorModel;
import com.carrier.ahu.calculator.ExcelForPriceCodeUtils;
import com.carrier.ahu.calculator.PriceCalculator;
import com.carrier.ahu.calculator.WeightCalculator;
import com.carrier.ahu.calculator.price.CalPriceTool;
import com.carrier.ahu.calculator.price.PriceCodePO;
import com.carrier.ahu.calculator.price.PriceCodeXSLXPO;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.calculation.CalculationException;
import com.carrier.ahu.common.exception.calculation.PriceCalcException;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.model.partition.PanelCalculationObj;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.dao.AhuDao;
import com.carrier.ahu.license.LicenseManager;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.param.NSPriceParam;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.service.cal.WeightBean;
import com.carrier.ahu.service.constant.ServiceConstant;
import com.carrier.ahu.service.service.PriceService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.PriceResultVO;
import com.carrier.ahu.vo.PriceResultVO.PriceBean;
import com.carrier.ahu.vo.SysConstants;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.carrier.ahu.vo.WeightResultVO;
import com.google.gson.Gson;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.constant.CommonConstant.SYS_ASSERT_TRUE;
import static com.carrier.ahu.constant.CommonConstant.SYS_PUNCTUATION_SLASH;

@Component
@Data
@Slf4j
public class CalculatorTool{

    private static WeightCalculator weightCalculator = new WeightCalculator();
    @Autowired
    private PriceCalculator priceCalculator;
    private Gson gson = new Gson();
    @Autowired
    PartitionService partitionService;
    @Autowired
    ProjectService projectService;

    @Autowired
    SectionService sectionService;

    @Autowired
    PriceService priceService;

    @Autowired
    AhuDao ahuDao;

    private static CalculatorModel calculatorModel;
    @Value("${price.code.test.output}")
    private String priceCodeTestOutput;

    public CalculatorModel getCalculatorModel() {
        if (null == calculatorModel) {
            calculatorModel = weightCalculator.getCalModelInstance();
        }
        return calculatorModel;
    }

    public List<CalculatorSpec> getSpec() {
        return getCalculatorModel().getSpec();
    }

    public WeightCalculator getWeightCalculator() {
        return weightCalculator;
    }

    /**
     * 计算AHU的总重量信息
     *
     * @param unit
     * @return
     */
    public WeightResultVO calculateAHUWeight(Unit unit) {
        return calculateAHUWeight(unit, null);
    }
    
    /**
     * 计算AHU的总重量信息
     *
     * @param unit
     * @param partition
     * @return
     */
    public WeightResultVO calculateAHUWeight(Unit unit, Partition partition) {
        if (StringUtils.isEmpty(unit.getSeries())) {
            throw new CalculationException(ErrorCode.UNIT_MODEL_NOT_SELECTED);
        }

        List<WeightBean> weightBeans = new ArrayList<>();
        double total = 0;

        // 计算分段的面板重量
        log.info("Start to calculate partition weight ...");
        if (EmptyUtil.isEmpty(partition)) {
            partition = partitionService.findPartitionByAHUId(unit.getUnitid());
        }

        if (EmptyUtil.isNotEmpty(partition)) {
            List<AhuPartition> ahuPartitions = AhuPartitionUtils.parseAhuPartition(unit, partition);
            Iterator<AhuPartition> ahuPartitionItr = ahuPartitions.iterator();
            while (ahuPartitionItr.hasNext()) {
                AhuPartition ahuPartition = ahuPartitionItr.next();
                System.out.println(ahuPartition.getSections().get(0).get("metaId"));
                double partitionWeight = 0;
                if (AhuPartitionUtils.hasEndFacePanel(ahuPartition, ahuPartitions)) {
                    partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ahuPartition, ahuPartitions.size(), true);
                } else {
                    partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ahuPartition, ahuPartitions.size(), false);
                }

                WeightBean weightBean = new WeightBean();
                String partitionKey = RestConstant.SYS_WGT_PARTITION_HYPHEN + ahuPartition.getPos();
                weightBean.setKey(partitionKey);
                weightBean.setName(RestConstant.SYS_WGT_PARTITION_WEIGHT + ahuPartition.getPos());
                weightBean.setWeight(partitionWeight);
                weightBean.setId(partitionKey);
                weightBeans.add(weightBean);

                total += partitionWeight;
            }
        }
        unit.setWeight(total); // update weight for late save

        WeightResultVO weightResultVo = new WeightResultVO();
        weightResultVo.setAhuId(unit.getUnitid());
        weightResultVo.setTotal(total);
        weightResultVo.setDetails(weightBeans);
        weightResultVo.setPriceSpecVersion(RestConstant.SYS_WGT_PARTITION_VERSION);
        return weightResultVo;
    }

    /**
     * 计算整个ahu所有段价格
     * @param unit
     * @param partition
     * @param parts
     * @param language
     * @return
     */
    public PriceResultVO calculateAHUPrice(Unit unit, Partition partition, List<Part> parts, LanguageEnum language) {
        PriceResultVO priceResultVo = null;
        try {
        	//所有价格相关的计算中，都采用39G的面板布置原则去计算
            List<PanelCalculationObj> casingList = AhuPartitionGenerator.getCasinglist(unit.getSeries(),unit, partition, true,false);//计算价格不使用变形型号
            List<AhuPartition> partitions = AhuPartitionUtils.parseAhuPartition(unit, partition);
            Project project = this.projectService.getProjectById(unit.getPid());
            List<PriceCodePO> priceCodes = ExcelForPriceCodeUtils.getPriceCodes(unit, parts, partitions,true);

            boolean isInputExist=CalPriceTool.output2File(project.getPriceBase(), getPriceCodeOutputString(priceCodes, casingList));

            if(!isInputExist) {
            	return priceResultVo;
            }
//            generatePriceCodeForTest(priceCodes);

            Map<String, Double> prices = CalPriceTool.calPrice(project.getPriceBase(), language);
            log.error("cal result: "+prices);
            if(prices.get(CommonConstant.SYS_MAP_RESULT)==0) {
            	//TODO 暂时忽略价格报错
            	throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED); 	
            }
            priceResultVo = this.generatePriceResultVO(unit, partition, priceCodes, prices);
        } catch (Exception e) {
            log.debug(e.getMessage(), e);
            String tempMsg = EmptyUtil.isNotEmpty(e.getMessage())?getIntlString(e.getMessage(), AHUContext.getLanguage()):null;
            if(EmptyUtil.isNotEmpty(tempMsg)){
                throw new PriceCalcException(e.getMessage());
            }else{
                throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED);
            }

        }
        return priceResultVo;
    }

    /**
     * 计算整个ahu所有段价格
     * @param unit
     * @param parts
     * @return
     */
    public Unit calculateAHUNsPrice(Unit unit, List<Part> parts) {
        try {
            unit = this.generateNsPriceResult(unit, parts);
        } catch (Exception e) {
            log.debug(e.getMessage(), e);
            throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED);
        }
        return unit;
    }

    /**
     * 获取价格系列，39CQ 使用G的面板切割做价格，非CQ不变
     * @param series
     * @return
     */
    private String getSeries(String series){
        if(series.contains(RestConstant.SYS_UNIT_SERIES_39CQ)){
            return RestConstant.SYS_UNIT_SERIES_39G+AhuUtil.getUnitNo(series);
        }else{
            return series;
        }
    }
    /**
     * 计算单个段不同配置参数结果的价格
     * @param unit
     * @param parts
     * @param language
     * @return
     */
    public PriceResultVO calculatePartPrice(Unit unit, List<Part> parts, LanguageEnum language) {
        PriceResultVO priceResultVo = null;
        try {
            Project project = this.projectService.getProjectById(unit.getPid());
            List<PriceCodePO> priceCodes = ExcelForPriceCodeUtils.getPriceCodes(unit, parts, null,true);
            CalPriceTool.output2File(project.getPriceBase(), getPriceCodeOutputString(priceCodes, null));

            generatePriceCodeForTest(priceCodes);

            Map<String, Double> prices = CalPriceTool.calPrice(project.getPriceBase(), language);
            priceResultVo = this.generatePriceResultVO(unit, null, priceCodes, prices);
        } catch (Exception e) {
            log.debug(e.getMessage(), e);
            throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED);
        }
        return priceResultVo;
    }
    private void generatePriceCodeForTest(List<PriceCodePO> priceCodes) {
        try {
            if (Boolean.valueOf(priceCodeTestOutput)) {
                FileUtil.writeFile(SysConstants.TEST_PRICE_CODE_OUTPUT, getPriceCodeTestOutputString(priceCodes));
            }
        } catch (IOException e) {
            log.error("Failed to generate price code for test", e);
        }
    }
    private String isTB(PriceCodePO priceCode){
        if(EmptyUtil.isEmpty(priceCode.getTopLayer())){
            return "";
        }else{
            return priceCode.getTopLayer()?"T":"B";
        }
    }
    private String getPriceCodeOutputString(List<PriceCodePO> priceCodes, List<PanelCalculationObj> casingList) {
        String lineSeparator = System.getProperty(RestConstant.SYS_PROPERTY_LINE_SEPARATOR);
        StringBuilder outputString = new StringBuilder();
        for (PriceCodePO priceCode : priceCodes) {
            if (EmptyUtil.isNotEmpty(priceCode.getPriceCode())) { // skip empty price code
                outputString.append(String.join(RestConstant.SYS_PUNCTUATION_COMMA, priceCode.getFuncName(),
                        priceCode.getPriceCode(), priceCode.getPartitionNo().toString(),isTB(priceCode)));
                outputString.append(lineSeparator);
            }
        }

        // uncomment when casing list is ready
        // @formatter:off
        // generate casing list
        if(EmptyUtil.isNotEmpty(casingList))
        for (PanelCalculationObj casingObj : casingList) {
        	//TODO 忽略底座的计算，每次加入底座后，计算价格报错
        	/*if(casingObj.getCategory().contains(RestConstant.SYS_STRING_BASE)) {
        		continue;
        	}*/
            outputString.append(
                    String.format("%s,%s,%s,%s,%s,%s",
                            EmptyUtil.toString(casingObj.getCategory(), StringUtils.EMPTY),
                            EmptyUtil.toString(casingObj.getPartLM(), String.valueOf(0)),
                            EmptyUtil.toString(casingObj.getPartWM(), String.valueOf(0)),
                            EmptyUtil.toString(casingObj.getQuantity(), String.valueOf(0)),
                            EmptyUtil.toString(casingObj.getMemo(), StringUtils.EMPTY),
                            EmptyUtil.toString(casingObj.getPro(), StringUtils.EMPTY)
                            ));
            outputString.append(lineSeparator);
        }
        // @formatter:on
        return outputString.toString();
    }

    private String getPriceCodeTestOutputString(List<PriceCodePO> priceCodes) {
        String lineSeparator = System.getProperty(RestConstant.SYS_PROPERTY_LINE_SEPARATOR);
        StringBuilder outputString = new StringBuilder();
        for (PriceCodePO priceCode : priceCodes) {
            outputString.append(priceCode.getFuncName());
            for (PriceCodeXSLXPO codeRule : priceCode.getCodeRules()) {
                outputString.append(RestConstant.SYS_PUNCTUATION_COMMA);
                outputString
                        .append(String.join(RestConstant.SYS_PUNCTUATION_LOW_HYPHEN, codeRule.getNo(), codeRule.getDescription(), codeRule.getValueDesp()));
            }
            outputString.append(lineSeparator);
        }
        return outputString.toString();
    }
    private PriceResultVO generateEmptyPriceResultVO(Unit unit) {
        PriceResultVO priceResultVo = new PriceResultVO();
        priceResultVo.setWeight(calculateAHUWeight(unit).getTotal());
        priceResultVo.setTotal(0);
        priceResultVo.setDetails(null);
        priceResultVo.setPriceSpecVersion(RestConstant.SYS_WGT_PARTITION_VERSION); // not sure about the version
        return priceResultVo;
    }
    @SuppressWarnings("unchecked")
    private PriceResultVO generatePriceResultVO(Unit unit, Partition partition, List<PriceCodePO> priceCodes,
            Map<String, Double> prices) {
        boolean isClearPrice = false;
        String message = "";
        
        //设计院角色不用计算价格
        if(LicenseManager.isDesign()){
            return generateEmptyPriceResultVO(unit);
        }

        PriceResultVO priceResultVo = new PriceResultVO();
        List<PriceBean> priceBeans = new ArrayList<>();
        Double totalPrice = 0d;
        Double NStdpackingPrice = 0d;//非标总价格
        List<String> multiSectionCodes = getMultiSectionCode(priceCodes);
        Map<String, Object> unitMeta = JSON.parseObject(unit.getMetaJson(), Map.class);
        
        boolean hasW = false;
        Iterator<String> pcs = prices.keySet().iterator();
        while (pcs.hasNext()){
            String pc = pcs.next();
            if(pc.contains("_FUNC_W_")){
                hasW = true;
            }
        }
        boolean isRecycle = false; 
        for (PriceCodePO priceCode : priceCodes) {
            String funcName = priceCode.getFuncName();
            if (SystemCalculateConstants.BOX_SECTION_CODE.equals(priceCode.getSectionCode())) {
                funcName = priceCode.getFuncNameWithoutNo();
            } else {
                funcName = priceCode.getFuncName();
            }
            double sectionPrice = 0;
            if (prices.containsKey(funcName)) {
                sectionPrice = NumberUtil.scale(prices.get(funcName), 2); // two decimal
                if(funcName.contains("_FUNC_0") && hasW){
//                    sectionPrice= AhuUtil.getHeightOfAHU(unit.getSeries())>=18?sectionPrice*SYS_PANEL_18:sectionPrice;
                }
            }
            PriceBean priceBean = new PriceResultVO.PriceBean();
            if (SystemCalculateConstants.BOX_SECTION_CODE.equals(priceCode.getSectionCode())) {
                priceBean.setKey(SystemCalculateConstants.SECTION_BOX);
                priceBean.setName(getIntlString(SystemCalculateConstants.SECTION_BOX));
                priceBean.setWeight(AhuPartitionUtils.calculateAHUContainerWeight(unit, partition));
                unitMeta.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_BOX_PRICE), sectionPrice);
            } else {
                SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeByCode(priceCode.getSectionCode());
                priceBean.setKey(sectionType.getId());
                String name = getIntlString(sectionType.getCnName())
                        + (multiSectionCodes.contains(priceCode.getSectionCode())
                                ? String.valueOf(priceCode.getSectionNo())
                                : StringUtils.EMPTY);
                priceBean.setName(name);
            }

            Part part = priceCode.getPart();
            
            if (EmptyUtil.isNotEmpty(part)) { // update price of part for late save
                //如果是热回收或者转轮，则热回收段或者转轮段，只显示一条
                if(part.getSectionKey().contains(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId()) ||
                        part.getSectionKey().contains(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId())){
                    if(isRecycle){
                        continue;
                    }
                }
                Map<String, Object> partMeta = JSON.parseObject(part.getMetaJson(), Map.class);
                partMeta.put(SectionMetaUtils.getMetaSectionKey(part.getSectionKey(), MetaKey.KEY_PRICE), sectionPrice);
                part.setMetaJson(JSON.toJSONString(partMeta));

                // add ns price of part
                //Double tempPartNsPrice = getNSPriceOfPart(part, partMeta);
                String tempPartArray = getNSPriceOfPart(unit, part, partMeta);
                String tempPartFlag = tempPartArray.split("\\|\\|")[1];
                if("true".equals(tempPartFlag) && !isClearPrice){
                    isClearPrice = true;
                }
                if("true".equals(tempPartFlag)){
                    message += tempPartArray.split("\\|\\|")[2];
                }
                double tempPartNsPrice = 0;
                //如果是热回收或者转轮段，只计算一遍非标价格和重量
                if(part.getSectionKey().contains(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId()) ||
                        part.getSectionKey().contains(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId())){
                    if(!isRecycle){
                        tempPartNsPrice = Double.parseDouble(tempPartArray.split("\\|\\|")[0] == "" ? "0":tempPartArray.split("\\|\\|")[0]);
                        priceBean.setWeight(AhuUtil.getWeight(part));
                    }
                    isRecycle = true;
                }else{
                    tempPartNsPrice = Double.parseDouble(tempPartArray.split("\\|\\|")[0] == "" ? "0":tempPartArray.split("\\|\\|")[0]);
                    priceBean.setWeight(AhuUtil.getWeight(part));
                }
                sectionPrice += tempPartNsPrice;
                NStdpackingPrice += tempPartNsPrice;
                priceBean.setNsPrice(tempPartNsPrice);
            } else { // update box price for late save
                if (EmptyUtil.isNotEmpty(unit)) {
                    unitMeta.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_PRICE), priceResultVo.getTotal());
                    unit.setMetaJson(JSON.toJSONString(unitMeta));

                    // add ns price of unit
                    //Double tempAhuNsPrice = getNSPriceOfUnit(unitMeta);
                    String tempAhuArray = getNSPriceOfUnit(unitMeta,unit);
                    String tempPartFlag = tempAhuArray.split("\\|\\|")[1];
                    if("true".equals(tempPartFlag) && !isClearPrice){
                        isClearPrice = true;
                    }
                    if("true".equals(tempPartFlag)){
                        message += tempAhuArray.split("\\|\\|")[2];
                    }
                    double tempAhuNsPrice = Double.parseDouble(tempAhuArray.split("\\|\\|")[0] == "" ? "0":tempAhuArray.split("\\|\\|")[0]);
                    sectionPrice += tempAhuNsPrice;
                    NStdpackingPrice += tempAhuNsPrice;
                    priceBean.setNsPrice(tempAhuNsPrice);
                }
            }
            
            priceBean.setPrice(sectionPrice);
            priceBeans.add(priceBean);
            totalPrice += sectionPrice;
        }
        if(EmptyUtil.isNotEmpty(unit)) {
            if(isClearPrice){
                if(unit.getPrice() != -1 && EmptyUtil.isNotEmpty(unit.getNsprice())) {
                    unit.setStandPrice(unit.getPrice() - unit.getNsprice());
                }
                unit.setPrice(-1.0); // set total price for late save
                unit.setNsprice(-1.0);// 设置非标总价格
            }else{
                unit.setPrice(totalPrice); // set total price for late save
                unit.setNsprice(NStdpackingPrice);// 设置非标总价格
                unit.setStandPrice(unit.getPrice() - unit.getNsprice());
            }
        }
        priceResultVo.setWeight(calculateAHUWeight(unit).getTotal());
        if(isClearPrice){
            priceResultVo.setTotal(-1);
            priceResultVo.setTotalNs(-1);
            priceResultVo.setDetails(priceBeans);
            priceResultVo.setNsMessage(message.substring(1));
        }else{
            priceResultVo.setTotal(totalPrice);
            priceResultVo.setTotalNs(NStdpackingPrice);
            priceResultVo.setDetails(priceBeans);
        }
        priceResultVo.setPriceSpecVersion(RestConstant.SYS_WGT_PARTITION_VERSION); // not sure about the version
        return priceResultVo;
    }
    @SuppressWarnings("unchecked")
    private Unit generateNsPriceResult(Unit unit, List<Part> parts) {
        Double nStdpackingPrice = 0d;//非标总价格
        boolean isClearPrice = false;
        PriceResultVO priceResultVo = new PriceResultVO();
        Map<String, Object> unitMeta = JSON.parseObject(unit.getMetaJson(), Map.class);
        boolean isRecycle =false; 
        for (Part part : parts) {
            if (EmptyUtil.isNotEmpty(part)) { // update price of part for late save
                Map<String, Object> partMeta = JSON.parseObject(part.getMetaJson(), Map.class);
                part.setMetaJson(JSON.toJSONString(partMeta));
                // add ns price of part
                String tempPartArray = getNSPriceOfPart(unit, part, partMeta);
                String tempPartFlag = tempPartArray.split("\\|\\|")[1];
                if("true".equals(tempPartFlag) && !isClearPrice){
                    isClearPrice = true;
                }
                double tempPartNsPrice = 0;
                //如果是热回收或者转轮段，只计算一遍非标价格和重量
                if(part.getSectionKey().contains(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId()) ||
                        part.getSectionKey().contains(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId())){
                    if(!isRecycle){
                        tempPartNsPrice = Double.parseDouble(tempPartArray.split("\\|\\|")[0] == "" ? "0":tempPartArray.split("\\|\\|")[0]);
                    }
                    isRecycle = true;
                }else{
                    tempPartNsPrice = Double.parseDouble(tempPartArray.split("\\|\\|")[0] == "" ? "0":tempPartArray.split("\\|\\|")[0]);
                }
                nStdpackingPrice += tempPartNsPrice;
            }

        }
        if (EmptyUtil.isNotEmpty(unit)) {
//                    unitMeta.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_PRICE), priceResultVo.getTotal());
//                    unit.setMetaJson(JSON.toJSONString(unitMeta));
            // add ns price of unit
            String tempAhuArray = getNSPriceOfUnit(unitMeta,unit);
            String tempPartFlag = tempAhuArray.split("\\|\\|")[1];
            if("true".equals(tempPartFlag) && !isClearPrice){
                isClearPrice = true;
            }
            double tempAhuNsPrice = Double.parseDouble(tempAhuArray.split("\\|\\|")[0] == "" ? "0":tempAhuArray.split("\\|\\|")[0]);
            nStdpackingPrice += tempAhuNsPrice;
        }
        if(EmptyUtil.isNotEmpty(unit)) {
            if(isClearPrice){
                if(unit.getPrice() != -1){
                    unit.setStandPrice(unit.getPrice() - unit.getNsprice());
                }
                unit.setNsprice(-1.0);// 设置非标总价格
                unit.setPrice(-1.0);
                
            }else{
                double oldNsPrice = unit.getNsprice()==null ? 0 : unit.getNsprice();
                unit.setNsprice(nStdpackingPrice);// 设置非标总价格
                if(unit.getPrice() == -1){
                    unit.setPrice(unit.getStandPrice() + nStdpackingPrice);
                }else{
                    unit.setPrice(unit.getPrice() - (oldNsPrice - nStdpackingPrice));
                }
            }

        }
        return unit;
    }
    /**
     * 段非标价格
     * @param part
     * @param partMeta
     * @return
     */
    private String getNSPriceOfPart(Unit unit,Part part, Map<String, Object> partMeta) {
        String sectionId = part.getSectionKey();
        double nsPriceOfPart = 0;
        boolean isClearPrice = false;
        StringBuffer message = new StringBuffer();
        
        String nsPriceEnable = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_ENABLE)));

        NSPriceParam nsPriceParam = getRealNsPrice(unit,part,partMeta);
        Map<String, String> realPrice = priceService.getNSPrice(nsPriceParam);
        if(SYS_ASSERT_TRUE.equals(nsPriceEnable)){//允许非标
            String nsPriceKey = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_PRICE);
            nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsPriceKey)));

            //判断如果选中允许非标，则当memo不为空的时候，价格必须不能为空
            String nsPrice = String.valueOf(partMeta.get(nsPriceKey));
            String nsPriceMemo = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_MEMO);
            String memo = String.valueOf(partMeta.get(nsPriceMemo));
            if(!EmptyUtil.isEmptyOrNull(memo) && EmptyUtil.isEmptyOrNull(nsPrice)){
                isClearPrice = true;
                //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
            }
            if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionId)) {
                String nsChangeSupplier = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGESUPPLIER)));
                if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeSupplier)){//湿膜加湿段，变更供应商
                    String nsSupplierPriceKey = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSSUPPLIERPRICE);

                    String nsSupplierPrice = String.valueOf(partMeta.get(nsSupplierPriceKey));
                    if(EmptyUtil.isEmptyOrNull(nsSupplierPrice)){
                        isClearPrice = true;
                        //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsSupplierPriceKey)));
                        //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
                    }else{//如果校验通过，则计算最新价格
                        //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NSSUPPLIERPRICE)));
                        String sNsSupplierPrice = String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NSSUPPLIERPRICE));
                        int nsSupplierPriceDouble = 0;
                        if(!EmptyUtil.isEmptyOrNull(sNsSupplierPrice)){
                            nsSupplierPriceDouble = NumberUtil.convertStringToDoubleInt(sNsSupplierPrice);
                        }

                        nsPriceOfPart += nsSupplierPriceDouble;
                        partMeta.put(SectionMetaUtils.getNSSectionKey(sectionId, ServiceConstant.SYS_STRING_NSSUPPLIERPRICE), nsSupplierPriceDouble);
                        String sectionStr = JSONObject.toJSONString(partMeta);
                        part.setMetaJson(sectionStr);
                        sectionService.updateSection(part, AHUContext.getUserName());
                    }
                }
                if(isClearPrice){
                    message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_WETFILMHUMIDIFIER, AHUContext.getLanguage()));
                }
            } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(sectionId)) {
                String nsChangeSupplier = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGESUPPLIER)));
                if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeSupplier)){//高压喷雾加湿段，变更供应商
                    String nsSupplierPriceKey = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSSUPPLIERPRICE);
                    //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsSupplierPriceKey)));

                    String nsSparyHumidifierPrice = String.valueOf(partMeta.get(nsSupplierPriceKey));
                    if(EmptyUtil.isEmptyOrNull(nsSparyHumidifierPrice)){
                        isClearPrice = true;
                        //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
                    }else{//如果校验通过，则计算最新价格
                        //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NSSUPPLIERPRICE)));
                        String sNsSparyHumidifierPrice = String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NSSUPPLIERPRICE));
                        int nsSparyHumidifierPriceDouble = 0;
                        if(!EmptyUtil.isEmptyOrNull(sNsSparyHumidifierPrice)){
                            nsSparyHumidifierPriceDouble = NumberUtil.convertStringToDoubleInt(sNsSparyHumidifierPrice);
                        }
                        nsPriceOfPart += nsSparyHumidifierPriceDouble;
                        partMeta.put(SectionMetaUtils.getNSSectionKey(sectionId, ServiceConstant.SYS_STRING_NSSUPPLIERPRICE), nsSparyHumidifierPriceDouble);
                        String sectionStr = JSONObject.toJSONString(partMeta);
                        part.setMetaJson(sectionStr);
                        sectionService.updateSection(part, AHUContext.getUserName());
                    }
                }
                if(isClearPrice){
                    message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_SPRAYHUMIDIFIER, AHUContext.getLanguage()));
                }
            } else if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionId)) {
                String nsChangeFan = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGEFAN)));
                if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeFan)){//风机段，变更风机供应商
                    String nsFanPriceKey = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSFANPRICE);
                    //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsFanPriceKey)));

                    String nsFanPrice = String.valueOf(partMeta.get(nsFanPriceKey));
                    if(EmptyUtil.isEmptyOrNull(nsFanPrice)){
                        isClearPrice = true;
                        //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
                    }else{//如果校验通过，则计算最新价格
                        //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NS_FAN_PRICE)));
                        String  sNsFanPrice = String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NS_FAN_PRICE));
                        int nsFanPriceDouble = 0;
                        if(!EmptyUtil.isEmptyOrNull(sNsFanPrice)){
                            nsFanPriceDouble = NumberUtil.convertStringToDoubleInt(sNsFanPrice);
                        }
                        nsPriceOfPart += nsFanPriceDouble;
                        partMeta.put(SectionMetaUtils.getNSSectionKey(sectionId, ServiceConstant.SYS_STRING_NS_FAN_PRICE), nsFanPriceDouble);
                        String sectionStr = JSONObject.toJSONString(partMeta);
                        part.setMetaJson(sectionStr);
                        sectionService.updateSection(part, AHUContext.getUserName());
                    }
                }

                String nsChangeMotor = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGEMOTOR)));
                if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeMotor)){//风机段，变更电机功率
                    String nsMotorPriceKey = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSMOTORPRICE);
                    //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsMotorPriceKey)));

                    String nsMotorPrice = String.valueOf(partMeta.get(nsMotorPriceKey));
                    if(EmptyUtil.isEmptyOrNull(nsMotorPrice)){
                        isClearPrice = true;
                        //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
                    }else{//如果校验通过，则计算最新价格
                        //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NS_MOTOR_PRICE)));
                        String  sNsMotorPrice = String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NS_MOTOR_PRICE));
                        int nsMotorPriceDouble = 0;
                        if(!EmptyUtil.isEmptyOrNull(sNsMotorPrice)){
                            nsMotorPriceDouble = NumberUtil.convertStringToDoubleInt(sNsMotorPrice);
                        }
                        nsPriceOfPart += nsMotorPriceDouble;
                        partMeta.put(SectionMetaUtils.getNSSectionKey(sectionId, ServiceConstant.SYS_STRING_NS_MOTOR_PRICE), nsMotorPriceDouble);
                        String sectionStr = JSONObject.toJSONString(partMeta);
                        part.setMetaJson(sectionStr);
                        sectionService.updateSection(part, AHUContext.getUserName());
                    }
                }

                String nsSpringTransForm = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSSPRINGTRANSFORM)));
                if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsSpringTransForm)){//风机段，JD弹簧改HD弹簧
                    String nsSpringtransformPrice = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSSPRINGTRANSFORMPRICE);
                    //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsSpringtransformPrice)));

                    String nsSpringtransformPrices = String.valueOf(partMeta.get(nsSpringtransformPrice));
                    if(EmptyUtil.isEmptyOrNull(nsSpringtransformPrices)){
                        isClearPrice = true;
                        //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
                    }else{//如果校验通过，则计算最新价格
                        //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NS_SPRING_TRANSFORM_PRICE)));
                        String sNsSpringtransformPrices = String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NS_SPRING_TRANSFORM_PRICE));
                        int nsSpringtransformPricesDouble = 0;
                        if(!EmptyUtil.isEmptyOrNull(sNsSpringtransformPrices)){
                            nsSpringtransformPricesDouble = NumberUtil.convertStringToDoubleInt(sNsSpringtransformPrices);
                        }

                        nsPriceOfPart += nsSpringtransformPricesDouble;
                        partMeta.put(SectionMetaUtils.getNSSectionKey(sectionId, ServiceConstant.SYS_STRING_NS_SPRING_TRANSFORM_PRICE), nsSpringtransformPricesDouble);
                        String sectionStr = JSONObject.toJSONString(partMeta);
                        part.setMetaJson(sectionStr);
                        sectionService.updateSection(part, AHUContext.getUserName());
                    }
                }
                if(isClearPrice){
                    message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_FAN, AHUContext.getLanguage()));
                }
            } else if (SectionTypeEnum.TYPE_SINGLE.getId().equals(sectionId)
                    || SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionId)
                    || SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionId)) {
                String nsPressureGuage = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSPRESSUREGUAGE)));
                if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsPressureGuage)){//启用非标压差计
                    String nsPressureGuagePrice = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSPRESSUREGUAGEPRICE);
                    //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsPressureGuagePrice)));

                    String nsPressureGuagePrices = String.valueOf(partMeta.get(nsPressureGuagePrice));
                    if(EmptyUtil.isEmptyOrNull(nsPressureGuagePrices)){
                        isClearPrice = true;
                        //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
                    }else{//如果校验通过，则计算最新价格
                        //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NS_PRESSURE_GUAGE_PRICE)));
                        String sNsPressureGuagePrices = String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NS_PRESSURE_GUAGE_PRICE));
                        int nsPressureGuagePricesDouble = 0;
                        if(!EmptyUtil.isEmptyOrNull(sNsPressureGuagePrices)){
                            nsPressureGuagePricesDouble = NumberUtil.convertStringToDoubleInt(sNsPressureGuagePrices);
                        }
                        nsPriceOfPart += nsPressureGuagePricesDouble;
                        partMeta.put(SectionMetaUtils.getNSSectionKey(sectionId, ServiceConstant.SYS_STRING_NS_PRESSURE_GUAGE_PRICE), nsPressureGuagePricesDouble);
                        String sectionStr = JSONObject.toJSONString(partMeta);
                        part.setMetaJson(sectionStr);
                        sectionService.updateSection(part, AHUContext.getUserName());
                    }
                }
                if(isClearPrice){
                    if (SectionTypeEnum.TYPE_SINGLE.getId().equals(sectionId)){
                        message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_SINGLE, AHUContext.getLanguage()));
                    }else if(SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionId)){
                        message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_HEPAFILTER, AHUContext.getLanguage()));
                    }else if(SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionId)){
                        message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_COMPOSITE, AHUContext.getLanguage()));
                    }
                }
            } else if (SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionId)) {
                String nsDrainPan = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSDRAINPAN)));
                if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsDrainPan)){//空段，变更水盘
                    String nsDrainpanPrice = SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSDRAINPANPRICE);
                    //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(partMeta.get(nsDrainpanPrice)));

                    String nsDrainpanPrices = String.valueOf(partMeta.get(nsDrainpanPrice));
                    if(EmptyUtil.isEmptyOrNull(nsDrainpanPrices)){
                        isClearPrice = true;
                        //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
                    }else{//如果校验通过，则计算最新价格
                        //nsPriceOfPart += NumberUtils.toDouble(String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NS_ACCESS_SHUIPAN_PRICE)));
                        String sNsDrainpanPrices = String.valueOf(realPrice.get(ServiceConstant.SYS_STRING_NS_ACCESS_SHUIPAN_PRICE));
                        int nsDrainpanPricesDouble = 0;
                        if(!EmptyUtil.isEmptyOrNull(sNsDrainpanPrices)) {
                            nsDrainpanPricesDouble = NumberUtil.convertStringToDoubleInt(sNsDrainpanPrices);
                        }
                        nsPriceOfPart += nsDrainpanPricesDouble;
                        partMeta.put(SectionMetaUtils.getNSSectionKey(sectionId, ServiceConstant.SYS_STRING_NS_ACCESS_SHUIPAN_PRICE), nsDrainpanPricesDouble);
                        String sectionStr = JSONObject.toJSONString(partMeta);
                        part.setMetaJson(sectionStr);
                        sectionService.updateSection(part, AHUContext.getUserName());
                    }
                }
                if(isClearPrice){
                    message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_ACCESS, AHUContext.getLanguage()));
                }
            }
            if(isClearPrice){
                message.append(getNsPriceTips(sectionId));
            }
        }
        
        return String.valueOf(nsPriceOfPart) + "||" + isClearPrice + "||" + message.toString();
    }

    /**
     * 潘洁提出非标计算价格位-1时，提示到具体那个信息段价格输入有问题，这里获取对应段的提示信息
     * @param sectionId
     * @return
     */
    private String getNsPriceTips(String sectionId) {
        StringBuffer message = new StringBuffer();
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_MIX, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_COLD.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_COLD, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_DIRECTEXPENSIONCOIL, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_HEATINGCOIL, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_STEAMCOIL.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_STEAMCOIL, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_ELECTRICHEATINGCOIL, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_STEAMHUMIDIFIER, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_COMBINEDMIXINGCHAMBER, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_ATTENUATOR.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_ATTENUATOR, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_DISCHARGE, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_ELECTRODEHUMIDIFIER, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_ELECTROSTATICFILTER, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_CTR.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_CTR, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_HEATRECYCLE.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_HEATRECYCLE, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_WHEELHEATRECYCLE, AHUContext.getLanguage()));
        }else if (SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(sectionId)){
            message.append(SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_PLATEHEATRECYCLE, AHUContext.getLanguage()));
        }
        
        return message.toString();
    }
    
    
    private Map<String, Object> getJsonMap(String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        Map<String, Object> map = new HashMap<String, Object>();
        map = jsonObject;
        return map;
    }
    /**
     * 实时计算非标价格
     * @param unit
     * @param part
     * @param partMeta
     * @return
     */
    private NSPriceParam getRealNsPrice(Unit unit,Part part,Map<String, Object> partMeta) {
        NSPriceParam nsPriceParam = new NSPriceParam();
        nsPriceParam.setUnitid(unit.getUnitid());
        nsPriceParam.setSerial(unit.getSeries());

        String sectionId = part.getSectionKey();
        if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionId)) {
            nsPriceParam.setSectionType(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getCode());
            String nsChangeSupplier = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGESUPPLIER)));
            if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeSupplier)) {//湿膜加湿段，变更供应商
                nsPriceParam.setNs_section_wetfilmHumidifier_nsChangeSupplier(nsChangeSupplier);
                String thickness = String.valueOf(partMeta.get(SectionMetaUtils.getMetaSectionKey(sectionId, MetaKey.KEY_THICKNESS)));
                nsPriceParam.setThickness(thickness);
            }

        } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(sectionId)) {
            nsPriceParam.setSectionType(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getCode());
            String nsChangeSupplier = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGESUPPLIER)));
            if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeSupplier)){//高压喷雾加湿段，变更供应商
                nsPriceParam.setNs_section_sprayHumidifier_nsChangeSupplier(nsChangeSupplier);
                nsPriceParam.setHq(Integer.parseInt(String.valueOf(partMeta.get(SectionMetaUtils.getMetaSectionKey(sectionId, MetaKey.KEY_HQ)))));
            }
        } else if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionId)) {
            nsPriceParam.setSectionType(SectionTypeEnum.TYPE_FAN.getCode());
            nsPriceParam.setFanModel(String.valueOf(partMeta.get(SectionMetaUtils.getMetaSectionKey(sectionId, MetaKey.KEY_FANMODEL))));
            String nsChangeFan = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGEFAN)));
            if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeFan)){//风机段，变更风机供应商
                nsPriceParam.setNs_section_fan_nsChangeFan(nsChangeFan);
                nsPriceParam.setNs_section_fan_nsFanSupplier(String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_FANSUPPLIER))));
                nsPriceParam.setNs_section_fan_nsFanModel(String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_FANMODEL))));
            }

            String nsChangeMotor = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSCHANGEMOTOR)));
            if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsChangeMotor)){//风机段，变更电机功率
                nsPriceParam.setNs_section_fan_nsChangeMotor(nsChangeMotor);
                String nsMotorPower= String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_MOTORPOWER)));
                nsPriceParam.setNs_section_fan_nsMotorPower(nsMotorPower);
                String nsMmotorPole= String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_MOTORPOLE)));
                nsPriceParam.setNs_section_fan_nsMmotorPole(nsMmotorPole);
                String type = String.valueOf(partMeta.get(SectionMetaUtils.getMetaSectionKey(sectionId, MetaKey.KEY_TYPE)));
                nsPriceParam.setType(type);
                String supplier = String.valueOf(partMeta.get(SectionMetaUtils.getMetaSectionKey(sectionId, MetaKey.KEY_sUPPLIER)));
                nsPriceParam.setSupplier(supplier);
            }

            String nsSpringTransForm = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSSPRINGTRANSFORM)));
            if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsSpringTransForm)){//风机段，JD弹簧改HD弹簧
                nsPriceParam.setNs_section_fan_nsSpringtransform(nsSpringTransForm);
                String motorBaseNo = String.valueOf(partMeta.get(SectionMetaUtils.getMetaSectionKey(sectionId, MetaKey.KEY_MOTORBASENO)));
                nsPriceParam.setMotorBaseNo(motorBaseNo);
            }

        } else if (SectionTypeEnum.TYPE_SINGLE.getId().equals(sectionId)
                || SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionId)
                || SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionId)) {
            String nsPressureGuage = null;
            String nsPressureGuageModel = null;
            if (SectionTypeEnum.TYPE_SINGLE.getId().equals(sectionId)) {
                nsPriceParam.setSectionType(SectionTypeEnum.TYPE_SINGLE.getCode());
                nsPressureGuage = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_PRESSUREGUAGE)));
                nsPriceParam.setNs_section_filter_nsPressureGuage(nsPressureGuage);
                nsPressureGuageModel = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_PRESSUREGUAGEMODEL)));
                nsPriceParam.setNs_section_filter_nsPressureGuageModel(nsPressureGuageModel);
            } else if (SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionId)) {
                nsPriceParam.setSectionType(SectionTypeEnum.TYPE_HEPAFILTER.getCode());
                nsPressureGuage = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_PRESSUREGUAGE)));
                nsPriceParam.setNs_section_HEPAFilter_nsPressureGuage(nsPressureGuage);
                nsPressureGuageModel = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_PRESSUREGUAGEMODEL)));
                nsPriceParam.setNs_section_HEPAFilter_nsPressureGuageModel(nsPressureGuageModel);
            } else if (SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionId)) {
                nsPriceParam.setSectionType(SectionTypeEnum.TYPE_COMPOSITE.getCode());
                nsPressureGuage = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_PRESSUREGUAGE)));
                nsPriceParam.setNs_section_combinedFilter_nsPressureGuage(nsPressureGuage);
                nsPressureGuageModel = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_PRESSUREGUAGEMODEL)));
                nsPriceParam.setNs_section_combinedFilter_nsPressureGuageModel(nsPressureGuageModel);
                String snsPressureGuageCount = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_PRESSUREGUAGECOUNT)));
                int  nsPressureGuageCount = 0;
                if(!EmptyUtil.isEmptyOrNull(snsPressureGuageCount)){
                    nsPressureGuageCount = Integer.parseInt(snsPressureGuageCount);
                    nsPriceParam.setNs_section_combinedFilter_nsPressureGuageCount(nsPressureGuageCount);
                }
            }
        } else if (SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionId)) {
            nsPriceParam.setSectionType(SectionTypeEnum.TYPE_ACCESS.getCode());
            String nsPriceEnable = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_ENABLE)));
            nsPriceParam.setEnable(nsPriceEnable);
            String nsDrainPan = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NSDRAINPAN)));
            if(ServiceConstant.SYS_ASSERT_TRUE.equals(nsDrainPan)) {//空段，变更水盘
                double sectionL = NumberUtil.convertStringToDouble(String.valueOf(partMeta.get(SectionMetaUtils.getMetaSectionKey(sectionId, MetaKey.KEY_SECTIONL))));
                nsPriceParam.setSectionL(sectionL);
                nsPriceParam.setNs_section_access_nsDrainpan(nsDrainPan);
                String nsDrainpanMaterial = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_DRAINPANMATERIAL)));
                nsPriceParam.setNs_section_access_nsDrainpanMaterial(nsDrainpanMaterial);
                String nsDrainpanType = String.valueOf(partMeta.get(SectionMetaUtils.getNSSectionKey(sectionId, MetaKey.KEY_NS_DRAINPANTYPE)));
                nsPriceParam.setNs_section_access_nsDrainpanType(nsDrainpanType);

            }
        }
        return nsPriceParam;

    }
    /**
     * ahu非标价格
     * @param unitMeta
     * @return
     */
    private String getNSPriceOfUnit(Map<String, Object> unitMeta,Unit unit) {
        double nsPriceOfUnit = 0;
        boolean isClearPrice =false;
        String message = "";
        
        String nsPriceEnable = String.valueOf(unitMeta.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_ENABLE)));
        if (SYS_ASSERT_TRUE.equals(nsPriceEnable)){//允许非标
            String nsPriceKey = SectionMetaUtils.getNSAHUKey(MetaKey.KEY_PRICE);
            nsPriceOfUnit += NumberUtils.toDouble(String.valueOf(unitMeta.get(nsPriceKey)));

            //判断如果选中允许非标，则当memo不为空的时候，价格必须不能为空
            String nsPrice = String.valueOf(unitMeta.get(nsPriceKey));
            String nsPriceMemo = SectionMetaUtils.getNSAHUKey(MetaKey.KEY_MEMO);
            String memo = String.valueOf(unitMeta.get(nsPriceMemo));
            if(!EmptyUtil.isEmptyOrNull(memo) && EmptyUtil.isEmptyOrNull(nsPrice)){
                isClearPrice = true;
                //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
            }

            NSPriceParam nsPriceParam = new NSPriceParam();
            nsPriceParam.setSerial(String.valueOf(unitMeta.get(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_SERIAL))));
            nsPriceParam.setUnitid(unit.getUnitid());
            // channel steel base price
            nsPriceParam.setNs_ahu_nsChannelSteelBase(
                    String.valueOf(unitMeta.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_CHANNEL_STEEL_BASE))));
            nsPriceParam.setNs_ahu_nsChannelSteelBaseModel(String
                    .valueOf(unitMeta.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_CHANNEL_STEEL_BASE_MODEL))));
            // deformation price
            nsPriceParam.setBoxPrice(NumberUtils
                    .toDouble(String.valueOf(unitMeta.get(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_BOX_PRICE)))));
            nsPriceParam.setNs_ahu_deformation(
                    String.valueOf(unitMeta.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_DEFORMATION))));
            String deformationSerialKey = SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_DEFORMATIONSERIAL);
            String deformationSerial = String.valueOf(unitMeta.get(deformationSerialKey));
            nsPriceParam.setDeformationSerial(deformationSerial);
            if(EmptyUtil.isNotEmpty(nsPriceParam.getDeformationSerial()) && nsPriceParam.getDeformationSerial().length() == 4) {
                nsPriceParam.setNswidth(NumberUtils.toInt(deformationSerial.substring(2)));
                nsPriceParam.setNsheight(NumberUtils.toInt(deformationSerial.substring(0, 2)));
            }
//            nsPriceParam.setNswidth(NumberUtils
//                    .toInt(String.valueOf(unitMeta.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_WIDTH)))));
//            nsPriceParam.setNsheight(NumberUtils
//                    .toInt(String.valueOf(unitMeta.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_HEIGHT)))));
            Map<String, String> nsPriceMap = priceService.getNSPrice(nsPriceParam);

            String nsPriceDeformation = String.valueOf(unitMeta.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_DEFORMATION)));
            if (SYS_ASSERT_TRUE.equals(nsPriceDeformation)){//ahu变形
                String nsDeformationPrice = SectionMetaUtils.getNSAHUKey(MetaKey.KEY_DEFORMATIONPRICE);
                //nsPriceOfUnit += NumberUtils.toDouble(String.valueOf(unitMeta.get(nsDeformationPrice)));
                String nsDeformationPrices = String.valueOf(unitMeta.get(nsDeformationPrice));
                if(EmptyUtil.isEmptyOrNull(nsDeformationPrices)){
                    isClearPrice = true;
                    //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
                }else{//如果校验通过，则计算最新价格
                    //nsPriceOfUnit += NumberUtils.toDouble(String.valueOf(nsPriceMap.get(MetaKey.KEY_DEFORMATION_PRICE)));
                    JSONObject ahuJson = (JSONObject) JSON.parse(unit.getMetaJson());
                    String sNsDeformationPrice = String.valueOf(nsPriceMap.get(MetaKey.KEY_DEFORMATION_PRICE));
                    int nsDeformationPriceDouble = 0;
                    if(!EmptyUtil.isEmptyOrNull(sNsDeformationPrice)){
                        nsDeformationPriceDouble = NumberUtil.convertStringToDoubleInt(sNsDeformationPrice);
                    }
                    nsPriceOfUnit += nsDeformationPriceDouble;
                    ahuJson.put(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_DEFORMATION_PRICE), nsDeformationPriceDouble);
                    unit.setMetaJson(JSON.toJSONString(ahuJson));
                    this.ahuDao.save(unit);
                }
            }

            String nsChannelSteelBase = String.valueOf(unitMeta.get(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_CHANNEL_STEEL_BASE)));
            if (SYS_ASSERT_TRUE.equals(nsChannelSteelBase)){//槽钢底座
                String nsChannelSteelBasePrice = SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_CHANNEL_STEEL_BASE_PRICE);

                String nsChannelSteelBasePrices = String.valueOf(unitMeta.get(nsChannelSteelBasePrice));
                if(EmptyUtil.isEmptyOrNull(nsChannelSteelBasePrices)){
                    //nsPriceOfUnit += NumberUtils.toDouble(String.valueOf(unitMeta.get(nsChannelSteelBasePrice)));
                    isClearPrice = true;
                    //throw new PriceCalcException(ErrorCode.PRICE_CALCULATION_FAILED_BY_NS);
                }else{//如果校验通过，则计算最新价格
                    //nsPriceOfUnit += NumberUtils.toDouble(String.valueOf(nsPriceMap.get(MetaKey.KEY_NS_CHANNEL_STEEL_BASE_PRICE)));
                    JSONObject ahuJson = (JSONObject) JSON.parse(unit.getMetaJson());
                    String sNsChannelSteelBasePrice = String.valueOf(nsPriceMap.get(MetaKey.KEY_NS_CHANNEL_STEEL_BASE_PRICE));
                    int nsChannelSteelBasePriceDouble = 0;
                    if(!EmptyUtil.isEmptyOrNull(sNsChannelSteelBasePrice)) {
                        nsChannelSteelBasePriceDouble = NumberUtil.convertStringToDoubleInt(sNsChannelSteelBasePrice);
                    }
                    nsPriceOfUnit += nsChannelSteelBasePriceDouble;
                    ahuJson.put(SectionMetaUtils.getNSAHUKey(MetaKey.KEY_NS_CHANNEL_STEEL_BASE_PRICE), nsChannelSteelBasePriceDouble);
                    unit.setMetaJson(JSON.toJSONString(ahuJson));
                    this.ahuDao.save(unit);
                }
            }
            
            if(isClearPrice){
                message = SYS_PUNCTUATION_SLASH + getIntlString(VALIDATE_NS_AHU, AHUContext.getLanguage());
            }
            
        }
        return String.valueOf(nsPriceOfUnit) + "||" + isClearPrice + "||" + message;
    }

    private List<String> getMultiSectionCode(List<PriceCodePO> priceCodes) {
        Map<String, Integer> sectionCodeMap = new HashMap<>();
        for (PriceCodePO priceCode : priceCodes) {
            String sectionCode = priceCode.getSectionCode();
            if (sectionCodeMap.containsKey(sectionCode)) {
                int count = sectionCodeMap.get(sectionCode);
                sectionCodeMap.put(sectionCode, count + 1);
            } else {
                sectionCodeMap.put(sectionCode, 1);
            }
        }
        List<String> multiSectionCodes = new ArrayList<>();
        sectionCodeMap.forEach((sectionCode, count) -> {
            if (count > 1) {
                multiSectionCodes.add(sectionCode);
            }
        });
        return multiSectionCodes;
    }

}

package com.carrier.ahu.rest;

import static com.carrier.ahu.common.intl.I18NConstants.REPORT_GENERATING;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.ReportRequestVo;

import io.swagger.annotations.Api;

@Api(description = "报表接口")
@RestController
public class ReportController extends AbstractController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private GenerateReportTask generateReportTask;

    @RequestMapping(value = "report", method = RequestMethod.POST)
    public ApiResult<Object> report(ReportRequestVo requestVo) throws Exception {
        String projectId = requestVo.getReportsObj().getProjectId();
        if (EmptyUtil.isEmpty(projectId)) {
            throw new ApiException(ErrorCode.PROJECT_ID_IS_EMPTY);
        }
        Project project = projectService.getProjectById(projectId);
        if (EmptyUtil.isEmpty(project)) {
            throw new ApiException(ErrorCode.PROJECT_IS_NOT_EXIST);
        }

        generateReportTask.generateReport(requestVo.getReportsObj(), project);

        return ApiResult.success(getIntlString(REPORT_GENERATING));
    }

}

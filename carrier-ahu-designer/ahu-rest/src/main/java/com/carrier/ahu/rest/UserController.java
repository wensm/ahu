package com.carrier.ahu.rest;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.common.entity.UserMetaId;
import com.carrier.ahu.common.enums.UserMetaEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.UserMetaVO;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by liujianfeng on 2017/9/10.
 */
@Api(description = "多语言相关接口")
@RestController
public class UserController extends AbstractController {

	@RequestMapping(value = "calculationNumber/query", method = RequestMethod.GET)
	public ApiResult<String> getQueryCalculationNumber() {

		return null;
	}

	@RequestMapping(value = "calculationNumber/save", method = RequestMethod.POST)
	public ApiResult<String> getSaveCalculationNumber() {

		return null;
	}

	// seems not been used
	public ApiResult<String> updateLanguangeSetting(String language) throws Exception {
		User user = getUser();
		if (!isValidLanguage(language)) {
			throw new ApiException(String.format("The language is not found: %s ", language));
		}
		user.setPreferredLocale(language);
		return ApiResult.success(language);
	}

	private boolean isValidLanguage(String language) {
		return false;
	}

	/**
	 * é
	 *
	 * @return 用户对象，包含用户相关的默认信息
	 */
	@RequestMapping(value = "user", method = RequestMethod.GET)
	public ApiResult<User> getUserProfile() throws Exception {
		return ApiResult.success(getUser());
	}

	@RequestMapping(value = "user/meta", method = RequestMethod.GET)
	public ApiResult<UserMetaVO> getUserMeta() throws Exception {
		UserMetaVO userMetaVo = new UserMetaVO(AHUContext.getUserName(), AHUContext.getFactoryName());
		List<UserMeta> userMetas = this.userService.findUserMetaByUserAndFactory(userMetaVo.getUserId(),
				userMetaVo.getFactoryId());
		if (userMetas.size() < 1) {
			User user = this.userService.findUserById(userMetaVo.getUserId());
			if (user != null) {

				for (UserMetaEnum userMetaEnum : UserMetaEnum.values()) {
					UserMeta uMeta = AHUContext.getUserMeta(userMetaEnum);
					if (EmptyUtil.isEmpty(uMeta) || CommonConstant.SYS_ASSERT_NULL.equals(uMeta)) {
						uMeta = new UserMeta();
						UserMetaId userMetaId = new UserMetaId();
						userMetaId.setUserId(user.getUserId());
						userMetaId.setMetaKey(userMetaEnum.name());
						uMeta.setUserMetaId(userMetaId);
						uMeta.setFactoryId(user.getUserFactory());
						uMeta.setMetaValue(userMetaEnum.getDefaultValue());
						userMetas.add(uMeta);
					}
				}
			}
		}
		userMetaVo.addUserMeta(userMetas);
		return ApiResult.success(userMetaVo);
	}

	/**
	 * update userMetaId.metaKey and metaValue.
	 * 
	 * MetaKey must be from UserMetaEnum.
	 * 
	 * @param userMetaVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "user/meta", method = RequestMethod.POST)
	public ApiResult<String> saveUserMeta(UserMetaVO userMetaVo) throws Exception {
		try {
			User user = this.userService.findUserById(userMetaVo.getUserId());
			if (user != null) {
				Map<String, String> metaData = userMetaVo.getMetaData();
				if (metaData != null) {
					List<UserMeta> updatedUserMetas = new ArrayList<>();
					Iterator<String> metaKeyItr = metaData.keySet().iterator();
					while (metaKeyItr.hasNext()) {
						String metaKey = metaKeyItr.next();
						UserMetaEnum userMetaEnum = UserMetaEnum.valueOf(metaKey);
						UserMetaId userMetaId = new UserMetaId();
						userMetaId.setUserId(user.getUserId());
						userMetaId.setMetaKey(userMetaEnum.name());

						UserMeta newUserMeta = new UserMeta();
						newUserMeta.setUserMetaId(userMetaId);
						newUserMeta.setFactoryId(user.getUserFactory());
						newUserMeta.setMetaValue(metaData.get(metaKey));
						updatedUserMetas.add(newUserMeta);
					}
					this.userService.saveUserMetas(updatedUserMetas);
					AHUContext.setUserMeta(
							this.userService.findUserMetaByUserAndFactory(user.getUserId(), user.getUserFactory()));
				}
			}
		} catch (Exception ex) {
			return ApiResult.error("Failed to update user meta.");
		}
		return ApiResult.success();
	}

	/**
	 * é
	 *
	 * @return 用户对象，包含用户相关的默认信息
	 */
	@RequestMapping(value = "user", method = RequestMethod.POST)
	public ApiResult<User> updateUserProfile(User user) {
		User currentUser = this.getUser();
		if (EmptyUtil.isNotEmpty(currentUser.getUserId())) {
			if (EmptyUtil.isNotEmpty(user.getUnitPrefer())) {// 这个场景，默认前端不会调用，前端默认会设置单位代码，而不具体设置某一个属性，留着将来扩展用
				currentUser.setUnitPrefer(user.getUnitPrefer());
			}
			if (EmptyUtil.isNotEmpty(user.getUnitPreferCode())) {
				currentUser.setUnitPreferCode(user.getUnitPreferCode());
				currentUser.setUnitPrefer(AhuSectionMetas.getInstance().getUnitSetting().get(user.getUnitPreferCode()));
			}
			if (EmptyUtil.isNotEmpty(user.getPreferredLocale())) {
				currentUser.setPreferredLocale(user.getPreferredLocale());
			}
			if (EmptyUtil.isNotEmpty(user.getDefaultParaPrefer())) {
				currentUser.setDefaultParaPrefer(user.getDefaultParaPrefer());
			}
			this.userService.updateUser(currentUser, currentUser.getUserName());
		}
		// or throw exception for invalid user profile
		return ApiResult.success(currentUser);
	}

}

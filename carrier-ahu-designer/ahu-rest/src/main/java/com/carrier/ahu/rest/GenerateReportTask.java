package com.carrier.ahu.rest;

import static com.carrier.ahu.common.intl.I18NConstants.COLLECTING;
import static com.carrier.ahu.common.intl.I18NConstants.GENERATE_FAILED;
import static com.carrier.ahu.common.intl.I18NConstants.GENERATE_SUCCESS;
import static com.carrier.ahu.common.intl.I18NConstants.GENERATING;
import static com.carrier.ahu.common.intl.I18NConstants.REPORT_GENERATING;
import static com.carrier.ahu.common.intl.I18NConstants.SECTION_INFORMATION;
import static com.carrier.ahu.common.intl.I18NConstants.SPLIT_SECTION;
import static com.carrier.ahu.report.common.ReportConstants.*;
import static com.carrier.ahu.vo.SysConstants.COLON;
import static com.carrier.ahu.vo.SystemCalculateConstants.PARTITOIN_POSITION_BOTTOM;
import static com.carrier.ahu.vo.SystemCalculateConstants.PARTITOIN_POSITION_TOP;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.report.*;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.calculator.ExcelForPriceCodeUtils;
import com.carrier.ahu.calculator.price.PriceCodePO;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.model.partition.PanelCalculationObj;
import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.engine.cad.AhuExporter;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.report.ReportExcelProject.ProjAhu;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.service.cal.CalContextUtil;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.SysConstants;

/**
 * Async generate report task.
 * 
 * @author Braden Zhou
 *
 */
@Component
public class GenerateReportTask {

    protected static Logger logger = LoggerFactory.getLogger(GenerateReportTask.class);

    @Autowired
    private AhuService ahuService;
    @Autowired
    private SectionService sectionService;
    @Autowired
    private PartitionService partitionService;
    @Autowired
    private AhuExporter exporter;

    @Async
    public void generateReport(ReportRequestVob reportRequest, Project project) {
        CalContextUtil.reload();
        CalContextUtil.sendMessage(project.getPid(), AHUContext.getIntlString(REPORT_GENERATING));

        ReportData reportData = new ReportData();
        reportData.setProject(project);
        populatePartition(project.getPid(), reportRequest.getAhuIds(), reportData);
        populatePartData(reportData);
        List<Report> generatedReports = generateReportFile(reportRequest.getReports(), reportData);
        reportGenerated(reportRequest.getProjectId(), reportRequest.getAhuIds(), generatedReports);
    }

    private void populatePartition(String projectId, String[] reportIds, ReportData reportData) {
        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                AHUContext.getIntlString(COLLECTING) + COLON + AHUContext.getIntlString(SPLIT_SECTION));

        List<Unit> projectUnitList = ahuService.findAhuList(projectId, null);
        Map<String, Partition> partitionMap = new HashMap<>();

        List<Unit> reportUnitList = new ArrayList<>();
        if (EmptyUtil.isEmpty(reportIds)) {
            for (Unit unit : projectUnitList) {// TODO 面板布置结束才可以导出
                if (AhuStatusEnum.SELECT_COMPLETE.getId().equals(unit.getRecordStatus())) {
                    reportUnitList.add(unit);
                    Partition partition = partitionService.findPartitionByAHUId(unit.getUnitid());
                    try {
                    	//TODO 如果机组状态为选型完成，但是分段信息丢失的情况下，插入默认分段给机组
                    	if(CommonConstant.isInjectPartition) {
                            if (EmptyUtil.isEmpty(partition)) {
                            	partition = AhuPartitionGenerator.generatePartition(unit.getUnitid(), unit, sectionService.findSectionList(unit.getUnitid()));
                                partitionService.savePartition(partition, CommonConstant.INJECTPARTITIONUSER);                }
                    		}
                    }catch(Exception e) {
                    	logger.error("missed seciton partition data. exception throwed during inject partition data ");
                    }
                    partitionMap.put(unit.getUnitid(), partition);
                }
            }
        } else {
            List<String> reportIdList = Arrays.asList(reportIds);
            for (Unit unit : projectUnitList) {
                if (reportIdList.contains(unit.getUnitid())
                        && AhuStatusEnum.SELECT_COMPLETE.getId().equals(unit.getRecordStatus())) {
                    reportUnitList.add(unit);
                    Partition partition = partitionService.findPartitionByAHUId(unit.getUnitid());
                    try {
                    	//TODO 如果机组状态为选型完成，但是分段信息丢失的情况下，插入默认分段给机组
                    	if(CommonConstant.isInjectPartition) {
                            if (EmptyUtil.isEmpty(partition)) {
                            	partition = AhuPartitionGenerator.generatePartition(unit.getUnitid(), unit, sectionService.findSectionList(unit.getUnitid()));
                                partitionService.savePartition(partition, CommonConstant.INJECTPARTITIONUSER);                }
                    		}
                    }catch(Exception e) {
                    	logger.error("missed seciton partition data. exception throwed during inject partition data ");
                    }
                    partitionMap.put(unit.getUnitid(), partition);
                }
            }
        }
        Collections.sort(reportUnitList, new Comparator<Unit>() {
            public int compare(Unit u1, Unit u2) {
                return u1.getUnitNo().compareTo(u2.getUnitNo());
            }
        });
        reportData.setUnitList(reportUnitList);
        reportData.setPartitionMap(partitionMap);
    }

    private void populatePartData(ReportData reportData) {
        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                AHUContext.getIntlString(COLLECTING) + COLON + AHUContext.getIntlString(SECTION_INFORMATION));

        Map<String, PartPO> partMap = new LinkedHashMap<String, PartPO>();
        for (Unit unit : reportData.getUnitList()) {
            List<PartPO> partList = sectionService.findLinkedSectionsList(unit.getUnitid());
            for (PartPO partPO : partList) {
                Part part = partPO.getCurrentPart();
                partMap.put(UnitConverter.genUnitKey(unit, part), partPO);
            }
        }
        reportData.setPartMap(partMap);
    }

    private List<Report> generateReportFile(Report[] reports, ReportData reportData) {
        List<Report> returnReportList = new ArrayList<>();
        String path = SysConstants.ASSERT_DIR + SysConstants.REPORT_DIR + reportData.getProject().getPid();
        Report wordReport = getWordReport(reports);
        for (Report report : reports) {
            if (isNeedOutput(report,wordReport)) {
                String type = report.getType();
                if (SysConstants.EXCEL.equals(type)) {
                    try {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATING) + COLON + report.getName());
                        generateExcel(report, path, reportData);
                        returnReportList.add(report);
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_SUCCESS) + COLON + report.getName());
                    } catch (Exception e) {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_FAILED) + COLON + report.getName());
                        logger.error("Failed to generate excel report: " + report.getName(), e);
                    }
                } else if (SysConstants.PDF.equals(type)) {
                    try {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATING) + COLON + report.getName());
                        generatePdf(report, path, reportData);
                        returnReportList.add(report);
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_SUCCESS) + COLON + report.getName());
                    } catch (Exception e) {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_FAILED) + COLON + report.getName());
                        logger.error("Failed to generate pdf report: " + report.getName(), e);
                    }
                } else if (SysConstants.WORD.equals(type)) {
                    try {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATING) + COLON + report.getName());
                        generateWord(report, path, reportData);
                        returnReportList.add(report);
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_SUCCESS) + COLON + report.getName());
                    } catch (Exception e) {
                        CalContextUtil.sendMessage(reportData.getProject().getPid(),
                                AHUContext.getIntlString(GENERATE_FAILED) + COLON + report.getName());
                        logger.error("Failed to generate pdf report: " + report.getName(), e);
                    }
                }
            }
        }
        return returnReportList;
    }

    private Report getWordReport(Report[] reports) {
        for (Report report : reports) {
            if (Word4ReportUtils.T_TECHPROJ.equals(report.getClassify())) {// word技术说明(工程版)跳过
                return report;
            }
        }
        return null;
    }

    /**
     * 判断报告是否打印
     * pdf 报告前端没有勾选、word 报告前端没有勾选：如果选择了技术报告子项，pdf 报告也返回true（pdf报告作为默认报告格式）
     * @param report
     * @param wordReport
     * @return
     */
    private boolean isNeedOutput(Report report, Report wordReport) {
        if(report.isOutput()){
            return true;
        }else{
            if (Word4ReportUtils.T_TECHPROJ.equals(report.getClassify())) {// word技术说明(工程版)跳过
                return false;
            }
            if (PDF4ReportUtils.T_TECHPROJ.equals(report.getClassify())) {// pdf技术说明(工程版)允许只打印item项。
                for (ReportItem item : report.getItems()) {
                    if(item.isOutput() && !wordReport.isOutput()){//word技术说明报告打印，pdf 不作为默认打印
                        report.setType(SysConstants.PDF);
                        return true;
                    }
                }
            }
            return false;
        }
    }

    private void generateExcel(Report report, String path, ReportData reportData) throws Exception {
        String filePath = path + "/" + report.getName() + SysConstants.EXCEL2007_EXTENSION;
        String excelClassify = report.getClassify();
        if ("prolist".equals(excelClassify)) {
            generateExcelProlist(filePath, reportData);
        } else if ("paralist".equals(excelClassify)) {
            generateExcelParalist(report, filePath, reportData);
        } else if ("delivery".equals(excelClassify)) {
            this.generateExcellDelivery(filePath, reportData);
        } else if ("crmlist".equals(excelClassify)) {
            this.generateExcellCrmList(filePath, reportData);
        } else if ("saplist".equals(excelClassify)) {
            filePath = path + "/" + report.getName() + SysConstants.ZIP_EXTENSION;
            this.generateExcelSAPPriceCode(filePath, reportData);
        } else if ("dwglist".equals(excelClassify)) {
            filePath = path + "/" + report.getName() + SysConstants.ZIP_EXTENSION;
            this.generateDWGDrawing(filePath, reportData);
        } else {
            logger.warn("Report Excel Document Classify Is Not Exist: " + excelClassify);
        }
        report.setPath(filePath.replace(SysConstants.ASSERT_DIR, SysConstants.FILES_DIR));
    }

    private void generateExcellCrmList(String filePath, ReportData reportData) throws IOException {
        List<ReportExcelCRMList> crmList = new ArrayList<>();
        for (Unit unit : reportData.getUnitList()) {
            ProjAhu proAhu = transUnit2ProjAhu(unit);
            try {
                ReportExcelCRMList reprotCrmList = new ReportExcelCRMList();
                reprotCrmList.setUnitId(unit.getUnitid());
                reprotCrmList.setUnitType(unit.getProduct());
                if("true".equals(proAhu.getDeformation())){//crm模板导出型号使用变形后型号
                    reprotCrmList.setSerial(unit.getSeries().substring(0,unit.getSeries().length()-4) + proAhu.getDeformationSerial());
                }else{
                    reprotCrmList.setSerial(unit.getSeries());
                }
                reprotCrmList.setStdPrice(proAhu.getStdPrice());
                reprotCrmList.setFactoryForm(proAhu.getFactoryForm());
                reprotCrmList.setMount(unit.getMount());
                reprotCrmList.setDrawingNo(unit.getDrawingNo());
                reprotCrmList.setFacePrice(proAhu.getPackingPrice());

                //产地
                String cd = LanguageEnum.Chinese.equals(AHUContext.getLanguage())?"上海1":"CARS";
                reprotCrmList.setProductionPlace(cd);
                String contract = reportData.getProject().getContract();
                if(EmptyUtil.isNotEmpty(contract)){
                    reprotCrmList.setContract(contract.substring(0, contract.length() - 1));
                    reprotCrmList.setSelectionTimes(contract.substring(contract.length() - 1));
                }

                //绿色建筑
                String healthBuildingStr = createHBS(unit,reportData);
                reprotCrmList.setIsHealthBuilding(healthBuildingStr.length()>0?"1":"0");
                reprotCrmList.setHealthBuilding(healthBuildingStr);

                crmList.add(reprotCrmList);
            } catch (Exception e) {
                logger.warn("报表-参数列表-CRM清单信息整合-异常, UnitId: " + unit.getUnitid(), e);
                continue;
            }
        }
        String reportTemplate = LanguageEnum.Chinese.equals(AHUContext.getLanguage())
                ? RestConstant.SYS_PATH_TEMPLATE_PATH_CRMLIST
                : RestConstant.SYS_PATH_TEMPLATE_PATH_CRMLIST_EN;
        File tempSource = new File(reportTemplate);
        File tempDest = new File(filePath);
        if (tempDest.exists() && BACKUPFILE_EXCELLCRMLIST) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(tempDest, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        FileCopyUtils.copyFileUsingApacheCommonsIO(tempSource, tempDest);

        Collections.sort(crmList, new Comparator<ReportExcelCRMList>() {
            public int compare(ReportExcelCRMList o1, ReportExcelCRMList o2) {
                return ListUtils.addZero2Str(Integer.parseInt(o1.getDrawingNo()),5).compareTo(ListUtils.addZero2Str(Integer.parseInt(o2.getDrawingNo()),5));
            }
        });

        for(ReportExcelCRMList crm : crmList) {
            int i = Integer.parseInt(crm.getDrawingNo());
            crm.setContractSerial(crm.getSerial()
                    +(EmptyUtil.isNotEmpty(crm.getContract())?crm.getContract():"")
                    +(EmptyUtil.isNotEmpty(crm.getSelectionTimes())?crm.getSelectionTimes():"")
                    +ListUtils.addZero2Str(i,3));
        }
        Excel4ReportUtils.generateExcelCRMList(tempDest, crmList);
    }

    /**
     * 获取机组的绿色建筑选项
     *
     * 选项	                                            类型	                    MEMO关键词	                条件	                                                                                    满足条件打印绿色建筑选项内容
     * ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
     * 光触媒Photocatalyst 	                            非标MEMO关键词	        光触媒或光催化或纳米光氢离子	判断：空段、混合段、出风段	                                                                光触媒
     * 活性炭过滤器 Carbon filter	                        非标MEMO关键词	        活性炭	                    判断：空段、单层综合过滤段                                                                 活性炭
     * 紫外线灯 UV light	                                标准段属性、非标MEMO关键词	紫外线	                    判断：空段、混合段、是否安装杀菌灯；出风段（memo包含活性炭过滤器）	                            紫外线灯
     * 高效过滤器 HEPA filter	                            标准段		                                        判断：机组包含高效过滤段	                                                                高效过滤器
     * 高压静电过滤器 Electrostatic filter	            标准段		                                        判断：机组包含静电过滤段	                                                                高压静电过滤器
     * 中效袋式过滤器，效率F7/F8/F9：F7/F8/F9 bag filter 	标准段属性、非标MEMO关键词	F7或F8或F9或GZ或YG 	        判断：机组包含综合过滤段选择了袋式过滤器且效率F7/F8/F9/GZ/YG ；或者memo包含F7/F8/F9/GZ/YG 	    中效袋式过滤器(效率F7/F8/F9/GZ/YG)
     * ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
     */
    private String createHBS(Unit unit, ReportData reportData) {
        StringBuffer hbs = new StringBuffer();
        //所有段信息
        List<PartPO> partList = new ArrayList<>();
        for (Map.Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
            if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
                partList.add(e.getValue());
            }
        }
        //所有段json
        Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);

        //绿色建筑中文关键词、内容
        String gcmKey1 = "光触媒";
        String gcmKey2 = "光催化";
        String gcmKey3 = "纳米光氢离子";
        String gcmZH = "光触媒";

        String hxtFilterKey = "活性炭";
        String hxtFilterZH = "活性炭过滤器";

        String uvLampKey = "紫外线";
        String uvLampZH = "紫外线灯";

        String HEPAFilterZH  = "高效过滤器";
        String electrostaticFilterZH  = "高压静电过滤器";
        String combinedFilterZH = "中效袋式过滤器(效率F7/F8/F9/GZ/YG)";

        //处理绿色建筑内容
        boolean hasgcm = false;
        boolean hashxtFilterZH = false;
        boolean hasuvLampZH = false;
        boolean hasHEPAFilterZH = false;
        boolean haselectrostaticFilterZH = false;
        boolean hascombinedFilterZH = false;
        for (PartPO partPO : partList) {
            Part part = partPO.getCurrentPart();
            String key = part.getSectionKey();
            Map<String, String> allPartMap = allMap.get(UnitConverter.genUnitKey(unit, part));

            //1、光触媒
            if(!hasgcm && (SectionTypeEnum.TYPE_ACCESS.equals(SectionTypeEnum.getSectionTypeFromId(key))
                    || SectionTypeEnum.TYPE_MIX.equals(SectionTypeEnum.getSectionTypeFromId(key))
                    || SectionTypeEnum.TYPE_DISCHARGE.equals(SectionTypeEnum.getSectionTypeFromId(key)))){
                if(part.getMetaJson().contains(gcmKey1) || part.getMetaJson().contains(gcmKey2) || part.getMetaJson().contains(gcmKey3)){
                    hbs.append(gcmZH).append(SYS_PUNCTUATION_COMMA);
                    hasgcm = true;
                }
            }

            //2、活性炭
            if(!hashxtFilterZH && (SectionTypeEnum.TYPE_ACCESS.equals(SectionTypeEnum.getSectionTypeFromId(key))
                    || SectionTypeEnum.TYPE_SINGLE.equals(SectionTypeEnum.getSectionTypeFromId(key))
                    || SectionTypeEnum.TYPE_COMPOSITE.equals(SectionTypeEnum.getSectionTypeFromId(key)))){
                if(part.getMetaJson().contains(hxtFilterKey)){
                    hbs.append(hxtFilterZH).append(SYS_PUNCTUATION_COMMA);
                    hashxtFilterZH = true;
                }
            }

            //3、紫外线灯
            if(!hasuvLampZH && SectionTypeEnum.TYPE_ACCESS.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                String uvLamp = BaseDataUtil.constraintString(allPartMap.get(UtilityConstant.METASEXON_ACCESS_UVLAMP));
                if (UtilityConstant.SYS_ASSERT_TRUE.equals(uvLamp)) {
                    hbs.append(uvLampZH).append(SYS_PUNCTUATION_COMMA);
                    hasuvLampZH = true;
                }
            }
            if(!hasuvLampZH && SectionTypeEnum.TYPE_MIX.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                String uvLamp = BaseDataUtil.constraintString(allPartMap.get(UtilityConstant.METASEXON_MIX_UVLAMP));
                if (UtilityConstant.SYS_ASSERT_TRUE.equals(uvLamp)) {
                    hbs.append(uvLampZH).append(SYS_PUNCTUATION_COMMA);
                    hasuvLampZH = true;
                }
            }
            if(!hasuvLampZH && SectionTypeEnum.TYPE_DISCHARGE.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                if(part.getMetaJson().contains(uvLampKey)){//出风段只判断memo即可
                    hbs.append(uvLampZH).append(SYS_PUNCTUATION_COMMA);
                    hasuvLampZH = true;
                }
            }

            //4、高效过滤器
            if(!hasHEPAFilterZH && SectionTypeEnum.TYPE_HEPAFILTER.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                hbs.append(HEPAFilterZH).append(SYS_PUNCTUATION_COMMA);
                hasHEPAFilterZH = true;
            }

            //5、高压静电过滤器
            if(!haselectrostaticFilterZH && SectionTypeEnum.TYPE_ELECTROSTATICFILTER.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                hbs.append(electrostaticFilterZH).append(SYS_PUNCTUATION_COMMA);
                haselectrostaticFilterZH = true;
            }

            //6、中效综合袋式过滤器
            if(!hascombinedFilterZH && SectionTypeEnum.TYPE_COMPOSITE.equals(SectionTypeEnum.getSectionTypeFromId(key))){

                //选择了过滤器，效率F7/F8/F9/GZ/YG
                if(!JSON_FILTER_FILTEROPTIONS_NOFILTER.equals(allPartMap.get(METASEXON_COMBINEDFILTER_RMATERIAL))
                        && "F7/F8/F9/GZ/YG".contains(allPartMap.get(METASEXON_COMBINEDFILTER_RMATERIALE))){
                    hbs.append(combinedFilterZH).append(SYS_PUNCTUATION_COMMA);
                    hascombinedFilterZH = true;
                }
                //memo出现，效率F7/F8/F9/GZ/YG
                if(!hascombinedFilterZH && (part.getMetaJson().contains("F7")
                        ||part.getMetaJson().contains("F8")
                        ||part.getMetaJson().contains("F9")
                        ||part.getMetaJson().contains("GZ")
                        ||part.getMetaJson().contains("YG"))){
                    hbs.append(combinedFilterZH).append(SYS_PUNCTUATION_COMMA);
                    hascombinedFilterZH = true;
                }
            }
        }
        String rethbs = hbs.toString();
        if(rethbs.endsWith(SYS_PUNCTUATION_COMMA)){
            rethbs = rethbs.substring(0,rethbs.length()-1);
        }
        return rethbs;
    }

    private void generateExcelSAPPriceCode(String filePath, ReportData reportData) throws IOException {
        List<File> unitSAPFiles = new ArrayList<>();
        
        for (Unit unit : reportData.getUnitList()) {
            try {
                List<Part> parts = sectionService.findSectionList(unit.getUnitid());
                Partition partition = partitionService.findPartitionByAHUId(unit.getUnitid());
                List<PanelCalculationObj> casingList = AhuPartitionGenerator.getCasinglist(unit.getSeries(),unit, partition,false, true);
                List<PanelCalculationObj> XTcasingList = AhuPartitionGenerator.getXTCasinglist(unit.getSeries(),unit, partition);
                casingList.addAll(XTcasingList);
                List<AhuPartition> partitions = AhuPartitionUtils.parseAhuPartition(unit, partition);
                List<PriceCodePO> priceCodes = ExcelForPriceCodeUtils.getPriceCodes(unit, parts, partitions,false);
                List<Part> airFlowParts = ExcelForPriceCodeUtils.getPriceCodesAirFlowParts(unit, parts, partitions);

                PriceCodePO priceCodePo = priceCodes.remove(0); // move box to the end
                priceCodes.add(priceCodePo);

                AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
                List<SAPFunction> sapFunctions = new ArrayList<>();
                for (int i = 0; i < priceCodes.size(); i++) {
                    if (i == priceCodes.size() - 1) {
                        // process box
                        PriceCodePO priceCode = priceCodes.get(priceCodes.size() - 1);
                        SAPFunction sapFunction = new SAPFunction();
                        sapFunction.setFuncName(priceCode.getFuncNameWithoutNo());
                        sapFunction.setFuncCode(priceCode.getPriceCode());
                        sapFunction.setFuncList(0);
                        sapFunction.setInstSection(0);
                        sapFunction.setSectionPosition(String.valueOf(0));
                        sapFunction.setSectionWeight(String.valueOf(0));
                        sapFunctions.add(sapFunction);
                    } else {
                        Part part = airFlowParts.get(i);
                        PriceCodePO priceCode = priceCodes.get(i);
                        SAPFunction sapFunction = new SAPFunction();

                        if(priceCode.getFuncName().contains("_FUNC_W_")){
                            sapFunction.setFuncName(priceCode.getFuncName().split("_FUNC_W")[0]+"_FUNC_W");
                        }else {
                            sapFunction.setFuncName(priceCode.getFuncName());
                        }
                        sapFunction.setFuncCode(priceCode.getPriceCode());
                        sapFunction.setFuncList(priceCode.getSectionList());
                        sapFunction.setInstSection(priceCode.getPartitionNo());
                        sapFunction.setSectionPosition(getLayoutPosition(part, ahuLayout));
                        sapFunction.setSectionWeight(part.getMetaJson());
                        populatePosAndWeightOfPartitionOfPart(unit,part, partitions, sapFunction);
                        sapFunctions.add(sapFunction);
                    }
                }

                File tempSource = new File(RestConstant.SYS_PATH_TEMPLATE_PATH_SAP_PRICE_CODE);
                // use customer PO number as the file name
                File tempDest = new File(new File(filePath).getParentFile(),
                        (EmptyUtil.isEmpty(unit.getCustomerName()) ? unit.getUnitid() : unit.getCustomerName().trim())
                                + SYS_EXCEL2003_EXTENSION);
                if (tempDest.exists() && BACKUPFILE_SAPPRICECODE) {
                    FileCopyUtils.copyFileUsingApacheCommonsIO(tempDest, new File(FileCopyUtils.getBackupFileName(filePath)));
                }
                FileCopyUtils.copyFileUsingApacheCommonsIO(tempSource, tempDest);
                Excel4ReportUtils.generateExcelSAPPriceCode(tempDest, sapFunctions, casingList);
                unitSAPFiles.add(tempDest);
            } catch (Exception e) {
                logger.error("Failed to generate sap price report for unit: " + unit.getUnitid(), e);
            }
        }

        // generate zip file
        File zipFile = new File(filePath);
        if (zipFile.exists()) {
            zipFile.delete();
        }
        FileUtil.writeZipFile(filePath, unitSAPFiles);
    }

    private void generateDWGDrawing(String filePath, ReportData reportData) throws IOException {
        List<File> dwgFiles = new ArrayList<>();
        for (Unit unit : reportData.getUnitList()) {
            try {
                List<PartPO> partList = sectionService.findLinkedSectionsList(unit.getUnitid());
                Partition partition = partitionService.findPartitionByAHUId(unit.getUnitid());
                AhuParam ahuParam = ValueFormatUtil.transDBData2AhuParam(reportData.getProject(), unit, partList, partition);

                String fileName = unit.getDrawingNo()
                        + (EmptyUtil.isEmpty(unit.getName()) ? RestConstant.SYS_BLANK
                        : RestConstant.SYS_PUNCTUATION_LOW_HYPHEN + getLimitLen(unit.getName()))
                        + RestConstant.SYS_DWG_EXTENSION;
                String dwgFilePath = exporter.download(ahuParam, fileName);
                File dwgFile = new File(RestConstant.SYS_ASSERT_DIR + dwgFilePath);
                // 三秒DWG生成缓冲时间
                long startTime = System.currentTimeMillis();
                while (dwgFile.exists()) {
                    if (System.currentTimeMillis() - startTime > 4000) {
                        break;
                    }
                }
                dwgFiles.add(dwgFile);
            } catch (Exception e) {
                logger.error("Failed to generate dwg file for unit: " + unit.getUnitid(), e);
            }
        }

        // generate zip file
        File zipFile = new File(filePath);
        if (zipFile.exists()) {
            zipFile.delete();
        }
        File fileParent = zipFile.getParentFile();
        if(!fileParent.exists()){
            fileParent.mkdirs();
        }
        if (zipFile.createNewFile()) {
            FileUtil.writeZipFile(filePath, dwgFiles);
        }
    }

    /**
     *  cad_exporter.exe 参数1 参数2
     *  参数1如果内容过大导致cmd命令失败如果内容长度大于100 取100位
     * @param name
     * @return
     */
    private String getLimitLen(String name) {
        if(name.length()>100){
            return name.substring(0,100).trim();
        }
        return name.trim();
    }

    private static String getLayoutPosition(Part part, AhuLayout ahuLayout) {
        int[][] layoutData = ahuLayout.getLayoutData();
        if (layoutData.length == 4) { // wheel/plate heat recycle
            for (int i = 0; i < layoutData.length; i++) {
                int[] layout = layoutData[i];
                for (int pos : layout) {
                    if (pos == part.getPosition()) {
                        return (i == 0 || i == 1) ? PARTITOIN_POSITION_TOP : PARTITOIN_POSITION_BOTTOM;
                    }
                }
            }
        }
        // B as default position
        return PARTITOIN_POSITION_BOTTOM;
    }

    private static void populatePosAndWeightOfPartitionOfPart(Unit unit, Part part, List<AhuPartition> partitions, SAPFunction sapFunction) {
        for (AhuPartition partition : partitions) {
            LinkedList<Map<String, Object>> sections = partition.getSections();
            for (Map<String, Object> section : sections) {
                short posOfSection = AhuPartition.getPosOfSection(section);
                if (part.getPosition().equals(posOfSection)) {
                    double weight = SectionContentConvertUtils.getPartitionWeight(unit,partition,partitions);
                    sapFunction.setSectionWeight(String.valueOf(BaseDataUtil.doubleConversionInteger(weight)));
                    return;
                }
            }
        }
        sapFunction.setSectionWeight(String.valueOf(0));
    }

    private void generateExcelProlist(String filePath, ReportData reportData) throws IOException {
        ReportExcelProject reportExcelProject = new ReportExcelProject();
        reportExcelProject.setProject(reportData.getProject());
        List<ProjAhu> projAhus = transUnit2ProjAhu(reportData.getUnitList());
        reportExcelProject.setProjAhus(projAhus);

        String reportTemplate = LanguageEnum.Chinese.equals(AHUContext.getLanguage())
                ? RestConstant.SYS_PATH_TEMPLATE_PATH_PROJ
                : RestConstant.SYS_PATH_TEMPLATE_PATH_PROJ_EN;
        File tempSource = new File(reportTemplate);
        File tempDest = new File(filePath);
        if (tempDest.exists() && BACKUPFILE_EXCELPROLIST) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(tempDest, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        FileCopyUtils.copyFileUsingApacheCommonsIO(tempSource, tempDest);
        try {
            Excel4ReportUtils.generateExcelProlist(tempDest, reportExcelProject, reportData);
        } catch (Exception e) {
            logger.warn("报表-创建-[项目清单]-报表内容-异常, UnitId: " + reportData.getUnitList(), e);
        }
    }

    @SuppressWarnings("unchecked")
    private List<ProjAhu> transUnit2ProjAhu(List<Unit> ahuToOutputList) {
        List<ProjAhu> projAhus = new ArrayList<>();
        for (Unit unit : ahuToOutputList) {
            ProjAhu proAhu = transUnit2ProjAhu(unit);
            projAhus.add(proAhu);
        }
        Collections.sort(projAhus, new Comparator<ProjAhu>() {
            public int compare(ProjAhu o1, ProjAhu o2) {
                return o1.getUnitId().compareTo(o2.getUnitId());
            }
        });
        return projAhus;
    }

    /**
     * 转换单个ProjAhu
     * @param unit
     * @return
     */
    private ProjAhu transUnit2ProjAhu(Unit unit) {
        String ahuMetaJson = unit.getMetaJson();
        Map<String, String> metaMap = JSON.parseObject(ahuMetaJson, HashMap.class);
        ProjAhu ahu = new ProjAhu();
        ahu.setUnitId(unit.getUnitNo());
        ahu.setSerial(unit.getSeries());
        ahu.setAhuName(unit.getName());
        Short mount = EmptyUtil.isEmpty(unit.getMount()) ? -1 : unit.getMount();
        ahu.setAmount(mount);

        // 装箱单价: 价格引擎计算（包含非标）
        int packingPrice = (int)Math.round(EmptyUtil.isEmpty(unit.getPrice()) ? -1 : unit.getPrice());
        ahu.setPackingPrice(packingPrice);

        // 非标单价：非标价格引擎计算
        int nspackingPrice = (int)Math.round(EmptyUtil.isEmpty(unit.getNsprice()) ? -1 : unit.getNsprice());
        ahu.setNStdpackingPrice(nspackingPrice);

        // 标准单价：价格引擎计算（不包含非标）
        ahu.setStdPrice(packingPrice-nspackingPrice);

        // 总价格：装箱单价*个数
        ahu.setTotalPrice(packingPrice * mount);

        ahu.setWeight(EmptyUtil.isEmpty(unit.getWeight()) ? -1 : BaseDataUtil.doubleConversionInteger(unit.getWeight()));

        ahu.setFactoryForm(MetaSelectUtil.getTextForSelect("meta.ahu.delivery", metaMap.get("meta.ahu.delivery"),
                AHUContext.getLanguage()));
        ahu.setDeformation(String.valueOf(metaMap.get("ns.ahu.deformation")));

        //变形
        String deformationEnable = String.valueOf(metaMap.get(UtilityConstant.METANS_NS_AHU_DEFORMATION));
        if(UtilityConstant.SYS_ASSERT_TRUE.equals(deformationEnable) &&
                null != metaMap.get(METANS_NS_AHU_DEFORMATIONSERIAL)){
            ahu.setDeformationSerial(String.valueOf(metaMap.get(METANS_NS_AHU_DEFORMATIONSERIAL)));
        }
        return ahu;
    }

    private void generateExcelParalist(Report report, String filePath, ReportData reportData) throws IOException {
        String reportTemplate = LanguageEnum.Chinese.equals(AHUContext.getLanguage())
                ? RestConstant.SYS_PATH_TEMPLATE_PATH_PARA
                : RestConstant.SYS_PATH_TEMPLATE_PATH_PARA_EN;
        File tempSource = new File(reportTemplate);
        File tempDest = new File(filePath);
        if (tempDest.exists() && BACKUPFILE_PARALIST) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(tempDest, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        FileCopyUtils.copyFileUsingApacheCommonsIO(tempSource, tempDest);
        try {
            Excel4ReportUtils.generateExcelParalist(tempDest, report, reportData, AHUContext.getUnitSystem());
        } catch (Exception e) {
            logger.warn("报表-创建-[参数汇总]-报表内容-异常, UnitId: " + reportData.getUnitList(), e);
        }
    }

    private void generateExcellDelivery(String filePath, ReportData reportData) throws IOException {
        List<ReportExcelDelivery> deliveryList = new ArrayList<>();
        // TODO -Simon gen deliveryList
        for (Unit unit : reportData.getUnitList()) {
            try {
                List<Part> partList = sectionService.findSectionList(unit.getUnitid());
                ReportExcelDelivery reprotDelivery = Excel4ReportUtils.generateReportExcelDelivery(unit, partList);
                if (null != reprotDelivery) {
                    deliveryList.add(reprotDelivery);
                }
            } catch (Exception e) {
                logger.warn("报表-参数列表-延长交货周期选项列表信息整合-异常, UnitId: " + unit.getUnitid(), e);
                continue;
            }
        }
        String reportTemplate = LanguageEnum.Chinese.equals(AHUContext.getLanguage())
                ? RestConstant.SYS_PATH_TEMPLATE_PATH_DELI
                : RestConstant.SYS_PATH_TEMPLATE_PATH_DELI_EN;
        File tempSource = new File(reportTemplate);
        File tempDest = new File(filePath);
        if (tempDest.exists() && BACKUPFILE_EXCELLDELIVERY) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(tempDest, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        FileCopyUtils.copyFileUsingApacheCommonsIO(tempSource, tempDest);

        Collections.sort(deliveryList, new Comparator<ReportExcelDelivery>() {
            public int compare(ReportExcelDelivery o1, ReportExcelDelivery o2) {
                return o1.getUnitId().compareTo(o2.getUnitId());
            }
        });

        Excel4ReportUtils.generateExcelDelivery(tempDest, deliveryList);
    }

    private void generatePdf(Report report, String path, ReportData reportData) throws Exception {
        LanguageEnum language = AHUContext.getLanguage();
        UnitSystemEnum unitType = AHUContext.getUnitSystem();

        String filePath = path + "/" + report.getName() + SysConstants.PDF_EXTENSION;
        String pdfClassify = report.getClassify();

        File pdfFile = new File(filePath);
        if (!pdfFile.getParentFile().exists()) {
            pdfFile.getParentFile().mkdirs();
        }
        if (pdfFile.exists() && BACKUPFILE_PDF) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(pdfFile, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        if (PDF4ReportUtils.T_ARUPPF.equals(pdfClassify)) {// 奥雅纳工程格式报告
            PDF4ReportUtils.genPdf4Aruppf(pdfFile, report, reportData, language, unitType);
        } else if (PDF4ReportUtils.T_BOCHENGPF.equals(pdfClassify)) {// 柏诚工程格式报告
            PDF4ReportUtils.genPdf4Bochengpf(pdfFile, report, reportData, language, unitType);
        } else if (PDF4ReportUtils.T_TECHPROJ.equals(pdfClassify)) {// 技术说明(工程版)
            PDF4ReportUtils.genPdf4Techproj(pdfFile, report, reportData, language, unitType);
			report.setName(report.getName() + SysConstants.PDF_EXTENSION);
        } else if (PDF4ReportUtils.T_TECHSALER.equals(pdfClassify)) {// 技术说明(销售版)
            PDF4ReportUtils.genPdf4Techsaler(pdfFile, report, reportData, language, unitType);
        } else {
            logger.warn("Failed to generate pdf: Classify is undefined! >>PDFClassify: " + pdfClassify);
        }
        report.setPath(filePath.replace(SysConstants.ASSERT_DIR, SysConstants.FILES_DIR));
    }

    private void generateWord(Report report, String path, ReportData reportData) throws Exception {
        LanguageEnum language = AHUContext.getLanguage();
        UnitSystemEnum unitType = AHUContext.getUnitSystem();

        String filePath = path + "/" + report.getName() + SysConstants.WORD2007_EXTENSION;
        String wordClassify = report.getClassify();

        File wordFile = new File(filePath);
        if (!wordFile.getParentFile().exists()) {
            wordFile.getParentFile().mkdirs();
        }
        if (wordFile.exists() && BACKUPFILE_WORD) {
            FileCopyUtils.copyFileUsingApacheCommonsIO(wordFile, new File(FileCopyUtils.getBackupFileName(filePath)));
        }
        if (Word4ReportUtils.T_SPECIALLIST.equals(wordClassify)) {// 非标清单
            Word4ReportUtils.genWord4Speciallist(wordFile, report, reportData, language, unitType);
        } else if (Word4ReportUtils.T_TECHPROJ.equals(wordClassify)) {// 技术说明(工程版)
            Word4ReportUtils.genWord4Techproj(wordFile, report, reportData, language, unitType);
            report.setName(report.getName() + SysConstants.WORD2007_EXTENSION);
        } else {
            logger.warn("Failed to generate word: Classify is undefined! >>wordClassify: " + wordClassify);
        }
        report.setPath(filePath.replace(SysConstants.ASSERT_DIR, SysConstants.FILES_DIR));
    }

    private void reportGenerated(String projectId, String[] ahuIdArray, List<Report> generatedReports) {
        ReportRequestVob responseB = new ReportRequestVob();
        responseB.setProjectId(projectId);
        responseB.setAhuIds(ahuIdArray);
        responseB.setReports(generatedReports.toArray(new Report[generatedReports.size()]));

        CalContextUtil.sendResultMessage(projectId, JSON.toJSONString(responseB));
        CalContextUtil.sendMessage(projectId, AHUContext.getIntlString(GENERATE_SUCCESS));
        CalContextUtil.sendFinishMessage(projectId);
    }

}

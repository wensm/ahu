package com.carrier.ahu.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.vo.ApiResult;

import io.swagger.annotations.Api;

/**
 * 数据模板下载功能
 * @author WANGD1
 *
 */
@Api(description = "数据模板下载功能")
@RestController
public class DataTemplateDownload extends AbstractController {
	@RequestMapping(value = "/tool/templatedownload", method = RequestMethod.GET)
	public ApiResult<String> getDocument() throws Exception {
		LanguageEnum languageEnum = getLanguage();
		String documentPath = RestConstant.SYS_BLANK;
		String fileName = RestConstant.SYS_BLANK;
		if (languageEnum != null) {
			if (languageEnum.getId().equals(RestConstant.SYS_LANGUAGE_EN_US)) {// 英文环境下
				documentPath = RestConstant.SYS_PATH_TEMPLATE_EN;
				fileName = RestConstant.SYS_PATH_TEMPLATE_NAME_EN + RestConstant.SYS_EXCEL2003_EXTENSION;
			} else {
				documentPath = RestConstant.SYS_PATH_TEMPLATE_CN;
				fileName = RestConstant.SYS_PATH_TEMPLATE_NAME_CN + RestConstant.SYS_EXCEL2003_EXTENSION;
			}
		}
		return ApiResult.success(documentPath);
	}
}
package com.carrier.ahu.vo;

import java.util.Map;

import com.carrier.ahu.metadata.entity.AhuPropUnit;

import lombok.Data;

@Data
public class UnitMetadataVO {

    private Map<String, AhuPropUnit> unitMeta;
    private Map<String, Map<String, String>> unitSetting;

    public UnitMetadataVO(Map<String, AhuPropUnit> unitMeta, Map<String, Map<String, String>> unitSetting) {
        this.unitMeta = unitMeta;
        this.unitSetting = unitSetting;
    }

}

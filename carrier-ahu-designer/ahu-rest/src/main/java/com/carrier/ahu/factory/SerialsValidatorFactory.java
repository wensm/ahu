package com.carrier.ahu.factory;

import com.carrier.ahu.product.SerialsValidatorProduct;
import com.carrier.ahu.product.ValidatorProduct;
import com.carrier.ahu.service.AhuService;

/**
 * 机组系列校验器（工厂模式-工厂）
 * @author wangd1
 *
 */
public class SerialsValidatorFactory implements ValidatorFactory {

	@Override
	public ValidatorProduct getValidator(AhuService ahuService) {
		return new SerialsValidatorProduct(ahuService);
	}
	
	@Override
	public ValidatorProduct getValidator() {
		return new SerialsValidatorProduct();
	}
	
}
package com.carrier.ahu.factory;

import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Service;

import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.util.EmptyUtil;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;

/**
 * Created by liujianfeng on 2017/6/20.
 */
@Service
public class AhuServerProperties extends ServerProperties {

	

	@Override
	public Integer getPort() {
		if (EmptyUtil.isNotEmpty(super.getPort())) {
			return super.getPort();
		}
		int result = RestConstant.SYS_DEFAULT_PORTAL;
		for (; result < RestConstant.SYS_MAX_PORT_NUMBER; result++) {
			if (available(result)) {
				setPort(result);
				return getPort();
			}
		}
		throw new IllegalArgumentException("failed to get port ");

	}

	/**
	 * @param port
	 * @return
	 */
	public static boolean available(int port) {
		if (port > RestConstant.SYS_MAX_PORT_NUMBER) {
			throw new IllegalArgumentException("Invalid start port: " + port);
		}
		ServerSocket ss = null;
		DatagramSocket ds = null;
		try {
			ss = new ServerSocket(port);
			ss.setReuseAddress(true);
			ds = new DatagramSocket(port);
			ds.setReuseAddress(true);
			return true;
		} catch (IOException e) {
		} finally {
			if (EmptyUtil.isNotEmpty(ds)) {
				ds.close();
			}
			if (EmptyUtil.isNotEmpty(ss)) {
				try {
					ss.close();
				} catch (IOException e) {
				}
			}
		}
		return false;
	}

}

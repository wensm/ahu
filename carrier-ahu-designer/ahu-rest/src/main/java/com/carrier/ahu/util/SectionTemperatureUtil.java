package com.carrier.ahu.util;

public class SectionTemperatureUtil {

    public static double coilTemperatureValidation(double inDryBulbT, double inWetBulbT) {
        AirConditionBean airConditionBean = AirConditionUtils.FAirParmCalculate1(inDryBulbT, inWetBulbT);
        return airConditionBean.getParmF();
    }

}

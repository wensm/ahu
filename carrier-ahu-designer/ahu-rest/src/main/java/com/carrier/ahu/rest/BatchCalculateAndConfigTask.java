package com.carrier.ahu.rest;

import static com.carrier.ahu.common.intl.I18NConstants.BATCH_CONFIG_EXECUTE_COMPLETE;

import java.util.*;
import java.util.Map.Entry;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.length.util.SystemCountUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.BatchCfgProgress;
import com.carrier.ahu.common.entity.GroupInfo;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.entity.UserConfigParam;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.CalcTypeEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.AhuSizeDetail;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.BatchCfgProgressService;
import com.carrier.ahu.service.GroupService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.service.cal.CalContextUtil;
import com.carrier.ahu.service.cal.ICalContext;
import com.carrier.ahu.service.cal.impl.AhuCalBus;
import com.carrier.ahu.service.cal.impl.DefaultCalContext;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.Excel4ModelInAndOutUtils;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.UnitUtil;
import com.carrier.ahu.util.UserConfigUtil;
import com.carrier.ahu.util.ahu.AhuParamUtils;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.gson.Gson;

@Component
public class BatchCalculateAndConfigTask {
	protected static Logger logger = LoggerFactory.getLogger(BatchCalculateAndConfigTask.class);

	@Autowired
	ProjectService projectService;
	@Autowired
	AhuService ahuService;
	@Autowired
	SectionService sectionService;
	@Autowired
	GroupService groupService;
	@Autowired
	BatchCfgProgressService progressService;
	@Autowired
	private PartitionService partitionService;

	@Autowired
	private AhuStatusTool ahuStatusTool;
	@Autowired
	private CalculatorTool calculatorTool;

	@SuppressWarnings("unchecked")
	@Async
	public void runCalulator(String projectid, String groupid, CalcTypeEnum calcTypeEnum, LanguageEnum language,
							 String username, String ahuId,String userConfigString) {
		List<Unit> units = ahuService.findAhuList(projectid, groupid);
		if(!RestConstant.SYS_ASSERT_ALL.equals(ahuId)){//单个ahu批量参数计算
			Unit theOneUnit = ahuService.findAhuById(ahuId);
			if(null != theOneUnit){
				units = new ArrayList<Unit>();
				units.add(theOneUnit);
			}
		}
		List<Unit> toUpdateAhuList = new ArrayList<>();
		List<Part> toUpdatePartList = new ArrayList<>();
		Project project = projectService.getProjectById(projectid);
		GroupInfo group = groupService.findGroupById(groupid);
		Gson gson = new Gson();
		String rootName = project.getName();
		if(null != group){//未分组，一键选型，gourp 为null
			rootName += ">>" + group.getGroupName();
		}

		CalContextUtil.getInstance().setCalculatorExist(true);
		DefaultCalContext rootContext = new DefaultCalContext();

		CalContextUtil.getInstance().refreshContext(projectid, rootContext);
		rootContext.setText(ICalContext.NAME_ROOT);
		rootContext.setName(rootName);
		rootContext.setCalcTypeEnum(calcTypeEnum);
		if(!EmptyUtil.isEmpty(userConfigString)){
			Map<String,Object> mapUserConfig = gson.fromJson(userConfigString,Map.class);
			UserConfigParam userConfigParam = new UserConfigParam();
			userConfigParam.setMapConfig(mapUserConfig);
			rootContext.setUserConfigParam(userConfigParam);
		}
		Map<String, List<String>> result = new HashMap<>();
		result.put(RestConstant.SYS_MSG_RESPONSE_SUCCESS, new ArrayList<String>());
		result.put(RestConstant.SYS_MSG_RESPONSE_FAILURE, new ArrayList<String>());
		for (Unit unit : units) {
			// 计算机组型号
			String metaJson = unit.getMetaJson();
			if (StringUtils.isBlank(unit.getMetaJson())) {
				logger.warn("机组遍历时，未获取到MetaJson，Unitid：" + unit.getUnitid());
				return;
			}
			Map<String, Object> map = gson.fromJson(metaJson, Map.class);
			String volume = MapValueUtils.getStringValue(RestConstant.METAHU_SAIRVOLUME, map);
			String eairVolume = MapValueUtils.getStringValue(RestConstant.METAHU_EAIRVOLUME, map);
			if (StringUtils.isEmpty(volume)) {
				volume = String.valueOf(TemplateUtil.genAhuSectionTemplate(SectionTypeEnum.TYPE_AHU).get(RestConstant.METAHU_SAIRVOLUME));
			}
			double dAirVolume = Double.parseDouble(volume);
			if (StringUtils.isEmpty(eairVolume)) {
				eairVolume = String.valueOf(TemplateUtil.genAhuSectionTemplate(SectionTypeEnum.TYPE_AHU).get(RestConstant.METAHU_EAIRVOLUME));
			}
			double dEairVolume = Double.parseDouble(eairVolume);

			List<Part> pList = sectionService.findSectionList(unit.getUnitid());
			AhuParam para = null;

			List<AhuSizeDetail> dtList = getBestOne(dAirVolume, calcTypeEnum,userConfigString,unit);

			boolean ahuStatus = true;
			int tryCalnum=0;
			for (AhuSizeDetail dt : dtList) {
				if (dt == null) {
					break;
				}
				ahuStatus = true;
				tryCalnum++;
				updateAhuSerial(unit, dt, map, dAirVolume, dEairVolume, calcTypeEnum, userConfigString);
				para = AhuParamUtils.getAhuParam(unit, pList, null);
				para.setWeight(0d);
				rootContext.setCalOperation(CalOperationEnum.BATCH);
				AhuCalBus.getInstance().setCalOperation(CalOperationEnum.BATCH);
				try {
					AhuCalBus.getInstance().calculate(para, language, rootContext);

					if (EmptyUtil.isNotEmpty(para.getWeight()))
						unit.setWeight(getNoDotDouble(para.getWeight()));
					rootContext.setSuccess(true);
				} catch (Exception e) {
					ahuStatus = false;
					logger.debug(e.getMessage(), e);
					logger.error(e.getMessage());
					rootContext.setSuccess(false);
					rootContext.setError(e.getMessage());
					continue;
				}
				BatchCfgProgress progress = new BatchCfgProgress();
				progress.setProjectId(projectid);
				progress.setGroupId(groupid);
				progress.setGroupCode(unit.getGroupCode());
				progress.setJobJson(JSON.toJSONString(rootContext));
				progressService.addProgress(progress, username);

				Map<String, Object> paramsAfterCalc = para.getParams();
				List<PartParam> partParamsAfteralc = para.getPartParams();

				Gson ahuGson = new Gson();
				Map<String, Object> m = ahuGson.fromJson(unit.getMetaJson(), Map.class);
				mergeObjMap(m, paramsAfterCalc);
				unit.setMetaJson(ahuGson.toJson(m));
				UnitUtil.genSerialAndProduct(unit);
				unit.setCreateInfo(username);

				for (Part p : pList) {
					String partkey = p.getSectionKey();
					Short position = p.getPosition();
					PartParam parap = getPartParam(partkey, position, partParamsAfteralc);
					if (EmptyUtil.isEmpty(parap)) {
						logger.warn("Part Param not Found After Calculate");
						continue;
					}
					Map<String, Object> mp = gson.fromJson(p.getMetaJson(), Map.class);
					Map<String, Object> paramsAfterCalcP = parap.getParams();
					mergeObjMap(mp, paramsAfterCalcP);

					if (mp.containsKey(SystemCalculateConstants.META_SECTION_COMPLETED)) {
						String value = BaseDataUtil
								.constraintString(mp.get(SystemCalculateConstants.META_SECTION_COMPLETED));
						if (RestConstant.SYS_ASSERT_FALSE.equals(value)) {// 如果当前机组中有任意段计算异常当前机组状态为选型中
							ahuStatus = false;
							mp.put(SystemCalculateConstants.META_SECTION_COMPLETED, false);
						} else {
							mp.put(SystemCalculateConstants.META_SECTION_COMPLETED, true);
						}
					} else {
						mp.put(SystemCalculateConstants.META_SECTION_COMPLETED, true);
					}
					
					mp.put(SystemCalculateConstants.META_SECTION_CONFIRMED_USER, SystemCalculateConstants.BATCH_CONFIRMED);

					
					p.setMetaJson(gson.toJson(mp));
					p.setRecordStatus(AhuStatusEnum.SELECT_COMPLETE.getId());
					toUpdatePartList.add(p);
				}
				result.get(RestConstant.SYS_MSG_RESPONSE_SUCCESS).add(unit.getDrawingNo());
				// 机组选型是否完成，根据当前机组所有段是否全部配置完成决定

				if(ahuStatus) {
					break;
				}else {
					if(tryCalnum==1) {
						break;
					}
				}
			}
			if (ahuStatus) {
				unit.setRecordStatus(AhuStatusEnum.SELECT_COMPLETE.getId());
				try {
					Partition partition = AhuPartitionGenerator.generatePartition(unit.getUnitid(), unit, pList);
					this.partitionService.savePartition(partition, username);
					calculatorTool.calculateAHUWeight(unit);
					calculatorTool.calculateAHUPrice(unit, partition, pList, language);
				} catch (Exception e) {
					logger.error("Exception in generate new partition:" + e.getMessage(),e);
				}
			} else {
				unit.setRecordStatus(AhuStatusEnum.SELECTING.getId());
				// clear price and weight
				unit.setPrice(0d);
				unit.setWeight(0d);
			}

			if(EmptyUtil.isNotEmpty(unit.getWeight()))
				unit.setWeight(getNoDotDouble(unit.getWeight()));
			if(EmptyUtil.isNotEmpty(unit.getPrice()))
				unit.setPrice(getNoDotDouble(unit.getPrice()));

			toUpdateAhuList.add(unit);
		}
		ahuService.addOrUpdateAhus(toUpdateAhuList);
		sectionService.updateSections(toUpdatePartList);
		ahuStatusTool.syncStatus(projectid, username);
		CalContextUtil.getInstance().refreshQueueIncrementally(projectid,
				AHUContext.getIntlString(BATCH_CONFIG_EXECUTE_COMPLETE));
		CalContextUtil.getInstance().refreshQueueIncrementally(projectid, CalContextUtil.OVER_STR);
		CalContextUtil.getInstance().setCalculatorExist(false);
	}
	public static Double getNoDotDouble(Double d){
		return Double.parseDouble(String.valueOf(NumberUtil.convertStringToDoubleInt(String.valueOf(d))));
	}
	private PartParam getPartParam(String key, Short position, List<PartParam> paraList) {
		for (PartParam para : paraList) {
			if (key.equals(para.getKey()) && position.shortValue() == para.getPosition().shortValue()) {
				return para;
			}
		}
		return null;
	}

	private static Map<String, Object> mergeObjMap(Map<String, Object> sourceMap, Map<String, Object> newDataMap) {
		if (EmptyUtil.isEmpty(sourceMap)) {
			sourceMap = new HashMap<>();
		}
		for (Entry<String, Object> e : newDataMap.entrySet()) {
			try {
				if(JSONArray.class==e.getValue().getClass()){//防止数组转string 导致前端解析失败。
					sourceMap.put(e.getKey(), e.getValue());
				}else{
					sourceMap.put(e.getKey(), Excel4ModelInAndOutUtils.getStringValue(e.getValue()));
				}
			} catch (Exception e1) {
				logger.error(e1.getMessage());
				continue;
			}
		}
		return sourceMap;
	}

	@SuppressWarnings("unchecked")
	private void updateAhuSerial(Unit unit, AhuSizeDetail dt, Map<String, Object> map, double dAirVolume, double dEairVolume, CalcTypeEnum calcTypeEnum,String userConfigString) {
		if(CalcTypeEnum.PERFORMANCE_KEEP.getId().equals(calcTypeEnum.getId()) && !EmptyUtil.isEmpty(unit.getSeries())){//如果选择保持性能
			logger.info("Performance Keep Unit Id：" + unit.getUnitid());
		}else{

			Gson gson = new Gson();
			if (EmptyUtil.isEmpty(dt)) {
				logger.warn("机组遍历时，未获取到最佳选型，Unitid：" + unit.getUnitid());
				map.put(RestConstant.METAHU_PRODUCT, RestConstant.SYS_BLANK);
				map.put(RestConstant.METAHU_HEIGHT, RestConstant.SYS_BLANK);
				map.put(RestConstant.METAHU_WIDTH, RestConstant.SYS_BLANK);
				map.put(RestConstant.METAHU_ASSIS_HEIGHT, RestConstant.SYS_BLANK);
				map.put(RestConstant.METAHU_SERIAL, RestConstant.SYS_BLANK);
				map.put(RestConstant.METAHU_SVELOCITY, RestConstant.SYS_BLANK);
				map.put(RestConstant.METAHU_EVELOCITY, RestConstant.SYS_BLANK);
				unit.setSeries(RestConstant.SYS_BLANK);
				unit.setProduct(RestConstant.SYS_BLANK);
				unit.setMetaJson(gson.toJson(map));
			} else {
				String serial = dt.getAhu();
				String product = serial.substring(0, serial.length() - 4);
				double area = dt.getArea();
				double velocity = NumberUtil.scale(dAirVolume / 3600 / area, 2);
				double eVelocity = NumberUtil.scale(dEairVolume / 3600 / area, 2);
				map.put(RestConstant.METAHU_PRODUCT, product);
				map.put(RestConstant.METAHU_HEIGHT, String.valueOf(dt.getHeight()));
				map.put(RestConstant.METAHU_WIDTH, String.valueOf(dt.getWidth()));
				map.put(RestConstant.METAHU_ASSIS_HEIGHT, String.valueOf(SystemCountUtil.getUnitHeight(serial)));
				map.put(RestConstant.METAHU_SERIAL, serial);
				map.put(RestConstant.METAHU_SVELOCITY, String.valueOf(velocity));
				map.put(RestConstant.METAHU_EVELOCITY, String.valueOf(eVelocity));
				unit.setSeries(serial);
				unit.setProduct(product);
				unit.setMetaJson(gson.toJson(map));
			}
		}
	}

	private static List<AhuSizeDetail> getBestOne(double airvolume,CalcTypeEnum calcTypeEnum,String userConfigString,Unit unit) {
		List<AhuSizeDetail> list = new ArrayList<AhuSizeDetail>();
		list.addAll(AhuMetadata.findAll(AhuSizeDetail.class));
		if (EmptyUtil.isEmpty(list)) {
			return null;
		}
		Collections.sort(list, new Comparator<AhuSizeDetail>() {
			public int compare(AhuSizeDetail o1, AhuSizeDetail o2) {
				double v1 = airvolume / (o1.getCoilFaceArea() * 3600);
				double v2 = airvolume / (o2.getCoilFaceArea() * 3600);
				if (v1 > v2) {
					return -1;
				} else if (v1 < v2) {
					return 1;
				} else {
					return 0;
				}
			}
		});

		List<AhuSizeDetail> OPTIMUMList = new LinkedList<AhuSizeDetail>();// 性能最优

		List<AhuSizeDetail> ECONOMICALList = new LinkedList<AhuSizeDetail>();// 最经济

		UserConfigParam userConfigParam = UserConfigUtil.getUserConfig(userConfigString);

		double minVelocity=TemplateUtil.DEFAULT_VELOCITY;
		double maxVelocity=TemplateUtil.DEFAULT_ECONOMY;
		if(null!=userConfigParam) {
			Double calVelocity = userConfigParam.getMaximumCoilFaceVelocity();
			if(!EmptyUtil.isEmpty(calVelocity)){
				if (calVelocity < minVelocity) {
					minVelocity = calVelocity;
					maxVelocity = calVelocity;
				} else if (calVelocity >= minVelocity && calVelocity <= maxVelocity) {
					minVelocity = TemplateUtil.DEFAULT_VELOCITY;
					maxVelocity = calVelocity;
				} else {
					minVelocity = TemplateUtil.DEFAULT_VELOCITY;
					maxVelocity = TemplateUtil.DEFAULT_ECONOMY;
				}
			}
		}

		for (AhuSizeDetail dt : list) {
			if(dt.getAhu().contains(unit.getProduct())){
				double v = NumberUtil.scale(airvolume / 3600 / dt.getCoilFaceArea(), 2);
				if (v <= minVelocity) {
					OPTIMUMList.add(dt);
				}
				if (v <= maxVelocity) {
					ECONOMICALList.add(dt);
				}
			}
		}

		if(EmptyUtil.isEmpty(ECONOMICALList)){
			return null;
		}
		if(OPTIMUMList.size()==0){
			OPTIMUMList=ECONOMICALList;
		}

		if (calcTypeEnum.getValue().equals(CalcTypeEnum.OPTIMUM.getValue())) {
			return OPTIMUMList;
		}
		return ECONOMICALList;
	}
}
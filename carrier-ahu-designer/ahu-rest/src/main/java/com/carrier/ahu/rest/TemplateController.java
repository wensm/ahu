package com.carrier.ahu.rest;

import com.carrier.ahu.common.entity.AhuTemplate;
import com.carrier.ahu.section.meta.TemplateUtil;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(description = "预定义模板相关接口")
@RestController
@Deprecated
public class TemplateController extends AbstractController {
	@RequestMapping(value = "/templates", method = RequestMethod.GET)
	@ResponseBody
	public List<AhuTemplate> getAhus() {
		return TemplateUtil.getPredefinedTemplate();
	}

}

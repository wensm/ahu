package com.carrier.ahu.factory;

import java.util.HashMap;
import java.util.Map;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.RestConstant;

public class MaterialPropKeyConstances {

	public static String[] ahuProps = { RestConstant.METAHU_PRODUCT, RestConstant.METAHU_SAIRVOLUME, RestConstant.METAHU_SEXTERNALSTATIC,
			RestConstant.METAHU_EAIRVOLUME, RestConstant.METAHU_EEXTERNALSTATIC };
	public static String[] mixProps = { RestConstant.METASEXON_MIX_SINDRYBULBT, RestConstant.METASEXON_MIX_SINWETBULBT,
			RestConstant.METASEXON_MIX_WINDRYBULBT, RestConstant.METASEXON_MIX_WINWETBULBT, RestConstant.METASEXON_MIX_NARATIO };
	public static String[] filterProps = { RestConstant.METASEXON_FILTER_FITETF, RestConstant.METASEXON_FILTER_FILTEROPTIONS,
			RestConstant.METASEXON_FILTER_FILTEREFFICIENCY };
	public static String[] combineFilterProps = { RestConstant.METASEXON_COMBINEDFILTER_FILTERF,
			RestConstant.METASEXON_COMBINEDFILTER_LMATERIAL, RestConstant.METASEXON_COMBINEDFILTER_LMATERIALE,
			RestConstant.METASEXON_COMBINEDFILTER_LMATERIALE, RestConstant.METASEXON_COMBINEDFILTER_RMATERIAL,
			RestConstant.METASEXON_COMBINEDFILTER_RMATERIALE };
	public static String[] coolingCoilProps = { RestConstant.METASEXON_COOLINGCOIL_COILTYPE, RestConstant.METASEXON_COOLINGCOIL_FINTYPE,
			RestConstant.METASEXON_COOLINGCOIL_AIRVOLUME, RestConstant.METASEXON_COOLINGCOIL_ENTERINGAIRDB,
			RestConstant.METASEXON_COOLINGCOIL_ENTERINGAIRWB, RestConstant.METASEXON_COOLINGCOIL_TOTALCAPACITY,
			RestConstant.METASEXON_COOLINGCOIL_ENTERINGFLUIDTEMPERATURE, RestConstant.METASEXON_COOLINGCOIL_MAXWPD,
			RestConstant.METASEXON_COOLINGCOIL_TEMPRETURECHANGE };
	public static String[] fanProps = { RestConstant.METASEXON_FAN_OUTLET, RestConstant.METASEXON_FAN_SUPPLIER,
			RestConstant.METASEXON_FAN_TYPE, RestConstant.METASEXON_FAN_EXTRASTATIC };

	public static Map<String, String[]> getPropsMap() {
		Map<String, String[]> propsMap = new HashMap<String, String[]>();
		propsMap.put(SectionTypeEnum.TYPE_AHU.getId(), ahuProps);
		propsMap.put(SectionTypeEnum.TYPE_MIX.getId(), mixProps);
		propsMap.put(SectionTypeEnum.TYPE_SINGLE.getId(), filterProps);
		propsMap.put(SectionTypeEnum.TYPE_COMPOSITE.getId(), combineFilterProps);
		propsMap.put(SectionTypeEnum.TYPE_COLD.getId(), coolingCoilProps);
		propsMap.put(SectionTypeEnum.TYPE_FAN.getId(), fanProps);
		return propsMap;
	}
}

package com.carrier.ahu.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.carrier.ahu.constant.RestConstant;

/**
 * 页面展示控制层
 */
@Controller
public class IndexController extends AbstractController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return RestConstant.SYS_MSG_RESPONSE_INDEX_HTML;
    }

//    @RequestMapping(value = "/ahu.html", method = RequestMethod.GET)
//    public String page3() {
//        return "ahu.html";
//    }

}

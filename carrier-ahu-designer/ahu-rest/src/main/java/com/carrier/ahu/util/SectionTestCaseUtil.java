package com.carrier.ahu.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.*;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.util.NumberUtil;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.vo.SectionVO;
import com.carrier.ahu.vo.SysConstants;

import static com.carrier.ahu.constant.CommonConstant.OLD_VALUE_FLAG;

/**
 * 
 * @ClassName: SectionTestCaseUtil 
 * @Description: (写入计算的最新结果到excel，方便测试人员判断计算是否正确) 
 * @author dongxiangxiang <dong.xiangxiang@carries.utc.com>   
 * @date 2018年3月23日 下午3:40:17 
 *
 */
public class SectionTestCaseUtil {
	protected static Logger logger = LoggerFactory.getLogger(SectionTestCaseUtil.class);
	
	/**
	 * 写入测试case对应sheet值
	 * @param unit
	 * @param sectionVOs
	 * @param language
	 * @param unitType
	 * @param project
	 * @return
	 * @throws IOException
	 */
	public static boolean doWrite(Unit unit, List<SectionVO> sectionVOs, LanguageEnum language, UnitSystemEnum unitType, Project project) throws IOException {
		File tempdesc = new File(
				RestConstant.TEST_CASE_FOLDER + unit.getUnitid() + RestConstant.SYS_EXCEL2003_EXTENSION);
		
		if (!tempdesc.exists()) {
			tempdesc = new File(
					RestConstant.TEST_CASE_FOLDER + unit.getUnitid() + RestConstant.SYS_EXCEL2007_EXTENSION);
		}
		
		if (tempdesc.exists()) {
			//获取所有段信息
			Map<String, Map<String, String>> allMetaMap = getAllUnitMetaJsonData(unit, sectionVOs);
			//获取所有testCaseKey 以及老系统计算结果
			Map<String, List<String>> allTestCaseKeyAndOldValues = getAllTestCaseKeyAndOldValue(unit, sectionVOs);
			//组装所有testCaseValue
			Map<String, List<String>> allTestCaseValue = getAllTestCaseValue(allTestCaseKeyAndOldValues, allMetaMap,language,unitType,project,unit);
			
			
			InputStream fileInputStream = new FileInputStream(tempdesc);
			Workbook wb = null;
			/* 判断文件的类型，是2003还是2007 */
			boolean isExcel2003 = true;
			if (ExcelUtils.isExcel2007(tempdesc.getPath())) {
				isExcel2003 = false;
			}
			try {
				if (isExcel2003) {
					wb = ExcelUtils.openExcelByPOIFSFileSystem(fileInputStream);
				} else {
					wb = ExcelUtils.openExcelByFactory(fileInputStream);
				}
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
				return false;
			} catch (InvalidFormatException e) {
				e.printStackTrace();
				return false;
			} finally {
				if (!EmptyUtil.isEmpty(fileInputStream)) {
					fileInputStream.close();
				}
			}
			
			
			CellStyle wrapStyle = getWrapStyle(wb,RestConstant.SYS_FONT_ARIAL,9,false,false);// 设置内容列字体和cell样式

			//处理ahu信息
			Sheet ahuSheet = wb.getSheetAt(0);
			List<String> unitValue = allTestCaseValue.get(UnitConverter.UNIT);
			List<String> unitOldValue = allTestCaseKeyAndOldValues.get(UnitConverter.UNIT+OLD_VALUE_FLAG);
			for (int r = 0; r < unitValue.size(); r++) {
				String newValue = unitValue.get(r);//新系统计算结果
				String oldValue = unitOldValue.get(r);//老系统计算结果

				Row row = ahuSheet.getRow(r+1);
				Cell cell = row.createCell(RestConstant.CALC_COLUMN_NUM);
				cell.setCellStyle(wrapStyle);
				cell.setCellValue(unitValue.get(r));

				if(EmptyUtil.isNotEmpty(oldValue) && !"null".equals(oldValue)) {

					if(!compareNumber(oldValue,newValue)) {
						Cell cellCompare = row.createCell(RestConstant.CALC_OLD_NEW_COLUMN_NUM);
						cellCompare.setCellStyle(wrapStyle);
						cellCompare.setCellValue("FALSE");
					}else{
						Cell cellCompare = row.createCell(RestConstant.CALC_OLD_NEW_COLUMN_NUM);
						cellCompare.setCellStyle(wrapStyle);
						cellCompare.setCellValue("");
					}
				}
			}
			
			
			//处理所有段信息
			for (int p=0;p<sectionVOs.size();p++) {
				SectionVO sectionVO = sectionVOs.get(p);
				
				Part partP = new Part();
				partP.setSectionKey(sectionVO .getKey());
				partP.setPosition(sectionVO.getPosition());
				List<String> newPartValue = allTestCaseValue.get(UnitConverter.genUnitKey(unit, partP));//新系统段计算结果集合
				List<String> oldPartValue = allTestCaseKeyAndOldValues.get(UnitConverter.genUnitKey(unit, partP)+OLD_VALUE_FLAG);//老系统段计算结果集合

				Sheet sheet = wb.getSheetAt(p+1);
				
				for (int r = 0; r < newPartValue.size(); r++) {
					String oldValue = oldPartValue.get(r);
					String newValue = newPartValue.get(r);

					Row row = sheet.getRow(r+1);
					Cell cell = row.createCell(RestConstant.CALC_COLUMN_NUM);
					if(EmptyUtil.isNotEmpty(newValue)){
						logger.error("noexist test case excel!");
					}
					cell.setCellStyle(wrapStyle);
					cell.setCellValue(newValue);

					if(EmptyUtil.isNotEmpty(oldValue) && !"null".equals(oldValue)) {

						if(!compareNumber(oldValue,newValue)) {
							Cell cellCompare = row.createCell(RestConstant.CALC_OLD_NEW_COLUMN_NUM);
							cellCompare.setCellStyle(wrapStyle);
							cellCompare.setCellValue("FALSE");
						}else{
							Cell cellCompare = row.createCell(RestConstant.CALC_OLD_NEW_COLUMN_NUM);
							cellCompare.setCellStyle(wrapStyle);
							cellCompare.setCellValue("");
						}
					}
				}
					
			}

			//写入定制列对比结果
			File txtForCustom = new File(RestConstant.TEST_CASE_FOLDER + unit.getUnitid() + RestConstant.SYS_TXT_EXTENSION);
			if (txtForCustom.exists()) {
				FileReader fileReader = new FileReader(txtForCustom);
				String result = fileReader.readString();
				if(EmptyUtil.isNotEmpty(result)){
					String[] customKeys = result.split(",");

					//生成第一行内容
					List<String> rowFirstContent = new ArrayList<>();
					rowFirstContent.add("段");
					for (String customKey : customKeys) {
						rowFirstContent.add("old-"+customKey);
						rowFirstContent.add("new-"+customKey);
					}

					//生成所有段内容
					List<List<String>> rowSectionContents = new ArrayList<>();
					for (int p=0;p<sectionVOs.size();p++) {
						List<String> rowSectionContent = new ArrayList<>();
						SectionVO sectionVO = sectionVOs.get(p);
						rowSectionContent.add(sectionVO.getKey());

						Part partP = new Part();
						partP.setSectionKey(sectionVO .getKey());
						partP.setPosition(sectionVO.getPosition());

						List<String> partKey = allTestCaseKeyAndOldValues.get(UnitConverter.genUnitKey(unit, partP));//所有计算key
						List<String> oldPartValue = allTestCaseKeyAndOldValues.get(UnitConverter.genUnitKey(unit, partP)+OLD_VALUE_FLAG);//老系统段计算结果集合
						List<String> newPartValue = allTestCaseValue.get(UnitConverter.genUnitKey(unit, partP));//新系统段计算结果集合

						for (String customKey : customKeys) {
							boolean hasMatch = false;
							for (int r = 0; r < partKey.size(); r++) {
								String key = partKey.get(r);
								if(verifyKey(customKey,sectionVO.getKey()).equalsIgnoreCase(key)){
									rowSectionContent.add(oldPartValue.get(r));
									rowSectionContent.add(newPartValue.get(r));
									hasMatch = true;
								}
							}

							if(!hasMatch){
								//没有值的用--占位
								rowSectionContent.add("--");
								rowSectionContent.add("--");
							}
						}

						rowSectionContents.add(rowSectionContent);
					}

					Sheet sheetCustom = wb.getSheet("customKeysCompare");
					if(EmptyUtil.isEmpty(sheetCustom)) {
						sheetCustom = wb.createSheet("customKeysCompare");
					}else{
						//清空所有内容
						for (int i = 0; i < sectionVOs.size(); i++) {
							sheetCustom.removeRow(sheetCustom.getRow(i));
						}
					}

					//写入第一行
					Row rowFirst = sheetCustom.createRow(0);
					for (int i = 0; i < rowFirstContent.size(); i++) {
						Cell cell = rowFirst.createCell(i);
						cell.setCellStyle(wrapStyle);
						cell.setCellValue(rowFirstContent.get(i));
					}
					//写入段所有内容
					for (int i = 0; i < rowSectionContents.size(); i++) {
						Row tempRow = sheetCustom.createRow(i+1);
						List<String> contents = rowSectionContents.get(i);
						for (int i1 = 0; i1 < contents.size(); i1++) {
							Cell cell = tempRow.createCell(i1);
							cell.setCellStyle(wrapStyle);
							cell.setCellValue(contents.get(i1));
						}
					}
				}
			}



			FileOutputStream os = new FileOutputStream(tempdesc);
			wb.write(os);
			os.close();
			wb.close();
			fileInputStream.close();


			logger.error("allTestCaseKeyAndOldValue >>>: "+ JSON.toJSONString(allTestCaseKeyAndOldValues));
			logger.error("allTestCaseValue >>>: "+JSON.toJSONString(allTestCaseValue));

			return true;
		}else{
			logger.error("noexist test case excel!");
			return false;
		}
	}

	/**
	 * 百分之五内的差距允许比较结果通过
	 * @param oldValue
	 * @param newValue
	 * @return true:相等，false:不相等
	 */
	public static boolean compareNumber(String oldValue , String newValue){
		try {
			oldValue = oldValue.trim();
			newValue = newValue.trim();

			double range = 0.05;
			if(NumberUtil.isNumber(newValue) && NumberUtil.isNumber(oldValue)){
				Double oldVal = new Double(oldValue);
				Double newVal = new Double(newValue);

				if(Math.abs(new Double(oldVal-newVal))/oldVal<=range){
					return true;
				}
			}
		} catch (NumberFormatException e) {

		}

		if(oldValue.equals(newValue)){
			return true;
		}else {
			return false;
		}
	}

	/**
	 * 获取字体
	 * @param wb
	 * @return
	 */
	private static CellStyle getWrapStyle(Workbook wb,String fontName,int fontSize,boolean isBold,boolean hasBorder) {
		// 创建字体对象
		 Font ztFont = wb.createFont();  
		ztFont.setFontName(fontName);
		ztFont.setFontHeightInPoints((short)fontSize);    // 将字体大小设置为?px     
		ztFont.setBold(isBold); //字体加粗 
		// 创建单元格样式对象     
		 CellStyle wrapStyle = wb.createCellStyle();     
		wrapStyle.setWrapText(true);
		wrapStyle.setAlignment(HorizontalAlignment.CENTER);   
		wrapStyle.setVerticalAlignment(VerticalAlignment.CENTER);   
		
		if(hasBorder){
			wrapStyle.setBorderBottom(BorderStyle.THIN);     
			wrapStyle.setBorderTop(BorderStyle.THIN);     
			wrapStyle.setBorderLeft(BorderStyle.THIN);     
			wrapStyle.setBorderRight(BorderStyle.THIN);    
		}
		wrapStyle.setFont(ztFont);
		return wrapStyle;
	}

	/**
	 * 加载ExcelKey 和 老系统计算结果
	 * @param unit
	 * @param sectionVOs
	 * @return
	 * @throws FileNotFoundException
	 */
	private static Map<String, List<String>> getAllTestCaseKeyAndOldValue(
			Unit unit, List<SectionVO> sectionVOs) throws FileNotFoundException {
		Map<String, List<String>> allKeysAndOldValues = new HashMap<String, List<String>>();
		
		String testCaseFile = RestConstant.TEST_CASE_FOLDER + unit.getUnitid() + RestConstant.SYS_EXCEL2003_EXTENSION;



		try {
			if (!new File(testCaseFile).exists()) {
				testCaseFile = RestConstant.TEST_CASE_FOLDER + unit.getUnitid() + RestConstant.SYS_EXCEL2007_EXTENSION;
			}

			List<String> keys = new ArrayList<String>();
			List<String> oldValues = new ArrayList<String>();
			
			List<List<String>> list = Collections.emptyList();
			InputStream is = new FileInputStream(new File(testCaseFile));
			list = ExcelUtils.read(is, ExcelUtils.isExcel2003(testCaseFile), 0, 1);
			for (List<String> stringList : list) {
					if(EmptyUtil.isEmpty(stringList.get(RestConstant.KEY_COLUMN_NUM)))
						continue;

					keys.add(stringList.get(RestConstant.KEY_COLUMN_NUM).toLowerCase());//例如：meta.ahu.serial
					oldValues.add(stringList.get(RestConstant.OLD_CALC_COLUMN_NUM));//例如：meta.ahu.serial
			}
			allKeysAndOldValues.put(UnitConverter.UNIT, keys);

			allKeysAndOldValues.put(UnitConverter.UNIT+OLD_VALUE_FLAG, oldValues);

			for (int i =0;i<sectionVOs.size();i++) {
				keys = new ArrayList<String>();
				oldValues = new ArrayList<String>();

				list = Collections.emptyList();
				InputStream isP = new FileInputStream(new File(testCaseFile));
				list = ExcelUtils.read(isP, ExcelUtils.isExcel2003(testCaseFile), i+1, 1);
				for (List<String> stringList : list) {
					if(EmptyUtil.isEmpty(stringList.get(RestConstant.KEY_COLUMN_NUM)))
						continue;

					keys.add(verifyKey(stringList.get(RestConstant.KEY_COLUMN_NUM),sectionVOs.get(i).getKey()).toLowerCase());//例如：meta.section.coolingCoil.Wconcentration
					oldValues.add(stringList.get(RestConstant.OLD_CALC_COLUMN_NUM));
				}
				Part partP = new Part();
				partP.setSectionKey(sectionVOs.get(i).getKey());
				partP.setPosition(sectionVOs.get(i).getPosition());

				allKeysAndOldValues.put(UnitConverter.genUnitKey(unit, partP), keys);
				allKeysAndOldValues.put(UnitConverter.genUnitKey(unit, partP)+OLD_VALUE_FLAG, oldValues);
			}
			
		} catch (IOException e) {
			logger.error("Failed to load test case excel: " + testCaseFile, e);
			return null;
		} catch (InvalidFormatException e) {
			logger.error("Failed to load test case excel: " + testCaseFile, e);
			return null;
		}
		return allKeysAndOldValues;
	}
	/**
	 * 生成excel里面Q列所有meta值集合
	 * @param allTestCaseKey
	 * @param allMetaMap
	 * @param unitType 
	 * @param language 
	 * @param project 
	 * @param unit 
	 * @return
	 * @throws IOException 
	 */
	private static Map<String, List<String>> getAllTestCaseValue( 
			Map<String, List<String>> allTestCaseKey, 
			Map<String, Map<String, String>> allMetaMap, 
			LanguageEnum language, UnitSystemEnum unitType, 
			Project project, Unit unit) throws IOException {
		Map<String, List<String>> allValue = new HashMap<String, List<String>>();
		
		Iterator<String> keyIterator = allTestCaseKey.keySet().iterator();
		while(keyIterator.hasNext()) {//遍历key
			String caseMapKey = keyIterator.next();
			if(caseMapKey.contains(OLD_VALUE_FLAG)){
				continue;
			}
			
			List<String> values = new ArrayList<String>();//值集合
			List<String> keys = allTestCaseKey.get(caseMapKey);//获取key 集合
			Map<String, String> metaValueMap = allMetaMap.get(caseMapKey);//获取meta 值集合
			
			for (String key : keys) {
				String value = String.valueOf(metaValueMap.get(key));
				value = ValueFormatUtil.transMetaValue(key, value, project, unit, language, unitType);//重构value
				if(EmptyUtil.isEmpty(value) || "null".equals(value)){
					logger.error(">>>null key:"+key);
				}
				values.add(value);//值集合,添加值
			}
			
			allValue.put(caseMapKey, values);
		}
			
		return allValue;
	}
	/**
	 * 重构 metakey
	 * @param key
	 * @param metaId
	 * @return
	 */
	private static String verifyKey(String key, String metaId) {
		if (key.indexOf(RestConstant.SYS_PUNCTUATION_DOT) == -1) {
			key = MetaCodeGen.calculateAttributePrefix(metaId) + RestConstant.SYS_PUNCTUATION_DOT + key;
		}
		return key.trim();
	}

	/**
	 * 获得AHU以及对应的段的所有metadata
	 * @param unit
	 * @param sectionVOs
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static Map<String, Map<String, String>> getAllUnitMetaJsonData(Unit unit, List<SectionVO> sectionVOs) {
		Map<String, String> unitMap = new HashMap<>();
		Map<String, Map<String, String>> allMetaMap = new HashMap<String, Map<String, String>>();
		try {
			String ahuMetaJson = unit.getMetaJson();
			unitMap.putAll(JSON.parseObject(ahuMetaJson, HashMap.class));
			allMetaMap.put(UnitConverter.UNIT, unitMap);
		} catch (Exception e) {
			logger.warn(MessageFormat.format("转化AHU的metaJson时异常", new Object[] {}), e);
		}
		for (SectionVO sectionVO : sectionVOs) {
			if (sectionVO.getUnitid().startsWith(RestConstant.SYS_ALPHABET_A_UP)) {
				try {
					Map<String, String> sectionMap = new HashMap<>();
					String sectionMetaJson = sectionVO.getMetaJson();
					sectionMap.putAll(unitMap);//优先加载机组属性
					sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));//段的属性覆盖机组属性 例如：风向
					sectionMap.put( sectionVO.getKey().concat(RestConstant.SYS_PUNCTUATION_DOT).concat(SysConstants.PART_PREVIOUS), String.valueOf(sectionVO.getKey()));
					Part partP = new Part();
					partP.setPosition(sectionVO.getPosition());
					partP.setSectionKey(sectionVO.getKey());
					allMetaMap.put(UnitConverter.genUnitKey(unit, partP), transformUpperCase(sectionMap));
				} catch (Exception e) {
					logger.warn(MessageFormat.format("转化段metaJson时异常, unitid:{0}-->partKey:{1}",
							new Object[] { unit.getUnitid(), sectionVO.getKey() }), e);
					continue;
				}
			} else {
				logger.warn(MessageFormat.format("转化段metaJson时，unitid不匹配>>unitid:{0}--partUnitid:{1}",
						new Object[] { unit.getUnitid(), sectionVO.getUnitid() }));
				continue;
			}
		}
		return allMetaMap;
	}

	/**
	 * map key 转换为小写
	 * @param orgMap
	 * @return
	 */
	public static Map<String, String> transformUpperCase(Map<String, String> orgMap) {
		Map<String, String> resultMap = new HashMap<>();

		if (orgMap == null || orgMap.isEmpty()) {
			return resultMap;
		}

		Set<String> keySet = orgMap.keySet();
		for (String key : keySet) {
			String newKey = key.toLowerCase();
			resultMap.put(newKey, orgMap.get(key));
		}

		return resultMap;
	}
}

package com.carrier.ahu.vo;

public class AhuGroupVO {
	String groupId;
	String groupName;
	String projectId;
	String groupCode;
	String ahuIds;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getAhuIds() {
		return ahuIds;
	}

	public void setAhuIds(String ahuIds) {
		this.ahuIds = ahuIds;
	}

}

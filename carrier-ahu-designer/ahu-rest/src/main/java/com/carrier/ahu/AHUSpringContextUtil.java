package com.carrier.ahu;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 获取系统环境变量
 */
@Component
public class AHUSpringContextUtil implements ApplicationContextAware {
    private static ApplicationContext context = null;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        this.context = applicationContext;
    }
    public static String getActiveProfile() {
        return context.getEnvironment().getActiveProfiles()[0];
    }
    public static boolean isDevProfile() {
        String profilesStr = getActiveProfile();
        return profilesStr.toLowerCase().contains("dev");
    }
    public static boolean isTestProfile() {
        String profilesStr = getActiveProfile();
        return profilesStr.toLowerCase().contains("test");
    }
}
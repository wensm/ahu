package com.carrier.ahu.browser;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.util.ExecUtils;

import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserAdapter;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserCommandEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserListener;


public class WebBrowserPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    private JPanel webBrowserPanel;

    private JWebBrowser webBrowser;

    public WebBrowserPanel(String url) {
        super(new BorderLayout());
        webBrowserPanel = new JPanel(new BorderLayout());
        webBrowser = new JWebBrowser();
        if (url != null && !"".equals(url)) {
            webBrowser.navigate(url);
        }
        webBrowser.setButtonBarVisible(true);
        webBrowser.setMenuBarVisible(true);
        webBrowser.setBarsVisible(false);
        webBrowser.setStatusBarVisible(false);
        webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
        webBrowser.addWebBrowserListener(new WebBrowserAdapter() {
            @Override
            public void commandReceived(WebBrowserCommandEvent event) {
                String command = event.getCommand();
                // receive sendNSCommand from JWebBrowser
                if (RestConstant.CMD_CHROME.equals(command)) {
                    startChrome(String.valueOf(event.getParameters()[0]));
                }
            }
        });
        add(webBrowserPanel, BorderLayout.CENTER);
        // webBrowser.executeJavascript("javascrpit:window.location.href='http://www.baidu.com'");
    }

    public void addWebBrowserListener(WebBrowserListener listener) {
        webBrowser.addWebBrowserListener(listener);
    }

    public void startChrome(String url) {
        ExecUtils.executeCmd(RestConstant.CMD_CHROME_LINE + url);
    }

    public void navigate(String url) {
        this.webBrowser.navigate(url);
    }

    public void setContent(String content) {
        this.webBrowser.setHTMLContent(content);
    }

}
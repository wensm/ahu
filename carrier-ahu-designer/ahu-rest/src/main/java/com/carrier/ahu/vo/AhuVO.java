package com.carrier.ahu.vo;

import lombok.Data;

@Data
public class AhuVO {
	private String pid;//项目ID
    private String unitid;//AHU ID
    private String groupId;//组ID
    private String unitNo;//机组编号
    private String drawingNo;//图纸编号
    private String series;//机组型号
    private String product;
    private Short mount;//数量
    private String name;//AHU名称
    private Double weight;//重量
    private Double price;//价格
    private String modifiedno;
    private String paneltype;//类型
    private Double nsprice;
    private Double cost;
    private Double lb;
    private byte[] drawing;
    private String customerName;//客户名称
    private String ahuStr;
    private String  sectionStr;
    private String  allSectionsStr;//所有段json信息
    private String  validateSections;//按照风向排列段位置json信息
    private String  sectionrelations;//按照风向排列段位置json数组（仅包含position 从0开始）
    private String layoutStr;
    private Short position;
    private String isNS;//是否从非标确认按钮点击事件

}

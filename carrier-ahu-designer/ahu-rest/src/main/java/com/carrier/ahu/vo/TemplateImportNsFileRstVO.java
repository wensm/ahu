package com.carrier.ahu.vo;


import lombok.Data;

@Data
public class TemplateImportNsFileRstVO {
	
    private String success;//导入成功的机组编号
    private String existUnitPRB;//已经存在的机组编号
    private String existPartPRB;//已经存在的机组编号
}

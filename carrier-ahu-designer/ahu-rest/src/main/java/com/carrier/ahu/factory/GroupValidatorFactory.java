package com.carrier.ahu.factory;

import com.carrier.ahu.product.GroupValidatorProduct;
import com.carrier.ahu.product.ValidatorProduct;
import com.carrier.ahu.service.AhuService;

/**
 * 组别校验器（工厂模式-工厂）
 * @author WangD1
 *
 */
public class GroupValidatorFactory implements ValidatorFactory {

	@Override
	public ValidatorProduct getValidator(AhuService ahuService) {
		return new GroupValidatorProduct(ahuService);
	}

	@Override
	public ValidatorProduct getValidator() {
		return new GroupValidatorProduct();
	}
	
}
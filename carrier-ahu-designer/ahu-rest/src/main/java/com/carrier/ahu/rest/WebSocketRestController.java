package com.carrier.ahu.rest;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.rest.websocket.MyWebSocket;
import com.carrier.ahu.rest.websocket.Response;
import com.carrier.ahu.service.cal.CalContextUtil;
import com.carrier.ahu.service.cal.impl.DefaultCalContext;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.ApiResult;

@Profile({ "!test" })
@RestController
public class WebSocketRestController {
	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	@RequestMapping(value = "ws/getalljson/{projectid}", method = RequestMethod.GET)
	public ApiResult<DefaultCalContext> getAllJson(@PathVariable("projectid") String projectid) throws Exception {
		DefaultCalContext ctxt = CalContextUtil.getInstance().getDefaultCalContext(projectid);
		return ApiResult.success(ctxt);
	}

	@RequestMapping(value = "ws/getalltext/{projectid}", method = RequestMethod.GET)
	public void getAllText(@PathVariable("projectid") String projectid) throws Exception {
		// sendMessage(CalContextUtil.getInstance().getAllQueueContext(projectid));
		sendMessage(projectid, CalContextUtil.getInstance().getAllQueueContext(projectid));
	}

	@RequestMapping(value = "ws/gettext/{projectid}", method = RequestMethod.GET)
	public void getDynamicText(@PathVariable("projectid") String projectid) throws Exception {
		// sendMessage(CalContextUtil.getInstance().getDynamicQueueContext(projectid));
		sendMessage(projectid, CalContextUtil.getInstance().getDynamicQueueContext(projectid));
		int count = 0;
		while (count < 60 * 30) {// 轮询半小时
			Thread.sleep(1000);
			// sendMessage(CalContextUtil.getInstance().getDynamicQueueContext(projectid));
			sendMessage(projectid, CalContextUtil.getInstance().getDynamicQueueContext(projectid));
			count++;
		}
	}

	public void sendMessage(ConcurrentLinkedQueue<String> queue) throws Exception {
		if (EmptyUtil.isEmpty(queue)) {
			sendMessage(RestConstant.SYS_MSG_RESPONSE_NO_DATA);
		}
		for (String msg : queue) {
			sendMessage(msg);
		}
	}

	public void sendMessage(String message) throws Exception {
		messagingTemplate.convertAndSend("/topic/getResponse", new Response(message));
	}

	public void sendMessage(String projectId, ConcurrentLinkedQueue<String> queue) throws Exception {
		if (EmptyUtil.isEmpty(queue)) {
			sendMessage(projectId, RestConstant.SYS_MSG_RESPONSE_NO_DATA);
		}
		for (String msg : queue) {
			sendMessage(projectId, msg);
		}
	}

	public void sendMessage(String projectId, String message) throws Exception {
		MyWebSocket.sendMessage(projectId, message);
	}
}

package com.carrier.ahu.util;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.vo.AhuVO;
import com.carrier.ahu.vo.AirDirectionVO;
import com.carrier.ahu.vo.SectionRelationVO;
import com.carrier.ahu.vo.SectionVO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.carrier.ahu.constant.CommonConstant.*;

public class BeanTransferUtil {
	public static final short CAL_ALL_SECTION = -1;
	/**
	 * 转换前端传输的VO对象，为计算参数对象，以方便后续计算 本方法作为其它Service方法接受前端参数的入口
	 *
	 * Updated by JL 10/20
	 */
	public static AhuParam getAhuParam(AhuVO ahuVO) {
		String sectionStr = ahuVO.getSectionStr();
		if(EmptyUtil.isNotEmpty(ahuVO.getAllSectionsStr())){
			sectionStr = ahuVO.getAllSectionsStr();
		}

        // PATCH: 系统存在无效预制的选项，需要处理后才能确认
        if (sectionStr.contains("errorValue")) {
            throw new ApiException(ErrorCode.INVALID_SECTION_META_OPTION);
        }

		Gson gson = new Gson();
		List<SectionVO> sectionVOs = gson.fromJson(sectionStr, new TypeToken<List<SectionVO>>() {
		}.getType());
		AhuParam ahuParam = beanCopyFromAhuVoToAhuParam(ahuVO);
		AhuLayout layout = AhuLayoutUtils.parse(ahuVO.getLayoutStr());
		ahuParam.setLayout(layout);

		boolean isV1 = isV1(layout);//立式机组,混合段顶部开门和上层风机不能碰撞

		// 拷贝当前选中的属性到
		List<PartParam> partParams = new ArrayList<PartParam>();
		PartParam partTemp = null;
		for (int i=0;i<sectionVOs.size();i++) {
			
			SectionVO section = sectionVOs.get(i);
			if (ahuVO.getPosition() == CAL_ALL_SECTION
					|| (ahuVO.getPosition() != null && ahuVO.getPosition().equals(section.getPosition()))) {
				partTemp = beanCopyFromSectionVoToPartParam(section);

				//上下关系段添加
				if(EmptyUtil.isNotEmpty(ahuVO.getAllSectionsStr()) && EmptyUtil.isNotEmpty(ahuVO.getValidateSections())) {
					addPreNextPart(partTemp, ahuVO);
				}
			}
		}
		if(EmptyUtil.isNotEmpty(partTemp)){
			setLenMixofFanInVerticalDirection(isV1,partTemp,sectionVOs);
			partParams.add(partTemp);
		}

		ahuParam.setPartParams(partParams);
		ahuParam.setIsNS(ahuVO.getIsNS());
		return ahuParam;
	}

	private static void setLenMixofFanInVerticalDirection(boolean isV1, PartParam partTemp, List<SectionVO> sectionVOs) {
		int theLenMixofFanInVerticalDirection = 0;
		if(isV1 & partTemp.getKey().equals(SectionTypeEnum.TYPE_FAN.getId())){
			boolean isPassMix = false;
			for (int i=0;i<sectionVOs.size();i++) {
				SectionVO section = sectionVOs.get(i);
				Map<String, Object> map = (Map<String, Object>) JSONObject.parseObject(section.getMetaJson());
				if(section.getKey().equals(SectionTypeEnum.TYPE_MIX.getId())){
					String top = String.valueOf(map.get(METASEXON_MIX_RETURNTOP));//顶部出风
					if(SYS_ASSERT_FALSE.equals(top)){
						theLenMixofFanInVerticalDirection = 999;//没有选择顶部出风不用判断风机和混合碰触为。
						break;
					}
					isPassMix = true;
					continue;
				}

				if(isPassMix){
					int sectionL = NumberUtil.convertStringToDoubleInt(""+map.get(METASEXON_PREFIX+section.getKey().replace("ahu.","")+METACOMMON_POSTFIX_SECTIONL));
					theLenMixofFanInVerticalDirection +=sectionL;
				}
			}
		}
		partTemp.setLenMixofFanInVerticalDirection(""+theLenMixofFanInVerticalDirection);
	}

	private static boolean isV1(AhuLayout layout) {
		boolean isv1=false;

		if(EmptyUtil.isNotEmpty(layout) && LayoutStyleEnum.VERTICAL_UNIT_1.style() == layout.getStyle()){
			isv1 = true;
		}
		return isv1;
	}

	/**
	 * 按照风向填写前后端信息
	 *
	 * 定制：新回排的送风第一个段，本无上个段，但是需要设置为新回排段。
	 * 定制逻辑被用在：“混合段/空段/新回排段/出风段”也是指选了检修门的。如果没有选检修门，不能作为检修段用。
	 * @param partCurrent
	 * @param ahuVO
	 */
	private static void addPreNextPart(PartParam partCurrent, AhuVO ahuVO) {
		Gson gson = new Gson();
		List<AirDirectionVO> airDirectionVOList = gson.fromJson(ahuVO.getValidateSections(), new TypeToken<List<AirDirectionVO>>() {}.getType());
		if (!EmptyUtil.isEmpty(airDirectionVOList)) {
			boolean enabled = true;
			String msg = "";
			for (AirDirectionVO airDirectionVO : airDirectionVOList) {
				List<SectionRelationVO> relationVOs = airDirectionVO.getValue();
				//段前后循序校验
				for (int i = 0; i < relationVOs.size(); i++) {
					SectionRelationVO sectionCurrent = relationVOs.get(i);
					if(partCurrent.getPosition() == Integer.parseInt(sectionCurrent.getId())){
						if(i>0) {
							SectionRelationVO sectionPre = relationVOs.get(i - 1);
							partCurrent.setPreParams(getPreNextMetaJson(ahuVO,Integer.parseInt(sectionPre.getId())));
						}

						//新回排机组：送风第一个段的，上个段特殊写死为新回排段
						if(i==0 && sectionCurrent.getDirection().equalsIgnoreCase("S")){
							partCurrent.setPreParams(getCombinedMixingChamberMetaJson(ahuVO));
						}


						if(i<relationVOs.size()-1) {
							SectionRelationVO sectionNext = relationVOs.get(i + 1);
							partCurrent.setNextParams(getPreNextMetaJson(ahuVO,Integer.parseInt(sectionNext.getId())));
						}
						if(i<relationVOs.size()-2) {
							SectionRelationVO sectionNextNext = relationVOs.get(i + 2);
							partCurrent.setNextNextParams(getPreNextMetaJson(ahuVO,Integer.parseInt(sectionNextNext.getId())));
						}

					}
				}
			}
		}
	}
	private static Map<String, Object> getPreNextMetaJson(AhuVO ahuVO,int position){
		String sectionStr = ahuVO.getAllSectionsStr();
		Gson gson = new Gson();
		List<SectionVO> sectionVOs = gson.fromJson(sectionStr, new TypeToken<List<SectionVO>>() {}.getType());
		for (SectionVO sectionVO : sectionVOs) {
			if(sectionVO.getPosition() == position ){
				Map<String, Object> preNextMetaMap = (Map<String, Object>) JSONObject.parseObject(sectionVO.getMetaJson());
				preNextMetaMap.put(RestConstant.SYS_MAP_AHU_KEY,sectionVO.getKey());
				return preNextMetaMap;
			}
		}
		return null;
	}
	private static Map<String, Object> getCombinedMixingChamberMetaJson(AhuVO ahuVO){
		String sectionStr = ahuVO.getAllSectionsStr();
		Gson gson = new Gson();
		List<SectionVO> sectionVOs = gson.fromJson(sectionStr, new TypeToken<List<SectionVO>>() {}.getType());
		for (SectionVO sectionVO : sectionVOs) {
			if(sectionVO.getKey().equals(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId())){
				Map<String, Object> preNextMetaMap = (Map<String, Object>) JSONObject.parseObject(sectionVO.getMetaJson());
				preNextMetaMap.put(RestConstant.SYS_MAP_AHU_KEY,sectionVO.getKey());
				return preNextMetaMap;
			}
		}
		return null;
	}
	private static PartParam beanCopyFromSectionVoToPartParam(SectionVO section) {
		PartParam partParam = new PartParam();
		partParam.setAvdirection(section.getAvdirection());
		partParam.setCost(section.getCost());
		partParam.setGroupi(section.getGroupi());
		partParam.setKey(section.getKey());
		partParam.setOrders(section.getOrders());
		partParam.setPartid(section.getPartid());
		partParam.setPic(section.getPic());
		partParam.setPid(section.getPid());
		partParam.setPosition(section.getPosition());
		partParam.setSetup(section.getSetup());
		partParam.setTypes(
				null == section.getTypes() ? MetaCodeGen.getSectionType(section.getKey()) : section.getTypes());
		partParam.setUnitid(section.getUnitid());
		Map<String, Object> map = (Map<String, Object>) JSONObject.parseObject(section.getMetaJson());
		partParam.setParams(map);
		return partParam;
	}

	private static AhuParam beanCopyFromAhuVoToAhuParam(AhuVO ahuVO) {
		AhuParam ahuParam = new AhuParam();
		ahuParam.setCost(ahuVO.getCost());
		ahuParam.setCustomerName(ahuVO.getCustomerName());
		ahuParam.setDrawing(ahuVO.getDrawing());
		ahuParam.setDrawingNo(ahuVO.getDrawingNo());
		ahuParam.setGroupId(ahuVO.getGroupId());
		ahuParam.setLb(ahuVO.getLb());
		ahuParam.setModifiedno(ahuVO.getModifiedno());
		ahuParam.setMount(ahuVO.getMount());
		ahuParam.setName(ahuVO.getName());
		ahuParam.setNsprice(ahuVO.getNsprice());
		ahuParam.setPaneltype(ahuVO.getPaneltype());
		ahuParam.setPid(ahuVO.getPid());
		ahuParam.setPrice(ahuVO.getPrice());
		ahuParam.setProduct(ahuVO.getProduct());
		ahuParam.setSeries(ahuVO.getSeries());
		ahuParam.setUnitid(ahuVO.getUnitid());
		ahuParam.setUnitNo(ahuVO.getUnitNo());
		ahuParam.setWeight(ahuVO.getWeight());
		if (StringUtils.isNotBlank(ahuVO.getAhuStr())) {
			JSONObject jasonObject = JSONObject.parseObject(ahuVO.getAhuStr());
			Map<String, Object> map = (Map<String, Object>) jasonObject;
			ahuParam.setParams(map);
		}
		return ahuParam;
	}
}

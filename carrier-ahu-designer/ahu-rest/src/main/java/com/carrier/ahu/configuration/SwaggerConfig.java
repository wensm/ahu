package com.carrier.ahu.configuration;

import org.springframework.boot.autoconfigure.h2.H2ConsoleAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * using --spring.profiles.active=dev as Program Arguments to enable swagger, it
 * will be disabled in production environment.
 * 
 * add H2ConsoleAutoConfiguration in Import configuration to enable h2 console.
 * 
 * Created by Braden Zhou on 2018/05/10.
 */
@Configuration
@Import({ H2ConsoleAutoConfiguration.class, })
@Profile("dev")
@EnableSwagger2
public class SwaggerConfig {

}

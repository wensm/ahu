package com.carrier.ahu.rest;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.constant.RestConstant;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.swagger.annotations.Api;

@Api(description = "Internationalization Data Interface")
@RestController
public class I18NController extends AbstractController {

    @RequestMapping(value = "/i18n/{lang}", method = RequestMethod.GET)
    public Map<String, Object> getI18n(@PathVariable("lang") String lang) throws Exception {
        LanguageEnum languageEnum = LanguageEnum.byId(lang);
        if(languageEnum == null) {
            lang = LanguageEnum.Chinese.getId();
        }
        String langFile = String.format("intl/%s.json", lang);
        InputStream is = I18NController.class.getClassLoader().getResourceAsStream(langFile);
        return new Gson().fromJson(new InputStreamReader(is, Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP)),
                new TypeToken<Map<String, Object>>() {
                }.getType());
    }

}

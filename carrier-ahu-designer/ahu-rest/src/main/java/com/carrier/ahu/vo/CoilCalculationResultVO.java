package com.carrier.ahu.vo;

import java.util.Collection;

import lombok.Data;

@Data
public class CoilCalculationResultVO {
	private Collection<Object> resultData;
	private String sectionKey;
	private String season;
	private String userRole;//角色
}

package com.carrier.ahu.vo;

public class MaterialVO {
	
	private String projectId;
	private String groupId;
	private String materialStr;
	
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getMaterialStr() {
		return materialStr;
	}
	public void setMaterialStr(String materialStr) {
		this.materialStr = materialStr;
	}
}

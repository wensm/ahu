package com.carrier.ahu.vo;

import com.carrier.ahu.service.cal.WeightBean;
import lombok.Data;

import java.util.List;

/**
 * Created by liujianfeng on 2017/6/25.
 *
 * Return the weight result of an ahu by the ahu id
 */
@Data
public class WeightResultVO {
	private List<WeightBean> details;
	private double total;
	private String ahuId;
	private String priceSpecVersion;

}

package com.carrier.ahu.product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.report.ExportPart;
import com.carrier.ahu.report.ExportUnit;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;

/**
 * 温度校验器（工厂模式-产品）
 * @author wangd1
 *
 */
public class TemperatureValidatorProduct implements ValidatorProduct {

	@Autowired
	private AhuService ahuService;
	
	public TemperatureValidatorProduct(AhuService ahuService) {
		this.ahuService = ahuService;
	}

	public TemperatureValidatorProduct() {
	
	}
	
	/**
	 * 模板导入时，校验干湿温度。
	 */
	@Override
	public boolean validate(ExportUnit eUnit, Project project, String groupId) {
		boolean result = true;
//		Map<String, String> eUnitMap = JSON.parseObject(eUnit.getMetaJson(), HashMap.class);
		Map<String, String> constantKeys = new HashMap<String, String>();
		constantKeys.put(RestConstant.METASEXON_COOLINGCOIL_SINDRYBULBT, RestConstant.METASEXON_COOLINGCOIL_SINWETBULBT);
		constantKeys.put(RestConstant.METASEXON_HEATINGCOIL_WINDRYBULBT, RestConstant.METASEXON_HEATINGCOIL_WINWETBULBT);
		constantKeys.put(RestConstant.METASEXON_WETFILMHUMIDIFIER_SINDRYBULBT, RestConstant.METASEXON_WETFILMHUMIDIFIER_SINWETBULBT);
		constantKeys.put(RestConstant.METASEXON_SPRAYHUMIDIFIER_SINDRYBULBT, RestConstant.METASEXON_SPRAYHUMIDIFIER_SINWETBULBT);
		constantKeys.put(RestConstant.METASEXON_ELECTRODEHUMIDIFIER_SINDRYBULBT, RestConstant.METASEXON_ELECTRODEHUMIDIFIER_SINWETBULBT);
		
		List<ExportPart> parts = eUnit.getExportParts();
		for (ExportPart ePart : parts) {
			Map<String, String> partMap = JSON.parseObject(ePart.getMetaJson(), HashMap.class);
			for (Entry<String, String> entry : constantKeys.entrySet()) {
				String DryBulbT = BaseDataUtil
						.constraintString(partMap.get(entry.getKey()));//干球温度
				String WetBulbT = BaseDataUtil
						.constraintString(partMap.get(entry.getValue()));//湿球温度
				result = validationTool(DryBulbT, WetBulbT);
				if (!result) {
					break;
				}
			}
			if (!result) {
				break;
			}
		}
		
		
		return result;
	}
	
	private static boolean validationTool (String dry , String wet) {
		boolean rst = true;
		
		//干湿温度不允许只导入二者其一。
		if ((EmptyUtil.isEmpty(dry) && EmptyUtil.isNotEmpty(wet))
				|| (EmptyUtil.isNotEmpty(dry) && EmptyUtil.isEmpty(wet))) {
			rst = false;
			return rst;
		}
		if (EmptyUtil.isNotEmpty(dry) && EmptyUtil.isNotEmpty(wet)) {
			double dryDB = Double.parseDouble(dry);
			double wetDB = Double.parseDouble(wet);
			// 干球温度必须大于湿球温度
			if (dryDB < wetDB) {
				rst = false;
				return rst;
			}
		}
		
		return rst;
	}

	@Override
	public Map<String, Object> validate1(ExportUnit eUnit, Project project, String groupId) {
		Map<String, Object> rstMap = new HashMap<String, Object>();
		String PRB = "";

		boolean result = true;
		Map<String, String> eUnitMap = JSON.parseObject(eUnit.getMetaJson(), HashMap.class);
		Map<String, String> constantKeys = new HashMap<String, String>();
		constantKeys.put(RestConstant.METASEXON_COOLINGCOIL_SINDRYBULBT,
				RestConstant.METASEXON_COOLINGCOIL_SINWETBULBT);
		constantKeys.put(RestConstant.METASEXON_HEATINGCOIL_WINDRYBULBT,
				RestConstant.METASEXON_HEATINGCOIL_WINWETBULBT);
		constantKeys.put(RestConstant.METASEXON_WETFILMHUMIDIFIER_SINDRYBULBT,
				RestConstant.METASEXON_WETFILMHUMIDIFIER_SINWETBULBT);
		constantKeys.put(RestConstant.METASEXON_SPRAYHUMIDIFIER_SINDRYBULBT,
				RestConstant.METASEXON_SPRAYHUMIDIFIER_SINWETBULBT);
		constantKeys.put(RestConstant.METASEXON_ELECTRODEHUMIDIFIER_SINDRYBULBT,
				RestConstant.METASEXON_ELECTRODEHUMIDIFIER_SINWETBULBT);

		for (Entry<String, String> entry : constantKeys.entrySet()) {
			String DryBulbT = BaseDataUtil.constraintString(eUnitMap.get(entry.getKey()));// 干球温度
			String WetBulbT = BaseDataUtil.constraintString(eUnitMap.get(entry.getValue()));// 湿球温度
			result = validationTool(DryBulbT, WetBulbT);
		}

		if (!result) {
			PRB += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
		}
		rstMap.put(RestConstant.SYS_STRING_RESULT, result);
		rstMap.put(RestConstant.SYS_STRING_PRB, PRB);

		return rstMap;
	}

	@Override
	public Map<String, Object> validate2(ExportUnit eUnit, Project project, String groupId) {
		
		return null;
	}


}
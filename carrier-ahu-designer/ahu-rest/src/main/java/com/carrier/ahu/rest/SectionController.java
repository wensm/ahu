package com.carrier.ahu.rest;

import java.util.*;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.RoleEnum;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.AhuGroupBindService;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.GroupService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.unit.KeyGenerator;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.SectionTestCaseUtil;
import com.carrier.ahu.util.UnitUtil;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.swagger.annotations.Api;

import static com.carrier.ahu.common.intl.I18NConstants.NS_SAVE_FAILD;
import static com.carrier.ahu.common.intl.I18NConstants.PRICE_NS_CALCULATION_LIMIT;
import static com.carrier.ahu.constant.CommonConstant.*;

/**
 * 段业务接口 Created by Wen zhengtao on 2017/3/16.
 */
@Api(description = "段(section)相关接口")
@RestController
public class SectionController extends AbstractController {
	@Autowired
	SectionService sectionService;
	@Autowired
	AhuService ahuService;
	@Autowired
	GroupService groupService;
	@Autowired
	private AhuGroupBindService ahuGroupBindService;
	@Autowired
	private AhuStatusTool ahuStatusTool;
	@Autowired
	private PartitionService partitionService;
	@Autowired
	ProjectService projectService;

	@Autowired
	private CalculatorTool calculatorTool;

	private boolean toWrite = false;//（保存计算结果到excel test case）开关
	 
	@RequestMapping(value = "section/list", method = RequestMethod.GET)
	public ApiResult<List<Part>> list(String ahuId) {
		List<Part> result = sectionService.findSectionList(ahuId);
		Collections.sort(result, new Comparator<Part>() {
			public int compare(Part arg0, Part arg1) {
				return arg0.getPosition().compareTo(arg1.getPosition());
			}
		});
		return ApiResult.success(result);
	}

	@RequestMapping(value = "section/{sectionId}", method = RequestMethod.GET)
	public ApiResult<Part> findById(@PathVariable("sectionId") String sectionId) {
		Part section = sectionService.findSectionById(sectionId);
		return ApiResult.success(section);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "section/save", method = RequestMethod.POST)
	public ApiResult<String> save(Part section) {
		String pid = RestConstant.SYS_BLANK;
		String pMetaJson = section.getMetaJson();
		section.setRecordStatus(AhuStatusEnum.SELECTING.getId());
		if (EmptyUtil.isNotEmpty(pMetaJson)) {
			Map<String, Object> conMap = JSON.parseObject(pMetaJson, HashMap.class);
			if (EmptyUtil.isNotEmpty(conMap)) {
				Boolean isComplete = MapValueUtils.getBooleanValue(SystemCalculateConstants.META_SECTION_COMPLETED,
						conMap);
				if (EmptyUtil.isNotEmpty(isComplete)) {
					if (isComplete) {
						section.setRecordStatus(AhuStatusEnum.SELECT_COMPLETE.getId());
					}
				}
			}
		}
		if (StringUtils.isBlank(section.getPartid())) {
			User user = getUser();
			pid = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
			section.setPartid(pid);
			sectionService.addSection(section, getUserName());
		} else {
			pid = sectionService.updateSection(section, getUserName());
		}
		ahuStatusTool.syncStatus(section.getPid(), getUserName());
		return ApiResult.success(pid);
	}

	/**
	 * 批量保存段信息</br>
	 * 项目状态变化</br>
	 * 
	 * @param ahuVO
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "section/bacthsave", method = RequestMethod.POST)
	public ApiResult<String> bacthSave(AhuVO ahuVO) throws Exception {
		Unit unit = ahuService.findAhuById(ahuVO.getUnitid());
		unit.setMetaJson(ahuVO.getAhuStr());
		unit.setLayoutJson(ahuVO.getLayoutStr());
		unit.setSectionrelations(ahuVO.getSectionrelations());
		String sectionStr = ahuVO.getSectionStr();
		Gson gson = new Gson();
		List<SectionVO> sectionVOs = gson.fromJson(sectionStr, new TypeToken<List<SectionVO>>() {
		}.getType());

		//非标页面保存，校验销售角色不允许修改非标ahu价格。
		if("true".equals(ahuVO.getIsNS())){
			if(RoleEnum.Sales.name().equalsIgnoreCase(AHUContext.getUserRole())){
                Map<String, Object> ahuMetaJsonMap = JSON.parseObject(unit.getMetaJson(), HashMap.class);
                double ahuNsPrice = NumberUtils.toDouble(String.valueOf(ahuMetaJsonMap.get(METANS_AHU_ID_PRICE)));
                if(0 != ahuNsPrice){
                    return ApiResult.error(I18NBundle.getString(PRICE_NS_CALCULATION_LIMIT, getLanguage()));
                }

                for (SectionVO sectionVO : sectionVOs) {
                    Map<String, Object> partMetaJsonMap = JSON.parseObject(sectionVO.getMetaJson(), HashMap.class);
                    Iterator<String> it = partMetaJsonMap.keySet().iterator();
                    while (it.hasNext()) {
                        String key = it.next();
                        if ((key.startsWith("ns")||key.startsWith("NS")) && (key.endsWith(".Price")||key.endsWith(".price"))) { // is material property
                            if(EmptyUtil.isNotEmpty(partMetaJsonMap.get(key))){
                                return ApiResult.error(I18NBundle.getString(PRICE_NS_CALCULATION_LIMIT, getLanguage()));
                            }
                        }
                    }
                }
            }else{
                Map<String, Object> ahuMetaJsonMap = JSON.parseObject(ahuVO.getAhuStr(), HashMap.class);
                String deformationSerial = String.valueOf(ahuMetaJsonMap.get(METANS_NS_AHU_DEFORMATIONSERIAL));
                String nsEnable = String.valueOf(ahuMetaJsonMap.get(METANS_NS_AHU_ENABLE));
                String nsDeformation = String.valueOf(ahuMetaJsonMap.get(METANS_NS_AHU_DEFORMATION));
                if("true".equals(nsEnable) && "true".equals(nsDeformation)){
                    if(EmptyUtil.isEmptyOrNull(deformationSerial) || deformationSerial.length() != 4){
                        return ApiResult.error(I18NBundle.getString(NS_SAVE_FAILD, getLanguage()));
                    }
                }
                
            }
			

		}

		UnitUtil.genSerialAndProduct(unit);

		String groupCodeOld = unit.getGroupCode();
		boolean allSectionComplete = false;

		List<Part> parts = new ArrayList<Part>();
		List<Part> oldParts = sectionService.findSectionList(ahuVO.getUnitid());
		List<String> partIds = new ArrayList<String>();
		for (SectionVO sectionVO : sectionVOs) {
			Part part = new Part();
			part.setPartid(sectionVO.getPartid());
			String pMetaJson = sectionVO.getMetaJson();
			part.setRecordStatus(AhuStatusEnum.SELECTING.getId());
			if (EmptyUtil.isNotEmpty(pMetaJson)) {
				Map<String, Object> conMap = JSON.parseObject(pMetaJson, HashMap.class);
				if (EmptyUtil.isNotEmpty(conMap)) {
					Boolean isComplete = MapValueUtils
							.getBooleanValue(SystemCalculateConstants.META_SECTION_COMPLETED, conMap);
					if (EmptyUtil.isNotEmpty(isComplete)) {
						if (isComplete) {
							part.setRecordStatus(AhuStatusEnum.SELECT_COMPLETE.getId());
							allSectionComplete = true;
						}else{
							allSectionComplete = false;//有没有完成选型的段
						}
					}
				}
			}
			part.setMetaJson(pMetaJson);
			part.setSectionKey(sectionVO.getKey());
			part.setCost(sectionVO.getCost());
			part.setPid(ahuVO.getPid());
			part.setUnitid(ahuVO.getUnitid());
			part.setPosition(sectionVO.getPosition());

			if (StringUtils.isBlank(part.getPartid())) {
				User user = getUser();
				String pid = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
				part.setPartid(pid);
				sectionService.addSection(part, getUserName());
			} else {
				partIds.add(sectionVO.getPartid());
				sectionService.updateSection(part, getUserName());
			}
			parts.add(part);
		}
		// divide(parts, unit);

		/**
		 * 删除废弃的段
		 */
		for (Part oldPart : oldParts) {
			boolean deleteFlag = true;
			for (String partId : partIds) {
				if (oldPart.getPartid().equals(partId)) {
					deleteFlag = false;
				}
			}
			if (deleteFlag) {
				sectionService.deleteSection(oldPart.getPartid());
			}
		}

		/**
		 * ahu 附加属性
		 */
		// AHU段信息发生变化后，自动移动到未分组,变更组编码，删除组绑定
		List<Part> partList = sectionService.findSectionList(ahuVO.getUnitid());
		String groupCodeNew = TemplateUtil.getGroupCodeSectionKeysSorted(null, partList);
		if (!groupCodeNew.equals(groupCodeOld)) {
			// 机组编码发生变化时，移动到未分组
			unit.setGroupId(null);
			unit.setGroupCode(groupCodeNew);
			ahuGroupBindService.deleteBind(ahuVO.getUnitid());
		}

		//标准页面保存
		if(!"true".equals(ahuVO.getIsNS())) {
			// 分段中
			unit.setRecordStatus(AhuStatusEnum.UNSPLIT.getId());
			// clear price and weight
			unit.setPrice(0d);
			unit.setWeight(0d);
		}else{//非标页面保存
			String ahuMetaJson = unit.getMetaJson();
			Map<String, String> metaMap = JSON.parseObject(ahuMetaJson, HashMap.class);

			String deformationEnable = String.valueOf(metaMap.get(METANS_NS_AHU_DEFORMATION));
			String deformationSerial = String.valueOf(metaMap.get(METANS_NS_AHU_DEFORMATIONSERIAL));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(deformationEnable)
					&&EmptyUtil.isNotEmpty(metaMap.get(METANS_NS_AHU_DEFORMATIONSERIAL))){
				unit.setPanelSeries(unit.getProduct()+deformationSerial);
			}else{
				unit.setPanelSeries(unit.getSeries());//没有勾选变形、非标型号置空
			}
			//计算非标总价格，需求要求非标界面点击保存时要计算非标价格，并更新总价格
			//unit = calculatorTool.calculateAHUNsPrice(unit, parts);
			//2020/11/25开会讨论，要求废弃上面的逻辑，非标界面点击保存时清空标准价格，需要重新点击计算价格
			//非标界面保存时，清空标准价格
            if(unit.getPrice() != -1) {
                unit.setStandPrice(unit.getPrice() - unit.getNsprice());
            }
			unit.setPrice(-1.0);
		}

		ahuService.updateAhu(unit, getUserName());
		ahuStatusTool.syncStatus(ahuVO.getPid(), getUserName());


        // 如果机组正常结束，保存的时候，需要将默认分段的数据同时保存
        if (allSectionComplete && !"true".equals(ahuVO.getIsNS())) {
            Partition partition = AhuPartitionGenerator.generatePartition(ahuVO.getUnitid(), unit, partList);
            this.partitionService.savePartition(partition, getUserName());
        }

		/**
		 * 自动测试相关 should be deleted
		 */
        //this.generateTestData(ahuVO, sectionVOs, unit);

		return ApiResult.success("");
	}
	
	/**
	 * Should be deleted.
	 * 
	 * @param ahuVO
	 * @param sectionVOs
	 * @param unit
	 */
    private void generateTestData(AhuVO ahuVO, List<SectionVO> sectionVOs, Unit unit) {
        try {
            // 将正常机组最新段信息写入 testCase excel
			logger.error("**************" + unit.getUnitid() + " generateTestData start:" + toWrite);
			if (toWrite) {
                User user = getUser();
                LanguageEnum language = LanguageEnum.byId(user.getPreferredLocale());
                UnitSystemEnum unitType = UnitSystemEnum.byId(user.getUnitPreferCode());
                Project project = projectService.getProjectById(ahuVO.getPid());
                boolean writeRet = SectionTestCaseUtil.doWrite(unit, sectionVOs, language, unitType, project);
                logger.error("**************" + unit.getUnitid() + " write result:" + writeRet + "**************");
            }
        } catch (Exception e) {
            logger.error("Exception in generate new partition:" + e.getMessage(), e);
        }
    }

	// private String divide(List<Part> parts, Unit unit) {
	// LinkedList<String> keys = new LinkedList<String>();
	// for (Part part : parts) {
	// keys.add(part.getSectionKey());
	// }
	// String ahuGroupCode = MetaCodeGen.getAhuGroupCode(keys);
	// List<GroupInfo> ahuGroups = groupService.findGroupList(unit.getPid());
	// boolean exist = false;
	// if (EmptyUtil.isNotEmpty(ahuGroupCode)) {
	// for (GroupInfo ahuGroup : ahuGroups) {
	// if (ahuGroupCode.equals(ahuGroup.getGroupCode())) {
	// exist = true;
	// unit.setGroupId(ahuGroup.getGroupId());
	// break;
	// }
	// }
	// }
	// if (!exist) {
	// unit.setGroupId(null);
	// }
	// // if(!exist){
	// // AhuGroup ahuGroup = new AhuGroup();
	// // ahuGroup.setAhuGroupCode(ahuGroupCode);
	// // ahuGroup.setGroupName(ahuGroupCode);
	// // ahuGroup.setPid(unit.getPid());
	// // String ahuGroupId = ahuGroupService.addAhuGroup(ahuGroup);
	// // unit.setGroupId(ahuGroupId);
	// // }
	// ahuService.updateAhu(unit, getUserName());
	// return "";
	// }

	@RequestMapping(value = "section/delete/{sectionId}", method = RequestMethod.DELETE)
	public ApiResult<Boolean> delete(@PathVariable("sectionId") String sectionId) {
		Part part = sectionService.findSectionById(sectionId);
		if (EmptyUtil.isNotEmpty(part)) {
			sectionService.deleteSection(sectionId);
			ahuStatusTool.syncStatus(part.getPid(), getUserName());
		}
		return ApiResult.success(true);
	}
    @RequestMapping(value = "section/write2excel", method = RequestMethod.GET)
    public ApiResult<String> write2excel(String write) throws Exception {
    	if(RestConstant.SYS_ASSERT_TRUE.equals(write)){
    		toWrite = true;
    	}
        return ApiResult.success(String.valueOf(toWrite));
    }
}

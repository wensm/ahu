package com.carrier.ahu.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.carrier.ahu.constant.RestConstant;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	// 为ws.HTML 提供便捷的路径映射。
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController(RestConstant.SYS_CONFIG_ADD_VIEW_NAME_WS)
				.setViewName(RestConstant.SYS_CONFIG_ADD_VIEW_NAME_WS);
	}

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}

package com.carrier.ahu.vo;

import lombok.Data;

import java.util.Collection;

@Data
public class ElectricHeatingVO {
    private Collection<Object> resultData;
    private String sectionKey;
    private String userRole;//角色

}

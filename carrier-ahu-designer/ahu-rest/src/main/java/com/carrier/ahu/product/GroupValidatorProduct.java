package com.carrier.ahu.product;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.report.ExportUnit;
import com.carrier.ahu.service.AhuService;

@Component
public class GroupValidatorProduct implements ValidatorProduct{
	@Autowired
	private AhuService ahuService;
	
	public GroupValidatorProduct(AhuService ahuService) {
		this.ahuService = ahuService;
	}
	
	@Override
	public boolean validate(ExportUnit eUnit, Project project, String groupId) {

	
		return false;
	}
	
	/**
	 * model为1的覆盖模式，对机组编号进行校验。
	 * @param eUnit
	 * @param project
	 * @param groupId
	 * @return Map
	 */
	@Override
	public Map<String, Object> validate1(ExportUnit eUnit, Project project, String groupId) {

		Map<String, Object> rstMap = new HashMap<String, Object>();
		String pid = project.getPid();
		String drawingNo = eUnit.getDrawingNo();

		// Unit unitBefore = ahuService.findAhuByDrawingNO(pid, drawingNo);
		Unit unitBefore = ahuService.findAhuByDrawingNOAndGroupId(pid, drawingNo, groupId);// 本组历史记录查询

		if (null == unitBefore) {// 不存在于本组中
			Unit unitBefore1 = ahuService.findAhuByDrawingNO(pid, drawingNo);// 所有历史记录查询
			if (eUnit.getDrawingNo().equals(unitBefore1 != null ? unitBefore1.getDrawingNo() : null)) {// 机组编号已存在于其他分组，不新增
				rstMap.put(RestConstant.SYS_STRING_ACTION, RestConstant.SYS_STRING_ACTION_SKIP);// action:skip
				rstMap.put(RestConstant.SYS_STRING_RESULT, "groupPRB");// result:"groupPRB"
			} else {//否则新增
				rstMap.put(RestConstant.SYS_STRING_ACTION, RestConstant.SYS_STRING_ACTION_ADD);// action:add
				rstMap.put(RestConstant.SYS_STRING_RESULT, RestConstant.SYS_MSG_RESPONSE_SUCCESS_1);// result:success
			}

		} else {// 存在于本组中，直接覆盖
			rstMap.put(RestConstant.SYS_STRING_ACTION, RestConstant.SYS_STRING_ACTION_COVER);// action:cover
			rstMap.put(RestConstant.SYS_STRING_RESULT, RestConstant.SYS_MSG_RESPONSE_SUCCESS_1);// result:success
		}

		return rstMap;
	}
	
	/**
	 * model不为1的新增模式，对机组编号进行校验。
	 * @param eUnit
	 * @param project
	 * @param groupId
	 * @return Map
	 */
	public Map<String, Object> validate2(ExportUnit eUnit, Project project, String groupId) {
		
		Map<String, Object> rstMap = new HashMap<String, Object>();
		String pid = project.getPid();
		String drawingNo = eUnit.getDrawingNo();
		
		Unit unitBefore = ahuService.findAhuByDrawingNOAndGroupId(pid, drawingNo, groupId);
		Unit unitBefore1 = ahuService.findAhuByDrawingNO(pid, drawingNo);
		if ((eUnit.getDrawingNo()).equals(unitBefore != null ? unitBefore.getDrawingNo() : null)) {// 机组编号已存在于本组
			rstMap.put(RestConstant.SYS_STRING_ACTION, RestConstant.SYS_STRING_ACTION_SKIP);//action:skip
			rstMap.put(RestConstant.SYS_STRING_RESULT, "existPRB");//result:"existPRB"
		} else if ((eUnit.getDrawingNo()).equals(unitBefore1 != null ? unitBefore1.getDrawingNo() : null)) {// 机组编号已存在于其他组
			rstMap.put(RestConstant.SYS_STRING_ACTION, RestConstant.SYS_STRING_ACTION_SKIP);//action:skip
			rstMap.put(RestConstant.SYS_STRING_RESULT, "groupPRB");//result:"existPRB"
		}else {//否则新增操作
			rstMap.put(RestConstant.SYS_STRING_ACTION, RestConstant.SYS_STRING_ACTION_ADD);//action:add
			rstMap.put(RestConstant.SYS_STRING_RESULT, RestConstant.SYS_MSG_RESPONSE_SUCCESS_1);//result:success
		}
		
		return rstMap;
	}
	
	//用来注入ahuService
	public GroupValidatorProduct() {
    }
	public static GroupValidatorProduct groupValidatorProduct;
	@PostConstruct
    public void init() {
		groupValidatorProduct = this;
		groupValidatorProduct.ahuService = this.ahuService;
    }
}
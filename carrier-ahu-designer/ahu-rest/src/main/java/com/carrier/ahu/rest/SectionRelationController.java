package com.carrier.ahu.rest;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.SectionRelationValidatorUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.vo.AirDirectionVO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.carrier.ahu.constant.CommonConstant.METAHU_PRODUCT;

@Api(description = "section 验证")
@RestController
public class SectionRelationController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(SectionRelationController.class);

    @RequestMapping(value = "sectionrelation", method = RequestMethod.POST)
    public Map<String, Object> validate(String sections,String ahuStr) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        Gson gson = new Gson();
        List<AirDirectionVO> airDirectionVOList = gson.fromJson(sections, new TypeToken<List<AirDirectionVO>>() {
        }.getType());
        Map<String, Object> ahuMp = gson.fromJson(ahuStr, Map.class);
        if (!EmptyUtil.isEmpty(airDirectionVOList)) {
            result.put(RestConstant.SYS_MSG_RESPONSE_RESULT_PASS, true);
            String message = RestConstant.SYS_BLANK;
            try {
                SectionRelationValidatorUtil.dragDropValidate(airDirectionVOList,ahuMp);
            } catch (ApiException exp) {
                if(null != exp.getErrorCode())
                    message = AHUContext.getIntlString(exp.getErrorCode().getMessage(), exp.getParams());
                else
                    message = exp.getMessage();

                result.put(RestConstant.SYS_MSG_RESPONSE_RESULT_PASS, false);

                //只有一个可忽略的异常明细报错（包含“-”的报错信息：前端可以忽略的报错，即使报错了也可以保存）
                if(message.indexOf(";-")>=0 && message.indexOf(";-") == message.lastIndexOf(";-")){
                    result.put(RestConstant.SYS_MSG_RESPONSE_RESULT_PASS, true);
                    message = message.replace(";-","");
                }
            }
            result.put(RestConstant.SYS_MSG_RESPONSE_RESULT_MSG, message);
        }
        return result;
    }
}

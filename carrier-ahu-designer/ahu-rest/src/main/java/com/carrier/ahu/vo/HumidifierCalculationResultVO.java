package com.carrier.ahu.vo;

import java.util.List;

import lombok.Data;

@Data
public class HumidifierCalculationResultVO<J extends Object> {
	private List<J> resultData;
	private String sectionKey;
	private String season;
}

package com.carrier.ahu.rest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.ApiResult;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.carrier.ahu.common.model.partition.AhuPartition.S_MKEY_METAJSON;
import static com.carrier.ahu.constant.CommonConstant.METANS_NS_AHU_DEFORMATIONSERIAL;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39XT;

@Api(description = "面板接口")
@RestController
public class PanelController extends AbstractController {
    @Autowired
    SectionService sectionService;
    @Autowired
    AhuService ahuService;
    @Autowired
    PartitionService partitionService;

    @ApiOperation(value="初始化面板切割")
    @RequestMapping(value = "/panel/init/{ahuId}", method = RequestMethod.GET)
    public ApiResult<Object> init(@PathVariable("ahuId") String ahuId,String panelSeries) throws Exception {
        Unit unit = ahuService.findAhuById(ahuId);
        Partition partition = partitionService.findPartitionByAHUId(ahuId);
//        List<AhuPartition> ahuPartitionList = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
        List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit,partition);

        //强制更新panelSeries 防止老数据没有panelSeriees
        String ahuMetaJson = unit.getMetaJson();
        Map<String, String> metaMap = JSON.parseObject(ahuMetaJson, HashMap.class);

        String deformationEnable = String.valueOf(metaMap.get(UtilityConstant.METANS_NS_AHU_DEFORMATION));
        String deformationSerial = String.valueOf(metaMap.get(METANS_NS_AHU_DEFORMATIONSERIAL));
        if(UtilityConstant.SYS_ASSERT_TRUE.equals(deformationEnable) &&
                null != metaMap.get(METANS_NS_AHU_DEFORMATIONSERIAL) &&
                EmptyUtil.isNotEmpty(deformationSerial)){
            unit.setPanelSeries(unit.getProduct()+deformationSerial);
        }

        //引用unit的panelSeries
        if(EmptyUtil.isEmpty(panelSeries) && EmptyUtil.isNotEmpty(unit.getPanelSeries()) &&
                ahuPartitionList.size()>0 && EmptyUtil.isEmpty(ahuPartitionList.get(0).getPanels())){
            panelSeries = unit.getPanelSeries();
        }

        List<Part> partList = sectionService.findSectionList(ahuId);
        reBuildPartMetaJson(partList,ahuPartitionList);

        Gson gson = new Gson();
        String series = unit.getSeries();
        boolean isChange=false;
        //面板切割页面变形处理。
        if(EmptyUtil.isNotEmpty(panelSeries) && !series.equals(panelSeries)){
            series = panelSeries;
            isChange = true;
            for (AhuPartition ahuPartition : ahuPartitionList) {
                int[] dimension = AhuUtil.getHeightAndWidthOfAHU(panelSeries);
                ahuPartition.setHeight(dimension[0]);
                ahuPartition.setWidth(dimension[1]);

                //清空切割
                //panel
                ahuPartition.setPanels(null);
                //底座
                ahuPartition.setBase("");

                //写入型号变形
                unit.setPanelSeries(panelSeries);
                ahuService.updateAhu(unit, getUserName());
            }
        }

        if(ahuPartitionList.size()>0 && EmptyUtil.isEmpty(ahuPartitionList.get(0).getPanels())) {
            try {
                AhuPartitionGenerator.populatePartitionPanel(series,isChange, ahuPartitionList);
                partition.setPartitionJson(gson.toJson(ahuPartitionList));
                //XT系列处理面板箱体零部件清单统计
                if(series.contains(AHU_PRODUCT_39XT)) {
                    AhuPartitionGenerator.initXTSummary(series,unit,partition);
                }
                this.partitionService.savePartition(partition, getUserName());
            } catch (Exception e) {
                e.printStackTrace();
                throw new ApiException(ErrorCode.PANEL_INIT_ERROR);
            }
        }
        return ApiResult.success();
    }

    /**
     * 重置ahuPartitionList sections part metaJson防止类似“方向设置”等功能修改段信息么有同步到面板切割问题。
     * @param partList
     * @param ahuPartitionList
     */
    private void reBuildPartMetaJson(List<Part> partList, List<AhuPartition> ahuPartitionList) {
        Map<String,Part> parts = new HashedMap<>();
        for (Part part : partList) {
            parts.put(String.valueOf(part.getPosition()),part);
        }
        for (AhuPartition ahuPartition : ahuPartitionList) {
            for (Map<String, Object> section : ahuPartition.getSections()) {
                String position = String.valueOf(AhuPartition.getPosOfSection(section));
                if(parts.containsKey(position)){
                    Part curPart = parts.get(position);
                    curPart.getMetaJson();
                    section.put(S_MKEY_METAJSON,curPart.getMetaJson());
                }
            }
        }

    }

    /**
     * 初始化面板AB
     * @param panelJson 当前分段面板Json:panelJson
     * @return
     * @throws Exception
     */
    @ApiOperation(value="初始化面板AB")
    @RequestMapping(value = "/panel/initab", method = RequestMethod.POST)
    public ApiResult<Object> initab(String panelJson) throws Exception {
        String initedJson = CommonConstant.SYS_BLANK;
        try {
            initedJson = AhuPartitionGenerator.initab(panelJson);
        } catch (Exception e) {
            String message = getIntlString(e.getMessage());
            if(message.equals(e.getMessage())) {
                throw new ApiException(ErrorCode.PANEL_INITAB_ERROR);
            }else{
                throw new ApiException(message);
            }
        }
        return ApiResult.success(initedJson);
    }

    /**
     * 面板合并
     * @param panel1 第一个面板id
     * @param panel2 第二个面板id
     * @param panelJson 当前操作的面Json
     * @return
     * @throws Exception
     */
    @ApiOperation(value="面板合并")
    @RequestMapping(value = "/panel/mergePanel", method = RequestMethod.POST)
    public ApiResult<Object> mergePanel(String panel1,String panel2,String panelJson,String product) throws Exception {
        String mergedJson = CommonConstant.SYS_BLANK;
        try {
            mergedJson = AhuPartitionGenerator.mergePanel(panel1, panel2,panelJson,product);
        } catch (Exception e) {
            String message = getIntlString(e.getMessage());
            if(message.equals(e.getMessage())) {
                throw new ApiException(ErrorCode.PANEL_MERGEPANEL_ERROR);
            }else{
                throw new ApiException(message);
            }
        }
        return ApiResult.success(mergedJson);
    }
}

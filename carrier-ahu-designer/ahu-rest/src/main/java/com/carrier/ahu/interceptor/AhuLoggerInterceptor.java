package com.carrier.ahu.interceptor;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.carrier.ahu.constant.RestConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 请求拦截器
 * 1,用于做日志记录分组
 * Created by Wen zhengtao on 2017/3/28.
 */
public class AhuLoggerInterceptor extends HandlerInterceptorAdapter{
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //根据用户做日志分组,userName可以根据一定的规则生成，用于区分是哪个厂，哪个人
        String userName = (String)request.getAttribute(RestConstant.SYS_USER_NAME);
        if(StringUtils.isEmpty(userName)){
            userName = RestConstant.SYS_USER_NAME_UNKNOWN;//一个默认账户
        }
        MDC.put(RestConstant.SYS_USER_NAME,userName);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        super.afterConcurrentHandlingStarted(request, response, handler);
    }
}

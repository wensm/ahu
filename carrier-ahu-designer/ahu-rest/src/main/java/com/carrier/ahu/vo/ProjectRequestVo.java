package com.carrier.ahu.vo;

import lombok.Data;

/**
 * 
 * @author Simon
 * @since 2017-12-17
 */
@Data
public class ProjectRequestVo {
	/** 项目id */
	private String projectId;
	/** 检修门方向 */
	private String doororientation;
	/** 接管方向 */
	private String pipeorientation;
	/** 需要变更的AHUid列表 */
	private String[] ahuIds;

}

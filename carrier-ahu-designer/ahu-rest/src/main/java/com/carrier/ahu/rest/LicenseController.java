package com.carrier.ahu.rest;

import java.net.URLDecoder;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.carrier.ahu.AHUSpringContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.license.LicenseInfo;
import com.carrier.ahu.license.LicenseManager;
import com.carrier.ahu.po.auth.License;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.util.upgrade.UpgradeUtils;
import com.carrier.ahu.vo.ApiResult;

import io.swagger.annotations.Api;

/**
 * Created by liangd4 on 2018/2/28.
 */
@Api(description = "license校验接口")
@RestController
public class LicenseController extends AbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LicenseController.class.getName());
    private static boolean sign = true; // license开关

    @Value("${system.admin.email}")
    private String adminEmail;

    @Value("${ahu.version}")
    private String ahuVersion;
    @Value("${ahu.patch.version}")
    private String ahuPatchVersion;

    @RequestMapping(value = "license", method = RequestMethod.GET)
    public ApiResult<License> getLicense() throws Exception {
        License license = new License();
        license.setSuccess(true);
        if (sign) {// 标签是否开启
            try {
                LicenseInfo licenseInfo = LicenseManager.loadLicenseInfo(ahuVersion);
                LicenseInfo machineInfo = LicenseManager.getMachineLicenseInfo();
                boolean isDevUser = AHUSpringContextUtil.isDevProfile();
                if (!LicenseManager.validateLicenseInfo(licenseInfo, ahuVersion,isDevUser)) {
                    license.setSuccess(false);
                }
                if (!license.isSuccess()) {
                    machineInfo.setVersion(ahuVersion); // set current ahu version
                    machineInfo.setPatchVersion(ahuPatchVersion); // set current ahu patch version
                    String licenseString = LicenseManager.encodeLicenseApplication(machineInfo);
                    license.setLicenseDat(licenseString);
                    license.setAdminEmail(adminEmail);
                }
                return ApiResult.success(license);
            } catch (Exception e) {
                LOGGER.error("License authentication failed: " + e);
                throw new ApiException(ErrorCode.GET_LICENSE_FAILED);
            }
        }
        return ApiResult.success(license);
    }

    @RequestMapping(value = "license/sign", method = RequestMethod.GET)
    public ApiResult<String> getLicenseSign(boolean signB) throws Exception {
        sign = signB;
        return ApiResult.success(String.valueOf(signB));
    }

    @RequestMapping(value = "license/application/download", method = RequestMethod.GET)
    public ResponseEntity<byte[]> downloadLicenseApplication() throws Exception {
        try {
            LicenseInfo machineInfo = LicenseManager.getMachineLicenseInfo();
            machineInfo.setVersion(ahuVersion); // set current ahu version
            machineInfo.setPatchVersion(ahuPatchVersion); // set current ahu patch version
            String licenseApplicationString = LicenseManager.encodeLicenseApplication(machineInfo);
            licenseApplicationString = URLDecoder.decode(licenseApplicationString, RestConstant.SYS_ENCODING_UTF8_UP);
            return download(Base64.getDecoder().decode(licenseApplicationString),
                    RestConstant.SYS_FILE_NAME_LICENSE_DAT);
        } catch (Exception e) {
            LOGGER.error("Failed to download license application: " + e);
        }
        throw new ApiException(ErrorCode.GET_LICENSE_FAILED);
    }

    @RequestMapping(value = "license/install", method = RequestMethod.POST)
    public ApiResult<String> installLicense(HttpServletRequest request) throws Exception {
        try {
            CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
                    request.getSession().getServletContext());
            MultipartFile licenseFile = null;
            if (multipartResolver.isMultipart(request)) {
                MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
                Iterator<String> iter = multiRequest.getFileNames();
                if (iter.hasNext()) {
                    licenseFile = multiRequest.getFile(iter.next());
                }
            }
            boolean isDevUser = AHUSpringContextUtil.isDevProfile();
            LicenseInfo licenseInfo = LicenseManager.getLicenseInfo(licenseFile.getBytes());
            if (!LicenseManager.validateLicenseInfo(licenseInfo, ahuVersion, isDevUser)) {
                throw new ApiException(ErrorCode.LICENSE_FILE_IS_INVALID);
            }
            // install license file
            LicenseManager.installLicense(licenseFile.getBytes());

            // load user into context
            User user = this.userService.installUser(licenseInfo);
            user.setUnitPrefer(AhuSectionMetas.getInstance().getUnitSetting().get(user.getUnitPreferCode()));
            user.setDefaultParaPrefer(TemplateUtil.getDefaultParameterFromMeta());
            AHUContext.setUser(user);
            List<UserMeta> userMetas = this.userService.findUserMetaByUserAndFactory(licenseInfo.getUserName(),
                    licenseInfo.getFactory());
            AHUContext.setUserMeta(userMetas);
        } catch (Exception e) {
            LOGGER.error("Failed to install license", e);
            throw new ApiException(ErrorCode.INSTALL_LICENSE_FILE_FAILED);
        }
        return ApiResult.success();
    }

    @RequestMapping(value = "upgrade", method = RequestMethod.POST)
    public ApiResult<String> upgrade(HttpServletRequest request) throws Exception {
        try {
            CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
                    request.getSession().getServletContext());
            MultipartFile upgradeFile = null;
            if (multipartResolver.isMultipart(request)) {
                MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
                Iterator<String> iter = multiRequest.getFileNames();
                if (iter.hasNext()) {
                    upgradeFile = multiRequest.getFile(iter.next());
                }
            }
            if (upgradeFile != null) {
                if (UpgradeUtils.isValidZipVersion(upgradeFile.getOriginalFilename(), AHUContext.getAhuVersion())) {
                    unzipUpgradeFile(upgradeFile.getInputStream());
                } else {
                    throw new ApiException(ErrorCode.INVALID_UPGRADE_FILE);
                }
            }
        } catch (ApiException exp) {
            throw exp;
        } catch (Exception e) {
            LOGGER.error("Failed to install license", e);
            throw new ApiException(ErrorCode.INSTALL_UPGRADE_FILE_FAILED);
        }
        return ApiResult.success();
    }

}

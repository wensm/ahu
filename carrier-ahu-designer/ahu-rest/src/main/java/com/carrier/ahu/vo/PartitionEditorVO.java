package com.carrier.ahu.vo;

import lombok.Data;

import java.util.List;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;

/**
 *
 * This VO is designed for render the partition page
 * Created by liujianfeng on 2017/9/21.
 */
@Data
public class PartitionEditorVO {
    private Partition partition;
    private Unit unit;
    private List<Part> parts;

}

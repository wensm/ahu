package com.carrier.ahu.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.carrier.ahu.constant.RestConstant;

/**
 * Restful API接口清单
 * Created by Wen zhengtao on 2017/3/7.
 */
@Controller
public class SwaggerApiController extends AbstractController{
    @RequestMapping(value="/api",method = RequestMethod.GET)
    public String index(){
        return RestConstant.SYS_PATH_REDIRECT_SWAGGER_UI_HTML;
    }
}

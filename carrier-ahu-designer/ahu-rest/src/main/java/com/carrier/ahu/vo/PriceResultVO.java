package com.carrier.ahu.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by liujianfeng on 2017/6/25.
 */
@Data
public class PriceResultVO {
	private List<PriceBean> details;
	private double total;
	private double totalNs;
	private double weight;
	private String ahuId;
	private String priceSpecVersion;
	//add by gaok2 为了非标价格计算位-1时的提示信息
	private String nsMessage;

	@Data
	public static class PriceBean {
		private String name;
		private double price;
		private double nsPrice;
		private String key;
		private double weight;
	}
}

package com.carrier.ahu.vo;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/14.
 */
@Data
public class MixAirConditionParam {

    /*机组信息*/
    private double airVolume;//送风、回风区分
    /*混合段*/
    private double InDryBulbT;//回风干球温度
    private double InWetBulbT;//回风湿球温度
    private double InRelativeT;//回风相对湿度
    private double NewDryBulbT;//新风干球温度
    private double NewWetBulbT;//新风湿球温度
    private double NewRelativeT;//新风相对湿度
    private double NARatio;//新风比
    private double NAVolume;//新风量


}

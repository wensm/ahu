package com.carrier.ahu.vo;

import com.carrier.ahu.report.ReportRequestVob;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import lombok.Data;

/**
 * Created by liujianfeng on 2017/6/20.
 */
@Data
public class ReportRequestVo {
	private String reports;
	private ReportRequestVob reportsObj;

	public void setReports(String reports) {
		this.reports = reports;
		Gson gson = new Gson();
		ReportRequestVob obj = gson.fromJson(reports, new TypeToken<ReportRequestVob>() {
			private static final long serialVersionUID = 1L;
		}.getType());
		reportsObj = obj;
	}

}

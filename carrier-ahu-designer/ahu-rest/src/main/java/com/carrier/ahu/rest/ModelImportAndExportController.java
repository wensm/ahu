package com.carrier.ahu.rest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.calculator.ExcelForPriceCodeUtils;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.*;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.common.util.VersionUtils;
import com.carrier.ahu.common.util.VersionUtils.VersionPair;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.entity.HeatRecycleInfo;
import com.carrier.ahu.factory.GroupValidatorFactory;
import com.carrier.ahu.factory.SerialsValidatorFactory;
import com.carrier.ahu.factory.TemperatureValidatorFactory;
import com.carrier.ahu.factory.ValidatorFactory;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.product.GroupValidatorProduct;
import com.carrier.ahu.product.SerialsValidatorProduct;
import com.carrier.ahu.product.TemperatureValidatorProduct;
import com.carrier.ahu.product.ValidatorProduct;
import com.carrier.ahu.report.*;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.service.*;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.CCRDFileUtil;
import com.carrier.ahu.unit.KeyGenerator;
import com.carrier.ahu.util.*;
import com.carrier.ahu.util.ahu.AhuExporterUtils;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.*;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.*;

import static com.carrier.ahu.common.util.MapRemoveNullUtil.removeNullEntry;
import static com.carrier.ahu.constant.CommonConstant.*;
import static com.carrier.ahu.vo.SysConstants.*;

/**
 * 模型导入导出业务接口 Created by zhiqiang.zhang on 2017/3/16.
 */
@Api(description = "模型导入导出相关接口")
@RestController
public class ModelImportAndExportController extends AbstractController {
    @Value("${ahu.version}")
    private String ahuVersion;
    @Value("${ahu.patch.version}")
    private String ahuPatchVersion;
    @Value("${ahu.export.support.version}")
    private String exportSupportVersion;
    @Value("${ahu.template.support.version}")
    private String supportVersionT;
    @Value("${ahu.export.reset.version}")
    private String exportResetVersion;
    @Autowired
    AhuService ahuService;
    @Autowired
    ProjectService projectService;
    @Autowired
    SectionService sectionService;
    @Autowired
    GroupService groupService;
    @Autowired
    AhuGroupBindService ahuGroupBindService;
    @Autowired
    PartitionService partitionService;
    @Autowired
    CalculatorTool calculatorTool;
    @Autowired
    AhuStatusTool ahuStatusTool;

    /**
     * 项目导入<br>
     * 导入时需要保证：所有的项目组成元素的ID都存在，相互依存关系也存在，采用树形结构的方式进行约束<br>
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "import/proj", method = RequestMethod.POST)
    public ApiResult<Project> importProject(HttpServletRequest request, String projectId) throws Exception {
        Project toBeMergedProject = null;
        if (EmptyUtil.isNotEmpty(projectId)) {
            toBeMergedProject = projectService.getProjectById(projectId);
            if (EmptyUtil.isEmpty(toBeMergedProject)) {
                throw new ApiException(ErrorCode.TARGET_PROJECT_NOT_EXIST);
            }
        }
        CommonsMultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (resolver.isMultipart(request)) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            Iterator<String> iter = multipartRequest.getFileNames();
            while (iter.hasNext()) {
                MultipartFile uploadFile = multipartRequest.getFile(iter.next());
                if (uploadFile != null) {
                    ExportProject exportProject = parseProjectFromUploadFile(uploadFile);
                    checkExportProjectVersion(exportProject);

                    // 不允许导入已存在的项目， 但是可以合并
                    Project existedProject = projectService.getProjectById(exportProject.getPid());
                    if (toBeMergedProject == null && existedProject != null) {
                        return ApiResult.error(String.valueOf(ErrorCode.IMPORT_PROJECT_ID_EXISTS.getCode()),
                                getIntlString(ErrorCode.IMPORT_PROJECT_ID_EXISTS.getMessage()), existedProject);
                    }

                    Map<String, String> updateIds = importOrMergeProject(exportProject, toBeMergedProject);
                    AhuExporterUtils.importImgFile(uploadFile.getInputStream(), updateIds);
                }
            }
        }
        return ApiResult.success(null);
    }

    private ExportProject parseProjectFromUploadFile(MultipartFile uploadFile) throws Exception {
        InputStream input = null;
        BufferedReader reader = null;
        try {
            if (uploadFile.getOriginalFilename().endsWith(PRJ_EXTENSION)) {
                input = uploadFile.getInputStream();
            } else if (uploadFile.getOriginalFilename().endsWith(ZIP_EXTENSION)) {
                AhuExporterUtils.cleanImportFilePath();
                String filePath = DIR_IMPORT + DateUtil.getUserDate(RestConstant.SYS_FORMAT_yyMMddHHmmss) + SLASH_LINUX;
                try {
                    AhuExporterUtils.unzipProjectFile(filePath, uploadFile.getInputStream());
                } catch (Exception exp) {
                    throw new ApiException(ErrorCode.INVALID_PROJECT_FILE);
                }
                File projectFile = AhuExporterUtils.findProjectFile(filePath);
                if (projectFile == null) {
                    throw new ApiException(ErrorCode.INVALID_PROJECT_FILE);
                }
                input = new FileInputStream(projectFile);
            } else {
                throw new ApiException(ErrorCode.INVALID_PROJECT_FILE);
            }

            StringBuffer projectJson = new StringBuffer();
            reader = new BufferedReader(new InputStreamReader(input, Charset.forName("UTF-8")));
            String line = null;
            while ((line = reader.readLine()) != null) {
                projectJson.append(line);
            }

            return JSONObject.parseObject(projectJson.toString(), ExportProject.class);
        } finally {
            IOUtils.closeQuietly(reader);
            IOUtils.closeQuietly(input);
        }
    }

    private void checkExportProjectVersion(ExportProject project) {
        if (!EmptyUtil.isEmpty(project.getVersion()) && !EmptyUtil.isEmpty(exportSupportVersion)) {
            if (!VersionValidationUtil.packageProjectValidation(project.getVersion(), exportSupportVersion)) {
                throw new ApiException(ErrorCode.IMPORT_VERSION_NOT_EQUALS);
            }
        }
    }

    private Map<String, String> importOrMergeProject(ExportProject exportProject, Project toBeMergedProject) throws Exception {
        Map<String, String> updateIds = Maps.newHashMap();

        Project updateProject = toBeMergedProject == null ? new Project() : toBeMergedProject;
        if (toBeMergedProject == null) {
            BeanUtils.copyProperties(updateProject, exportProject);
        } else {
            updateIds.put(exportProject.getPid(), toBeMergedProject.getPid());
        }

        List<GroupInfo> groups = Lists.newArrayList();
        List<Unit> units = Lists.newArrayList();
        List<Part> parts = Lists.newArrayList();
        List<Partition> partitions = Lists.newArrayList();
        List<AhuGroupBind> groupBinds = Lists.newArrayList();

        // 导入分组
        List<ExportGroup> exportGroups = exportProject.getGroups();
        for (ExportGroup exportGroup : exportGroups) {
            GroupInfo group = importOrMergeGroup(exportGroup, groups, toBeMergedProject);

            List<ExportUnit> exportUnits = exportGroup.getUnits();
            for (ExportUnit exportUnit : exportUnits) {
                Unit unit = importOrMergeGroupUnit(exportUnit, group, units, toBeMergedProject);
                updateIds.put(exportUnit.getUnitid(), unit.getUnitid());

                importOrMergeGroupBind(group, unit, groupBinds);
                importOrMergePartition(exportUnit, unit, partitions, toBeMergedProject);
                importOrMergePart(exportUnit, unit, parts, toBeMergedProject);
            }
        }

        // 导入未分组
        List<ExportUnit> exportUnits = exportProject.getUnits();
        for (ExportUnit exportUnit : exportUnits) {
            Unit unit = importOrMergeGroupUnit(exportUnit, null, units, toBeMergedProject);
            updateIds.put(exportUnit.getUnitid(), unit.getUnitid());

            importOrMergePartition(exportUnit, unit, partitions, toBeMergedProject);
            importOrMergePart(exportUnit, unit, parts, toBeMergedProject);
        }

        if (toBeMergedProject != null) {
            updateDrawingNoBeforeMerge(updateProject, units);
        }

        // should be in the same transaction
        if (toBeMergedProject == null) {
            projectService.addProject(updateProject, getUserName());
        } else {
            projectService.updateProject(updateProject, getUserName());
        }

        groupService.addGroups(groups);
        ahuService.addOrUpdateAhus(units);
        sectionService.addSections(parts);
        ahuGroupBindService.addBinds(groupBinds, getUserName());
        partitionService.addOrUpdateAhus(partitions);
        if (toBeMergedProject != null) {
            ahuStatusTool.syncStatus(toBeMergedProject.getPid(), getUserName());
        } else {
            ahuStatusTool.syncStatus(exportProject.getPid(), getUserName());
        }

        // check if project version in reset configuration
        // then need to reset project status to selecting
        if (EmptyUtil.isNotEmpty(exportResetVersion)) {
            List<VersionPair> versionPairs = VersionUtils.parseVersionPairs(exportResetVersion);
            if (VersionUtils.withinVersionPairs(exportProject.getVersion(), versionPairs)) {
                ahuStatusTool.resetStatus(
                        toBeMergedProject != null ? toBeMergedProject.getPid() : exportProject.getPid(), getUserName());
            }
        }

        return updateIds;
    }

    private GroupInfo importOrMergeGroup(ExportGroup exportGroup, List<GroupInfo> groups, Project toBeMergedProject)
            throws Exception {
        if (StringUtils.isBlank(exportGroup.getGroupId())) {
            throw new ApiException(ErrorCode.IMPORT_GROUP_ID_EMPTY);
        }

        GroupInfo group = new GroupInfo();
        BeanUtils.copyProperties(group, exportGroup);
        if (toBeMergedProject != null) {
            // 合并时统一重新生成ID
            group.setGroupId(KeyGenerator.genGroupId(AHUContext.getFactoryName(), getUserName()));
            group.setProjectId(toBeMergedProject.getPid());
        }
        groups.add(group);
        return group;
    }

    private Unit importOrMergeGroupUnit(ExportUnit exportUnit, GroupInfo group, List<Unit> units,
                                        Project toBeMergedProject) throws Exception {
        if (StringUtils.isBlank(exportUnit.getUnitid())) {
            throw new ApiException(ErrorCode.IMPORT_UNIT_ID_EMPTY);
        }

        Unit unit = new Unit();
        BeanUtils.copyProperties(unit, exportUnit);
        if (toBeMergedProject != null) {
            // 合并时统一重新生成ID
            unit.setUnitid(KeyGenerator.genUnitId(AHUContext.getFactoryName(), getUserName()));
            unit.setPid(toBeMergedProject.getPid());
        }
        if (group != null) {
            unit.setGroupId(group.getGroupId());
        }
        units.add(unit);
        return unit;
    }

    private void importOrMergePartition(ExportUnit exportUnit, Unit unit, List<Partition> partitions,
                                        Project toBeMergedProject) throws Exception {
        if (exportUnit.getPartition() != null) {
            Partition partition = new Partition();
            BeanUtils.copyProperties(partition, exportUnit.getPartition());
            // clear partition id, let system to generate
            partition.setPartitionid(null);
            partition.setUnitid(unit.getUnitid());
            if (toBeMergedProject != null) {
                partition.setPid(toBeMergedProject.getPid());
            }
            partitions.add(partition);
        }
    }

    private void importOrMergeGroupBind(GroupInfo group, Unit unit, List<AhuGroupBind> groupBinds) {
        AhuGroupBind groupBind = new AhuGroupBind();
        groupBind.setUnitid(unit.getUnitid());
        groupBind.setGroupid(group.getGroupId());
        groupBinds.add(groupBind);
    }

    private void importOrMergePart(ExportUnit exportUnit, Unit unit, List<Part> parts, Project toBeMergedProject)
            throws Exception {
        for (ExportPart exportPart : exportUnit.getExportParts()) {
            if (StringUtils.isBlank(exportPart.getPartid())) {
                throw new ApiException(ErrorCode.IMPORT_SECTION_ID_EMPTY);
            }
            Part part = new Part();
            BeanUtils.copyProperties(part, exportPart);
            if (toBeMergedProject != null) {
                // 合并时统一重新生成ID
                part.setPartid(KeyGenerator.genPartId(AHUContext.getFactoryName(), getUserName()));
                part.setUnitid(unit.getUnitid());
                part.setPid(toBeMergedProject.getPid());
            }
            parts.add(part);
        }
    }

    // 合并的机组drawing从当前机组最大值开始递增
    private void updateDrawingNoBeforeMerge(Project updateProject, List<Unit> units) {
        Collections.sort(units, new Comparator<Unit>() {
            @Override
            public int compare(Unit u1, Unit u2) {
                u1.getDrawingNo().compareTo(u2.getDrawingNo());
                return 0;
            }
        });

        int maxDrawingNo = ahuService.getMaxDrawingNo(updateProject.getPid());
        for (int i = 0; i < units.size(); i++) {
            Unit unit = units.get(i);
            String drawingNo = String.valueOf(++maxDrawingNo);
            unit.setUnitNo(StringUtils.leftPad(drawingNo, SYS_UNIT_NO_LENGTH, SYS_STRING_NUMBER_0));
            unit.setDrawingNo(drawingNo);
        }
    }

    /**
     * 项目导入<br>
     * 导入时需要保证：所有的项目组成元素的ID都存在，相互依存关系也存在，采用树形结构的方式进行约束<br>
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "import/proj_v0", method = RequestMethod.POST)
    public ApiResult<Project> projImport(HttpServletRequest request, String projectId) throws Exception {
        Project selectedProject = null;
        boolean hasProject2CoverFlag = false;
        if (EmptyUtil.isNotEmpty(projectId)) {
            hasProject2CoverFlag = true;
            selectedProject = projectService.getProjectById(projectId);
            if (EmptyUtil.isEmpty(selectedProject)) {
                throw new ApiException(ErrorCode.TARGET_PROJECT_NOT_EXIST);
            }
        }
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
                request.getSession().getServletContext());
        // 判断 request 是否有文件上传,即多部分请求
        if (multipartResolver.isMultipart(request)) {
            // 转换成多部分request
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            // 取得request中的所有文件名
            Iterator<String> iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                // 取得上传文件
                MultipartFile uploadFile = multiRequest.getFile(iter.next());
                if (EmptyUtil.isNotEmpty(uploadFile)) {
                    InputStream in = null;
                    if (uploadFile.getOriginalFilename().endsWith(SysConstants.PRJ_EXTENSION)) {
                        in = uploadFile.getInputStream();
                    } else if (uploadFile.getOriginalFilename().endsWith(SysConstants.ZIP_EXTENSION)) {
                        AhuExporterUtils.cleanImportFilePath();
                        String importFilePath = SysConstants.DIR_IMPORT
                                + DateUtil.getUserDate(RestConstant.SYS_FORMAT_yyMMddHHmmss) + SysConstants.SLASH_LINUX;
                        try {
                            AhuExporterUtils.importImgFile(importFilePath, uploadFile.getInputStream());
                        } catch (Exception exp) {
                            throw new ApiException(ErrorCode.INVALID_PROJECT_FILE);
                        }
                        File projectFile = AhuExporterUtils.findProjectFile(importFilePath);
                        if (projectFile == null) {
                            throw new ApiException(ErrorCode.INVALID_PROJECT_FILE);
                        }
                        in = new FileInputStream(projectFile);
                    } else {
                        throw new ApiException(ErrorCode.INVALID_PROJECT_FILE);
                    }

                    // read project json string from .prj file
                    StringBuffer projectJsonString = new StringBuffer();
                    InputStreamReader reader = new InputStreamReader(in, Charset.forName("UTF-8"));
                    BufferedReader br = new BufferedReader(reader);
                    String s = null;
                    while ((s = br.readLine()) != null) {// 使用readLine方法，一次读一行
                        projectJsonString.append(s);
                    }
                    br.close();
                    in.close();

                    // 解析上传内容，序列化后保存到数据库
                    ExportProject eproject = JSONObject.parseObject(projectJsonString.toString(), ExportProject.class);
                    /*增加版本验证*/
                    if (!EmptyUtil.isEmpty(eproject.getVersion()) && !EmptyUtil.isEmpty(exportSupportVersion)) {
                        boolean bol = VersionValidationUtil.packageProjectValidation(eproject.getVersion(), exportSupportVersion);
                        if (!bol) {
                            throw new ApiException(ErrorCode.IMPORT_VERSION_NOT_EQUALS);
                        }
                    }
                    Project projectSrc = projectService.getProjectById(eproject.getPid());
                    if (EmptyUtil.isNotEmpty(projectSrc)) {
                        return ApiResult.error(String.valueOf(ErrorCode.IMPORT_PROJECT_ID_EXISTS.getCode()),
                                getIntlString(ErrorCode.IMPORT_PROJECT_ID_EXISTS.getMessage()), projectSrc);
                    }
                    if (!hasProject2CoverFlag) {
                        if (StringUtils.isBlank(eproject.getPid())) {
                            throw new ApiException(ErrorCode.IMPORT_PROJECT_ID_EMPTY);
                        }
                        Project project = new Project();
                        BeanUtils.copyProperties(project, eproject);
                        projectService.addProject(project, getUserName());
                    }
                    List<ExportGroup> groups = eproject.getGroups();
                    List<GroupInfo> groupList = new ArrayList<>();
                    List<Unit> unitList = new ArrayList<>();
                    List<Part> partList = new ArrayList<>();
                    List<Partition> partitionList = new ArrayList<>();
                    List<AhuGroupBind> bindList = new ArrayList<>();
                    for (ExportGroup egroup : groups) {
                        if (StringUtils.isBlank(egroup.getGroupId())) {
                            throw new ApiException(ErrorCode.IMPORT_GROUP_ID_EMPTY);
                        }
                        GroupInfo group = new GroupInfo();
                        BeanUtils.copyProperties(group, egroup);
                        if (hasProject2CoverFlag) {
                            group.setProjectId(projectId);
                        }
                        groupList.add(group);
                        List<ExportUnit> units = egroup.getUnits();
                        for (ExportUnit eunit : units) {
                            if (StringUtils.isBlank(eunit.getUnitid())) {
                                throw new ApiException(ErrorCode.IMPORT_UNIT_ID_EMPTY);
                            }
                            Unit unit = new Unit();
                            BeanUtils.copyProperties(unit, eunit);
                            if (hasProject2CoverFlag) {
                                unit.setPid(projectId);
                            }
                            unitList.add(unit);

                            if (EmptyUtil.isNotEmpty(eunit.getPartition())) {
                                Partition partition = new Partition();
                                BeanUtils.copyProperties(partition, eunit.getPartition());
                                partition.setPartitionid(null); // clear partition id, let system to generate
                                if (hasProject2CoverFlag) {
                                    partition.setPid(projectId);
                                }
                                partitionList.add(partition);
                            }

                            AhuGroupBind bind = new AhuGroupBind();
                            bind.setUnitid(unit.getUnitid());
                            bind.setGroupid(unit.getGroupId());
                            bindList.add(bind);

                            List<ExportPart> parts = eunit.getExportParts();
                            for (ExportPart epart : parts) {
                                if (StringUtils.isBlank(epart.getPartid())) {
                                    throw new ApiException(ErrorCode.IMPORT_SECTION_ID_EMPTY);
                                }
                                Part part = new Part();
                                BeanUtils.copyProperties(part, epart);
                                if (hasProject2CoverFlag) {
                                    part.setPid(projectId);
                                }
                                partList.add(part);
                            }
                        }
                    }
                    List<ExportUnit> units = eproject.getUnits();
                    for (ExportUnit eunit : units) {
                        if (StringUtils.isBlank(eunit.getUnitid())) {
                            throw new ApiException(ErrorCode.IMPORT_UNIT_ID_EMPTY);
                        }
                        Unit unit = new Unit();
                        BeanUtils.copyProperties(unit, eunit);
                        if (hasProject2CoverFlag) {
                            unit.setPid(projectId);
                        }
                        unitList.add(unit);

                        if (EmptyUtil.isNotEmpty(eunit.getPartition())) {
                            Partition partition = new Partition();
                            BeanUtils.copyProperties(partition, eunit.getPartition());
                            partition.setPartitionid(null); // clear partition id, let system to generate
                            if (hasProject2CoverFlag) {
                                partition.setPid(projectId);
                            }
                            partitionList.add(partition);
                        }

                        List<ExportPart> parts = eunit.getExportParts();
                        for (ExportPart epart : parts) {
                            if (StringUtils.isBlank(epart.getPartid())) {
                                throw new ApiException(ErrorCode.IMPORT_SECTION_ID_EMPTY);
                            }
                            Part part = new Part();
                            BeanUtils.copyProperties(part, epart);
                            if (hasProject2CoverFlag) {
                                part.setPid(projectId);
                            }
                            partList.add(part);
                        }
                    }
                    // should be in the same transaction
                    groupService.addGroups(groupList);
                    ahuService.addOrUpdateAhus(unitList);
                    // should delete old parts from unit
                    unitList.forEach(unit -> {
                        sectionService.deleteAHUSections(unit.getUnitid());
                    });
                    sectionService.addSections(partList);
                    ahuGroupBindService.addBinds(bindList, getUserName());
                    partitionService.addOrUpdateAhus(partitionList);
                    if (hasProject2CoverFlag) {
                        ahuStatusTool.syncStatus(projectId, getUserName());
                    } else {
                        ahuStatusTool.syncStatus(eproject.getPid(), getUserName());
                    }

                    // check if project version in reset configuration
                    // then need to reset project status to selecting
                    if (EmptyUtil.isNotEmpty(exportResetVersion)) {
                        List<VersionPair> versionPairs = VersionUtils.parseVersionPairs(exportResetVersion);
                        if (VersionUtils.withinVersionPairs(eproject.getVersion(), versionPairs)) {
                            ahuStatusTool.resetStatus(hasProject2CoverFlag ? projectId : eproject.getPid(),
                                    getUserName());
                        }
                    }
                }
            }
        }
        return ApiResult.success(null);
    }

    /**
     * 项目导出
     *
     * @return
     */
    @RequestMapping(value = "export/proj/{projectId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> projExport(@PathVariable("projectId") String projectId, String filename)
            throws Exception {
        // Start generate export Project
        ExportProject eProject = new ExportProject();
        Project project = projectService.getProjectById(projectId);
        if (EmptyUtil.isEmpty(project)) {
            logger.error("Project is not exist, projectID:" + projectId);
            throw new ApiException(ErrorCode.PROJECT_IS_NOT_EXIST);
        }
        BeanUtils.copyProperties(eProject, project);
        List<GroupInfo> groupList = groupService.findGroupList(projectId);
        List<ExportGroup> groups = new ArrayList<>();
        /*遍历出使用的图片路径其中包括分组和不分组*/
        Map<String, String> pictureMap = new HashMap<>();
        for (GroupInfo group : groupList) {
            ExportGroup eGroup = new ExportGroup();
            BeanUtils.copyProperties(eGroup, group);
            List<Unit> ahuList = ahuService.findAhuList(projectId, group.getGroupId());
            List<ExportUnit> units = new ArrayList<>();
            for (Unit ahu : ahuList) {
                ExportUnit eAhu = new ExportUnit();
                BeanUtils.copyProperties(eAhu, ahu);
                List<Part> partList = sectionService.findSectionList(ahu.getUnitid());
                List<ExportPart> parts = new ArrayList<>();
                for (Part part : partList) {
                    ExportPart ePart = new ExportPart();
                    BeanUtils.copyProperties(ePart, part);
                    parts.add(ePart);
                    Map<String, String> metaMap = JSON.parseObject(part.getMetaJson(), HashMap.class);
                    String path = String.valueOf(metaMap.get(UtilityConstant.METASEXON_FAN_CURVE));
                    if (EmptyUtil.isNotEmpty(path)) {
                        pictureMap.put(path.replace(UtilityConstant.FILES, UtilityConstant.ASSERTS), path.replace(UtilityConstant.FILES, UtilityConstant.ASSERTS));
                    }
                }
                eAhu.setExportParts(parts);
                Partition partition = partitionService.findPartitionByAHUId(ahu.getUnitid());
                if (EmptyUtil.isNotEmpty(partition)) {
                    ExportPartition ePartition = new ExportPartition();
                    BeanUtils.copyProperties(ePartition, partition);
                    eAhu.setPartition(ePartition);
                }
                units.add(eAhu);
            }
            eGroup.setUnits(units);
            groups.add(eGroup);
        }
        List<Unit> ahuList = ahuService.findUngoupedAhuList(projectId);
        List<ExportUnit> units = new ArrayList<>();
        for (Unit ahu : ahuList) {
            ExportUnit eAhu = new ExportUnit();
            BeanUtils.copyProperties(eAhu, ahu);
            List<Part> partList = sectionService.findSectionList(ahu.getUnitid());
            List<ExportPart> parts = new ArrayList<>();
            for (Part part : partList) {
                ExportPart ePart = new ExportPart();
                BeanUtils.copyProperties(ePart, part);
                parts.add(ePart);
                Map<String, String> metaMap = JSON.parseObject(part.getMetaJson(), HashMap.class);
                String path = String.valueOf(metaMap.get(UtilityConstant.METASEXON_FAN_CURVE));
                if (EmptyUtil.isNotEmpty(path)) {
                    pictureMap.put(path.replace(UtilityConstant.FILES, UtilityConstant.ASSERTS), path.replace(UtilityConstant.FILES, UtilityConstant.ASSERTS));
                }
            }

            eAhu.setExportParts(parts);
            Partition partition = partitionService.findPartitionByAHUId(ahu.getUnitid());
            if (EmptyUtil.isNotEmpty(partition)) {
                ExportPartition ePartition = new ExportPartition();
                BeanUtils.copyProperties(ePartition, partition);
                eAhu.setPartition(ePartition);
            }
            units.add(eAhu);
        }
        eProject.setUnits(units);
        eProject.setGroups(groups);
        eProject.setVersion(ahuVersion);
        eProject.setPatchVersion(ahuPatchVersion);
        // Complete generate export Project

        String toWritttenStr = JSONObject.toJSONString(eProject);

        // generate project .prj file
        String projectExportFilePath = SysConstants.DIR_EXPORT + projectId + SysConstants.PRJ_EXTENSION;
        File projectExportFile = new File(projectExportFilePath);
        if (!projectExportFile.getParentFile().exists()) {
            projectExportFile.getParentFile().mkdirs();
        }
        if (!projectExportFile.exists()) {
            projectExportFile.createNewFile();
        }
        FileOutputStream o = new FileOutputStream(projectExportFile);
        o.write(toWritttenStr.getBytes(Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP)));
        o.close();

        /*删除多余文件*/
        deletePicture(pictureMap, projectId);

        // prepare generating zip file
        List<File> exportFiles = new ArrayList<>();
        exportFiles.add(projectExportFile);
        exportFiles.add(new File(RestConstant.SYS_PATH_BMP_YLD + projectId));
        exportFiles.add(new File(RestConstant.SYS_PATH_BMP_GK + projectId));
        exportFiles.add(new File(RestConstant.SYS_PATH_BMP_KRUGER + projectId));
        //exportFiles.add(new File(RestConstant.SYS_PATH_BMP_CAD + projectId));//暂时屏蔽导出三视图

        // generate zip file
        String zipFileName = "";
        String dateString = DateUtil.getStringDateYYMMDD();
        if (StringUtils.isBlank(filename)) {
            zipFileName = format(SysConstants.NAME_EXPORT_PROJECT, project.getPid(), dateString);
        } else {
            zipFileName = format(SysConstants.NAME_EXPORT_PROJECT,
                    new String(filename.getBytes(Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP)),
                            RestConstant.SYS_ENCODING_ISO_8859_1),
                    dateString);
        }
        String zipFilePath = SysConstants.DIR_EXPORT + zipFileName;
        File zipFile = new File(zipFilePath);
        if (zipFile.exists()) {
            zipFile.delete();
        }
        FileUtil.zipFiles(exportFiles, zipFilePath);

        return download(zipFilePath, zipFileName);
    }

    /**
     * 非标文件导出 add by gaok2
     *
     * @param projectId
     * @param filename
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "export/nsfile/{projectId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> nsFileExport(@PathVariable("projectId") String projectId, String filename) throws Exception {
        String fileName = "";
        String filePath = null;
        List<Part> partList = null;
        Partition partition = null;
        List<AhuPartition> partitions = null;
        List<Part> airFlowParts = null;
        List<ExportUnit> units = new ArrayList<>();

        // Start generate result
        List<Unit> unitList = ahuService.findAhuListByRecordStatus(projectId, SysConstants.RECORD_STATUS_SELECTED);
        if (EmptyUtil.isEmpty(unitList)) {
            logger.error("UnitList is not exist, projectId:" + projectId);
            throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
        }

        for (Unit unit : unitList) {
            ExportUnit eUnit = new ExportUnit();
            BeanUtils.copyProperties(eUnit, unit);
            partList = sectionService.findSectionList(unit.getUnitid());
            partition = partitionService.findPartitionByAHUId(unit.getUnitid());
            partitions = AhuPartitionUtils.parseAhuPartition(unit, partition);
            //由于双层机组排序不正确，这里要重新排序一下，后面导出需要根据空气流动顺序来导出
            airFlowParts = ExcelForPriceCodeUtils.getPriceCodesAirFlowParts(unit, partList, partitions);
            List<ExportPart> parts = new ArrayList<>();
            for (Part part : airFlowParts) {
                ExportPart ePart = new ExportPart();
                BeanUtils.copyProperties(ePart, part);
                parts.add(ePart);
            }
            eUnit.setExportParts(parts);
            eUnit.setVersion(ahuVersion);
            units.add(eUnit);
        }
        // Complete generate result

        if (StringUtils.isBlank(filename)) {
            fileName = format(SysConstants.NS_EXPORT_FILENAME, projectId);
        } else {
            fileName = format(SysConstants.NS_EXPORT_FILENAME,
                    new String(filename.getBytes(Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP)), RestConstant.SYS_ENCODING_ISO_8859_1));
        }
        filePath = SysConstants.NS_EXPORT_DIR + fileName;
        Excel4ModelInAndOutUtils.exportNsExcel(filePath, units, getLanguage());
        //标记版本信息
        VersionValidationUtil.markAtTemplate(filePath, fileName, ahuVersion);

        return download(filePath, fileName);
    }


    /**
     * 导入非标文件 add by gaok2
     *
     * @param request
     * @param projectId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "import/nsfile/{projectId}", method = RequestMethod.POST)
    public ApiResult nsFileImport(HttpServletRequest request, @PathVariable("projectId") String projectId) throws Exception {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
                request.getSession().getServletContext());

        //初始化返回给前端的实体对象
        TemplateImportNsFileRstVO resultvo = new TemplateImportNsFileRstVO();
        Map<String, String> returnMap = new HashMap<String, String>();
        returnMap.put("success", I18NBundle.getString(I18NConstants.IMPORT_SUCCESS_NS_FILE, getLanguage()));
        returnMap.put("exsitUnitPRB", I18NBundle.getString(I18NConstants.IMPORT_FAIL_NOT_EXIST_UNIT, getLanguage()));
        returnMap.put("exsitPartPRB", I18NBundle.getString(I18NConstants.IMPORT_FAIL_NOT_EXIST_PART, getLanguage()));

        if (!(multipartResolver.isMultipart(request))) {
            throw new ApiException(ErrorCode.REQUEST_DATA_ABNORMAL);
        }
        Project project = projectService.getProjectById(projectId);
        if (EmptyUtil.isEmpty(project)) {
            throw new ApiException(ErrorCode.PROJECT_IS_NOT_EXIST);
        }

        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
        Iterator<String> iter = multiRequest.getFileNames();
        while (iter.hasNext()) {
            String fileName = iter.next();
            MultipartFile f = multiRequest.getFile(fileName);
            if (EmptyUtil.isNotEmpty(f)) {
                if (!ExcelUtils.isExcelFileName(f.getOriginalFilename())) {
                    throw new ApiException(ErrorCode.WRONG_FILE_FORMAT);
                }
                InputStream in = f.getInputStream();
                /*增加版本控制------start------*/
                String version = VersionValidationUtil.readMarkFromTemplate(in, fileName);
                if (!EmptyUtil.isEmpty(version) && !EmptyUtil.isEmpty(supportVersionT)) {
                    boolean bol = VersionValidationUtil.TemplateValidation(version, supportVersionT);
                    if (!bol) {
                        throw new ApiException(ErrorCode.IMPORT_VERSION_NOT_EQUALS);
                    }
                }
                in = f.getInputStream();
                /*增加版本控制------end------*/
                List<ExportNsFile> exportNsFileList = Excel4ModelInAndOutUtils.importnsFileExcel(in, ExcelUtils.isExcel2003(fileName));
                if (EmptyUtil.isEmpty(exportNsFileList)) {
                    throw new ApiException(ErrorCode.RETURN_NULL_AFTER_EXCEL_ANALYSIS);
                }
                // 解析上传内容，序列化后保存到数据库
                String pid = project.getPid();
                Unit unit = null;
                String unitMetaJson = null;
                Part part = null;
                String sMemo = null;
                String sPrice = null;
                String sSegName = null;
                int start = -1;
                for(ExportNsFile exportNsFile : exportNsFileList){
//                    //机组编号判空
//                    if (EmptyUtil.isEmpty(exportNsFile.getUnitNo())) {
//                        String a = returnMap.get("UnitNoPRB");
//                        a += exportNsFile.getUnitNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//                        returnMap.put("UnitNoPRB", a);
//                        continue;
//                    }
//                    //功能序号判空
//                    if (EmptyUtil.isEmpty(exportNsFile.getSegNo())) {
//                        String a = returnMap.get("SegNoPRB");
//                        a += exportNsFile.getUnitNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//                        returnMap.put("SegNoPRB", a);
//                        continue;
//                    }

                    unit = ahuService.findAhuListByPIdAndUnitNo(pid,exportNsFile.getUnitNo());
                    if (EmptyUtil.isEmpty(unit)) {
                        String a = returnMap.get("exsitUnitPRB");
                        a += exportNsFile.getUnitNo() + RestConstant.SYS_PUNCTUATION_COMMA;
                        returnMap.put("exsitUnitPRB", a);
                        continue;
                    }


                    //如果功能段序号是0，则说明是箱体段，这个时候要更新机组表
                    if("0".equals(exportNsFile.getSegNo())){
                        //先查询机组信息
                        unitMetaJson = unit.getMetaJson();
                        Map<String, Object> umap = new HashMap<String, Object>();
                        if (StringUtils.isNotBlank(unitMetaJson)) {
                            umap = JSON.parseObject(unitMetaJson, Map.class);
                        }
                        umap.put(METANS_AHU_ID_MEMO, exportNsFile.getNsMemo());
                        umap.put(METANS_AHU_ID_PRICE, exportNsFile.getNsPrice()+"");
                        if(EmptyUtil.isNotEmpty(exportNsFile.getNsPrice()) || EmptyUtil.isNotEmpty(exportNsFile.getNsMemo())) {
                            umap.put(METANS_AHU_ID_ENABLE, true);
                        }
                        unit.setMetaJson(JSON.toJSONString(umap));

                        ahuService.updateAhu(unit, getUserName());
                    }else{
                        //如果功能段序号不是0，则说明是其他段信息，这个时候要更新功能段信息表
                        part = sectionService.findAhuListByUnitIdAndPosition(unit.getUnitid(), Short.parseShort(exportNsFile.getSegNo()));
                        if (EmptyUtil.isEmpty(part)) {
                            String a = returnMap.get("exsitPartPRB");
                            a += exportNsFile.getUnitNo() + RestConstant.SYS_PUNCTUATION_OR + exportNsFile.getSegNo() + RestConstant.SYS_PUNCTUATION_COMMA;
                            returnMap.put("exsitPartPRB", a);
                            continue;
                        }
                        String partSectionKey = part.getSectionKey();
                        Map<String, Object> pmap = JSON.parseObject(part.getMetaJson(), Map.class);
                        start = partSectionKey.lastIndexOf(UtilityConstant.SYS_PUNCTUATION_DOT);
                        sSegName = partSectionKey.substring(start + 1);
                        pmap.put(METANS_NS_SECTION + sSegName + METACOMMON_POSTFIX_PRICE, exportNsFile.getNsPrice()+"");
                        if(EmptyUtil.isNotEmpty(exportNsFile.getNsPrice()) || EmptyUtil.isNotEmpty(exportNsFile.getNsMemo())){
                            pmap.put(METANS_NS_SECTION + sSegName + METACOMMON_POSTFIX_ENABLE, true);
                        }
                        pmap.put(METANS_NS_SECTION + sSegName + METACOMMON_POSTFIX_MEMO, exportNsFile.getNsMemo());
                        part.setMetaJson(JSON.toJSONString(pmap));

                        sectionService.updateSection(part, getUserName());
                    }
                    List<Part> partList = sectionService.findSectionList(unit.getUnitid());
                    unit = calculatorTool.calculateAHUNsPrice(unit, partList);
                    ahuService.updateAhu(unit, getUserName());
                    String a = returnMap.get("success");
                    a += exportNsFile.getUnitNo() + RestConstant.SYS_PUNCTUATION_SLASH + exportNsFile.getSegNo() + RestConstant.SYS_PUNCTUATION_COMMA + "\r";
                    returnMap.put("success", a);
                }
            }
        }
        wrapNsFIleResultVO(returnMap, resultvo);

        return ApiResult.success(resultvo);
    }

    /*删除相关不使用图片*/
    private void deletePicture(Map pictureMap, String projectId) {
        String pathYLD = RestConstant.SYS_PATH_BMP_YLD + projectId;
        File fileYLD = new File(pathYLD);
        File[] filesYLD = fileYLD.listFiles();

        String pathGK = RestConstant.SYS_PATH_BMP_GK + projectId;
        File fileGK = new File(pathGK);
        File[] filesGK = fileGK.listFiles();

        String pathKRUGER = RestConstant.SYS_PATH_BMP_KRUGER + projectId;
        File fileKRUGER = new File(pathKRUGER);
        File[] filesKRUGER = fileKRUGER.listFiles();

        File[] fileAll = null;

        if (EmptyUtil.isNotEmpty(filesYLD)) {
            fileAll = ArrayUtils.addAll(filesYLD);
        }
        if (EmptyUtil.isNotEmpty(filesGK)) {
            fileAll = ArrayUtils.addAll(filesGK);
        }
        if (EmptyUtil.isNotEmpty(filesKRUGER)) {
            fileAll = ArrayUtils.addAll(filesKRUGER);
        }
        if (EmptyUtil.isNotEmpty(filesYLD) && EmptyUtil.isNotEmpty(filesGK)) {
            fileAll = ArrayUtils.addAll(filesYLD, filesGK);
        }
        if (EmptyUtil.isNotEmpty(filesYLD) && EmptyUtil.isNotEmpty(filesKRUGER)) {
            fileAll = ArrayUtils.addAll(filesYLD, filesKRUGER);
        }
        if (EmptyUtil.isNotEmpty(filesGK) && EmptyUtil.isNotEmpty(filesKRUGER)) {
            fileAll = ArrayUtils.addAll(filesGK, filesKRUGER);
        }
        if (EmptyUtil.isNotEmpty(filesYLD) && EmptyUtil.isNotEmpty(filesGK) && EmptyUtil.isNotEmpty(filesKRUGER)) {
            File[] YLDGK = (File[]) ArrayUtils.addAll(filesYLD, filesGK);
            fileAll = (File[]) ArrayUtils.addAll(YLDGK, filesKRUGER);
        }
        if (EmptyUtil.isNotEmpty(fileAll)) {
            String dir = System.getProperty("user.dir");
            for (File f : fileAll) {
                String path = f.getPath();
                if (path.contains("yld")) {
                    path = path + "\\" + SYS_NAME_YLD_BMP;
                } else if (path.contains("gk")) {
                    path = path + "\\" + SYS_NAME_GUKE_BMP;
                } else if (path.contains("kruger")) {
                    path = path + "\\" + SYS_NAME_KRUGER_BMP;
                }
                path = path.replaceAll("\\\\", "/");
                if (!pictureMap.containsKey(path)) {
                    CCRDFileUtil.deleteDirectory(dir + "\\" + path.replace(SYS_NAME_YLD_BMP, "").replace(SYS_NAME_GUKE_BMP, "").replace(SYS_NAME_KRUGER_BMP, "").replaceAll("/", "\\\\"));
                }
            }
        }
    }

    /**
     * AHU导出
     *
     * @return
     */
    @RequestMapping(value = "export/unit/{ahuId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> unitExport(@PathVariable("ahuId") String ahuId) throws Exception {
        Unit unit = ahuService.findAhuById(ahuId);
        if (EmptyUtil.isEmpty(unit)) {
            logger.error("ahu is not exist, ahuID:" + ahuId);
            throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
        }
        File unitExportFile = ahuService.exportUnit(unit, ahuVersion, ahuPatchVersion);
        return download(unitExportFile, unitExportFile.getName());
    }

    /**
     * AHU导入
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "import/unit", method = RequestMethod.POST)
    public ApiResult<String> unitImport(HttpServletRequest request, String ahuId) throws Exception {
        Unit selectedAhu = null;
        Partition selectedPartition = null;
        if (EmptyUtil.isNotEmpty(ahuId)) {
            selectedAhu = ahuService.findAhuById(ahuId);
            selectedPartition = partitionService.findPartitionByAHUId(ahuId);
            if (EmptyUtil.isEmpty(selectedAhu)) {
                throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
            }
            sectionService.deleteAHUSections(ahuId);
            if (EmptyUtil.isNotEmpty(selectedPartition)) {
                partitionService.deletePartition(selectedPartition.getPartitionid());
            }
        } else {
            throw new ApiException(ErrorCode.UNIT_NOT_EXISTS);
        }
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
                request.getSession().getServletContext());
        // 判断 request 是否有文件上传,即多部分请求
        if (multipartResolver.isMultipart(request)) {
            // 转换成多部分request
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            // 取得request中的所有文件名
            Iterator<String> iter = multiRequest.getFileNames();
            while (iter.hasNext()) {
                // 取得上传文件
                MultipartFile f = multiRequest.getFile(iter.next());
                if (EmptyUtil.isNotEmpty(f)) {
                    StringBuffer result = new StringBuffer();
                    FileInputStream in = (FileInputStream) f.getInputStream();
                    InputStreamReader reader = new InputStreamReader(in, Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP));
                    BufferedReader br = new BufferedReader(reader);
                    String s = null;
                    while ((s = br.readLine()) != null) {// 使用readLine方法，一次读一行
                        result.append(s);
                    }
                    br.close();
                    in.close();
                    // 解析上传内容，序列化后选择关键字段更新保存到数据库
                    List<Unit> unitList = new ArrayList<>();
                    List<Part> partList = new ArrayList<>();
                    List<Partition> partitionList = new ArrayList<>();

                    ExportUnit eunit = JSONObject.parseObject(result.toString(), ExportUnit.class);
                    //增加版本验证
                    if (!EmptyUtil.isEmpty(eunit.getVersion()) && !EmptyUtil.isEmpty(exportSupportVersion)) {
                        boolean bol = VersionValidationUtil.packageProjectValidation(eunit.getVersion(), exportSupportVersion);
                        if (!bol) {
                            throw new ApiException(ErrorCode.IMPORT_VERSION_NOT_EQUALS);
                        }
                    }
                    if (StringUtils.isBlank(eunit.getUnitid())) {
                        throw new ApiException(ErrorCode.IMPORT_UNIT_ID_EMPTY);
                    }

                    // 机组信息
                    Unit unit = new Unit();
                    BeanUtils.copyProperties(unit, eunit);
                    selectedAhu.setName(eunit.getName());
                    selectedAhu.setLayoutJson(eunit.getLayoutJson());
                    selectedAhu.setMetaJson(eunit.getMetaJson());
                    selectedAhu.setPrice(eunit.getPrice());
                    selectedAhu.setProduct(eunit.getProduct());
                    selectedAhu.setRecordStatus(eunit.getRecordStatus());
                    selectedAhu.setSeries(eunit.getSeries());
                    selectedAhu.setWeight(eunit.getWeight());
                    selectedAhu.setGroupCode(eunit.getGroupCode());
                    unitList.add(selectedAhu);

                    // 分段信息
                    ExportPartition partition = eunit.getPartition();
                    if (EmptyUtil.isNotEmpty(partition)) {
                        if (EmptyUtil.isEmpty(selectedPartition)) {
                            selectedPartition = new Partition();
                            BeanUtils.copyProperties(selectedPartition, partition);
                        } else {
                            selectedPartition.setMetaJson(partition.getMetaJson());
                            selectedPartition.setPartitionJson(partition.getPartitionJson());
                            selectedPartition.setRecordStatus(partition.getRecordStatus());
                        }
                        selectedPartition.setPartitionid(RestConstant.SYS_BLANK); // regenerate partition id
                        selectedPartition.setPid(selectedAhu.getPid());
                        selectedPartition.setUnitid(selectedAhu.getUnitid());
                        partitionList.add(selectedPartition);
                    }
                    User user = getUser();
                    // 段详细信息
                    List<ExportPart> parts = eunit.getExportParts();
                    for (ExportPart epart : parts) {
                        if (StringUtils.isBlank(epart.getPartid())) {
                            throw new ApiException(ErrorCode.IMPORT_SECTION_ID_EMPTY);
                        }
                        Part part = new Part();
                        BeanUtils.copyProperties(part, epart);
                        String partId = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
                        part.setPartid(partId);
                        part.setPid(selectedAhu.getPid());
                        part.setUnitid(selectedAhu.getUnitid());
                        partList.add(part);
                    }
                    ahuService.addOrUpdateAhus(unitList);
                    partitionService.addOrUpdateAhus(partitionList);
                    sectionService.addSections(partList);
                }
            }
        }
        return ApiResult.success(RestConstant.SYS_MSG_RESPONSE_SUCCESS);
    }

    /**
     * 分组导入<br>
     * 分组导入时，Excel中的所有AHU在数据库上新建，使用树形结构约束<br>
     * model 为1时即覆盖现有参数<br>
     * model 为0时，只新建AHU和段，对原有数据不做操作<br>
     *
     * @param request
     * @param projectId
     * @param groupId
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "import/group/{projectId}/{groupId}", method = RequestMethod.POST)
    public ApiResult<TemplateImportRstVO> groupImport(HttpServletRequest request, @PathVariable("projectId") String projectId,
                                                      @PathVariable("groupId") String groupId, String model) throws Exception {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
                request.getSession().getServletContext());

        //初始化返回给前端的实体对象
        TemplateImportRstVO resultvo = new TemplateImportRstVO();
//		String success = I18NBundle.getString(I18NConstants.IMPORT_SUCCESS, getLanguage());
//		String groupPRB = I18NBundle.getString(I18NConstants.IMPORT_FAIL_GROUP, getLanguage());
//		String existPRB = I18NBundle.getString(I18NConstants.IMPORT_FAIL_EXIST, getLanguage());
//		String tempPRB = I18NBundle.getString(I18NConstants.IMPORT_FAIL_EXIST, getLanguage());
        Map<String, String> returnMap = new HashMap<String, String>();
        returnMap.put("success", I18NBundle.getString(I18NConstants.IMPORT_SUCCESS, getLanguage()));
        returnMap.put("groupPRB", I18NBundle.getString(I18NConstants.IMPORT_FAIL_GROUP, getLanguage()));
        returnMap.put("existPRB", I18NBundle.getString(I18NConstants.IMPORT_FAIL_EXIST, getLanguage()));
        returnMap.put("tempPRB", I18NBundle.getString(I18NConstants.IMPORT_FAIL_TEMP, getLanguage()));
        returnMap.put("serialsPRB", I18NBundle.getString(I18NConstants.IMPORT_FAIL_SERIALS, getLanguage()));
        if (!(multipartResolver.isMultipart(request))) {
            throw new ApiException(ErrorCode.REQUEST_DATA_ABNORMAL);
        }
        Project project = projectService.getProjectById(projectId);
        if (EmptyUtil.isEmpty(project)) {
            throw new ApiException(ErrorCode.PROJECT_IS_NOT_EXIST);
        }
        GroupInfo group = groupService.findGroupById(groupId);
        if (EmptyUtil.isEmpty(group)) {
            throw new ApiException(ErrorCode.GROUP_IS_NOT_FOUND);
        }
        String groupType = group.getGroupType();
        String groupCode = group.getGroupCode();
        if (EmptyUtil.isEmpty(groupCode) && !RestConstant.SYS_STRING_GROUPTYPE_TP_0.equals(groupType)) {
            throw new ApiException(ErrorCode.GROUP_CODE_NOT_FOUND);
        }

        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
        Iterator<String> iter = multiRequest.getFileNames();
        while (iter.hasNext()) {
            String fileName = iter.next();
            MultipartFile f = multiRequest.getFile(fileName);
            if (EmptyUtil.isNotEmpty(f)) {
                if (!ExcelUtils.isExcelFileName(f.getOriginalFilename())) {
                    throw new ApiException(ErrorCode.WRONG_FILE_FORMAT);
                }
                InputStream in = f.getInputStream();
                /*增加版本控制------start------*/
                String version = VersionValidationUtil.readMarkFromTemplate(in, fileName);
                if (!EmptyUtil.isEmpty(version) && !EmptyUtil.isEmpty(supportVersionT)) {
                    boolean bol = VersionValidationUtil.TemplateValidation(version, supportVersionT);
                    if (!bol) {
                        throw new ApiException(ErrorCode.IMPORT_VERSION_NOT_EQUALS);
                    }
                }
                /*增加版本控制------end------*/
                in = f.getInputStream();
                List<ExportUnit> units = Excel4ModelInAndOutUtils.importExcelGroup(in, ExcelUtils.isExcel2003(fileName),
                        groupCode);
                if (EmptyUtil.isEmpty(units)) {
                    throw new ApiException(ErrorCode.RETURN_NULL_AFTER_EXCEL_ANALYSIS);
                }
                // 解析上传内容，序列化后保存到数据库
                String pid = project.getPid();
                if (model.equals(RestConstant.SYS_STRING_NUMBER_1)) {
                    for (ExportUnit eUnit : units) {
                        //机组系列判空
                        ValidatorFactory factory0 = new SerialsValidatorFactory();
                        ValidatorProduct product0 = (SerialsValidatorProduct) factory0.getValidator();
                        if (!product0.validate(eUnit, project, groupId)) {
                            String a = returnMap.get("serialsPRB");
                            a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
                            returnMap.put("serialsPRB", a);
                            continue;
                        }

                        //校验温度
                        ValidatorFactory factory = new TemperatureValidatorFactory();
                        ValidatorProduct product = (TemperatureValidatorProduct) factory.getValidator();
                        if (!product.validate(eUnit, project, groupId)) {//校验未通过
                            String a = returnMap.get("tempPRB");
                            a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
                            returnMap.put("tempPRB", a);
                            continue;
                        }
                        //分组校验
                        ValidatorFactory factory2 = new GroupValidatorFactory();
                        ValidatorProduct product2 = (GroupValidatorProduct) factory2.getValidator(ahuService);
                        Map<String, Object> rstMap2 = product2.validate1(eUnit, project, groupId);
                        takeAction(rstMap2, eUnit, pid, groupId, groupCode, returnMap);

//						String drawingNo = eUnit.getDrawingNo();
//						// Unit unitBefore = ahuService.findAhuByDrawingNO(pid, drawingNo);
//						Unit unitBefore = ahuService.findAhuByDrawingNOAndGroupId(pid, drawingNo, groupId);
//						if (null == unitBefore) {
//							if (EmptyUtil.isEmpty(eUnit.getDrawingNo())) {// 机组编号为空就不新增
//								continue;
//							}
//							Unit unitBefore1 = ahuService.findAhuByDrawingNO(pid, drawingNo);
//							if (eUnit.getDrawingNo().equals(unitBefore1!=null?unitBefore1.getDrawingNo():null)) {//机组编号已存在于其他分组，不新增
//								groupPRB += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//								continue;
//							}
//							importAddNewAhu(eUnit, pid, groupId, groupCode);
//							success += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//						} else {
//							if (EmptyUtil.isEmpty(eUnit.getDrawingNo())) {// 机组编号为空就不覆盖
//								continue;
//							}
//							//清空覆盖所有参数后，老unit 价格、重量
//							unitBefore.setPrice(0.0);
//							unitBefore.setWeight(0.0);
//							importCoverAhu(eUnit, unitBefore, pid, groupId, groupCode);
//							success += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//						}
                    }
                } else {
                    for (ExportUnit eUnit : units) {
                        //机组系列判空
                        ValidatorFactory factory0 = new SerialsValidatorFactory();
                        ValidatorProduct product0 = (SerialsValidatorProduct) factory0.getValidator();
                        if (!product0.validate(eUnit, project, groupId)) {
                            String a = returnMap.get("serialsPRB");
                            a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
                            returnMap.put("serialsPRB", a);
                            continue;
                        }
                        //校验温度
                        ValidatorFactory factory = new TemperatureValidatorFactory();
                        ValidatorProduct product = (TemperatureValidatorProduct) factory.getValidator();
                        if (!product.validate(eUnit, project, groupId)) {//校验未通过
                            String a = returnMap.get("tempPRB");
                            a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
                            returnMap.put("tempPRB", a);
                            continue;
                        }
                        //分组校验
                        ValidatorFactory factory3 = new GroupValidatorFactory();
                        ValidatorProduct product3 = (GroupValidatorProduct) factory3.getValidator(ahuService);
                        Map<String, Object> rstMap3 = product3.validate2(eUnit, project, groupId);
                        takeAction(rstMap3, eUnit, pid, groupId, groupCode, returnMap);
//						String drawingNo = eUnit.getDrawingNo();
//						Unit unitBefore = ahuService.findAhuByDrawingNOAndGroupId(pid, drawingNo, groupId);
//						Unit unitBefore1 = ahuService.findAhuByDrawingNO(pid, drawingNo);
//						if (EmptyUtil.isEmpty(eUnit.getDrawingNo())) {// 机组编号为空 或者机组编号已存在就不新增
//							continue;
//						}else if ((eUnit.getDrawingNo()).equals(unitBefore != null ? unitBefore.getDrawingNo() : null)) {//机组编号已存在于本组
//							existPRB += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//							continue;
//						}else if ((eUnit.getDrawingNo()).equals(unitBefore1 != null ? unitBefore1.getDrawingNo() : null)) {//机组编号已存在于其他组
//							groupPRB += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
//							continue;
//						}
//						importAddNewAhu(eUnit, pid, groupId, groupCode);
//						success += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
                    }
                }
            }
        }
        ahuStatusTool.syncStatus(projectId, getUserName());
//		return ApiResult.success(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1);
        wrapResultVO(returnMap, resultvo);

        return ApiResult.success(resultvo);
    }

    /**
     * 根据校验结果进行处理
     */
    private void takeAction(Map validationMap, ExportUnit eUnit, String pid, String groupId, String groupCode, Map returnMap)
            throws Exception {
        String action = (String) validationMap.get(RestConstant.SYS_STRING_ACTION);
        String result = (String) validationMap.get(RestConstant.SYS_STRING_RESULT);
        Unit unitBefore = ahuService.findAhuByDrawingNOAndGroupId(pid, eUnit.getDrawingNo(), groupId);
        if (RestConstant.SYS_STRING_ACTION_ADD.equals(action)) {
            importAddNewAhu(eUnit, pid, groupId, groupCode);
            String temp = String.valueOf(returnMap.get(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1));
            temp += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
            returnMap.put(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1, temp);
        } else if (RestConstant.SYS_STRING_ACTION_COVER.equals(action)) {
            unitBefore.setPrice(0.0);
            unitBefore.setWeight(0.0);
            importCoverAhu(eUnit, unitBefore, pid, groupId, groupCode);
            String temp = String.valueOf(returnMap.get(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1));
            temp += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
            returnMap.put(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1, temp);
        } else if (RestConstant.SYS_STRING_ACTION_SKIP.equals(action)) {
//			String tempRst = String.valueOf(returnMap.get(RestConstant.SYS_STRING_RESULT));
            if ("groupPRB".equals(result)) {
                String a = String.valueOf(returnMap.get("groupPRB"));
                a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
                returnMap.put("groupPRB", a);
            } else if ("existPRB".equals(result)) {
                String a = String.valueOf(returnMap.get("existPRB"));
                a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
                returnMap.put("existPRB", a);
            } else if ("tempPRB".equals(result)) {
                String a = String.valueOf(returnMap.get("tempPRB"));
                a += eUnit.getDrawingNo() + RestConstant.SYS_PUNCTUATION_COMMA;
                returnMap.put("tempPRB", a);
            }

        }
    }

    /**
     * 封装返回给前端的vo对象
     *
     * @param returnMap
     * @param resultvo
     */
    private void wrapResultVO(Map returnMap, TemplateImportRstVO resultvo) {
        String success = String.valueOf(returnMap.get(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1));
        String groupPRB = String.valueOf(returnMap.get("groupPRB"));
        String existPRB = String.valueOf(returnMap.get("existPRB"));
        String tempPRB = String.valueOf(returnMap.get("tempPRB"));
        String serialsPRB = String.valueOf(returnMap.get("serialsPRB"));

        // 若最后有逗号，就去掉最后的逗号
        resultvo.setSuccess(success.substring(0,
                (success.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
                        ? success.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
                        : success.length())));
        resultvo.setGroupPRB(groupPRB.substring(0,
                (groupPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
                        ? groupPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
                        : groupPRB.length())));
        resultvo.setExistPRB(existPRB.substring(0,
                (existPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
                        ? existPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
                        : existPRB.length())));
        resultvo.setTempPRB(tempPRB.substring(0,
                (tempPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
                        ? tempPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
                        : tempPRB.length())));
        resultvo.setSerialsPRB(serialsPRB.substring(0,
                (serialsPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
                        ? serialsPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
                        : serialsPRB.length())));
    }

    /**
     * 封装返回给前端的vo对象
     *
     * @param returnMap
     * @param resultvo
     */
    private void wrapNsFIleResultVO(Map returnMap, TemplateImportNsFileRstVO resultvo) {
        String success = String.valueOf(returnMap.get(RestConstant.SYS_MSG_RESPONSE_SUCCESS_1));
        String existUnitPRB = String.valueOf(returnMap.get("existUnitPRB"));
        String existPartPRB = String.valueOf(returnMap.get("existPartPRB"));

        // 若最后有逗号，就去掉最后的逗号
        resultvo.setSuccess(success.substring(0,
                (success.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
                        ? success.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
                        : success.length())));
        resultvo.setExistUnitPRB(existUnitPRB.substring(0,
                (existUnitPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
                        ? existUnitPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
                        : existUnitPRB.length())));
        resultvo.setExistPartPRB(existPartPRB.substring(0,
                (existPartPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA) > 0
                        ? existPartPRB.lastIndexOf(RestConstant.SYS_PUNCTUATION_COMMA)
                        : existPartPRB.length())));
    }

    /**
     * 分组导出
     *
     * @return
     */
    @RequestMapping(value = "export/group/{projectId}/{groupId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> groupExport(@PathVariable("projectId") String projectId,
                                              @PathVariable("groupId") String groupId, String filename) throws Exception {

        // 获取全局配置参数：导出模式 0-全量 1-配置
        String exportMode = new UserConfigParam().getExportMode();
        // 默认为根据配置导出
        if (EmptyUtil.isEmpty(exportMode)) {
            exportMode = RestConstant.SYS_STRING_NUMBER_1;
        }

        // Start generate result
        ExportProject eProject = new ExportProject();
        Project project = projectService.getProjectById(projectId);
        if (EmptyUtil.isEmpty(project)) {
            logger.error("Project is not exist, projectID:" + projectId);
            throw new ApiException(ErrorCode.PROJECT_IS_NOT_EXIST);
        }
        BeanUtils.copyProperties(eProject, project);
        GroupInfo group = groupService.findGroupById(groupId);
        if (EmptyUtil.isEmpty(group)) {
            logger.error("GroupInfo is not exist, groupId:" + groupId);
            throw new ApiException(ErrorCode.GROUP_IS_NOT_FOUND);
        }
        ExportGroup eGroup = new ExportGroup();
        BeanUtils.copyProperties(eGroup, group);
        List<Unit> unitList = ahuService.findAhuList(projectId, groupId);
        if (EmptyUtil.isEmpty(unitList)) {
            logger.error("UnitList is not exist, groupId:" + groupId);
            throw new ApiException(ErrorCode.NO_AHU_IN_GROUP);
        }
        List<ExportUnit> units = new ArrayList<>();
        for (Unit unit : unitList) {
            ExportUnit eUnit = new ExportUnit();
            BeanUtils.copyProperties(eUnit, unit);
            List<Part> partList = sectionService.findSectionList(unit.getUnitid());
            if (EmptyUtil.isEmpty(partList)) {
                logger.error("PartList is not exist, unitid:" + unit.getUnitid());
                throw new ApiException(ErrorCode.NO_SECTION_UNDER_UNIT);
            }
            List<ExportPart> parts = new ArrayList<>();
            for (Part part : partList) {
                ExportPart ePart = new ExportPart();
                BeanUtils.copyProperties(ePart, part);
                ePart.setMetaJson(filter4MetaJson(part.getMetaJson(), TemplateUtil.getPerformanceExportKeyList()));
                parts.add(ePart);
            }
            eUnit.setMetaJson(filter4MetaJson(unit.getMetaJson(), TemplateUtil.getPerformanceExportKeyList()));
            eUnit.setExportParts(parts);
            eUnit.setVersion(ahuVersion);
            units.add(eUnit);
        }
        eGroup.setUnits(units);
        List<ExportGroup> groups = new ArrayList<>();
        groups.add(eGroup);
        eProject.setGroups(groups);
        eProject.setVersion(ahuVersion);
        // Complete generate result

        String fileName = "";
        if (StringUtils.isBlank(filename)) {
            fileName = format(SysConstants.NAME_EXPORT_GROUP, group.getGroupId());
        } else {
            fileName = format(SysConstants.NAME_EXPORT_GROUP,
                    new String(filename.getBytes(Charset.forName(RestConstant.SYS_ENCODING_UTF8_UP)), RestConstant.SYS_ENCODING_ISO_8859_1));
        }

        String filePath = SysConstants.DIR_EXPORT + fileName;

        if (RestConstant.SYS_STRING_NUMBER_0.equals(exportMode)) {
            Excel4ModelInAndOutUtils.exportExcelGroup(filePath, eProject, getLanguage());
        } else if (RestConstant.SYS_STRING_NUMBER_1.equals(exportMode)) {
            Excel4ModelInAndOutUtils.exportExcelGroupByTemplet(filePath, eProject, getLanguage());// 读取导入模板文件，将数据填充到模板中，并导出文件。
        }
        /*标记版本信息*------start------*/
        VersionValidationUtil.markAtTemplate(filePath, fileName, ahuVersion);
        /*标记版本信息*------end------*/
        return download(filePath, fileName);
    }

    /**
     * 未分组导入
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "import/ungroup/{projectId}", method = RequestMethod.POST)
    public ApiResult<String> unGroupImport(HttpServletRequest request, @PathVariable("projectId") String projectId)
            throws Exception {
        return ApiResult.success("developing......");
    }

    @SuppressWarnings("unchecked")
    private static String filter4MetaJson(String resourceJson, List<String> keepKeys) {
        try {
            Map<String, String> m = JSON.parseObject(resourceJson, Map.class);
            Map<String, String> rm = new HashMap<>();
            for (String key : keepKeys) {
                if (m.containsKey(key)) {
                    rm.put(key, Excel4ModelInAndOutUtils.getStringValue(m.get(key)));
                }
            }
            String returnString = JSON.toJSONString(rm);
            return returnString;
        } catch (Exception e) {
            return resourceJson;
        }
    }

    @SuppressWarnings("unchecked")
    private void importAddNewAhu(ExportUnit eUnit, String pid, String groupId, String groupCode) throws Exception {
        User user = getUser();
        Unit unit = new Unit();
        String sVolume = "";/* 获取机组送风风量 */
        String eVolume = "";/* 获取机组回风风量 */
        String sAirVolume = RestConstant.METAHU_SAIRVOLUME;// 机组送风风量
        String eAirVolume = RestConstant.METAHU_EAIRVOLUME;// 机组回风风量
        BeanUtils.copyProperties(unit, eUnit);
        String unitid = KeyGenerator.genUnitId(user.getUnitPreferCode(), user.getUserId());
        unit.setUnitid(unitid);
        unit.setProduct(eUnit.getProduct());
        unit.setPid(pid);
        unit.setGroupId(groupId);
        unit.setMount(eUnit.getMount());
        unit.setName(eUnit.getName());
        unit.setGroupCode(groupCode);
        AhuLayout layout = new AhuLayout();
        GroupInfo group = groupService.findGroupById(groupId);
        // 将group code转为layout

        GroupTypeEnum groupType = GroupTypeEnum.valueIdOf(group.getGroupType());
        if (GroupTypeEnum.TYPE_CUSTOM.equals(groupType)) {
            if (EmptyUtil.isEmpty(group.getExt1())) {
                layout.setStyle(LayoutStyleEnum.COMMON.style());
                layout.setLayoutData(LayoutStyleEnum.COMMON.getInitLayoutData(group.getSectionsNum()));
            } else {
                unit.setLayoutJson(group.getExt1());
            }
        } else {
            unit.setLayoutJson(TemplateUtil.getTemplateLayoutString(group.getGroupType()));
        }
        unit.setRecordStatus(AhuStatusEnum.IMPORT_COMPLETE.getId());
        unit = TemplateUtil.getTemplateUnit(unit);//此处导入unit属性，并对unit机组进行默认值设置。

        //如果回风风量为空，则将回风量默认设为送风量
        String eVolume1 = "";
        String sVolume1 = "";
        Map<String, String> tempMap = JSON.parseObject(unit.getMetaJson(), HashMap.class);
        Map<String, String> tempSourceMap = JSON.parseObject(eUnit.getMetaJson(), HashMap.class);
        if (tempSourceMap != null && tempSourceMap.containsKey(UtilityConstant.METAHU_EAIRVOLUME)) {
            eVolume1 = BaseDataUtil.constraintString(tempSourceMap.get(UtilityConstant.METAHU_EAIRVOLUME));
        }
        if (tempSourceMap != null && tempSourceMap.containsKey(UtilityConstant.METAHU_SAIRVOLUME)) {
            sVolume1 = BaseDataUtil.constraintString(tempSourceMap.get(UtilityConstant.METAHU_SAIRVOLUME));
        }
        if (EmptyUtil.isEmpty(eVolume1) || UtilityConstant.SYS_STRING_NUMBER_0.equals(eVolume1)) {
            tempMap.put(UtilityConstant.METAHU_EAIRVOLUME, sVolume1);
        }
        unit.setMetaJson(JSON.toJSONString(tempMap));

        // List<String> ss = ahuService.findAllUnitNOInuse(pid);
        // String unitNo = ValueFormatUtil.getUsefulUnitNO(ss, -1);
        // unit.setUnitNo(unitNo);
        unit.setUnitNo(eUnit.getDrawingNo());
        Map<String, String> unitMap = JSON.parseObject(unit.getMetaJson(), HashMap.class);
        if (unitMap.containsKey(sAirVolume)) {
            sVolume = BaseDataUtil.constraintString(unitMap.get(sAirVolume));// 获取机组送风风量
        }
        if (unitMap.containsKey(eAirVolume)) {
            eVolume = BaseDataUtil.constraintString(unitMap.get(eAirVolume));// 获取机组回风风量
        }
        ahuService.addAhu(unit, getUserName());

        AhuGroupBind bind = new AhuGroupBind();
        bind.setUnitid(unitid);
        bind.setGroupid(groupId);
        ahuGroupBindService.addBind(bind, getUserName());

        List<ExportPart> parts = eUnit.getExportParts();
        List<Part> partList = new ArrayList<>();
        for (ExportPart ePart : parts) {
            Part part = new Part();
            BeanUtils.copyProperties(part, ePart);
            String partid = KeyGenerator.genPartId(user.getUnitPreferCode(), user.getUserId());
            part.setPartid(partid);
            part.setPid(pid);
            part.setUnitid(unitid);
            Map<String, Object> mapTemplate = new HashMap<>(TemplateUtil.genAhuSectionTemplate(ePart.getSectionKey()));
            removeNullEntry(mapTemplate);
            Map<String, String> map = JSON.parseObject(ePart.getMetaJson(), HashMap.class);
            /* 如果风机段风量字段为空默认使用机组送风风量 */
            if (SectionTypeEnum.TYPE_FAN.getId().equals(ePart.getSectionKey())) {
                String fanAirVolume = RestConstant.METASEXON_FAN_AIRVOLUME;

                if (!map.containsKey(fanAirVolume)) {// 如果风机字段没有定义风量，风机风量默认使用机组风量
                    map.put(fanAirVolume, sVolume);
                }
                /*
                 * String airDirection = "meta.section.airDirection";
                 *//* 如果风向是回风风向 *//*
                 * if(map.containsKey(airDirection)){ String airDirectionValue =
                 * BaseDataUtil.constraintString(airDirection);
                 * if(AirDirectionEnum.RETURNAIR.getCode().equals(airDirectionValue)){
                 * if(!map.containsKey(fanAirVolume)){//如果风机字段没有定义风量，风机风量默认使用机组风量
                 * map.put(fanAirVolume,eVolume); } }else{
                 * if(!map.containsKey(fanAirVolume)){//如果风机字段没有定义风量，风机风量默认使用机组风量
                 * map.put(fanAirVolume,sVolume); } } }
                 */
            }
            //如果是单层过滤段
            if (SectionTypeEnum.TYPE_SINGLE.getId().equals(ePart.getSectionKey())) {
                String filerEfficiency = RestConstant.METASEXON_FILTER_FILTEREFFICIENCY;

                if (map.containsKey(filerEfficiency)) {
                    map.put(RestConstant.METASEXON_FILTER_FILTERSTANDARD, RestConstant.SYS_ASSERT_ALL);
                }

                //如果过滤器形式是外抽，则抽取方式强制为侧抽
                String filterF = map.get(UtilityConstant.METASEXON_FILTER_FITETF);
                if ("X".equals(filterF)) {
                    map.put(UtilityConstant.METASEXON_FILTER_MEDIALOADING, UtilityConstant.SYS_STRING_MEDIALOADING_SIDELOADING);
                }
            }
            //如果是综合过滤段
            if (SectionTypeEnum.TYPE_COMPOSITE.getId().equals(ePart.getSectionKey())) {
                String filterF = RestConstant.METASEXON_COMBINEDFILTER_FITETF;

                if (map.containsKey(filterF)) {
                    map.put(RestConstant.METASEXON_COMBINEDFILTER_FILTERSTANDARD, RestConstant.SYS_ASSERT_ALL);
                }
            }

            //热转轮回收
            if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(ePart.getSectionKey())) {
                String heatrecycledetail = RestConstant.METASEXON_WHEELHEATRECYCLE_HEATRECYCLEDETAIL;

                if (!map.containsKey(heatrecycledetail)) {
                    map.put(RestConstant.METASEXON_WHEELHEATRECYCLE_HEATRECYCLEDETAIL, JSON.toJSONString(new ArrayList<HeatRecycleInfo>()));
                }
            }

            //板式回收
            if (SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(ePart.getSectionKey())) {
                String heatrecycledetail = RestConstant.METASEXON_PLATEHEATRECYCLE_HEATRECYCLEDETAIL;

                if (!map.containsKey(heatrecycledetail)) {
                    map.put(RestConstant.METASEXON_PLATEHEATRECYCLE_HEATRECYCLEDETAIL, JSON.toJSONString(new ArrayList<HeatRecycleInfo>()));
                }
            }

            removeNullEntry(map);// 过滤excel中的空数据
            mapTemplate.putAll(map);
            part.setMetaJson(JSON.toJSONString(mapTemplate));
            partList.add(part);
        }
        sectionService.addSections(partList);
    }

    @SuppressWarnings("unchecked")
    private void importCoverAhu(ExportUnit eUnit, Unit unitbefore, String pid, String groupId, String groupCode)
            throws Exception {
        unitbefore.setRecordStatus(AhuStatusEnum.IMPORT_COMPLETE.getId());
        Map<String, String> beforeUnitMap = JSON.parseObject(unitbefore.getMetaJson(), HashMap.class);
        unitbefore.setName(eUnit.getName());
        unitbefore.setProduct(eUnit.getProduct());
        unitbefore.setMount(eUnit.getMount());
        if (EmptyUtil.isEmpty(beforeUnitMap)) {
            beforeUnitMap = new HashMap<>();
        }
        Map<String, String> eUnitMap = JSON.parseObject(eUnit.getMetaJson(), HashMap.class);
        if (EmptyUtil.isNotEmpty(eUnitMap)) {
            beforeUnitMap.putAll(eUnitMap);
        }
        unitbefore.setMetaJson(JSON.toJSONString(beforeUnitMap));

        List<Part> poList = sectionService.findSectionList(unitbefore.getUnitid());
        List<ExportPart> parts = eUnit.getExportParts();

        List<Part> toUpdateList = new ArrayList<>();
        for (Part part : poList) {
            String sectionKey = part.getSectionKey();
            Part imptPart = getEPart(sectionKey, parts);
            if (null == imptPart) {
                continue;
            } else {
                Map<String, String> mapSource = JSON.parseObject(part.getMetaJson(), HashMap.class);
                Map<String, String> map = JSON.parseObject(imptPart.getMetaJson(), HashMap.class);
                logic4MapSource(map, sectionKey);
                mapSource.putAll(map);
                mapSource.put(RestConstant.METASEXON_COMPLETED, RestConstant.SYS_BLANK);//设置为正在选型状态
                part.setMetaJson(JSON.toJSONString(mapSource));
                part.setRecordStatus(AhuStatusEnum.SELECTING.getId());
                toUpdateList.add(part);
            }
        }
        ahuService.updateAhu(unitbefore, getUserName());
        sectionService.updateSections(toUpdateList);
    }

    /**
     * 导入数据添加逻辑
     *
     * @param map
     * @param sectionKey
     */
    private void logic4MapSource(Map<String, String> map, String sectionKey) {
        //单层过滤段
        if ("ahu.filter".equals(sectionKey)) {
            //如果过滤器形式是外抽，则抽取方式强制为侧抽
            String filterF = map.get(UtilityConstant.METASEXON_FILTER_FITETF);
            if ("X".equals(filterF)) {
                map.put(UtilityConstant.METASEXON_FILTER_MEDIALOADING, UtilityConstant.SYS_STRING_MEDIALOADING_SIDELOADING);
            }
        }
    }

    private static Part getEPart(String sectionKey, List<ExportPart> parts)
            throws IllegalAccessException, InvocationTargetException {
        for (ExportPart ePart : parts) {
            if (sectionKey.equals(ePart.getSectionKey())) {
                Part part = new Part();
                BeanUtils.copyProperties(part, ePart);
                part.setMetaJson(ePart.getMetaJson());
                return part;
            }
        }
        return null;
    }

    private static String format(String pattern, Object... arguments) {
        return MessageFormat.format(pattern, arguments);
    }
}

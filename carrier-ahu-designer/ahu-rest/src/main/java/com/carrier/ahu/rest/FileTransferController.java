package com.carrier.ahu.rest;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.util.Base64Utils;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.ApiResult.ErrorCodeEnum;
import com.carrier.ahu.vo.FileNamesLoadInSystem;

import io.swagger.annotations.Api;

@Api(description = "文件接口")
@RestController
public class FileTransferController extends AbstractController {
	@Autowired
	AhuService ahuService;
	@Autowired
	PartitionService partitionService;

	@RequestMapping(value = "/panel/img/upload/check", method = RequestMethod.POST)
	public ApiResult<Object> uploadCheck(String unitid) {
		Unit unit = ahuService.findAhuById(unitid);
		Partition partition = partitionService.findPartitionByAHUId(unitid);
		String partitionid = partition.getPartitionid();
		List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit,partition);

		if (EmptyUtil.isEmpty(unitid)) {
			throw new ApiException(ErrorCode.UNIT_CODE_IS_EMPTY);
		}
		JSONObject retObj = new JSONObject();
		for (AhuPartition ahuPartition : ahuPartitionList) {
			String filePath = MessageFormat.format(FileNamesLoadInSystem.PANEL_IMG_PATH, unitid);
			String fileName = partitionid + (ahuPartition.getPos()+1) + RestConstant.SYS_PNG_EXTENSION;
			String fullPath = filePath + File.separator + fileName;
			logger.info("图片校验路径={}", fullPath);
			File destFile = new File(fullPath);

			if(destFile.exists()){
				retObj.put(""+(ahuPartition.getPos()+1),"true");
			}else{
				retObj.put(""+(ahuPartition.getPos()+1),"false");
			}
		}
		return ApiResult.success(retObj);
	}
	@RequestMapping(value = "/panel/img/upload/base64", method = RequestMethod.POST)
	public ApiResult<Object> uploadPoster(HttpServletRequest request, HttpServletResponse response) {

		String unitid = request.getParameter(RestConstant.SYS_MSG_REQUEST_UNITID);
		String partitionid = request.getParameter(RestConstant.SYS_MSG_REQUEST_PARTITIONID);
		String base64Img = request.getParameter(RestConstant.SYS_MSG_REQUEST_BASE64);
		String pNumber = request.getParameter(RestConstant.SYS_MSG_REQUEST_PARTITIONNUMBER);//分段编号

		if (EmptyUtil.isEmpty(unitid)) {
		    throw new ApiException(ErrorCode.UNIT_CODE_IS_EMPTY);
		}
		if (EmptyUtil.isEmpty(partitionid)) {
		    throw new ApiException(ErrorCode.SECTION_CODE_IS_EMPTY);
		}
		if (EmptyUtil.isEmpty(base64Img)) {
		    throw new ApiException(ErrorCode.IMAGE_DATA_IS_EMPTY);
		}
		if (EmptyUtil.isEmpty(pNumber)) {
		    throw new ApiException(ErrorCode.PARTITION_NUMBER_IS_EMPTY);
		}

		if (base64Img.trim().startsWith(RestConstant.SYS_IMG_DATA_START)) {
		    throw new ApiException(ErrorCode.IMAGE_DATA_FORMAT_ILLEGAL);
		}
		base64Img = base64Img.trim().substring(22);

		String filePath = MessageFormat.format(FileNamesLoadInSystem.PANEL_IMG_PATH, unitid);
		logger.info("图片保存路径={}", filePath);
		String fileName = partitionid + pNumber + RestConstant.SYS_PNG_EXTENSION;
		String fullPath = filePath + File.separator + fileName;

		try {
			Base64Utils.decodeToFile(fullPath, base64Img);
		} catch (Exception e) {
			logger.error("转化保存图片失败", e);
			throw new ApiException(ErrorCode.CONVERT_SAVING_IMAGE_FAILURE);
		}

		Map<String, Object> result = new HashMap<>();
		result.put(RestConstant.SYS_MSG_RESPONSE_FILEPATH, fullPath);
		return ApiResult.success(result);
	}

	@PutMapping("/panel/img/upload")
	public ApiResult<Object> uploadImg(@RequestParam("editormd-image-file") MultipartFile multipartFile, String unitid,
			String partitionid) {
		if (multipartFile.isEmpty() || EmptyUtil.isEmpty(multipartFile.getOriginalFilename())) {
		    throw new ApiException(ErrorCode.UPLOAD_FILE_IS_EMPTY);
		}
		if (EmptyUtil.isEmpty(unitid)) {
		    throw new ApiException(ErrorCode.UNIT_CODE_IS_EMPTY);
		}
		if (EmptyUtil.isEmpty(partitionid)) {
		    throw new ApiException(ErrorCode.SECTION_CODE_IS_EMPTY);
		}
		String contentType = multipartFile.getContentType();
		if (!contentType.contains("")) {
		    throw new ApiException(ErrorCode.FILE_TYPE_EXCEPTION);
		}
		String root_fileName = multipartFile.getOriginalFilename();
		logger.info("上传图片:name={},type={}", root_fileName, contentType);
		// 获取路径
		String filePath = MessageFormat.format(FileNamesLoadInSystem.PANEL_IMG_PATH, unitid);
		logger.info("图片保存路径={}", filePath);
		String file_name = null;
		try {
			file_name = saveImg(multipartFile, filePath, partitionid);

			Map<String, Object> map = new HashMap<>();
			map.put(RestConstant.SYS_MSG_RESPONSE_TYPE, ErrorCodeEnum.ERROR.getCode());
			if (EmptyUtil.isNotEmpty(file_name)) {
				map.put(RestConstant.SYS_MSG_RESPONSE_TYPE, ErrorCodeEnum.SUCCESS.getCode());
				map.put(RestConstant.SYS_MSG_RESPONSE_MESSAGE, getIntlString(I18NConstants.UPLOAD_SUCCESS));
				map.put(RestConstant.SYS_MSG_RESPONSE_URL, filePath + File.separator + file_name);
			}
			logger.info("返回值：{0}", map);
			return ApiResult.success(map);
		} catch (IOException e) {
		    throw new ApiException(ErrorCode.FILE_SAVING_ERROR);
		}
	}

	/**
	 * 保存文件，直接以multipartFile形式
	 * 
	 * @param multipartFile
	 * @param path
	 *            文件保存绝对路径
	 * @return 返回文件名
	 * @throws IOException
	 */
	public static String saveImg(MultipartFile multipartFile, String path, String partitionId) throws IOException {
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
		FileInputStream fileInputStream = (FileInputStream) multipartFile.getInputStream();
		String fileName = partitionId + RestConstant.SYS_BMP_EXTENSION;
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path + File.separator + fileName));
		byte[] bs = new byte[1024];
		int len;
		while ((len = fileInputStream.read(bs)) != -1) {
			bos.write(bs, 0, len);
		}
		bos.flush();
		bos.close();
		return fileName;
	}
}

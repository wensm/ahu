package com.carrier.ahu.rest;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.upgrade.MessageType;
import com.carrier.ahu.common.upgrade.UpdateInfo;
import com.carrier.ahu.common.upgrade.UpgradeConfig;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.license.LicenseManager;
import com.carrier.ahu.rest.websocket.MyWebSocket;
import com.carrier.ahu.util.upgrade.UpgradeUtility;
import com.carrier.ahu.util.upgrade.UpgradeUtility.UpgradeListener;
import com.carrier.ahu.vo.ApiResult;
import com.carrier.ahu.vo.SysConstants;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by Braden Zhou on 2018/07/16.
 */
@Api(description = "系统升级接口")
@Slf4j
@RestController
public class UpgradeController extends AbstractController {

    @Value("${ahu.version}")
    private String ahuVersion;

    @RequestMapping(value = "update/check", method = RequestMethod.GET)
    public ApiResult<UpdateInfo> checkUpdates(String lang) throws Exception {
        UpdateInfo updateInfo = null;
        try {
            String factory = LicenseManager.getLicenseInfo().getFactory();
            String languageCode = UpgradeConfig.getLanguageCode(lang);
            updateInfo = UpgradeUtility.checkUpdate(languageCode);
            if (updateInfo != null) {
                updateInfo.setAhuVersion(ahuVersion);
                String release = UpgradeConfig.isMajorRelease(updateInfo.getAhuVersion(), updateInfo.getUpdateVersion())
                        ? CommonConstant.SYS_UPGRADE_RELEASE_MAJOR
                        : CommonConstant.SYS_UPGRADE_RELEASE_MINOR;
                File upgradeFile = new File(SysConstants.ASSERT_DIR + UpgradeConfig.getUpdateFileName(release, factory,
                        languageCode, updateInfo.getUpdateVersion()));
                if (upgradeFile.exists()) {// 防止重复下载
                    updateInfo.setDownloaded(true);
                } else {
                    updateInfo.setDownloaded(false);
                }
            }
        } catch (Exception e) {
            log.error("Failed to check system update");
        }
        return ApiResult.success(updateInfo);
    }

    @RequestMapping(value = "update/upgrade", method = RequestMethod.GET)
    public ApiResult<String> upgrade(String release, String lang, String version) throws Exception {
        String factory = LicenseManager.getLicenseInfo().getFactory();
        String languageCode = UpgradeConfig.getLanguageCode(lang);
        File upgradeFile = new File(
                SysConstants.ASSERT_DIR + UpgradeConfig.getUpdateFileName(release, factory, languageCode, version));
        if (upgradeFile.exists()) {
            if (CommonConstant.SYS_UPGRADE_RELEASE_MAJOR.equals(release)) { // run executable file
                try {
                    Desktop.getDesktop().open(upgradeFile);
                } catch (Exception e) {
                    log.error("Failed to open upgrade file", e);
                }
            } else { // unzip upgrade file to lib folder
                sendMessage(CommonConstant.SYS_UPGRADE_UPGRADE, AHUContext.getIntlString(I18NConstants.UPGRADING));
                unzipUpgradeFile(new FileInputStream(upgradeFile));
                upgradeFile.delete();
                sendMessage(CommonConstant.SYS_UPGRADE_UPGRADE, MessageType.DONE.name());
            }
        } else {
            log.error("Upgrade file not found");
        }
        return ApiResult.success();
    }

    @RequestMapping(value = "update/download", method = RequestMethod.GET)
    public ApiResult<String> downloadUpdate(String release, String lang, String version) throws Exception {
        String factory = LicenseManager.getLicenseInfo().getFactory();
        String languageCode = UpgradeConfig.getLanguageCode(lang);
        sendMessage(CommonConstant.SYS_UPGRADE_UPGRADE, AHUContext.getIntlString(I18NConstants.CONNECTING));
        UpgradeUtility.downloadUpgrade(release, factory, languageCode, new UpgradeListener() {

            @Override
            public void onProgress(String progress) {
                log.debug("progress:" + progress);
                sendMessage(CommonConstant.SYS_UPGRADE_UPGRADE, MessageType.PROGRESS.name() + progress);
            }

            @Override
            public void onError() {
                log.debug("error");
                sendMessage(CommonConstant.SYS_UPGRADE_UPGRADE,
                        MessageType.ERROR.name() + AHUContext.getIntlString(I18NConstants.DOWNLOAD_FAILED));
            }

            @Override
            public void onFinish() {
                log.debug("done");
                sendMessage(CommonConstant.SYS_UPGRADE_UPGRADE, MessageType.DONE.name());
            }

        });

        return ApiResult.success();
    }

    private void sendMessage(String session, String message) {
        try {
            MyWebSocket.sendMessage(session, message);
        } catch (IOException e) {
            log.error("Failed to send message to the client", e);
        }
    }

}

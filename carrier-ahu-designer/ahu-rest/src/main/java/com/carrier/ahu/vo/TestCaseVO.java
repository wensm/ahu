package com.carrier.ahu.vo;


import lombok.Data;

import java.util.List;

@Data
public class TestCaseVO {
	private String key;
	private String title;
    private List<TestCaseVO> children;
}
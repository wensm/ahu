package com.carrier.ahu.vo;


import lombok.Data;

@Data
public class TemplateImportRstVO {
	
    private String success;//导入成功的机组编号
    private String groupPRB;//非本组机组编号
    private String existPRB;//已经存在的机组编号
    private String tempPRB;//温度校验未通过的机组编号
    private String serialsPRB;//机组系列判空的机组编号
}

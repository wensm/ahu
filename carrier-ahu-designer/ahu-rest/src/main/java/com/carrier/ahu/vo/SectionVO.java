package com.carrier.ahu.vo;


import lombok.Data;

@Data
public class SectionVO {
	private String pid;
	private String unitid;
    private String partid;//段ID
    private String key;
    private Short orders;
    private Short types;
    private Short position;
    private Short setup;
    private String pic;
    private String groupi;
    private Double cost;
    private Short avdirection;
    private String metaJson;
    

}

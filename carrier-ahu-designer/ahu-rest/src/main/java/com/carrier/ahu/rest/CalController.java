package com.carrier.ahu.rest;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.*;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.RoleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UserMetaEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.engine.CoilEngineException;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.RestConstant;
import com.carrier.ahu.engine.CoilEH.ElectricHeatService;
import com.carrier.ahu.engine.CoilEH.entity.ElectricHeatInfo;
import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.service.coil.CoilService;
import com.carrier.ahu.engine.service.fan.FanService;
import com.carrier.ahu.engine.service.heatRecycle.HeatRecycleService;
import com.carrier.ahu.length.param.ElectricHeatingParam;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.po.meta.MetaParameter;
import com.carrier.ahu.po.meta.SectionMeta;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.service.AhuService;
import com.carrier.ahu.service.PartitionService;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.BeanTransferUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.InvokeTool;
import com.carrier.ahu.vo.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.*;

import static com.carrier.ahu.common.intl.I18NConstants.COIL_ENGINE_CALCULATION_FAILED;
import static com.carrier.ahu.constant.CommonConstant.METAHU_SERIAL;
import static com.carrier.ahu.util.BeanTransferUtil.CAL_ALL_SECTION;

@Api(description = "计算接口")
@RestController
public class CalController extends AbstractController {

    @Autowired
    private CoilService coilService;
    @Autowired
    private FanService fanService;
    @Autowired
    private HeatRecycleService heatRecycleService;
    @Autowired
    private ElectricHeatService electricHeatService;

    private int calNumber = 5;

    @Autowired
    AhuService ahuService;
    @Autowired
    private SectionService sectionService;
    @Autowired
    private CalculatorTool calculatorTool;
    @Autowired
    private PartitionService partitionService;

    @RequestMapping(value = "calNumber", method = RequestMethod.GET)
    public ApiResult<String> getCalNumber(int number) throws Exception {
        calNumber = number;
        return ApiResult.success(String.valueOf(number));
    }

    /**
     * @param ahuVO
     * @param season；夏季：S；冬季：W
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "cal", method = RequestMethod.POST)
    public ApiResult<Object> calSection(AhuVO ahuVO, String season , Boolean calculateAllResults) throws Exception {
        AhuParam aparam = BeanTransferUtil.getAhuParam(ahuVO);
        List<PartParam> pl = aparam.getPartParams();
        if (pl.isEmpty()) {
            throw new ApiException(ErrorCode.REQUEST_DATA_NO_SECTION);
        }
        Part part = generalParts(ahuVO);
        Unit unit = ahuService.findAhuById(ahuVO.getUnitid());
        unit.setMetaJson(ahuVO.getAhuStr());

        //cal 接口没有传入layoutStr 需要判断空不设置，否则会置空layoutJson
        if(EmptyUtil.isNotEmpty(ahuVO.getLayoutStr())){
            unit.setLayoutJson(ahuVO.getLayoutStr());
        }

        if(EmptyUtil.isEmpty(unit.getSeries())){
            unit.setSeries(String.valueOf(aparam.getParams().get(METAHU_SERIAL)));
        }

        boolean calCoilfanListprice = false;
        UserMeta userMeta = AHUContext.getUserMeta(UserMetaEnum.CAL_COILFAN_LISTPRICE);
        if (!EmptyUtil.isEmpty(userMeta)) {//根据用户设置是否启用计算盘管、风机添加价格列
            if(BaseDataUtil.stringConversionBoolean(userMeta.getMetaValue())){
                calCoilfanListprice = true;
            }
        }
        User user = getUser();//当前用户

        PartParam pparam = pl.get(0);
        if (SectionTypeEnum.TYPE_MIX.getId().equals(pparam.getKey())) {
            return ApiResult.success("Service has been deprecated, pleate contact JL");
        } else if (SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId().equals(pparam.getKey())) {//增加电加热盘管计算逻辑
            ElectricHeatingParam sElectricHeatingParam = new InvokeTool<ElectricHeatingParam>().genInParamFromAhuParam(aparam, pparam, SystemCalculateConstants.seasonS, new ElectricHeatingParam());
            List<ElectricHeatInfo> electricHeatInfoList = electricHeatService.getRowNumAndGroupC(sElectricHeatingParam);
            Map<String, Object> result = phraseCoilInSimpleForat(electricHeatInfoList, SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL);
            ElectricHeatingVO vo = new ElectricHeatingVO();
            vo.setUserRole(user.getUserRole());
            vo.setResultData(result.values());
            vo.setSectionKey(MetaCodeGen.calculateAttributePrefix(SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId()));
            return ApiResult.success(vo);
        } else if (SectionTypeEnum.TYPE_SINGLE.getId().equals(pparam.getKey())) {
            return ApiResult.success("Service has been deprecated, pleate contact JL");
        } else if (SectionTypeEnum.TYPE_COLD.getId().equals(pparam.getKey())) {
            CoilInParam coilInParam = new InvokeTool<CoilInParam>().genInParamFromAhuParam(aparam, pparam, season, new CoilInParam());
            coilInParam.setSeason(season.toUpperCase());
            String coilType = SystemCalculateConstants.waterCoil;
            coilInParam.setCoilType(coilType);
            coilInParam.setCalculateAllResults(calculateAllResults);
            if (EmptyUtil.isEmpty(coilInParam.getSerial())) {
                throw new ApiException(ErrorCode.UNIT_MODEL_IS_EMPTY);
            }
            double enteringFluidTemperature = coilInParam.getEnteringFluidTemperature();//进水温度
            double wTAScend = coilInParam.getWTAScend();//水温升
            double inDryBulbT = coilInParam.getInDryBulbT();//进风干球温度
            double inWetBulbT = coilInParam.getInWetBulbT();//进风湿球温度
            //如果是冬季验证
            if (SystemCalculateConstants.seasonW.equals(coilInParam.getSeason())) {
                /*冬季计算增加 进水温度 - 水温升 必须大于 进风干球温度*/
                if (enteringFluidTemperature - wTAScend <= inDryBulbT || inDryBulbT > 50 || inWetBulbT >= inDryBulbT) {
                    throw new CoilEngineException(COIL_ENGINE_CALCULATION_FAILED);
                }
            }else{
                if (inDryBulbT < 12 || inWetBulbT >= inDryBulbT) {
                    throw new CoilEngineException(COIL_ENGINE_CALCULATION_FAILED);
                }
            }
            List<CoilInfo> list = coilService.getCoil(coilInParam, null, coilType, AHUContext.getAhuVersion());
            if (null == list || list.size() == 0) {
                throw new ApiException(ErrorCode.COIL_NOT_FIND_RESULT);
            }

            //追加dll计算价格到列表
            if(calCoilfanListprice) {
                appendCoilPrice(part, unit, list,SectionTypeEnum.TYPE_COLD.getId());
            }

            Map<String, Object> result = phraseCoilInSimpleForat(list, coilType, SectionTypeEnum.TYPE_COLD, season);
            CoilCalculationResultVO vo = new CoilCalculationResultVO();
            vo.setUserRole(user.getUserRole());
            vo.setSeason(season);
            vo.setResultData(result.values());
            vo.setSectionKey(MetaCodeGen.calculateAttributePrefix(SectionTypeEnum.TYPE_COLD.getId()));
            return ApiResult.success(vo);
        } else if (SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId().equals(pparam.getKey())) {
            CoilInParam coilInParam = new InvokeTool<CoilInParam>().genInParamFromAhuParam(aparam, pparam, season, new CoilInParam());
            coilInParam.setSeason(season.toUpperCase());
            coilInParam.setSeason(season);
            String coilType = SystemCalculateConstants.DXCoil;
            coilInParam.setCoilType(coilType);
            calculateAllResults = false;
            coilInParam.setCalculateAllResults(calculateAllResults);
            if (EmptyUtil.isEmpty(coilInParam.getSerial())) {
                throw new ApiException(ErrorCode.UNIT_MODEL_IS_EMPTY);
            }
            double enteringFluidTemperature = coilInParam.getEnteringFluidTemperature();//进水温度
            double wTAScend = coilInParam.getWTAScend();//水温升
            double inDryBulbT = coilInParam.getInDryBulbT();//进风干球温度
            double inWetBulbT = coilInParam.getInWetBulbT();//进风湿球温度
            //如果是冬季验证
            if (SystemCalculateConstants.seasonW.equals(coilInParam.getSeason())) {
                /*冬季计算增加 进水温度 - 水温升 必须大于 进风干球温度*/
                if (enteringFluidTemperature - wTAScend <= inDryBulbT || inDryBulbT > 50 || inWetBulbT >= inDryBulbT) {
                    throw new CoilEngineException(COIL_ENGINE_CALCULATION_FAILED);
                }
            }else{
                if (inDryBulbT < 12 || inWetBulbT >= inDryBulbT) {
                    throw new CoilEngineException(COIL_ENGINE_CALCULATION_FAILED);
                }
            }
            List<CoilInfo> list = coilService.getCoil(coilInParam, null, coilType, AHUContext.getAhuVersion());
            if (null == list || list.size() == 0) {
                throw new ApiException(ErrorCode.COIL_NOT_FIND_RESULT);
            }

            //追加dll计算价格到列表
            if(calCoilfanListprice) {
                appendCoilPrice(part, unit, list,SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId());
            }

            Map<String, Object> result = phraseCoilInSimpleForat(list, coilType,SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL, season);
            CoilCalculationResultVO vo = new CoilCalculationResultVO();
            vo.setUserRole(user.getUserRole());
            vo.setSeason(season);
            vo.setResultData(result.values());
            vo.setSectionKey(MetaCodeGen.calculateAttributePrefix(SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId()));
            return ApiResult.success(vo);
        } else if (SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(pparam.getKey())) {
            CoilInParam coilInParam = new InvokeTool<CoilInParam>().genInParamFromAhuParam(aparam, pparam, season, new CoilInParam());
            coilInParam.setSeason(season.toUpperCase());
            String coilType = SystemCalculateConstants.waterCoilH;
            coilInParam.setCoilType(coilType);
            coilInParam.setCalculateAllResults(calculateAllResults);
            if (EmptyUtil.isEmpty(coilInParam.getSerial())) {
                throw new ApiException(ErrorCode.UNIT_MODEL_IS_EMPTY);
            }
            double enteringFluidTemperature = coilInParam.getEnteringFluidTemperature();//进水温度
            double wTAScend = coilInParam.getWTAScend();//水温升
            double inDryBulbT = coilInParam.getInDryBulbT();//进风干球温度
            double inWetBulbT = coilInParam.getInWetBulbT();//进风湿球温度
            if (enteringFluidTemperature - wTAScend <= inDryBulbT || inDryBulbT > 50 || inWetBulbT >= inDryBulbT) {
                throw new CoilEngineException(COIL_ENGINE_CALCULATION_FAILED);
            }
            List<CoilInfo> list = coilService.getCoil(coilInParam, null, coilType, SystemCalculateConstants.version);
            if (null == list || list.size() == 0) {
                throw new ApiException(ErrorCode.COIL_NOT_FIND_RESULT);
            }
            //追加dll计算价格到列表
            if(calCoilfanListprice) {
                appendCoilPrice(part, unit, list,SectionTypeEnum.TYPE_HEATINGCOIL.getId());
            }
            CoilCalculationResultVO vo = new CoilCalculationResultVO();
            vo.setUserRole(user.getUserRole());
            vo.setSeason(season);
            Map<String, Object> result = phraseCoilInSimpleForat(list, coilType, SectionTypeEnum.TYPE_COLD, season);
            vo.setResultData(result.values());
            vo.setSectionKey(MetaCodeGen.calculateAttributePrefix(SectionTypeEnum.TYPE_HEATINGCOIL.getId()));
            return ApiResult.success(vo);
        } else if (SectionTypeEnum.TYPE_FAN.getId().equals(pparam.getKey())) {
            FanInParam fanInParam = new InvokeTool<FanInParam>().genInParamFromAhuParam(aparam, pparam, season, new FanInParam());

            //如果是销售员，则总静压不能大于2500Pa，如果大于2500pa，界面提示，不能选择风机
            if(RoleEnum.Sales.name().equalsIgnoreCase(AHUContext.getUserRole())) {
                if(fanInParam.getTotalStatic() > 2500){
                    throw new ApiException(ErrorCode.TOTAL_STATIC_TOO_BIGER);
                }
            }
            fanInParam.setProjectId(unit.getPid());
            if (EmptyUtil.isEmpty(fanInParam.getSerial())) {
                throw new ApiException(ErrorCode.UNIT_MODEL_IS_EMPTY);
            }
            List<FanInfo> list = fanService.getFanEngine(fanInParam, SystemCalculateConstants.version);
            if (list.isEmpty()) {
                throw new ApiException(ErrorCode.SUITABLE_FAN_NOT_FOUND);
            }

            //追加dll计算价格到列表
            if(calCoilfanListprice) {
                appendFanPrice(part, unit, list);
            }

            Map<String, Object> result = phraseFan(list, SectionTypeEnum.TYPE_FAN, season);
            return ApiResult.success(result.values());
        } else {
            return ApiResult.success(RestConstant.SYS_BLANK);
        }
    }

    private void appendFanPrice(Part part, Unit unit, List<FanInfo> list) {
        List<Part> priceParts = new ArrayList<>();
        for (FanInfo info : list) {
            Part partsTemp = ObjectUtil.clone(part);
            mergePriceProp(partsTemp, JSON.toJSONString(info),SectionTypeEnum.TYPE_FAN.getId());

            priceParts.add(partsTemp);
        }
        PriceResultVO priceResultVo = calculatorTool.calculatePartPrice(unit, priceParts, getLanguage());
        int i = 0;
        for (PriceResultVO.PriceBean priceBean : priceResultVo.getDetails()) {
            if (priceBean.getKey().equals(SectionTypeEnum.TYPE_FAN.getId())) {
                list.get(i).setPrice(String.valueOf(priceBean.getPrice()));
                i++;
            }
        }
    }

    private void appendCoilPrice(Part part, Unit unit, List<CoilInfo> list,String sectionKey) {
        List<Part> priceParts = new ArrayList<>();
        for (CoilInfo info : list) {
            Part partsTemp = ObjectUtil.clone(part);
            mergePriceProp(partsTemp, JSON.toJSONString(info),sectionKey);
            priceParts.add(partsTemp);
        }
        PriceResultVO priceResultVo = calculatorTool.calculatePartPrice(unit, priceParts, getLanguage());
        int i = 0;
        for (PriceResultVO.PriceBean priceBean : priceResultVo.getDetails()) {
            if (priceBean.getKey().equals(sectionKey)) {
                list.get(i).setPrice(String.valueOf(priceBean.getPrice()));
                i++;
            }
        }
    }

    /**
     * merge entityInfo 的属性到临时段
     * @param partTemp
     * @param entityInfoJson
     */
    private void mergePriceProp(Part partTemp, String entityInfoJson,String sectionKey) {
        String preKey = MetaCodeGen.calculateAttributePrefix(sectionKey);
        Map<String, Object> partMetaJsonMap = JSON.parseObject(partTemp.getMetaJson(), HashMap.class);

        Map<String, Object> infoMap = JSON.parseObject(entityInfoJson, HashMap.class);
        infoMap.forEach((key, value) -> {
            if(partMetaJsonMap.containsKey(preKey+"."+key)){
                partMetaJsonMap.put(preKey+"."+key,value);
            }
        });
        partTemp.setMetaJson(JSON.toJSONString(partMetaJsonMap));
    }

    /**
     * 生成当前计算段的part对象
     * @param ahuVO
     * @return
     */
    private Part generalParts(AhuVO ahuVO){
        String sectionStr = ahuVO.getSectionStr();
        Gson gson = new Gson();
        List<SectionVO> sectionVOs = gson.fromJson(sectionStr, new TypeToken<List<SectionVO>>() {
        }.getType());
        List<Part> parts = new ArrayList<Part>();
        for (SectionVO sectionVO : sectionVOs) {
            Part part = new Part();
            part.setPartid(sectionVO.getPartid());
            String pMetaJson = sectionVO.getMetaJson();
            part.setRecordStatus(AhuStatusEnum.SELECTING.getId());
            if (EmptyUtil.isNotEmpty(pMetaJson)) {
                Map<String, Object> conMap = JSON.parseObject(pMetaJson, HashMap.class);
                if (EmptyUtil.isNotEmpty(conMap)) {
                    Boolean isComplete = MapValueUtils
                            .getBooleanValue(SystemCalculateConstants.META_SECTION_COMPLETED, conMap);
                    if (EmptyUtil.isNotEmpty(isComplete)) {
                        if (isComplete) {
                            part.setRecordStatus(AhuStatusEnum.SELECT_COMPLETE.getId());
                        }
                    }
                }
            }
            part.setMetaJson(pMetaJson);
            part.setSectionKey(sectionVO.getKey());
            part.setCost(sectionVO.getCost());
            part.setPid(ahuVO.getPid());
            part.setUnitid(ahuVO.getUnitid());
            part.setPosition(sectionVO.getPosition());

            if (ahuVO.getPosition() == CAL_ALL_SECTION
                    || (ahuVO.getPosition() != null && ahuVO.getPosition().equals(sectionVO.getPosition()))) {
                return part;
            }

        }
        return null;
    }
    private Map<String, Object> phraseCoilInSimpleForat(List<ElectricHeatInfo> list, SectionTypeEnum type) {
        Map<String, Object> result = new LinkedHashMap<>();
        Field[] fields = ElectricHeatInfo.class.getDeclaredFields();
        for (ElectricHeatInfo info : list) {
            String key = String.format("39G0711-%s-%s", info.getRowNum(), info.getGroupC());
            Map<String, Object> calResult = new HashMap<>();
            for (int i = 0; i < fields.length; i++) {
                String fieldNamei = fields[i].getName();
                calResult.put(fieldNamei, getFieldValueByName(fieldNamei, info));
            }
            result.put(key, calResult);
        }
        return result;
    }

    /**
     * 封装盘管计算结果给前台，但是，只返回简单名称，不加段的前缀名
     *
     * @param list
     * @param coilType
     * @param type
     * @param season
     * @return
     */
    private Map<String, Object> phraseCoilInSimpleForat(List<CoilInfo> list, String coilType, SectionTypeEnum type,String season) {
        Map<String, Object> result = new LinkedHashMap<>();
        Field[] fields = CoilInfo.class.getDeclaredFields();
        for (CoilInfo info : list) {
            String key = JSONArray.toJSONString(info);
            Map<String, Object> calResult = new HashMap<>();
            for (int i = 0; i < fields.length; i++) {
                String fieldNamei = fields[i].getName();
                calResult.put(fieldNamei, getFieldValueByName(fieldNamei, info));
            }
            result.put(key, calResult);
        }
        return result;
    }

    private Map<String, Object> phraseFan(List<FanInfo> resultList, SectionTypeEnum type, String season) {
        SectionMeta sectionMeta = AhuSectionMetas.getInstance().getSectionMeta(type);
        Map<String, MetaParameter> parameters = sectionMeta.getParameters();
        Field[] fields = FanInfo.class.getDeclaredFields();
        String metaStringPrefix = RestConstant.METASEXON_FAN_PREFIX;
        Map<String, Object> result = new LinkedHashMap<>();
        for(FanInfo fanInfo : resultList){
            String key = JSONArray.toJSONString(fanInfo);
            Map<String, Object> fanData = new HashMap<String, Object>();
            for (int i = 0; i < fields.length; i++) {
                String fieldNamei = fields[i].getName();
                String mkey = metaStringPrefix + fieldNamei;
                if (parameters.containsKey(mkey)) {
                    fanData.put(mkey, getFieldValueByName(fieldNamei, fanInfo));
                } else {
                    if (StringUtils.isNotBlank(season)) {
                        String mkeyWithSeason = metaStringPrefix + season.toUpperCase() + fieldNamei;
                        if (parameters.containsKey(mkeyWithSeason)) {
                            fanData.put(mkeyWithSeason, getFieldValueByName(fieldNamei, fanInfo));
                        } else {
                            logger.error(MessageFormat.format("Calculate Section [{0}] Meta [{1}] is not found.",
                                    type.getId(), fieldNamei));
                        }
                    } else {
                        /*logger.error(MessageFormat.format("Calculate Section [{0}] Meta [{1}] is not found.",
                                type.getId(), fieldNamei));*/
                        fanData.put(mkey, getFieldValueByName(fieldNamei, fanInfo));//价格等参数也返回
                    }
                }
            }
            result.put(key, fanData);
        }
        return result;
    }

    /**
     * 根据属性名获取属性值
     */
    private Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = RestConstant.SYS_STRING_GET + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[]{});
            Object value = method.invoke(o, new Object[]{});
            return value;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }
}

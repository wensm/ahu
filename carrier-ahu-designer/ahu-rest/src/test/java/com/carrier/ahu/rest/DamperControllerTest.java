package com.carrier.ahu.rest;

import static com.carrier.ahu.common.enums.DamperPosEnum.Top;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_DISCHARGE;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_MIX;
import static com.carrier.ahu.constant.CommonConstant.SYS_MSG_RESPONSE_SUCCESS;
import static com.carrier.ahu.util.meta.SectionMetaUtils.getMetaSectionKey;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_POS_SIZE_A;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_POS_SIZE_B;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_POS_SIZE_C;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_POS_SIZE_D;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_SIZE_X;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DAMPER_SIZE_Y;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39CQ;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39G;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39XT;
import static com.carrier.ahu.vo.SystemCalculateConstants.DISCHARGE_AINTERFACE_FLANGE;
import static com.carrier.ahu.vo.SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_ALUMINUM;
import static com.carrier.ahu.vo.SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_GL;
import static com.carrier.ahu.vo.SystemCalculateConstants.MIX_DAMPERMETERIAL_AL;
import static com.carrier.ahu.vo.SystemCalculateConstants.MIX_DAMPERMETERIAL_GI;
import static com.carrier.ahu.vo.SystemCalculateConstants.MIX_DAMPEROUTLET_FD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.function.Function;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.JsonPathResultMatchers;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.DamperPosEnum;
import com.carrier.ahu.common.enums.DamperTypeEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.vo.AdjustDamperVO;
import com.carrier.ahu.vo.DamperPosSizeResultVO;

public class DamperControllerTest extends AbstractControllerTest {

    private String unitNo = "1418";

    private class DamperSizeInputHelper {

        private String content;

        DamperSizeInputHelper(String jsonFile) {
            this.content = getJsonContent(jsonFile, AdjustDamperVO.class);
        }

        DamperSizeInputHelper product(String product) {
            content = content.replace("{product}", product);
            return this;
        }

        DamperSizeInputHelper series(String unitModel) {
            content = content.replace("{series}", unitModel);
            return this;
        }

        DamperSizeInputHelper airvolume(String eairvolume, String sairvolume) {
            content = content.replace("{eairvolume}", eairvolume);
            content = content.replace("{sairvolume}", sairvolume);
            return this;
        }

        DamperSizeInputHelper airDirection(String airDirection) {
            content = content.replace("{airDirection}", airDirection);
            return this;
        }

        DamperSizeInputHelper naRatio(String naRatio) {
            content = content.replace("{NARatio}", naRatio);
            return this;
        }

        DamperSizeInputHelper damperType(String damperType) {
            content = content.replace("{damperType}", damperType);
            return this;
        }

        DamperSizeInputHelper sectionL(String sectionL) {
            content = content.replace("{sectionL}", sectionL);
            return this;
        }

        DamperSizeInputHelper material(String material) {
            content = content.replace("{material}", material);
            return this;
        }

        DamperSizeInputHelper margins(String modeA, String modeB, String modeC, String modeD) {
            content = content.replace("{modeA}", modeA);
            content = content.replace("{modeB}", modeB);
            content = content.replace("{modeC}", modeC);
            content = content.replace("{modeD}", modeD);
            return this;
        }

        DamperSizeInputHelper pos(String pos) {
            content = content.replace("{pos}", pos);
            return this;
        }

        String build() {
            return this.content;
        }
    }

    @Test
    public void testAdjustDamperSize_givenMixDamperWithInvalidMargin() throws Exception {
        String requestBody = givenAdjustMixDamperRequest(AHU_PRODUCT_39G, unitNo, "11",
                new String[] { "1", "2", "2", "2" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_PANEL_MARGIN.getMessage(), 2);

        requestBody = givenAdjustMixDamperRequest(AHU_PRODUCT_39G, unitNo, "11",
                new String[] { "2", "1", "2", "2" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_PANEL_MARGIN.getMessage(), 2);

        requestBody = givenAdjustMixDamperRequest(AHU_PRODUCT_39G, unitNo, "6",
                new String[] { "2", "2", "2", "2" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_BLADE_SIZE.getMessage(), 3);

        requestBody = givenAdjustMixDamperRequest(AHU_PRODUCT_39G, unitNo, "11",
                new String[] { "2", "2", "1", "2" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_PANEL_MARGIN.getMessage(), 2);

        requestBody = givenAdjustMixDamperRequest(AHU_PRODUCT_39G, unitNo, "11",
                new String[] { "2", "2", "2", "1" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_PANEL_MARGIN.getMessage(), 2);

        requestBody = givenAdjustMixDamperRequest(AHU_PRODUCT_39G, unitNo, "6",
                new String[] { "2", "2", "2", "2" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_BLADE_SIZE.getMessage(), 3);
    }

    @Test
    public void testAdjustDamperSize_givenMixDamperWithInvalidFanSpeed() throws Exception {
        String requestBody = givenAdjustMixDamperRequest(AHU_PRODUCT_39G, unitNo, "11",
                new String[] { "4", "4", "4", "4" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.DAMPER_FAN_SPEED_EXCEED_THRESHOLD.getMessage(), 8);
    }

    private String givenAdjustMixDamperRequest(String product, String unitNo, String sectionL, String[] margins) {
        return new DamperSizeInputHelper("adjust_damper_size_mix.json")
                .product(product).series(product + unitNo).airvolume("18000", "18000")
                .sectionL(sectionL).naRatio("100")
                .airDirection(AirDirectionEnum.SUPPLYAIR.getCode())
                .pos(Top.name())
                .damperType(DamperTypeEnum.Fresh.name())
                .material(MIX_DAMPEROUTLET_FD)
                .margins(margins[0], margins[1], margins[2], margins[3]).build();
    }

    @Test
    public void testAdjustDamperSize_givenMixTopDamper() throws Exception {
        double[] resultSizes1 = { 467.0, 467.0, 626.25, 626.25, 256.0, 637.5 };
        String requestBody = givenAdjustMixDamperRequest(AHU_PRODUCT_39CQ, Top, MIX_DAMPEROUTLET_FD);
        assertAdjustDamperRespone(requestBody, TYPE_MIX, Top, new DamperPosSizeResultVO(resultSizes1));

        requestBody = givenAdjustMixDamperRequest(AHU_PRODUCT_39G, Top, MIX_DAMPERMETERIAL_GI);
        assertAdjustDamperRespone(requestBody, TYPE_MIX, Top, new DamperPosSizeResultVO(resultSizes1));

        double[] resultSizes2 = { 466.0, 466.0, 571.0, 571.0, 258.0, 748.0 };
        requestBody = givenAdjustMixDamperRequest(AHU_PRODUCT_39XT, Top, MIX_DAMPERMETERIAL_AL);
        assertAdjustDamperRespone(requestBody, TYPE_MIX, Top, new DamperPosSizeResultVO(resultSizes2));
    }

    private String givenAdjustMixDamperRequest(String product, DamperPosEnum pos, String material) {
        return new DamperSizeInputHelper("adjust_damper_size_mix.json")
                .product(product).series(product + unitNo).airvolume("18000", "18000")
                .sectionL("11").naRatio("20")
                .airDirection(AirDirectionEnum.SUPPLYAIR.getCode())
                .pos(pos.name())
                .damperType(DamperTypeEnum.Fresh.name())
                .material(material)
                .margins("4", "4", "5", "5").build();
    }

    public void assertAdjustDamperRespone(String content, SectionTypeEnum sectionType, DamperPosEnum damperPos,
            DamperPosSizeResultVO resultVo) throws Exception {
        this.mockMvc
                .perform(post("/dampers/adjustSize/" + damperPos.name())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.msg").value(SYS_MSG_RESPONSE_SUCCESS))
                .andExpect(dataJsonPath(sectionType, KEY_DAMPER_POS_SIZE_A, damperPos).value(resultVo.getPosSizeA()))
                .andExpect(dataJsonPath(sectionType, KEY_DAMPER_POS_SIZE_B, damperPos).value(resultVo.getPosSizeB()))
                .andExpect(dataJsonPath(sectionType, KEY_DAMPER_POS_SIZE_C, damperPos).value(resultVo.getPosSizeC()))
                .andExpect(dataJsonPath(sectionType, KEY_DAMPER_POS_SIZE_D, damperPos).value(resultVo.getPosSizeD()))
                .andExpect(dataJsonPath(sectionType, KEY_DAMPER_SIZE_X, damperPos).value(resultVo.getSizeX()))
                .andExpect(dataJsonPath(sectionType, KEY_DAMPER_SIZE_Y, damperPos).value(resultVo.getSizeY()));
    }

    private JsonPathResultMatchers dataJsonPath(SectionTypeEnum sectionType, Function<DamperPosEnum, String> metaKey,
            DamperPosEnum damperPos) {
        return jsonPath(String.format("$.data['%s']['%s']", damperPos.name(),
                getMetaSectionKey(sectionType, metaKey.apply(damperPos))));
    }

    public void assertAdjustDamperRespone(String content, DamperPosEnum damperPos, String errorCode, Object... params)
            throws Exception {
        this.mockMvc
                .perform(post("/dampers/adjustSize/" + damperPos.name())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data['" + damperPos.name() + "']")
                        .value(AHUContext.getIntlString(errorCode, params)));
    }

    @Test
    public void testAdjustDamperSize_givenDischargeDamperWithInvalidMargin() throws Exception {
        String requestBody = givenAdjustDischargeDamperRequest(AHU_PRODUCT_39G, unitNo, "11",
                new String[] { "1", "2", "2", "2" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_PANEL_MARGIN.getMessage(), 2);

        requestBody = givenAdjustDischargeDamperRequest(AHU_PRODUCT_39G, unitNo, "11",
                new String[] { "2", "1", "2", "2" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_PANEL_MARGIN.getMessage(), 2);

        requestBody = givenAdjustDischargeDamperRequest(AHU_PRODUCT_39G, unitNo, "6",
                new String[] { "2", "2", "2", "2" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_BLADE_SIZE.getMessage(), 3);

        requestBody = givenAdjustDischargeDamperRequest(AHU_PRODUCT_39G, unitNo, "11",
                new String[] { "2", "2", "1", "2" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_PANEL_MARGIN.getMessage(), 2);

        requestBody = givenAdjustDischargeDamperRequest(AHU_PRODUCT_39G, unitNo, "11",
                new String[] { "2", "2", "2", "1" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_PANEL_MARGIN.getMessage(), 2);

        requestBody = givenAdjustDischargeDamperRequest(AHU_PRODUCT_39G, unitNo, "6",
                new String[] { "2", "2", "2", "2" });
        assertAdjustDamperRespone(requestBody, Top, ErrorCode.INVALID_DAMPER_BLADE_SIZE.getMessage(), 3);
    }

    @Test
    public void testAdjustDamperSize_givenDischargeDamper() throws Exception {
        double[] resultSizes1 = { 467.0, 467.0, 626.25, 626.25, 256.0, 637.5 };
        String requestBody = givenAdjustDischargeDamperRequest(AHU_PRODUCT_39CQ, Top, DISCHARGE_AINTERFACE_FLANGE);
        assertAdjustDamperRespone(requestBody, TYPE_DISCHARGE, Top, new DamperPosSizeResultVO(resultSizes1));

        requestBody = givenAdjustDischargeDamperRequest(AHU_PRODUCT_39G, Top, DISCHARGE_DAMPERMETERIAL_GL);
        assertAdjustDamperRespone(requestBody, TYPE_DISCHARGE, Top, new DamperPosSizeResultVO(resultSizes1));

        double[] resultSizes2 = { 466.0, 466.0, 571.0, 571.0, 258.0, 748.0 };
        requestBody = givenAdjustDischargeDamperRequest(AHU_PRODUCT_39XT, Top, DISCHARGE_DAMPERMETERIAL_ALUMINUM);
        assertAdjustDamperRespone(requestBody, TYPE_DISCHARGE, Top, new DamperPosSizeResultVO(resultSizes2));
    }

    private String givenAdjustDischargeDamperRequest(String product, DamperPosEnum pos, String material) {
        return new DamperSizeInputHelper("adjust_damper_size_discharge.json")
                .product(product).series(product + unitNo).airvolume("3600", "3600")
                .sectionL("11")
                .airDirection(AirDirectionEnum.SUPPLYAIR.getCode())
                .pos(pos.name())
                .damperType(DamperTypeEnum.Fresh.name())
                .material(material)
                .margins("4", "4", "5", "5").build();
    }

    private String givenAdjustDischargeDamperRequest(String product, String unitNo, String sectionL, String[] margins) {
        return new DamperSizeInputHelper("adjust_damper_size_discharge.json")
                .product(product).series(product + unitNo).airvolume("18000", "18000")
                .sectionL(sectionL)
                .airDirection(AirDirectionEnum.SUPPLYAIR.getCode())
                .pos(Top.name())
                .damperType(DamperTypeEnum.Fresh.name())
                .material(DISCHARGE_AINTERFACE_FLANGE)
                .margins(margins[0], margins[1], margins[2], margins[3]).build();
    }

}
package com.carrier.ahu.test.service;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.service.SectionService;
import com.carrier.ahu.test.AbstractTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Wen zhengtao on 2017/4/1.
 */
public class SectionServiceTest extends AbstractTest{
    @Autowired
    SectionService sectionService;

    @Test
    public void addSectionTest(){
        try {
            Part part = new Part();
            part.setPid("34");
            part.setUnitid("1");
            part.setOrders((short)1);
            part.setMetaJson("abcd");
            String sectionId = sectionService.addSection(part,"admin");
            System.out.println(sectionId);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void updateSectionTest(){
        try {
            String sectionId = "1";
            Part part = sectionService.findSectionById(sectionId);
            if(null != part){
                part.setOrders((short)2);
                sectionService.updateSection(part,"admin");
                System.out.println(part.getPartid());
            }
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void deleteSectionTest(){
        try {
            String sectionId = "1";
            sectionService.deleteSection(sectionId);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }

    }

    @Test
    public void findSectionByIdTest(){
        try {
            String sectionId = "1";
            Part part = sectionService.findSectionById(sectionId);
            System.out.println(part);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void findSectionListTest(){
        try {
            String ahuId = "1";
            List<Part> partList = sectionService.findSectionList(ahuId);
            System.out.println(partList);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}

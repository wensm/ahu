package com.carrier.ahu.test.service;

import com.carrier.ahu.common.entity.GroupInfo;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.service.GroupService;
import com.carrier.ahu.test.AbstractTest;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Wen zhengtao on 2017/3/28.
 */
public class GroupServiceTest extends AbstractTest {
    @Autowired
    GroupService groupService;

    @Test
    public void addGroupTest(){
        try {
            GroupInfo groupInfo = new GroupInfo();
            groupInfo.setGroupName("测试组");
            groupInfo.setProjectId("33");
            groupInfo.setGroupType(GroupTypeEnum.TYPE_BASIC.name());
            String groupId = groupService.addGroup(groupInfo,"admin");
            System.out.println(groupId);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void updateGroupTest(){
        try {
            String groupId = "2";
            GroupInfo groupInfo = groupService.findGroupById(groupId);
            if(null != groupInfo){
                groupInfo.setGroupName("测试组update");
                groupService.updateGroup(groupInfo,"admin");
                System.out.println(groupInfo.getGroupId());
            }
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void deleteGroupTest(){
        try {
            String groupId = "1";
            groupService.deleteGroup(groupId);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void findGroupByIdTest(){
        try {
            String groupId = "2";
            GroupInfo groupInfo = groupService.findGroupById(groupId);
            System.out.println(groupInfo);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void findGroupListTest(){
        try {
            String projectId = "33";
            List<GroupInfo> groupInfoList = groupService.findGroupList(projectId);
            System.out.println(groupInfoList);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}

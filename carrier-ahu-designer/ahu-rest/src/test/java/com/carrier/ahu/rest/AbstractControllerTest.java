package com.carrier.ahu.rest;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;

@ActiveProfiles({ "test" })
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = { TestApplication.class })
@AutoConfigureMockMvc
public abstract class AbstractControllerTest {

    @Autowired
    protected MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {

    }

    protected <T> String getJsonContent(String jsonFile, Class<T> clazz) {
        Gson gson = new Gson();
        InputStream jsonStream = this.getClass().getResourceAsStream(jsonFile);
        T t = gson.fromJson(new InputStreamReader(jsonStream), clazz);
        return gson.toJson(t);
    }

}

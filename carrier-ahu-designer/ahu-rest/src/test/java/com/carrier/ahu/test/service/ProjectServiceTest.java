package com.carrier.ahu.test.service;

import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.service.ProjectService;
import com.carrier.ahu.test.AbstractTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Wen zhengtao on 2017/3/28.
 */
public class ProjectServiceTest extends AbstractTest {
    @Autowired
    ProjectService projectService;

    @Test
    public void addProjectTest(){
        try {
            Project project = new Project();
            project.setNo("123456");
            project.setName("测试项目");
            project.setAddress("测试地址");
            project.setContract("456789");
            project.setSaler("taosy");
            project.setEnquiryNo("4587214532145");
            String id = projectService.addProject(project,"admin");
            System.out.println(id);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void existsTest(){
        try {
            String projectNo = "123456";
            boolean flag = projectService.exists(projectNo);
            System.out.println(flag);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void getProjectByIdTest(){
        try {
            String projectId = "33";
            Project project = projectService.getProjectById(projectId);
            System.out.println(project);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void findProjectListTest(){
        try {
            List<Project> projectList = projectService.findProjectList();
            System.out.println(projectList);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void updateProjectTest(){
        try {
            String projectId = "34";
            Project project = projectService.getProjectById(projectId);
            if(null != project){
                project.setName("测试项目update");
                projectService.updateProject(project,"admin");
                System.out.println(project.getPid());
            }
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void deleteProjectTest(){
        try {
            String projectId = "33";
            projectService.deleteProject(projectId);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}

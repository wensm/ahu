package com.carrier.ahu.report.pdf.format;

import java.util.List;

import lombok.Data;

/**
 * Set special cell image.
 * 
 * Created by Braden Zhou on 2018/10/31.
 */
@Data
public class CellImage {

    private String image;
    private Float scalePercent;
    private String cellHorizontalAlignment;
    private List<String> positions;

}

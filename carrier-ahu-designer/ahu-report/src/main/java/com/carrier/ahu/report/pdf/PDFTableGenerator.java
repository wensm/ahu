package com.carrier.ahu.report.pdf;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.carrier.ahu.report.common.FontColorEnum;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.report.common.FontEnum;
import com.carrier.ahu.report.common.FontHelper;
import com.carrier.ahu.report.common.ReportConstants;
import com.carrier.ahu.report.pdf.format.CellAlignment;
import com.carrier.ahu.report.pdf.format.CellBorder;
import com.carrier.ahu.report.pdf.format.CellImage;
import com.carrier.ahu.report.pdf.format.CellSpan;
import com.carrier.ahu.report.pdf.format.PdfFormat;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.RegexNumberValidate;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

import lombok.extern.slf4j.Slf4j;

/**
 * Generate PdfPTable based on report contents and defined format.
 * 
 * Created by Braden Zhou on 2018/10/23.
 */
@Slf4j
public class PDFTableGenerator {

    private static final char NO_BORDER = '0';

    public static PdfPTable generate(String[][] contents, String reportName) throws DocumentException, IOException {
        // load report format
        PdfFormat format = null;
        int index = reportName.indexOf(ReportConstants.REPORT_SEPARATOR);
        if (index > 0) {
            String parentReportName = reportName.substring(0, reportName.indexOf(ReportConstants.REPORT_SEPARATOR));
            PdfFormat parentFormat = PdfFormat.loadPdfFormat(parentReportName);
            format = parentFormat.getDetailFormat(reportName);
        } else {
            format = PdfFormat.loadPdfFormat(reportName);
        }

        int rows = contents.length;
        int cols = contents[0].length;

        PdfPTable table = new PdfPTable(cols);
        table.setWidthPercentage(format.getWidthPercentage());
        table.setHorizontalAlignment(getAlignment(format.getTableHorizontalAlignment(), PdfPCell.ALIGN_CENTER));
        table.setWidths(ArrayUtils.toPrimitive(format.getWidths()));

        List<String> spannedCells = new ArrayList<>();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                String rowAndCol = getRowAndCol(row, col);
                if (!spannedCells.contains(rowAndCol)) { // skip spanned cells
                    String content = contents[row][col];
                    CellImage cellImage = getCellImage(format.getCellImages(), rowAndCol);
                    if (cellImage != null) { // write image into cell
                        String imagePath = AHUContext.getImagePath(cellImage.getImage());
                        addCellImage(table, imagePath, format, cellImage);
                    } else {
                        CellSpan cellSpan = getCellSpan(format.getCellSpans(), rowAndCol);
                        PdfFormat realFormat = getRealFormat(format, cellSpan, rowAndCol);
                        if (cellSpan != null) {
                            addCellSpan(table, content, cellSpan.getRowSpan(), cellSpan.getColSpan(), realFormat);
                            spannedCells
                                    .addAll(getSpannedCells(row, col, cellSpan.getRowSpan(), cellSpan.getColSpan()));
                        } else {
                            addCell(table, content, realFormat);
                        }
                    }
                }
            }
        }

        return table;
    }

    private static String getRowAndCol(int row, int col) {
        return String.format("%s:%s", row, col);
    }

    private static CellImage getCellImage(List<CellImage> images, String rowAndCol) {
        if (images != null) {
            for (CellImage image : images) {
                if (image.getPositions().contains(rowAndCol)) {
                    return image;
                }
            }
        }
        return null;
    }

    private static PdfFormat getRealFormat(PdfFormat format, CellSpan cellSpan, String rowAndCol)
            throws DocumentException {
        CellBorder cellBorder = getCellBorder(format.getCellBorders(), rowAndCol);
        CellAlignment cellAlignment = getCellAlignment(format.getCellAlignments(), rowAndCol);
        String border = cellBorder == null ? format.getBorder() : cellBorder.getBorder();
        String paragraphAlignment = cellAlignment == null ? format.getParagraphAlignment()
                : cellAlignment.getAlignment();
        Integer fontSize = format.getFontSize();
        String cellHorizontalAlignment = format.getCellHorizontalAlignment();
        Float cellPaddingTop = format.getCellPaddingTop();
        Float cellPaddingBottom = format.getCellPaddingBottom();

        // override format
        if (cellSpan != null) {
            fontSize = cellSpan.getFontSize() != null ? cellSpan.getFontSize() : fontSize;
            paragraphAlignment = cellSpan.getParagraphAlignment() != null ? cellSpan.getParagraphAlignment()
                    : paragraphAlignment;
            cellHorizontalAlignment = cellSpan.getCellHorizontalAlignment() != null
                    ? cellSpan.getCellHorizontalAlignment()
                    : cellHorizontalAlignment;
            border = cellSpan.getBorder() != null ? cellSpan.getBorder() : border;
            cellPaddingTop = cellSpan.getCellPaddingTop() != null ? cellSpan.getCellPaddingTop() : cellPaddingTop;
            cellPaddingBottom = cellSpan.getCellPaddingBottom() != null ? cellSpan.getCellPaddingBottom()
                    : cellPaddingBottom;
        }
        PdfFormat realFormat = null;
        try {
            realFormat = (PdfFormat) BeanUtils.cloneBean(format);
            realFormat.setFontSize(fontSize);
            realFormat.setCellHorizontalAlignment(cellHorizontalAlignment);
            realFormat.setBorder(border);
            realFormat.setParagraphAlignment(paragraphAlignment);
            realFormat.setCellPaddingTop(cellPaddingTop);
            realFormat.setCellPaddingBottom(cellPaddingBottom);
        } catch (Exception e) {
            throw new DocumentException(e);
        }
        return realFormat;
    }

    private static CellSpan getCellSpan(List<CellSpan> spans, String rowAndCol) {
        if (spans != null) {
            for (CellSpan span : spans) {
                if (span.getPositions().contains(rowAndCol)) {
                    return span;
                }
            }
        }
        return null;
    }

    private static CellBorder getCellBorder(List<CellBorder> borders, String rowAndCol) {
        if (borders != null) {
            for (CellBorder border : borders) {
                if (border.getPositions().contains(rowAndCol)) {
                    return border;
                }
            }
        }
        return null;
    }

    private static CellAlignment getCellAlignment(List<CellAlignment> alignments, String rowAndCol) {
        if (alignments != null) {
            for (CellAlignment alignment : alignments) {
                if (alignment.getPositions().contains(rowAndCol)) {
                    return alignment;
                }
            }
        }
        return null;
    }

    private static List<String> getSpannedCells(int row, int col, int rowSpan, int colSpan) {
        List<String> spannedCells = new ArrayList<>();
        int toRow = row + rowSpan;
        int toCol = col + colSpan;
        for (int r = row; r < toRow; r++) {
            for (int c = col; c < toCol; c++) {
                spannedCells.add(getRowAndCol(r, c));
            }
        }
        return spannedCells;
    }

    public static void addCellImage(PdfPTable table, String imagePath, PdfFormat format, CellImage cellImage)
            throws DocumentException, IOException {
        Image image = Image.getInstance(imagePath);
        image.scalePercent(cellImage.getScalePercent());

        PdfPCell cell = new PdfPCell();
        cell.setHorizontalAlignment(getAlignment(cellImage.getCellHorizontalAlignment(), PdfPCell.ALIGN_CENTER));
        cell.setVerticalAlignment(getAlignment(format.getCellVerticalAlignment(), PdfPCell.ALIGN_MIDDLE));
        setCellBorder(cell, format.getBorder());
        cell.setPaddingTop(format.getCellPaddingTop());
        cell.setPaddingBottom(format.getCellPaddingBottom());
        cell.addElement(image);
        cell.setBorderWidth(format.getBorderWidth());
        table.addCell(cell);
    }

    private static void addCellSpan(PdfPTable table, String content, int rowSpan, int colSpan, PdfFormat format)
            throws DocumentException, IOException {
        Paragraph paragraph = getParagraph(content, format);
        paragraph.setAlignment(getAlignment(format.getParagraphAlignment(), PdfPCell.ALIGN_CENTER));

        addCell(table, paragraph, rowSpan, colSpan, format);
    }

    private static void addCell(PdfPTable table, String content, PdfFormat format)
            throws DocumentException, IOException {
        Paragraph paragraph = getParagraph(content, format);
        paragraph.setLeading(format.getParagraphLeading());
        paragraph.setAlignment(getAlignment(format.getParagraphAlignment(), PdfPCell.ALIGN_CENTER));
        addCell(table, paragraph, 0, 0, format);
    }

    private static void addCell(PdfPTable table, Paragraph paragraph, int rowSpan, int colSpan, PdfFormat format)
            throws DocumentException, IOException {
        PdfPCell cell = new PdfPCell();
        if (rowSpan > 0) {
            cell.setRowspan(rowSpan);
        }
        if (colSpan > 0) {
            cell.setColspan(colSpan);
        }
        setCellBorder(cell, format.getBorder());
        cell.setHorizontalAlignment(getAlignment(format.getCellHorizontalAlignment(), PdfPCell.ALIGN_CENTER));
        cell.setVerticalAlignment(getAlignment(format.getCellVerticalAlignment(), PdfPCell.ALIGN_MIDDLE));
        cell.setPaddingTop(format.getCellPaddingTop());
        cell.setPaddingBottom(format.getCellPaddingBottom());
        cell.addElement(paragraph);
        cell.setBorderWidth(format.getBorderWidth());
        table.addCell(cell);
    }

    private static Paragraph getParagraph(String content, PdfFormat format) {
        Font font = null;
        if(content==null) {
        	 return new Paragraph(" ", font);
        }
        if (content.startsWith("#")) {
            font = getFont(format.getFont(), true, format.getFontSize());
            content = content.substring(1);
        } else {
            font = getFont(format.getFont(), false, format.getFontSize());
        }
        if (format.getCellPaddingTop() > 0) {
            if (EmptyUtil.isEmpty(content)) {
                content = StringUtils.SPACE;
            }
        }

        if(EmptyUtil.isNotEmpty(format.getFontColor())) {
            if (format.getFontColor().equals(FontColorEnum.red.toString())) {
                font.setColor(Color.RED);
            } else if (format.getFontColor().equals(FontColorEnum.black.toString())) {
                font.setColor(Color.black);
            } else if (format.getFontColor().equals(FontColorEnum.blue.toString())) {
                font.setColor(Color.blue);
            } else if (format.getFontColor().equals(FontColorEnum.gree.toString())) {
                font.setColor(Color.green);
            }
        }
        return new Paragraph(content, font);
    }

    private static Font getFont(String font, boolean isBold, int fontSize) {
        FontEnum fontEnum = FontEnum.valueOf(font);
        if (fontEnum == null) { // SIMSUN as default
            fontEnum = isBold ? FontEnum.SIMSUN_BOLD : FontEnum.SIMSUN_BOLD;
        } else if (isBold) {
            switch (fontEnum) {
            case SIMSUN:
                fontEnum = FontEnum.SIMSUN_BOLD;
                break;
            case MSYH:
                fontEnum = FontEnum.MSYH_BOLD;
                break;
            default:
            }
        }
        return FontHelper.getFont(fontEnum, fontSize);
    }

    private static int getAlignment(String alignment, int defaultAlignment) {
        try {
            if (EmptyUtil.isNotEmpty(alignment)) {
                return FieldUtils.getField(PdfPCell.class, alignment).getInt(null);
            }
        } catch (Exception e) {
            log.error("failed to parse alignment:" + alignment, e);
        }
        return defaultAlignment;
    }

    private static void setCellBorder(PdfPCell cell, String hexKey) throws DocumentException, IOException {
        String binKey = "0000";
        try {
            binKey = RegexNumberValidate.hexString2binaryString(hexKey);
            if (hexKey.length() == 4) {
                binKey = hexKey;
            }
        } catch (Exception e) {
            log.debug("error occurred.", e);
        }
        if (binKey.charAt(0) == NO_BORDER) {
            cell.disableBorderSide(Rectangle.TOP);
        }
        if (binKey.charAt(1) == NO_BORDER) {
            cell.disableBorderSide(Rectangle.BOTTOM);
        }
        if (binKey.charAt(2) == NO_BORDER) {
            cell.disableBorderSide(Rectangle.LEFT);
        }
        if (binKey.charAt(3) == NO_BORDER) {
            cell.disableBorderSide(Rectangle.RIGHT);
        }
    }

}

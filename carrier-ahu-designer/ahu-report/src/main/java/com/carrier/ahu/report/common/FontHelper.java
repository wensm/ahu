package com.carrier.ahu.report.common;

import static com.carrier.ahu.constant.CommonConstant.SYS_PATH_ARIAL_FONT;
import static com.carrier.ahu.constant.CommonConstant.SYS_PATH_CHINESEMSYHFONTPATH;
import static com.carrier.ahu.constant.CommonConstant.SYS_PATH_CHINESESONGTIFONTPATH;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by Braden Zhou on 2018/10/24.
 */
@Slf4j
public class FontHelper {

    private static BaseFont BASE_FONT_SIMSUN = null;
    private static BaseFont BASE_FONT_MSYH = null;
    private static BaseFont ARIAL_FONT = null;

    static {
        try {
            BASE_FONT_SIMSUN = BaseFont.createFont(SYS_PATH_CHINESESONGTIFONTPATH, BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED);
            BASE_FONT_MSYH = BaseFont.createFont(SYS_PATH_CHINESEMSYHFONTPATH, BaseFont.IDENTITY_H,
                    BaseFont.NOT_EMBEDDED);
            ARIAL_FONT = BaseFont.createFont(SYS_PATH_ARIAL_FONT, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
            log.error("Failed to create font for PDF report.", e);
        }
    }

    public static Font getFont(FontEnum fontType, int fontSize) {
        // use Arial for language other than Chinese
//        if (!LanguageEnum.Chinese.equals(AHUContext.getLanguage())) {
//            if (FontEnum.MSYH_BOLD.equals(fontType) || FontEnum.SIMSUN_BOLD.equals(fontType)) {
//                fontType = FontEnum.ARIAL_BOLD;
//            } else {
//                fontType = FontEnum.ARIAL;
//            }
//            log.debug("the font: {} is selected", fontType.name());
//        }

        switch (fontType) {
        case SIMSUN:
            return new Font(BASE_FONT_SIMSUN, fontSize, Font.NORMAL);
        case SIMSUN_BOLD:
            return new Font(BASE_FONT_SIMSUN, fontSize, Font.BOLD);
        case MSYH:
            return new Font(BASE_FONT_MSYH, fontSize, Font.NORMAL);
        case MSYH_BOLD:
            return new Font(BASE_FONT_MSYH, fontSize, Font.BOLD);
        case ARIAL:
            return new Font(ARIAL_FONT, fontSize, Font.NORMAL);
        case ARIAL_BOLD:
            return new Font(ARIAL_FONT, fontSize, Font.BOLD);
        }

        log.warn("the font： {} is not recognized", fontType.name());
        return null;
    }

}

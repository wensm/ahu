package com.carrier.ahu.report.common;

import com.carrier.ahu.constant.CommonConstant;

/**
 * Created by Braden Zhou on 2018/10/24.
 */
public class ReportConstants extends CommonConstant {

    public static final String FORMAT_PATH = "com/carrier/ahu/report/format/%s.json";
    public static final String CONTENT_PATH = "com/carrier/ahu/report/content/%s.json";

    /** 是否备份 **/
    public static final boolean BACKUPFILE_PDF = false;//是否备份：pdf技术说明
    public static final boolean BACKUPFILE_WORD = false;//是否备份：word技术说明
    public static final boolean BACKUPFILE_EXCELLDELIVERY = false;//是否备份：延期交货周期选项
    public static final boolean BACKUPFILE_PARALIST = false;//是否备份：段连接清单
    public static final boolean BACKUPFILE_EXCELPROLIST = false;//是否备份：项目清单
    public static final boolean BACKUPFILE_SAPPRICECODE = false;//是否备份：SAP Price Code
    public static final boolean BACKUPFILE_EXCELLCRMLIST = false;//是否备份：CRM清单
    public static final boolean BACKUPFILE_EXPORTEXCELGROUPBYTEMPLET = false;//是否备份：批量模板
    public static final boolean BACKUPFILE_EXPORTEXCELGROUP = false;//是否备份：批量导出[分组]

    /** 字体大小 文档 大标题 */
    public final static Integer TITLE_BIGGER = 32;
    /** 字体大小 表格 大表头 */
    public final static Integer TABLE_TITLE_BIGGER = 15;
    /** 字体大小 表格 常规表头 */
    public final static Integer TABLE_TITLE_NOMAL = 10;
    /** 字体大小 常规内容 */
    public final static Integer CONTENT_NOMAL = 8;
    /** 字体大小 常规内容1 */
    public final static Integer CONTENT_NOMAL_1 = 10;
    /** 字体大小 页眉 */
    public final static Integer HEADER_TITLE = 12;
    /** 字体大小 页脚 */
    public final static Integer FOOTER_TITLE = 10;
    /** 表格行间距 */
    public final static float LINE_SPACING_SMALL = 8f;
    /** 表格行间距 */
    public final static float LINE_SPACING_BIG = 18f;

    /** reports */
    public static final String REPORT_SEPARATOR = ".";
    public static final float DEFAULT_PARAGRAPH_LEADING = 8;
    public static final float DEFAULT_WIDTH_PERCENTAGE = 100;
    public static final float DEFAULT_PADDING = 0;
    public static final float DEFAULT_BORDER_WIDTH = 0;
    public static final int DEFAULT_FONT_SIZE = 8;
    public static final String DEFAULT_ALIGNMENT = "ALIGN_CENTER";
    public static final String DEFAULT_BORDER = "0";

    /** 奥雅纳工程格式报告 */
    public static final String REPORT_T_ARUPPF = "aruppf";

    /** 柏诚工程格式报告 */
    public static final String REPORT_T_BOCHENG_SPEC = "bocheng.spec";
    public static final String REPORT_T_BOCHENG_AHU = "bocheng.ahu";
    public static final String REPORT_T_BOCHENG_COOLINGCOIL = "bocheng.coolingCoil";
    public static final String REPORT_T_BOCHENG_FAN = "bocheng.fan";
    public static final String REPORT_T_BOCHENG_FILTER = "bocheng.filter";
    public static final String REPORT_T_BOCHENG_HEADER = "bocheng.header";
    public static final String REPORT_T_BOCHENG_HEATINGCOIL = "bocheng.heatingCoil";
    public static final String REPORT_T_BOCHENG_WET = "bocheng.wet";

    public static final String REPORT_TECH_HEADER = "tech.header";
    public static final String REPORT_TECH_HEADER_EURO_39CQ = "tech.header.euro.39CQ";
    public static final String REPORT_TECH_HEADER_EURO_39XT = "tech.header.euro.39XT";
    public static final String REPORT_TECH_AHU_OVERALL = "tech.ahu.overall";
    public static final String REPORT_TECH_AHU_DELIVERY = "tech.ahu.delivery";
    public static final String REPORT_TECH_MIX = "tech.mix";
    public static final String REPORT_TECH_MIX_SUMMER = "tech.mix.summer";
    public static final String REPORT_TECH_MIX_WINTER = "tech.mix.winter";
    public static final String REPORT_TECH_FILTER = "tech.filter";
    public static final String REPORT_TECH_COMBINEDFILTER = "tech.combinedFilter";
    public static final String REPORT_TECH_COMBINEDFILTER_APPENDIX = "tech.combinedFilter.appendix";
    public static final String REPORT_TECH_COOLINGCOIL = "tech.coolingCoil";
    public static final String REPORT_TECH_COOLINGCOIL_SUMMER = "tech.coolingCoil.summer";
    public static final String REPORT_TECH_COOLINGCOIL_WINTER = "tech.coolingCoil.winter";
    public static final String REPORT_TECH_HEATINGCOIL = "tech.heatingCoil";
    public static final String REPORT_TECH_STEAMCOIL = "tech.steamCoil";
    public static final String REPORT_TECH_STEAMCOIL_SUMMER = "tech.steamCoil.summer";
    public static final String REPORT_TECH_STEAMCOIL_WINTER = "tech.steamCoil.winter";
    public static final String REPORT_TECH_ELECTROSTATICFILTER = "tech.electrostaticFilter";
    public static final String REPORT_TECH_ELECTRODEHUMIDIFIER_APPENDIX = "tech.electrodeHumidifier.appendix";
    public static final String REPORT_TECH_ELECTRODEHUMIDIFIER = "tech.electrodeHumidifier";
    public static final String REPORT_TECH_ELECTRODEHUMIDIFIER_SUMMER = "tech.electrodeHumidifier.summer";
    public static final String REPORT_TECH_ELECTRODEHUMIDIFIER_WINTER = "tech.electrodeHumidifier.winter";
    public static final String REPORT_TECH_ELECTRICHEATINGCOIL = "tech.electricHeatingCoil";
    public static final String REPORT_TECH_ELECTRICHEATINGCOIL_SUMMER = "tech.electricHeatingCoil.summer";
    public static final String REPORT_TECH_ELECTRICHEATINGCOIL_WINTER = "tech.electricHeatingCoil.winter";
    public static final String REPORT_TECH_STEAMHUMIDIFIER = "tech.steamHumidifier";
    public static final String REPORT_TECH_STEAMHUMIDIFIER_SUMMER = "tech.steamHumidifier.summer";
    public static final String REPORT_TECH_STEAMHUMIDIFIER_WINTER = "tech.steamHumidifier.winter";
    public static final String REPORT_TECH_WETFILMHUMIDIFIER = "tech.wetfilmHumidifier";
    public static final String REPORT_TECH_WETFILMHUMIDIFIER_SUMMER = "tech.wetfilmHumidifier.summer";
    public static final String REPORT_TECH_WETFILMHUMIDIFIER_WINTER = "tech.wetfilmHumidifier.winter";
    public static final String REPORT_TECH_SPRAYHUMIDIFIER = "tech.sprayHumidifier";
    public static final String REPORT_TECH_SPRAYHUMIDIFIER_APPENDIX = "tech.sprayHumidifier.appendix";
    public static final String REPORT_TECH_SPRAYHUMIDIFIER_SUMMER = "tech.sprayHumidifier.summer";
    public static final String REPORT_TECH_SPRAYHUMIDIFIER_WINTER = "tech.sprayHumidifier.winter";
    public static final String REPORT_TECH_FAN = "tech.fan";
    public static final String REPORT_TECH_FAN_EURO = "tech.fan.euro";
    public static final String REPORT_TECH_FAN_SOUND = "tech.fan.sound";
    public static final String REPORT_TECH_FAN_ABSORBER = "tech.fan.absorber";
    public static final String REPORT_TECH_FAN_CURVE = "tech.fan.curve";
    public static final String REPORT_TECH_FAN_REMARK = "tech.fan.remark";
    public static final String REPORT_TECH_FAN_AHRI = "tech.fan.ahri";
    public static final String REPORT_TECH_FAN_AHRI_WORD = "tech.fan.ahri.word";
    public static final String REPORT_TECH_COMBINEDMIXINGCHAMBER = "tech.combinedMixingChamber";
    public static final String REPORT_TECH_COMBINEDMIXINGCHAMBER_SUMMER = "tech.combinedMixingChamber.summer";
    public static final String REPORT_TECH_COMBINEDMIXINGCHAMBER_WINTER = "tech.combinedMixingChamber.winter";
    public static final String REPORT_TECH_ATTENUATOR = "tech.attenuator";
    public static final String REPORT_TECH_DISCHARGE = "tech.discharge";
    public static final String REPORT_TECH_ACCESS = "tech.access";
    public static final String REPORT_TECH_HEPAFILTER = "tech.HEPAFilter";
    public static final String REPORT_TECH_CTR = "tech.ctr";
    public static final String REPORT_TECH_HEATRECYCLE = "tech.heatRecycle";
    public static final String REPORT_TECH_HEATRECYCLE_DELIVERY = "tech.heatRecycle.delivery";
    public static final String REPORT_TECH_NONSTANDARD = "technonStandard";
    public static final String REPORT_TECH_DIRECTEXPENSIONCOIL = "tech.directExpensionCoil";
    public static final String REPORT_TECH_DIRECTEXPENSIONCOIL_WINTER = "tech.directExpensionCoil.winter";
    public static final String REPORT_TECH_PLATEHEATRECYCLE = "tech.plateHeatRecycle";
    public static final String REPORT_TECH_PLATEHEATRECYCLE_DELIVERY = "tech.plateHeatRecycle.delivery";
    public static final String REPORT_TECH_PRODUCTPACKINGLIST = "tech.productPackingList";
    public static final String REPORT_TECH_PRODUCTPACKINGLIST_EXPORT = "tech.productPackingList.export";
    public static final String REPORT_TECH_UNITNAMEPLATEDATA = "tech.unitNameplateData";
    public static final String REPORT_TECH_UNITNAMEPLATEDATA_SHAPE = "tech.unitNameplateData.shape";
    public static final String REPORT_TECH_SECTION_CONNECTION_LIST = "tech.sectionConnectionList";
    public static final String REPORT_TECH_SECTION_CONNECTION_LIST_STANDARD = "tech.sectionConnectionList.standard";
    public static final String REPORT_TECH_SECTION_CONNECTION_LIST_FOREPART = "tech.sectionConnectionList.forepart";
    public static final String REPORT_TECH_SECTION_CONNECTION_LIST_POSITIVE = "tech.sectionConnectionList.positive";
    public static final String REPORT_TECH_SECTION_CONNECTION_LIST_VERTICAL = "tech.sectionConnectionList.vertical";

    public static final String REPORT_SALES_AHU_OVERALL = "sales.ahu.overall";
    public static final String REPORT_SALES_AHU_DELIVERY = "sales.ahu.delivery";
    public static final String REPORT_SALES_MIX = "sales.mix";
    public static final String REPORT_SALES_MIX_SUMMER = "sales.mix.summer";
    public static final String REPORT_SALES_MIX_WINTER = "sales.mix.winter";
    public static final String REPORT_SALES_FILTER = "sales.filter";
    public static final String REPORT_SALES_COMBINEDFILTER = "sales.combinedFilter";
    public static final String REPORT_SALES_COOLINGCOIL = "sales.coolingCoil";
    public static final String REPORT_SALES_COOLINGCOIL_WINTER = "sales.coolingCoil.winter";
    public static final String REPORT_SALES_HEATINGCOIL = "sales.heatingCoil";
    public static final String REPORT_SALES_STEAMCOIL = "sales.steamCoil";
    public static final String REPORT_SALES_STEAMCOIL_SUMMER = "sales.steamCoil.summer";
    public static final String REPORT_SALES_STEAMCOIL_WINTER = "sales.steamCoil.winter";
    public static final String REPORT_SALES_ELECTROSTATICFILTER = "sales.electrostaticFilter";
    public static final String REPORT_SALES_ELECTRODEHUMIDIFIER = "sales.electrodeHumidifier";
    public static final String REPORT_SALES_ELECTRODEHUMIDIFIER_SUMMER = "sales.electrodeHumidifier.summer";
    public static final String REPORT_SALES_ELECTRODEHUMIDIFIER_WINTER = "sales.electrodeHumidifier.winter";
    public static final String REPORT_SALES_ELECTRICHEATINGCOIL = "sales.electricHeatingCoil";
    public static final String REPORT_SALES_ELECTRICHEATINGCOIL_SUMMER = "sales.electricHeatingCoil.summer";
    public static final String REPORT_SALES_ELECTRICHEATINGCOIL_WINTER = "sales.electricHeatingCoil.winter";
    public static final String REPORT_SALES_STEAMHUMIDIFIER = "sales.steamHumidifier";
    public static final String REPORT_SALES_STEAMHUMIDIFIER_SUMMER = "sales.steamHumidifier.summer";
    public static final String REPORT_SALES_STEAMHUMIDIFIER_WINTER = "sales.steamHumidifier.winter";
    public static final String REPORT_SALES_WETFILMHUMIDIFIER = "sales.wetfilmHumidifier";
    public static final String REPORT_SALES_WETFILMHUMIDIFIER_SUMMER = "sales.wetfilmHumidifier.summer";
    public static final String REPORT_SALES_WETFILMHUMIDIFIER_WINTER = "sales.wetfilmHumidifier.winter";
    public static final String REPORT_SALES_SPRAYHUMIDIFIER = "sales.sprayHumidifier";
    public static final String REPORT_SALES_SPRAYHUMIDIFIER_SUMMER = "sales.sprayHumidifier.summer";
    public static final String REPORT_SALES_SPRAYHUMIDIFIER_WINTER = "sales.sprayHumidifier.winter";
    public static final String REPORT_SALES_FAN = "sales.fan";
    public static final String REPORT_SALES_FAN_SOUND = "sales.fan.sound";
    public static final String REPORT_SALES_FAN_ABSORBER = "sales.fan.absorber";
    public static final String REPORT_SALES_FAN_CURVE = "sales.fan.curve";
    public static final String REPORT_SALES_FAN_REMARK = "sales.fan.remark";
    public static final String REPORT_SALES_COMBINEDMIXINGCHAMBER = "sales.combinedMixingChamber";
    public static final String REPORT_SALES_COMBINEDMIXINGCHAMBER_SUMMER = "sales.combinedMixingChamber.summer";
    public static final String REPORT_SALES_COMBINEDMIXINGCHAMBER_WINTER = "sales.combinedMixingChamber.winter";
    public static final String REPORT_SALES_ATTENUATOR = "sales.attenuator";
    public static final String REPORT_SALES_DISCHARGE = "sales.discharge";
    public static final String REPORT_SALES_ACCESS = "sales.access";
    public static final String REPORT_SALES_HEPAFILTER = "sales.HEPAFilter";
    public static final String REPORT_SALES_CTR = "sales.ctr";
    public static final String REPORT_SALES_HEATRECYCLE = "sales.heatRecycle";
    public static final String REPORT_SALES_HEATRECYCLE_DELIVERY = "sales.heatRecycle.delivery";
    public static final String REPORT_SALES_NONSTANDARD = "sales.nonStandard";
    public static final String REPORT_SALES_DIRECTEXPENSIONCOIL = "sales.directExpensionCoil";
    public static final String REPORT_SALES_DIRECTEXPENSIONCOIL_WINTER = "sales.directExpensionCoil.winter";
    public static final String REPORT_SALES_PLATEHEATRECYCLE = "sales.plateHeatRecycle";
    public static final String REPORT_SALES_PLATEHEATRECYCLE_DELIVERY = "sales.plateHeatRecycle.delivery";
    public static final String REPORT_SALES_PRODUCTPACKINGLIST = "sales.productPackingList";
    public static final String REPORT_SALES_WHEELHEATRECYCLE = "sales.wheelHeatRecycle";

    /** */
    public static final String REPORT_DOC_PARALIST_AHU = "doc.paralist.ahu";
    public static final String REPORT_DOC_PARALIST_MIX = "doc.paralist.mix";
    public static final String REPORT_DOC_PARALIST_COOLINGCOIL = "doc.paralist.coolingCoil";
    public static final String REPORT_DOC_PARALIST_WINTERCOOLINGCOIL = "doc.paralist.winterCoolingCoil";
    public static final String REPORT_DOC_PARALIST_FAN = "doc.paralist.fan";
    public static final String REPORT_DOC_PARALIST_HEATINGCOIL = "doc.paralist.heatingCoil";
    public static final String REPORT_DOC_PARALIST_OTHER = "doc.paralist.other";
    public static final String REPORT_DOC_PARALIST_STEAMHUMIDIFIER = "doc.paralist.steamHumidifier";
    public static final String REPORT_DOC_PARALIST_WETFILMHUMIDIFIER = "doc.paralist.wetFilmHumidifier";
    public static final String REPORT_DOC_PARALIST_SPRAYHUMIDIFIER = "doc.paralist.sprayHumidifier";
    public static final String REPORT_DOC_PARALIST_ELECTRODEHUMIDIFIER = "doc.paralist.electrodeHumidifier";
    
    /** */
    public static final String REPORT_NS_AHU = "ns.ahu";
    public static final String REPORT_NS_AHU_NONSTANDARD = "ns.ahu.nonStandard";
    public static final String REPORT_NS_AHU_NONSTANDARD_MEMO = "ns.ahu.nonStandard.memo";
    public static final String REPORT_NS_COMMON = "ns.common";
    public static final String REPORT_NS_COMMON_MEMO = "ns.common.memo";
    public static final String REPORT_NS_COOLINGCOIL = "ns.coolingCoil";
    public static final String REPORT_NS_COOLINGCOIL_MEMO = "ns.coolingCoil.memo";
    public static final String REPORT_NS_FAN = "ns.fan";
    public static final String REPORT_NS_ACCESS = "ns.access";
    public static final String REPORT_NS_ACCESS_MEMO = "ns.access.memo";
    public static final String REPORT_NS_FAN_MEMO = "ns.fan.memo";
    public static final String REPORT_NS_FILTER = "ns.filter";
    public static final String REPORT_NS_FILTER_MEMO = "ns.filter.memo";
    public static final String REPORT_NS_COMPOSITE = "ns.combinedfilter";
    public static final String REPORT_NS_COMPOSITE_MEMO = "ns.combinedfilter.memo";
    public static final String REPORT_NS_HEPAFILTER = "ns.hepafilter";
    public static final String REPORT_NS_HEPAFILTER_MEMO = "ns.hepafilter.memo";
    public static final String REPORT_NS_HEATINGCOIL = "ns.heatingCoil";
    public static final String REPORT_NS_HEATINGCOIL_MEMO = "ns.heatingCoil.memo";
    public static final String REPORT_NS_SPRAYHUMIDIFIER = "ns.sprayHumidifier";
    public static final String REPORT_NS_SPRAYHUMIDIFIER_MEMO = "ns.sprayHumidifier.memo";
    public static final String REPORT_NS_STEAMHUMIDIFIER = "ns.steamHumidifier";
    public static final String REPORT_NS_STEAMHUMIDIFIER_MEMO = "ns.steamHumidifier.memo";
    public static final String REPORT_NS_WETFILMHUMIDIFIER = "ns.wetfilmHumidifier";
    public static final String REPORT_NS_WETFILMHUMIDIFIER_MEMO = "ns.wetfilmHumidifier.memo";
    
    /** 非标清单模板字段  */
    public static final String SPECIALLIST_ATTR_DATE = "date";
    public static final String SPECIALLIST_ATTR_TIME = "time";
    public static final String SPECIALLIST_ATTR_NO = "no";
    public static final String SPECIALLIST_ATTR_SYSTEMVERSION = "SystemVersion";
    public static final String SPECIALLIST_ATTR_CONTRACT = "contract";
    public static final String SPECIALLIST_ATTR_NAME = "name";
    public static final String SPECIALLIST_ATTR_SALER = "saler";
    public static final String SPECIALLIST_ATTR_ADDRESS = "address";
    public static final String SPECIALLIST_ATTR_ENQUIRYNO = "enquiryNo";
    public static final String SPECIALLIST_ATTR_GZL = "gzl";
    public static final String SPECIALLIST_ATTR_CUSTOMERPO = "po";
    
    /** Classify */
    public static final String CLASSIFY_TVIEW = "tView";
    public static final String CLASSIFY_HANSHI = "hanshi";
    public static final String CLASSIFY_FAN = "fan";
    public static final String CLASSIFY_PANEL_SCREENSHOTS = "panelScreenshots";
    public static final String CLASSIFY_UNIT_NAMEPLATE_DATA = "unitNameplateData";
    public static final String CLASSIFY_SECTION_CONNECTION_LIST = "sectionConnectionList";
    public static final String CLASSIFY_PRODUCT_PACKING_LIST = "productPackingList";

}

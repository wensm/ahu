package com.carrier.ahu.tool.comparecoderule;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.util.ExcelUtils;

/**
 * 本类用来比较price包下的coderule.xlsx文件和它的更新版本
 * @author WANGD1
 *
 */
public class CompareCodeRule {
	
	private static Logger logger = LoggerFactory.getLogger(CompareCodeRule.class.getName());
	
	public String excelHandl() {
		//read old and new (map)
		Map<String, List<String>> newmap = this.readAct("d:/myDocuments/20180508/new_coderule.xlsx","new_coderule.xlsx","new");
		Map<String, List<String>> oldmap = this.readAct("d:/myDocuments/20180508/coderule.xlsx","coderule.xlsx","old");
		//compare
		this.compareTwo(oldmap , newmap);
		//output result
		String out = JSON.toJSONString(newmap);
		
		return out;
	}
	
	/**
	 * 读取操作
	 * @param filePath	文件路径
	 * @param fileName	文件名称
	 * @param type	读取类型  old/new
	 * @return
	 */
	private Map<String, List<String>> readAct(String filePath,String fileName,String type){
		Map<String, List<String>> retMap = new HashMap<String,List<String>>();
		Map<String, String> sheetMap = this.wrapMapofSheetname();
		if("old".equals(type)) {//old
			for(String sheetName : sheetMap.keySet()) {//读取所有的sheet页的内容
				List<String> rowContent = new ArrayList<String>();
				List<List<String>> sheetList = this.readExcel(filePath, fileName, sheetName);
				for(List<String> rowlist : sheetList) {//读取sheet页的每一行
					String cellStr = "";
					for(String cell : rowlist) {//将每一行转换为string字符串
						cellStr += "#"+cell;
					}
					rowContent.add(cellStr);
				}
				retMap.put(sheetMap.get(sheetName), rowContent);
			}
		}else {//new
			Set<String> sheetKeySet = sheetMap.keySet();
			Set<String> sheetValueSet = new HashSet<String>();
			for(String key : sheetKeySet) {
				sheetValueSet.add(sheetMap.get(key));
			}
			
			for(String sheetName : sheetValueSet) {//读取所有的sheet页的内容
				List<String> rowContent = new ArrayList<String>();
				List<List<String>> sheetList = this.readExcel(filePath, fileName, sheetName);
				for(List<String> rowlist : sheetList) {//读取sheet页的每一行
					String cellStr = "";
					int count = 0;
					for(String cell : rowlist) {//将每一行转换为string字符串
						if(count == 0) {
							count++;
							continue;
						}else if(count == 2) {
							if(cell.endsWith("_1")) {
								cell = cell.substring(0, cell.length()-2);
							}
						}
						cellStr += "#"+cell;
						count++;
					}
					if(cellStr.endsWith("###")) {
						cellStr = cellStr.substring(0,cellStr.length()-2);
					}
					rowContent.add(cellStr);
				}
				retMap.put(sheetName, rowContent);
			}
		}

		return retMap;
	}
	
	/**
	 * 比较从两个excel中读取的内容，将相同部分剔除(获取newMap中与oldMap不同的部分)
	 * @param oldMap
	 * @param newMap
	 * @return Map<String,List<String>>
	 */
	private void compareTwo(Map<String, List<String>> oldMap, Map<String, List<String>> newMap){
			
		for(String key : newMap.keySet()) {//遍历newMap中每一个sheet页内容
			List<String> tempList = new ArrayList<String>();//用来暂存相同内容的元素
			
			List<String> newSheetContent = newMap.get(key);//获取newMap中对应的sheet页内容
			List<String> oldSheetContent = oldMap.get(key);//获取oldMap中对应的sheet页内容
			for(String newContent : newSheetContent) {//取每一行的字符串
				for(String oldContent : oldSheetContent) {//遍历oldMap对于sheet页的每一行
					if(oldContent.contains(newContent)) {//若字符串相同，则从newMap中去除该字符串
//						newSheetContent.remove(newContent);
						tempList.add(newContent);
						continue;
					}
				}
			}
			
//			Iterator<String> iterator = newSheetContent.listIterator();
//			while(iterator.hasNext()) {//取每一行的字符串
//				String newContent = iterator.next();
//				List<String> oldSheetContent = oldMap.get(key);//获取oldMap中对应的sheet页内容
//					if(oldContent.contains(newContent)) {//若字符串相同，则从newMap中去除该字符串
////						iterator.remove();
//						tempSet.add(count);
//					}
//				}
//				count++;
//			}
			for(String temp : tempList) {
				newSheetContent.remove(temp);
			}
		}
		
	}
	
	/**
	 * 封装两个excel具有映射关系的sheet页名
	 * @return map
	 */
	private Map<String, String> wrapMapofSheetname(){
		Map<String, String> map = new HashMap<String,String>();
		
		map.put("box", "箱体");
		map.put("ahu.mix", "混合段1");
		map.put("ahu.filter", "过滤段1");
		map.put("ahu.combinedFilter", "综合过滤段1");
		map.put("ahu.coolingCoil", "冷水盘管段1");
		map.put("ahu.heatingCoil", "热水盘管段1");
		map.put("ahu.steamCoil", "蒸汽盘管段1");
		map.put("ahu.electricHeatingCoil", "电加热盘管段1");
		map.put("ahu.steamHumidifier", "干蒸汽加湿段1");
		map.put("ahu.wetfilmHumidifier", "湿膜加湿段1");
		map.put("ahu.sprayHumidifier", "高压喷雾加湿段1");
		map.put("ahu.fan", "风机段1");
		map.put("ahu.combinedMixingChamber", "新回排风段");
		map.put("ahu.attenuator", "消音段1");
		map.put("ahu.discharge", "出风段1");
		map.put("ahu.access", "空段1");
		map.put("ahu.HEPAFilter", "高效过滤段1");
		map.put("ahu.heatRecycle", "热回收段");
		map.put("ahu.electrodeHumidifier", "电极加湿1");
		map.put("ahu.electrostaticFilter", "静电过滤器1");
		map.put("ahu.ctr", "控制段");
		
		return map;
	}
	
	/**
	 * 根据文件路径，文件名和sheet页名读取excel内容。
	 * @param fileName
	 * @param sheetName
	 * @return
	 */
	private List<List<String>> readExcel(String filePath, String fileName, String sheetName) {
		List<List<String>> list = Collections.emptyList();

		try {
			File file = new File(filePath);
//			InputStream is = this.getClass().getClassLoader().getResourceAsStream(fileName);
			InputStream is = new FileInputStream(file);
			list = ExcelUtils.read(is, ExcelUtils.isExcel2003(fileName), sheetName, 1);
		} catch (EncryptedDocumentException e) {
			logger.error("Failed to load excel: " + fileName, e);
			return list;
		} catch (InvalidFormatException e) {
			logger.error("Failed to load excel: " + fileName, e);
			return list;
		} catch (IOException e) {
			logger.error("Failed to load excel: " + fileName, e);
			return list;
		}

		return list;
	}
	
	public static void main(String[] args) {
		CompareCodeRule ccr = new CompareCodeRule();
		String out = ccr.excelHandl();
		System.out.println("============result==============");
		System.out.println(out);
	}
}
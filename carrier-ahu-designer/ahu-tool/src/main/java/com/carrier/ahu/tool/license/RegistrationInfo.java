package com.carrier.ahu.tool.license;

import java.util.Date;

import com.carrier.ahu.license.LicenseInfo;
import com.carrier.ahu.util.DateUtil;

import lombok.Data;

/**
 * Created by Braden Zhou on 2019/07/24.
 */
@Data
public class RegistrationInfo {

    private String email;
    private String mac;
    private String userName;
    private String computerName;
    private String domain;
    private String registrationDate;
    private String effectiveDate;
    private String factory;
    private String role;
    private String version;
    private String patchVersion;

    public RegistrationInfo() {
    }

    public RegistrationInfo(String email, LicenseInfo licenseInfo) {
        this.email = email;
        this.mac = licenseInfo.getMac();
        this.userName = licenseInfo.getUserName();
        this.computerName = licenseInfo.getComputerName();
        this.domain = licenseInfo.getDomain();
        this.registrationDate = DateUtil.dateToStr(new Date());
        this.effectiveDate = licenseInfo.getDate();
        this.factory = licenseInfo.getFactory();
        this.role = licenseInfo.getRole();
        this.version = licenseInfo.getVersion();
        this.patchVersion = licenseInfo.getPatchVersion();
    }

}

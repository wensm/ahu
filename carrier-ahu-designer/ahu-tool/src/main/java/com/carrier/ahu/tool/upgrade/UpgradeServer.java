package com.carrier.ahu.tool.upgrade;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

import com.carrier.ahu.common.upgrade.MessageType;
import com.carrier.ahu.common.upgrade.PacketTransfer;
import com.carrier.ahu.common.upgrade.UpgradeConfig;
import com.carrier.ahu.common.upgrade.UpgradeMessage;
import com.carrier.ahu.common.upgrade.UpgradePacket;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.license.LicenseInfo;
import com.carrier.ahu.util.DateUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Created by Braden Zhou on 2018/07/16.
 */
@Slf4j
public class UpgradeServer {

    public static void startUpgradeServer() throws IOException {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(CommonConstant.SYS_UPGRADE_UPGRADE_PORT);
            while (true) {
                Socket socket = serverSocket.accept();
                new Thread(new UpgradeThread(socket)).start();
            }
        } catch (Exception ex) {
            log.error("Error happened", ex);
        } finally {
            if (serverSocket != null && !serverSocket.isClosed()) {
                serverSocket.close();
            }
        }
    }

    public static String getReleaseVersion(String factory) {
        Properties releaseProp = new Properties();
        try {
            InputStream inputStream = new FileInputStream(System.getProperty("user.dir")+"/"+CommonConstant.SYS_UPGRADE_AHU_UPGRADE_FOLDER+CommonConstant.SYS_UPGRADE_RELEASE_PROPERTY_FILE);
            releaseProp.load(inputStream);
            return releaseProp.getProperty(String.format(CommonConstant.SYS_UPGRADE_LATEST_RELEASE_VERSION, factory));
        } catch (Exception e) {
            log.error("Failed to load new release version", e);
        }
        return null;
    }

    private static class UpgradeThread implements Runnable {

        private Socket socket;
        private String releaseVersion;
        private File releaseFile;
        private LicenseInfo licenseInfo;

        public UpgradeThread(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                UpgradePacket packet = this.sendAndReceiveMessage(MessageType.LOGIN);
                while (!socket.isClosed()) {
                    packet = this.handleMessage(packet);
                }
            } catch (Exception ex) {
                log.error("Error happened", ex);
            } finally {
                try {
                    if (!socket.isClosed()) {
                        this.sendMessage(MessageType.ERROR);
                        socket.close();
                    }
                } catch (IOException ex) {
                    log.error("Error happened", ex);
                }
            }
        }

        public UpgradePacket handleMessage(UpgradePacket packet) throws IOException {
            if (packet != null) {
                Gson gson = new Gson();
                UpgradeMessage message = gson.fromJson(packet.getBody(), UpgradeMessage.class);
                MessageType messageType = MessageType.valueOf(message.getType());
                log.error("msgTypeName:"+messageType.name());
                log.error("msgContent:"+message.getContent());
                switch (messageType) {
                case LOGIN:
                    // check if the valid license info
                    this.licenseInfo = new Gson().fromJson(message.getContent(), LicenseInfo.class);
                    if (this.licenseInfo != null
                            && DateUtil.getNow().before(DateUtil.strToDate(licenseInfo.getDate()))) {
                        this.releaseVersion = getReleaseVersion(licenseInfo.getFactory());
                        if (EmptyUtil.isNotEmpty(releaseVersion)) {
                            return this.sendAndReceiveMessage(MessageType.RELEASE);
                        }
                    }
                    this.sendMessage(MessageType.ERROR);
                    socket.close();
                    break;
                case RELEASE:
                    if (CommonConstant.SYS_UPGRADE_RELEASE_MAJOR.equals(message.getContent())) {
                        this.releaseFile = new File(System.getProperty("user.dir")+"/"+UpgradeConfig.getUpdateFileName(CommonConstant.SYS_UPGRADE_RELEASE_MAJOR,
                                licenseInfo.getFactory(), licenseInfo.getLanguageCode(), releaseVersion));
                    } else {
                        this.releaseFile = new File(System.getProperty("user.dir")+"/"+UpgradeConfig.getUpdateFileName(CommonConstant.SYS_UPGRADE_RELEASE_MINOR,
                                licenseInfo.getFactory(), licenseInfo.getLanguageCode(), releaseVersion));
                    }
                    if (this.releaseFile.exists()) {
                        log.error("releaseFile exists:"+this.releaseFile.getPath());
                        return this.sendAndReceiveMessage(MessageType.DOWNOADING,
                                String.valueOf(this.releaseFile.length()));
                    } else {
                        log.error("releaseFile not exists:"+this.releaseFile.getPath());
                        this.sendMessage(MessageType.ERROR);
                        socket.close();
                    }
                    break;
                case READY:
                    this.handleDownload();
                    return this.sendAndReceiveMessage(MessageType.DONE);
                case CONFIRM:
                    socket.close();
                    break;
                default:
                }
            }
            return null;
        }

        private UpgradePacket sendAndReceiveMessage(MessageType messageType) throws IOException {
            return this.sendAndReceiveMessage(messageType, null);
        }

        private UpgradePacket sendAndReceiveMessage(MessageType messageType, String content) throws IOException {
            this.sendMessage(messageType, content);
            return PacketTransfer.receivePacket(socket);
        }

        private void sendMessage(MessageType messageType) throws IOException {
            this.sendMessage(messageType, null);
        }

        private void sendMessage(MessageType messageType, String content) throws IOException {
            log.info(messageType.name());
            UpgradeMessage message = new UpgradeMessage();
            message.setType(messageType.name());
            message.setContent(content);

            String messageString = new Gson().toJson(message);
            UpgradePacket packet = new UpgradePacket(messageString);
            PacketTransfer.sendPacket(socket, packet);
        }

        private void handleDownload() throws IOException {
            OutputStream os = socket.getOutputStream();
            int length = 0;
            byte[] buffer = new byte[1024];
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(this.releaseFile));
            while ((length = bis.read(buffer)) != -1) {
                os.write(buffer, 0, length);
            }
            os.flush();
            bis.close();
        }

    }

}

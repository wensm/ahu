package com.carrier.ahu.tool.license;

import static com.carrier.ahu.tool.common.ToolConstants.ARRAY_SEPARATOR;
import static com.carrier.ahu.tool.common.ToolConstants.LICENSE_FILE_EXTENSION;
import static com.carrier.ahu.tool.common.ToolConstants.LICENSE_FILE_NAME;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_EFFECT_TIME_LIST;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_EMPTY_EFFECTIVE_TIME;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_EMPTY_EMAIL;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_EMPTY_FACTORY;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_EMPTY_ROLE;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_ERROR;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_FAILED_TO_GENERATE_AUTH_LICENSE_FILE;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_FAILED_TO_OPEN_GENERATE_AUTH_LICENSE_FILE_FOLDER;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_FAILED_TO_PARSE_LICENSE_FILE;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_FAILED_TO_PARSE_LICENSE_KEY;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_GENERATE_AUTH_LICENSE_FILE_SUCCESS;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_INFORMATION;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_LICENSE_FILE;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_OPEN_FILE_FAILURE;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_OPEN_LICENSE_FILE;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_PARSE_LICENSE_KEY_SUCCESS;
import static com.carrier.ahu.tool.common.ToolConstants.TEXT_ROLE_LIST;

import java.awt.Desktop;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.beanutils.BeanUtils;
import org.assertj.core.util.Lists;

import com.carrier.ahu.common.util.ReflectionUtils;
import com.carrier.ahu.license.LicenseInfo;
import com.carrier.ahu.license.LicenseManager;
import com.carrier.ahu.util.EmptyUtil;

import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import cn.hutool.core.text.csv.CsvWriter;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

/**
 * Created by Braden Zhou on 2018/04/19.
 */
public class LicenseController implements Initializable {

    static Logger logger = Logger.getLogger("LicenseTool");

    static {
        try {
            FileHandler fh = new FileHandler("license-tool.log");
            fh.setFormatter(new SimpleFormatter());
            logger.addHandler(fh);
        } catch (SecurityException | IOException e) {
        }
    }

    @FXML
    Menu help;
    @FXML
    private Label licenseFileLabel;
    @FXML
    private Label macLabel;
    @FXML
    private Label userNameLabel;
    @FXML
    private Label computerNameLabel;
    @FXML
    private Label computerDomainLabel;
    @FXML
    private Label ahuVersionLabel;
    @FXML
    private Label ahuPatchVersionLabel;
    @FXML
    private TextField factoryText;
    @FXML
    private TextField emailText;
    @FXML
    private TextArea licenseText;
    @FXML
    private ComboBox<String> effectTimeComBox;
    @FXML
    private ComboBox<String> roleComBox;
    @FXML
    private Button openLicenseButton;
    @FXML
    private Button generateLicenseButton;
    @FXML
    private Button openGenLicenseButton;

    private LicenseInfo licenseInfo;
    private File licenseFile;

    private ResourceBundle bundle;

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        this.bundle = bundle;
        String[] effectTimes = getString(TEXT_EFFECT_TIME_LIST).split(ARRAY_SEPARATOR);
        ObservableList<String> effectTimeList = FXCollections.observableArrayList(effectTimes);
        effectTimeComBox.setItems(effectTimeList);

        String[] roles = getString(TEXT_ROLE_LIST).split(ARRAY_SEPARATOR);
        ObservableList<String> roleList = FXCollections.observableArrayList(roles);
        roleComBox.setItems(roleList);
        this.openGenLicenseButton.setDisable(true);
        licenseText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try {
                    if (EmptyUtil.isNotEmpty(licenseText.getText())) {
                        parseUserLicenseKey(licenseText.getText());
                        openGenLicenseButton.setDisable(true);
                    }
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Failed to parse license file.", e);
                    showErrorDialog(TEXT_FAILED_TO_PARSE_LICENSE_FILE);
                }
            }
        });
    }

    @FXML
    private void handleOpenLicenseButtonAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(getString(TEXT_OPEN_LICENSE_FILE));
        fileChooser.getExtensionFilters()
                .add(new FileChooser.ExtensionFilter(getString(TEXT_LICENSE_FILE), LICENSE_FILE_EXTENSION));
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            licenseFileLabel.setText(file.getAbsolutePath());
            try {
                parseUserLicenseFile(file);
                this.openGenLicenseButton.setDisable(true);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Failed to parse license file.", e);
                this.showErrorDialog(TEXT_FAILED_TO_PARSE_LICENSE_FILE);
            }
        }
    }

    private void parseUserLicenseFile(File licenseFile) throws Exception {
        licenseInfo = LicenseManager.getLicenseInfo(licenseFile);
        this.macLabel.setText(licenseInfo.getMac());
        this.userNameLabel.setText(licenseInfo.getUserName());
        this.computerNameLabel.setText(licenseInfo.getComputerName());
        this.computerDomainLabel.setText(licenseInfo.getDomain());
        this.ahuVersionLabel.setText(licenseInfo.getVersion());
        this.ahuPatchVersionLabel.setText(licenseInfo.getPatchVersion());
    }

    @FXML
    private void handleGenerateLicenseButtonAction(ActionEvent event) {
        if (EmptyUtil.isEmpty(this.emailText.getText())) {
            this.showErrorDialog(TEXT_EMPTY_EMAIL);
        } else if (EmptyUtil.isEmpty(this.effectTimeComBox.getValue())) {
            this.showErrorDialog(TEXT_EMPTY_EFFECTIVE_TIME);
        } else if (EmptyUtil.isEmpty(this.factoryText.getText())) {
            this.showErrorDialog(TEXT_EMPTY_FACTORY);
        } else if (EmptyUtil.isEmpty(this.roleComBox.getValue())) {
            this.showErrorDialog(TEXT_EMPTY_ROLE);
        } else {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File directory = directoryChooser.showDialog(null);
            if (directory != null) {
                generateAuthLicenseFile(directory);
            }
        }
    }

    @FXML
    private void handleOpenRegFileAction(ActionEvent event) {
        try {
            Desktop.getDesktop().open(this.getRegistrationDataFile());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "failed to open file: " + this.getRegistrationDataFile().getAbsolutePath(), e);
            showErrorDialog(TEXT_OPEN_FILE_FAILURE);
        }
    }

    @FXML
    private void handleOpenGenLicenseButtonAction(ActionEvent event) {
        try {
            Desktop.getDesktop().open(licenseFile.getParentFile());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to open generated file folder.", e);
            this.showErrorDialog(TEXT_FAILED_TO_OPEN_GENERATE_AUTH_LICENSE_FILE_FOLDER);
        }
    }

    private void parseUserLicenseKey(String licenseKey) {
        try {
            licenseInfo = LicenseManager.getLicenseInfo(licenseKey);
            this.macLabel.setText(licenseInfo.getMac());
            this.userNameLabel.setText(licenseInfo.getUserName());
            this.computerNameLabel.setText(licenseInfo.getComputerName());
            this.computerDomainLabel.setText(licenseInfo.getDomain());
            this.licenseFileLabel.setText(getString(TEXT_PARSE_LICENSE_KEY_SUCCESS));
            this.ahuVersionLabel.setText(licenseInfo.getVersion());
            this.ahuPatchVersionLabel.setText(licenseInfo.getPatchVersion());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to parse license key.", e);
            this.licenseFileLabel.setText(getString(TEXT_FAILED_TO_PARSE_LICENSE_KEY));
        }
    }

    private void generateAuthLicenseFile(File licenseDirectory) {
        try {
            int effectTimeIndex = this.effectTimeComBox.getItems().indexOf(this.effectTimeComBox.getValue());
            licenseInfo.setDate(Options.getEffectiveDate(effectTimeIndex));
            licenseInfo.setFactory(this.factoryText.getText());
            int roleIndex = this.roleComBox.getItems().indexOf(this.roleComBox.getValue());
            licenseInfo.setRole(Options.getRole(roleIndex));
            licenseFile = new File(licenseDirectory, LICENSE_FILE_NAME);
            LicenseManager.generateLicenseFile(licenseInfo, licenseFile);

            this.saveRegistrationData(licenseInfo);

            this.openGenLicenseButton.setDisable(false);
            this.showMessageDialog(TEXT_GENERATE_AUTH_LICENSE_FILE_SUCCESS);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to generate authorized license file.", e);
            this.showErrorDialog(TEXT_FAILED_TO_GENERATE_AUTH_LICENSE_FILE);
        }
    }

    private void saveRegistrationData(LicenseInfo licenseInfo) {
        List<RegistrationInfo> registrations = this.loadRegistrationData().stream()
                .filter(r -> !r.getEmail().equalsIgnoreCase(this.emailText.getText().trim()))
                .collect(Collectors.toList());
        registrations.add(new RegistrationInfo(this.emailText.getText().trim(), licenseInfo));

        try {
            CsvWriter csvWriter = CsvUtil.getWriter(new FileWriter(this.getRegistrationDataFile()));
            List<String> fieldNames = ReflectionUtils.getFieldNames(RegistrationInfo.class);
            csvWriter.write(fieldNames.toArray(new String[0]));

            registrations.forEach(r -> {
                List<String> values = Lists.newArrayList();
                fieldNames.forEach(f -> {
                    try {
                        values.add(BeanUtils.getProperty(r, f));
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, "failed to read field value", e);
                    }
                });
                csvWriter.write(values.toArray(new String[0]));
            });
            csvWriter.close();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "failed to save registration data", e);
        }
    }

    private List<RegistrationInfo> loadRegistrationData() {
        File regFile = this.getRegistrationDataFile();
        if (!regFile.exists()) {
            return Lists.newArrayList();
        }

        List<RegistrationInfo> registrations = Lists.newArrayList();
        try {
            CsvData csvData = CsvUtil.getReader().read(new FileReader(regFile));
            List<String> header = csvData.getRow(0).getRawList();

            IntStream.range(1, csvData.getRowCount()).forEach(r -> {
                RegistrationInfo registrationInfo = new RegistrationInfo();
                CsvRow csvRow = csvData.getRow(r);
                IntStream.range(0, header.size()).forEach(h -> {
                    try {
                        BeanUtils.setProperty(registrationInfo, header.get(h), csvRow.get(h));
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, "failed to write field value", e);
                    }
                });
                registrations.add(registrationInfo);
            });
        } catch (Exception e) {
            logger.log(Level.SEVERE, "failed to load registration data", e);
        }

        return registrations;
    }

    private File getRegistrationDataFile() {
        String userhome = System.getProperty("user.home");
        String regFilePath = userhome + "\\.carrier\\ahu_registration_info.csv";
        File regFile = new File(regFilePath);

        File dir = regFile.getParentFile();
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return regFile;
    }

    private void showErrorDialog(String messageKey) {
        this.showDialog(AlertType.ERROR, TEXT_ERROR, messageKey);
    }

    private void showMessageDialog(String messageKey) {
        this.showDialog(AlertType.INFORMATION, TEXT_INFORMATION, messageKey);
    }

    private void showDialog(AlertType alertType, String titleKey, String messageKey) {
        Alert alert = new Alert(alertType);
        alert.setTitle(getString(titleKey));
        alert.setHeaderText(null);
        alert.setContentText(getString(messageKey));
        alert.showAndWait();
    }

    private String getString(String key) {
        return bundle.getString(key);
    }

}

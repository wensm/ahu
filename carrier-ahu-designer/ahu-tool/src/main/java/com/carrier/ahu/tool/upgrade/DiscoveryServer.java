package com.carrier.ahu.tool.upgrade;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import com.carrier.ahu.common.upgrade.MessageType;
import com.carrier.ahu.common.upgrade.UpgradeConfig;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.license.LicenseInfo;
import com.carrier.ahu.util.EmptyUtil;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Created by Braden Zhou on 2018/07/16.
 */
@Slf4j
public class DiscoveryServer {

    private static final Thread discoveryThred = new Thread(new DiscoveryThread());

    public static void startDicoveryServer() {
        if (!discoveryThred.isAlive()) {
            discoveryThred.start();
        }
    }

    private static class DiscoveryThread implements Runnable {

        private DatagramSocket socket;

        @Override
        public void run() {
            try {
                socket = new DatagramSocket(CommonConstant.SYS_UPGRADE_UPGRADE_DISCOVERY_PORT,
                        InetAddress.getByName(CommonConstant.SYS_UPGRADE_LOCAL_ADDRESS));
                socket.setBroadcast(true);
                while (true) {
                    byte[] buffer = new byte[1024];
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    socket.receive(packet);
                    String message = new String(packet.getData(), 0, packet.getLength());
                    if (message.startsWith(MessageType.CHECK_UPDATE.name())) {
                        String licenseInfoJson = message.substring(MessageType.CHECK_UPDATE.name().length());
                        if (EmptyUtil.isNotEmpty(licenseInfoJson)) {
                            LicenseInfo licenseInfo = new Gson().fromJson(licenseInfoJson, LicenseInfo.class);
                            byte[] sendData = (MessageType.CHECK_UPDATE.name()
                                    + UpgradeServer.getReleaseVersion(licenseInfo.getFactory())).getBytes();
                            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length,
                                    packet.getAddress(), packet.getPort());
                            socket.send(sendPacket);
                        }
                    }
                }
            } catch (IOException ex) {
                log.error("Error happened", ex);
            } finally {
                socket.close();
            }
        }
    }

}

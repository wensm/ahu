package com.carrier.ahu.tool.coilrules;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.util.ExcelUtils;

/**
 * 本工具用来读取Excel文件，并输出json字符串
 * 
 * @author WANGD1
 *
 */
public class ReadExcel2json {
	private static Logger logger = LoggerFactory.getLogger(ReadExcel2json.class.getName());

	public static final String UNITTYPE_PREFIX = "39CQ";
	
	
	public String excelHandle() {
		Map<String, int[]> retmap1 = this.readCircuit("abcdefg.xlsx", "4分盘管回路限制表格", "H");
		Map<String, int[]> retmap2 = this.readCircuit("abcdefg.xlsx", "3分盘管回路限制", "H");
		retmap1.putAll(retmap2);
		Map<String, int[]> sortedMap1 = sortMapByKey(retmap1);
		Map finalMap1 = new HashMap();
		finalMap1.put("valueOptionMap", sortedMap1);
		String a1 = JSON.toJSONString(finalMap1);

		Map retmap3 = this.readRow("abcdefg.xlsx", "4分盘管回路限制表格", "H");
		Map retmap4 = this.readRow("abcdefg.xlsx", "3分盘管回路限制", "H");
		retmap3.putAll(retmap4);
		Map<String, int[]> sortedMap2 = sortMapByKey(retmap3);
		Map finalMap2 = new HashMap();
		finalMap2.put("valueOptionMap", sortedMap2);
		String a2 = JSON.toJSONString(finalMap2);

		Map retmap5 = this.readCircuit("abcdefg.xlsx", "4分盘管回路限制表格", "C");
		Map retmap6 = this.readCircuit("abcdefg.xlsx", "3分盘管回路限制", "C");
		retmap5.putAll(retmap6);
		Map<String, int[]> sortedMap3 = sortMapByKey(retmap5);
		Map finalMap3 = new HashMap();
		finalMap3.put("valueOptionMap", sortedMap3);
		String a3 = JSON.toJSONString(finalMap3);
		
		Map retmap7 = this.readRow("abcdefg.xlsx", "4分盘管回路限制表格", "C");
		Map retmap8 = this.readRow("abcdefg.xlsx", "3分盘管回路限制", "C");
		retmap7.putAll(retmap8);
		Map<String, int[]> sortedMap4 = sortMapByKey(retmap7);
		Map finalMap4 = new HashMap();
		finalMap4.put("valueOptionMap", sortedMap4);
		String a4 = JSON.toJSONString(finalMap4);
		
		Map retmap9 = this.readCircuit("abcdefg.xlsx", "4分盘管回路限制表格", "D");
		Map retmap10 = this.readCircuit("abcdefg.xlsx", "3分盘管回路限制", "D");
		retmap9.putAll(retmap10);
		Map<String, int[]> sortedMap5 = sortMapByKey(retmap9);
		Map finalMap5 = new HashMap();
		finalMap5.put("valueOptionMap", sortedMap5);
		String a5 = JSON.toJSONString(finalMap5);
		
		Map retmap11 = this.readRow("abcdefg.xlsx", "4分盘管回路限制表格", "D");
		Map retmap12 = this.readRow("abcdefg.xlsx", "3分盘管回路限制", "D");
		retmap11.putAll(retmap12);
		Map<String, int[]> sortedMap6 = sortMapByKey(retmap11);
		Map finalMap6 = new HashMap();
		finalMap6.put("valueOptionMap", sortedMap6);
		String a6 = JSON.toJSONString(finalMap6);
		
		
//		Map finalmap = new HashMap();
//		finalmap.put("circuit-H", a1);
//		finalmap.put("row-H", a2);
//		finalmap.put("circuit-C", a3);
//		finalmap.put("row-C", a4);
//		String finalout = JSON.toJSONString(finalmap);
		
		String finalout = "{\"circuit-H\":"+a1+",\n";
		finalout += "\"row-H=\":"+a2+",\n";
		finalout += "\"circuit-C=\":"+a3+",\n";
		finalout += "\"row-C=\":"+a4+",\n";
		finalout += "\"circuit-D=\":"+a5+",\n";
		finalout += "\"row-D=\":"+a6+"}\n";
		
		return finalout;
	}

	/**
	 * 读取excel文件，返回相应circuit的map
	 * 
	 * @param fileName
	 *            Excel的文件名
	 * @param sheetName
	 *            需要处理的sheet页名
	 * @param coiltype
	 *            分别为C-冷管，H-热管，D-直接蒸发
	 * @return map
	 */
	private Map<String, int[]> readCircuit(String fileName, String sheetName, String coiltype) {
		Map retMap = new HashMap();
		// String circuitJson = new String();
		List<List<String>> list = Collections.emptyList();
		try {
			list = this.readExcel(fileName, sheetName);
			// 返回coiltype类型json数据
			Map<String, int[]> circuitMap = this.transferCircuit(list, coiltype, sheetName);
			retMap.putAll(circuitMap);
		} catch (Exception e) {
			logger.error("Failed to read excel: " + fileName, e);
			return retMap;
		}

		return retMap;
	}

	/**
	 * 读取excel文件，返回相应row的map
	 * 
	 * @param fileName
	 * @param sheetName
	 * @param coiltype
	 * @return
	 */
	private Map<String, int[]> readRow(String fileName, String sheetName, String coiltype) {
		Map retMap = new HashMap();
		List<List<String>> list = Collections.emptyList();
		try {
			list = this.readExcel(fileName, sheetName);

			Map<String, int[]> rowMap = this.transferRow(list, coiltype, sheetName);
			retMap.putAll(rowMap);
		} catch (Exception e) {
			logger.error("Failed to read excel: " + fileName, e);
			return retMap;
		}

		return retMap;
	}

	/**
	 * 读取excel文件，并返回list类型结果集
	 * 
	 * @param fileName
	 * @param sheetName
	 * @return
	 */
	private List<List<String>> readExcel(String fileName, String sheetName) {
		List<List<String>> list = Collections.emptyList();

		try {
			File file = new File("D:/work/ahu_20180424/CarrierAHU/carrier-ahu-designer/ahu-tool/src/main/java/coilrules/abcdefg.xlsx");
			InputStream is = new FileInputStream(file);
//			InputStream is = this.getClass().getClassLoader().getResourceAsStream(fileName);
			list = ExcelUtils.read(is, ExcelUtils.isExcel2003(fileName), sheetName, 1);
		} catch (EncryptedDocumentException e) {
			logger.error("Failed to load excel: " + fileName, e);
			return list;
		} catch (InvalidFormatException e) {
			logger.error("Failed to load excel: " + fileName, e);
			return list;
		} catch (IOException e) {
			logger.error("Failed to load excel: " + fileName, e);
			return list;
		}

		return list;
	}

	/**
	 * 将工具类读取excel的list转换为Map
	 * 
	 * @param list
	 * @param coiltype
	 *            C-冷管， H-热管，D-直接蒸发
	 * @param sheetName
	 *            要读取的sheet页
	 * @return
	 */
	private Map<String, int[]> transferCircuit(List<List<String>> list, String coiltype, String sheetName) {
		Map<String, int[]> map = new HashMap();
		// 区别三分管还是四分管
		String tubeType = new String();
		if (sheetName.indexOf("3") >= 0) {
			tubeType = "T3";
		} else if (sheetName.indexOf("4") >= 0) {
			tubeType = "T4";
		}
		
		for (List<String> stringList : list) {// 遍历sheet的每一行
			int i = 0;
			String keyName = new String();
			boolean coiltypeValid = false;// 判别是否是当前coiltype，false则不处理
			int[] retCircuitArr;
			for (String s : stringList) {// 遍历每一行的每一个单元格
				// excel中列的顺序分别为：UNITTYPE，COILTYPE，ROW，CIRCUIT
				if (i == 0) {
					String unittypeStr = s;
					if (!StringUtils.isEmpty(unittypeStr)) {
						keyName = UNITTYPE_PREFIX + unittypeStr + "#";
					}
					i++;
				} else if (i == 1) {
					String coiltypeStr = s;
					if (!StringUtils.isEmpty(coiltypeStr) && coiltype.equals(coiltypeStr)) {
						coiltypeValid = true;// COILTYPE匹配
					}
					i++;
				} else if (i == 2) {
					String rowStr = s;
					if (!StringUtils.isEmpty(rowStr)) {
						if ("A".equals(rowStr)) {// 将A转义为"-1"
							rowStr = "-1";
						}
						keyName += rowStr + "#";
					}
					i++;
				} else if (i == 3) {
					String circuitStr = s;
					
					// 处理map
					if (coiltypeValid) {// 如果是当前coiltype，则把相应的circuit值数组放入map
						String[] temp = circuitStr.split(",|，");
						retCircuitArr = new int[temp.length];
						int j = 0;
						for (String str : temp) {
							// 转义
							if ("A".equals(str)) {
								retCircuitArr[j] = 0;
								j++;
							} else if ("H".equals(str)) {
								retCircuitArr[j] = 1;
								j++;
							} else if ("F".equals(str)) {
								retCircuitArr[j] = 2;
								j++;
							} else if ("D".equals(str)) {
								retCircuitArr[j] = 3;
								j++;
							}
						}
						
						if (keyName.indexOf(",") >= 0 || keyName.indexOf("，") >= 0) {// 当excel中row有多个值时，进行拆分。形如"39CQ0608#3,4,5#"
							// 拆分keyName，形如"39CQ0608#3,4,5#"
							String[] tempNames = keyName.split("#");
							String rows = tempNames[1];
							String[] tempRows = rows.split(",|，");
							for (String rowName : tempRows) {
								String newKeyName = tempNames[0] + "#" + rowName + "#" + tubeType;
								map.put(newKeyName, retCircuitArr);
							}
						} else {// 当row只有一个值的情形
							map.put(keyName + tubeType, retCircuitArr);
						}
					}
					i++;
				}
			}
		}

		return map;
	}

	/**
	 * 将工具类读取excel的list转换为Map
	 * 
	 * @param list
	 * @param coiltype
	 *            C-冷管， H-热管，D-直接蒸发
	 * @param sheetName
	 *            要读取的sheet页
	 * @return
	 */
	private Map<String, int[]> transferRow(List<List<String>> list, String coiltype, String sheetName) {
		Map<String, int[]> map = new HashMap();
		Map<String, Set<Integer>> tempMap = new HashMap();
		
		// 区别三分管还是四分管
		String tubeType = new String();
		if (sheetName.indexOf("3") >= 0) {
			tubeType = "T3";
		} else if (sheetName.indexOf("4") >= 0) {
			tubeType = "T4";
		}
		
		for (List<String> stringList : list) {// 遍历sheet页的每一行
			int i = 0;
			Set<Integer> set = new HashSet();
			String keyName = new String();
			boolean coiltypeValid = false;
			for (String s : stringList) {
				// excel中列的顺序分别为：UNITTYPE，COILTYPE，ROW，CIRCUIT
				if(i==0) {//UNITTYPE
					String unittypeStr = s;
					if (!StringUtils.isEmpty(unittypeStr)) {
						keyName = UNITTYPE_PREFIX + unittypeStr + "#" + tubeType;
					}
					i++;
				}else if(i==1) {
					String coiltypeStr = s;
					if (!StringUtils.isEmpty(coiltypeStr) && coiltype.equals(coiltypeStr)) {
						coiltypeValid = true;// COILTYPE匹配
					}
					i++;
				}else if(i==2) {
					String rowStr = s;
					if (!StringUtils.isEmpty(rowStr)) {
						if ("A".equals(rowStr)) {// 将A转义为"-1"
							rowStr = "0";
						}
					}
					if(coiltypeValid) {//如果是当前coiltype，则开始组装map
						if(rowStr.indexOf(",")>=0 || rowStr.indexOf("，")>=0) {//当有多个row时
							String[] rows = rowStr.split(",|，");
							for(String row : rows) {
//								if ("10".equals(row)) {
//									set.add(9);
//								}else if ("12".equals(row)) {
//									set.add(10);
//								}else {
									set.add(Integer.valueOf(row));									
//								}
							}
						}else {
//							if ("10".equals(rowStr)) {
//								set.add(9);
//							}else if ("12".equals(rowStr)) {
//								set.add(10);
//							}else {
								set.add(Integer.valueOf(rowStr));									
//							}
						}
						if(tempMap.containsKey(keyName)) {//若已经存在了当前keyName映射关系
							Set<Integer> tempSet = ((Set<Integer>)tempMap.get(keyName));
							tempSet.addAll(set);
							tempMap.put(keyName, tempSet);
						}else {
							tempMap.put(keyName, set);
						}
						
					}
					i++;
				}
				
			}

		}
		Set<Integer> contentSet = Collections.emptySet();
		//遍历tempMap，转数组并转到map中。
		for(String keyname : tempMap.keySet()) {
			contentSet = (Set<Integer>)tempMap.get(keyname);
			Object[] arry = contentSet.toArray();
			int[] temparr = new int[arry.length];
			for (int i = 0; i < arry.length; i++) {  
	            temparr[i] = (int) arry[i];  
	        }  
			
//			String strarry = StringUtils.join(contentSet.toArray(), "#");
//			int[] strs = strarry.split("#");
			map.put(keyname, temparr);
			
		}
		
		return map;
	}
	
	public static Map<String, int[]> sortMapByKey(Map<String, int[]> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }

        Map<String, int[]> sortMap = new TreeMap<String, int[]>(
                new MapKeyComparator());

        sortMap.putAll(map);

        return sortMap;
    }
	
	public static void main(String[] args) {
		
		ReadExcel2json a = new ReadExcel2json();
		String out = a.excelHandle();
		System.out.println(out);
	}
}

class MapKeyComparator implements Comparator<String>{

    @Override
    public int compare(String str1, String str2) {
        
        return str1.compareTo(str2);
    }
}
package com.carrier.ahu.tool.license;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import com.carrier.ahu.common.enums.RoleEnum;
import com.carrier.ahu.util.DateUtil;

public class Options {

    private static final int ONE_MONTH = 0;
    private static final int THREE_MONTHS = 1;
    private static final int HALF_YEAR = 2;
    private static final int ONE_YEAR = 3;

    public static String getEffectiveDate(int effectTimeIndex) {
        Date effectDate = new Date();
        switch (effectTimeIndex) {
        case ONE_MONTH:
            effectDate = DateUtils.addMonths(effectDate, 1);
            break;
        case THREE_MONTHS:
            effectDate = DateUtils.addMonths(effectDate, 3);
            break;
        case HALF_YEAR:
            effectDate = DateUtils.addMonths(effectDate, 6);
            break;
        case ONE_YEAR:
            effectDate = DateUtils.addMonths(effectDate, 12);
            break;
        }
        return DateUtil.dateToStr(effectDate);
    }

    public static String getRole(int roleIndex) {
        return RoleEnum.values()[roleIndex].name();
    }

}

package com.carrier.ahu.tool.common;

/**
 * Created by Braden Zhou on 2018/04/19.
 */
public class ToolConstants {

    public static final String COMMA = ",";
    public static final String ARRAY_SEPARATOR = COMMA;

    public static final String FXML_LICENSE_FILE = "license.fxml";
    public static final String BUNDLE_PACKAGE = "com.carrier.ahu.tool.bundles.Tool";
    public static final String LICENSE_ATTR_SEPARATOR = ";";
    public static final String LICENSE_FILE_NAME = "license.dat";
    public static final String LICENSE_FILE_EXTENSION = "*.dat";
    public static final String TEXT_LICENSE_TITLE = "license_title";
    public static final String TEXT_OPEN_LICENSE_FILE = "open_license_file";
    public static final String TEXT_EFFECT_TIME_LIST = "effect_time_list";
    public static final String TEXT_ROLE_LIST = "role_list";
    public static final String TEXT_LICENSE_FILE = "license_file";
    public static final String TEXT_ERROR = "error";
    public static final String TEXT_INFORMATION = "infomation";
    public static final String TEXT_FAILED_TO_PARSE_LICENSE_FILE = "failed_to_parse_license_file";
    public static final String TEXT_FAILED_TO_PARSE_LICENSE_KEY = "failed_to_parse_license_key";
    public static final String TEXT_PARSE_LICENSE_KEY_SUCCESS = "parse_license_key_success";
    public static final String TEXT_FAILED_TO_GENERATE_AUTH_LICENSE_FILE = "failed_to_generate_auth_license_file";
    public static final String TEXT_FAILED_TO_OPEN_GENERATE_AUTH_LICENSE_FILE_FOLDER = "failed_to_open_generate_auth_license_file_folder";
    public static final String TEXT_GENERATE_AUTH_LICENSE_FILE_SUCCESS = "generate_auth_license_file_success";
    public static final String TEXT_EMPTY_EFFECTIVE_TIME = "empty_effective_time";
    public static final String TEXT_EMPTY_FACTORY = "empty_factory";
    public static final String TEXT_EMPTY_EMAIL = "empty_email";
    public static final String TEXT_EMPTY_ROLE = "empty_role";
    public static final String TEXT_OPEN_FILE_FAILURE = "open_file_failure";

}

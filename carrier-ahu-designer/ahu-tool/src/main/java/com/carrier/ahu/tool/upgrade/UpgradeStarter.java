package com.carrier.ahu.tool.upgrade;

import java.io.IOException;

/**
 * 
 * Created by Braden Zhou on 2018/07/16.
 */
public class UpgradeStarter {

    public static void main(String[] args) throws IOException {
        DiscoveryServer.startDicoveryServer();
        UpgradeServer.startUpgradeServer();
    }

}

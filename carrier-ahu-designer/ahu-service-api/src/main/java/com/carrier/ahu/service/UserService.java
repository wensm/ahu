package com.carrier.ahu.service;

import java.util.List;

import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.common.entity.UserMetaId;
import com.carrier.ahu.license.LicenseInfo;

/**
 * 用户相关接口
 * 
 * @author Simon
 *
 */
public interface UserService {
    /**
     * 新增用户
     * 
     * @param user
     * @param userName
     * @return
     */
    String addUser(User user, String userName);

    /**
     * 修改用户
     * 
     * @param user
     * @param userName
     * @return
     */
    String updateUser(User user, String userName);

    /**
     * 删除用户
     * 
     * @param userId
     * @return
     */
    void deleteUser(String userId);

    /**
     * 查询用户详情
     * 
     * @param userId
     * @return
     */
    User findUserById(String userId);

    /**
     * 查询用户列表
     * 
     * @return
     */
    Iterable<User> findAll();

    /**
     * 检查用户名称是否重复
     * 
     * @param userName
     * @return
     */
    boolean checkName(String userName);

    /**
     * 
     * @param userMeta
     */
    void saveUserMeta(UserMeta userMeta);

    /**
     * 
     * @param userMetas
     */
    void saveUserMetas(List<UserMeta> userMetas);

    /**
     * 
     * @param userMetaId
     */
    void deleteUserMeta(UserMetaId userMetaId);

    /**
     * 
     * @param userMetaId
     * @return
     */
    UserMeta findUserMetaById(UserMetaId userMetaId);

    /**
     * 
     * @return
     */
    List<UserMeta> findAllUserMeta();

    /**
     * 
     * @param userId
     * @param factoryId
     * @return
     */
    List<UserMeta> findUserMetaByUserAndFactory(String userId, String factoryId);

    /**
     * Return local user, or create a default user.
     * 
     * @param licenseInfo
     * @return
     */
    User getLocalUser(LicenseInfo licenseInfo);

	User installUser(LicenseInfo licenseInfo);

}

package com.carrier.ahu.service.meta;

import com.carrier.ahu.po.meta.SectionMeta;

import java.io.File;
import java.util.List;

/**
 * 段元数据业务接口
 * Created by Wen zhengtao on 2017/3/17.
 */
public interface SectionMetaService {
	List<SectionMeta> getSectionMetas(File[] files);
}

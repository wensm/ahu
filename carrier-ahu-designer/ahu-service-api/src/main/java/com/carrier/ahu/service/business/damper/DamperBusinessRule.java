package com.carrier.ahu.service.business.damper;

import java.util.Map;

import com.carrier.ahu.common.enums.DamperPosEnum;

public interface DamperBusinessRule {

    Map<String, Object> adjustDamperSize(DamperPosEnum damperPos);

}

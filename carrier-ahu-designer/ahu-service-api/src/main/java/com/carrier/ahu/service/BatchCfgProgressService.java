package com.carrier.ahu.service;

/**
 * Created by Simon on 2017/12/09.
 */

import java.util.List;

import com.carrier.ahu.common.entity.BatchCfgProgress;

/**
 * 批量配置进度相关接口
 */
public interface BatchCfgProgressService {
	/**
	 * 新增批量配置进度
	 * 
	 * @param progress
	 * @param userName
	 * @return
	 */
	String addProgress(BatchCfgProgress progress, String userName);

	/**
	 * 修改批量配置进度
	 * 
	 * @param progress
	 * @param userName
	 * @return
	 */
	String updateProgress(BatchCfgProgress progress, String userName);

	/**
	 * 删除批量配置进度
	 * 
	 * @param progressId
	 * @return
	 */
	void deleteProgress(String progressId);

	/**
	 * 查询批量配置进度详情
	 * 
	 * @param progressId
	 * @return
	 */
	BatchCfgProgress findProgressById(String progressId);

	/**
	 * 查询批量配置进度列表
	 * 
	 * @param projectId
	 * @return
	 */
	List<BatchCfgProgress> findProgressList(String projectId);

	/**
	 * 查询批量配置进度列表
	 * 
	 * @return
	 */
	Iterable<BatchCfgProgress> findAll();

	/**
	 * 根据状态查询批量配置进度列表
	 * 
	 * @param projectId
	 * @param recordStatus
	 * @return
	 */
	List<BatchCfgProgress> findProgressListByRecordStatus(String projectId, String recordStatus);

	/**
	 * 根据项目ID、组编码、机组编码、段编码查询进度列表
	 * 
	 * 如果项目ID为null，返回null，其他条件为null，则对应条件不生效
	 * 
	 * @param projectId
	 * @param groupId
	 * @param unitId
	 * @param partId
	 * @return
	 */
	List<BatchCfgProgress> findProgressListByParams(String projectId, String groupId);

	/**
	 * 批量获取项目下所有批量配置的ID
	 *
	 * @param pid
	 * @return
	 */
	List<String> getAllIds(String pid);
}

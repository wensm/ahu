package com.carrier.ahu.service;

import java.util.List;

import com.carrier.ahu.common.entity.AhuTemplate;

public interface AhuTemplateService {
	
	List<AhuTemplate> list();

	String addAhuTempate(AhuTemplate ahuTemplate);
	String updateAhuTempate(AhuTemplate ahuTemplate);
}

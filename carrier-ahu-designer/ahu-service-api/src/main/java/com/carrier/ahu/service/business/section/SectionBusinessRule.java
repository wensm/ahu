package com.carrier.ahu.service.business.section;

public interface SectionBusinessRule {

    boolean isSupplierAir();

    boolean isReturnAir();

    double getSupplierAirVolume();

    double getReturnAirVolume();

}
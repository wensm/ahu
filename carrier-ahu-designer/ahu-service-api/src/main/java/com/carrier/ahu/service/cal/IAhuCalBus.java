package com.carrier.ahu.service.cal;

import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.CalcTypeEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.model.calunit.AhuParam;

/**
 * AHU计算总线，用于AHU后台计算的统一入口
 */
public interface IAhuCalBus {

	void calculate(AhuParam ahu, LanguageEnum language, ICalContext context) throws Exception;

	void setCalOperation(CalOperationEnum operation);

	void setCalType(CalcTypeEnum calType);
	// void calculatePrice(AhuParam ahu, PartJob partJob);
	//
	// void calculateLength(AhuParam ahu, PartJob partJob);
	//
	// Map<Short, List<WeightBean>> calculateWeight(AhuParam ahu, PartJob partJob)
	// throws BadRequestException;
	//
	// void calculateResistance(AhuParam ahu, PartJob partJob);
	//
	// void calculateTemperature(AhuParam ahu, PartJob partJob);
	//
	// void calculateHumidity(AhuParam ahu, PartJob partJob);
	//
	// void calculatePanelArrangement(AhuParam ahu, PartJob partJob);

}

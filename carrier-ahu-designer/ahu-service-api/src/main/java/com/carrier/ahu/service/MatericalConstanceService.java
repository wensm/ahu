package com.carrier.ahu.service;

import java.util.List;

import com.carrier.ahu.common.entity.MatericalConstance;

/**
 * 材料批量配置接口 Created by zhang zhiqiang on 2017/6/22.
 */
public interface MatericalConstanceService {

	/**
	 * 新增材料批量配置
	 * 
	 * @param constance
	 * @return
	 */
	String addConstance(MatericalConstance constance, String userName);

	/**
	 * 根据工程ID和分组ID查询材料批量配置是否存在
	 * 
	 * @param projid
	 * @param groupid
	 * @return
	 */
	boolean exists(String projid, String groupid);

	/**
	 * 根据主键ID查询材料批量配置
	 * 
	 * @param pid
	 * @return
	 */
	MatericalConstance getConstanceById(String pid);

	/**
	 * 根据工程ID和分组ID查询材料批量配置
	 * 
	 * @param projid
	 * @param groupid
	 * @return
	 */
	MatericalConstance getConstanceByProjidAndGroupid(String projid, String groupid);

	/**
	 * 根据工程ID查询材料批量配置列表
	 * 
	 * @param projid
	 * @return
	 */
	List<MatericalConstance> findByProjectid(String projid);

	/**
	 * 更新材料批量配置
	 * 
	 * @param constance
	 * @param userName
	 * @return
	 */
	String updateConstance(MatericalConstance constance, String userName);

	/**
	 * 根据主键ID删除材料批量配置
	 * 
	 * @param pid
	 * @return
	 */
	void deleteConstance(String pid);

	/**
	 * 批量获取项目下所有材料配置的ID
	 *
	 * @param pid
	 * @return
	 */
	List<String> getAllIds(String pid);
}

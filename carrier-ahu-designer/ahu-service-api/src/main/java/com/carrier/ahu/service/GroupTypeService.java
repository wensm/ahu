package com.carrier.ahu.service;

import java.util.List;

import com.carrier.ahu.common.entity.GroupType;

/**
 * 组类型相关接口
 */
public interface GroupTypeService {
	/**
	 * 新增组类型
	 * 
	 * @param type
	 * @param userName
	 * @return
	 */
	String add(GroupType type, String userName);

	/**
	 * 修改组类型
	 * 
	 * @param type
	 * @param userName
	 * @return
	 */
	String update(GroupType type, String userName);

	/**
	 * 删除组类型
	 * 
	 * @param id
	 * @return
	 */
	void deleteType(String id);

	/**
	 * 查询组类型详情
	 * 
	 * @param id
	 * @return
	 */
	GroupType getById(String id);

	/**
	 * 查询组类型列表
	 * 
	 * @param projectId
	 * @return
	 */
	List<GroupType> findListByProjectId(String projectId);

	/**
	 * 查询组类型列表
	 * 
	 * @return
	 */
	Iterable<GroupType> findAll();

	/**
	 * 检查分组类型名称是否重复
	 * 
	 * @param projectId
	 * @param name
	 * @return
	 */
	boolean checkName(String projectId, String name);

	/**
	 * 根据组类型编码查询组类型列表
	 * 
	 * @param projectId
	 * @param code
	 * @return
	 */
	List<GroupType> findListByCode(String projectId, String code);

}

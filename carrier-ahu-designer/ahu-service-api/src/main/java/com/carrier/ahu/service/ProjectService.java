package com.carrier.ahu.service;

import java.util.List;

import com.carrier.ahu.common.entity.Project;

/**
 * 项目业务接口
 * Created by Wen zhengtao on 2017/3/17.
 */
public interface ProjectService {

	/**
	 * 新增项目
	 * @param project
	 * @param userName
	 * @return
	 */
	String addProject(Project project,String userName);

	/**
	 * 根据项目编号检查项目是否存在
	 * @param projectNo
	 * @return
	 */
	boolean exists(String projectNo);

	/**
	 * 根据主键ID查询项目
	 * @param projectId
	 * @return
	 */
	Project getProjectById(String projectId);

	/**
	 * 查询所有项目
	 * @return
	 */
	List<Project> findProjectList();
	/**
	 * 查询所有已归档项目
	 * @return
	 */
	List<Project> findProjectListArchieved();
	/**
	 * 查询所有未归档项目
	 * @return
	 */
	List<Project> findProjectListUnArchieved();

	/**
	 * 更新项目
	 * @param project
	 * @param userName
	 * @return
	 */
	String updateProject(Project project,String userName);

	/**
	 * 根据主键ID删除项目
	 * @param projectId
	 * @return
	 */
	void deleteProject(String projectId);

    /**
     * 另存为项目
     * 
     * @param project
     * @param projectName
     * @return project id
     */
    String saveAs(Project project, String projectName);
}

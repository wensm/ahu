package com.carrier.ahu.service;

import java.util.List;

import com.carrier.ahu.common.entity.AhuGroupBind;

/**
 * ahu空气处理单元与分组绑定关系<br>
 * 
 * @author Zhang Zhiqiang on 2017/6/30.
 */
public interface AhuGroupBindService {
	/**
	 * 新增绑定关系
	 * 
	 * @param bind
	 * @param username
	 * @return
	 */
	public String addBind(AhuGroupBind bind, String username);

	/**
	 * 删除绑定关系
	 * 
	 * @param ahuId
	 * @return
	 */
	public void deleteBind(String ahuId);
	
	/**
	 * 删除组绑定关系
	 * 
	 * @param groupId
	 * @return
	 */
	public void deleteGroup(String groupId);

	/**
	 * 获取AHU的组信息
	 * 
	 * @param ahuId
	 * @return
	 */
	public AhuGroupBind getByAhuId(String ahuId);

	/**
	 * 查询组里的所有ahu
	 * 
	 * @param groupId
	 * @return
	 */
	public List<AhuGroupBind> findByGroup(String groupId);

	/**
	 * 新增绑定关系
	 * 
	 * @param binds
	 * @param username
	 * @return
	 */
	public List<String> addBinds(List<AhuGroupBind> binds, String username);
}

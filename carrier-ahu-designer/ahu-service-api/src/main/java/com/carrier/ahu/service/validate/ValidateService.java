package com.carrier.ahu.service.validate;

import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.exception.TempCalErrorException;

/**
 * @ClassName: ValidateService 
 * @Description: (业务的校验数据合法性校验校验) 
 * @author dongxiangxiang <dong.xiangxiang@carries.utc.com>   
 * @date 2018年2月5日 下午1:34:22 
 *
 */
public interface ValidateService {
	/**
	 * T,TB 通过干球温度、湿球温度合法性校验
	 * @param ParmT
	 * @param ParmTB
	 * @param languageEnum
	 * @throws TempCalErrorException
	 */
	void compareT(double ParmT, double ParmTB, LanguageEnum languageEnum) throws TempCalErrorException;
	/**
	 * 加湿量合法性校验
	 * @param humidificationQ
	 * @param languageEnum
	 * @throws TempCalErrorException
	 */
	void compareHumidificationQ(double humidificationQ, LanguageEnum languageEnum) throws TempCalErrorException;
	/**
	 * 相对湿度合法性校验
	 * @param relativeT
	 * @param languageEnum
	 * @throws TempCalErrorException
	 */
	void compareRelativeT(double relativeT, LanguageEnum languageEnum) throws TempCalErrorException;
}

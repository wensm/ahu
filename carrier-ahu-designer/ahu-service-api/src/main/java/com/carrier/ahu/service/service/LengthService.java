package com.carrier.ahu.service.service;

import com.carrier.ahu.common.exception.calculation.SectionLengthCalcException;
import com.carrier.ahu.length.param.LengthParam;

/**
 * Created by liangd4 on 2017/9/28.
 * 计算段长
 */
public interface LengthService {

    //sectionType A:混合段 B:过滤段 C:综合过滤段 D:冷水盘管段 E:热水盘管段 F:蒸汽盘管段
    //            G:电加热盘管段 H:干蒸汽加湿段 I:湿膜加湿段 J:高压喷雾加湿段 K:风机段
    //            L:新回排风段 M:消音段 N:出风段 O:空段 V:高效过滤段 W:热回收段 X:电极加湿
    //            Y:静电过滤器 Z:控制段
    //version：版本 CN-HTC-1.0 国家二字码-工厂简称-版本号
    int getDefaultLength(LengthParam lengthParam) throws SectionLengthCalcException;

    int getCalLength(LengthParam lengthParam) throws SectionLengthCalcException;

    int getShowLength(LengthParam lengthParam) throws SectionLengthCalcException;
}

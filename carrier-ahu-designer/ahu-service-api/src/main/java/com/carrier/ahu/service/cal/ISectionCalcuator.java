package com.carrier.ahu.service.cal;

import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;


/**
 * 计算接口，
 */
public interface ISectionCalcuator {
	void calculate(AhuParam ahuParam, PartParam partParam);
}

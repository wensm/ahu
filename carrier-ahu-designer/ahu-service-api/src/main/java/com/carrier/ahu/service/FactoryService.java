package com.carrier.ahu.service;

import com.carrier.ahu.common.entity.Factory;

/**
 * 工厂相关接口
 * 
 * @author Simon
 *
 */
public interface FactoryService {
	/**
	 * 新增工厂
	 * 
	 * @param factory
	 * @param factoryName
	 * @return
	 */
	String addFactory(Factory factory, String factoryName);

	/**
	 * 修改工厂
	 * 
	 * @param factory
	 * @param factoryName
	 * @return
	 */
	String updateFactory(Factory factory, String factoryName);

	/**
	 * 删除工厂
	 * 
	 * @param factoryId
	 * @return
	 */
	void deleteFactory(String factoryId);

	/**
	 * 查询工厂详情
	 * 
	 * @param factoryId
	 * @return
	 */
	Factory findFactoryById(String factoryId);

	/**
	 * 查询工厂列表
	 * 
	 * @return
	 */
	Iterable<Factory> findAll();

	/**
	 * 检查工厂名称是否重复
	 * 
	 * @param factoryName
	 * @return
	 */
	boolean checkName(String factoryName);

}

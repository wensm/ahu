package com.carrier.ahu.service.cal;

import com.carrier.ahu.common.entity.UserConfigParam;
import com.carrier.ahu.common.enums.CalOperationEnum;
import com.carrier.ahu.common.enums.CalcTypeEnum;

public interface ICalContext {
	
	
	String NAME_ROOT = "ROOT";
	String NAME_AHU = "AHU";
	String NAME_PART = "PART";
	String NAME_ENGINE = "ENGINE";
	String NAME_ITEM = "ENGINE_ITEM";
	
	
	void completed(double d);
	void error(String s);
	void enter(ICalContext subContext);
	void setSuccess(boolean bol);
	CalOperationEnum getCalOperation();
	CalcTypeEnum getCalType();
	UserConfigParam getUserConfigParam();
}

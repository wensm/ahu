package com.carrier.ahu.engine.heatRecycle.plate;

import java.util.ArrayList;
import java.util.List;

import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.heatRecycle.HeatRecycle;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.util.heatrecycle.EfficientParam;
import com.carrier.ahu.util.heatrecycle.HeatRecycleUtils;

/**
 * 
 * Created by Braden Zhou on 2019/01/17.
 */
public class PlateHeatRecycle extends HeatRecycle {

    @Override
    public List<HeatRecycleInfo> getHeatRecyleInfo(HeatRecycleParam heatRecycleParam) throws Exception {
        EfficientParam efficientParam = HeatRecycleUtils.plateCal(heatRecycleParam.getNAVolume(), 1, 1.2,
                heatRecycleParam.getSerial());
        HeatRecycleInfo summerHRI = getSummerHeatRecycleInfo(heatRecycleParam, efficientParam, efficientParam);
        summerHRI.setReturnSeason(EngineConstant.JSON_AHU_SEASON_SUMMER);

        List<HeatRecycleInfo> heatRecycleInfos = new ArrayList<HeatRecycleInfo>();
        heatRecycleInfos.add(summerHRI);
        if (heatRecycleParam.isEnableWinter()) {
            HeatRecycleInfo winterHRI = getWinterHeatRecycleInfo(heatRecycleParam, efficientParam, efficientParam);
            winterHRI.setReturnSeason(EngineConstant.JSON_AHU_SEASON_WINTER);
            heatRecycleInfos.add(winterHRI);
        }
        return heatRecycleInfos;
    }

}

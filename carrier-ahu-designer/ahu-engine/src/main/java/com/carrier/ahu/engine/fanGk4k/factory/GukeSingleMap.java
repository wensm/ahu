package com.carrier.ahu.engine.fanGk4k.factory;

public class GukeSingleMap<E> {

	String key;
	E value;

	public String getKey() {
		return key;
	}
	
	public void put(String key, E value) {
		this.key=key;
		this.value=value;
	}
	
	public E get(String key) {
		return value;
	}
}

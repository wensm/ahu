package com.carrier.ahu.engine.coilWater4d.count;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.carrier.ahu.metadata.entity.coil.CoilSurfEff;
import com.carrier.ahu.util.ExcelUtils;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.vo.SysConstants;
import lombok.extern.slf4j.Slf4j;

import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.coil.SZua;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import static com.carrier.ahu.constant.CommonConstant.SURF_EFF_COIL;
import static com.carrier.ahu.vo.SystemCalculateConstants.HEATINGCOIL_TUBEDIAMETER_3_8;
import static com.carrier.ahu.vo.SystemCalculateConstants.waterCoilH;

//盘管系数计算
@Slf4j
public class CoilCoff {

    /**
     * 重新计算盘管系数
     *
     * @param season       --季节
     * @param finDensity   --片距
     * @param finType      --翅片类型
     * @param rows         --排数
     * @param tubeDiameter --管径
     * @return
     */
    public static Double calulateCoilSurfEff(String coilType,String season, String finDensity, String finType, String rows, String tubeDiameter,double wenteringFluidTemperature) {
//        List<CoilCoffInfo> coilCoffInfoList = new ArrayList<CoilCoffInfo>();
//        double surfEff = 0.0;
//        List<List<String>> list = Collections.emptyList();
//        String filePath = SURF_EFF_COIL;
//
//        try {
//            //获取excel输入流
//            InputStream is = new FileInputStream(SysConstants.ASSERT_DIR + filePath);
//            list = ExcelUtils.read(is, ExcelUtils.isExcel2003(filePath), 0, 1);
//        } catch (IOException e) {
//            log.error("Failed to load excel: " + filePath, e);
//            return surfEff;
//        } catch (InvalidFormatException e) {
//            log.error("Failed to load excel: " + filePath, e);
//            return surfEff;
//        }
//        //读取excel中所有数据到list
//        for (List<String> stringList : list) {
//            CoilCoffInfo coilCoffInfo = new CoilCoffInfo();
//            int i = 0;
//            for (String s : stringList) {
//                if (i == 0) {
//                    coilCoffInfo.setFinDensity(s);
//                    i++;
//                    continue;
//                } else if (i == 1) {
//                    coilCoffInfo.setFinType(s);
//                    i++;
//                    continue;
//                } else if (i == 2) {
//                    coilCoffInfo.setRows(s);
//                    i++;
//                    continue;
//                } else if (i == 3) {
//                    coilCoffInfo.setTubeDiameter(s);
//                    i++;
//                    continue;
//                } else if (i == 4) {
//                    coilCoffInfo.setCoilType(s);
//                    i++;
//                    continue;
//                }else if (i == 5) {
//                    coilCoffInfo.setSeason(s);
//                    i++;
//                    continue;
//                } else if (i == 6) {
//                    coilCoffInfo.setSurfEff(s);
//                    i++;
//                    continue;
//                } else if (i == 7) {
//                    coilCoffInfo.setSurfEff100(s);
//                    i++;
//                    continue;
//                }
//            }
//            coilCoffInfoList.add(coilCoffInfo);
//        }
//        for (CoilSurfEff coilCoffInfo : coilCoffInfoList) {
//            //匹配当前盘管选定条件下的盘管系数
//            if (coilType.equals(coilCoffInfo.getCoilType()) && season.equals(coilCoffInfo.getSeason()) && finDensity.equals(coilCoffInfo.getFinDensity()) && finType.equals(coilCoffInfo.getFinType())
//                    && rows.equals(coilCoffInfo.getRows()) && tubeDiameter.equals(coilCoffInfo.getTubeDiameter())) {
//                if(HEATINGCOIL_TUBEDIAMETER_3_8.equals(coilCoffInfo.getTubeDiameter()) && waterCoilH.equals(coilCoffInfo.getCoilType())){//如果为3分管且为热水盘管，则根据进水温度计算盘管系数(yanglin提出，这种工况下系数和进水温度有关)
//                    //G248+(H248-G248)*(I248-50)/50
//                    double surfEff50 = NumberUtil.convertStringToDouble(coilCoffInfo.getSurfEff());
//                    double surfEff100 = NumberUtil.convertStringToDouble(coilCoffInfo.getSurfEff100());
//                    surfEff = surfEff50 + (surfEff100 - surfEff50) * (wenteringFluidTemperature - 50)/50;
//                    surfEff = NumberUtil.scale(surfEff,2);
//                }else{
//                    surfEff = NumberUtil.convertStringToDouble(coilCoffInfo.getSurfEff());
//                }
//                break;
//            }
//        }

        double surfEff = 0.0;
        CoilSurfEff coilCoffInfo = AhuMetadata.findOne(CoilSurfEff.class,coilType,finDensity,finType,rows,tubeDiameter,season);
        if(HEATINGCOIL_TUBEDIAMETER_3_8.equals(coilCoffInfo.getTubeDiameter()) && waterCoilH.equals(coilCoffInfo.getCoilType())){//如果为3分管且为热水盘管，则根据进水温度计算盘管系数(yanglin提出，这种工况下系数和进水温度有关)
            //G248+(H248-G248)*(I248-50)/50
            double surfEff50 = NumberUtil.convertStringToDouble(coilCoffInfo.getSurfEff());
            double surfEff100 = NumberUtil.convertStringToDouble(coilCoffInfo.getSurfEff100());
            surfEff = surfEff50 + (surfEff100 - surfEff50) * (wenteringFluidTemperature - 50)/50;
            surfEff = NumberUtil.scale(surfEff,2);
        }else{
            surfEff = NumberUtil.convertStringToDouble(coilCoffInfo.getSurfEff());
        }
        return surfEff;
    }
    
    //盘管系数计算
    public static Double coilCoff(int tubeDiameter, String season, String uniTtype, double indb, double inwb, int rownum, boolean ahri) {
        uniTtype = uniTtype.substring(uniTtype.length()-4,uniTtype.length());
        Double coilCoff = 0.00;
        if (ahri) {//欧标
            coilCoff = coilCoffAHRI(tubeDiameter, season, uniTtype, indb, inwb, rownum);
        } else {//非欧标
            if (EngineConstant.JSON_AHU_SEASON_W.equals(season)) {//冬季
                return 1.13;
            } else {//夏季
                Double zua = 0.00;
                if(uniTtype.equals(EngineConstant.JSON_AHU_SERIAL_0711)){
                    zua = (double) (1 + (7 - 3 * Float.parseFloat(String.valueOf(rownum))) / 100);//1+(7-3*排数)/100
                }else{
                    zua = (double) (1 + (2 - Float.parseFloat(String.valueOf(rownum))) / 100);//1+(2-排数)/100
                }
                coilCoff = packageZua(indb, inwb, rownum, zua);
            }
        }
        return coilCoff;
    }

    //欧标
    public static Double coilCoffAHRI(int tubeDiameter, String season, String uniTtype, double indb, double inwb, int rownum) {
        Double coilCoff = 0.00;
        if (EngineConstant.JSON_AHU_SEASON_W.equals(season)) {//如果为冬季
            if (1 == tubeDiameter) {//管径为四分管
                coilCoff = 1.1;
            } else {//管径为三分管
                coilCoff = 1.02;
            }
        } else {
            if (uniTtype.equals(EngineConstant.JSON_AHU_SERIAL_0711)) {
                coilCoff = packageZut(indb, inwb, rownum, EngineConstant.JSON_COIL_ZUA_0711);
            } else {
                coilCoff = packageZut(indb, inwb, rownum, null);
            }
        }

        return coilCoff;
    }

    //封装zua
    private static Double packageZua(double indb, double inwb, int rownum, Double zua) {
        if ((indb - 27 <= 0.0001) && (inwb - 19.5 <= 0.0001)) {
            return zua;//如果干球-27<=0.0001 并且 湿球-19.5<=0.0001 盘管系数=zua
        } else if ((indb - 35) < 0.0001 && inwb - 28 < 0.0001 && zua > 1) {
            return zua * 1.02;//如果干球-35<=0.0001 并且 湿球-28<=0.0001 并且 zua>1   盘管系数=zua*1.02
        } else if (((indb - 35) < 0.0001 && inwb - 28 < 0.0001) && zua < 1) {
            return zua / 1.02;//如果干球-35<=0.0001 并且 湿球-28<=0.0001 并且 zua<1   盘管系数=zua/1.02
        } else if (zua > 1) {
            return zua * 1.01;//如果zua>1 盘管系数=zua*1.01
        } else if (zua < 1) {
            return zua / 1.01;//如果zua<1 盘管系数=zua/1.01
        }
        return zua;
    }

    //封装zut
    private static Double packageZut(double indb, double inwb, int rownum, String zuafield) {
        double d = 0.00;
        if (EngineConstant.JSON_COIL_ZUA_0711.equals(zuafield)) {//机型为0711
            zuafield = EngineConstant.JSON_COIL_ZUA_0711;
        } else {
            if ((indb - 27 <= 0.0001) && (inwb - 19.5 <= 0.0001)) {
                zuafield = EngineConstant.JSON_COIL_ZUA_C_R;
            } else if ((indb - 35) < 0.0001 && inwb - 28 < 0.0001) {
                zuafield = EngineConstant.JSON_COIL_ZUA_C_F;
            } else {
                zuafield = EngineConstant.JSON_COIL_ZUA_C_O;
            }
        }
        List<SZua> sZuaPoList = AhuMetadata.findList(SZua.class, String.valueOf(rownum));
        for (SZua sZuaPo : sZuaPoList) {
            if (EngineConstant.JSON_COIL_ZUA_0711.equals(zuafield)) {
                d = sZuaPo.getZua0711();
            } else if (EngineConstant.JSON_COIL_ZUA_C_R.equals(zuafield)) {
                d = sZuaPo.getZuaCR();
            } else if (EngineConstant.JSON_COIL_ZUA_C_F.equals(zuafield)) {
                d = sZuaPo.getZuaCF();
            } else if (EngineConstant.JSON_COIL_ZUA_C_O.equals(zuafield)) {
                d = sZuaPo.getZuaCO();
            }
        }
        return d;
    }
}

/*if (2 == tubeDiameter && "S".equals(season)) {//管径（三分管）并且季节（夏）
        if (uniTtype.equals(EngineConstant.JSON_AHU_SERIAL_0711)) {
        zua = (double) (1 + (7 - 3 * Float.parseFloat(String.valueOf(rownum))) / 100);//1+(7-3*排数)/100
        coilCoff = packageZua(indb, inwb, rownum, zua);
        } else {
        coilCoff = packageZut(indb, inwb, rownum, null);
        }
        } else if (2 == tubeDiameter && "W".equals(season)) {//管径（三分管）并且季节（冬）
        if (uniTtype.equals(EngineConstant.JSON_AHU_SERIAL_0711)) {
        coilCoff = 1.13;
        } else {
        coilCoff = packageZut(indb, inwb, rownum, null);
        }
        } else if (1 == tubeDiameter && "S".equals(season)) {//管径（四分管）并且季节（夏）
        if (uniTtype.equals(EngineConstant.JSON_AHU_SERIAL_0711)) {
        zua = (double) (1 + (7 - 3 * Float.parseFloat(String.valueOf(rownum))) / 100);//1+(7-3*排数)/100
        coilCoff = packageZua(indb, inwb, rownum, zua);
        } else {
        zua = (double) (1 + (2 - Float.parseFloat(String.valueOf(rownum))) / 100);//1+(2-排数)/100
        coilCoff = packageZua(indb, inwb, rownum, zua);
        }
        } else if (1 == tubeDiameter && "W".equals(season)) {//管径（四分管）并且季节（冬）
        coilCoff = 1.13;
        }*/

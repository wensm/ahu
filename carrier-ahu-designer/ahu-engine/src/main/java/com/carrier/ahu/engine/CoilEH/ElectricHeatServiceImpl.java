package com.carrier.ahu.engine.CoilEH;

import static com.carrier.ahu.common.intl.I18NConstants.QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.calculation.CalculationException;
import com.carrier.ahu.engine.CoilEH.entity.ElectricHeatInfo;
import com.carrier.ahu.length.param.ElectricHeatingParam;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.cad.ECoilDimension;
import com.carrier.ahu.metadata.entity.validation.S4GRow;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

@Service
public class ElectricHeatServiceImpl implements ElectricHeatService {

    @Override
    public List<ElectricHeatInfo> getRowNumAndGroupC(ElectricHeatingParam electricHeatingParam) {
        List<ElectricHeatInfo> electricHeatInfoList = new ArrayList<ElectricHeatInfo>();
        String rowNum = getRow(electricHeatingParam);
        List<String> groupCList = calGroupC(electricHeatingParam);//计算组数
        if (!EmptyUtil.isEmpty(groupCList)) {//加工计算结果
            for (String string : groupCList) {
                ElectricHeatInfo electricHeatInfo = new ElectricHeatInfo();
                electricHeatInfo.setRowNum(rowNum);
                electricHeatInfo.setGroupC(string);
                electricHeatInfoList.add(electricHeatInfo);
            }
        } else {//无法计算出结果
            throw new CalculationException(ErrorCode.QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN);
        }
        return electricHeatInfoList;
    }

    /*计算排数*/
    private String getRow(ElectricHeatingParam electricHeatingParam) {
        double WHeatQ = 0.00;
        double SHeatQ = electricHeatingParam.getSHeatQ();//夏季热量
        String serial = electricHeatingParam.getSerial();//机组型号
        double heatQ = electricHeatingParam.getSHeatQ();//默认取夏季热量
        if(electricHeatingParam.isEnableWinter()){//冬季生效
            WHeatQ = electricHeatingParam.getWHeatQ();//冬季热量
        }
        if (WHeatQ > SHeatQ) {
            heatQ = WHeatQ;
        }
        S4GRow s4GRow = AhuMetadata.findOne(S4GRow.class, serial);
        if (!EmptyUtil.isEmpty(s4GRow)) {
            if (heatQ < s4GRow.getRow1()) {
                return SystemCalculateConstants.ELECTRICHEATINGCOIL_ROWNUM_1R;
            } else if (heatQ < s4GRow.getRow2()) {
                return SystemCalculateConstants.ELECTRICHEATINGCOIL_ROWNUM_2R;
            } else if (heatQ < s4GRow.getRow3()) {
                return SystemCalculateConstants.ELECTRICHEATINGCOIL_ROWNUM_3R;
            } else {
                throw new CalculationException(QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN);
            }
        } else {
            throw new CalculationException(QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN);
        }
    }

    /*计算组数*/
    private List<String> calGroupC(ElectricHeatingParam electricHeatingParam) {
        double WHeatQ = 0.00;
        double SHeatQ = electricHeatingParam.getSHeatQ();//夏季热量
        String serial = electricHeatingParam.getSerial();//机组型号
        double heatQ = electricHeatingParam.getSHeatQ();//默认取夏季热量
        if(electricHeatingParam.isEnableWinter()){//冬季生效
            WHeatQ = electricHeatingParam.getWHeatQ();//冬季热量
        }
        if (WHeatQ > SHeatQ) {
            heatQ = WHeatQ;
        }
//        serial = serial.replace("CQ", "G");//TODO 数据源为G数据
        ECoilDimension eCoilDimension = AhuMetadata.findOne(ECoilDimension.class, serial);
        if (!EmptyUtil.isEmpty(eCoilDimension)) {
            List<String> stringList = new ArrayList<String>();
            double minstage = Math.round(heatQ / 50 + 0.5);
            int n = BaseDataUtil.doubleConversionInteger((BaseDataUtil.stringConversionDouble(eCoilDimension.getN()) + 1) / 3);
            for (int i = 1; i <= n; i++)
                if ((BaseDataUtil.stringConversionDouble(eCoilDimension.getN()) + 1) % i == 0) {
                    if (i >= minstage) {
                        switch (i) {
                            case 1:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_1G);break;
                            case 2:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_2G);break;
                            case 3:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_3G);break;
                            case 4:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_4G);break;
                            case 5:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_5G);break;
                            case 6:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_6G);break;
                            case 7:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_7G);break;
                            case 8:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_8G);break;
                            case 9:
                                stringList.add(SystemCalculateConstants.ELECTRICHEATINGCOIL_GROUPC_9G);break;
                        }
                    }
                }
            return stringList;
        } else {
            throw new CalculationException(QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN);
        }
    }
}

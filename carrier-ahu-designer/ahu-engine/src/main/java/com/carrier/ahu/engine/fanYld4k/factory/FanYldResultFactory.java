package com.carrier.ahu.engine.fanYld4k.factory;

import com.carrier.ahu.common.enums.FanEnginesEnum;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.motor.IMotorDataParser;
import com.carrier.ahu.engine.fan.motor.MortorDataParser;
import com.carrier.ahu.engine.fan.util.CountUtil;
import com.carrier.ahu.engine.fanYld4k.entity.FanYldDllInfo;
import com.carrier.ahu.engine.fanYld4k.param.FanCountParam;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.metadata.entity.fan.SKFanType;
import com.carrier.ahu.metadata.entity.fan.SKPlugFanType;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;

import javax.swing.border.EmptyBorder;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static com.carrier.ahu.constant.CommonConstant.SYS_MAP_FAN_FANMODEL_K;

//封装计算总结果
public class FanYldResultFactory {
    IMotorDataParser parser = new MortorDataParser();

    @SuppressWarnings("unused")
    public List<FanInfo> packageCountResultEx3(Vector<SKFanType> conditons, List<FanYldDllInfo> windMachineDLLResultList, PublicRuleParam publicRuleParam) {
        List<FanInfo> fanInfoList = new ArrayList<FanInfo>();// 返回引擎加工数据
        for (FanYldDllInfo dllResult : windMachineDLLResultList) {
            byte[] bData = dllResult.getBData();// DLL接口返回值
            double[] pDoubleData = dllResult.getPDoubleData();// 调用风机DLL接口返回值
            double[] pDoubleDataSYW = dllResult.getPDoubleDataSYW();// 调用风机DLL接口返回值
            String fanType = dllResult.getFanType();// 机组型号 39CQ0608
            String fanSupplier = dllResult.getFanSupplier();// 供应商 A：亿利达 + 谷科
            String seriesNames = new String(bData, 0, 10).trim();
            String subTypeNames = new String(bData, 255, 10).trim();
            String fanDia = new String(bData, 510, 10).trim();
            String motorType = new String(bData, 765, 10).trim();// 电机类型"A"
            motorType = "Y" + motorType.substring(motorType.length() - 1, motorType.length());
            if (seriesNames.substring(0, 3).equals(EngineConstant.SYS_MAP_FAN_FANMODEL_SYW)) {// 2.1.1.seriesNames
                // 截取3位=SYW判断
                // 取值：系列名称第一位到第三位，如果等于SYW
                String type = fanType.substring(fanType.length() - 4, fanType.length());// 0608
                List<SKPlugFanType> skPlugfanTypeList = AhuMetadata.findList(SKPlugFanType.class, type);
                for (SKPlugFanType skPlugfanTypePo : skPlugfanTypeList) {
                    if ((fanDia + subTypeNames).equals(skPlugfanTypePo.getFan())) {// 280HR
                        String rpm = String.valueOf(pDoubleData[0]);// 风机转速
                        String efficiency = String.valueOf(pDoubleData[1]);// 全压内效率
                        String absorberPower = String.valueOf(pDoubleData[3]);// 内功率
                        String outletVelocity = String.valueOf(pDoubleData[6]);// 静压
                        Double motor = pDoubleData[19];// 实际噪声
                        String noise = String.valueOf(pDoubleData[38]);// 总A声压级
                        String pt = String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[8], 1));// 全压
                        String maxSpeed = String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[45], 1));// 风机最高转速R/MIN
                        String maxAbsorbedPower = String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[44], 1));// 最大吸收功率 KW
                        CountUtil countUtil = new CountUtil();
                        Integer js = countUtil.packageCALJS(Double.valueOf(rpm));// 返回一个2、4、6、8的值
                        // 电机级数
                        String fanName = seriesNames + fanDia + subTypeNames; // 子系列名称+型号名称+子系列名称（运算逻辑）
                        if (motor >= 0.75 && motor <= 250 && js > 0 && js < 8 && (EmptyUtil.isNotEmpty(pDoubleDataSYW) && pDoubleDataSYW[0] < 8.0)) {// 如果电机级数在0到8范围内，不包含0、8
                            // 封装运算结果
                            FanCountParam countParam = new FanCountParam();
                            // =============================待优化===================================
                            countParam.setrMotorMaxMachineSite(Double.valueOf(skPlugfanTypePo.getMaxMachineSite()));// 最大机器
                            countParam.setrMotorMaxPower(skPlugfanTypePo.getMaxPower());// 最大功率
                            countParam.setFanSupplier(skPlugfanTypePo.getSupplier());// 供应商
                            countParam.setMotorType(dllResult.getMotorType());// 电机类型
                            countParam.setFanName(fanName);// 风机名称
                            countParam.setMotor(motor);// 电机功率
                            countParam.setJs(js);// 电机级数
                            countParam.setOutletVelocity(outletVelocity);// 出风口风速
                            countParam.setEfficiency(efficiency);// 效率
                            countParam.setAbsorberPower(absorberPower);// 吸收功率
                            countParam.setRpm(rpm);// 转速
                            countParam.setNoise(noise);// 噪音
                            FanInfo fanInfo = countUtil.packageResult(countParam,dllResult);
                            fanInfo.setTotalPressure(pt);//全压
                            fanInfo.setMaxRPM(maxSpeed);//风机最高转速R/MIN
                            fanInfo.setMaxAbsorbedPower(maxAbsorbedPower);//最大吸收功率
                            fanInfo.setCurve(dllResult.getCurve());
                            fanInfo.setModel(fanInfo.getModel());
                            fanInfo.setSeries(fanInfo.getSeries());
                            fanInfo.setK(fanInfo.getK());
//                            fanInfo.setMotorEff(BaseDataUtil.doubleConversionString(parser.getMotorEff(dllResult.getSupplier(),dllResult.getType(),fanInfo.getMotorPower(), fanInfo.getPole()))+"%")
                            fanInfo.setPole(BaseDataUtil.doubleConversionString(pDoubleDataSYW[0]));
//                            fanInfo.setEfficiency(BaseDataUtil.doubleConversionString(pDoubleDataSYW[1]));// 效率
                            fanInfo.setHz(String.format(EngineConstant.SYS_FORMAT_2F, pDoubleDataSYW[3]));
                            fanInfo.setEngineType(FanEnginesEnum.YLD.getCode());
                            packageFaninfoEngineData(fanInfo, pDoubleData);
                            fanInfoList.add(fanInfo);
                        }
                    }
                }
            }
        }
        return fanInfoList;
    }

    @SuppressWarnings("unused")
    public List<FanInfo> packageCountResultEx2(FanInParam fanInParam, Vector<SKFanType> conditons, List<FanYldDllInfo> windMachineDLLResultList, PublicRuleParam publicRuleParam) {
        List<FanInfo> fanInfoList = new ArrayList<FanInfo>();// 返回引擎加工数据
        for (FanYldDllInfo dllResult : windMachineDLLResultList) {
            byte[] bData = dllResult.getBData();// DLL接口返回值
            double[] pDoubleData = dllResult.getPDoubleData();// 调用风机DLL接口返回值
            String fanType = dllResult.getFanType();// 机组型号 39CQ0608
            String fanSupplier = dllResult.getFanSupplier();// 供应商 A：亿利达 + 谷科
            String seriesNames = new String(bData, 0, 10).trim();
            String subTypeNames = new String(bData, 255, 10).trim();
            String fanDia = new String(bData, 510, 10).trim();
            String motorType = new String(bData, 765, 10).trim();// 电机类型"A" "三级能效电机";"B" "防爆电机";"C""双速电机";"D" "变频电机(IC416)";如果是‘无蜗壳风机’="变频电机直联(IC416)
            if (seriesNames.substring(0, 3).equals(EngineConstant.SYS_MAP_FAN_FANMODEL_SYD)) {// 2.1.1.seriesNames// 截取3位=SYW判断// 取值：系列名称第一位到第三位，如果等于SYW
                String type = fanType.substring(fanType.length() - 4, fanType.length());// 0608
                String rpm = String.valueOf(pDoubleData[0]);// 风机转速
                String efficiency = String.valueOf(pDoubleData[1]);// 全压内效率
                String absorberPower = String.valueOf(pDoubleData[3]);// 内功率
                String outletVelocity = String.valueOf(pDoubleData[6]);// 静压
                Double motor = pDoubleData[19];// 实际噪声
                String noise = String.valueOf(pDoubleData[38]);// 总A声压级
                String pt = String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[8], 1));// 全压
                String maxSpeed = String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[45], 1));// 风机最高转速R/MIN
                String maxAbsorbedPower = String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[44], 1));// 最大吸收功率 KW
                CountUtil countUtil = new CountUtil();
                Integer js = countUtil.packageCALJSEX2(Double.valueOf(rpm));//电机级数返回一个 2、4、6、8的值
                if (motor > 0.55 && motor <= 250) {
                    List<SKFanMotor> skFanMotorList = AhuMetadata.findList(SKFanMotor.class, String.valueOf(motor),
                            String.valueOf(js));
                    for (SKFanMotor skFanMotor : skFanMotorList) {
                        for (SKFanType skFanTypePo : conditons) {
                            if (js == 1 || BaseDataUtil.stringConversionDouble(skFanMotor.getMachineSite()) > skFanTypePo.getMaxMachineSite()) {
                                continue;
                            } else {
                                String fan = skFanTypePo.getFan();
                                if ((EngineConstant.SYS_MAP_FAN_FANMODEL_SYD + fan.substring(fan.length() - 3, fan.length()) + subTypeNames).equals(EngineConstant.SYS_MAP_FAN_FANMODEL_SYD + fanDia + subTypeNames)) {//如果SYD+skFanTypePo.getFan()+R和DLL计算结果做筛选
                                    FanCountParam countParam = new FanCountParam();
                                    // =============================待优化===================================
//                        countParam.setrMotorMaxMachineSite(Double.valueOf(skFanMotorPo.getMachineSite()));// 最大机器
//                        countParam.setrMotorMaxPower(skPlugfanTypePo.getMaxPower());// 最大功率
//                        countParam.setFanSupplier(skPlugfanTypePo.getSupplier());// 供应商
                                    countParam.setJs(js);//级数
                                    countParam.setMotorType(dllResult.getMotorType());// 电机类型
                                    String fanName2 = EngineConstant.SYS_MAP_FAN_FANMODEL_FC + fan.substring(fan.length() - 3, fan.length());
                                    if (SYS_MAP_FAN_FANMODEL_K.equals(subTypeNames)) {
                                        fanName2 = EngineConstant.SYS_MAP_FAN_FANMODEL_FC + fanDia + subTypeNames;
                                    }
                                    countParam.setFanName(fanName2);// 风机名称
                                    countParam.setMotor(motor);// 功率
                                    countParam.setJs(js);// 电机级数
                                    countParam.setOutletVelocity(outletVelocity);// 出风口风速
                                    countParam.setEfficiency(efficiency);// 效率
                                    countParam.setAbsorberPower(absorberPower);// 吸收功率
                                    countParam.setRpm(rpm);// 转速
                                    countParam.setNoise(noise);// 噪音
                                    FanInfo fanInfo = countUtil.packageResult2R(countParam, skFanTypePo, fanInParam);
                                    fanInfo.setTotalPressure(pt);//全压
                                    fanInfo.setMaxRPM(maxSpeed);//风机最高转速R/MIN
                                    fanInfo.setMaxAbsorbedPower(maxAbsorbedPower);//最大吸收功率
                                    fanInfo.setCurve(dllResult.getCurve());
                                    fanInfo.setModel(fanInfo.getModel());
                                    fanInfo.setSeries(fanInfo.getSeries());
                                    fanInfo.setK(fanInfo.getK());
                                    fanInfo.setMotorEff(BaseDataUtil.doubleConversionString(parser.getMotorEff(fanInParam.getSupplier(), fanInParam.getType(), fanInfo.getMotorPower(), fanInfo.getPole())) + "%");
                                    fanInfo.setEngineType(FanEnginesEnum.YLD.getCode());
                                    packageFaninfoEngineData(fanInfo, pDoubleData);
                                    fanInfoList.add(fanInfo);
                                    break;
                                } else {
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }
        return fanInfoList;
    }

    public List<FanInfo> packageCountResultExSYQ(FanInParam fanInParam, Vector<SKFanType> conditons, List<FanYldDllInfo> windMachineDLLResultList, PublicRuleParam publicRuleParam) {
        List<FanInfo> fanInfoList = new ArrayList<FanInfo>();// 返回引擎加工数据
        for (FanYldDllInfo dllResult : windMachineDLLResultList) {
            byte[] bData = dllResult.getBData();// DLL接口返回值
            double[] pDoubleData = dllResult.getPDoubleData();// 调用风机DLL接口返回值
            String fanType = dllResult.getFanType();// 机组型号 39CQ0608
            String fanSupplier = dllResult.getFanSupplier();// 供应商 A：亿利达 + 谷科
            String seriesNames = new String(bData, 0, 10).trim();
            String subTypeNames = new String(bData, 255, 10).trim();
            String fanDia = new String(bData, 510, 10).trim();
            String motorType = new String(bData, 765, 10).trim();// 电机类型"A" "三级能效电机";"B" "防爆电机";"C""双速电机";"D" "变频电机(IC416)";如果是‘无蜗壳风机’="变频电机直联(IC416)
            if (seriesNames.substring(0, 3).equals(EngineConstant.SYS_MAP_FAN_FANMODEL_SYQ)) {// 2.1.1.seriesNames// 截取3位=SYW判断// 取值：系列名称第一位到第三位，如果等于SYW
                String type = fanType.substring(fanType.length() - 4, fanType.length());// 0608
                String rpm = String.valueOf(pDoubleData[0]);// 风机转速
                String efficiency = String.valueOf(pDoubleData[1]);// 全压内效率
                String absorberPower = String.valueOf(pDoubleData[3]);// 内功率
                String outletVelocity = String.valueOf(pDoubleData[6]);// 静压
                Double motor = pDoubleData[19];// 实际噪声
                String noise = String.valueOf(pDoubleData[38]);// 总A声压级
                String pt = String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[8], 1));// 全压
                String maxSpeed = String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[45], 1));// 风机最高转速R/MIN
                String maxAbsorbedPower = String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[44], 1));// 最大吸收功率 KW
                CountUtil countUtil = new CountUtil();
                Integer js = countUtil.packageCALJSEX2(Double.valueOf(rpm));//电机级数返回一个 2、4、6、8的值
                if (motor >= 0.75 && motor <= 250) {
                    List<SKFanMotor> skFanMotorList = AhuMetadata.findList(SKFanMotor.class, String.valueOf(motor),
                            String.valueOf(js));
                    for (SKFanMotor skFanMotor : skFanMotorList) {
                        for (SKFanType skFanTypePo : conditons) {
                            if (js == 1 || BaseDataUtil.stringConversionDouble(skFanMotor.getMachineSite()) > skFanTypePo.getMaxMachineSite()) {
                                continue;
                            } else {
                                String fan = skFanTypePo.getFan();
                                if ((EngineConstant.SYS_MAP_FAN_FANMODEL_SYQ + fan.substring(fan.length() - 3, fan.length()) + subTypeNames).equals(EngineConstant.SYS_MAP_FAN_FANMODEL_SYQ + fanDia + subTypeNames)) {//如果SYD+skFanTypePo.getFan()+R和DLL计算结果做筛选
                                    FanCountParam countParam = new FanCountParam();
                                    // =============================待优化===================================
                                    countParam.setJs(js);//级数
                                    countParam.setMotorType(dllResult.getMotorType());// 电机类型
//                                    String fanName2 = EngineConstant.SYS_MAP_FAN_FANMODEL_FC + fan.substring(fan.length() - 3, fan.length());
                                    String fanName2 = EngineConstant.SYS_MAP_FAN_FANMODEL_SYQ + fanDia + subTypeNames;
                                    countParam.setFanName(fanName2);// 风机名称
                                    countParam.setMotor(motor);// 功率
                                    countParam.setJs(js);// 电机级数
                                    countParam.setOutletVelocity(outletVelocity);// 出风口风速
                                    countParam.setEfficiency(efficiency);// 效率
                                    countParam.setAbsorberPower(absorberPower);// 吸收功率
                                    countParam.setRpm(rpm);// 转速
                                    countParam.setNoise(noise);// 噪音
                                    FanInfo fanInfo = countUtil.packageResult2R(countParam, skFanTypePo, fanInParam);
                                    fanInfo.setTotalPressure(pt);//全压
                                    fanInfo.setMaxRPM(maxSpeed);//风机最高转速R/MIN
                                    fanInfo.setMaxAbsorbedPower(maxAbsorbedPower);//最大吸收功率
                                    fanInfo.setCurve(dllResult.getCurve());
                                    fanInfo.setModel(fanInfo.getModel());
                                    fanInfo.setSeries(fanInfo.getSeries());
                                    fanInfo.setK(fanInfo.getK());
                                    fanInfo.setMotorEff(BaseDataUtil.doubleConversionString(parser.getMotorEff(fanInParam.getSupplier(), fanInParam.getType(), fanInfo.getMotorPower(), fanInfo.getPole())) + "%");
                                    fanInfo.setEngineType(FanEnginesEnum.YLD.getCode());
                                    packageFaninfoEngineData(fanInfo, pDoubleData);
                                    fanInfoList.add(fanInfo);
                                    break;
                                } else {
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }
        return fanInfoList;
    }

    private FanInfo packageFaninfoEngineData(FanInfo fanInfo, double[] pDoubleData) {
        fanInfo.setEngineData1(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[11], 0)));
        fanInfo.setEngineData2(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[12], 0)));
        fanInfo.setEngineData3(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[13], 0)));
        fanInfo.setEngineData4(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[14], 0)));
        fanInfo.setEngineData5(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[15], 0)));
        fanInfo.setEngineData6(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[16], 0)));
        fanInfo.setEngineData7(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[17], 0)));
        fanInfo.setEngineData8(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[18], 0)));
        fanInfo.setEngineData9(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[30], 0)));
        fanInfo.setEngineData10(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[31], 0)));
        fanInfo.setEngineData11(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[32], 0)));
        fanInfo.setEngineData12(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[33], 0)));
        fanInfo.setEngineData13(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[34], 0)));
        fanInfo.setEngineData14(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[35], 0)));
        fanInfo.setEngineData15(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[36], 0)));
        fanInfo.setEngineData16(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[37], 0)));
        fanInfo.setAPower(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[20], 1)));
        fanInfo.setASoundPressure(String.valueOf(BaseDataUtil.decimalConvert(pDoubleData[38], 1)));

        return fanInfo;
    }

}

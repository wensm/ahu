package com.carrier.ahu.engine.coilWater4d.entity;

import lombok.Data;

/**
 * @author gaok2
 * @PackageName:com.carrier.ahu.engine.coilWater4d.entity
 * @className: CoilCoffInfo
 * @Description:
 * @date 2021/2/23 8:34
 */

/**
 * 盘管系数实体类
 */
@Data
public class CoilCoffInfo {
    //片距
    private String finDensity;

    //翅片材质
    private String finType;
    
    //排数
    private String rows;
    
    //管径
    private String tubeDiameter;

    //盘管类型
    private String coilType;
    
    //季节
    private String season;
    
    //盘管系数
    private String surfEff;

    //进水温度100度盘管系数
    private String surfEff100;
}

package com.carrier.ahu.engine.heatRecycle.dri;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import org.assertj.core.util.Lists;

import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.heatRecycle.HeatRecycle;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.engine.heatrecycle.DRIBean;
import com.carrier.ahu.engine.heatrecycle.F2200Lib;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWWheel;
import com.carrier.ahu.metadata.entity.heatrecycle.WheelInfo;
import com.carrier.ahu.metadata.entity.heatrecycle.WheelPrice;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.heatrecycle.EfficientParam;
import com.carrier.ahu.util.heatrecycle.HeatRecycleUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by Braden Zhou on 2019/01/22.
 */
public class DRIHeatRecycle extends HeatRecycle {

    private enum DRIType {
        MOLECULAR("1"), SILICA_GEL("2"), PURE_ALUMINUM("3");

        private String code;

        private DRIType(String code) {
            this.code = code;
        }
    }

    private class PartDRIWheel {
        int wheeldia;
        int wheeltype;

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + wheeldia;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            PartDRIWheel other = (PartDRIWheel) obj;
            if (!getOuterType().equals(other.getOuterType())) {
                return false;
            }
            if (wheeldia != other.wheeldia) {
                return false;
            }
            return true;
        }

        private DRIHeatRecycle getOuterType() {
            return DRIHeatRecycle.this;
        }

    }

    public List<HeatRecycleInfo> getHeatRecyleInfo(HeatRecycleParam heatRecycleParam) throws Exception {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();
        String unitModel = heatRecycleParam.getSerial();// 机组型号
        double supplyair = heatRecycleParam.getNAVolume();// 新风风量
        double returnAir = heatRecycleParam.getRAVolume();// 回风风量
        double inputOAS_DBT = heatRecycleParam.getSNewDryBulbT();// 夏季新风干球温度
        double inputOAS_WBT = heatRecycleParam.getSNewWetBulbT();// 夏季新风湿球温度
        double inputRaS_DBT = heatRecycleParam.getSInDryBulbT();// 夏季回风干球温度
        double inputRaS_RH = heatRecycleParam.getSInRelativeT();// 夏季回风相对湿度
        boolean DRIunitSI = true;
        double DRIinputSupplyAir = heatRecycleParam.getNAVolume();// 新风
        if (Math.abs(supplyair - 3780) < 0.001) {
            DRIinputSupplyAir = 3790;
        } else {
            DRIinputSupplyAir = supplyair;
        }
        double DRIinputReturnAir = heatRecycleParam.getRAVolume();// 回风
        if (Math.abs(returnAir - 3780) < 0.001) {
            DRIinputReturnAir = 3790;
        } else {
            DRIinputReturnAir = returnAir;
        }
        double DRIinputOAS_DBT = inputOAS_DBT;
        double DRIinputOAS_WBT = inputOAS_WBT;
        double DRIinputRaS_DBT = inputRaS_DBT;
        double DRIinputRaS_RH = inputRaS_RH;
        boolean DRIwheelSensible = true;
        DRIBean driBean = null;
        driBean = F2200Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir, DRIinputOAS_DBT,
                DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, null);
        
        if(driBean.getDRIoutRaVelocity()>5.5 || driBean.getDRIoutSaVelocity()>5.5) {
        	return heatRecycleInfoList;
        }

        String ECOFRESHModel = driBean.getDRIOutECOFRESHModel();
        double saEfficiencySummerTotal = driBean.getDRIoutSaEfficiencySummerTotal();// 引擎
        double saEfficiencySummerExhaustCondensation = driBean.getDRIoutEfficiencySummerExhaustCondensation();// 引擎
        double saEfficiencySummerExhaustLatent = driBean.getDRIoutEfficiencySummerExhaustLatent();// 引擎

        double SaEfficiencyWinterTotal = driBean.getDRIoutSaEfficiencyWinterTotal();// 引擎
        double SaEfficiencyWinterTempEffectiveness = driBean.getDRIoutTempEffectiveness();// 引擎
        double SaEfficiencyWinterHumEffectiveness = driBean.getDRIoutHumEffectiveness();// 引擎

        double outSaPDropWinter = driBean.getDRIoutSaPDropWinter();
        double outSaPDropSummer = driBean.getDRIoutSaPDropSummer();
        double calwheeldia;
        if (ECOFRESHModel.length() == 13) {
            calwheeldia = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 7));
        } else {
            calwheeldia = BaseDataUtil.stringConversionDouble(ECOFRESHModel.substring(4, 8));
        }
        String efficiencyType = EngineConstant.SYS_STRING_NUMBER_1;
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel, efficiencyType);
        double maxwheeldia;
        double wheeltype;
        if (heatRecycleParam.getSerial().contains(SystemCalculateConstants.AHU_PRODUCT_39CQ)) {
            maxwheeldia = partWWheelPo.getWheelDiaByAir39CQ();
            wheeltype = partWWheelPo.getWheelTypeByAir39CQ();
        } else {
            maxwheeldia = partWWheelPo.getWheelDiaByAir39G();
            wheeltype = partWWheelPo.getWheelTypeByAir39G();
        }
        int ecofreshmodel = BaseDataUtil.doubleConversionInteger(calwheeldia);
        if (calwheeldia > maxwheeldia) {
            double DRIinputModel = wheeltype;
            driBean = F2200Lib.getInstance().calculate(DRIunitSI, DRIinputSupplyAir, DRIinputReturnAir, DRIinputOAS_DBT,
                    DRIinputOAS_WBT, DRIinputRaS_DBT, DRIinputRaS_RH, DRIwheelSensible, wheeltype);
            
            if(driBean.getDRIoutRaVelocity()>5.5 || driBean.getDRIoutSaVelocity()>5.5) {
            	return heatRecycleInfoList;
            }

            saEfficiencySummerTotal = driBean.getDRIoutSaEfficiencySummerTotal();// 引擎
            saEfficiencySummerExhaustCondensation = driBean.getDRIoutEfficiencySummerExhaustCondensation();// 引擎
            saEfficiencySummerExhaustLatent = driBean.getDRIoutEfficiencySummerExhaustLatent();// 引擎

            SaEfficiencyWinterTotal = driBean.getDRIoutSaEfficiencyWinterTotal();// 引擎
            SaEfficiencyWinterTempEffectiveness = driBean.getDRIoutTempEffectiveness();// 引擎
            SaEfficiencyWinterHumEffectiveness = driBean.getDRIoutHumEffectiveness();// 引擎

            outSaPDropWinter = driBean.getDRIoutSaPDropWinter();
            outSaPDropSummer = driBean.getDRIoutSaPDropSummer();
            ECOFRESHModel = driBean.getDRIOutECOFRESHModel();
            ecofreshmodel = BaseDataUtil.doubleConversionInteger(maxwheeldia);
        }
        // if (EmptyUtil.isNotEmpty(ECOFRESHModel)) {
        // if (ECOFRESHModel.length() == 13) {
        // ecofreshmodel = BaseDataUtil.stringConversionInteger(ECOFRESHModel.substring(4, 7));
        // } else {
        // ecofreshmodel = BaseDataUtil.stringConversionInteger(ECOFRESHModel.substring(4, 8));
        // }
        // }

        // add summer
        EfficientParam outEfficientParam = new EfficientParam();
        outEfficientParam.setN01(ecofreshmodel);
        outEfficientParam.setR01(BaseDataUtil.decimalConvert(outSaPDropSummer, 1));
        outEfficientParam.setR03(BaseDataUtil.decimalConvert(saEfficiencySummerTotal, 1));
        outEfficientParam.setR05(BaseDataUtil.decimalConvert(saEfficiencySummerExhaustCondensation, 1));
        outEfficientParam.setR06(BaseDataUtil.decimalConvert(saEfficiencySummerExhaustLatent, 1));

        EfficientParam exhaustEfficientParam = new EfficientParam();
        exhaustEfficientParam.setN01(ecofreshmodel);
        exhaustEfficientParam.setR01(BaseDataUtil.decimalConvert(outSaPDropWinter, 1));
        exhaustEfficientParam.setR03(BaseDataUtil.decimalConvert(saEfficiencySummerTotal, 1));
        exhaustEfficientParam.setR05(BaseDataUtil.decimalConvert(saEfficiencySummerExhaustCondensation, 1));
        exhaustEfficientParam.setR06(BaseDataUtil.decimalConvert(saEfficiencySummerExhaustLatent, 1));

        HeatRecycleInfo summerHRI = getSummerHeatRecycleInfo(heatRecycleParam, outEfficientParam,
                exhaustEfficientParam);
        summerHRI.setReturnSeason(EngineConstant.JSON_AHU_SEASON_SUMMER);
        summerHRI.setReturnWheelSize(String.valueOf(ecofreshmodel));
        heatRecycleInfoList.add(summerHRI);

        // add winter
        outEfficientParam = new EfficientParam();
        outEfficientParam.setN01(ecofreshmodel);
        outEfficientParam.setR01(BaseDataUtil.decimalConvert(outSaPDropWinter, 1));
        outEfficientParam.setR03(BaseDataUtil.decimalConvert(SaEfficiencyWinterTotal, 1));
        outEfficientParam.setR05(BaseDataUtil.decimalConvert(SaEfficiencyWinterTempEffectiveness, 1));
        outEfficientParam.setR06(BaseDataUtil.decimalConvert(SaEfficiencyWinterHumEffectiveness, 1));

        exhaustEfficientParam = new EfficientParam();
        exhaustEfficientParam.setN01(ecofreshmodel);
        exhaustEfficientParam.setR01(BaseDataUtil.decimalConvert(outSaPDropWinter, 1));
        exhaustEfficientParam.setR03(BaseDataUtil.decimalConvert(SaEfficiencyWinterTotal, 1));
        exhaustEfficientParam.setR05(BaseDataUtil.decimalConvert(SaEfficiencyWinterTempEffectiveness, 1));
        exhaustEfficientParam.setR06(BaseDataUtil.decimalConvert(SaEfficiencyWinterHumEffectiveness, 1));

        HeatRecycleInfo winterHRI = getWinterHeatRecycleInfo(heatRecycleParam, outEfficientParam,
                exhaustEfficientParam);
        winterHRI.setReturnSeason(EngineConstant.JSON_AHU_SEASON_WINTER);
        winterHRI.setReturnWheelSize(String.valueOf(ecofreshmodel));
        heatRecycleInfoList.add(winterHRI);
        return heatRecycleInfoList;
    }

    /**
     * 返回多组热回收计算数据供用户选择
     */
    public List<List<HeatRecycleInfo>> getHeatRecyleInfoGroups(HeatRecycleParam heatRecycleParam) throws Exception {
        List<List<HeatRecycleInfo>> heatRecycleInfoGroups = Lists.newArrayList();
        double supplyair = roundAirVolume(heatRecycleParam.getNAVolume());// 新风风量
        double returnAir = roundAirVolume(heatRecycleParam.getRAVolume());// 回风风量
        DRIBean driBean = F2200Lib.getInstance().calculate(true, supplyair, returnAir,
                heatRecycleParam.getSNewDryBulbT(),
                heatRecycleParam.getSNewWetBulbT(), heatRecycleParam.getSInDryBulbT(),
                heatRecycleParam.getSInRelativeT(), true, null);

        int calwheeldia = parseWheelDiameter(driBean.getDRIOutECOFRESHModel());
        PartDRIWheel maxDRIWheel = getMaxPartWWheelInfo(heatRecycleParam.getSerial());
        List<PartDRIWheel> partDRIWheels = this.getGroupPartWWheel(heatRecycleParam.getSerial(), calwheeldia,
                maxDRIWheel.wheeldia);
        for (PartDRIWheel partDRIWheel : partDRIWheels) {
            driBean = F2200Lib.getInstance().calculate(true, supplyair, returnAir, heatRecycleParam.getSNewDryBulbT(),
                    heatRecycleParam.getSNewWetBulbT(), heatRecycleParam.getSInDryBulbT(),
                    heatRecycleParam.getSInRelativeT(), true, new Double(partDRIWheel.wheeltype));
            if(0==driBean.getDRIoutSaEfficiencySummerTotal()) {
            	//忽略为0的数据集
            	continue;
            }
            
            if(driBean.getDRIoutRaVelocity()>5.5 || driBean.getDRIoutSaVelocity()>5.5) {
            	continue;
            }
            heatRecycleInfoGroups.add(getHeatRecycleInfoOfTwoSeasons(heatRecycleParam, driBean, partDRIWheel.wheeldia));
        }
        if(heatRecycleInfoGroups.size()==0){
            throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        return heatRecycleInfoGroups;
    }

    private double roundAirVolume(double airVolume) {
        if (Math.abs(airVolume - 3780) < 0.001) {
            return 3790;
        }
        return airVolume;
    }

    private int parseWheelDiameter(String ECOFRESHModel) {
        if (ECOFRESHModel.length() == 13) {
            return BaseDataUtil.stringConversionInteger(ECOFRESHModel.substring(4, 7));
        } else {
        	int wheelDiameter=0;
        	try {
        		String[] wheelStr=ECOFRESHModel.split("-");
        		if(EmptyUtil.isNotEmpty(wheelStr)&&wheelStr.length>=2) {
        			wheelDiameter=BaseDataUtil.stringConversionInteger(wheelStr[1]);
        		}
        	}catch(Exception e) {
        		wheelDiameter= BaseDataUtil.stringConversionInteger(ECOFRESHModel.substring(4, 8));
        	}
        	
        	return wheelDiameter;
        }
    }

    private PartDRIWheel getMaxPartWWheelInfo(String unitModel) {
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel, EngineConstant.SYS_STRING_NUMBER_1);
        return getPartDRIWheel(partWWheelPo, unitModel);
    }

    private PartDRIWheel getPartDRIWheel(PartWWheel partWWheel, String unitModel) {
        PartDRIWheel partDRIWheel = new PartDRIWheel();
        if (unitModel.contains(SystemCalculateConstants.AHU_PRODUCT_39CQ)) {
            partDRIWheel.wheeldia = partWWheel.getWheelDiaByAir39CQ();
            partDRIWheel.wheeltype = partWWheel.getWheelTypeByAir39CQ();
        } else {
            partDRIWheel.wheeldia = partWWheel.getWheelDiaByAir39G();
            partDRIWheel.wheeltype = partWWheel.getWheelTypeByAir39G();
        }
        return partDRIWheel;
    }

    private PartDRIWheel getPartDRIWheel(WheelInfo wheelInfo) {
        PartDRIWheel partDRIWheel = new PartDRIWheel();
        partDRIWheel.wheeldia = wheelInfo.getWheelDia();
        partDRIWheel.wheeltype = wheelInfo.getWheelType();
        return partDRIWheel;
    }

    private List<PartDRIWheel> getGroupPartWWheel(String unitModel, int calwheeldia, int maxwheeldia) {
        LinkedList<PartDRIWheel> results = new LinkedList<>();
        List<WheelInfo> wheelInfos = AhuMetadata.findList(WheelInfo.class, "DRI");
        if (calwheeldia >= maxwheeldia) {
            for (int i = 0; i < wheelInfos.size(); i++) {
                PartDRIWheel partDRIWheel = getPartDRIWheel(wheelInfos.get(i));
                if (!results.contains(partDRIWheel)) {
                    results.add(partDRIWheel);
                }
                if (results.size() > 3) {
                    results.pop();
                }
                if (partDRIWheel.wheeldia >= maxwheeldia) {
                    break;
                }
            }
        } else if (calwheeldia < maxwheeldia) {
            for (int i = 0; i < wheelInfos.size(); i++) {
                PartDRIWheel partDRIWheel = getPartDRIWheel(wheelInfos.get(i));
                if (!results.contains(partDRIWheel)) {
                    results.add(partDRIWheel);
                }
                if (results.size() > 3) {
                    results.pop();
                }
                
                if (partDRIWheel.wheeldia >= calwheeldia && partDRIWheel.wheeldia <= maxwheeldia) {
                    if (results.size() == 3) {
                        break;
                    }
                }
            }
        }
        return results;
    }

    private List<HeatRecycleInfo> getHeatRecycleInfoOfTwoSeasons(HeatRecycleParam heatRecycleParam, DRIBean driBean,
            double ecofreshmodel) throws Exception {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();

        String priceCode = String.format("HRW-%.0f-MS200", ecofreshmodel);
        WheelPrice wheelPrice = this.getHeatRecylePrice(priceCode);

        // add summer
        EfficientParam outEfficientParam = new EfficientParam();
        outEfficientParam.setN01(ecofreshmodel);
        outEfficientParam.setR01(BaseDataUtil.decimalConvert(driBean.getDRIoutSaPDropSummer(), 1));
        outEfficientParam.setR03(BaseDataUtil.decimalConvert(driBean.getDRIoutSaEfficiencySummerTotal(), 1));
        outEfficientParam
                .setR05(BaseDataUtil.decimalConvert(driBean.getDRIoutEfficiencySummerExhaustCondensation(), 1));
        outEfficientParam.setR06(BaseDataUtil.decimalConvert(driBean.getDRIoutEfficiencySummerExhaustLatent(), 1));

        EfficientParam exhaustEfficientParam = new EfficientParam();
        exhaustEfficientParam.setN01(ecofreshmodel);
        exhaustEfficientParam.setR01(BaseDataUtil.decimalConvert(driBean.getDRIoutSaPDropWinter(), 1));
        exhaustEfficientParam.setR03(BaseDataUtil.decimalConvert(driBean.getDRIoutSaEfficiencySummerTotal(), 1));
        exhaustEfficientParam
                .setR05(BaseDataUtil.decimalConvert(driBean.getDRIoutEfficiencySummerExhaustCondensation(), 1));
        exhaustEfficientParam.setR06(BaseDataUtil.decimalConvert(driBean.getDRIoutEfficiencySummerExhaustLatent(), 1));

        HeatRecycleInfo summerHRI = getSummerHeatRecycleInfo(heatRecycleParam, outEfficientParam,
                exhaustEfficientParam);
        summerHRI.setReturnSeason(EngineConstant.JSON_AHU_SEASON_SUMMER);
        summerHRI.setReturnWheelSize(String.valueOf(ecofreshmodel));
        //summerHRI.setMaterialPrice(String.format("%.0f/%.0f", wheelPrice.getMaterialPrice(), wheelPrice.getLabourPrice()));
        summerHRI.setMaterialPrice(String.format("%.0f", wheelPrice.getMaterialPrice()));
        summerHRI.setExchangeModel(priceCode);
        heatRecycleInfoList.add(summerHRI);

        // add winter
        outEfficientParam = new EfficientParam();
        outEfficientParam.setN01(ecofreshmodel);
        outEfficientParam.setR01(BaseDataUtil.decimalConvert(driBean.getDRIoutSaPDropWinter(), 1));
        outEfficientParam.setR03(BaseDataUtil.decimalConvert(driBean.getDRIoutSaEfficiencyWinterTotal(), 1));
        outEfficientParam.setR05(BaseDataUtil.decimalConvert(driBean.getDRIoutTempEffectiveness(), 1));
        outEfficientParam.setR06(BaseDataUtil.decimalConvert(driBean.getDRIoutHumEffectiveness(), 1));

        exhaustEfficientParam = new EfficientParam();
        exhaustEfficientParam.setN01(ecofreshmodel);
        exhaustEfficientParam.setR01(BaseDataUtil.decimalConvert(driBean.getDRIoutSaPDropWinter(), 1));
        exhaustEfficientParam.setR03(BaseDataUtil.decimalConvert(driBean.getDRIoutSaEfficiencyWinterTotal(), 1));
        exhaustEfficientParam.setR05(BaseDataUtil.decimalConvert(driBean.getDRIoutTempEffectiveness(), 1));
        exhaustEfficientParam.setR06(BaseDataUtil.decimalConvert(driBean.getDRIoutHumEffectiveness(), 1));

        HeatRecycleInfo winterHRI = getWinterHeatRecycleInfo(heatRecycleParam, outEfficientParam,
                exhaustEfficientParam);
        winterHRI.setReturnSeason(EngineConstant.JSON_AHU_SEASON_WINTER);
        winterHRI.setReturnWheelSize(String.valueOf(ecofreshmodel));
        //winterHRI.setMaterialPrice(String.format("%.0f/%.0f", wheelPrice.getMaterialPrice(), wheelPrice.getLabourPrice()));
        winterHRI.setMaterialPrice(String.format("%.0f", wheelPrice.getMaterialPrice()));
        heatRecycleInfoList.add(winterHRI);
        return heatRecycleInfoList;
    }

    private WheelPrice getHeatRecylePrice(String wheelDia) {
        
        WheelPrice wheelPrice = AhuMetadata.findOne(WheelPrice.class, wheelDia);
        if (wheelPrice == null) {
            // TODO 价格不完整，需要补充
            wheelPrice = new WheelPrice();
        }
        return wheelPrice;
    }

}

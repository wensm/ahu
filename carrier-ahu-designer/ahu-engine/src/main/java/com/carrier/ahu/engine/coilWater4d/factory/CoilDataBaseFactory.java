package com.carrier.ahu.engine.coilWater4d.factory;

import java.util.List;

import org.springframework.stereotype.Component;

import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.coil.UnitTypeRow;

@Component
public class CoilDataBaseFactory {
    public static String getUnitTypeRow(CoilInParam coilInParam) {
        String serial = coilInParam.getSerial();
        String coilType = coilInParam.getCoilType();
        String tubeDiameter = coilInParam.getTubeDiameter();
        List<UnitTypeRow> unitTypeRowList = AhuMetadata.findAll(UnitTypeRow.class);
        for (UnitTypeRow unitTypeRow : unitTypeRowList) {
            if (unitTypeRow.getUnit().equals(serial) && unitTypeRow.getTubeDiameter().equals(tubeDiameter) && unitTypeRow.getType().equals(coilType)) {
                return unitTypeRow.getRows().replace("-1,", EngineConstant.SYS_BLANK);
            }
        }
        return EngineConstant.SYS_BLANK;
    }
}

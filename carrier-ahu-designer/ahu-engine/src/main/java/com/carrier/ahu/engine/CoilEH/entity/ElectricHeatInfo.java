package com.carrier.ahu.engine.CoilEH.entity;

import lombok.Data;

@Data
public class ElectricHeatInfo {
    private String RowNum;
    private String GroupC;
}

package com.carrier.ahu.engine.coil.util;

/**
 * Created by LIANGD4 on 2017/6/28.
 */
public class FormulaUtil {

    //增加计算公式
    public static Double fTemperatureConvert(Double ParmCF, Boolean ParmFlag) {
        if (ParmFlag) {
            return ParmCF * 9 / 5 + 32;
        } else {
            return (ParmCF - 32) * 5 / 9;
        }
    }

}

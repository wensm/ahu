package com.carrier.ahu.engine.heatRecycle;

import java.util.List;

import lombok.Data;

/**
 * Created by liangd4 on 2018/2/9.
 */
@Data
public class HeatRecycleBean {
    private List<HeatRecycleInfo> heatRecycleInfoList;
}

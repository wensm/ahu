package com.carrier.ahu.engine.fanGk4k;

import java.util.List;

import com.carrier.ahu.engine.fan.util.Utils;
import com.carrier.ahu.util.EmptyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.factory.AFanEngine;
import com.carrier.ahu.engine.fanGk4k.factory.FanGkDllInParamFactory;
import com.carrier.ahu.engine.fanGk4k.factory.GukeRunner;
import com.carrier.ahu.engine.fanGk4k.factory.GukeSingleMap;
import com.carrier.ahu.engine.fanGk4k.param.FanGKDllInParam;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.unit.KeyGenerator;
import com.carrier.ahu.util.DateUtil;

/**
 * Created by LIANGD4 on 2017/12/17.
 */
public class FanGkServiceImpl extends AFanEngine {
    private static final Logger LOGGER = LoggerFactory.getLogger(FanGkServiceImpl.class.getName());

    private static final FanGkDllInParamFactory fanGkDllInParamFactory = new FanGkDllInParamFactory();
    // private static final FanGkDllFactory fanGkDllFactory = new FanGkDllFactory();
    // private static final FanGkResultFactory fanGkResultFactory = new
    // FanGkResultFactory();

    @Override
    public List<FanInfo> cal(FanInParam fanInParam) throws FanEngineException {
        LOGGER.info("GGGGGGGG FanGkServiceImpl GK Engine Begin KKKKKKKKK"+ DateUtil.getStringDate());
        List<FanGKDllInParam> fanGKDllInParamList = fanGkDllInParamFactory.packageParam(fanInParam,this.getFanConditions());
        if(EmptyUtil.isEmpty(fanGKDllInParamList)){//不满足封装条件直接返回null
            return null;
        }
        // List<FanGkDllInfo> fanGkDllInfoList = fanGkDllFactory.packageGKEngine(fanGKDllInParamList);
        // fanInfoList = fanGkResultFactory.packageResult(fanGkDllInfoList);
        String key = KeyGenerator.getTimeStampSequence();
        GukeSingleMap<List<FanGKDllInParam>> inVal = new GukeSingleMap<List<FanGKDllInParam>>();
        inVal.put(key, fanGKDllInParamList);
        GukeRunner.getInstance().begin();
        GukeRunner.getInstance().setInput(inVal);
        List<FanInfo> fanInfoList = GukeRunner.getInstance().getOutput(key);
        fanInfoList = Utils.filterMotorMachinesite(fanInParam, fanInfoList);
        LOGGER.info("GGGGGGGG FanGkServiceImpl GK Engine end KKKKKKKKK"+ DateUtil.getStringDate());
        return fanInfoList;
    }
	@Override
	public FanInfo calByRule(FanInParam windMachineInParam, PublicRuleParam publicRuleParam) throws FanEngineException {
		// TODO Auto-generated method stub
		return null;
	}
}

package com.carrier.ahu.engine.fanYld4k;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.factory.AFanEngine;
import com.carrier.ahu.engine.fanYld4k.entity.FanYldDllInfo;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllEx3Factory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllInEx3ParamFactory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldResultFactory;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.FanYldDllInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.util.DateUtil;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by liangd4 on 2017/8/29. 风机引擎计算
 */
public class FanYldServiceImpl extends AFanEngine {

	private static final Logger LOGGER = LoggerFactory.getLogger(FanYldServiceImpl.class.getName());
	private static final FanYldDllInEx3ParamFactory FAN_YLD_DLL_IN_EX_3_PARAM_FACTORY = new FanYldDllInEx3ParamFactory();
	private static final FanYldDllEx3Factory FAN_YLD_DLL_EX_3_FACTORY = new FanYldDllEx3Factory();
	private static final FanYldResultFactory fanYldResultFactory = new FanYldResultFactory();

	// 风机引擎计算
	@Override
	public List<FanInfo> cal(FanInParam fanInParam) throws FanEngineException {
		LOGGER.info("YLDYLDYLD FanYldServiceImpl YLD Engine Begin "+ DateUtil.getStringDate());
		/* 封装YLDDll参数 */
		FanYldDllInParam fanYldDllInParam = FAN_YLD_DLL_IN_EX_3_PARAM_FACTORY.packageParam(fanInParam);
		LOGGER.info("YLDYLDYLD FanYldServiceImpl YLD FanYldDllInParam "+ DateUtil.getStringDate());
		/* 根据加工参数调用Dll */
		List<FanYldDllInfo> fanYldDllInfoList = FAN_YLD_DLL_EX_3_FACTORY.fanSelectionEx3(fanYldDllInParam,fanInParam);
		LOGGER.info("YLDYLDYLD FanYldServiceImpl YLD fanYldDllInfoList "+ DateUtil.getStringDate());
		/* 根据Dll返回结果封装数据 */
		List<FanInfo> fanInfoList = fanYldResultFactory.packageCountResultEx3(this.getFanConditions(),fanYldDllInfoList,null);
		LOGGER.info("YLDYLDYLD FanYldServiceImpl YLD Engine end "+ DateUtil.getStringDate());
		return fanInfoList;
	}

	@Override
	public FanInfo calByRule(FanInParam fanInParam, PublicRuleParam publicRuleParam) throws FanEngineException{
		/* 封装YLDDll参数 */
		FanYldDllInParam fanYldDllInParam = FAN_YLD_DLL_IN_EX_3_PARAM_FACTORY.packageParam(fanInParam);
		/* 根据加工参数调用Dll */
		List<FanYldDllInfo> fanYldDllInfoList = FAN_YLD_DLL_EX_3_FACTORY.fanSelectionEx3(fanYldDllInParam,fanInParam);
		/* 根据Dll返回结果封装数据 */
		List<FanInfo> infoList = fanYldResultFactory.packageCountResultEx3(this.getFanConditions(),fanYldDllInfoList,null);
		if (EmptyUtil.isEmpty(infoList)) {
			return null;
		}
		return infoList.get(0);
	}
}

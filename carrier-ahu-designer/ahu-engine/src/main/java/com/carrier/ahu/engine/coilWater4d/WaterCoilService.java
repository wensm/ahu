package com.carrier.ahu.engine.coilWater4d;

import java.util.List;

import org.springframework.stereotype.Service;

import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.param.CoilInParam;

/**
 * Created by liangd4 on 2017/9/7.
 * 盘管引擎计算
 * 冷水盘管、热水盘管
 */
@Service
public interface WaterCoilService {

    List<CoilInfo> getWaterCoil(CoilInParam coldWaterInParam);

}

package com.carrier.ahu.engine.fan.factory;

import java.util.List;

import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;

public interface IFanEngine {
	
	//根据页面参数计算风机引擎
    List<FanInfo> cal(FanInParam fanInParam) throws FanEngineException;

    //根据规则计算风机引擎
    FanInfo calByRule(FanInParam windMachineInParam, PublicRuleParam publicRuleParam) throws FanEngineException;

}

package com.carrier.ahu.engine.coilWater4d.param;

import java.util.List;

import lombok.Data;

@Data
public class CoilDllInParam {
    //========================调用盘管DLL使用页面参数=================//
    private List<CWDCPCParam> cPCParamList;//CPC路径使用参数
    private Double vFace;//面风速 m/s
    private Double faceArea;//迎风面积
    private Double tdbAirIn;//进风干球温度 oC
    private Double twbAirIn;//进风湿球温度 oC
    private double rtAirIn;//进风相对湿度
    private Double pAirIn;//进气压力 kPa
    private Double surfEff;//盘管系数
    private Double RADMULT;// 数据库对应字段
    private Double DPDMULT;// 数据库对应字段
    private Double RAWMULT;// 数据库对应字段
    private Double DPWMULT;// 数据库对应字段
    private Double fFouling;//数据库查询 m2*K/W
    private Double ODEXP;//数据库查询
    private Double TW;//数据库查询
    private Double idBend;//数据库查询
    private Double idHairpin;//数据库查询
    private Double PT;//数据库查询
    private Double PR;//数据库查询
    private Double tCond;//数据库查询
    private Long curveNum;//数据库查询
    private Double tubeNum;//数据库查询
    private Double fThick;//数据库查询
    private Double fCond;//数据库查询
    private Double FPI;//片距
    private String CIR;//回路
    private Double tWaterIn;//水温升
    private Double dtWater;//水流量
    private Long fluidId;//介质
    private Double fluidConcentration;//浓度
    private Double height;//海拔高度
    private String unitType;//机组型号
    private Double ewt;//进水温度
    private String season;//季节 W:冬、S:夏、
    private String coilType;//盘管类型 D:冷水盘管、DX:直接蒸发式
    private int tubeDiameter;//转换后的管径
    private String calculationConditions;//水温升、水流量标识

    /*直接蒸发式盘管增加以下属性*/
    private Double sst;//系数
    private Double dtsh;//过热度
    private Double sct;//饱和冷凝温度
    private Double tliq;//饱和冷凝温度-过冷度

    /*直接蒸发式盘管增加属性*/
    private String Cond;//冷凝器进风温度
    private String SatCond;//饱和冷凝温度
    private String CDU;//unit
    private String subClg;//过冷度
}

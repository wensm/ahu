package com.carrier.ahu.engine.heatRecycle.yufeng;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.engine.heatrecycle.YuFengLib;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWPlate;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWWheel;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.heatrecycle.HeatRecycleUtils;
import com.sun.jna.ptr.DoubleByReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LIANGD4 on 2017/12/19.
 */
public class HeatRecycleYuFengDLLFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeatRecycleYuFengDLLFactory.class.getName());
    private static final HeatRecycleYuFengResultFactory HEAT_RECYCLE_YU_FENG_RESULT_FACTORY = new HeatRecycleYuFengResultFactory();

    //yufengDLL调用封装
    public List<HeatRecycleInfo> getYuFengDll(HeatRecycleParam heatRecycleParam) throws TempCalErrorException {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();
        boolean winter = heatRecycleParam.isEnableWinter();//冬季标识
        //夏季默认封装，旧软件无论是单选冬季或者夏季 默认夏季都计算
        HeatRecycleInfo sHeatRecycleInfo = getYuFengEngineSummer(heatRecycleParam);
        if (!EmptyUtil.isEmpty(sHeatRecycleInfo)) {
            heatRecycleInfoList.add(sHeatRecycleInfo);
        } else {
            LOGGER.error("YuFeng Engine Cal Error Season S");
        }
        if (winter) {
            HeatRecycleInfo wHeatRecycleInfo = getYuFengEngineWinter(heatRecycleParam);
            if (!EmptyUtil.isEmpty(wHeatRecycleInfo)) {
                heatRecycleInfoList.add(wHeatRecycleInfo);
            } else {
                LOGGER.error("YuFeng Engine Cal Error Season W");
            }
        }
        return heatRecycleInfoList;
    }

    //YuFeng夏季引擎调用
    private HeatRecycleInfo getYuFengEngineSummer(HeatRecycleParam heatRecycleParam) throws TempCalErrorException {
        int nWorkStatus = 0;
        int nRotorType = 1;
        int nPurge = 0;
        String unitModel = heatRecycleParam.getSerial();//机组型号
        double dPSI = heatRecycleParam.getSeaLevel();//海拔高度
        double dBV21 = heatRecycleParam.getRAVolume();//回风风量
        double dDB21 = heatRecycleParam.getSInDryBulbT();//夏季回风干球温度
        double dRH21 = heatRecycleParam.getSInRelativeT();//夏季回风相对湿度
        double dBV11 = heatRecycleParam.getNAVolume();//新风风量
        double dDB11 = heatRecycleParam.getSNewDryBulbT();//夏季新风干球温度
        double dRH11 = heatRecycleParam.getSNewRelativeT();//夏季新风相对湿度
        String rotordia = HeatRecycleUtils.getRotordiaHeatX(dBV21, EngineConstant.SYS_STRING_NUMBER_1, EngineConstant.JSON_HEATRECYCLE_POWER_EN);
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel);
        String maxrotordia = String.valueOf(partWWheelPo.getWheelDiaYuFeng());//WHEELDIA_YUFENG
        int nDiameter;
        if (BaseDataUtil.stringConversionInteger(rotordia) > BaseDataUtil.stringConversionInteger(maxrotordia)) {
            nDiameter = BaseDataUtil.stringConversionInteger(maxrotordia);
        } else {
            nDiameter = BaseDataUtil.stringConversionInteger(rotordia);
        }
        String ECOFRESHModel = String.valueOf(nDiameter);
        double velface = SystemCountUtil.velfacecal(dBV11, Double.parseDouble(ECOFRESHModel), EngineConstant.SYS_STRING_NUMBER_1);
        if (velface > 6) {
            throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        YuFengLib.SetSumParam(dBV21, dBV11, dDB21, dDB11, dRH21, dRH11);//引擎参数入参
        YuFengLib.SetWorkStatus(nDiameter, nWorkStatus, dPSI, nRotorType, nPurge);
        int ret = YuFengLib.CalcSummerCondition();
        if (ret == 0) {
            double sumPreDropOA = YuFengLib.GetSumPreDropOA();//新风压力降
            double sumPreDropRA = YuFengLib.GetSumPreDropRA();//回风压力降
            double sumAirflowSA = YuFengLib.GetSumAirflowSA();//出口送风风量
            double sumAirflowEA = YuFengLib.GetSumAirflowEA();//出口排风风量
            double sumDrybulbSA = YuFengLib.GetSumDrybulbSA();//出口送风干球温度
            double sumDrybulbEA = YuFengLib.GetSumDrybulbEA();//出口排风干球温度
            double sumRHSA = YuFengLib.GetSumRHSA();//出口送风相对湿度
            double sumRHEA = YuFengLib.GetSumRHEA();//出口排风相对湿度
            AirConditionBean sa = AirConditionUtils.FAirParmCalculate3(sumDrybulbSA, sumRHSA);
            AirConditionBean ea = AirConditionUtils.FAirParmCalculate3(sumDrybulbEA, sumRHEA);
            DoubleByReference dXin = new DoubleByReference(0.0);
            DoubleByReference dHui = new DoubleByReference(0.0);
            YuFengLib.GetSumQuanreXiaolv(dXin, dHui);//全热效率
            double sumVelocityOA = YuFengLib.GetSumVelocityOA();//入口新风迎面风速
            double sumVelocityRA = YuFengLib.GetSumVelocityRA();//入口回风迎面风速
            //全热Qt = V/3600*1.27*273/(273+T1)*(H1-H2)
            //显热Qs = V/3600*1.27*273/(273+T1)*(T1-T2)
            //潜热Ql= Qt-Qs
            AirConditionBean h1TF = AirConditionUtils.FAirParmCalculate3(dDB11, dRH11);//新风干球温度 新风相对湿度
            AirConditionBean h2TF = AirConditionUtils.FAirParmCalculate3(sa.getParmT(), sa.getParmF()); //送风干球温度 送风相对湿度
            double qt = dBV11 / 3600 * 1.27 * 273 / (273 + dDB11) * (h1TF.getParamI() - h2TF.getParamI());
            double qs = dBV11 / 3600 * 1.27 * 273 / (273 + dDB11) * (dDB11 - sa.getParmT());
            //引擎返回结果转换对象
            HeatRecycleInfo heatRecycleInfo = HEAT_RECYCLE_YU_FENG_RESULT_FACTORY.packageHeatRecycleInfo(nDiameter, EngineConstant.JSON_AHU_SEASON_SUMMER, sa.getParmT(), sa.getParmTb(), sa.getParmF(), sumPreDropOA, dXin.getValue(),
                    ea.getParmT(), ea.getParmTb(), ea.getParmF(), sumPreDropRA, dHui.getValue(), qt, qs);
            if (EngineConstant.METASEXON_PLATEHEATRECYCLE_HEATPLATE.equals(heatRecycleParam.getHeatRecoveryType())) {
                PartWPlate partWPlate = AhuMetadata.findOne(PartWPlate.class, heatRecycleParam.getSerial());
                if (null != partWPlate) {
                    heatRecycleInfo.setSectionSideL(BaseDataUtil.integerConversionString(partWPlate.getPlateWidth()));//截面边长
                    heatRecycleInfo.setHeatExchangerL(BaseDataUtil.integerConversionString(partWPlate.getPlateLength()));//热交换器长度
                    heatRecycleInfo.setReturnWheelSize(heatRecycleInfo.getSectionSideL() + EngineConstant.SYS_PUNCTUATION_STAR + heatRecycleInfo.getHeatExchangerL());//转轮尺寸
                }
            }
            return heatRecycleInfo;
        } else {
            LOGGER.error("YuFeng Engine Cal getYuFengEngineSummer Error Season S" + JSONArray.toJSONString(heatRecycleParam));
        }
        return null;
    }

    //YuFeng冬季引擎调用
    private HeatRecycleInfo getYuFengEngineWinter(HeatRecycleParam heatRecycleParam) throws TempCalErrorException {
        int nWorkStatus = 0;
        int nRotorType = 1;
        int nPurge = 0;
        String unitModel = heatRecycleParam.getSerial();//机组型号
        double dPSI = heatRecycleParam.getSeaLevel();//海拔高度
        double dBV21 = heatRecycleParam.getRAVolume();//新风风量
        double dDB21 = heatRecycleParam.getWNewDryBulbT();//冬季新风干球温度
        double dRH21 = heatRecycleParam.getWNewRelativeT();//冬季新风相对湿度
        double dBV11 = heatRecycleParam.getNAVolume();//回风风量
        double dDB11 = heatRecycleParam.getWInDryBulbT();//冬季回风干球温度
        double dRH11 = heatRecycleParam.getWInRelativeT();//冬季回风相对湿度
        String rotordia = HeatRecycleUtils.getRotordiaHeatX(dBV21, EngineConstant.SYS_STRING_NUMBER_1, EngineConstant.JSON_HEATRECYCLE_POWER_EN);
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel);
        String maxrotordia = String.valueOf(partWWheelPo.getWheelDiaYuFeng());//WHEELDIA_YUFENG
        int nDiameter;
        if (BaseDataUtil.stringConversionInteger(rotordia) > BaseDataUtil.stringConversionInteger(maxrotordia)) {
            nDiameter = BaseDataUtil.stringConversionInteger(maxrotordia);
        } else {
            nDiameter = BaseDataUtil.stringConversionInteger(rotordia);
        }
        String ECOFRESHModel = String.valueOf(nDiameter);
        double velface = SystemCountUtil.velfacecal(dBV11, Double.parseDouble(ECOFRESHModel), EngineConstant.SYS_STRING_NUMBER_1);
        if (velface > 6) {
            throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);//抛出异常到界面提醒
        }
        YuFengLib.SetWinParam(dBV21, dBV11, dDB21, dDB11, dRH21, dRH11);
        YuFengLib.SetWorkStatus(nDiameter, nWorkStatus, dPSI, nRotorType, nPurge);
        int ret = YuFengLib.CalcWinterCondition();
        if (ret == 0) {
            double winPreDropOA = YuFengLib.GetWinPreDropOA();
            double winPreDropRA = YuFengLib.GetWinPreDropRA();
            double winAirflowSA = YuFengLib.GetWinAirflowSA();
            double winAirflowEA = YuFengLib.GetWinAirflowEA();
            double winDrybulbSA = YuFengLib.GetWinDrybulbSA();
            double winDrybulbEA = YuFengLib.GetWinDrybulbEA();
            double winRHSA = YuFengLib.GetWinRHSA();
            double winRHEA = YuFengLib.GetWinRHEA();
            AirConditionBean sa = AirConditionUtils.FAirParmCalculate3(winDrybulbSA, winRHSA);
            AirConditionBean ea = AirConditionUtils.FAirParmCalculate3(winDrybulbEA, winRHEA);
            DoubleByReference dXin = new DoubleByReference(0.0);
            DoubleByReference dHui = new DoubleByReference(0.0);
            YuFengLib.GetSumQuanreXiaolv(dXin, dHui);
            double winVelocityOA = YuFengLib.GetWinVelocityOA();
            double winVelocityRA = YuFengLib.GetWinVelocityRA();
            //全热Qt = V/3600*1.27*273/(273+T1)*(H1-H2)
            //显热Qs = V/3600*1.27*273/(273+T1)*(T1-T2)
            //潜热Ql= Qt-Qs
            AirConditionBean h1TF = AirConditionUtils.FAirParmCalculate3(dDB21, dRH21);
            AirConditionBean h2TF = AirConditionUtils.FAirParmCalculate3(sa.getParmT(), sa.getParmF());
            double qt = dBV11 / 3600 * 1.27 * 273 / (273 + dDB21) * (h1TF.getParamI() - h2TF.getParamI());
            double qs = dBV11 / 3600 * 1.27 * 273 / (273 + dDB21) * (dDB21 - sa.getParmT());
            //引擎返回结果转换对象
            HeatRecycleInfo heatRecycleInfo = HEAT_RECYCLE_YU_FENG_RESULT_FACTORY.packageHeatRecycleInfo(nDiameter, EngineConstant.JSON_AHU_SEASON_WINTER, sa.getParmT(), sa.getParmTb(), sa.getParmF(), winPreDropOA, dXin.getValue(),
                    ea.getParmT(), ea.getParmTb(), ea.getParmF(), winPreDropRA, dHui.getValue(), qt, qs);
            if (EngineConstant.METASEXON_PLATEHEATRECYCLE_HEATPLATE.equals(heatRecycleParam.getHeatRecoveryType())) {
                PartWPlate partWPlate = AhuMetadata.findOne(PartWPlate.class, heatRecycleParam.getSerial());
                if (!EmptyUtil.isEmpty(partWPlate)) {
                    heatRecycleInfo.setSectionSideL(BaseDataUtil.integerConversionString(partWPlate.getPlateWidth()));//截面边长
                    heatRecycleInfo.setHeatExchangerL(BaseDataUtil.integerConversionString(partWPlate.getPlateLength()));//热交换器长度
                    heatRecycleInfo.setReturnWheelSize(heatRecycleInfo.getSectionSideL() + EngineConstant.SYS_PUNCTUATION_STAR + heatRecycleInfo.getHeatExchangerL());//转轮尺寸
                }
            }
            return heatRecycleInfo;
        } else {
            LOGGER.error("YuFeng Engine Cal getYuFengEngineWinter Error Season W" + JSONArray.toJSONString(heatRecycleParam));
        }
        return null;
    }
}

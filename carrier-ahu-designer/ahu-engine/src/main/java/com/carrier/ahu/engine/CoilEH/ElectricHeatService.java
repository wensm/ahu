package com.carrier.ahu.engine.CoilEH;

import java.util.List;

import com.carrier.ahu.engine.CoilEH.entity.ElectricHeatInfo;
import com.carrier.ahu.length.param.ElectricHeatingParam;

public interface ElectricHeatService {

    List<ElectricHeatInfo> getRowNumAndGroupC(ElectricHeatingParam electricHeatingParam);

}

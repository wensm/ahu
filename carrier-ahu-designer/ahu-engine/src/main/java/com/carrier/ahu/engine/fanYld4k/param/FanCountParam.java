package com.carrier.ahu.engine.fanYld4k.param;

/**
 * Created by liangd4 on 2017/7/3.
 */
public class FanCountParam {

    private double rMotorMaxMachineSite;//最大机器
    private double rMotorMaxPower;//最大功率
    private String motorType;//电机类型 A：三级能效电机 B：防爆电机 C：双速电机 D：变频电机 E：二级能效电机 //0.1/0.25 - MEMOKW(8/6P)
    private String fanName;//风机名称
    private double motor;//功率
    private Integer js;//电机级数
    private String fanSupplier;//供应商
    private String motorBase;//Y80M
    private String outletVelocity;//出风口风速
    private String efficiency;//效率
    private String absorberPower;//吸收功率
    private String rpm;//转速
    private String noise;//噪音

    public Double getrMotorMaxMachineSite() {
        return rMotorMaxMachineSite;
    }

    public void setrMotorMaxMachineSite(Double rMotorMaxMachineSite) {
        this.rMotorMaxMachineSite = rMotorMaxMachineSite;
    }

    public Double getrMotorMaxPower() {
        return rMotorMaxPower;
    }

    public void setrMotorMaxPower(Double rMotorMaxPower) {
        this.rMotorMaxPower = rMotorMaxPower;
    }

    public String getMotorType() {
        return motorType;
    }

    public void setMotorType(String motorType) {
        this.motorType = motorType;
    }

    public String getFanName() {
        return fanName;
    }

    public void setFanName(String fanName) {
        this.fanName = fanName;
    }

    public Double getMotor() {
        return motor;
    }

    public void setMotor(Double motor) {
        this.motor = motor;
    }

    public Integer getJs() {
        return js;
    }

    public void setJs(Integer js) {
        this.js = js;
    }

    public String getFanSupplier() {
        return fanSupplier;
    }

    public void setFanSupplier(String fanSupplier) {
        this.fanSupplier = fanSupplier;
    }

    public String getMotorBase() {
        return motorBase;
    }

    public void setMotorBase(String motorBase) {
        this.motorBase = motorBase;
    }

    public String getOutletVelocity() {
        return outletVelocity;
    }

    public void setOutletVelocity(String outletVelocity) {
        this.outletVelocity = outletVelocity;
    }

    public String getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(String efficiency) {
        this.efficiency = efficiency;
    }

    public String getAbsorberPower() {
        return absorberPower;
    }

    public void setAbsorberPower(String absorberPower) {
        this.absorberPower = absorberPower;
    }

    public String getRpm() {
        return rpm;
    }

    public void setRpm(String rpm) {
        this.rpm = rpm;
    }

    public String getNoise() {
        return noise;
    }

    public void setNoise(String noise) {
        this.noise = noise;
    }


    //    private String motortype;//0.1/0.25 - MEMOKW(8/6P)
}

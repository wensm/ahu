package com.carrier.ahu.engine.service.heatRecycle;

import java.util.List;

import org.assertj.core.util.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.engine.HeatExchangeEngineException;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleDllFactory;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by LIANGD4 on 2017/11/28.
 */
@Service
public class HeatRecycleServiceImpl implements HeatRecycleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeatRecycleServiceImpl.class.getName());
    private static final HeatRecycleDllFactory HEAT_RECYCLE_DLL_FACTORY = new HeatRecycleDllFactory();

    @Override
    public List<HeatRecycleInfo> getHeatRecycleEngine(HeatRecycleParam heatRecycleParam, String version)
            throws HeatExchangeEngineException {
        try {
            if (heatRecycleParam.getRAVolume() == heatRecycleParam.getNAVolume()
                    && heatRecycleParam.getSInDryBulbT() == heatRecycleParam.getSNewDryBulbT()
                    && heatRecycleParam.getSInWetBulbT() == heatRecycleParam.getSNewWetBulbT()) {
                throw new ApiException(ErrorCode.RECYCLE_PARAM_EQUAL);
            }
            if (heatRecycleParam.isEnableWinter()) {
                if (heatRecycleParam.getRAVolume() == heatRecycleParam.getNAVolume()
                        && heatRecycleParam.getWInDryBulbT() == heatRecycleParam.getWNewDryBulbT()
                        && heatRecycleParam.getWInWetBulbT() == heatRecycleParam.getWNewWetBulbT()) {
                    throw new ApiException(ErrorCode.RECYCLE_PARAM_EQUAL);
                }
            }

            // return HEAT_RECYCLE_DLL_FACTORY.heatRecycleDll(heatRecycleParam);
            return Lists.newArrayList(); // useless
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            LOGGER.error(e.getMessage());
            throw new HeatExchangeEngineException(
                    "HeatRecycleServiceImpl CalFailedException message : " + e.getMessage());
        }
    }

    @Override
    public List<HeatRecycleInfo> getHeatRecycleEngineByRule(HeatRecycleParam heatRecycleParam,
            PublicRuleParam publicRuleParam, String version) throws Exception {
        if (heatRecycleParam.getRAVolume() == heatRecycleParam.getNAVolume()
                && heatRecycleParam.getSInDryBulbT() == heatRecycleParam.getSNewDryBulbT()
                && heatRecycleParam.getSInWetBulbT() == heatRecycleParam.getSNewWetBulbT()) {
            throw new ApiException(ErrorCode.RECYCLE_PARAM_EQUAL);
        }
        if (heatRecycleParam.isEnableWinter()) {
            if (heatRecycleParam.getRAVolume() == heatRecycleParam.getNAVolume()
                    && heatRecycleParam.getWInDryBulbT() == heatRecycleParam.getWNewDryBulbT()
                    && heatRecycleParam.getWInWetBulbT() == heatRecycleParam.getWNewWetBulbT()) {
                throw new ApiException(ErrorCode.RECYCLE_PARAM_EQUAL);
            }
        }
        List<HeatRecycleInfo> heatRecycleInfo = HEAT_RECYCLE_DLL_FACTORY.heatRecycleDll(heatRecycleParam);
        if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
            return heatRecycleInfo;
        } else {
            LOGGER.info("HeatRecycleServiceImpl getHeatRecycleEngineByRule calculator null");
        }
        return null;
    }

    @Override
    public List<List<HeatRecycleInfo>> getHeatRecycleEngineByRule2(HeatRecycleParam heatRecycleParam,
            PublicRuleParam publicRuleParam, String version) throws Exception {
        if (heatRecycleParam.getRAVolume() == heatRecycleParam.getNAVolume()
                && heatRecycleParam.getSInDryBulbT() == heatRecycleParam.getSNewDryBulbT()
                && heatRecycleParam.getSInWetBulbT() == heatRecycleParam.getSNewWetBulbT()) {
            throw new ApiException(ErrorCode.RECYCLE_PARAM_EQUAL);
        }
        if (heatRecycleParam.isEnableWinter()) {
            if (heatRecycleParam.getRAVolume() == heatRecycleParam.getNAVolume()
                    && heatRecycleParam.getWInDryBulbT() == heatRecycleParam.getWNewDryBulbT()
                    && heatRecycleParam.getWInWetBulbT() == heatRecycleParam.getWNewWetBulbT()) {
                throw new ApiException(ErrorCode.RECYCLE_PARAM_EQUAL);
            }
        }
        return HEAT_RECYCLE_DLL_FACTORY.heatRecycleDll2(heatRecycleParam);
    }
}

package com.carrier.ahu.engine.fanKruger.factory;

import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.engine.fan.KrugerSelectInfo;
import com.carrier.ahu.engine.fan.KrugerSelection;
import com.carrier.ahu.engine.fanKruger.krugerI.KrugerEngineService;
import com.carrier.ahu.engine.fanKruger.krugerI.KrugerEngineServiceImpl;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.unit.BaseDataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.carrier.ahu.constant.CommonConstant.JSON_FAN_OUTLET_AIRFOIL;

@Component
public class FanKrugerDllCalFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(FanKrugerDllCalFactory.class.getName());

    private static final KrugerEngineService krugerEngineService = new KrugerEngineServiceImpl();

    public List<KrugerSelection> calculation(KrugerSelectInfo krugerSelectInfo, FanInParam fanInParam) {
        String fanType = fanInParam.getSerial();//机组型号
        float pressure = krugerSelectInfo.getPressure();
        String outlet = fanInParam.getOutlet();//风机形式
        List<KrugerSelection> krugerSelectionList = new ArrayList<KrugerSelection>();
        if (JSON_FAN_OUTLET_AIRFOIL.equals(outlet)) {//机翼型只调用ADA
            krugerSelectInfo.setProductType(5);
            if (BaseDataUtil.stringConversionInteger(SystemCountUtil.getUnit(fanType)) < 1117) {
                krugerSelectInfo.setPressure(pressure + 200);
            }
            List<KrugerSelection> krugerSelectionList5 = krugerEngineService.select(krugerSelectInfo);
            krugerSelectionList.addAll(krugerSelectionList5);
        } else if(CommonConstant.JSON_FAN_OUTLET_F.equals(outlet)){
        	 krugerSelectInfo.setProductType(3);
        	 if (BaseDataUtil.stringConversionInteger(SystemCountUtil.getUnit(fanType)) < 1117) {
                 krugerSelectInfo.setPressure(pressure + 200);
             }
             List<KrugerSelection> krugerSelectionList3 = krugerEngineService.select(krugerSelectInfo);
             krugerSelectionList.addAll(krugerSelectionList3);
        	
        }else if(CommonConstant.JSON_FAN_OUTLET_B.equals(outlet)){
        	krugerSelectInfo.setProductType(9);
            if (BaseDataUtil.stringConversionInteger(SystemCountUtil.getUnit(fanType)) < 1117) {
                krugerSelectInfo.setPressure(pressure + 200);
            }
            List<KrugerSelection> krugerSelectionList9 = krugerEngineService.select(krugerSelectInfo);
            krugerSelectionList.addAll(krugerSelectionList9);
    	
    }else{
            /*根据productType重新封装 FDA BDB ADA*/
            krugerSelectInfo.setProductType(3);
            if (BaseDataUtil.stringConversionInteger(SystemCountUtil.getUnit(fanType)) < 1117) {
                krugerSelectInfo.setPressure(pressure + 200);
            }
            List<KrugerSelection> krugerSelectionList3 = krugerEngineService.select(krugerSelectInfo);
            krugerSelectionList.addAll(krugerSelectionList3);
            krugerSelectInfo.setProductType(5);
            if (BaseDataUtil.stringConversionInteger(SystemCountUtil.getUnit(fanType)) < 1117) {
                krugerSelectInfo.setPressure(pressure + 200);
            }
            List<KrugerSelection> krugerSelectionList5 = krugerEngineService.select(krugerSelectInfo);
            krugerSelectionList.addAll(krugerSelectionList5);
            krugerSelectInfo.setProductType(9);
            if (BaseDataUtil.stringConversionInteger(SystemCountUtil.getUnit(fanType)) < 1117) {
                krugerSelectInfo.setPressure(pressure + 200);
            }
        List<KrugerSelection> krugerSelectionList9 = krugerEngineService.select(krugerSelectInfo);
        krugerSelectionList.addAll(krugerSelectionList9);
    }
        krugerSelectInfo.setPressure(pressure);
        return krugerSelectionList;
    }


}

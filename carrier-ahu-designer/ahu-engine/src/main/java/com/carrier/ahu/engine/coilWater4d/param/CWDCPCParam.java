package com.carrier.ahu.engine.coilWater4d.param;

public class CWDCPCParam {

    //========================调用盘管DLL使用页面参数=================//
    private Double LBTS;//
    private String strCpc;//cpc路径
    private Double qWaterIn;//水流量
    private Integer tId;
    private Integer cirNo;
    private Integer tLen;
    private Integer tNo;
    private Integer tRow;
    private Double flowRate;

    public Double getLBTS() {
        return LBTS;
    }

    public void setLBTS(Double LBTS) {
        this.LBTS = LBTS;
    }

    public String getStrCpc() {
        return strCpc;
    }

    public void setStrCpc(String strCpc) {
        this.strCpc = strCpc;
    }

    public Double getqWaterIn() {
        return qWaterIn;
    }

    public void setqWaterIn(Double qWaterIn) {
        this.qWaterIn = qWaterIn;
    }

    public Integer gettId() {
        return tId;
    }

    public void settId(Integer tId) {
        this.tId = tId;
    }

    public Integer getCirNo() {
        return cirNo;
    }

    public void setCirNo(Integer cirNo) {
        this.cirNo = cirNo;
    }

    public Integer gettLen() {
        return tLen;
    }

    public void settLen(Integer tLen) {
        this.tLen = tLen;
    }

    public Integer gettNo() {
        return tNo;
    }

    public void settNo(Integer tNo) {
        this.tNo = tNo;
    }

    public Integer gettRow() {
        return tRow;
    }

    public void settRow(Integer tRow) {
        this.tRow = tRow;
    }

	public Double getFlowRate() {
		return flowRate;
	}

	public void setFlowRate(Double flowRate) {
		this.flowRate = flowRate;
	}
}

package com.carrier.ahu.engine.fanGk4k.factory;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.engine.fan.util.Utils;
import com.carrier.ahu.engine.fanGk4k.param.FanGKDllInParam;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllInEx3ParamFactory;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.SKFanType;
import com.carrier.ahu.metadata.entity.fan.SKFanType1;
import com.carrier.ahu.util.EmptyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by LIANGD4 on 2017/12/17.
 */
public class FanGkDllInParamFactory {

    private static final Logger logger = LoggerFactory.getLogger(FanYldDllInEx3ParamFactory.class.getName());

    //谷科DLLParam封装
    public List<FanGKDllInParam> packageParam(FanInParam fanInParam, Vector<SKFanType> conditions) {
        List<FanGKDllInParam> fanGKDllInParamList = new ArrayList<FanGKDllInParam>();
        /*风机参数加工==============================================================*/
        String fanType = fanInParam.getSerial();//机组型号
        String fanSupplier = fanInParam.getFanSupplier();//供应商
        int shape = Utils.getShape(fanInParam); //前弯或后弯
        double airVolume = fanInParam.getSairvolume();

        if (AirDirectionEnum.RETURNAIR.getCode().equals(fanInParam.getAirDirection())) {//回风
            airVolume = fanInParam.getEairvolume();//送风风量
        }


        int fanHeight = Integer.valueOf(fanType.substring(fanType.length() - 4, fanType.length() - 2));//风机高度
        //根据条件查询结果
        try {
            for (SKFanType skFanTypePo : conditions) {
                String fanName = skFanTypePo.getFanQuery();//KTQ1.6 KHF1000;
                String fanCode = fanName.substring(3, fanName.length());//风机代号 KTQ1.8 1000;
                SKFanType1 skFanType1 = null;
                if (shape != 0) {
                    skFanType1 = AhuMetadata.findOne(SKFanType1.class, fanType, fanSupplier, fanName, String.valueOf(shape)); //根据机组型号、供应商查询：机组型号、供应商、风机形式
                } else {
                    skFanType1 = AhuMetadata.findOne(SKFanType1.class, fanType, fanSupplier, fanName);
                }
                if(EmptyUtil.isEmpty(skFanType1)){
                    return null;
                }
                double maxPower = skFanType1.getMaxPower();//90.0
                double maxMachineSite = skFanType1.getMaxMachineSite();//3000.0
                if (SystemCountUtil.lteSmallUnitHeight(fanHeight)) {//fanHeight<=23
                    fanName = skFanTypePo.getFan();//KTQ1.6 KHF1000
                    fanCode = fanName.substring(2, fanName.length());//风机代号 KTQ1.8 1000
                }
                double power = skFanTypePo.getMaxPower();
                //添加YFH(K)\YFH(G)两种型号到计算
                //fanName+fanCode 风机型号，总静压，风量，海拔，293，入参
                FanGKDllInParam fanGKDllInParamK = new FanGKDllInParam();
                fanGKDllInParamK.setPst(fanInParam.getTotalStatic());//总静压
                fanGKDllInParamK.setQvt(airVolume);//风量
                fanGKDllInParamK.setNst(0.0);//转速 引擎固定值
                fanGKDllInParamK.setMotorposition("H");//电机位置 引擎固定值 C:侧置H:后置
                fanGKDllInParamK.setFanoutlet("A");//风机出风方向 引擎固定值 A: 0 B:90A C:90B D:180
                fanGKDllInParamK.setTpw(0.0);//电机功率 引擎固定值
                fanGKDllInParamK.setEtr(true);//转速,静压,全压判定
                fanGKDllInParamK.setNpt(true);//转速,静压,全压判定
                fanGKDllInParamK.setFn("YFH(K)" + fanCode);//风机型号
                fanGKDllInParamK.setPower(power);
                fanGKDllInParamK.setMaxPower(maxPower);
                fanGKDllInParamK.setMaxMachineSite(maxMachineSite);
                fanGKDllInParamK.setFanCode(fanCode);
                fanGKDllInParamK.setType(fanInParam.getType());//电机类型
                fanGKDllInParamK.setSupplier(fanInParam.getSupplier());//电机品牌
                fanGKDllInParamK.setProjectId(fanInParam.getProjectId());
                fanGKDllInParamK.setSerial(fanInParam.getSerial());
                fanGKDllInParamK.setAltitude(fanInParam.getAltitude());
                fanGKDllInParamK.setSkFanTypeVector(JSONArray.toJSONString(conditions));

                fanGKDllInParamList.add(fanGKDllInParamK);//增加YFH(K)

                FanGKDllInParam fanGKDllInParamG = new FanGKDllInParam();
                fanGKDllInParamG.setPst(fanInParam.getTotalStatic());//总静压
                fanGKDllInParamG.setQvt(airVolume);//风量
                fanGKDllInParamG.setNst(0.0);//转速 引擎固定值
                fanGKDllInParamG.setMotorposition("H");//电机位置 引擎固定值 C:侧置H:后置
                fanGKDllInParamG.setFanoutlet("A");//风机出风方向 引擎固定值 A: 0 B:90A C:90B D:180
                fanGKDllInParamG.setTpw(0.0);//电机功率 引擎固定值
                fanGKDllInParamG.setEtr(true);//转速,静压,全压判定
                fanGKDllInParamG.setNpt(true);//转速,静压,全压判定
                fanGKDllInParamG.setFn("YFH(G)" + fanCode);//风机型号
                fanGKDllInParamG.setPower(power);
                fanGKDllInParamG.setMaxPower(maxPower);
                fanGKDllInParamG.setMaxMachineSite(maxMachineSite);
                fanGKDllInParamG.setFanCode(fanCode);
                fanGKDllInParamG.setType(fanInParam.getType());//电机类型
                fanGKDllInParamG.setSupplier(fanInParam.getSupplier());//电机品牌
                fanGKDllInParamG.setProjectId(fanInParam.getProjectId());
                fanGKDllInParamG.setSerial(fanInParam.getSerial());
                fanGKDllInParamG.setSkFanTypeVector(JSONArray.toJSONString(conditions));
                fanGKDllInParamList.add(fanGKDllInParamG);//增加YFH(G)
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return fanGKDllInParamList;
    }

}

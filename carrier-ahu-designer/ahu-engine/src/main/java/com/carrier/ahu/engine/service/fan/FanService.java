package com.carrier.ahu.engine.service.fan;

import java.util.List;

import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;

/**
 * Created by liangd4 on 2017/9/8.
 */
public interface FanService {

    //根据页面参数计算风机引擎
    //windMachineInParam 入参
    //version：版本 CN-HTC-1.0 国家二字码-工厂简称-版本号
    List<FanInfo> getFanEngine(FanInParam windMachineInParam, String version) throws FanEngineException;

    //根据规则计算风机引擎
    //windMachineInParam 入参
    //fanType：yld 亿利达 gk 谷壳
    //version：版本 CN-HTC-1.0 国家二字码-工厂简称-版本号
    FanInfo getFanEngineByRule(FanInParam windMachineInParam, PublicRuleParam publicRuleParam, String version) throws FanEngineException;

}

package com.carrier.ahu.engine.fan.util;

import static com.carrier.ahu.constant.CommonConstant.JSON_FAN_TYPE_A;
import static com.carrier.ahu.constant.CommonConstant.JSON_FAN_TYPE_B;
import static com.carrier.ahu.constant.CommonConstant.JSON_FAN_TYPE_C;
import static com.carrier.ahu.constant.CommonConstant.JSON_FAN_TYPE_D;
import static com.carrier.ahu.constant.CommonConstant.JSON_FAN_TYPE_E;
import static com.carrier.ahu.vo.SystemCalculateConstants.ABB;
import static com.carrier.ahu.vo.SystemCalculateConstants.ABB_SYW;
import static com.carrier.ahu.vo.SystemCalculateConstants.SIEMENS_SYW;
import static com.carrier.ahu.vo.SystemCalculateConstants.DEFAULT;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_OPTION_OUTLET_B;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_OPTION_OUTLET_F;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_OPTION_OUTLET_FOB;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_OPTION_OUTLET_WWK;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_SUPPLIER_ABB;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_SUPPLIER_DEFAULT;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_SUPPLIER_DONGYUAN;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_SUPPLIER_SIMENS;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_TYPE_1;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_TYPE_2;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_TYPE_3;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_TYPE_4;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_TYPE_5;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_TYPE_6;
import static com.carrier.ahu.vo.SystemCalculateConstants.SIEMENS;
import static com.carrier.ahu.vo.SystemCalculateConstants.TECO;
import static com.carrier.ahu.vo.SystemCalculateConstants.TECO_SYW;
import static com.carrier.ahu.vo.SystemCalculateConstants.WOLONG_SYW;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_SUPPLIER_GE;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.enums.FanEnginesEnum;
import com.carrier.ahu.common.exception.engine.CoilEngineException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.motor.IMotorDataParser;
import com.carrier.ahu.engine.fan.motor.MortorDataParser;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.FanCodeElectricSize;
import com.carrier.ahu.metadata.entity.fan.FanCodeElectricSizeBig;
import com.carrier.ahu.metadata.entity.fan.SFanMotorBaseDimension;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.metadata.entity.fan.SKFanMotorDimension;
import com.carrier.ahu.metadata.entity.fan.SKFanType;
import com.carrier.ahu.metadata.entity.fan.SKFanType1;
import com.carrier.ahu.metadata.entity.fan.STwoSpeedMotor;
import com.carrier.ahu.metadata.entity.fan.SVMotorRange;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.MotorPositionUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.sun.jna.ptr.DoubleByReference;

/**
 * Created by liaoyw on 2017/3/28.
 */
public class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class.getName());

    public static DoubleByReference dRef(double d) {
        return new DoubleByReference(d);
    }

    public static DoubleByReference dRef() {
        return new DoubleByReference();
    }

    static String readCpc(String cpcPath) {
        try {
            List<String> lines = Files.readAllLines(new File(cpcPath).toPath());
            return lines.stream().map(s -> s.trim()).collect(Collectors.joining(EngineConstant.SYS_PUNCTUATION_POUND));
        } catch (IOException e) {
            throw new CoilEngineException("Failed to read cpc", e);
        }
    }

    public static int getShape(FanInParam fanInParam) {
        int shape = -1;// 风机形式
        if (FAN_OPTION_OUTLET_FOB.equals(fanInParam.getOutlet())) {// 前弯或后弯
            shape = 0;
        } else if (FAN_OPTION_OUTLET_WWK.equals(fanInParam.getOutlet())) {// 无蜗壳风机
            shape = 7;
        } else if (FAN_OPTION_OUTLET_B.equals(fanInParam.getOutlet())) {// 后弯
            shape = 2;
        } else if (FAN_OPTION_OUTLET_F.equals(fanInParam.getOutlet())) {// 前弯
            shape = 1;
        }
        return shape;
    }

    //封装电机类型A：三级能效电机// B：防爆电机 C：双速电机// D：变频电机 E：二级能效电机
    public static String getFanType(String type) {
        String shape = JSON_FAN_TYPE_C;// 风机形式
        if (FAN_TYPE_1.equals(type)) {//二级能效电机
            shape = JSON_FAN_TYPE_E;
        } else if (FAN_TYPE_2.equals(type)) {//三级能效电机
            shape = JSON_FAN_TYPE_A;
        } else if (FAN_TYPE_3.equals(type)) {//防爆电机
            shape = JSON_FAN_TYPE_B;
        } else if (FAN_TYPE_4.equals(type)) {//双速电机
            shape = JSON_FAN_TYPE_C;
        } else if (FAN_TYPE_5.equals(type)) {//变频电机（IC416）
            shape = JSON_FAN_TYPE_D;
        } else if (FAN_TYPE_6.equals(type)) {//变频电机(直联)
            shape = JSON_FAN_TYPE_D;
        }
        return shape;
    }

    public static String getSupplier(String supplier) {//default：默认、Dongyuan：东元、ABB：ABB、Simens：西门子
        switch (supplier) {
            case FAN_SUPPLIER_DEFAULT:
                return DEFAULT;
            case FAN_SUPPLIER_DONGYUAN:
                return TECO;
            case FAN_SUPPLIER_ABB:
                return ABB;
            case FAN_SUPPLIER_SIMENS:
                return SIEMENS;
            case FAN_SUPPLIER_GE:
                return DEFAULT;
        }
        return EngineConstant.SYS_BLANK;
    }

    public static String getSupplierSYW(String supplier) {//default：默认、Dongyuan：东元、ABB：ABB、Simens：西门子
        switch (supplier) {
            case FAN_SUPPLIER_DEFAULT:
                return WOLONG_SYW;
            case FAN_SUPPLIER_GE:
                return WOLONG_SYW;
            case FAN_SUPPLIER_DONGYUAN:
                return TECO_SYW;
            case FAN_SUPPLIER_ABB:
                return ABB_SYW;
            case FAN_SUPPLIER_SIMENS:
                return SIEMENS_SYW;
        }
        return EngineConstant.SYS_BLANK;
    }

    public static String getFrequencyRange(String supplier, int js) {
        List<SVMotorRange> svMotorRangeList = AhuMetadata.findAll(SVMotorRange.class);
        if (!EmptyUtil.isEmpty(svMotorRangeList)) {
            for (SVMotorRange svMotorRange : svMotorRangeList) {
                if (svMotorRange.getMotorSupplier().equalsIgnoreCase(supplier) && svMotorRange.getJs() == js) {
                    return svMotorRange.getHZ50();
                }
            }
        }
        return EngineConstant.SYS_BLANK;
    }

    public static STwoSpeedMotor getSTwoSpeedMotor(String fanSupplier, double gl, int js) {
        List<STwoSpeedMotor> sTwoSpeedMotorList = AhuMetadata.findList(STwoSpeedMotor.class, fanSupplier);
        Collections.sort(sTwoSpeedMotorList, Comparator.comparing(STwoSpeedMotor::getGl));
        for (STwoSpeedMotor sTwoSpeedMotor : sTwoSpeedMotorList) {
            if (sTwoSpeedMotor.getGl() >= gl && sTwoSpeedMotor.getJs() == js) {
                return sTwoSpeedMotor;
            }
        }
        return null;
    }

    public static List<FanInfo> packageFanInfoList(List<FanInfo> fanInfoList, List<FanInfo> fanInfoListRep) {
        if (!EmptyUtil.isEmpty(fanInfoList) && EmptyUtil.isEmpty(fanInfoListRep)) {
            return fanInfoList;
        } else if (EmptyUtil.isEmpty(fanInfoList) && !EmptyUtil.isEmpty(fanInfoListRep)) {
            return fanInfoListRep;
        } else {
            Map<String, FanInfo> map = new HashMap<String, FanInfo>();
            List<FanInfo> newFanInfoList = new ArrayList<FanInfo>();
            for (FanInfo fanInfo : fanInfoList) {
                for (FanInfo fanInfoRep : fanInfoListRep) {
                    String fanModel = fanInfo.getFanModel();
                    String fanModelRep = fanInfoRep.getFanModel();
                    if (!fanModel.equals(fanModelRep) && !map.containsKey(fanModelRep) && !fanModelRep.contains(fanModel)) {
                        map.put(fanModelRep, fanInfoRep);
                        newFanInfoList.add(fanInfoRep);
                    }
                }
                newFanInfoList.addAll(fanInfoList);
                return newFanInfoList;
            }
        }
        return null;
    }

    /*===================================此处添加特殊逻辑================================================*/

    //封装变频范围到引擎结果中
    public static List<FanInfo> packageFrequencyRange(FanInParam fanInParam, List<FanInfo> fanInfoList) {
        IMotorDataParser parser = new MortorDataParser();
        if (!EmptyUtil.isEmpty(fanInfoList)) {//遍历结果封装变频范围
            for (FanInfo fanInfo : fanInfoList) {
                String HZ = Utils.getFrequencyRange(Utils.getSupplier(fanInParam.getSupplier()), BaseDataUtil.stringConversionInteger(fanInfo.getPole()));
                //此处封装支持功率、变频范围、马力
                //变频、变频直连
                if (fanInParam.getType().equals(SystemCalculateConstants.FAN_TYPE_5) || fanInParam.getType().equals(SystemCalculateConstants.FAN_TYPE_6)) {
                    if (fanInParam.getSupplier().equals(SystemCalculateConstants.FAN_SUPPLIER_GE)) {
                        if (fanInfo.getPole().equals("2")) {
                            fanInfo.setFrequencyRange("5~70Hz");//变频范围
                        } else {
                            fanInfo.setFrequencyRange("5~100Hz");//变频范围
                        }
                        fanInfo.setHz("50");//变频频率
                    } else if (!fanInParam.getSupplier().equals(SystemCalculateConstants.FAN_SUPPLIER_SIMENS)) {
                        fanInfo.setFrequencyRange(HZ);//变频范围
                    }
                } else if (fanInParam.getType().equals(SystemCalculateConstants.FAN_TYPE_4)) {//双速
                    //增加双速逻辑
                    STwoSpeedMotor sTwoSpeedMotor = Utils.getSTwoSpeedMotor(fanInParam.getSupplier(), BaseDataUtil.stringConversionDouble(fanInfo.getMotorPower()), BaseDataUtil.stringConversionInteger(fanInfo.getPole()));
                    fanInfo.setMotorEff("");//双速电机 电机效率不赋值
                    if (sTwoSpeedMotor != null) {
                        fanInfo.setMotorEffPole(sTwoSpeedMotor.getKwp() + EngineConstant.SYS_PUNCTUATION_HYPHEN + sTwoSpeedMotor.getMemo());//电机效率/极数
                        fanInfo.setMotorBaseNo(sTwoSpeedMotor.getMotorBase());
                    }
                } else {//封装效率
                    if (FanEnginesEnum.KRUGER.getCode().equals(fanInParam.getFanSupplier())) {
                        fanInfo.setMotorEff(BaseDataUtil.doubleConversionString(parser.getMotorEff(fanInParam.getSupplier(), fanInParam.getType(), fanInfo.getMotorPower(), fanInfo.getPole())) + "%");
                    }
                }
            }
        }
        return fanInfoList;
    }

    public static List<FanInfo> filterMotorMachinesite(FanInParam fanInParam, List<FanInfo> fanInfoList) {
        if (null == fanInfoList || fanInfoList.size() == 0) {
            return null;
        } else {
            List<FanInfo> newFanInfoList = new ArrayList<FanInfo>();
            for (FanInfo fanInfo : fanInfoList) {
                String motor = fanInfo.getMotorPower();
                String pole = fanInfo.getPole();
                List<SKFanMotor> skFanMotorList = AhuMetadata.findAll(SKFanMotor.class);
                SKFanMotor skFanMotor = null;
                for (SKFanMotor skFanMotor1 : skFanMotorList) {
                    if (BaseDataUtil.stringConversionDouble(motor) == skFanMotor1.getPower() && BaseDataUtil.stringConversionInteger(pole) == skFanMotor1.getLevel()) {
                        skFanMotor = skFanMotor1;
                    }
                }
                if (null == skFanMotor) {
                    continue;
                }
                double machinesite = BaseDataUtil.stringConversionDouble(skFanMotor.getMachineSite());
                int shape = Utils.getShape(fanInParam);
                String fanType = fanInParam.getSerial();// 机组型号

                List<SKFanType> skFanTypePoList = null;
                if (shape != 0) {// 如果风机形式不为前弯或后弯
                    skFanTypePoList = AhuMetadata.findList(SKFanType.class, fanType, EngineConstant.SYS_ALPHABET_A_UP, null, String.valueOf(shape));// 根据风机形式、供应商查询、风机形式
                } else {
                    skFanTypePoList = AhuMetadata.findList(SKFanType.class, fanType, EngineConstant.SYS_ALPHABET_A_UP);// 根据风机形式、供应商查询、风机形式
                }
                if (null == skFanTypePoList || skFanTypePoList.size() == 0) {
                    continue;
                } else {
                    double rMotorMaxPower;
                    double rMotormaxmachinesite;
                    for (SKFanType skFanType : skFanTypePoList) {
                        String fanName = skFanType.getFanQuery();
                        SKFanType1 skFanType1 = null;
                        if (!FAN_OPTION_OUTLET_FOB.equals(fanInParam.getOutlet())) {// 如果风机形式不为前弯或后弯
                            skFanType1 = AhuMetadata.findOne(SKFanType1.class, fanInParam.getSerial(), fanInParam.getFanSupplier(), fanName, BaseDataUtil.integerConversionString(shape)); //根据机组型号、供应商查询：机组型号、供应商、风机形式
                        } else {
                            skFanType1 = AhuMetadata.findOne(SKFanType1.class, fanInParam.getSerial(), fanInParam.getFanSupplier());
                        }
                        if (null != skFanType1) {
                            rMotorMaxPower = skFanType1.getMaxPower();
                            rMotormaxmachinesite = skFanType1.getMaxMachineSite();
                            if (machinesite > rMotormaxmachinesite || BaseDataUtil.stringConversionDouble(motor) > rMotorMaxPower) {
                                continue;
                            } else {
                                newFanInfoList.add(fanInfo);
                                break;
                            }
                        }
                    }
                }
            }
            return newFanInfoList;
        }
    }

    public static List<FanInfo> packageMotorPosition(FanInParam fanInParam, List<FanInfo> fanInfoList) {
        for (FanInfo fanInfo : fanInfoList) {
            String fanType = fanInParam.getSerial();//机组型号
            String supplier = fanInParam.getSupplier();//电机品牌
            int fanHeight = SystemCountUtil.getUnitHeight(fanType);// 风机高度
            String motorBaseNo = fanInfo.getMotorBaseNo();//机座号
            String code = BaseDataUtil.StringConversionNumber(fanInfo.getFanModel());
            String startStyle = fanInParam.getStartStyle();
            String fanTypeCode = "39CQ" + SystemCountUtil.getUnit(fanType);
            String fanOutlet=fanInParam.getOutlet();
            
            if(SystemCalculateConstants.FAN_OPTION_OUTLET_WWK.equals(fanOutlet)){
            	continue;
            }

            if (SystemCountUtil.lteSmallUnitHeight(fanHeight)) {//fanHeight <= 23
                FanCodeElectricSize fanCodeElectricSize = MotorPositionUtil.getSmallFanSize(code, fanTypeCode);
                double inWidth = MotorPositionUtil.getSmallMotorSize(fanInParam.getType(), fanInfo.getFanModel(), supplier, motorBaseNo, startStyle,
						fanCodeElectricSize);
                Integer INNERWIDTH=fanCodeElectricSize.getINNERWIDTH();
                Integer P=fanCodeElectricSize.getP();
                
                if (inWidth > INNERWIDTH) {
                        fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                    } else {
                        fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                        //fanInfo.setP(BaseDataUtil.integerConversionString(0));
                    }
                    /*2.当电机为后置时增加是否偏移计算逻辑*/
                    if (null != fanInfo.getMotorPosition() && SystemCalculateConstants.FAN_MOTORPOSITION_BACK.equals(fanInfo.getMotorPosition())) {
                        if (inWidth - P <= INNERWIDTH) {
                            fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                            fanInfo.setP(BaseDataUtil.integerConversionString(P));
                        }
                    
                } else {
                    LOGGER.error("fan packageMotorPosition unit less 23 data is null " + JSON.toJSONString(fanInfo));
                }

            } else {//大机组侧后置逻辑
                /*风机型号大于等于1400以上只能后置*/
                if (BaseDataUtil.stringConversionInteger(code) >= 1400) {
                    fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                } else {
                	FanCodeElectricSizeBig fanCodeElectricSizeBig = MotorPositionUtil.getBigFanSIze(code, fanTypeCode);
                	
                	double P=fanCodeElectricSizeBig.getP();
                    double inWidth = MotorPositionUtil.getBigUnitMotorSize(fanInParam.getType(), fanInfo.getFanModel(), supplier, motorBaseNo, code, startStyle,
							fanTypeCode,fanCodeElectricSizeBig);
                    if (inWidth < 100) {
                        fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_BACK);
                    } else {
                        fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                        //fanInfo.setP(BaseDataUtil.doubleConversionString(0.0));
                    }
                    /*2.当电机为后置时增加是否偏移计算逻辑*/
                    if (null != fanInfo.getMotorPosition() && SystemCalculateConstants.FAN_MOTORPOSITION_BACK.equals(fanInParam.getMotorPosition())) {
                        if (inWidth - P < 100) {
                            fanInfo.setMotorPosition(SystemCalculateConstants.FAN_MOTORPOSITION_SIDE);
                            fanInfo.setP(BaseDataUtil.doubleConversionString(P));
                        }
                    }
                }
            }
        }
        return fanInfoList;
    }

	

    private static String calRearMotor(double motorPower, String js, String motorSupplier, String motorType, String fanType, int base, boolean vfd, FanInParam fanInParam) {
        int IVarWidth = SystemCountUtil.getUnitWidth(fanInParam.getSerial());
        SKFanMotor skFanMotor = AhuMetadata.findOne(SKFanMotor.class, BaseDataUtil.doubleConversionString(motorPower), js);
        double base1 = skFanMotor.getMotorCode();
        List<SFanMotorBaseDimension> sFanMotorBaseDimensionList = AhuMetadata.findList(SFanMotorBaseDimension.class);
        SFanMotorBaseDimension newSFanMotorBaseDimension = null;
        for (SFanMotorBaseDimension sFanMotorBaseDimension : sFanMotorBaseDimensionList) {
            if (base1 >= sFanMotorBaseDimension.getBaseStart() && base1 <= sFanMotorBaseDimension.getBaseEnd()) {
                newSFanMotorBaseDimension = sFanMotorBaseDimension;
                break;
            }
        }
        double A = 0.0;
        double Q = 0.0;
        double C = 0.0;
        double INLET = 0.0;
        double MAXMOTOR = 0.0;
        double AMOTORDIMENSION = 0.0;
        double DMOTORDIMENSION = 0.0;
        double BASEDIMENSION = 0.0;
        double VFDdimension = 0.0;
        if (EmptyUtil.isEmpty(newSFanMotorBaseDimension)) {
            return "B";
        } else {
            A = newSFanMotorBaseDimension.getA();
            Q = newSFanMotorBaseDimension.getQ();
            C = newSFanMotorBaseDimension.getC();
            INLET = newSFanMotorBaseDimension.getInlet();
            MAXMOTOR = newSFanMotorBaseDimension.getMaxMotor();
        }

        SKFanMotorDimension skFanMotorDimension = AhuMetadata.findOne(SKFanMotorDimension.class, BaseDataUtil.doubleConversionString(motorPower), js, "D");
        if (!EmptyUtil.isEmpty(skFanMotorDimension)) {
            if (FAN_SUPPLIER_ABB.equals(motorSupplier)) {
                DMOTORDIMENSION = skFanMotorDimension.getA();
            } else if (FAN_SUPPLIER_DEFAULT.equals(motorSupplier)) {
                DMOTORDIMENSION = skFanMotorDimension.getB();
            } else if (FAN_SUPPLIER_DONGYUAN.equals(motorSupplier)) {
                DMOTORDIMENSION = skFanMotorDimension.getC();
            } else if (FAN_SUPPLIER_SIMENS.equals(motorSupplier)) {
                DMOTORDIMENSION = skFanMotorDimension.getS();
            }
        }

        if (fanInParam.getSerial().contains("39CQ")) {
            IVarWidth = IVarWidth * 100 - 10;
        } else {
            IVarWidth = IVarWidth * 100;
        }

        if (DMOTORDIMENSION < MAXMOTOR - Q) {
            BASEDIMENSION = A;
        } else {
            BASEDIMENSION = A + Q + DMOTORDIMENSION - MAXMOTOR;
        }

        BASEDIMENSION = BASEDIMENSION + 50 + INLET;
        if (BaseDataUtil.stringConversionInteger(SystemCountUtil.getUnit(fanInParam.getSerial())) <= 811) {
            VFDdimension = 58.5;
        } else {
            VFDdimension = 93.5;
        }

        if (vfd) {
            VFDdimension = 0;
        }

        if (IVarWidth - BASEDIMENSION - VFDdimension < 0) {
            return "B";
        } else {
            return "R";
        }
    }


    public static void main(String[] args) {
        System.out.println(BaseDataUtil.doubleConversionInteger(0.5));
    }
}
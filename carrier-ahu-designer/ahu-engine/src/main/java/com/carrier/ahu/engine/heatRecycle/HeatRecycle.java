package com.carrier.ahu.engine.heatRecycle;

import java.util.List;

import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.heatrecycle.AirParam;
import com.carrier.ahu.util.heatrecycle.EfficientParam;
import com.carrier.ahu.util.heatrecycle.HeatParam;
import com.carrier.ahu.util.heatrecycle.HeatRecycleUtils;

/**
 * Created by Braden Zhou on 2019/01/18.
 */
public abstract class HeatRecycle {

    public abstract List<HeatRecycleInfo> getHeatRecyleInfo(HeatRecycleParam heatRecycleParam) throws Exception;

    public HeatRecycleInfo getSummerHeatRecycleInfo(HeatRecycleParam heatRecycleParam,
            EfficientParam outEfficientParam, EfficientParam exhaustEfficientParam)
            throws Exception {
        AirParam outAirParam = HeatRecycleUtils.calcOutAirParm(heatRecycleParam.getSNewDryBulbT(),
                heatRecycleParam.getSNewWetBulbT(), heatRecycleParam.getSInDryBulbT(),
                heatRecycleParam.getSInWetBulbT(), outEfficientParam.getR03());
        AirParam exhaustAirParam = HeatRecycleUtils.calcExhaustAirParm(
                heatRecycleParam.getSNewDryBulbT(), heatRecycleParam.getSNewWetBulbT(),
                heatRecycleParam.getSInDryBulbT(), heatRecycleParam.getSInWetBulbT(), exhaustEfficientParam.getR03(),
                false);
        HeatParam heatParam = HeatRecycleUtils.calcHeatParam(heatRecycleParam.getNAVolume(),
                heatRecycleParam.getSNewDryBulbT(), heatRecycleParam.getSNewRelativeT(), outAirParam.getDryBulbT(),
                outAirParam.getRelativeT());

        return getHeatRecycleInfo(outAirParam, exhaustAirParam, outEfficientParam, exhaustEfficientParam, heatParam);
    }

    public HeatRecycleInfo getWinterHeatRecycleInfo(HeatRecycleParam heatRecycleParam,
            EfficientParam outEfficientParam, EfficientParam exhaustEfficientParam)
            throws Exception {
        AirParam outAirParam = HeatRecycleUtils.calcOutAirParm(heatRecycleParam.getWNewDryBulbT(),
                heatRecycleParam.getWNewWetBulbT(), heatRecycleParam.getWInDryBulbT(),
                heatRecycleParam.getWInWetBulbT(), outEfficientParam.getR03());
        AirParam exhaustAirParam = HeatRecycleUtils.calcExhaustAirParm(
                heatRecycleParam.getWNewDryBulbT(), heatRecycleParam.getWNewWetBulbT(),
                heatRecycleParam.getWInDryBulbT(), heatRecycleParam.getWInWetBulbT(), exhaustEfficientParam.getR03(),
                true);
        HeatParam heatParam = HeatRecycleUtils.calcHeatParam(heatRecycleParam.getNAVolume(),
                heatRecycleParam.getWNewDryBulbT(), heatRecycleParam.getWNewRelativeT(), outAirParam.getDryBulbT(),
                outAirParam.getRelativeT());

        return getHeatRecycleInfo(outAirParam, exhaustAirParam, outEfficientParam, exhaustEfficientParam, heatParam);
    }

    public HeatRecycleInfo getHeatRecycleInfo(AirParam outAirParam,
            AirParam exhaustAirParam, EfficientParam outEfficientParam,
            EfficientParam exhaustEfficientParam, HeatParam heatParam)
            throws TempCalErrorException {
        HeatRecycleInfo heatRecycleInfo = new HeatRecycleInfo();
        heatRecycleInfo.setSectionSideL(BaseDataUtil
                .integerConversionString(BaseDataUtil.decimalConvert(outEfficientParam.getN01(), 0).intValue()));
        heatRecycleInfo.setHeatExchangerL(BaseDataUtil
                .integerConversionString(BaseDataUtil.decimalConvert(outEfficientParam.getN02(), 0).intValue()));
        heatRecycleInfo.setReturnWheelSize(heatRecycleInfo.getSectionSideL() + UtilityConstant.SYS_PUNCTUATION_STAR
                + heatRecycleInfo.getHeatExchangerL());
        heatRecycleInfo.setReturnSDryBulbT(BaseDataUtil.doubleConversionString(outAirParam.getDryBulbT()));
        heatRecycleInfo.setReturnSWetBulbT(BaseDataUtil.doubleConversionString(outAirParam.getWetBulbT()));
        heatRecycleInfo.setReturnSRelativeT(BaseDataUtil.doubleConversionString(outAirParam.getRelativeT()));
        heatRecycleInfo.setReturnSAirResistance(BaseDataUtil.doubleConversionString(outEfficientParam.getR01()));
        heatRecycleInfo.setReturnSEfficiency(conbineEffec(BaseDataUtil.doubleConversionString(outEfficientParam.getR03()),BaseDataUtil.doubleConversionString((outEfficientParam.getR05())),BaseDataUtil.doubleConversionString((outEfficientParam.getR06()))));
        heatRecycleInfo.setReturnEDryBulbT(BaseDataUtil.doubleConversionString(exhaustAirParam.getDryBulbT()));
        heatRecycleInfo.setReturnEWetBulbT(BaseDataUtil.doubleConversionString(exhaustAirParam.getWetBulbT()));
        heatRecycleInfo.setReturnERelativeT(BaseDataUtil.doubleConversionString(exhaustAirParam.getRelativeT()));
        heatRecycleInfo.setReturnEAirResistance(BaseDataUtil.doubleConversionString(exhaustEfficientParam.getR01()));
        heatRecycleInfo.setReturnEEfficiency(conbineEffec(BaseDataUtil.doubleConversionString(outEfficientParam.getR03()),BaseDataUtil.doubleConversionString((outEfficientParam.getR05())),BaseDataUtil.doubleConversionString((outEfficientParam.getR06()))));
        heatRecycleInfo.setReturnTotalHeat(BaseDataUtil.doubleConversionString(heatParam.getQt()));
        heatRecycleInfo.setReturnSensibleHeat(BaseDataUtil.doubleConversionString(heatParam.getQs()));
        return heatRecycleInfo;
    }
    
	public static String conbineEffec(String totalEff, String tempEff, String humiEff) {

		return (BaseDataUtil
				.constraintString(BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(totalEff), 1)))
						.concat("/")
						.concat(BaseDataUtil.constraintString(
								BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(tempEff), 1)))
						.concat("/").concat(BaseDataUtil.constraintString(
								BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(humiEff), 1)));

	}
	
	public static String conbineEffecStr(String totalEff, String tempEff, String humiEff) {

		return (totalEff.concat("/")
						.concat(tempEff)
						.concat("/").concat(humiEff));

	}


}

package com.carrier.ahu.engine.heatRecycle;

import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import lombok.Data;

/**
 * Created by LIANGD4 on 2017/11/28.
 * 热回收段
 */
@Data
public class HeatRecycleInfo {

    private String SectionSideL;//热交换器模型
    private String HeatExchangerL;//热交换器长度
    private String ExchangeModel;
    
    //add by gaok2 板式热回收有部分型号为进口型号，需要标注在界面上，增加是否为进口字段
    private String isImport;

    /*夏季引擎结果*/
    private String ReturnWheelSize;//夏季转轮尺寸
    private String ReturnSeason;//夏季季节
    private String ReturnSDryBulbT;//夏季干球温度送风侧
    private String ReturnSWetBulbT;//夏季湿球温度送风侧
    private String ReturnSRelativeT;//夏季相对湿度送风侧
    private String ReturnSAirResistance;//夏季空气阻力送风侧
    private String ReturnSEfficiency;//夏季效率送风侧
    private String ReturnEDryBulbT;//夏季干球温度进风侧
    private String ReturnEWetBulbT;//夏季湿球温度进风侧
    private String ReturnERelativeT;//夏季相对湿度进风侧
    private String ReturnEAirResistance;//夏季空气阻力进风侧
    private String ReturnEEfficiency;//夏季效率进风侧
    private String ReturnTotalHeat;//夏季全热
    private String ReturnSensibleHeat;//夏季显热
    private String LargeSizeMessage = "";//尺寸过大给出提醒
    private String MaterialPrice;
    private String CorrugatedChannel;
    

//    public String getReturnSEfficiency() {
//        if(!EmptyUtil.isEmpty(ReturnSEfficiency)){
//            return BaseDataUtil.doubleConversionString(BaseDataUtil.doubleConvertABS(BaseDataUtil.stringConversionDouble(ReturnSEfficiency)));
//        }
//        return ReturnSEfficiency;
//    }
//
//    public String getReturnEEfficiency() {
//        if(!EmptyUtil.isEmpty(ReturnEEfficiency)){
//            return BaseDataUtil.doubleConversionString(BaseDataUtil.doubleConvertABS(BaseDataUtil.stringConversionDouble(ReturnEEfficiency)));
//        }
//        return ReturnEEfficiency;
//    }

    /*冬季引擎结果*//*
    private String WReturnWheelSize;//冬季转轮尺寸
    private String WReturnSeason;//冬季季节
    private String WReturnSDryBulbT;//冬季干球温度送风侧
    private String WReturnSWetBulbT;//冬季湿球温度送风侧
    private String WReturnSRelativeT;//冬季相对湿度送风侧
    private String WReturnSAirResistance;//冬季空气阻力送风侧
    private String WReturnSEfficiency;//冬季效率送风侧
    private String WReturnEDryBulbT;//冬季干球温度进风侧
    private String WReturnEWetBulbT;//冬季湿球温度进风侧
    private String WReturnERelativeT;//冬季相对湿度进风侧
    private String WReturnEAirResistance;//冬季空气阻力进风侧
    private String WReturnEEfficiency;//冬季效率进风侧
    private String WReturnTotalHeat;//冬季全热
    private String WReturnSensibleHeat;//冬季显热*/

}

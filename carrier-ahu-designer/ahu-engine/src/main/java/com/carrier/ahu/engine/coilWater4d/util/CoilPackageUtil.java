package com.carrier.ahu.engine.coilWater4d.util;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.UserConfigParam;
import com.carrier.ahu.common.enums.CalcTypeEnum;
import com.carrier.ahu.common.enums.RoleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UserConfigEnum;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.calculation.CalculationException;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.vo.SystemCalculateConstants;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.carrier.ahu.vo.SystemCalculateConstants.SWEATCHECK_2;

public class CoilPackageUtil {

    public static boolean coilInfoParamValidation(CoilInParam coilInParam) {
        String season = coilInParam.getSeason();//季节
        double enteringFluidTemperature = coilInParam.getEnteringFluidTemperature();//进水温度
        String calculationConditions = coilInParam.getCalculationConditions();//计算条件
        double inDryBulbT = coilInParam.getInDryBulbT();//进风干球温度
        double WTAScend = coilInParam.getWTAScend();//水温升
        if (SystemCalculateConstants.seasonS.equals(season)) {//夏季
            if (SystemCalculateConstants.COOLINGCOIL_SCALCULATIONCONDITIONS_1.equals(calculationConditions)) {
                if (inDryBulbT <= (enteringFluidTemperature + WTAScend)) {
                    throw new CalculationException(ErrorCode.COIL_CALCULATION_PARAM_WRONG);
                }
            }
        } else if (SystemCalculateConstants.seasonW.equals(season)) {//冬季
            if (SystemCalculateConstants.COOLINGCOIL_WCALCULATIONCONDITIONS_1.equals(calculationConditions)) {
                if (inDryBulbT >= (enteringFluidTemperature - WTAScend)) {
                    throw new CalculationException(ErrorCode.COIL_CALCULATION_PARAM_WRONG);
                }
            }
        }
        return true;
    }

    /*盘管用户自定义过滤*/
    public static List<CoilInfo> packageUserConfig(List<CoilInfo> coilInfoList, CoilInParam coilInParam) {
        if (EmptyUtil.isEmpty(coilInfoList)) {
            return null;
        }
        Map<String, Object> mapConfig = coilInParam.getMapConfig();
        if (!EmptyUtil.isEmpty(mapConfig)) {
            List<CoilInfo> coilInfos = new ArrayList<CoilInfo>();
            String maximumCoilFaceVelocityKey = UserConfigEnum.MAXIMUM_COIL_FACE_VELOCITY.getCode();
            for (CoilInfo coilInfo : coilInfoList) {
                if (mapConfig.containsKey(maximumCoilFaceVelocityKey)) {
                    String value = mapConfig.get(maximumCoilFaceVelocityKey).toString();
                    if (!EmptyUtil.isEmpty(value)) {
                        if (BaseDataUtil.stringConversionDouble(coilInfo.getVelocity()) > BaseDataUtil.stringConversionDouble(value)) {
                            continue;
                        }
                    }
                }
                coilInfos.add(coilInfo);
            }
            return coilInfos;
        } else {
            return coilInfoList;
        }
    }

    /*封装凝露/不凝露*/
    public static List<CoilInfo> packageCondensation(List<CoilInfo> coilInfoList, CoilInParam coilInParam) {
        if (EmptyUtil.isEmpty(coilInfoList)) {
            return null;
        }
        String serial = coilInParam.getSerial();//机组型号
        double ambientDryBulbT = coilInParam.getAmbientDryBulbT();//环境干球温度
        double ambientWetBulbT = coilInParam.getAmbientWetBulbT();//环境湿球温度
        //计算环境露点温度
        AirConditionBean ambientAirConditionBean = AirConditionUtils.FAirParmCalculate1(ambientDryBulbT, ambientWetBulbT);
        double dewPointTemperature = ambientAirConditionBean.getParamTd();//露点温度
        double kb = 0.66;//默认为39CQ系列
        if (serial.contains(SystemCalculateConstants.AHU_PRODUCT_39XT)) {
            kb = 0.77;
        } else if (serial.contains(SystemCalculateConstants.AHU_PRODUCT_39G)) {
            kb = 0.61;
        }
        for (CoilInfo coilInfo : coilInfoList) {
            double outDryBulbT = BaseDataUtil.stringConversionDouble(coilInfo.getOutDryBulbT());//出风干球温度
            double tAir = outDryBulbT - ambientDryBulbT;//环境干球温度
            double tMin = tAir * kb;// 冷桥因子公式 Kb=△tmin/△tair
            double tsmax = outDryBulbT - tMin;//△tmin=ti-tsmax
            //根据 出风干球温度 露点温度 计算是否凝露
            if (dewPointTemperature >= tsmax) {
                coilInfo.setSweatCheck(SWEATCHECK_2);//凝露
            }
        }
        return coilInfoList;
    }

    /*盘管条数过滤*/
    public static List<CoilInfo> packageCalculateResults(List<CoilInfo> coilInfoList, CoilInParam coilInParam) {
        if (coilInParam.isCalculateOptimalResults()) {
            if (!EmptyUtil.isEmpty(coilInfoList)) {
                List<CoilInfo> coilInfoNewList = new ArrayList<CoilInfo>();
                ListUtils.sort(coilInfoList, true, EngineConstant.SYS_MAP_ROWS, EngineConstant.SYS_MAP_FINDENSITY, EngineConstant.SYS_MAP_CIRCUIT);
                for (CoilInfo coilInfo : coilInfoList) {
                    if (coilInfoNewList.size() >= 5) {
                        return coilInfoNewList;
                    }
                    coilInfoNewList.add(coilInfo);
                }
                return coilInfoNewList;
            }
        }
        return coilInfoList;
    }

    /*盘管水阻过滤、限定条件过滤*/
    public static List<CoilInfo> packageNewCoilInfoList(CoilInParam coilInParam, List<CoilInfo> coilInfoList) {
        if (EmptyUtil.isEmpty(coilInfoList)) {
            return null;
        }
        List<CoilInfo> newCoilInfoList = new ArrayList<CoilInfo>(); //重新封装返回的条数
        String qualifiedConditions = coilInParam.getQualifiedConditions();//限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        double maxWPD = coilInParam.getMaxWPD();//最大水阻
        String tubeDiameter = coilInParam.getTubeDiameter();
        for (CoilInfo coilInfo : coilInfoList) {
            if (maxWPD < BaseDataUtil.stringConversionDouble(coilInfo.getWaterResistance())) {
                continue;
            }
            if (SystemCalculateConstants.waterCoil.equals(coilInParam.getCoilType())) {
                double waterFlow = BaseDataUtil.stringConversionDouble(coilInfo.getFluidvelocity());
                if (waterFlow < 0.3 || waterFlow > 2.64) {
                    String role = AHUContext.getUserRole();
                    if ((RoleEnum.Sales.name().equalsIgnoreCase(role) || RoleEnum.Design.name().equalsIgnoreCase(role))) {
                        continue;
                    }
                }
                if (SystemCalculateConstants.seasonS.equals(coilInParam.getSeason())) {//夏季
                    if (SystemCalculateConstants.COOLINGCOIL_SQUALIFIEDCONDITIONS_4.equals(qualifiedConditions)) {//默认条件 无
                        newCoilInfoList.add(coilInfo);
                    } else if (SystemCalculateConstants.COOLINGCOIL_SQUALIFIEDCONDITIONS_1.equals(qualifiedConditions)) {//根据冷量
                        if (coilInParam.getScoldQ() < coilInfo.getReturnColdQ()) {
                            newCoilInfoList.add(coilInfo);
                        }
                    } else if (SystemCalculateConstants.COOLINGCOIL_SQUALIFIEDCONDITIONS_2.equals(qualifiedConditions)) { //根据显冷
                        if (coilInParam.getSsensibleCapacity() < BaseDataUtil.stringConversionDouble(coilInfo.getReturnSensibleCapacity())) {
                            newCoilInfoList.add(coilInfo);
                        }
                    } else if (SystemCalculateConstants.COOLINGCOIL_SQUALIFIEDCONDITIONS_3.equals(qualifiedConditions)) {//出风温度
                        if (coilInParam.getSleavingAirDB() > BaseDataUtil.stringConversionDouble(coilInfo.getOutDryBulbT())) {
                            newCoilInfoList.add(coilInfo);
                        }
                    } else if (SystemCalculateConstants.COOLINGCOIL_SQUALIFIEDCONDITIONS_5.equals(qualifiedConditions)) {
                        //三通阀待定 TODO
                    }
                } else {//冬季
                    //分管的冷水盘管在制热工况时，软件风阻为131pa，测试风阻为157pa，不能通过测试。故需要将4分冷水盘管的制热工况下，风阻值额外上调15%
                    if ("T4".equals(tubeDiameter)) {
                    	Double res=Double.parseDouble(coilInfo.getAirResistance()) * 1.15;
                        BigDecimal   b   =   new   BigDecimal(res);  
                        double   rs   =   b.setScale(2,   BigDecimal.ROUND_HALF_UP).doubleValue(); 
                        coilInfo.setAirResistance(String.valueOf(rs));
                    }
                    if (SystemCalculateConstants.COOLINGCOIL_WQUALIFIEDCONDITIONS_3.equals(qualifiedConditions)) {//默认条件 无
                        newCoilInfoList.add(coilInfo);
                    } else if (SystemCalculateConstants.COOLINGCOIL_WQUALIFIEDCONDITIONS_1.equals(qualifiedConditions)) {//根据热量
                        if (coilInParam.getWheatQ() < coilInfo.getReturnHeatQ()) {
                            newCoilInfoList.add(coilInfo);
                        }
                    } else if (SystemCalculateConstants.COOLINGCOIL_WQUALIFIEDCONDITIONS_2.equals(qualifiedConditions)) {//出风温度
                        if (coilInParam.getWleavingAirDB() < BaseDataUtil.stringConversionDouble(coilInfo.getOutDryBulbT())) {
                            newCoilInfoList.add(coilInfo);
                        }
                    } else if (SystemCalculateConstants.COOLINGCOIL_WQUALIFIEDCONDITIONS_4.equals(qualifiedConditions)) {
                        //三通阀待定 TODO
                    }
                }
            } else if (SystemCalculateConstants.waterCoilH.equals(coilInParam.getCoilType())) {//热水盘管
                double waterFlow = BaseDataUtil.stringConversionDouble(coilInfo.getFluidvelocity());
                String role = AHUContext.getUserRole();
                if (waterFlow < 0.1 || waterFlow > 2.64) {
                    if ((RoleEnum.Sales.name().equalsIgnoreCase(role) || RoleEnum.Design.name().equalsIgnoreCase(role))) {
                        continue;
                    }
                }
                if (SystemCalculateConstants.seasonW.equals(coilInParam.getSeason())) {//冬季
                    if (SystemCalculateConstants.HEATINGCOIL_WQUALIFIEDCONDITIONS_3.equals(qualifiedConditions)) {//默认条件 无
                        newCoilInfoList.add(coilInfo);
                    } else if (SystemCalculateConstants.HEATINGCOIL_WQUALIFIEDCONDITIONS_1.equals(qualifiedConditions)) {//热量
                        if (coilInParam.getWheatQ() < coilInfo.getReturnHeatQ()) {
                            newCoilInfoList.add(coilInfo);
                        }
                    } else if (SystemCalculateConstants.HEATINGCOIL_WQUALIFIEDCONDITIONS_2.equals(qualifiedConditions)) {//出风温度
                        if (coilInParam.getWleavingAirDB() < BaseDataUtil.stringConversionDouble(coilInfo.getOutDryBulbT())) {
                            newCoilInfoList.add(coilInfo);
                        }
                    } else if (SystemCalculateConstants.HEATINGCOIL_WQUALIFIEDCONDITIONS_4.equals(qualifiedConditions)) {
                        //三通阀待定 TODO
                    }
                }
            } else {
                newCoilInfoList.add(coilInfo);
            }
        }
        return newCoilInfoList;
    }

    /*配置计算条件*/
    public static CoilInParam packageCoilInParam(CoilInParam coilInParam, PublicRuleParam publicRuleParam, String coilType) {
        try {
            coilInParam.setCoilType(coilType);
            if (coilInParam.isCalculateAllResults()) {
                coilInParam.setCalculateOptimalResults(false);
            } else {
                coilInParam.setCalculateOptimalResults(true);
            }
            if (!EmptyUtil.isEmpty(publicRuleParam)) {
                coilInParam.setCalculateOptimalResults(true);
                coilInParam.setCalculateAllResults(false);
                UserConfigParam userConfigParam = publicRuleParam.getUserConfigParam();
                if (!EmptyUtil.isEmpty(userConfigParam)) {
                    Map<String, Object> mapConfig = userConfigParam.getMapConfig();
                    if (!EmptyUtil.isEmpty(mapConfig)) {
                        coilInParam.setMapConfig(mapConfig);
                    }
                    String rows = userConfigParam.getRow();
                    if (!EmptyUtil.isEmpty(rows) && !"-1".equals(rows)) {
                        coilInParam.setRows(rows);
                    }
                }
            }
            AirConditionBean airConditionBean = AirConditionUtils.FAirParmCalculate1(coilInParam.getInDryBulbT(), coilInParam.getInWetBulbT());
            coilInParam.setInRelativeT(airConditionBean.getParmF());
        } catch (Exception e) {
            return null;
        }
        return coilInParam;
    }

    /*规则配置*/
    public static List<CoilInfo> getWaterCoilByRule(List<CoilInfo> coilInfoList, PublicRuleParam publicRuleParam, CoilInParam coilInParam) {
        if (EmptyUtil.isEmpty(coilInfoList)) {
            return null;
        }
        if (EmptyUtil.isEmpty(publicRuleParam)) {
            return coilInfoList;
        }
        String qualifiedConditions = coilInParam.getQualifiedConditions();
        double sColdQ = coilInParam.getScoldQ();
        double wHeatQ = coilInParam.getWheatQ();
        String season = coilInParam.getSeason();
        double coefficient = 0.3;

        if (!EmptyUtil.isEmpty(coilInfoList) && !EmptyUtil.isEmpty(publicRuleParam)) {
            if (SectionTypeEnum.TYPE_HEATINGCOIL.getCode().equals(coilInParam.getCoilType())) {
                ListUtils.sort(coilInfoList, true, EngineConstant.SYS_MAP_ROWS, EngineConstant.SYS_MAP_RETURNHEATQ);
            } else {
                ListUtils.sort(coilInfoList, true, EngineConstant.SYS_MAP_ROWS, EngineConstant.SYS_MAP_RETURNCOLDQ);
            }
            List<CoilInfo> newCoilInfoList = new ArrayList<CoilInfo>();
            if (publicRuleParam.getKey().equals(CalcTypeEnum.OPTIMUM.getId())) {
                if (SystemCalculateConstants.COOLINGCOIL_SQUALIFIEDCONDITIONS_1.equals(qualifiedConditions)) {
                    double capacity = 0.00;
                    if (SystemCalculateConstants.seasonS.equals(season)) {
                        capacity = sColdQ;
                    } else {
                        capacity = wHeatQ;
                    }
                    for (CoilInfo coilInfo : coilInfoList) {
                        if (SystemCalculateConstants.seasonS.equals(season)) {
                            if ((coilInfo.getReturnColdQ() - capacity) / 100 >= coefficient) {
                                newCoilInfoList.add(coilInfo);
                                return newCoilInfoList;
                            }
                        } else {
                            if ((coilInfo.getReturnHeatQ() - capacity) / 100 >= coefficient) {
                                newCoilInfoList.add(coilInfo);
                                return newCoilInfoList;
                            }
                        }
                    }
                }
                newCoilInfoList.add(coilInfoList.get(coilInfoList.size() - 1));
                return newCoilInfoList;
            } else if (publicRuleParam.getKey().equals(CalcTypeEnum.PERFORMANCE_KEEP.getId())) {
                if (SystemCalculateConstants.COOLINGCOIL_ROWS_AUTO.equals(coilInParam.getRows())) {
                    return null;
                }
                newCoilInfoList.add(coilInfoList.get(0));
                return newCoilInfoList;
            } else {
                newCoilInfoList.add(coilInfoList.get(0));
                return newCoilInfoList;
            }
        }
        return null;
    }

    /*配置冷水盘管系数*/
    public static double packageSurfEff(CoilInParam coilInParam, String rows) {
        String serial = coilInParam.getSerial();//机组型号
        String coilType = coilInParam.getCoilType();//盘管类型
        String season = coilInParam.getSeason();//季节
        double tempretureDBIn = coilInParam.getInDryBulbT();
        double tempretureWBIn = coilInParam.getInWetBulbT();

        if (SystemCalculateConstants.waterCoil.equals(coilType)) {//冷水盘管
            boolean isFreshAir = isFreshAir(tempretureDBIn, tempretureWBIn, season);
            if (SystemCalculateConstants.seasonS.equals(season)) {
                int unitHeight = SystemCountUtil.getUnitHeight(serial);

                if (isFreshAir) {

                    if (SystemCalculateConstants.COOLINGCOIL_ROWS_3.equals(rows)) {
                        return 0.95;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_4.equals(rows)) {
                        return 0.935;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_5.equals(rows)) {
                        return 0.927;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_6.equals(rows)) {
                        return 0.9;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_7.equals(rows)) {
                        return 0.91;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_8.equals(rows)) {
                        return 0.893;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_10.equals(rows)) {
                        return 0.855;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_12.equals(rows)) {
                        return 0.813;
                    }

                }

                if (unitHeight == 6) {
                    return 0.995;
                }

                if (SystemCalculateConstants.COOLINGCOIL_ROWS_3.equals(rows)) {
                    return 0.978;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_4.equals(rows)) {
                    return 0.981;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_5.equals(rows)) {
                    return 0.977;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_6.equals(rows)) {
                    return 0.977;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_7.equals(rows)) {
                    return 0.9742;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_8.equals(rows)) {
                    return 0.962;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_10.equals(rows)) {
                    return 0.945;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_12.equals(rows)) {
                    return 0.93;
                }
                
                
                /*if (unitHeight == 7) {
                    return 0.965;
                } else if (unitHeight < 11) {
                    return 0.967;
                } else if (unitHeight == 11) {
                    return 0.965;
                } else if (unitHeight == 18) {
                    return 0.973;
                } else if (unitHeight >= 13 && unitHeight < 18) {
                    return 0.975;
                } else if (unitHeight > 18 && unitHeight <= 23) {
                    return 0.96;
                } else if (unitHeight > 23) {
                    return 0.95;
                }*/
            } else {
                if (isFreshAir) {
                    if (SystemCalculateConstants.COOLINGCOIL_ROWS_1.equals(rows)) {
                        return 1.028;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_2.equals(rows)) {
                        return 1.035;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_3.equals(rows)) {
                        return 1.048;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_4.equals(rows)) {
                        return 1.075;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_5.equals(rows)) {
                        return 1.09;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_6.equals(rows)) {
                        return 1.19;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_7.equals(rows)) {
                        return 1.3;
                    } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_8.equals(rows)) {
                        return 1.5;
                    }

                }
                return 1.005;
            }
        } else if (SystemCalculateConstants.waterCoilH.equals(coilType)) {//热水盘管
            boolean isFreshAir = isFreshAir(tempretureDBIn, tempretureWBIn, SystemCalculateConstants.seasonW);
            if (isFreshAir) {
                if (SystemCalculateConstants.COOLINGCOIL_ROWS_1.equals(rows)) {
                    return 1.028;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_2.equals(rows)) {
                    return 1.035;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_3.equals(rows)) {
                    return 1.048;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_4.equals(rows)) {
                    return 1.075;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_5.equals(rows)) {
                    return 1.09;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_6.equals(rows)) {
                    return 1.19;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_7.equals(rows)) {
                    return 1.3;
                } else if (SystemCalculateConstants.COOLINGCOIL_ROWS_8.equals(rows)) {
                    return 1.5;
                }

            }
            return 1.005;
        }
        return 1.0;
    }

//        surfEff = surfEff * 0.97;//23以上 确定
//        surfEff = surfEff * 0.98;//18-23

//        surfEff = surfEff * 0.975;//18已下
//        surfEff = surfEff * 0.975;//13-18 确定
//        surfEff = surfEff * 0.97;//11 确定
//        surfEff = surfEff * 0.967;//00 确定
//        surfEff = surfEff * 0.99;//热

    private static boolean isFreshAir(double dbIn, double WbIn, String season) {

        if (SystemCalculateConstants.seasonS.equals(season)) {
            if (SystemCalculateConstants.SUMMER_FRESHAIR_DB_IN <= dbIn) {
                return true;
            }
        }

        if (SystemCalculateConstants.seasonW.equals(season)) {
            if (SystemCalculateConstants.WINTER_FRESHAIR_DB_IN >= dbIn) {
                return true;
            }
        }
        return false;

    }

}

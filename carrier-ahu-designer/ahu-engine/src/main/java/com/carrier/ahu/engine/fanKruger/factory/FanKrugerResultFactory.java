package com.carrier.ahu.engine.fanKruger.factory;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.enums.FanEnginesEnum;
import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.fan.KrugerChartInfo;
import com.carrier.ahu.engine.fan.KrugerChartResult;
import com.carrier.ahu.engine.fan.KrugerSelection;
import com.carrier.ahu.engine.fan.KrugerSoundSpectrumEx;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.util.CountUtil;
import com.carrier.ahu.engine.fan.util.Utils;
import com.carrier.ahu.engine.fanKruger.krugerI.KrugerEngineService;
import com.carrier.ahu.engine.fanKruger.krugerI.KrugerEngineServiceImpl;
import com.carrier.ahu.engine.fanYld4k.param.FanCountParam;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.metadata.entity.fan.SKFanType;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.CCRDFileUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static com.carrier.ahu.constant.CommonConstant.SYS_NAME_KRUGER_CHART;

public class FanKrugerResultFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(FanKrugerDllCalFactory.class.getName());
    private static final KrugerEngineService krugerEngineService = new KrugerEngineServiceImpl();

    public List<FanInfo> packageResult(List<KrugerSelection> krugerSelectionList, FanInParam fanInParam) {
        List<FanInfo> retFans = new ArrayList<FanInfo>();
        String outlet = SystemCalculateConstants.FAN_OPTION_OUTLET_F;
        for (int i = 0; i < krugerSelectionList.size(); i++) {
            KrugerSelection vfan = krugerSelectionList.get(i);
            String frame = "";
            if (vfan.getFanStyle() == 1) {
                frame = "CM";
            } else if (vfan.getFanStyle() == 2) {
                frame = "TM";
            } else if (vfan.getFanStyle() == 3) {
                frame = "XM";
            }


            List<SKFanType> skFanTypePoList = null;
            int shape = Utils.getShape(fanInParam); //前弯或后弯
            // 根据条件查询结果
            if (shape == 7) {
                continue;
            } else {
                String fanType = fanInParam.getSerial();
                if (shape != 0) {// 如果风机形式不为前弯或后弯
                    skFanTypePoList = AhuMetadata.findList(SKFanType.class, fanType, EngineConstant.SYS_ALPHABET_B_UP);// 根据风机形式、供应商查询、风机形式
                } else {
                    skFanTypePoList = AhuMetadata.findList(SKFanType.class, fanType, EngineConstant.SYS_ALPHABET_B_UP);// 根据风机形式、供应商查询、风机形式
                }
                if (!EmptyUtil.isEmpty(skFanTypePoList)) {
                    CountUtil countUtil = new CountUtil();
                    for (SKFanType skFanTypePo : skFanTypePoList) {

                        Integer js = countUtil.packageCALJSEX3(Double.valueOf(vfan.getFanSpeed()));//电机级数返回一个 2、4、6、8的值
                        int maxmachinesite = 0;
                        String strTyper = "";
                        if (vfan.getFanMotorRating() >= 0.75) {

                            /*if (CBFS_MotorType.value='C')  and (CBFS_MotorType.Value<>'D') and (strLeve<>'1')  then
                            findtwospeedmotor(strleve,fanmotorrating,CBFDianJi.value,motorstr,strTyper)
                            else*/
                            List<SKFanMotor> skFanMotorList = AhuMetadata.findList(SKFanMotor.class, String.valueOf(vfan.getFanMotorRating()), String.valueOf(js));
                            for (SKFanMotor skFanMotor : skFanMotorList) {
                                strTyper = skFanMotor.getMachineSiteNo();
                                maxmachinesite = Integer.parseInt(skFanMotor.getMachineSite());
                            }
                        }

                        String fanName = skFanTypePo.getFanQuery();//KTQ1.6 KHF1000;
                        FanInParam windMachineInParam = new FanInParam();
                        windMachineInParam.setSerial(fanType);
                        windMachineInParam.setFanSupplier(skFanTypePo.getSupplier());
                        windMachineInParam.setOutlet(String.valueOf(shape));
                        //calRearMotorMaxPower(fanType,lqry.fieldbyname('fan_query').asstring,RMotorMaxpower,RMotormaxmachinesite); //added   20151216
                        FanCountParam outParam = countUtil.calRearMotorMaxPower(windMachineInParam, fanName);

                        FanInfo fan = new FanInfo();
                        fan.setEngineType(FanEnginesEnum.KRUGER.getCode());
                        if (1 == skFanTypePo.getBend()) {
                            if (vfan.getFanDescription().split("/")[0].equals(skFanTypePo.getFan())
                                /*and (((strtofloat(E_AirQ.text)/volume[0])>0.3)
                                    and ((strtofloat(E_AirQ.text)/volume[0])<0.8)) */
                                    && outParam.getrMotorMaxMachineSite() >= maxmachinesite
                                    && outParam.getrMotorMaxPower() >= vfan.getFanMotorRating()
                                    ) {
                                packageCurve(vfan, fanInParam);
                                fan.setFanModel(vfan.getFanDescription().split("/")[0] + frame);
                                fan.setOutletVelocity("" + BaseDataUtil.decimalConvert(vfan.getAirVelocity(), 1));
                                fan.setEfficiency("" + BaseDataUtil.decimalConvert(vfan.getTotalEff(), 1));
                                fan.setAbsorbedPower("" + BaseDataUtil.decimalConvert(vfan.getPwrCondition(), 2));
                                fan.setRPM("" + BaseDataUtil.decimalConvert(vfan.getFanSpeed(), 1));
                                fan.setNoise("" + BaseDataUtil.decimalConvert(vfan.getInlet_LwLin_Overall(), 1));
                                fan.setMotorBaseNo(strTyper);
                                fan.setMotorPower("" + vfan.getFanMotorRating());
                                fan.setPole("" + js);
                                fan.setCurve(vfan.getChartFilePath());
                                fan.setMaxAbsorbedPower("" + vfan.getMaxPower());//最大吸收功率 KW
                                fan.setMaxRPM("" + vfan.getMaxSpeed());//最大转速
                                fan.setTotalPressure("" + vfan.getTotalPressure());//全压
                                if (fan.getFanModel().contains("FDA")) {
                                    outlet = SystemCalculateConstants.FAN_OPTION_OUTLET_F;
                                }
                                if (fan.getFanModel().contains("BDB")) {
                                    outlet = SystemCalculateConstants.FAN_OPTION_OUTLET_B;
                                }
                                if (fan.getFanModel().contains("AD")) {
                                    outlet = SystemCalculateConstants.FAN_OPTION_OUTLET_AIRFOIL;
                                }
                                fan.setOutlet(outlet);
                                fan.setMaxAbsorbedPower("" + vfan.getMaxPower());//最大吸收功率 KW
                                fan.setMaxRPM("" + vfan.getMaxSpeed());//最大转速
                                fan.setTotalPressure("" + vfan.getTotalPressure());//全压
                                fan.setAPower(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(vfan.getOutlet_LPA_Overall() + 7.0, 0)));
                                fan.setASoundPressure(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(vfan.getOutlet_LPA_Overall(), 1)));
                                try {
                                    fanInfoSetting(fan, vfan);//设置噪音
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                retFans.add(fan);
                            }
                        }
                        if (2 == skFanTypePo.getBend()) {
                            if (vfan.getFanDescription().split("/")[0].equals(skFanTypePo.getFan())
                                /*and (((strtofloat(E_AirQ.text)/volume[0])>0.4)
                                    and ((strtofloat(E_AirQ.text)/volume[0])<0.8)) */
                                    && outParam.getrMotorMaxMachineSite() >= maxmachinesite
                                    && outParam.getrMotorMaxPower() >= vfan.getFanMotorRating()
                                    && js != 8
                                    ) {
                                packageCurve(vfan, fanInParam);
                                fan.setFanModel(vfan.getFanDescription().split("/")[0] + frame);
                                fan.setOutletVelocity("" + BaseDataUtil.decimalConvert(vfan.getAirVelocity(), 1));
                                fan.setEfficiency("" + BaseDataUtil.decimalConvert(vfan.getTotalEff(), 1));
                                fan.setAbsorbedPower("" + BaseDataUtil.decimalConvert(vfan.getPwrCondition(), 2));
                                fan.setRPM("" + BaseDataUtil.decimalConvert(vfan.getFanSpeed(), 1));
                                fan.setNoise("" + BaseDataUtil.decimalConvert(vfan.getInlet_LwLin_Overall(), 1));
                                fan.setMotorBaseNo(strTyper);
                                fan.setMotorPower("" + vfan.getFanMotorRating());
                                fan.setPole("" + js);
                                fan.setCurve(vfan.getChartFilePath());
                                fan.setMaxAbsorbedPower("" + vfan.getMaxPower());//最大吸收功率 KW
                                fan.setMaxRPM("" + vfan.getMaxSpeed());//最大转速
                                fan.setTotalPressure("" + vfan.getTotalPressure());//全压
                                if (fan.getFanModel().contains("FDA")) {
                                    outlet = SystemCalculateConstants.FAN_OPTION_OUTLET_F;
                                }
                                if (fan.getFanModel().contains("BDB")) {
                                    outlet = SystemCalculateConstants.FAN_OPTION_OUTLET_B;
                                }
                                if (fan.getFanModel().contains("AD")) {
                                    outlet = SystemCalculateConstants.FAN_OPTION_OUTLET_AIRFOIL;
                                }
                                fan.setOutlet(outlet);
                                fan.setMaxAbsorbedPower("" + vfan.getMaxPower());//最大吸收功率 KW
                                fan.setMaxRPM("" + vfan.getMaxSpeed());//最大转速
                                fan.setTotalPressure("" + vfan.getTotalPressure());//全压
                                fan.setAPower(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(vfan.getOutlet_LPA_Overall() + 7.0, 0)));
                                fan.setASoundPressure(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(vfan.getOutlet_LPA_Overall(), 1)));
                                try {
                                    fanInfoSetting(fan, vfan);//设置噪音
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                retFans.add(fan);
                            }
                        }

                        if (3 == skFanTypePo.getBend()) {
                            if (vfan.getFanDescription().split("/")[0].equals(skFanTypePo.getFan())
                                /*and (((strtofloat(E_AirQ.text)/volume[0])>0.4)
                                    and ((strtofloat(E_AirQ.text)/volume[0])<0.8)) */
                                    && outParam.getrMotorMaxMachineSite() >= maxmachinesite
                                    && outParam.getrMotorMaxPower() >= vfan.getFanMotorRating()
                                    && js != 8
                                    ) {
                                packageCurve(vfan, fanInParam);
                                fan.setFanModel(vfan.getFanDescription().split("/")[0] + frame);
                                fan.setOutletVelocity("" + BaseDataUtil.decimalConvert(vfan.getAirVelocity(), 1));
                                fan.setEfficiency("" + BaseDataUtil.decimalConvert(vfan.getTotalEff(), 1));
                                fan.setAbsorbedPower("" + BaseDataUtil.decimalConvert(vfan.getPwrCondition(), 2));
                                fan.setRPM("" + BaseDataUtil.decimalConvert(vfan.getFanSpeed(), 1));
                                fan.setNoise("" + BaseDataUtil.decimalConvert(vfan.getInlet_LwLin_Overall(), 1));
                                fan.setMotorBaseNo(strTyper);
                                fan.setMotorPower("" + vfan.getFanMotorRating());
                                fan.setPole("" + js);
                                fan.setCurve(vfan.getChartFilePath());
                                fan.setMaxAbsorbedPower("" + vfan.getMaxPower());//最大吸收功率 KW
                                fan.setMaxRPM("" + vfan.getMaxSpeed());//最大转速
                                fan.setTotalPressure("" + vfan.getTotalPressure());//全压
                                if (fan.getFanModel().contains("FDA")) {
                                    outlet = SystemCalculateConstants.FAN_OPTION_OUTLET_F;
                                }
                                if (fan.getFanModel().contains("BDB")) {
                                    outlet = SystemCalculateConstants.FAN_OPTION_OUTLET_B;
                                }
                                if (fan.getFanModel().contains("AD")) {
                                    outlet = SystemCalculateConstants.FAN_OPTION_OUTLET_AIRFOIL;
                                }
                                fan.setOutlet(outlet);
                                fan.setMaxAbsorbedPower("" + vfan.getMaxPower());//最大吸收功率 KW
                                fan.setMaxRPM("" + vfan.getMaxSpeed());//最大转速
                                fan.setTotalPressure("" + vfan.getTotalPressure());//全压
                                fan.setAPower(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(vfan.getOutlet_LPA_Overall() + 7.0, 0)));
                                fan.setASoundPressure(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(vfan.getOutlet_LPA_Overall(), 1)));
                                try {
                                    fanInfoSetting(fan, vfan);//设置噪音
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                retFans.add(fan);
                            }
                        }
                    }
                }
            }
        }
        return retFans;
    }

    private void packageCurve(KrugerSelection krugerSelection, FanInParam fanInParam) {
        String code = JSONObject.toJSONString(krugerSelection);
        String path = System.getProperty("user.dir") + "/" + EngineConstant.SYS_PATH_BMP_KRUGER + fanInParam.getProjectId() + EngineConstant.SYS_PUNCTUATION_SLASH + code.hashCode() + EngineConstant.SYS_PUNCTUATION_SLASH;
        if (CCRDFileUtil.fileExists(path)) {
            path = path + SYS_NAME_KRUGER_CHART + EngineConstant.SYS_NAME_BMP;//kruger需要命名图片名称
            LOGGER.info("kruger bmp files path exists share bmp");
        } else {
            boolean b = CCRDFileUtil.createDir(path);// 调用方法创建目录
            if (!b) {// 创建目录失败引擎无法调用成功
                LOGGER.error("fan kruger create bmp error param :" + path);
            }
            KrugerChartInfo krugerChartInfo = new KrugerChartInfo(krugerSelection);
            krugerChartInfo.setWidth(500);
            krugerChartInfo.setHeight(500);
            krugerChartInfo.setChartName(SYS_NAME_KRUGER_CHART);
            krugerChartInfo.setChartType(1);
            krugerChartInfo.setChartPath(path);
            KrugerChartResult krugerChartResult = null;//曲线图
            try {
                krugerChartResult = krugerEngineService.generateChart(krugerChartInfo);
            } catch (FanEngineException e) {
                LOGGER.info("kruger bmp files error");
            }
            path = krugerChartResult.getChartFilePath();
        }
        path = path.replace("asserts", "files").replace(System.getProperty("user.dir") + "/", "");//页面接收图片规则
        krugerSelection.setChartFilePath(path);
        /**
         * 噪声
         */
        KrugerSoundSpectrumEx krugerSoundSpectrumEx = krugerEngineService.soundSpectrumEx(krugerSelection);
        krugerSelection.setOutletLwLinSpectrum(krugerSoundSpectrumEx.getOutlet_LwLin_Spectrum());
        krugerSelection.setInletLwLinSpectrum(krugerSoundSpectrumEx.getInlet_LwLin_Spectrum());
    }

    /**
     * 设置噪音
     *
     * @param fan
     * @param vfan
     */
    private void fanInfoSetting(FanInfo fan, KrugerSelection vfan) throws Exception {
        float[] outletLwLinSpectrum = vfan.getOutletLwLinSpectrum();
        float[] inletLwLinSpectrum = vfan.getInletLwLinSpectrum();

        for (int i = 0; i < outletLwLinSpectrum.length; i++) {
            String str = new DecimalFormat(".0").format(outletLwLinSpectrum[i]);
            FanInfo.class.getMethod("setEngineData" + (i + 1), String.class).invoke(fan, str);
        }
        for (int i = 0; i < inletLwLinSpectrum.length; i++) {
            String str = new DecimalFormat(".0").format(inletLwLinSpectrum[i]);
            FanInfo.class.getMethod("setEngineData" + (i + 9), String.class).invoke(fan, str);
        }

}
}

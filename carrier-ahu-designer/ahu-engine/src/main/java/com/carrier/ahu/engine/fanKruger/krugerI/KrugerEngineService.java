package com.carrier.ahu.engine.fanKruger.krugerI;

import java.util.List;

import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.KrugerChartInfo;
import com.carrier.ahu.engine.fan.KrugerChartResult;
import com.carrier.ahu.engine.fan.KrugerCurvePoints;
import com.carrier.ahu.engine.fan.KrugerSelectInfo;
import com.carrier.ahu.engine.fan.KrugerSelection;
import com.carrier.ahu.engine.fan.KrugerSoundSpectrumEx;

/**
 * Created by Braden Zhou on 2019/02/20.
 */
public interface KrugerEngineService {

    List<KrugerSelection> select(KrugerSelectInfo krugerSelectInfo) throws FanEngineException;

    KrugerCurvePoints curvePoints(KrugerSelection krugerSelection) throws FanEngineException;

    KrugerSoundSpectrumEx soundSpectrumEx(KrugerSelection krugerSelection) throws FanEngineException;

    KrugerChartResult generateChart(KrugerChartInfo krugerChartInfo) throws FanEngineException;

}

package com.carrier.ahu.engine.service.damper;

import static com.carrier.ahu.common.enums.DamperMaterialEnum.AL;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39CQ;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39G;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39XT;
import static com.google.common.base.Preconditions.checkArgument;

import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.engine.EngineException;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.util.ExcelUtils;
import com.carrier.ahu.vo.DamperPosSizeInputVO;
import com.carrier.ahu.vo.DamperPosSizeResultVO;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by Braden Zhou on 2019/08/13.
 */
@Slf4j
@Service
public class DamperSizeServiceImpl implements DamperSizeService {

    private static final String DAMPER_SIZE_39CQ_39G_FILE = "damper_size_39CQ_39G.xlsx";

    private static final Map<String, Integer> DAMPER_SIZE_SHEETS = Maps.newHashMap();

    static {
        DAMPER_SIZE_SHEETS.put(AHU_PRODUCT_39CQ, 0);
        DAMPER_SIZE_SHEETS.put(AHU_PRODUCT_39CQ + AL, 2);
        DAMPER_SIZE_SHEETS.put(AHU_PRODUCT_39XT, 0);
        DAMPER_SIZE_SHEETS.put(AHU_PRODUCT_39XT + AL, 2);
        DAMPER_SIZE_SHEETS.put(AHU_PRODUCT_39G, 1);
        DAMPER_SIZE_SHEETS.put(AHU_PRODUCT_39G + AL, 3);
    }

    private static final String[] DAMPER_AL_INPUT_CELLS = { "H50", "C56", "G56", "H65", "C71", "G71" };
    private static final String[] DAMPER_AL_OUTPUT_CELLS = { "C59", "G59", "C74", "G74", "D59", "D74" };

    private static final String[] DAMPER_GI_FD_INPUT_CELLS = { "H52", "C58", "G58", "H67", "C73", "G73" };
    private static final String[] DAMPER_GI_FD_OUTPUT_CELLS = { "C61", "G61", "C76", "G76", "D61", "D76" };

    @Override
    public DamperPosSizeResultVO adjustDamperSize(DamperPosSizeInputVO inputVo) {
        checkArgument(AhuUtil.isValidProduct(inputVo.getProduct()));

        Workbook wb = null;
        try {
            wb = new XSSFWorkbook(this.getClass().getClassLoader().getResourceAsStream(DAMPER_SIZE_39CQ_39G_FILE));
            FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();

            Sheet sheet = null;
            String[] inputCells = null;
            String[] outputCells = null;
            if (inputVo.getDamperMaterial().equals(AL)) {
                sheet = wb.getSheetAt(DAMPER_SIZE_SHEETS.get(inputVo.getProduct() + AL));
                inputCells = DAMPER_AL_INPUT_CELLS;
                outputCells = DAMPER_AL_OUTPUT_CELLS;
            } else {
                sheet = wb.getSheetAt(DAMPER_SIZE_SHEETS.get(inputVo.getProduct()));
                inputCells = DAMPER_GI_FD_INPUT_CELLS;
                outputCells = DAMPER_GI_FD_OUTPUT_CELLS;
            }

            int[] inputs = { inputVo.getUnitLengthMode(), inputVo.getModeA(), inputVo.getModeB(),
                    inputVo.getUnitWidthMode(), inputVo.getModeC(), inputVo.getModeD() };

            for (int i = 0; i < inputs.length; i++) {
                ExcelUtils.setCellValue(sheet, inputCells[i], inputs[i]);
            }

            double[] outputValues = new double[outputCells.length];
            for (int i = 0; i < outputCells.length; i++) {
                Cell cell = ExcelUtils.getCell(sheet, outputCells[i]);
                CellValue cellValue = evaluator.evaluate(cell);
                outputValues[i] = cellValue.getNumberValue();
            }

            return new DamperPosSizeResultVO(outputValues);
        } catch (Exception e) {
            log.error("failed to adjust damper size", e);
            throw new EngineException(ErrorCode.ADJUST_DAMPER_SIZE_FAILED);
        } finally {
            IOUtils.closeQuietly(wb);
        }
    }

}

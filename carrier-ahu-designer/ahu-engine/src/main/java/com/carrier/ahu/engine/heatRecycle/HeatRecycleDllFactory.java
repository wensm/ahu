package com.carrier.ahu.engine.heatRecycle;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.heatRecycle.dri.DRIHeatRecycle;
import com.carrier.ahu.engine.heatRecycle.heatX.HeatRecycleHeatXDllFactory;
import com.carrier.ahu.engine.heatRecycle.plate.PlateHeatRecycle;
import com.carrier.ahu.engine.heatRecycle.yufeng.YuFengHeatRecycle;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by LIANGD4 on 2017/11/28.
 */
public class HeatRecycleDllFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeatRecycleDllFactory.class.getName());
    private static final HeatRecycleHeatXDllFactory HEAT_RECYCLE_HEAT_X_DLL_FACTORY = new HeatRecycleHeatXDllFactory();
    private static final YuFengHeatRecycle YUFENG_HEAT_RECYCLE = new YuFengHeatRecycle();
    private static final PlateHeatRecycle PLATE_HEAT_RECYCLE = new PlateHeatRecycle();
    private static final DRIHeatRecycle DRI_HEAT_RECYCLE = new DRIHeatRecycle();

    // 调用热回收引擎
    public List<HeatRecycleInfo> heatRecycleDll(HeatRecycleParam heatRecycleParam) throws Exception {
        LOGGER.info("heatRecycleDll engine supper begin ");
        LOGGER.info("heatRecycleDll engine supper param:" + JSONArray.toJSONString(heatRecycleParam));
        heatRecycleParam = packageHeatRecycleParam(heatRecycleParam);// 特殊字段加工
        List<HeatRecycleInfo> heatRecycleInfoList = null;
        if (heatRecycleParam.isNSEnable()) {// 非标无转轮不参数计算
            LOGGER.info("heatRecycleDll TEP engine supper param:" + heatRecycleParam);
            return null;
		} else {
			if (EngineConstant.METASEXON_PLATEHEATRECYCLE_HEATPLATE.equals(heatRecycleParam.getHeatRecoveryType())) {
				if (heatRecycleParam.getBrand().equals("C")) {// Enventus:毅科
					heatRecycleInfoList = HEAT_RECYCLE_HEAT_X_DLL_FACTORY.getHeatXPlateDllAll(heatRecycleParam);
				}
			} else {
				if (heatRecycleParam.getBrand().equals("C")) {// Enventus:毅科
					heatRecycleInfoList = HEAT_RECYCLE_HEAT_X_DLL_FACTORY.getHeatXDllAll(heatRecycleParam);
				} else {// 如果是百瑞
					heatRecycleInfoList = DRI_HEAT_RECYCLE.getHeatRecyleInfo(heatRecycleParam);
				}
			}
		}
        LOGGER.info("heatRecycleDll engine supper end ");
        return heatRecycleInfoList;
    }

    // 调用热回收引擎
    public List<List<HeatRecycleInfo>> heatRecycleDll2(HeatRecycleParam heatRecycleParam) throws Exception {
        LOGGER.info("heatRecycleDll engine supper begin ");
        LOGGER.info("heatRecycleDll engine supper param:" + JSONArray.toJSONString(heatRecycleParam));
        heatRecycleParam = packageHeatRecycleParam(heatRecycleParam);// 特殊字段加工
        List<List<HeatRecycleInfo>> heatRecycleInfoGroups = Lists.newArrayList();
        if (heatRecycleParam.isNSEnable()) {// 非标无转轮不参数计算
            LOGGER.info("heatRecycleDll TEP engine supper param:" + heatRecycleParam);
            return Lists.newArrayList();
        } else {
            if (EngineConstant.METASEXON_PLATEHEATRECYCLE_HEATPLATE.equals(heatRecycleParam.getHeatRecoveryType())) {
                if (heatRecycleParam.getBrand().equals("C")) {// Enventus:毅科
                    heatRecycleInfoGroups = HEAT_RECYCLE_HEAT_X_DLL_FACTORY.getHeatXPlateDllAll2(heatRecycleParam);
                }
            } else {
                if (heatRecycleParam.getBrand().equals("C")) {// Enventus:毅科
                    heatRecycleInfoGroups = HEAT_RECYCLE_HEAT_X_DLL_FACTORY.getHeatXDllAll2(heatRecycleParam);
                } else {// 如果是百瑞
                    heatRecycleInfoGroups = DRI_HEAT_RECYCLE.getHeatRecyleInfoGroups(heatRecycleParam);
                }
            }
        }
        LOGGER.info("heatRecycleDll engine supper end ");

        doProcess(heatRecycleInfoGroups);

        return heatRecycleInfoGroups;
    }

    /**
     * 返回CalResult JSON 结果参数系数统一加工
     * @param heatRecycleInfoGroups
     */
    private void doProcess(List<List<HeatRecycleInfo>> heatRecycleInfoGroups) {

        try {
            for (List<HeatRecycleInfo> heatRecycleInfoGroup : heatRecycleInfoGroups) {
                for (HeatRecycleInfo heatRecycleInfo : heatRecycleInfoGroup) {
                    if(EmptyUtil.isNotEmpty(heatRecycleInfo.getReturnSAirResistance())){
                        double retSaR = BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(heatRecycleInfo.getReturnSAirResistance())*1.3, 1);
                        heatRecycleInfo.setReturnSAirResistance(String.valueOf(retSaR));
                    }
                    if(EmptyUtil.isNotEmpty(heatRecycleInfo.getReturnEAirResistance())){
                        double retEaR = BaseDataUtil.decimalConvert(BaseDataUtil.stringConversionDouble(heatRecycleInfo.getReturnEAirResistance())*1.3, 1);
                        heatRecycleInfo.setReturnEAirResistance(String.valueOf(retEaR));
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("返回CalResult JSON 结果参数系数统一加工 失败：",e);
        }
    }

    // 特殊字段加工
    private HeatRecycleParam packageHeatRecycleParam(HeatRecycleParam heatRecycleParam) {
        if (1 == heatRecycleParam.getWheelDepth()) {// 转轮厚度
            heatRecycleParam.setWheelDepth(200);
        }
        return heatRecycleParam;
    }

}

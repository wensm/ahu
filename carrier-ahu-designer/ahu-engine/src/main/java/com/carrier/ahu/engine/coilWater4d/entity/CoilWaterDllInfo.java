package com.carrier.ahu.engine.coilWater4d.entity;

import lombok.Data;

//盘管调用DLL结果
@Data
public class CoilWaterDllInfo {
    private Integer rows;//排数
    private Integer fpi;//片距
    private String cir;//回路
    private Double faceArea;//迎风面积
    private Double velocity;//风面速
    private Double eaDB;//进风干球温度
    private Double eaWB;//进风湿球温度
    private Double eaRH;//进风相对湿度
    private Double laDB;//出风干球温度
    private Double laWB;//出风湿球温度
    private Double laRH;//出风相对湿度
    private Double apd;//空气阻力
    private Double wpd;//水阻力
    private Double flow;//水流量
    private Double ewt;//进水温度
    private Double tr;//水温升
    private Double fluidVelocity;//介质流速
    private Double total;//冷量
    private Double sensible;//显冷
    private Double bypass;//Bypass
    private String uniType;//机组型号
    private String season;//季节 W:冬、S:夏、
    private String coilType;//盘管类型
    private Long fluid_ID;//入参参数
    private double CONC1;//入参参数
    private String calculationConditions;//水温升、水流量标识
    /*直接蒸发式属性*/
    private String Cond;//冷凝器进风温度
    private String SatCond;//饱和冷凝温度
    private String CDU;//unit
//    private String SubClg;//过冷度
    private Double returnNoAHRIWaterDrop;
    private Double waterFlowRate;

}

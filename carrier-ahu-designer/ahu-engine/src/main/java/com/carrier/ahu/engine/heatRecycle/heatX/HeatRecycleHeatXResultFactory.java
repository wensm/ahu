package com.carrier.ahu.engine.heatRecycle.heatX;

import com.carrier.ahu.engine.heatRecycle.HeatRecycle;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.unit.BaseDataUtil;

/**
 * Created by LIANGD4 on 2017/12/19.
 */
public class HeatRecycleHeatXResultFactory {

    //引擎结果转换对象
    public HeatRecycleInfo packageHeatRecycleInfo(String rotorSize, String season, double supplyAirDB,
            double supplyAirWB, double supplyAirRH, double supplyAirPD, double supplyAirTotalEff,double supplyAirTempEff,double supplyAirHumiEff,
            double exhaustAirDB, double exhaustAirWB, double exhaustAirRH, double exhaustAirPD,
            double exhaustAirTotalEff,double exhaustAirTempEff,double exhaustAirHumiEff,  double totalCap, double sensible, double materialPrice, double labourPrice,
            String exchangeModel, int height, int width, String corrugatedChannel) {
        HeatRecycleInfo heatRecycleInfo = new HeatRecycleInfo();
        heatRecycleInfo.setReturnWheelSize(rotorSize);//转轮尺寸
        heatRecycleInfo.setReturnSeason(season);//季节
        heatRecycleInfo.setReturnSDryBulbT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirDB, 1)));//干球温度送风侧
        heatRecycleInfo.setReturnSWetBulbT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirWB, 1)));//湿球温度送风侧
        heatRecycleInfo.setReturnSRelativeT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirRH, 1)));//相对湿度送风侧
        heatRecycleInfo.setReturnSAirResistance(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirPD, 1)));//空气阻力送风侧
        heatRecycleInfo.setReturnSEfficiency(HeatRecycle.conbineEffecStr((BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirTotalEff * 100, 2))),(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirTempEff * 100, 2))),(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(supplyAirHumiEff * 100, 2)))));//效率送风侧
        heatRecycleInfo.setReturnEDryBulbT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirDB, 1)));//干球温度进风侧
        heatRecycleInfo.setReturnEWetBulbT(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirWB, 1)));//湿球温度进风侧
        heatRecycleInfo.setReturnERelativeT(exhaustAirRH == 0 ? "0" :BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirRH, 1)));//相对湿度进风侧
        heatRecycleInfo.setReturnEAirResistance(exhaustAirPD == 0 ? "0" :BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirPD, 1)));//空气阻力进风侧
        heatRecycleInfo.setReturnEEfficiency(HeatRecycle.conbineEffecStr((BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirTotalEff * 100, 2))),(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirTempEff * 100, 2))),(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(exhaustAirHumiEff * 100, 2)))));//效率进风侧
        heatRecycleInfo.setReturnTotalHeat(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(totalCap, 1)));//全热
        heatRecycleInfo.setReturnSensibleHeat(BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(sensible, 1)));//显热
        //heatRecycleInfo.setMaterialPrice(String.format("%.0f/%.0f", materialPrice, labourPrice));
        heatRecycleInfo.setMaterialPrice(String.format("%.0f", materialPrice));
        heatRecycleInfo.setSectionSideL(String.valueOf(width));
        heatRecycleInfo.setHeatExchangerL(BaseDataUtil.integerConversionString(height));
        heatRecycleInfo.setCorrugatedChannel(corrugatedChannel);
        heatRecycleInfo.setExchangeModel(exchangeModel);
        return heatRecycleInfo;
    }

}

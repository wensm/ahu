package com.carrier.ahu.engine.fan.motor;

import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.SKFanMotorEff;
import com.carrier.ahu.metadata.entity.fan.SKFanVMotorEff;
import com.carrier.ahu.metadata.entity.fan.SKFanVMotorEffGe;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class MortorDataParser implements IMotorDataParser {

    @Override
    public double getMotorEff(String supplier, String motorType, String power, String level) {

        /*A:三级能效电机、B:防爆电机、C:双速电机、D:变频电机、E:二级能效*/
        /*A:ABB、B:默认、C:东元、S:西门子*/
        /*A W T T*/
        //如果是双速电机
        if (SystemCalculateConstants.FAN_TYPE_4.equals(motorType)) {
            return 0;
        }
        //如果是变频电机
        if (SystemCalculateConstants.FAN_TYPE_5.equals(motorType) || SystemCalculateConstants.FAN_TYPE_6.equals(motorType)) {
            //TODO修改为S_K_FAN_VMOTOR_EFF
            List<SKFanVMotorEff> skFanVMotorEffList = AhuMetadata.findList(SKFanVMotorEff.class);
            Collections.sort(skFanVMotorEffList, Comparator.comparing(SKFanVMotorEff::getPower));
            for (SKFanVMotorEff sKFanVMotorEff : skFanVMotorEffList) {
                if (sKFanVMotorEff.getPower() >= BaseDataUtil.stringConversionDouble(power) && sKFanVMotorEff.getLevel() == BaseDataUtil.stringConversionInteger(level)) {
                    if (SystemCalculateConstants.FAN_SUPPLIER_ABB.equals(supplier)) {//ABB A
                        return sKFanVMotorEff.getAeff();
                    } else if (SystemCalculateConstants.FAN_SUPPLIER_DEFAULT.equals(supplier)) {//default W
                        return sKFanVMotorEff.getWeff();
                    } else if (SystemCalculateConstants.FAN_SUPPLIER_DONGYUAN.equals(supplier) || SystemCalculateConstants.FAN_SUPPLIER_SIMENS.equals(supplier)) {//Dongyuan T Simens
                        return sKFanVMotorEff.getTeff();
                    }
                }
            }

            List<SKFanVMotorEffGe> skFanVMotorEffGeList = AhuMetadata.findList(SKFanVMotorEffGe.class);
            Collections.sort(skFanVMotorEffGeList, Comparator.comparing(SKFanVMotorEffGe::getPower));
            for (SKFanVMotorEffGe skFanVMotorEffGe : skFanVMotorEffGeList) {
                if (SystemCalculateConstants.FAN_SUPPLIER_GE.equals(supplier)) {
                    return skFanVMotorEffGe.getGe();
                }
            }
        }
        List<SKFanMotorEff> skFanMotorEffList = AhuMetadata.findList(SKFanMotorEff.class);
        Collections.sort(skFanMotorEffList, Comparator.comparing(SKFanMotorEff::getPower));
        for (SKFanMotorEff skFanMotorEffPo : skFanMotorEffList) {
            if (skFanMotorEffPo.getPower() >= BaseDataUtil.stringConversionDouble(power) && skFanMotorEffPo.getLevel() == BaseDataUtil.stringConversionInteger(level)) {
                if (SystemCalculateConstants.FAN_TYPE_2.equals(motorType) || SystemCalculateConstants.FAN_TYPE_3.equals(motorType)) {//三级能效电机 防爆电机
                    if ("380V-3Ph-50Hz".equals(power)) {
                        return skFanMotorEffPo.getV0E3A();
                    }
                    try {
                        return packageMotorCode(supplier, motorType, power, skFanMotorEffPo);//默认值
                    } catch (Exception e) {
                        return skFanMotorEffPo.getV0E3A();
                    }
                } else if (SystemCalculateConstants.FAN_TYPE_1.equals(motorType)) {//二级能效
                    if ("380V-3Ph-50Hz".equals(power)) {
                        return skFanMotorEffPo.getV0E2T();
                    }
                    try {
                        return packageMotorCode(supplier, motorType, power, skFanMotorEffPo);//默认值
                    } catch (Exception e) {
                        return skFanMotorEffPo.getV0E2T();
                    }
                } else if (SystemCalculateConstants.FAN_SUPPLIER_DONGYUAN.equals(supplier) || SystemCalculateConstants.FAN_SUPPLIER_SIMENS.equals(supplier)) {
                    return skFanMotorEffPo.getCEFF2();
                }
            }
        }
        return 0;
    }

    private double packageMotorCode(String supplier, String motorType, String power, SKFanMotorEff skFanMotorEffPo) {
        Class<?> aClass = SKFanMotorEff.class;
        Field[] fields = aClass.getDeclaredFields();
        Map<Object, Object> map = new HashMap<>();
        for (Field field : fields) {
            map.put(field.getName(), getResult(field.getName(), skFanMotorEffPo));
        }
        String value = map.get(valueCode(supplier, motorType, power)).toString();
        return BaseDataUtil.stringConversionDouble(value);
    }

    private String valueCode(String supplier, String motorType, String power) {
        String supplierCode = "A";//电机品牌
        if (SystemCalculateConstants.FAN_SUPPLIER_ABB.equals(supplier)) {//ABB A
            supplierCode = "A";
        } else if (SystemCalculateConstants.FAN_SUPPLIER_DEFAULT.equals(supplier)) {//default W
            supplierCode = "W";
        } else if (SystemCalculateConstants.FAN_SUPPLIER_DONGYUAN.equals(supplier) || SystemCalculateConstants.FAN_SUPPLIER_SIMENS.equals(supplier)) {//Dongyuan T Simens
            supplierCode = "T";
        }
        String powerCode = "0"; //电源
        if ("380V-3Ph-50Hz".equals(power)) {
            powerCode = "0";
        } else if ("380V-3Ph-60Hz".equals(power)) {
            powerCode = "1";
        } else if ("230V-3Ph-50Hz".equals(power)) {
            powerCode = "2";
        } else if ("230V-3Ph-60Hz".equals(power)) {
            powerCode = "3";
        } else if ("400V-3Ph-50Hz".equals(power)) {
            powerCode = "4";
        } else if ("400V-3Ph-60Hz".equals(power)) {
            powerCode = "5";
        } else if ("415V-3Ph-50Hz".equals(power)) {
            powerCode = "6";
        } else if ("415V-3Ph-60Hz".equals(power)) {
            powerCode = "7";
        } else if ("460V-3Ph-50Hz".equals(power)) {
            powerCode = "8";
        } else if ("460V-3Ph-60Hz".equals(power)) {
            powerCode = "9";
        }
        String motorTypeCode = SystemCalculateConstants.FAN_TYPE_3;
        if (SystemCalculateConstants.FAN_TYPE_1.equals(motorType)) {
            motorTypeCode = SystemCalculateConstants.FAN_TYPE_2;
        }
        String valueCode = "V" + powerCode + "E" + motorTypeCode + supplierCode;
        return valueCode;
    }

    private static Object getResult(Object fieldName, SKFanMotorEff skFanMotorEffPo) {
        try {
            Class<?> aClass = skFanMotorEffPo.getClass();
            Field declaredField = aClass.getDeclaredField(fieldName.toString());
            declaredField.setAccessible(true);
            PropertyDescriptor pd = new PropertyDescriptor(declaredField.getName(), aClass);
            Method readMethod = pd.getReadMethod();
            return readMethod.invoke(skFanMotorEffPo);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}

package com.carrier.ahu.engine.service.damper;

import com.carrier.ahu.vo.DamperPosSizeInputVO;
import com.carrier.ahu.vo.DamperPosSizeResultVO;

/**
 * Created by Braden Zhou on 2019/08/13.
 */
public interface DamperSizeService {

    DamperPosSizeResultVO adjustDamperSize(DamperPosSizeInputVO inputVo);

}

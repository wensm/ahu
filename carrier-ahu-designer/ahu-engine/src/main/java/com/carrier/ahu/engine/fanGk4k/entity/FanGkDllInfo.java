package com.carrier.ahu.engine.fanGk4k.entity;

import com.carrier.ahu.metadata.entity.fan.SKFanType;
import lombok.Data;

import java.util.Vector;

/**
 * Created by LIANGD4 on 2017/12/13.
 */
@Data
public class FanGkDllInfo {

    private String fanModel;//风机型号
    private double power;
    private double maxPower;
    private double maxMachineSite;
    private String fanCode;
    private double pole;//级数
    private double rla; //转速
    private double nst;//吸收功率
    private double pwt;//轴功率
    private double eft;//效率
    private double ptt;//全压
    private double co;//全压-总静压
    private double tpw;//电机功率
    private boolean suc;//计算结果
    private double maxmotorpower;//最大电机功率
    private double maxspeed;//最大转速
    private double PL;//皮带长度
    private double flcode;//风轮编码
    private double dlcode;//电轮编码
    private double Imped;//叶轮直径
    private String curve;//图片路径
    private double souspep1;
    private double souspep2;
    private double souspep3;
    private double souspep4;
    private double souspep5;
    private double souspep6;
    private double souspep7;
    private double souspep8;
    private double souspe1;
    private double souspe2;
    private double souspe3;
    private double souspe4;
    private double souspe5;
    private double souspe6;
    private double souspe7;
    private double souspe8;
    private String type;//电机类型 1:二级能效电机、2:三级能效电机、3:防爆电机、4:双速电机、5:变频电机、6:变频电机(直连)
    private String supplier;//电机品牌 default：默认、Dongyuan：东元、ABB：ABB、Simens：西门子
    private String skFanTypeVector;
    private String serial;
}

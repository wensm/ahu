package com.carrier.ahu.engine.fan.motor;

public interface IMotorDataParser {
	
	double getMotorEff(String supplier,String motorType,String power, String level);

}

package com.carrier.ahu.engine.heatRecycle.heatX;

import java.util.*;

import org.assertj.core.util.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.TempCalErrorException;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleInfo;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;
import com.carrier.ahu.engine.heatrecycle.CaldensityLib;
import com.carrier.ahu.engine.heatrecycle.HeaterxLib;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.heatrecycle.HeatxPlate;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWWheel;
import com.carrier.ahu.metadata.entity.heatrecycle.WheelInfo;
import com.carrier.ahu.metadata.entity.heatrecycle.WheelPrice;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.heatrecycle.HeatRecycleUtils;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.intl.I18NConstants.GET_IMPORT;

/**
 * Created by LIANGD4 on 2017/12/19.
 */
public class HeatRecycleHeatXDllFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeatRecycleHeatXDllFactory.class.getName());

    private static final HeatRecycleHeatXResultFactory HEAT_RECYCLE_HEAT_X_RESULT_FACTORY = new HeatRecycleHeatXResultFactory();

    //heatXDLL调用封装
    public List<HeatRecycleInfo> getHeatXDllAll(HeatRecycleParam heatRecycleParam) {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();
        boolean winter = false;
        if(heatRecycleParam.isEnableWinter()){
            winter = true;
            heatRecycleParam.setEnableWinter(false);
        }
        HeatRecycleInfo heatRecycleInfo = getHeatXDll(heatRecycleParam);
        if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
            heatRecycleInfoList.add(heatRecycleInfo);
        } else {
            LOGGER.error("HeatXDll Engine Cal Error Season S");
        }
        if (winter) {
            heatRecycleParam.setEnableWinter(true);
            heatRecycleInfo = getHeatXDll(heatRecycleParam);
            if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
                heatRecycleInfo.setReturnSeason("Winter");
                heatRecycleInfoList.add(heatRecycleInfo);
            } else {
                LOGGER.error("HeatXDll Engine Cal Error Season W");
            }
        }
        return heatRecycleInfoList;
    }

  //heatXDLL调用封装
    public List<HeatRecycleInfo> getHeatXPlateDllAll(HeatRecycleParam heatRecycleParam) {
        List<HeatRecycleInfo> heatRecycleInfoList = new ArrayList<HeatRecycleInfo>();
        boolean winter = false;
        if(heatRecycleParam.isEnableWinter()){
            winter = true;
            heatRecycleParam.setEnableWinter(false);
        }
        HeatRecycleInfo heatRecycleInfo = getHeatXPlateDll(heatRecycleParam);
        if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
            heatRecycleInfoList.add(heatRecycleInfo);
        } else {
            LOGGER.error("HeatXDll Engine Cal Error Season S");
        }
        if (winter) {
            heatRecycleParam.setEnableWinter(true);
            heatRecycleInfo = getHeatXPlateDll(heatRecycleParam);
            if (!EmptyUtil.isEmpty(heatRecycleInfo)) {
                heatRecycleInfo.setReturnSeason("Winter");
                heatRecycleInfoList.add(heatRecycleInfo);
            } else {
                LOGGER.error("HeatXDll Engine Cal Error Season W");
            }
        }
        return heatRecycleInfoList;
    }

    //heatXDLL调用封装
    private HeatRecycleInfo getHeatXDll(HeatRecycleParam heatRecycleParam) {
        String season = EngineConstant.JSON_AHU_SEASON_SUMMER;//季节
        if(heatRecycleParam.isEnableWinter()){
            season = EngineConstant.JSON_AHU_SEASON_WINTER;//季节
        }
        double supplyAirVolume = heatRecycleParam.getNAVolume();//回风风量
        double returnAirVolumn = heatRecycleParam.getRAVolume();//回风风量
        double warmTemp = heatRecycleParam.getSNewDryBulbT();//回风夏季干球温度
        double warmWetTemp = heatRecycleParam.getSNewWetBulbT();//回风夏季湿球温度
        double warmRT = heatRecycleParam.getSNewRelativeT();//回风夏季相对湿度
        double coldTemp = heatRecycleParam.getSInDryBulbT();//新风夏季干球温度
        double coldWetTemp = heatRecycleParam.getSInWetBulbT();//新风夏季湿球温度
        double coldRT = heatRecycleParam.getSInRelativeT();//回风夏季相对湿度
        if (heatRecycleParam.isEnableWinter()) {
            warmTemp = heatRecycleParam.getWNewDryBulbT();//回风冬季干球温度
            warmWetTemp = heatRecycleParam.getWNewWetBulbT();//回风冬季湿球温度
            coldTemp = heatRecycleParam.getWInDryBulbT();//新风冬季干球温度
            warmRT = heatRecycleParam.getWNewRelativeT();//回风夏季相对湿度
            coldRT = heatRecycleParam.getWInRelativeT();//回风夏季相对湿度
        }
        String unitModel = heatRecycleParam.getSerial();//机组型号
        String rotordia = HeatRecycleUtils.getRotordiaHeatX(supplyAirVolume / 3600, EngineConstant.SYS_STRING_NUMBER_2, "POWER_HEATX");
        //查表计算
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel);
        String maxrotordia = String.valueOf(partWWheelPo.getWheelDiaHeatX());//wheeldia_HeatX
        double Diameter;
        if (BaseDataUtil.stringConversionInteger(rotordia) > BaseDataUtil.stringConversionInteger(maxrotordia)) {
            Diameter = BaseDataUtil.stringConversionInteger(maxrotordia);
        } else {
            Diameter = BaseDataUtil.stringConversionInteger(rotordia);
        }
        String ECOFRESHModel = String.valueOf(Diameter);
        double suplyVelface = SystemCountUtil.velfacecal(supplyAirVolume, Double.parseDouble(ECOFRESHModel), EngineConstant.SYS_STRING_NUMBER_1);
        if(suplyVelface > 6){
//        	throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        
        double returnVelface = SystemCountUtil.velfacecal(returnAirVolumn, Double.parseDouble(ECOFRESHModel), EngineConstant.SYS_STRING_NUMBER_1);
        if(returnVelface > 6){
//        	throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        double depth = heatRecycleParam.getWheelDepth();//转轮厚度
        int mCode;//逻辑判断
        int rSpeed;//逻辑判断
        String model = heatRecycleParam.getModel();
        if (EngineConstant.SYS_STRING_NUMBER_1.equals(model)) {//热回收型号 1：分子筛 2：硅胶 3：显热
            mCode = 7;
            rSpeed = 25;
        } else if (EngineConstant.SYS_STRING_NUMBER_2.equals(model)) {
            mCode = 5;
            rSpeed = 12;
        } else {
            mCode = 0;
            rSpeed = 17;
        }
        double attitude = FAttitudeConvert(heatRecycleParam.getSeaLevel(), false);//海拔高度转换
        double temp = AirConditionUtils.FTemperatureConvert(warmTemp, true);//回风温度转换
        double density = CaldensityLib.CALDENSITY_INSTANCE.CaldensityBritish(attitude, warmTemp) * 1.2;//海拔高度和回风干球温度转换
        byte s1 = 0;
        byte s2 = 0;
        byte z1 = 0;
        byte z2 = 0;
        String key = "E/0.200/" + getMaterial(mCode);
//        String key = "E/0.200/AI";
//        Pointer in = new Memory(21 * Double.SIZE);
//        in.setDouble(0 * Native.getNativeSize(Double.TYPE), 0.0);//固定
//        in.setDouble(1 * Native.getNativeSize(Double.TYPE), 1.667);//固定
//        in.setDouble(2 * Native.getNativeSize(Double.TYPE),  0.0);
//        AirConditionBean rAirConditionBean = AirConditionUtils.FAirParmCalculate1(warmTemp, warmWetTemp);
//        in.setDouble(3 * Native.getNativeSize(Double.TYPE),  25);
//        in.setDouble(4 * Native.getNativeSize(Double.TYPE),   0.009868);
//        in.setDouble(5 * Native.getNativeSize(Double.TYPE), 1.667);//固定
//        in.setDouble(6 * Native.getNativeSize(Double.TYPE),  (0.0));
//        AirConditionBean nAirConditionBean = AirConditionUtils.FAirParmCalculate1(coldTemp, coldWetTemp);
//        in.setDouble(7 * Native.getNativeSize(Double.TYPE),  5);
//        in.setDouble(8 * Native.getNativeSize(Double.TYPE), 0.002685);
//        in.setDouble(9 * Native.getNativeSize(Double.TYPE),  0);
//        in.setDouble(10 * Native.getNativeSize(Double.TYPE), 0.2);
//        in.setDouble(11 * Native.getNativeSize(Double.TYPE),  12);//固定
//        in.setDouble(12 * Native.getNativeSize(Double.TYPE),  0.0014);
//        in.setDouble(13 * Native.getNativeSize(Double.TYPE), 0.2);//TODO 固定
//        in.setDouble(14 * Native.getNativeSize(Double.TYPE),  1);//固定
//        in.setDouble(15 * Native.getNativeSize(Double.TYPE), 0.00007);//固定
//        in.setDouble(16 * Native.getNativeSize(Double.TYPE), 101325);//固定
//        in.setDouble(17 * Native.getNativeSize(Double.TYPE), 1.0);//固定
//        in.setDouble(18 * Native.getNativeSize(Double.TYPE), 5.0);//固定
//        in.setDouble(19 * Native.getNativeSize(Double.TYPE), 0.0);//固定
//        in.setDouble(20 * Native.getNativeSize(Double.TYPE), 0.0);//固定
        Pointer in = new Memory(21 * Double.SIZE);
        in.setDouble(0 * Native.getNativeSize(Double.TYPE), 0.0);//固定
        in.setDouble(1 * Native.getNativeSize(Double.TYPE), suplyVelface);
        in.setDouble(2 * Native.getNativeSize(Double.TYPE),  0.0);
        AirConditionBean rAirConditionBean = AirConditionUtils.FAirParmCalculate1(warmTemp, warmWetTemp);
        in.setDouble(3 * Native.getNativeSize(Double.TYPE),  warmTemp);
        in.setDouble(4 * Native.getNativeSize(Double.TYPE),   rAirConditionBean.getParamD()/1000);
        in.setDouble(5 * Native.getNativeSize(Double.TYPE), returnVelface);
        in.setDouble(6 * Native.getNativeSize(Double.TYPE),  (0.0));
        AirConditionBean nAirConditionBean = AirConditionUtils.FAirParmCalculate1(coldTemp, coldWetTemp);
        in.setDouble(7 * Native.getNativeSize(Double.TYPE),  coldTemp);
        in.setDouble(8 * Native.getNativeSize(Double.TYPE), nAirConditionBean.getParamD()/1000);
        in.setDouble(9 * Native.getNativeSize(Double.TYPE),  mCode);
        in.setDouble(10 * Native.getNativeSize(Double.TYPE), Diameter/1000);
        in.setDouble(11 * Native.getNativeSize(Double.TYPE),  rSpeed);
        in.setDouble(12 * Native.getNativeSize(Double.TYPE),  0.002);
        in.setDouble(13 * Native.getNativeSize(Double.TYPE), depth/1000);
        in.setDouble(14 * Native.getNativeSize(Double.TYPE),  0);//固定
        in.setDouble(15 * Native.getNativeSize(Double.TYPE), 0.00005);//固定
        in.setDouble(16 * Native.getNativeSize(Double.TYPE), 101325);
        in.setDouble(17 * Native.getNativeSize(Double.TYPE), 1.0);//固定
        in.setDouble(18 * Native.getNativeSize(Double.TYPE), 5.0);//固定
        in.setDouble(19 * Native.getNativeSize(Double.TYPE), 0.0);//固定
        in.setDouble(20 * Native.getNativeSize(Double.TYPE), 0.0);//固定
        Pointer out = new Memory(55 * Double.SIZE);
        HeaterxLib.HEATERX_INSTANCE.GET_WHEELCALC(s1, s2, in, key, z1, z2, out);
        in.clear(21 * Double.SIZE);
        //TODO DLL参数返回值待确认
//        for(int i =1;i<55;i++) {
//        	System.out.println(i+"-->");
//        	System.out.println(out.getDouble(i * Native.getNativeSize(Double.TYPE)));
//        }
        Double sdbt = out.getDouble(23 * Native.getNativeSize(Double.TYPE));
        Double swbt = out.getDouble(41 * Native.getNativeSize(Double.TYPE));
        AirConditionBean airConditionBeanS = null;
        try {
            airConditionBeanS = AirConditionUtils.FAirParmCalculate2(sdbt, swbt * 1000);
        } catch (TempCalErrorException e) {
            LOGGER.error("HeatRecycleHeatXDllFactory getHeatXDll Calculate FAirParmCalculate2 S Error", e.getMessage());
        }
        double supplyAirD = sdbt;//干球温度送风侧
        double supplyAirWB = 0.00;//湿球温度送风侧
        double supplyAirRH = 0.00;//相对湿度送风侧
        if (null != airConditionBeanS) {
            supplyAirWB = airConditionBeanS.getParmTb();
            supplyAirRH = airConditionBeanS.getParmF();
        }
        Double supplyAirPD = Math.abs(out.getDouble(22 * Native.getNativeSize(Double.TYPE)));//空气阻力送风侧
        Double supplyAirTotalEff = out.getDouble(53 * Native.getNativeSize(Double.TYPE));//效率送风侧
        Double supplyAirTempEff = out.getDouble(31 * Native.getNativeSize(Double.TYPE));//效率送风侧
        Double supplyAirHumiEff = out.getDouble(40 * Native.getNativeSize(Double.TYPE));//效率送风侧

        AirConditionBean airConditionBeanE = null;
        Double edbt = out.getDouble(27 * Native.getNativeSize(Double.TYPE));
        Double ewbt = out.getDouble(28 * Native.getNativeSize(Double.TYPE));
        Double exhaustAirDB = edbt;//干球温度进风侧
        double exhaustAirWB = 0.00;//湿球温度进风侧
        double exhaustAirRH = 0.00;//相对湿度进风侧
        try {
            airConditionBeanE = AirConditionUtils.FAirParmCalculate2(edbt, ewbt*100);
        } catch (TempCalErrorException e) {
            LOGGER.error("HeatRecycleHeatXDllFactory getHeatXDll Calculate FAirParmCalculate2 E Error", e.getMessage());
        }
        if (null != airConditionBeanE) {
            exhaustAirWB = airConditionBeanE.getParmTb();//湿球温度进风侧
            exhaustAirRH = airConditionBeanE.getParmF();//相对湿度进风侧
        }
        Double exhaustAirPD = Math.abs(out.getDouble(26 * Native.getNativeSize(Double.TYPE)));//空气阻力进风侧
        Double exhaustAirTotalEff = out.getDouble(54 * Native.getNativeSize(Double.TYPE));//效率进风侧
        Double exhaustAirTempEff = out.getDouble(32 * Native.getNativeSize(Double.TYPE));//效率进风侧
        Double exhaustAirHumiEff = out.getDouble(40 * Native.getNativeSize(Double.TYPE));//效率进风侧
        double totalCap = Math.abs(out.getDouble(44 * Native.getNativeSize(Double.TYPE))+out.getDouble(45 * Native.getNativeSize(Double.TYPE))) / 1000.0;//全热
        double sensible = Math.abs(out.getDouble(44 * Native.getNativeSize(Double.TYPE))) / 1000.0;//显热
/*        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);
        df.format(value);*/
        HeatRecycleInfo heatRecycleInfo = HEAT_RECYCLE_HEAT_X_RESULT_FACTORY.packageHeatRecycleInfo(rotordia, season, supplyAirD, supplyAirWB, supplyAirRH, supplyAirPD, supplyAirTotalEff,supplyAirTempEff,supplyAirHumiEff,
        		exhaustAirDB, exhaustAirWB, exhaustAirRH, exhaustAirPD, exhaustAirTotalEff,exhaustAirTempEff,exhaustAirHumiEff, totalCap, sensible, 0, 0,"",0,0, "");
        if(!EmptyUtil.isEmpty(heatRecycleInfo)){
            return heatRecycleInfo;
        }
        return null;
    }

    public List<List<HeatRecycleInfo>> getHeatXDllAll2(HeatRecycleParam heatRecycleParam) {
        List<List<HeatRecycleInfo>> heatRecycleInfoGroups = Lists.newArrayList();
        double returnSAirResistance = 0;
        double returnEAirResistance = 0;

        int calcRotorDia = this.getCalcRotorDiameter(heatRecycleParam.getNAVolume());
        int maxRotorDia = this.getMaxRotorDiameter(heatRecycleParam.getSerial());
        List<WheelInfo> wheelInfos = this.getRotorDiameterGroups(calcRotorDia, maxRotorDia);
        for (WheelInfo wheelInfo : wheelInfos) {
        	 HeatRecycleInfo heatRecycleInfoSummer=null;
        	 HeatRecycleInfo heatRecycleInfoWinter=null;
        	try {
             heatRecycleInfoSummer = this.getHeatRecycleInfo(heatRecycleParam, wheelInfo, true);
             if(heatRecycleInfoSummer!=null) {
                returnSAirResistance = Double.parseDouble(heatRecycleInfoSummer.getReturnSAirResistance());
                returnEAirResistance = Double.parseDouble(heatRecycleInfoSummer.getReturnEAirResistance());
                if(Integer.parseInt(heatRecycleInfoSummer.getReturnWheelSize()) <= 1600){
                    if(returnSAirResistance > 300 || returnEAirResistance > 300) {
                        continue;
                    }
                }else{
                    if(returnSAirResistance > 250 || returnEAirResistance > 250) {
                        continue;
                    }
                }
                heatRecycleInfoWinter = this.getHeatRecycleInfo(heatRecycleParam, wheelInfo, false);
                if(heatRecycleInfoWinter != null){
                    returnSAirResistance = Double.parseDouble(heatRecycleInfoWinter.getReturnSAirResistance());
                    returnEAirResistance = Double.parseDouble(heatRecycleInfoWinter.getReturnEAirResistance());
                    if(Integer.parseInt(heatRecycleInfoWinter.getReturnWheelSize()) <= 1600){
                        if(returnSAirResistance > 300 || returnEAirResistance > 300) {
                            continue;
                        }
                    }else{
                        if(returnSAirResistance > 250 || returnEAirResistance > 250) {
                            continue;
                        }
                    }
                }
             }
        	}catch(Exception e) {
        		continue;
        	}
            List<HeatRecycleInfo> heatRecycleInfoOfTwoSeasons = Lists.newArrayList();
            if (heatRecycleInfoSummer != null) {
                heatRecycleInfoOfTwoSeasons.add(heatRecycleInfoSummer);
            }
            if (heatRecycleInfoWinter != null) {
                heatRecycleInfoOfTwoSeasons.add(heatRecycleInfoWinter);
            }
            if (!heatRecycleInfoOfTwoSeasons.isEmpty()) {
                heatRecycleInfoGroups.add(heatRecycleInfoOfTwoSeasons);
            }
        }
        
        if(EmptyUtil.isEmpty(heatRecycleInfoGroups)) {
        	throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }

        return heatRecycleInfoGroups;
    }

    private int getCalcRotorDiameter(double airVolume) {
        String rotordia = HeatRecycleUtils.getRotordiaHeatX(airVolume / 3600,
                EngineConstant.SYS_STRING_NUMBER_2, "POWER_HEATX");
        return BaseDataUtil.stringConversionInteger(rotordia);
    }

    private int getMaxRotorDiameter(String unitModel) {
        PartWWheel partWWheelPo = HeatRecycleUtils.getPartWWheel(unitModel);
        return partWWheelPo.getWheelDiaHeatX();
    }

    private List<WheelInfo> getRotorDiameterGroups(int calcRotorDia, int maxRotorDia) {
        LinkedList<WheelInfo> results = new LinkedList<>();
        List<WheelInfo> wheelInfos = AhuMetadata.findList(WheelInfo.class, "HeatX");
        if (calcRotorDia >= maxRotorDia) {
            for (int i = 0; i < wheelInfos.size(); i++) {
                WheelInfo wheelInfo = wheelInfos.get(i);
                if (!results.contains(wheelInfo)) {
                    results.add(wheelInfo);
                }
                if (results.size() > 3) {
                    results.pop();
                }
                if (wheelInfo.getWheelDia() >= maxRotorDia) {
                    break;
                }
            }
        } else if (calcRotorDia < maxRotorDia) {
            for (int i = 0; i < wheelInfos.size(); i++) {
                WheelInfo wheelInfo = wheelInfos.get(i);
                if (wheelInfo.getWheelDia() >= calcRotorDia && wheelInfo.getWheelDia() <= maxRotorDia) {
                    results.add(wheelInfo);
                    if (results.size() == 3) {
                        break;
                    }
                }
            }
        }
        return results;
    }

    private HeatRecycleInfo getHeatRecycleInfo(HeatRecycleParam heatRecycleParam, WheelInfo wheelInfo,
            boolean isSummer) throws ApiException {
        double warmTemp = heatRecycleParam.getSNewDryBulbT();// 回风夏季干球温度
        double warmWetTemp = heatRecycleParam.getSNewWetBulbT();// 回风夏季湿球温度
        double coldTemp = heatRecycleParam.getSInDryBulbT();// 新风夏季干球温度
        double coldWetTemp = heatRecycleParam.getSInWetBulbT();// 新风夏季干球温度
        if (!isSummer) {
            warmTemp = heatRecycleParam.getWInDryBulbT();// 回风冬季干球温度
            warmWetTemp = heatRecycleParam.getWInWetBulbT();// 回风冬季湿球温度
            coldTemp = heatRecycleParam.getWNewDryBulbT();// 新风冬季干球温度
            coldWetTemp = heatRecycleParam.getWNewWetBulbT();
        }

        String ECOFRESHModel = String.valueOf(wheelInfo.getWheelDia());
        double suplyVelface = SystemCountUtil.velfacecal(heatRecycleParam.getNAVolume(),
                Double.parseDouble(ECOFRESHModel), EngineConstant.SYS_STRING_NUMBER_1);
        if (suplyVelface > 6) {
//            throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }

        double returnVelface = SystemCountUtil.velfacecal(heatRecycleParam.getRAVolume(),
                Double.parseDouble(ECOFRESHModel), EngineConstant.SYS_STRING_NUMBER_1);
        if (returnVelface > 6) {
//            throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        
        double attitude = FAttitudeConvert(heatRecycleParam.getSeaLevel(), false);//海拔高度转换
        double coldDensity = 0;
        UnitSystemEnum unit = AHUContext.getUnitSystem();
        if(UnitSystemEnum.M==unit) {
        	coldDensity=101000/(287.05*(coldTemp+273.15));
        }else {
        	coldDensity=101000/(287.05*(coldTemp+273.15));
        }
        
        double warmDensity = 0;
        if(UnitSystemEnum.M==unit) {
        	warmDensity=101000/(287.05*(warmTemp+273.15));
        }else {
        	warmDensity=101000/(287.05*(warmTemp+273.15));
        }
        
        double warmVel=heatRecycleParam.getNAVolume()/3600*warmDensity;
        double coldVel=heatRecycleParam.getRAVolume()/3600*coldDensity;
        
        if (!isSummer) {
        	
        	 warmVel=heatRecycleParam.getRAVolume()/3600*warmDensity;
             coldVel=heatRecycleParam.getNAVolume()/3600*coldDensity;
        }
        
//        double suplyVel = heatRecycleParam.getNAVolume()/3600*density;
//        if (suplyVel > 6) {
////            throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
//        }
//        if(UnitSystemEnum.M==unit) {
//        	density=101000/(287.05*(warmTemp+273.15));
//        }else {
//        	density=101000/(287.05*(warmTemp+273.15));
//        }
//        double returnVel = heatRecycleParam.getRAVolume()/3600*density;;
//        if (returnVel > 6) {
////            throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
//        }


        double depth = heatRecycleParam.getWheelDepth();// 转轮厚度
        int mCode;// 逻辑判断
        int rSpeed;// 逻辑判断
        String model = heatRecycleParam.getModel();
        if (EngineConstant.SYS_STRING_NUMBER_1.equals(model)) {// 热回收型号 1：分子筛 2：硅胶 3：显热
            mCode = 7;
            rSpeed = 25;
        } else if (EngineConstant.SYS_STRING_NUMBER_2.equals(model)) {
            mCode = 5;
            rSpeed = 12;
        } else {
            mCode = 0;
            rSpeed = 17;
        }

        String key = "E/0.200/" + getMaterial(mCode);
        Pointer in = new Memory(21 * Double.SIZE);
        in.setDouble(0 * Native.getNativeSize(Double.TYPE), 0.0);// 固定
        in.setDouble(1 * Native.getNativeSize(Double.TYPE), BaseDataUtil.decimalDownConvert(warmVel,3));
        in.setDouble(2 * Native.getNativeSize(Double.TYPE), 0.0);
        AirConditionBean rAirConditionBean = AirConditionUtils.FAirParmCalculate1(warmTemp, warmWetTemp);
        in.setDouble(3 * Native.getNativeSize(Double.TYPE), warmTemp);
        in.setDouble(4 * Native.getNativeSize(Double.TYPE), rAirConditionBean.getParamD() / 1000);
        in.setDouble(5 * Native.getNativeSize(Double.TYPE), BaseDataUtil.decimalDownConvert(coldVel,3));
        in.setDouble(6 * Native.getNativeSize(Double.TYPE), 0.0);
        AirConditionBean nAirConditionBean = AirConditionUtils.FAirParmCalculate1(coldTemp,
        		coldWetTemp);
        in.setDouble(7 * Native.getNativeSize(Double.TYPE), coldTemp);
        in.setDouble(8 * Native.getNativeSize(Double.TYPE), nAirConditionBean.getParamD() / 1000);
        in.setDouble(9 * Native.getNativeSize(Double.TYPE), mCode);
        in.setDouble(10 * Native.getNativeSize(Double.TYPE), wheelInfo.getWheelDia() / 1000.0);
        in.setDouble(11 * Native.getNativeSize(Double.TYPE), rSpeed);
        in.setDouble(12 * Native.getNativeSize(Double.TYPE), 0.002);
        in.setDouble(13 * Native.getNativeSize(Double.TYPE), depth / 1000);
        in.setDouble(14 * Native.getNativeSize(Double.TYPE), 0);// 固定
        in.setDouble(15 * Native.getNativeSize(Double.TYPE), 0.00005);// 固定
        in.setDouble(16 * Native.getNativeSize(Double.TYPE), 101325);
        in.setDouble(17 * Native.getNativeSize(Double.TYPE), 1.0);// 固定
        in.setDouble(18 * Native.getNativeSize(Double.TYPE), 5.0);// 固定
        in.setDouble(19 * Native.getNativeSize(Double.TYPE), 0.0);// 固定
        in.setDouble(20 * Native.getNativeSize(Double.TYPE), 0.0);// 固定
        Pointer out = new Memory(55 * Double.SIZE);
        HeaterxLib.HEATERX_INSTANCE.GET_WHEELCALC((byte) 0, (byte) 0, in, key, (byte) 0, (byte) 0, out);

        in.clear(21 * Double.SIZE);
        // TODO DLL参数返回值待确认
        for (int i = 1; i < 55; i++) {
            System.out.println(i + "-->");
            System.out.println(out.getDouble(i * Native.getNativeSize(Double.TYPE)));
        }
//        Double errorCode=out.getDouble(0 * Native.getNativeSize(Double.TYPE));
//        if(errorCode!=0) {
//            return null;
//        }
        Double faceVWarm = out.getDouble(37 * Native.getNativeSize(Double.TYPE));
        Double faceVCold = out.getDouble(38 * Native.getNativeSize(Double.TYPE));
        
        if(faceVWarm>=6 || faceVCold>=6) {
        	return null;
        }
        
        
        
        Double warmSdbt = out.getDouble(23 * Native.getNativeSize(Double.TYPE));
        Double warmSwbt = out.getDouble(41 * Native.getNativeSize(Double.TYPE));
        AirConditionBean warmAirConditionBeanS = null;
        try {
        	warmAirConditionBeanS = AirConditionUtils.FAirParmCalculate2(warmSdbt, warmSwbt * 1000);
        } catch (TempCalErrorException e) {
            LOGGER.error("HeatRecycleHeatXDllFactory getHeatXDll Calculate FAirParmCalculate2 S Error", e.getMessage());
        }
        double warmAirDB = warmSdbt;// 干球温度送风侧
        double warmAirWB = 0.00;// 湿球温度送风侧
        double warmAirRH = 0.00;// 相对湿度送风侧
        if (null != warmAirConditionBeanS) {
            warmAirWB = warmAirConditionBeanS.getParmTb();
            warmAirRH = warmAirConditionBeanS.getParmF();
        }
        Double warmAirPD = Math.abs(out.getDouble(22 * Native.getNativeSize(Double.TYPE)));// 空气阻力送风侧
        Double warmAirTotalEff = out.getDouble(53 * Native.getNativeSize(Double.TYPE));// 效率送风侧
        Double warmAirTempEff = out.getDouble(31 * Native.getNativeSize(Double.TYPE));// 效率送风侧
        Double warmAirHumiEff = out.getDouble(39 * Native.getNativeSize(Double.TYPE));// 效率送风侧

        AirConditionBean coldAirConditionBeanE = null;
        Double coldEdbt = out.getDouble(27 * Native.getNativeSize(Double.TYPE));
        Double coldEwbt = out.getDouble(28 * Native.getNativeSize(Double.TYPE));
        Double coldAirDB = coldEdbt;// 干球温度进风侧
        double coldAirWB = 0.00;// 湿球温度进风侧
        double coldAirRH = 0.00;// 相对湿度进风侧
        try {
        	coldAirConditionBeanE = AirConditionUtils.FAirParmCalculate2(coldEdbt, coldEwbt * 1000);
        } catch (TempCalErrorException e) {
            LOGGER.error("HeatRecycleHeatXDllFactory getHeatXDll Calculate FAirParmCalculate2 E Error", e.getMessage());
        }
        if (null != coldAirConditionBeanE) {
        	coldAirWB = coldAirConditionBeanE.getParmTb();// 湿球温度进风侧
        	coldAirRH = coldAirConditionBeanE.getParmF();// 相对湿度进风侧
        }
        Double coldAirPD = Math.abs(out.getDouble(26 * Native.getNativeSize(Double.TYPE)));// 空气阻力进风侧
        Double coldAirTotalEff = out.getDouble(54 * Native.getNativeSize(Double.TYPE));// 效率进风侧
        
        Double coldAirTempEff = out.getDouble(32 * Native.getNativeSize(Double.TYPE));// 效率进风侧
        
        Double coldAirHumiEff = out.getDouble(40 * Native.getNativeSize(Double.TYPE));// 效率进风侧
        
        double totalCap = Math.abs(out.getDouble(44 * Native.getNativeSize(Double.TYPE))
                + out.getDouble(45 * Native.getNativeSize(Double.TYPE))) / 1000.0;// 全热
        double sensible = Math.abs(out.getDouble(44 * Native.getNativeSize(Double.TYPE))) / 1000.0;// 显热

        String priceCode = this.getHeatRecyclePriceCode(heatRecycleParam, wheelInfo);
        WheelPrice wheelPrice = AhuMetadata.findOne(WheelPrice.class, priceCode);
        if (wheelPrice == null) {
            // TODO 价格不完整，需要补充
            wheelPrice = new WheelPrice();
            LOGGER.error("failed to find price for heat recycle section: [{}]", priceCode);
        }

        double supplyAirDB = 0.0;
        double supplyAirWB = 0.0;
        double supplyAirRH = 0.0;
        double supplyAirPD = 0.0;
        double supplyAirTotalEff = 0.0;
        double supplyAirTempEff = 0.0;
        double supplyAirHumiEff = 0.0;
        
        double exhaustAirDB=0.0;
        double exhaustAirWB=0.0;
        double exhaustAirRH=0.0;
        double exhaustAirPD=0.0;
        double exhaustAirTotalEff=0.0;
        double exhaustAirTempEff=0.0;
        double exhaustAirHumiEff=0.0;	
        
        if(!isSummer) {
        	 supplyAirDB = coldAirDB;
             supplyAirWB = coldAirWB;
             supplyAirRH = coldAirRH;
             supplyAirPD = coldAirPD;
             supplyAirTotalEff = coldAirTotalEff;
             supplyAirTempEff = coldAirTempEff;
             supplyAirHumiEff = coldAirHumiEff;
            
             exhaustAirDB=warmAirDB;
             exhaustAirWB=warmAirWB;
             exhaustAirRH=warmAirRH;
             exhaustAirPD=warmAirPD;
             exhaustAirTotalEff=warmAirTotalEff;
             exhaustAirTempEff=warmAirTempEff;
             exhaustAirHumiEff=warmAirHumiEff;	
        	
        }else {
        	supplyAirDB = warmAirDB;
            supplyAirWB = warmAirWB;
            supplyAirRH = warmAirRH;
            supplyAirPD = warmAirPD;
            supplyAirTotalEff = warmAirTotalEff;
            supplyAirTempEff = warmAirTempEff;
            supplyAirHumiEff = warmAirHumiEff;
           
            exhaustAirDB=coldAirDB;
            exhaustAirWB=coldAirWB;
            exhaustAirRH=coldAirRH;
            exhaustAirPD=coldAirPD;
            exhaustAirTotalEff=coldAirTotalEff;
            exhaustAirTempEff=coldAirTempEff;
            exhaustAirHumiEff=coldAirHumiEff;
        	
        }
        return HEAT_RECYCLE_HEAT_X_RESULT_FACTORY.packageHeatRecycleInfo(String.valueOf(wheelInfo.getWheelDia()),
                isSummer ? EngineConstant.JSON_AHU_SEASON_SUMMER : EngineConstant.JSON_AHU_SEASON_WINTER,
                supplyAirDB, supplyAirWB, supplyAirRH, supplyAirPD, supplyAirTotalEff,supplyAirTempEff,supplyAirHumiEff,
                exhaustAirDB, exhaustAirWB, exhaustAirRH, exhaustAirPD, exhaustAirTotalEff,exhaustAirTempEff,exhaustAirHumiEff, totalCap, sensible,
                wheelPrice.getMaterialPrice(), wheelPrice.getLabourPrice(), priceCode, 0,wheelInfo.getWheelDia(), "");
    }

	private String getHeatRecyclePriceCode(HeatRecycleParam heatRecycleParam, WheelInfo wheelInfo) {
		String model = "";
		if (EngineConstant.JSON_WHEELHEATRECYCLE_MODEL_1.equals(heatRecycleParam.getModel())) {
			if (2700 <= wheelInfo.getWheelDia()) {
				model = "EQM";
			} else {
				model = "EM";
			}
		} else if (EngineConstant.JSON_WHEELHEATRECYCLE_MODEL_2.equals(heatRecycleParam.getModel())) {
			if (2700 <= wheelInfo.getWheelDia()) {
				model = "EQD";
			} else {
				model = "ED";
			}
		} else if (EngineConstant.JSON_WHEELHEATRECYCLE_MODEL_3.equals(heatRecycleParam.getModel())) {
			if (2700 <= wheelInfo.getWheelDia()) {
				model = "EQA";
			} else {
				model = "EA";
			}
		}
		// model/wheelDia/corrugatedChannel
		return String.format("%s/%d/%s", model, wheelInfo.getWheelDia(), "020");
	}

    //heatXDLL调用封装
    private HeatRecycleInfo getHeatXPlateDll(HeatRecycleParam heatRecycleParam) {
        String season = EngineConstant.JSON_AHU_SEASON_SUMMER;//季节
        if(heatRecycleParam.isEnableWinter()){
            season = EngineConstant.JSON_AHU_SEASON_WINTER;//季节
        }
        double supplyAirVolume = heatRecycleParam.getNAVolume();//回风风量
        double returnAirVolumn = heatRecycleParam.getRAVolume();//回风风量
        double warmTemp = heatRecycleParam.getSNewDryBulbT();//回风夏季干球温度
        double warmWetTemp = heatRecycleParam.getSNewWetBulbT();//回风夏季湿球温度
        double warmRT = heatRecycleParam.getSNewRelativeT();//回风夏季相对湿度
        double coldTemp = heatRecycleParam.getSInDryBulbT();//新风夏季干球温度
        double coldWetTemp = heatRecycleParam.getSInWetBulbT();//新风夏季湿球温度
        double coldRT = heatRecycleParam.getSInRelativeT();//回风夏季相对湿度
        if (heatRecycleParam.isEnableWinter()) {
            warmTemp = heatRecycleParam.getWNewDryBulbT();//回风冬季干球温度
            warmWetTemp = heatRecycleParam.getWNewWetBulbT();//回风冬季湿球温度
            coldTemp = heatRecycleParam.getWInDryBulbT();//新风冬季干球温度
            warmRT = heatRecycleParam.getWNewRelativeT();//回风夏季相对湿度
            coldRT = heatRecycleParam.getWInRelativeT();//回风夏季相对湿度
        }
        String unitModel = heatRecycleParam.getSerial();//机组型号
        double width=1000;
        double hight=1700;
       //TODO
        double suplyVelface = SystemCountUtil.plateVelFaceCal(supplyAirVolume, width,hight, EngineConstant.SYS_STRING_NUMBER_1);
        if(suplyVelface > 6){
        	throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        
        double returnVelface = SystemCountUtil.plateVelFaceCal(returnAirVolumn, width,hight, EngineConstant.SYS_STRING_NUMBER_1);
        if(returnVelface > 6){
        	throw new ApiException(ErrorCode.RECYCLE_AVOLUME_SOBIG);
        }
        byte s1 = 0;
        byte s2 = 0;
        byte z1 = 0;
        byte z2 = 0;
        String key = "H1000/6.0/E";

        Pointer in = new Memory(21 * Double.SIZE);
        in.setDouble(0 * Native.getNativeSize(Double.TYPE), 0.0);//固定
        in.setDouble(1 * Native.getNativeSize(Double.TYPE), suplyVelface);
        in.setDouble(2 * Native.getNativeSize(Double.TYPE),  0.0);
        AirConditionBean rAirConditionBean = AirConditionUtils.FAirParmCalculate1(warmTemp, warmWetTemp);
        in.setDouble(3 * Native.getNativeSize(Double.TYPE),  warmTemp);
        in.setDouble(4 * Native.getNativeSize(Double.TYPE),   rAirConditionBean.getParamD()/1000);
        in.setDouble(5 * Native.getNativeSize(Double.TYPE), returnVelface);
        in.setDouble(6 * Native.getNativeSize(Double.TYPE),  (0.0));
        AirConditionBean nAirConditionBean = AirConditionUtils.FAirParmCalculate1(coldTemp, coldWetTemp);
        in.setDouble(7 * Native.getNativeSize(Double.TYPE),  coldTemp);
        in.setDouble(8 * Native.getNativeSize(Double.TYPE), nAirConditionBean.getParamD()/1000);
        in.setDouble(9 * Native.getNativeSize(Double.TYPE),  width/1000);
        in.setDouble(10 * Native.getNativeSize(Double.TYPE), 1);
        in.setDouble(11 * Native.getNativeSize(Double.TYPE),  0);
        in.setDouble(12 * Native.getNativeSize(Double.TYPE),  0);
        in.setDouble(13 * Native.getNativeSize(Double.TYPE), 101325);
        in.setDouble(14 * Native.getNativeSize(Double.TYPE),  0);//固定
        in.setDouble(15 * Native.getNativeSize(Double.TYPE), 0);//固定
        in.setDouble(16 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(17 * Native.getNativeSize(Double.TYPE), 0);//固定
        in.setDouble(18 * Native.getNativeSize(Double.TYPE), 0);//固定
        in.setDouble(19 * Native.getNativeSize(Double.TYPE), 0);//固定
        in.setDouble(20 * Native.getNativeSize(Double.TYPE), 0);//固定
        Pointer out = new Memory(51 * Double.SIZE);
        HeaterxLib.HEATERX_INSTANCE.GET_CALCULATION(s1, s2, in, key, z1, z2, out);
        in.clear(21 * Double.SIZE);
        //TODO DLL参数返回值待确认
//        for(int i =1;i<51;i++) {
//        	System.out.println(i+"-->");
//        	System.out.println(out.getDouble(i * Native.getNativeSize(Double.TYPE)));
//        }
        Double sdbt = out.getDouble(23 * Native.getNativeSize(Double.TYPE));
        Double swbt = out.getDouble(24 * Native.getNativeSize(Double.TYPE));
        AirConditionBean airConditionBeanS = null;
        try {
            airConditionBeanS = AirConditionUtils.FAirParmCalculate2(sdbt, swbt * 1000);
        } catch (TempCalErrorException e) {
            LOGGER.error("HeatRecycleHeatXDllFactory getHeatXDll Calculate FAirParmCalculate2 S Error", e.getMessage());
        }
        double supplyAirD = sdbt;//干球温度送风侧
        double supplyAirWB = 0.00;//湿球温度送风侧
        double supplyAirRH = 0.00;//相对湿度送风侧
        if (null != airConditionBeanS) {
            supplyAirWB = airConditionBeanS.getParmTb();
            supplyAirRH = airConditionBeanS.getParmF();
        }
        Double supplyAirPD = Math.abs(out.getDouble(22 * Native.getNativeSize(Double.TYPE)));//空气阻力送风侧
        Double supplyAirTotalEff = out.getDouble(47 * Native.getNativeSize(Double.TYPE))/100;//效率送风侧

        AirConditionBean airConditionBeanE = null;
        Double edbt = out.getDouble(27 * Native.getNativeSize(Double.TYPE));
        Double ewbt = out.getDouble(28 * Native.getNativeSize(Double.TYPE));
        Double exhaustAirDB = edbt;//干球温度进风侧
        double exhaustAirWB = 0.00;//湿球温度进风侧
        double exhaustAirRH = 0.00;//相对湿度进风侧
        try {
            airConditionBeanE = AirConditionUtils.FAirParmCalculate2(edbt, ewbt*100);
        } catch (TempCalErrorException e) {
            LOGGER.error("HeatRecycleHeatXDllFactory getHeatXDll Calculate FAirParmCalculate2 E Error", e.getMessage());
        }
        if (null != airConditionBeanE) {
            exhaustAirWB = airConditionBeanE.getParmTb();//湿球温度进风侧
            exhaustAirRH = airConditionBeanE.getParmF();//相对湿度进风侧
        }
        Double exhaustAirPD = Math.abs(out.getDouble(22 * Native.getNativeSize(Double.TYPE)));//空气阻力进风侧
        Double exhaustAirTotalEff = out.getDouble(47 * Native.getNativeSize(Double.TYPE))/100;//效率进风侧
        double totalCap = Math.abs(out.getDouble(29 * Native.getNativeSize(Double.TYPE)))/ 1000.0;//全热
        double sensible = Math.abs(out.getDouble(30 * Native.getNativeSize(Double.TYPE))) / 1000.0;//显热
/*        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);
        df.format(value);*/
//        HeatRecycleInfo heatRecycleInfo = HEAT_RECYCLE_HEAT_X_RESULT_FACTORY.packageHeatRecycleInfo("700*800", season,
//                supplyAirD, supplyAirWB, supplyAirRH, supplyAirPD, supplyAirTotalEff,
//                exhaustAirDB, exhaustAirWB, exhaustAirRH, exhaustAirPD, exhaustAirTotalEff, totalCap, sensible, 0, 0);
//        if(!EmptyUtil.isEmpty(heatRecycleInfo)){
//            return heatRecycleInfo;
//        }
        return null;
    }

	public List<List<HeatRecycleInfo>> getHeatXPlateDllAll2(HeatRecycleParam heatRecycleParam) {
		List<List<HeatRecycleInfo>> heatRecycleInfoGroups = Lists.newArrayList();

		List<HeatxPlate> heatxPlates = AhuMetadata.findList(HeatxPlate.class,
				AhuUtil.getUnitNo(heatRecycleParam.getSerial()));
        double returnSAirResistance = 0;
        double returnEAirResistance = 0;
		for (HeatxPlate heatxPlate : heatxPlates) {
			List<HeatRecycleInfo> heatRecycleInfoOfTwoSeasons = Lists.newArrayList();
			HeatRecycleInfo heatRecycleInfoSummer = this.getPlateHeatRecycleInfo(heatRecycleParam, heatxPlate, true);
			if (heatRecycleInfoSummer != null) {
                returnSAirResistance = Double.parseDouble(heatRecycleInfoSummer.getReturnSAirResistance());
                returnEAirResistance = Double.parseDouble(heatRecycleInfoSummer.getReturnEAirResistance());
                if(returnSAirResistance > 300 || returnEAirResistance > 300) {
                    continue;
                }
                if(getImportPlateModel(heatxPlate.getModel())){
                    heatRecycleInfoSummer.setIsImport(getIntlString(GET_IMPORT));
                }
				heatRecycleInfoOfTwoSeasons.add(heatRecycleInfoSummer);
			}

			if (heatRecycleParam.isEnableWinter()) {
				HeatRecycleInfo heatRecycleInfoWinter = this.getPlateHeatRecycleInfo(heatRecycleParam, heatxPlate,
						false);
				if (heatRecycleInfoWinter != null) {
                    returnSAirResistance = Double.parseDouble(heatRecycleInfoWinter.getReturnSAirResistance());
                    returnEAirResistance = Double.parseDouble(heatRecycleInfoWinter.getReturnEAirResistance());
                    if(returnSAirResistance > 300 || returnEAirResistance > 300) {
                        continue;
                    }
                    if(getImportPlateModel(heatxPlate.getModel())){
                        heatRecycleInfoWinter.setIsImport(getIntlString(GET_IMPORT));
                    }
					heatRecycleInfoOfTwoSeasons.add(heatRecycleInfoWinter);
				}
			}

			if (!heatRecycleInfoOfTwoSeasons.isEmpty()) {
				heatRecycleInfoGroups.add(heatRecycleInfoOfTwoSeasons);
			}
		}

		return heatRecycleInfoGroups;
	}

    /**
     * 判断型号是否为进口
     * @param plateModel
     * @return
     */
	private boolean getImportPlateModel(String plateModel){
	    boolean flag = false;
        List importModelList = new ArrayList();
        importModelList.add("HA0600-0800-030-2EOO-2-2-0-0800");
        importModelList.add("HA1200-1700-060-2EOO-2-3-0-1700");
        importModelList.add("HA0600-1000-030-2EOO-2-2-0-1000");
        importModelList.add("HA0750-1000-045-2EOO-2-0-0-1000");
        importModelList.add("HA0750-1100-045-2EOO-2-0-0-1100");
        importModelList.add("HA0750-1200-045-2EOO-2-2-0-1200"); 
        if(importModelList.indexOf(plateModel) != -1){
            flag = true;
        }
        
        return flag;
    }
    
    private HeatRecycleInfo getPlateHeatRecycleInfo(HeatRecycleParam heatRecycleParam, HeatxPlate heatxPlate,
            boolean isSummer) {
        
    	double warmTemp = heatRecycleParam.getSNewDryBulbT();// 回风夏季干球温度
        double warmWetTemp = heatRecycleParam.getSNewWetBulbT();// 回风夏季湿球温度
        double coldTemp = heatRecycleParam.getSInDryBulbT();// 新风夏季干球温度
        double coldWetTemp = heatRecycleParam.getSInWetBulbT();// 新风夏季干球温度
        if (!isSummer) {
            warmTemp = heatRecycleParam.getWInDryBulbT();// 回风冬季干球温度
            warmWetTemp = heatRecycleParam.getWInWetBulbT();// 回风冬季湿球温度
            coldTemp = heatRecycleParam.getWNewDryBulbT();// 新风冬季干球温度
            coldWetTemp = heatRecycleParam.getWNewWetBulbT();
        }
        double attitude = FAttitudeConvert(heatRecycleParam.getSeaLevel(), false);//海拔高度转换
        double coldDensity = 0;
        UnitSystemEnum unit = AHUContext.getUnitSystem();
        if(UnitSystemEnum.M==unit) {
        	coldDensity=101000/(287.05*(coldTemp+273.15));
        }else {
        	coldDensity=101000/(287.05*(coldTemp+273.15));
        }
        
        double warmDensity = 0;
        if(UnitSystemEnum.M==unit) {
        	warmDensity=101000/(287.05*(warmTemp+273.15));
        }else {
        	warmDensity=101000/(287.05*(warmTemp+273.15));
        }
        
        double warmVel=heatRecycleParam.getNAVolume()/3600*warmDensity;
        double coldVel=heatRecycleParam.getRAVolume()/3600*coldDensity;
        
        if (!isSummer) {
        	
        	 warmVel=heatRecycleParam.getRAVolume()/3600*warmDensity;
             coldVel=heatRecycleParam.getNAVolume()/3600*coldDensity;
        }

        Pointer in = new Memory(21 * Double.SIZE);
        in.setDouble(0 * Native.getNativeSize(Double.TYPE), 0.0);// 固定
        in.setDouble(1 * Native.getNativeSize(Double.TYPE), warmVel);
        in.setDouble(2 * Native.getNativeSize(Double.TYPE), 0.0);
        AirConditionBean rAirConditionBean = AirConditionUtils.FAirParmCalculate1(warmTemp, warmWetTemp);
        in.setDouble(3 * Native.getNativeSize(Double.TYPE), warmTemp);
        in.setDouble(4 * Native.getNativeSize(Double.TYPE), rAirConditionBean.getParamD() / 1000);
        in.setDouble(5 * Native.getNativeSize(Double.TYPE), coldVel);
        in.setDouble(6 * Native.getNativeSize(Double.TYPE), (0.0));
        AirConditionBean nAirConditionBean = AirConditionUtils.FAirParmCalculate1(coldTemp, coldWetTemp);
        in.setDouble(7 * Native.getNativeSize(Double.TYPE), coldTemp);
        in.setDouble(8 * Native.getNativeSize(Double.TYPE), nAirConditionBean.getParamD() / 1000);
        in.setDouble(9 * Native.getNativeSize(Double.TYPE), ((double)heatxPlate.getHeight()) / 1000);
        in.setDouble(10 * Native.getNativeSize(Double.TYPE), 1);
        in.setDouble(11 * Native.getNativeSize(Double.TYPE), 0);
//        in.setDouble(12 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(13 * Native.getNativeSize(Double.TYPE), 101325);
        in.setDouble(14 * Native.getNativeSize(Double.TYPE), 0);// 固定
        in.setDouble(15 * Native.getNativeSize(Double.TYPE), 0);// 固定
        in.setDouble(16 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(17 * Native.getNativeSize(Double.TYPE), 0);// 固定
//        in.setDouble(18 * Native.getNativeSize(Double.TYPE), 0);// 固定
//        in.setDouble(19 * Native.getNativeSize(Double.TYPE), 0);// 固定
//        in.setDouble(20 * Native.getNativeSize(Double.TYPE), 0);// 固定
        Pointer out = new Memory(52 * Double.SIZE);
        HeaterxLib.HEATERX_INSTANCE.GET_CALCULATION((byte) 0, (byte) 0, in, heatxPlate.getType(), (byte) 0, (byte) 0,
                out);
        in.clear(21 * Double.SIZE);
        // TODO DLL参数返回值待确认
        System.out.println(heatxPlate.getType());
        for (int i = 0; i < 52; i++) {
        	
            System.out.println(i + "-->");
            System.out.println(out.getDouble(i * Native.getNativeSize(Double.TYPE)));
        }
        
        Double errorCode=out.getDouble(0 * Native.getNativeSize(Double.TYPE));
        if(errorCode!=0) {
        	return null;
        }
        
        Double warmSdbt = out.getDouble(23 * Native.getNativeSize(Double.TYPE));
        Double warmSwbt = out.getDouble(24 * Native.getNativeSize(Double.TYPE));
        AirConditionBean warmAirConditionBeanS = null;
        try {
            warmAirConditionBeanS = AirConditionUtils.FAirParmCalculate2(warmSdbt, warmSwbt * 1000);
        } catch (TempCalErrorException e) {
            LOGGER.error("HeatRecycleHeatXDllFactory getHeatXDll Calculate FAirParmCalculate2 S Error", e.getMessage());
        }
        double warmAirDB = warmSdbt;// 干球温度送风侧
        double warmAirWB = 0.00;// 湿球温度送风侧
        double warmAirRH = 0.00;// 相对湿度送风侧
        if (null != warmAirConditionBeanS) {
            warmAirWB = warmAirConditionBeanS.getParmTb();
            warmAirRH = warmAirConditionBeanS.getParmF();
        }
        Double warmAirPD = Math.abs(out.getDouble(22 * Native.getNativeSize(Double.TYPE)));// 空气阻力送风侧
        Double warmAirTotalEff = out.getDouble(47 * Native.getNativeSize(Double.TYPE)) / 100;// 效率送风侧

        AirConditionBean coldAirConditionBeanE = null;
        Double coldEdbt = out.getDouble(27 * Native.getNativeSize(Double.TYPE));
        Double coldEwbt = out.getDouble(28 * Native.getNativeSize(Double.TYPE));
        Double coldAirDB = coldEdbt;// 干球温度进风侧
        double coldAirWB = 0.00;// 湿球温度进风侧
        double coldAirRH = 0.00;// 相对湿度进风侧
        try {
        	coldAirConditionBeanE = AirConditionUtils.FAirParmCalculate2(coldEdbt, coldEwbt * 1000);
        } catch (TempCalErrorException e) {
            LOGGER.error("HeatRecycleHeatXDllFactory getHeatXDll Calculate FAirParmCalculate2 E Error", e.getMessage());
        }
        if (null != coldAirConditionBeanE) {
            coldAirWB = coldAirConditionBeanE.getParmTb();// 湿球温度进风侧
            coldAirRH = coldAirConditionBeanE.getParmF();// 相对湿度进风侧
        }
        Double coldAirPD = Math.abs(out.getDouble(26 * Native.getNativeSize(Double.TYPE)));// 空气阻力进风侧
        
//        if(exhaustAirPD>300 || supplyAirPD>300) {
//        	return null;
//        }
        Double coldAirTotalEff = out.getDouble(47 * Native.getNativeSize(Double.TYPE)) / 100;// 效率进风侧
        double totalCap = Math.abs(out.getDouble(29 * Native.getNativeSize(Double.TYPE))) / 1000.0;// 全热
        double sensible = Math.abs(out.getDouble(30 * Native.getNativeSize(Double.TYPE))) / 1000.0;// 显热
        
        double supplyAirDB = 0.0;
        double supplyAirWB = 0.0;
        double supplyAirRH = 0.0;
        double supplyAirPD = 0.0;
        double supplyAirTotalEff = 0.0;
        
        double exhaustAirDB=0.0;
        double exhaustAirWB=0.0;
        double exhaustAirRH=0.0;
        double exhaustAirPD=0.0;
        double exhaustAirTotalEff=0.0;
        
        if(!isSummer) {
        	 supplyAirDB = coldAirDB;
             supplyAirWB = coldAirWB;
             supplyAirRH = coldAirRH;
             supplyAirPD = coldAirPD;
             supplyAirTotalEff = coldAirTotalEff;
            
             exhaustAirDB=warmAirDB;
             exhaustAirWB=warmAirWB;
             exhaustAirRH=warmAirRH;
             exhaustAirPD=warmAirPD;
             exhaustAirTotalEff=warmAirTotalEff;
        	
        }else {
        	supplyAirDB = warmAirDB;
            supplyAirWB = warmAirWB;
            supplyAirRH = warmAirRH;
            supplyAirPD = warmAirPD;
            supplyAirTotalEff = warmAirTotalEff;
           
            exhaustAirDB=coldAirDB;
            exhaustAirWB=coldAirWB;
            exhaustAirRH=coldAirRH;
            exhaustAirPD=coldAirPD;
            exhaustAirTotalEff=coldAirTotalEff;
        	
        }
        
        return HEAT_RECYCLE_HEAT_X_RESULT_FACTORY.packageHeatRecycleInfo(
                String.format("%d*%d", heatxPlate.getHeight(), heatxPlate.getWidth()),
                isSummer ? EngineConstant.JSON_AHU_SEASON_SUMMER : EngineConstant.JSON_AHU_SEASON_WINTER,
                supplyAirDB, supplyAirWB, supplyAirRH, supplyAirPD, supplyAirTotalEff,0.0,0.0, 
                exhaustAirDB, exhaustAirWB, exhaustAirRH, exhaustAirPD, exhaustAirTotalEff,0.0,0.0, totalCap, sensible,
                heatxPlate.getMaterailPrice(), heatxPlate.getLabourPrice(), heatxPlate.getType(),
                heatxPlate.getHeight(),heatxPlate.getWidth(), heatxPlate.getCorrugatedChannel());
    }

    private String getMaterial(int material) {
        switch (material) {
            case 0:
                return "Al";
            case 1:
                return "Epoxy";
            case 6:
                return "Hybrid";
            case 5:
                return "Silica";
            case 7:
                return "MS";
            case 8:
                return "MS+";
        }
        return "";
    }

    private double FAttitudeConvert(double ParmCF, boolean ParmFlag) {
        if (ParmFlag) {
            return ParmCF / 3.281;
        } else {
            return ParmCF * 3.281;
        }
    }
}

package com.carrier.ahu.engine.coilDx4d.factory;

import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.util.FormulaUtil;
import com.carrier.ahu.engine.coilWater4d.entity.CoilWaterDllInfo;
import com.carrier.ahu.engine.coilWater4d.param.CWDRuleParam;
import com.carrier.ahu.engine.constant.EngineConstant;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.MapSortUtil;
import com.carrier.ahu.util.EmptyUtil;

import java.util.*;

//封装计算总结果
public class DXCoilResultFactory {

    // 根据DLL返回结果封装页面返回参数
    public CoilInfo packageCountResult(List<CoilWaterDllInfo> coldWaterDLLResultList, CWDRuleParam cwdRuleParam) {

        return new CoilInfo();
    }

    // 根据DLL返回结果封装页面返回参数
    public List<CoilInfo> packageCountResult(List<CoilWaterDllInfo> coldWaterDLLResultList) {
        // 如果Dll返回为null 不参数数据转换
        if (EmptyUtil.isEmpty(coldWaterDLLResultList)) {
            return null;
        }
        List<CoilInfo> engineCalculationResultList = new ArrayList<>();
        // 根据排数、片距、回路 计算：冷量、显冷、水流量
        Map<String, String> map = new HashMap<String, String>();
        for (CoilWaterDllInfo coldWaterDLLResult : coldWaterDLLResultList) {
            String rows = BaseDataUtil.integerConversionString(coldWaterDLLResult.getRows());// 排数
            String fpi = BaseDataUtil.integerConversionString(coldWaterDLLResult.getFpi());// 片距
            String cir = coldWaterDLLResult.getCir();// 回路
            double total = coldWaterDLLResult.getTotal();// 冷量
            double sensible = coldWaterDLLResult.getSensible();// 显冷
            String key = rows + EngineConstant.SYS_PUNCTUATION_HYPHEN + fpi + EngineConstant.SYS_PUNCTUATION_HYPHEN + cir;
            if (map.containsKey(key)) {
                String mapValue = map.get(key);
                double mapTotal = BaseDataUtil.stringConversionDouble(mapValue.split(EngineConstant.SYS_PUNCTUATION_HYPHEN)[0]);
                double mapSensible = BaseDataUtil.stringConversionDouble(mapValue.split(EngineConstant.SYS_PUNCTUATION_HYPHEN)[1]);
                total += mapTotal;
                sensible += mapSensible;
            }
            map.put(key, total + EngineConstant.SYS_PUNCTUATION_HYPHEN + sensible);
        }
        Map<String, String> mapSort = MapSortUtil.sortMapByKey(map);// map排序
        // 根据map遍历排数、片距、回路 纬度的数据
        Iterator<Map.Entry<String, String>> iterator = mapSort.entrySet().iterator();
        while (iterator.hasNext()) {
            CoilInfo coilInfo = new CoilInfo();
            Map.Entry<String, String> entry = iterator.next();
            String mapKey = String.valueOf(entry.getKey());
            String mapValue = String.valueOf(entry.getValue());
            String rows = mapKey.split(EngineConstant.SYS_PUNCTUATION_HYPHEN)[0];// 排数
            String fpi = mapKey.split(EngineConstant.SYS_PUNCTUATION_HYPHEN)[1];// 片距
            String cir = mapKey.split(EngineConstant.SYS_PUNCTUATION_HYPHEN)[2];// 回路
            double mapTotal = BaseDataUtil.stringConversionDouble(mapValue.split(EngineConstant.SYS_PUNCTUATION_HYPHEN)[0]);
            double mapSensible = BaseDataUtil.stringConversionDouble(mapValue.split(EngineConstant.SYS_PUNCTUATION_HYPHEN)[1]);
            for (CoilWaterDllInfo coilWaterDllInfo : coldWaterDLLResultList) {
                // 排数\片距\回路 相同封装返回参数
                if (rows.equals(BaseDataUtil.integerConversionString(coilWaterDllInfo.getRows()))
                        && fpi.equals(BaseDataUtil.integerConversionString(coilWaterDllInfo.getFpi()))
                        && cir.equals(coilWaterDllInfo.getCir())) {
                    coilInfo.setRows(BaseDataUtil.stringConversionInteger(rows));// 排数
                    coilInfo.setFinDensity(fpi);// 片距
                    coilInfo.setCircuit(cir);// 回路
                    coilInfo.setArea(String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getFaceArea()));// 迎风面积
                    coilInfo.setInRelativeT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getEaRH()));// 进风相对湿度
                    coilInfo.setOutRelativeT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getLaRH()));// 出风相对湿度
                    coilInfo.setReturnBypass(String.format(EngineConstant.SYS_FORMAT_4F, coilWaterDllInfo.getBypass()));// Bypass
                    if ("s_imperial".equals(EngineConstant.SYS_STRING_NUMBER_0)) {// 需要确认
                        coilInfo.setInDryBulbT(String.format(EngineConstant.SYS_FORMAT_1F,
                                FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getEaDB(), true)));// 进风干球温度
                        coilInfo.setInWetBulbT(String.format(EngineConstant.SYS_FORMAT_1F,
                                FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getEaWB(), true)));// 进风湿球温度
                        coilInfo.setOutDryBulbT(String.format(EngineConstant.SYS_FORMAT_1F,
                                FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getLaDB(), true)));// 出风干球温度
                        coilInfo.setOutWetBulbT(String.format(EngineConstant.SYS_FORMAT_1F,
                                FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getLaWB(), true)));// 出风湿球温度
                        coilInfo
                                .setAirResistance(String.format(EngineConstant.SYS_FORMAT_3F, coilWaterDllInfo.getApd() / 249));// 空气阻力
                        coilInfo
                                .setWaterResistance(String.format(EngineConstant.SYS_FORMAT_3F, coilWaterDllInfo.getWpd() / 2.99));// 水阻力
                        coilInfo
                                .setReturnWaterFlow(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getFlow() * 4.40));// 水流量
                        coilInfo.setReturnEnteringFluidTemperature(String.format(EngineConstant.SYS_FORMAT_1F,
                                FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getEwt(), true)));// 进水温度
                        coilInfo.setReturnWTAScend(String.format(EngineConstant.SYS_FORMAT_1F,
                                FormulaUtil.fTemperatureConvert(coilWaterDllInfo.getTr(), true)
                                        - FormulaUtil.fTemperatureConvert(
                                        coilWaterDllInfo.getEwt() - coilWaterDllInfo.getTr(), true)));// 水温升
                        coilInfo
                                .setReturnColdQ(coilWaterDllInfo.getTotal() * 3414);// 冷量
                        coilInfo.setReturnSensibleCapacity(
                                String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getSensible() * 3414));// 显冷
                        coilInfo
                                .setVelocity(String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getVelocity() / 0.00508));// 风面速
                        if (("0608").equals(coilWaterDllInfo.getUniType())) {// 机组型号
                            coilInfo.setFluidvelocity(String.format(EngineConstant.SYS_FORMAT_2F, 0.03 * 3.28));// 介质流速
                        } else {
                            coilInfo.setFluidvelocity(
                                    String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getFluidVelocity() * 3.28));// 介质流速
                        }
                    } else {
                        coilInfo.setInDryBulbT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getEaDB()));// 进风干球温度
                        coilInfo.setInWetBulbT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getEaWB()));// 进风湿球温度
                        coilInfo.setOutDryBulbT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getLaDB()));// 出风干球温度
                        coilInfo.setOutWetBulbT(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getLaWB()));// 出风湿球温度
                        coilInfo.setAirResistance(String.format(EngineConstant.SYS_FORMAT_1F, coilWaterDllInfo.getApd() * 9.8));// 空气阻力
                        coilInfo.setReturnColdQ(mapTotal / 1000);// 冷量
                        coilInfo.setReturnSensibleCapacity(String.format(EngineConstant.SYS_FORMAT_2F, mapSensible / 1000));// 显冷
                        coilInfo.setVelocity(String.format(EngineConstant.SYS_FORMAT_2F, coilWaterDllInfo.getVelocity()));// 面风速
                        coilInfo.setReturnCond(null == coilWaterDllInfo.getCond() ? EngineConstant.SYS_BLANK : coilWaterDllInfo.getCond());
                        coilInfo.setReturnSatCond(null == coilWaterDllInfo.getSatCond() ? EngineConstant.SYS_BLANK : coilWaterDllInfo.getSatCond());////饱和冷凝温度
                        coilInfo.setUnit(null == coilWaterDllInfo.getCDU() ? EngineConstant.SYS_BLANK : coilWaterDllInfo.getCDU());
                        coilInfo.setReturnSST("9.3");//饱和吸气温度
                    }
                    if (mapTotal > 0) {
                        engineCalculationResultList.add(coilInfo);
                    }
                    break;
                }
            }
        }
        return engineCalculationResultList;
    }
}

package com.carrier.ahu.engine.fanKruger.krugerI;

import cn.hutool.core.util.XmlUtil;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.*;
import com.carrier.ahu.util.EmptyUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.logging.Logger;

import static com.carrier.ahu.constant.CommonConstant.*;

/**
 * Created by Braden Zhou on 2019/02/20.
 */
@Service
public class KrugerEngineServiceImpl implements KrugerEngineService {
    static Logger logger = Logger.getLogger("KrugerEngineServiceImpl");


    private RestTemplate restTemplate = new RestTemplate();
    public static final String SYS_REST_KRUGER_URL_PORT_XML = "RestAPI/RestAPIHost.exe.config";
    private static String krugerServiceUrl = SYS_REST_KRUGER_URL;

    static {
        try {
            String server = "";//http://localhost
            String port = "";//60064
            Document docResult=XmlUtil.readXML(SYS_REST_KRUGER_URL_PORT_XML);
            Element appconfig = XmlUtil.getElementByXPath("//configuration/appSettings", docResult);
            NodeList addConfs = appconfig.getElementsByTagName("add");
            for (int i = 0; i < addConfs.getLength(); i++) {
                Node item = addConfs.item(i);
                if(item.getAttributes().getNamedItem("key").getTextContent().equals("BaseUrl")){
                    server = item.getAttributes().getNamedItem("value").getTextContent();
                }
                if(item.getAttributes().getNamedItem("key").getTextContent().equals("Port")){
                    port = item.getAttributes().getNamedItem("value").getTextContent();
                }
            }
            if(EmptyUtil.isNotEmpty(server) && EmptyUtil.isNotEmpty(port)){
                krugerServiceUrl = server+":"+port+"/api/";
            }

        } catch (Exception e) {
            logger.info("restapi conf xml error,"+e.getMessage());
            krugerServiceUrl = SYS_REST_KRUGER_URL;
        }
    }

    @Override
    public List<KrugerSelection> select(KrugerSelectInfo krugerSelectInfo) throws FanEngineException {
        try {
            HttpEntity<KrugerSelectInfo> krugerEntity = new HttpEntity<KrugerSelectInfo>(krugerSelectInfo);
            String response = restTemplate.postForObject(krugerServiceUrl + SYS_REST_KRUGER_SELECT,
                    krugerEntity, String.class);
            return new Gson().fromJson(response, new TypeToken<List<KrugerSelection>>() {
            }.getType());
        } catch (Exception exp) {
            throw new FanEngineException(ErrorCode.KRUGER_SELECT_FAILED, exp);
        }
    }

    @Override
    public KrugerCurvePoints curvePoints(KrugerSelection krugerSelection) throws FanEngineException {
        try {
            HttpEntity<KrugerSelection> krugerEntity = new HttpEntity<KrugerSelection>(krugerSelection);
            String response = restTemplate.postForObject(
                    krugerServiceUrl + SYS_REST_KRUGER_CURVE_POINTS, krugerEntity, String.class);
            return new Gson().fromJson(response, new TypeToken<KrugerCurvePoints>() {
            }.getType());
        } catch (Exception exp) {
            throw new FanEngineException(ErrorCode.KRUGER_CURVE_POINTS_FAILED, exp);
        }
    }

    @Override
    public KrugerSoundSpectrumEx soundSpectrumEx(KrugerSelection krugerSelection) throws FanEngineException {
        try {
            HttpEntity<KrugerSelection> krugerEntity = new HttpEntity<KrugerSelection>(krugerSelection);
            String response = restTemplate.postForObject(
                    krugerServiceUrl + SYS_REST_KRUGER_SOUND_SPECTRUM_EX, krugerEntity, String.class);
            return new Gson().fromJson(response, new TypeToken<KrugerSoundSpectrumEx>() {
            }.getType());
        } catch (Exception exp) {
            throw new FanEngineException(ErrorCode.KRUGER_SOUND_SPECTRUM_EX_FAILED, exp);
        }
    }

    @Override
    public KrugerChartResult generateChart(KrugerChartInfo krugerChartInfo) throws FanEngineException {
        try {
            HttpEntity<KrugerChartInfo> krugerChartEntity = new HttpEntity<KrugerChartInfo>(krugerChartInfo);
            String response = restTemplate.postForObject(
                    krugerServiceUrl + SYS_REST_KRUGER_GENERATE_CHART, krugerChartEntity, String.class);
            KrugerChartResult krugerChartResult = new KrugerChartResult();
            krugerChartResult.setSuccess(Boolean.valueOf(response));
            if (krugerChartResult.isSuccess()) {
                krugerChartResult.setChartFilePath(krugerChartInfo.getChartPath() + krugerChartInfo.getChartName()
                        + SYS_BMP_EXTENSION);
            }
            return krugerChartResult;
        } catch (Exception exp) {
            throw new FanEngineException(ErrorCode.KRUGER_GENERATE_CHART_FAILED, exp);
        }
    }

}

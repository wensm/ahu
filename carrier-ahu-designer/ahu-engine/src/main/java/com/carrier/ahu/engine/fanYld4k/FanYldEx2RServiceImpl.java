package com.carrier.ahu.engine.fanYld4k;

import java.util.List;

import com.carrier.ahu.engine.fan.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.factory.AFanEngine;
import com.carrier.ahu.engine.fanYld4k.entity.FanYldDllInfo;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllEx3Factory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllInEx2KParamFactory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllInEx2RParamFactory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldResultFactory;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.FanYldDllInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.util.DateUtil;
import com.carrier.ahu.util.EmptyUtil;

public class FanYldEx2RServiceImpl extends AFanEngine {

    private static final Logger LOGGER = LoggerFactory.getLogger(FanYldEx2RServiceImpl.class.getName());
    private static final FanYldDllInEx2RParamFactory fanYldDllInEx2RParamFactory = new FanYldDllInEx2RParamFactory();
    private static final FanYldDllInEx2KParamFactory fanYldDllInEx2KParamFactory = new FanYldDllInEx2KParamFactory();
    private static final FanYldDllEx3Factory fanYldDllEx3Factory = new FanYldDllEx3Factory();
    private static final FanYldResultFactory fanYldResultFactory = new FanYldResultFactory();

    @Override
    public List<FanInfo> cal(FanInParam fanInParam) throws FanEngineException {
        LOGGER.info("YLDYLDYLD FanYldEx2RServiceImpl YLD Engine Begin "+ DateUtil.getStringDate());
		/* 封装YLDDll参数 */
        FanYldDllInParam fanYldDllInParam = fanYldDllInEx2RParamFactory.packageParam(fanInParam);
        LOGGER.info("YLDYLDYLD FanYldEx2RServiceImpl YLD FanYldDllInParam "+ DateUtil.getStringDate());
		/* 根据加工参数调用Dll */
        List<FanYldDllInfo> fanYldDllInfoList = fanYldDllEx3Factory.fanSelectionEx2(fanYldDllInParam);
        LOGGER.info("YLDYLDYLD FanYldEx2RServiceImpl YLD fanYldDllInfoList "+ DateUtil.getStringDate());
		/* 根据Dll返回结果封装数据 */
        List<FanInfo> fanInfoList = fanYldResultFactory.packageCountResultEx2(fanInParam,this.getFanConditions(),fanYldDllInfoList,null);
        LOGGER.info("YLDYLDYLD FanYldEx2RServiceImpl YLD Engine end "+ DateUtil.getStringDate());
        //TODO 39CQ2226 当逻辑调用2R或者2K时候无法计算出结果重新调用3
        //无法计算出结果使用2K计算
        fanYldDllInParam = fanYldDllInEx2KParamFactory.packageParam(fanInParam);
        fanYldDllInfoList = fanYldDllEx3Factory.fanSelectionEx2(fanYldDllInParam);
        if(EmptyUtil.isEmpty(fanInfoList)){
            fanInfoList = fanYldResultFactory.packageCountResultEx2(fanInParam,this.getFanConditions(),fanYldDllInfoList,null);
        }else{
            List<FanInfo> fanInfoListK = fanYldResultFactory.packageCountResultEx2(fanInParam,this.getFanConditions(),fanYldDllInfoList,null);
            fanInfoList = Utils.packageFanInfoList(fanInfoList,fanInfoListK);
        }
        return fanInfoList;
    }

    @Override
    public FanInfo calByRule(FanInParam windMachineInParam, PublicRuleParam publicRuleParam)
            throws FanEngineException {
        // TODO Auto-generated method stub
        return null;
    }

}

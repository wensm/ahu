package com.carrier.ahu.engine.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.enums.UserConfigEnum;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;

public class UserConfigUtil {

    /*风机用户自定义过滤*/
    public static List<FanInfo> packageUserConfig(FanInParam fanInParam,List<FanInfo> fanInfoList) {
        Map<String, Object> mapConfig = fanInParam.getMapConfig();
        if (!EmptyUtil.isEmpty(mapConfig)) {
            List<FanInfo> fanInfos = new ArrayList<FanInfo>();
            String maximumFanOutletVelocityKey = UserConfigEnum.MAXIMUM_FAN_OUTLET_VELOCITY.getCode();
            String minimumEfficiencyOfFanKey = UserConfigEnum.MINIMUM_EFFICIENCY_OF_FAN.getCode();
            for (FanInfo fanInfo : fanInfoList) {
                if (mapConfig.containsKey(maximumFanOutletVelocityKey)) {
                    String value = mapConfig.get(maximumFanOutletVelocityKey).toString();
                    if (!EmptyUtil.isEmpty(value)) {
                        if (BaseDataUtil.stringConversionDouble(fanInfo.getOutletVelocity()) > BaseDataUtil.stringConversionDouble(value)) {
                            continue;
                        }
                    }
                }
                if (mapConfig.containsKey(minimumEfficiencyOfFanKey)) {
                    String value = mapConfig.get(minimumEfficiencyOfFanKey).toString();
                    if (!EmptyUtil.isEmpty(value)) {
                        if (BaseDataUtil.stringConversionDouble(fanInfo.getEfficiency()) < BaseDataUtil.stringConversionDouble(value)) {
                            continue;
                        }
                    }
                }
                fanInfos.add(fanInfo);
            }
            return fanInfos;
        } else {
            return fanInfoList;
        }
    }


}

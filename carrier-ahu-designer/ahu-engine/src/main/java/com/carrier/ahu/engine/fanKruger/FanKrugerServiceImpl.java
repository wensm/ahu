package com.carrier.ahu.engine.fanKruger;

import com.carrier.ahu.common.exception.engine.FanEngineException;
import com.carrier.ahu.engine.fan.KrugerSelectInfo;
import com.carrier.ahu.engine.fan.KrugerSelection;
import com.carrier.ahu.engine.fan.entity.FanInfo;
import com.carrier.ahu.engine.fan.factory.AFanEngine;
import com.carrier.ahu.engine.fanKruger.factory.FanKrugerDllCalFactory;
import com.carrier.ahu.engine.fanKruger.factory.FanKrugerDllInFactory;
import com.carrier.ahu.engine.fanKruger.factory.FanKrugerResultFactory;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.PublicRuleParam;
import com.carrier.ahu.util.EmptyUtil;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FanKrugerServiceImpl extends AFanEngine {

    private static final FanKrugerDllInFactory FAN_KRUGER_DLL_IN_FACTORY = new FanKrugerDllInFactory();
    private static final FanKrugerDllCalFactory FAN_KRUGER_DLL_CAL_FACTORY = new FanKrugerDllCalFactory();
    private static final FanKrugerResultFactory FAN_KRUGER_RESULT_FACTORY = new FanKrugerResultFactory();


    @Override
    public List<FanInfo> cal(FanInParam fanInParam) throws FanEngineException {
        /*根据fanInParam参数封装入参*/
        KrugerSelectInfo krugerSelectInfo = FAN_KRUGER_DLL_IN_FACTORY.packageKrugerInParam(fanInParam);
        /*根据krugerSelectInfo调用引擎*/
        List<KrugerSelection> krugerSelectionList = FAN_KRUGER_DLL_CAL_FACTORY.calculation(krugerSelectInfo,fanInParam);
        /*根据krugerSelectionList封装页面结果*/
        List<FanInfo> fanInfoList = FAN_KRUGER_RESULT_FACTORY.packageResult(krugerSelectionList,fanInParam);
        return fanInfoList;
    }

    @Override
    public FanInfo calByRule(FanInParam fanInParam, PublicRuleParam publicRuleParam) throws FanEngineException {
        List<FanInfo> fanInfoList = this.cal(fanInParam);
        if (!EmptyUtil.isEmpty(fanInfoList)) {
            return fanInfoList.get(0);
        }
        return null;
    }
}

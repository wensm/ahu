package com.carrier.ahu.engine.fanKruger;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 获取系统环境变量
 */
@Component
public class KrugerSpringContextUtil implements ApplicationContextAware {
    private static ApplicationContext context = null;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        this.context = applicationContext;
    }
    public static String getKrugerServiceUrl() {
        String profilesStr = context.getEnvironment().getProperty("kruger.service.url");
        return profilesStr;
    }
}
package com.carrier.ahu.engine.fanYld4k.factory;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.FanYldDllInParam;

//DLL接口入参封装
public class FanYldDllInEx2KParamFactory {

    private Logger logger = LoggerFactory.getLogger(FanYldDllInEx2KParamFactory.class.getName());

    //亿利达DLLParam封装
    public FanYldDllInParam packageParam(FanInParam fanInParam) {
        String fanType = fanInParam.getSerial();//机组型号
        double airVolume = 0.0;
        if (AirDirectionEnum.RETURNAIR.getCode().equals(fanInParam.getAirDirection())) {//回风
            airVolume = fanInParam.getEairvolume();//送风风量
        } else {
            airVolume = fanInParam.getSairvolume();//回风风量
        }
        FanYldDllInParam fanYldDllInParam = packageSYDKParam(airVolume, fanInParam.getTotalStatic(), fanType, fanInParam.getAltitude());
        fanYldDllInParam.setType(fanInParam.getType());
        fanYldDllInParam.setSupplier(fanInParam.getSupplier());
        fanYldDllInParam.setProjectId(fanInParam.getProjectId());
        return fanYldDllInParam;
    }

    //封装WMKWindMachineOutParam SYD - K
    public FanYldDllInParam packageSYDKParam(double pFlow, double pPres, String fanType, double altitude) {
        FanYldDllInParam fanYldDllInParam = new FanYldDllInParam();
        fanYldDllInParam.setPFlow(pFlow);//风量(m3/h)--------------------------页面提供
        fanYldDllInParam.setPPres(pPres);//风压（Pa）---------------------------页面提供
        fanYldDllInParam.setPAirDensity("");//空气密度 ---------------------------------根据海拔高度调用DLL文件获取
        fanYldDllInParam.setFanType(fanType);
        fanYldDllInParam.setStatus("4");
        fanYldDllInParam.setPUserSetMotorSafeCoff(false);//用户设定了电机容量安全系数true-是 false-否
        fanYldDllInParam.setPSeriesNames("SYD");//系列名称（为空时不限制），用分号相隔如SYQ或SYD;SYQ
        fanYldDllInParam.setPSubTypeNames("K");//子系列名称（为空时不限制），用分号相隔如K或K;K2
        fanYldDllInParam.setPPresType("1");//风压类型0-全压1-静压
        fanYldDllInParam.setPAirDesityType("1");//空气密度类型0-自定义1-直接输入空气密度
        fanYldDllInParam.setPB("120000");//大气压力
        fanYldDllInParam.setPT("25");//空气温度
        fanYldDllInParam.setPV("50");//相对湿度
        fanYldDllInParam.setPMotorSafeCoff("10");//电机容量安全系数(%)
        fanYldDllInParam.setAltitude(altitude);//海拔高度 老系统0-8000米
        return fanYldDllInParam;
    }
}

package com.carrier.ahu.engine.fanYld4k.param;

import lombok.Data;

@Data
public class FanYldDllInParam {

    //========================调用风机DLL参数=================//
    private String pSeriesNames;//系列名称（为空时不限制），用分号相隔如SYQ或SYD;SYQ
    private String pSubTypeNames;//子系列名称（为空时不限制），用分号相隔如K或K;K2
    private double pFlow;//风量(m3/h)
    private double pPres;//风压（Pa）
    private String pPresType;//风压类型 0-全压1-静压
    private String pOutFanType;//出风方式 0-管道出风1-自由出风
    private String pAirDesityType = "1";//空气密度类型 0-自定义1-直接输入空气密度
    private String pAirDensity = "0";//空气密度
    private String pB;//大气压力
    private String pT;//空气温度
    private String pV;//相对湿度
    private String pMotorSafeCoff;//电机容量安全系数(%)
    private Boolean pUserSetMotorSafeCoff;//用户设定了电机容量安全系数true-是 false-否
    private String status;//状态 1：FanSelection 2：FanSelectionEx2 3：FanSelectionEx3 4:FanSelectionEx2K
    private String fanType;//机组型号 39G0608
    private double altitude;//海拔高度 老系统0-8000米
    private String type;//电机类型 1:二级能效电机、2:三级能效电机、3:防爆电机、4:双速电机、5:变频电机、6:变频电机(直连)
    private String supplier;//电机品牌 default：默认、Dongyuan：东元、ABB：ABB、Simens：西门子
    private String projectId;
}

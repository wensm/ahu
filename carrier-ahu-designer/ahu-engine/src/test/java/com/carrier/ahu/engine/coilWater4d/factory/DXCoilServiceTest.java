package com.carrier.ahu.engine.coilWater4d.factory;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.coilWater4d.entity.CoilWaterDllInfo;
import com.carrier.ahu.engine.coilWater4d.param.CWDCPCParam;
import com.carrier.ahu.engine.coilWater4d.param.CoilDllInParam;
import com.carrier.ahu.engine.dxcoil.DXCoilResult;
import com.carrier.ahu.engine.watercoil.WaterCoilEngine;
import com.carrier.ahu.engine.watercoil.WaterCoilResult;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by liangd4 on 2017/9/7.
 */
public class DXCoilServiceTest implements SystemCalculateConstants {
    @Test
    public void DX_test() throws InterruptedException {
        CoilInParam cWDColdWaterInParam = new CoilInParam();
        cWDColdWaterInParam.setCoilType(DXCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        cWDColdWaterInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_1_2);//管径 1:四分管、2:三分管
        cWDColdWaterInParam.setRows(COOLINGCOIL_ROWS_6);//排数 -1、2、3、4、5、6、7、8、10、12
        cWDColdWaterInParam.setCircuit(COOLINGCOIL_CIRCUIT_FL);//回路 AUTO、HF半回路、FL全回路、DB双回路
        cWDColdWaterInParam.setFinDensity(COOLINGCOIL_FINDENSITY_10);//片距 -1、10FPI、12FPI、14FPI
        cWDColdWaterInParam.setSeason("S");//季节 W:冬、S:夏、ALL:冬夏
//        cWDColdWaterInParam.setArea(1.68);//迎风面积
//        cWDColdWaterInParam.setVelocity(2.97);//面风速
        cWDColdWaterInParam.setSerial("39CQ1317");//机组型号
        cWDColdWaterInParam.setCalculateOptimalResults(true);//计算最优

        cWDColdWaterInParam.setConcentration(1);//水温升
        cWDColdWaterInParam.setWTAScend(10.0);//水温升值
        cWDColdWaterInParam.setEnteringFluidTemperature(60.0);//进水温度
        cWDColdWaterInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        cWDColdWaterInParam.setInDryBulbT(27.0);//进风干球温度 小数精度限制、页面暂时没有限制
        cWDColdWaterInParam.setInWetBulbT(19.5);//进风湿球温度
        cWDColdWaterInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        cWDColdWaterInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        cWDColdWaterInParam.setConcentration(0);//浓度
        cWDColdWaterInParam.setQualifiedConditions("4");//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        cWDColdWaterInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        cWDColdWaterInParam.setSbalanceMethodUnit("");//夏 限定计量单位 暂时无使用
        cWDColdWaterInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        cWDColdWaterInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        cWDColdWaterInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        cWDColdWaterInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        cWDColdWaterInParam.setCountValue("");//计算条件对应值 暂时无使用
        cWDColdWaterInParam.setWaterFlow(0.0);//水流量值
        cWDColdWaterInParam.setLanguage(0);//语言 0:中文 1：英文
        cWDColdWaterInParam.setAltitude(0.0);//海拔高度
        cWDColdWaterInParam.setMaxWPD(89.0);//最大水阻
//        cWDColdWaterInParam.setAhri(true);//true:欧标 false:非欧标
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(cWDColdWaterInParam);
        inParamPrint(cWDColdWaterInParam);//入参打印

        CoilWaterDllFactory coilWaterDllFactory = new CoilWaterDllFactory();
        List<CoilWaterDllInfo> coldWaterDLLResultList = coilWaterDllFactory.getDLLResultList(waterOutParamList);
//        factoryCll(coldWaterDLLResultList);




        /*DXCoilResult r = DXCoilEngine.DX_calculate();
        enginePrint(r);//引擎结果打印*/
    }

    //DLL调用
    private List<CoilWaterDllInfo> engineCll(CoilInParam cWDColdWaterInParam, List<CoilDllInParam> waterOutParamList) throws InterruptedException {
        //================================DLL调用begin===============================
        List<CoilWaterDllInfo> coldWaterDLLResultList = new ArrayList<CoilWaterDllInfo>();
        for (CoilDllInParam coilDllInParam : waterOutParamList) {
            List<CWDCPCParam> cPCParamList = coilDllInParam.getCPCParamList();
            for (CWDCPCParam cwdcpcParam : cPCParamList) {
                outParamPrint(coilDllInParam, cwdcpcParam);//入参加工打印
                WaterCoilResult r = WaterCoilEngine.calculate(cwdcpcParam.getStrCpc(), coilDllInParam.getVFace(), coilDllInParam.getTdbAirIn(),
                        coilDllInParam.getTwbAirIn(), coilDllInParam.getPAirIn(), coilDllInParam.getFFouling(), coilDllInParam.getSurfEff(),
                        coilDllInParam.getRADMULT(), coilDllInParam.getDPDMULT(), coilDllInParam.getRAWMULT(), coilDllInParam.getDPWMULT(),
                        cwdcpcParam.getLBTS(), coilDllInParam.getODEXP(), coilDllInParam.getTW(), coilDllInParam.getIdBend(), coilDllInParam.getIdHairpin(),
                        coilDllInParam.getPT(), coilDllInParam.getPR(), coilDllInParam.getTCond(), Math.toIntExact(coilDllInParam.getCurveNum()),
                        coilDllInParam.getFThick(), coilDllInParam.getFCond(), coilDllInParam.getFPI(), coilDllInParam.getTWaterIn(),
                        coilDllInParam.getDtWater(), cwdcpcParam.getqWaterIn(), Math.toIntExact(coilDllInParam.getFluidId()), coilDllInParam.getFluidConcentration(),
                        coilDllInParam.getHeight());
//                coldWaterDLLResultList.add(packageCWDColdWaterDllResult(coilDllInParam, cwdcpcParam, r, cWDColdWaterInParam));
//                enginePrint(r);//引擎结果打印
            }
        }
        //================================DLL调用end===============================
        return coldWaterDLLResultList;
    }

    //outParamPrint
    private void outParamPrint(CoilDllInParam coilDllInParam, CWDCPCParam cwdcpcParam) {
//        System.out.println("=================outParamPrint begin=====================");
        System.out.println("排数：" + cwdcpcParam.gettRow() + "片距：" + coilDllInParam.getFPI() + "路径：" + cwdcpcParam.getStrCpc());
        System.out.print("【Vface】" + coilDllInParam.getVFace());
        System.out.print("【Tdb_air_in】" + coilDllInParam.getTdbAirIn());
        System.out.print("【Twb_air_in】" + coilDllInParam.getTwbAirIn());
        System.out.print("【P_air_in】" + coilDllInParam.getPAirIn());
        System.out.print("【F_fouling】" + coilDllInParam.getFFouling());
        System.out.print("【Surf_eff】" + coilDllInParam.getSurfEff());
        System.out.print("【RADMULT】" + coilDllInParam.getRADMULT());
        System.out.print("【DPDMULT】" + coilDllInParam.getDPDMULT());
        System.out.print("【RAWMULT】" + coilDllInParam.getRAWMULT());
        System.out.print("【DPWMULT】" + coilDllInParam.getDPWMULT());
        System.out.print("【LBTS】" + cwdcpcParam.getLBTS());
        System.out.print("【ODEXP】" + coilDllInParam.getODEXP());
        System.out.println("【TW】" + coilDllInParam.getTW());
        System.out.print("【IDbend】" + coilDllInParam.getIdBend());
        System.out.print("【IDhairpin】" + coilDllInParam.getIdHairpin());
        System.out.print("【PT】" + coilDllInParam.getPT());
        System.out.print("【PR】" + coilDllInParam.getPR());
        System.out.print("【Tcond】" + coilDllInParam.getTCond());
        System.out.print("【CurveNum】" + coilDllInParam.getCurveNum());
        System.out.print("【Fthick】" + coilDllInParam.getFThick());
        System.out.print("【Fcond】" + coilDllInParam.getFCond());
        System.out.print("【FPI】" + coilDllInParam.getFPI());
        System.out.print("【T_water_in】" + coilDllInParam.getTWaterIn());
        System.out.print("【DT_water】" + coilDllInParam.getDtWater());
        System.out.print("【Q_water_in】" + cwdcpcParam.getqWaterIn());
        System.out.println("【Fluid_ID】" + coilDllInParam.getFluidId());
//        System.out.println("=================outParamPrint end=======================");

    }

    //入参参数打印
    private void inParamPrint(CoilInParam cWDColdWaterInParam) {
        System.out.println("====================输入条件begin========================");
        System.out.print("【盘管类型】" + cWDColdWaterInParam.getCoilType());
        System.out.print("【排数】" + cWDColdWaterInParam.getRows());
        System.out.print("【片距】" + cWDColdWaterInParam.getFinDensity());
        System.out.print("【回路】" + cWDColdWaterInParam.getCircuit());
        System.out.print("【季节】" + cWDColdWaterInParam.getSeason());
        System.out.print("【介质】" + cWDColdWaterInParam.getCoolant());
//        System.out.print("【面风速】" + cWDColdWaterInParam.getVelocity());
//        System.out.print("【迎风面积】" + cWDColdWaterInParam.getArea());
        System.out.print("【进风干球温度】" + cWDColdWaterInParam.getInDryBulbT());
        System.out.print("【进风湿球温度】" + cWDColdWaterInParam.getInWetBulbT());
        System.out.print("【进风相对湿度】" + cWDColdWaterInParam.getInRelativeT());
        System.out.print("【进水温度】" + cWDColdWaterInParam.getEnteringFluidTemperature());
        System.out.print("【最大水阻】" + cWDColdWaterInParam.getMaxWPD());
        System.out.print("【水温升】" + cWDColdWaterInParam.getWTAScend());
        System.out.print("【水流量】" + cWDColdWaterInParam.getWaterFlow());
        System.out.println();
        System.out.println("====================输入条件end==========================");
    }

    //引擎结果打印
    private void enginePrint(DXCoilResult r) {
        System.out.println("=================原引擎返回结果begin=====================");
        /*System.out.print("【出风干球温度】" + r.getDryBulbTemperatureOut());
        System.out.print("【出风湿球温度】" + r.getWetBulbTemperatureOut());
        System.out.print("【出水温度】" + r.getWaterTemperatureOut());
        System.out.print("【空气阻力】" + r.getAirDrop());
        System.out.print("【水阻力】" + r.getWaterDrop());
        System.out.print("【水流量】" + r.getWaterFlowRate());
        System.out.print("【水温升】" + r.getWaterTemperatureDelta());
        System.out.print("【介质流速】" + r.getVwbrAve());
        System.out.println();
        System.out.print("【冷量】" + r.getTotalCap());
        System.out.print("【显冷量】" + r.getSensibleCap());
        System.out.println("【returnBypass】" + r.getBpf());
        System.out.println("err_mag" + r.getErr_mag());*/
        System.out.println("=================原引擎返回结果end=======================");
    }
}

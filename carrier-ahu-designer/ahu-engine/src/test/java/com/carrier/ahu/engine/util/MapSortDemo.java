package com.carrier.ahu.engine.util;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by LIANGD4 on 2017/8/24.
 */
public class MapSortDemo {

    public static void main(String[] args) {

        Map<String, String> map = new TreeMap<String, String>();
        map.put("3-12-HF", "wnba");
        map.put("3-10-HF", "wnba");
        map.put("z-10-10-FL", "kfc");
        map.put("14-10-FL", "kfc");
        map.put("2-NBA", "nba");
        map.put("2-CBA", "cba");

        Map<String, String> resultMap = sortMapByKey(map);    //按Key进行排序
        for (Map.Entry<String, String> entry : resultMap.entrySet()) {
            if(entry.getKey().split("-")[0].equals("z")){
                String  key = entry.getKey().substring(2,entry.getKey().length());
                System.out.println(key+ " " + entry.getValue());
            }else{
                System.out.println(entry.getKey()+ " " + entry.getValue());
            }
        }
    }

    /**
     * 使用 Map按key进行排序
     *
     * @param map
     * @return
     */
    public static Map<String, String> sortMapByKey(Map<String, String> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        Map<String, String> sortMap = new TreeMap<String, String>(new MapKeyComparator());
        sortMap.putAll(map);
        return sortMap;
    }
}


class MapKeyComparator implements Comparator<String> {

    @Override
    public int compare(String str1, String str2) {

        return str1.compareTo(str2);
    }
}
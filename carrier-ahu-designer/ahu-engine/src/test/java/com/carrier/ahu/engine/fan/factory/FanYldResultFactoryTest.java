package com.carrier.ahu.engine.fan.factory;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.carrier.ahu.engine.fanYld4k.entity.FanYldDllInfo;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldResultFactory;

/**
 * Created by liangd4 on 2017/7/18.
 */
public class FanYldResultFactoryTest {

    @Test
    public void packageCountResultEx3_test() throws Exception {//根据DLL返回结果封装页面值

        double[] pDoubleData = new double[50];//DLL返回值
        pDoubleData[0] = 1440.5;//风机转速
        pDoubleData[1] = 300.0;//全压内效率
        pDoubleData[3] = 17.5;//内功率
        pDoubleData[6] = 200.0;//静压
        pDoubleData[8] = 300.0;//全压
        pDoubleData[19] = 5.5;//实际噪声
        pDoubleData[38] = 18.9;//总A声压级
        pDoubleData[45] = 2000.8;//风机最高转速R/MIN

        String[][] pStrData = new String[5][255];//DLL返回值
        pStrData[0][0] = "SYW";
        pStrData[1][0] = "R";
        pStrData[2][0] = "250";
        pStrData[3][0] = "B";
        Integer tCount = 1;

        List<FanYldDllInfo> windMachineDLLResultList = new ArrayList<>();
        FanYldDllInfo windMachineDLLResult = new FanYldDllInfo();
        windMachineDLLResult.setBData(null);
        windMachineDLLResult.setPDoubleData(pDoubleData);
        windMachineDLLResult.setTCount(tCount);
        windMachineDLLResult.setFanType("39CQ0608");
        windMachineDLLResult.setMotorType("C");
        windMachineDLLResultList.add(windMachineDLLResult);

        //====================以上是调用DLL返回结果===========================
        FanYldResultFactory fanYldResultFactory = new FanYldResultFactory();
//        List<FanInfo> resultList = fanYldResultFactory.packageCountResultEx3(windMachineDLLResultList);
//        System.out.println(resultList);
    }

    @Test
    public void packageCountResultEx2_test() throws Exception {
        double[] pDoubleData = new double[50];//DLL返回值
        pDoubleData[0] = 1440.5;//风机转速
        pDoubleData[1] = 300.0;//全压内效率
        pDoubleData[3] = 17.5;//内功率
        pDoubleData[6] = 200.0;//静压
        pDoubleData[8] = 300.0;//全压
        pDoubleData[19] = 5.5;//实际噪声
        pDoubleData[38] = 18.9;//总A声压级
        pDoubleData[45] = 2000.8;//风机最高转速R/MIN

        String[][] pStrData = new String[5][255];//DLL返回值
        pStrData[0][0] = "SYW";
        pStrData[1][0] = "R";
        pStrData[2][0] = "250";
        pStrData[3][0] = "B";
        Integer tCount = 1;
        FanYldDllInfo windMachineDLLResult = new FanYldDllInfo();
        windMachineDLLResult.setBData(null);
        windMachineDLLResult.setPDoubleData(pDoubleData);
        windMachineDLLResult.setTCount(tCount);
        windMachineDLLResult.setFanType("39CQ0608");
        windMachineDLLResult.setMotorType("C");
        FanYldResultFactory fanYldResultFactory = new FanYldResultFactory();
//        List<FanInfo> resultList = fanYldResultFactory.packageCountResultEx2(windMachineDLLResult);
//        System.out.println(resultList);
    }

    @Test
    public void packageCountResultEx2K() throws Exception {
    }

    @Test
    public void packageCountResult() throws Exception {
    }

}
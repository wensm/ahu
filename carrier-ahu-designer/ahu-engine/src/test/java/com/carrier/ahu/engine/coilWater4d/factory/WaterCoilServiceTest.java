package com.carrier.ahu.engine.coilWater4d.factory;

import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_CIRCUIT_AUTO;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_CIRCUIT_FL;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_FINDENSITY_10;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_FINDENSITY_AUTO;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_FINTYPE_AL;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_ROWS_3;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_ROWS_4;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_ROWS_6;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_ROWS_AUTO;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_TUBEDIAMETER_1_2;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_TUBEDIAMETER_3_8;
import static com.carrier.ahu.vo.SystemCalculateConstants.waterCoil;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.engine.coil.entity.CoilInfo;
import com.carrier.ahu.engine.coil.param.CoilInParam;
import com.carrier.ahu.engine.coilWater4d.count.CalWaterDrop;
import com.carrier.ahu.engine.coilWater4d.entity.CoilWaterDllInfo;
import com.carrier.ahu.engine.coilWater4d.param.CWDCPCParam;
import com.carrier.ahu.engine.coilWater4d.param.CoilDllInParam;
import com.carrier.ahu.engine.watercoil.WaterCoilEngine;
import com.carrier.ahu.engine.watercoil.WaterCoilResult;

/**
 * Created by LIANGD4 on 2017/8/24.
 */
public class WaterCoilServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(WaterCoilServiceTest.class.getName());

    //热水盘管 季节为冬季 coil hot
    @Test
    public void cWDEngine_testHot1317() throws Exception {
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_3_8);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_6);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_FL);//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_10);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setSeason("W");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWTAScend(10.0);//水温升值
//        coilInParam.setAirVolume(18000.0);
        coilInParam.setSerial("39CQ1317");//机组型号
        coilInParam.setEnteringFluidTemperature(60.0);//进水温度
        coilInParam.setCalculateOptimalResults(true);//计算最优

        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(46.2);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(25.0);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================
        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }

    @Test
    public void cWDEngine_testCold1317D3() throws Exception {
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_3_8);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_3);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_FL);//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_10);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setSeason("S");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWTAScend(5.0);//水温升值
        coilInParam.setSerial("39CQ1317");//机组型号
        coilInParam.setEnteringFluidTemperature(7.0);//进水温度
        coilInParam.setCalculateOptimalResults(true);//计算最优


        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(27.0);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(19.5);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================
        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }


    //================================季节S============================================
    //=================================================================================
    //=================================================================================
    //3	10	HF	1.68	2.97	27	19.5	50	16.9	15.3	84.8	93.3	21.5	13	7	5	1.67	75.78	62.27	0.2291
    @Test  //入参 盘管类型：冷水盘管  排数：3  回路：HF 片距：10
    public void cWDEngine_test() throws Exception {
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_1_2);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_4);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_FL);//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_10);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setSeason("S");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWTAScend(5.0);//水温升值
        coilInParam.setSerial("39CQ3438");//机组型号
        coilInParam.setEnteringFluidTemperature(7.0);//进水温度
        coilInParam.setCalculateOptimalResults(true);//计算最优


        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(27.0);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(19.5);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");;//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================
        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }

    @Test  //入参 盘管类型：冷水盘管 管径：四分管 排数：AUTO 回路：AUTO  片距：AUTO 机组型号：39CQ1317
    public void cWDEngine_test1() throws Exception {
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_1_2);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_AUTO);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_AUTO);//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_AUTO);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setSeason("S");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWTAScend(5.0);//水温升值
        coilInParam.setSerial("39CQ1317");//机组型号
        coilInParam.setEnteringFluidTemperature(7.0);//进水温度
        coilInParam.setCalculateOptimalResults(true);//计算最优
//        coilInParam.setCalStyle(AirDirectionEnum.SUPPLYAIR.getCode());
        coilInParam.setSairvolume(18000);
        coilInParam.setSairvolume(18000);


        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(27.0);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(19.5);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================
        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }

    //4	10	FL	12.16	0.41	27	19.5	50	10.7	10.6	99	7.2	0	26.3	7	5	0.26	153.36	104.1	0.0177
    @Test  //入参 盘管类型：冷水盘管 管径：四分管 排数：4 回路：FL 片距：10 机组型号：39CQ3438
    public void cWDEngine_test2() throws Exception {
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_1_2);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_4);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_FL);//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_10);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setSeason("S");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWTAScend(5.0);//水温升值
        coilInParam.setEnteringFluidTemperature(7.0);//进水温度
        coilInParam.setSerial("39CQ3438");//机组型号
        coilInParam.setCalculateOptimalResults(true);//计算最优
//        coilInParam.setCalStyle(AirDirectionEnum.SUPPLYAIR.getCode());
        coilInParam.setSairvolume(100000);
        coilInParam.setSairvolume(100000);


        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(27.0);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(19.5);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        LOGGER.info("CoilInParam:"+JSONArray.toJSONString(coilInParam));
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================
        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }

    //6	10	FL	1.68	2.97	27	19.5	50	13.3	12.9	96.1	192.9	19.7	20	7	5	1.28	116.29	86.09	0.0606
    //6	12	FL	1.68	2.97	27	19.5	50	12.6	12.3	97.7	229.9	22.9	21.6	7	5	1.38	125.71	91.01	0.0387
    //6	14	FL	1.68	2.97	27	19.5	50	12	11.8	98.5	268.5	25.8	22.9	7	5	1.47	133.53	94.88	0.0267
    @Test  //入参 盘管类型：冷水盘管 管径：四分管 排数：6 回路：FL 片距：AUTO 机组型号：39CQ1317
    public void cWDEngine_test3() throws Exception {
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_1_2);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_6);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_FL);//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_AUTO);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setSeason("S");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setSerial("39CQ1317");//机组型号
        coilInParam.setEnteringFluidTemperature(7.0);//进水温度
        coilInParam.setWTAScend(5.0);//水温升值


        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(27.0);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(19.5);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setCalculateAllResults(true);//计算所有
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================
        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }

    //================================季节W============================================
    //=================================================================================
    //=================================================================================
    //3	10	FL	2.62	2.65	27	19.5	50	45.3	24.7	18.1	64.2	7.2	12.4	60	10	0.65	142.06
    @Test  //入参 盘管类型：冷水盘管 管径：四分管 排数：3 回路：FL 片距：10 机组型号：39CQ1621 季节W
    public void cWDEngine_test4() throws Exception {
        //================================入参封装begin================================
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_1_2);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_3);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_10);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setCircuit("FL");//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setSeason("S");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setSerial("39CQ1621");//机组型号
        coilInParam.setEnteringFluidTemperature(7.0);//进水温度
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWTAScend(5.0);//水温升值


        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(27.0);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(19.5);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
//        coilInParam.setWaterFlow(false);//水流量
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setCalculateOptimalResults(true);//计算最优
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================

        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }

    //6	10	FL	1.68	2.97	27	19.5	50	54	26.9	11.7	152.4	7.8	12.9	60	10	0.82	147.05
    @Test  //入参 盘管类型：冷水盘管 管径：三分管 排数：6 回路：FL 片距：10 机组型号：39CQ1317 季节W
    public void cWDEngine_test5() throws Exception {
        //================================入参封装begin================================
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_3_8);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_6);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_10);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_FL);//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setSeason("S");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setSerial("39CQ1317");//机组型号
        coilInParam.setEnteringFluidTemperature(60.0);//进水温度
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWTAScend(10.0);//水温升值


        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(27.0);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(19.5);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");;//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
//        coilInParam.setWaterFlow(false);//水流量
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setCalculateOptimalResults(true);//计算最优
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================

        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }

    @Test  //入参 盘管类型：冷水盘管 管径：四分管 排数：AUTO 回路：AUTO 片距：AUTO 机组型号：39CQ1317 季节W
    public void cWDEngine_test6() throws Exception {
        //================================入参封装begin================================
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_1_2);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_AUTO);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_10);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_AUTO);//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setSeason("W");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setSerial("39CQ1317");//机组型号
        coilInParam.setEnteringFluidTemperature(60.0);//进水温度
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWTAScend(10.0);//水温升值


        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(27.0);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(19.5);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");;//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
//        coilInParam.setWaterFlow(false);//水流量
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setCalculateOptimalResults(true);//计算最优
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================

        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }

    //================================特殊机型============================================
    //=================================================================================
    //=================================================================================
    //3	10	FL	0.46	3.03	27	19.5	50	19.8	17.1	76.8	85.5	6.8	2.1	7	5	0.3	12.11	12.11	0.3689
    @Test  //入参 盘管类型：冷水盘管 管径：四分管 排数：3 回路：FL 片距：10 机组型号：39CQ0711
    public void cWDEngine_test7() throws Exception {
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_1_2);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_3);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_FL);//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_10);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setSeason("S");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWTAScend(5.0);//水温升值
        coilInParam.setEnteringFluidTemperature(7.0);//进水温度
        coilInParam.setSerial("39CQ0711");//机组型号
        coilInParam.setCalculateOptimalResults(true);//计算最优


        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(35.0);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(28.5);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");;//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
//        coilInParam.setWaterFlow(false);//水流量
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================
        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }

    @Test  //入参 盘管类型：冷水盘管  排数：6  回路：HF 片距：10
    public void cWDEngine_test0608() throws Exception {
        CoilInParam coilInParam = new CoilInParam();
        coilInParam.setCoilType(waterCoil);//盘管类型 D:冷水盘管、DX:直接蒸发式
        coilInParam.setTubeDiameter(COOLINGCOIL_TUBEDIAMETER_1_2);//管径 1:四分管、2:三分管
        coilInParam.setRows(COOLINGCOIL_ROWS_6);//排数 -1、2、3、4、5、6、7、8、10、12
        coilInParam.setCircuit(COOLINGCOIL_CIRCUIT_FL);//回路 AUTO、HF半回路、FL全回路、DB双回路
        coilInParam.setFinDensity(COOLINGCOIL_FINDENSITY_10);//片距 -1、10FPI、12FPI、14FPI
        coilInParam.setSeason("S");//季节 W:冬、S:夏、ALL:冬夏
        coilInParam.setConcentration(1);//水温升
        coilInParam.setWTAScend(5.0);//水温升值
        coilInParam.setSerial("39CQ0608");//机组型号
        coilInParam.setEnteringFluidTemperature(7.0);//进水温度
        coilInParam.setCalculateOptimalResults(true);//计算最优


        coilInParam.setFinType(COOLINGCOIL_FINTYPE_AL);//翅片材质 procoatedAl:亲水铝翅片 copper:铜翅片 al:普通铝翅片暂无数据
        coilInParam.setInDryBulbT(27.0);//进风干球温度 小数精度限制、页面暂时没有限制
        coilInParam.setInWetBulbT(19.5);//进风湿球温度
        coilInParam.setInRelativeT(50.0);//进风相对湿度 计算时保留整数
        coilInParam.setCoolant(1);//介质 1:水 3:乙二醇 4:丙二醇
        coilInParam.setConcentration(0);//浓度
        coilInParam.setQualifiedConditions("4");;//夏 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setSbalanceMethodValue(19.55);//夏 限定条件值
        coilInParam.setSbalanceMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setWbalancedMethod(4);//冬 限定条件 1:冷量、2:显冷、3:出风温度、4:无、5:三通阀
        coilInParam.setWbalancedMethodValue(5.55);//冬 限定条件值
        coilInParam.setWbalancedMethodUnit("");//冬 限定计量单位 暂时无使用
        coilInParam.setCount("");//计算条件 水温升、水流量 暂时无使用
        coilInParam.setCountValue("");//计算条件对应值 暂时无使用
//        coilInParam.setWaterFlow(false);//水流量
        coilInParam.setWaterFlow(0.0);//水流量值
        coilInParam.setLanguage(0);//语言 0:中文 1：英文
        coilInParam.setAltitude(0.0);//海拔高度
        coilInParam.setMaxWPD(89.0);//最大水阻
        CoilWaterDllInParamFactory coilWaterDllInParamFactory = new CoilWaterDllInParamFactory();
        List<CoilDllInParam> waterOutParamList = coilWaterDllInParamFactory.packageParamHTC(coilInParam);
        inParamPrint(coilInParam);//入参打印
        //================================入参封装end================================
        List<CoilWaterDllInfo> coldWaterDLLResultList = engineCll(coilInParam, waterOutParamList);
        factoryCll(coldWaterDLLResultList);
    }

    //调用factory
    private void factoryCll(List<CoilWaterDllInfo> coldWaterDLLResultList) {
        //================================DLL数据加工begin===============================
        CoilWaterResultFactory packageResultFactory = new CoilWaterResultFactory();
        List<CoilInfo> engineCalculationList = packageResultFactory.packageCountResult(coldWaterDLLResultList);
        for (CoilInfo result : engineCalculationList) {
            factoryPrint(result);
        }
        //================================DLL数据加工end===============================
    }

    //DLL调用
    private List<CoilWaterDllInfo> engineCll(CoilInParam coilInParam, List<CoilDllInParam> waterOutParamList) throws InterruptedException {
        //================================DLL调用begin===============================
        List<CoilWaterDllInfo> coldWaterDLLResultList = new ArrayList<CoilWaterDllInfo>();
        for (CoilDllInParam coilDllInParam : waterOutParamList) {
            List<CWDCPCParam> cPCParamList = coilDllInParam.getCPCParamList();
            for (CWDCPCParam cwdcpcParam : cPCParamList) {
                outParamPrint(coilDllInParam, cwdcpcParam);//入参加工打印
                WaterCoilResult r = WaterCoilEngine.calculate(cwdcpcParam.getStrCpc(), coilDllInParam.getVFace(), coilDllInParam.getTdbAirIn(),
                        coilDllInParam.getTwbAirIn(), coilDllInParam.getPAirIn(), coilDllInParam.getFFouling(), coilDllInParam.getSurfEff(),
                        coilDllInParam.getRADMULT(), coilDllInParam.getDPDMULT(), coilDllInParam.getRAWMULT(), coilDllInParam.getDPWMULT(),
                        cwdcpcParam.getLBTS(), coilDllInParam.getODEXP(), coilDllInParam.getTW(), coilDllInParam.getIdBend(), coilDllInParam.getIdHairpin(),
                        coilDllInParam.getPT(), coilDllInParam.getPR(), coilDllInParam.getTCond(), Math.toIntExact(coilDllInParam.getCurveNum()),
                        coilDllInParam.getFThick(), coilDllInParam.getFCond(), coilDllInParam.getFPI(), coilDllInParam.getTWaterIn(),
                        coilDllInParam.getDtWater(), cwdcpcParam.getqWaterIn(), Math.toIntExact(coilDllInParam.getFluidId()), coilDllInParam.getFluidConcentration(),
                        coilDllInParam.getHeight());
                coldWaterDLLResultList.add(packageCWDColdWaterDllResult(coilDllInParam, cwdcpcParam, r, coilInParam));
                enginePrint(r);//引擎结果打印
            }
        }
        //================================DLL调用end===============================
        return coldWaterDLLResultList;
    }

    //DLL数据加工打印
    private void factoryPrint(CoilInfo result) {
//        System.out.println("====================DLL res begin====================");
        System.out.print("排数：" + result.getRows() + "片距：" + result.getFinDensity() + "回路：" + result.getCircuit());
        System.out.print("【出风干球温度】" + result.getOutDryBulbT());
        System.out.print("【出风湿球温度】" + result.getOutWetBulbT());
        System.out.print("【空气阻力】" + result.getAirResistance());
        System.out.print("【水阻力】" + result.getWaterResistance());
        System.out.print("【水流量】" + result.getReturnWaterFlow());
        System.out.print("【水温升】" + result.getReturnWTAScend());
        System.out.print("【介质流速】" + result.getFluidvelocity());
        System.out.print("【冷量】" + result.getReturnColdQ());
        System.out.print("【显冷量】" + result.getReturnSensibleCapacity());
        System.out.println("【returnBypass】" + result.getReturnBypass());
//        System.out.println("====================DLL res end======================");
    }

    //引擎结果打印
    private void enginePrint(WaterCoilResult r) {
        System.out.println("=================原引擎返回结果begin=====================");
        System.out.print("【出风干球温度】" + r.getDryBulbTemperatureOut());
        System.out.print("【出风湿球温度】" + r.getWetBulbTemperatureOut());
        System.out.print("【出水温度】" + r.getWaterTemperatureOut());
        System.out.print("【空气阻力】" + r.getAirDrop());
        System.out.print("【水阻力】" + r.getWaterDrop());
        System.out.print("【水流量】" + r.getWaterFlowRate());
        System.out.print("【水温升】" + r.getWaterTemperatureDelta());
        System.out.print("【介质流速】" + r.getVwbrAve());
        System.out.println();
        System.out.print("【冷量】" + r.getTotalCap());
        System.out.print("【显冷量】" + r.getSensibleCap());
        System.out.println("【returnBypass】" + r.getBpf());
        System.out.println("=================原引擎返回结果end=======================");
    }

    //outParamPrint
    private void outParamPrint(CoilDllInParam coilDllInParam, CWDCPCParam cwdcpcParam) {
//        System.out.println("=================outParamPrint begin=====================");
        System.out.println("排数：" + cwdcpcParam.gettRow() + "片距：" + coilDllInParam.getFPI() + "路径：" + cwdcpcParam.getStrCpc());
        System.out.print("【Vface】" + coilDllInParam.getVFace());
        System.out.print("【Tdb_air_in】" + coilDllInParam.getTdbAirIn());
        System.out.print("【Twb_air_in】" + coilDllInParam.getTwbAirIn());
        System.out.print("【P_air_in】" + coilDllInParam.getPAirIn());
        System.out.print("【F_fouling】" + coilDllInParam.getFFouling());
        System.out.print("【Surf_eff】" + coilDllInParam.getSurfEff());
        System.out.print("【RADMULT】" + coilDllInParam.getRADMULT());
        System.out.print("【DPDMULT】" + coilDllInParam.getDPDMULT());
        System.out.print("【RAWMULT】" + coilDllInParam.getRAWMULT());
        System.out.print("【DPWMULT】" + coilDllInParam.getDPWMULT());
        System.out.print("【LBTS】" + cwdcpcParam.getLBTS());
        System.out.print("【ODEXP】" + coilDllInParam.getODEXP());
        System.out.println("【TW】" + coilDllInParam.getTW());
        System.out.print("【IDbend】" + coilDllInParam.getIdBend());
        System.out.print("【IDhairpin】" + coilDllInParam.getIdHairpin());
        System.out.print("【PT】" + coilDllInParam.getPT());
        System.out.print("【PR】" + coilDllInParam.getPR());
        System.out.print("【Tcond】" + coilDllInParam.getTCond());
        System.out.print("【CurveNum】" + coilDllInParam.getCurveNum());
        System.out.print("【Fthick】" + coilDllInParam.getFThick());
        System.out.print("【Fcond】" + coilDllInParam.getFCond());
        System.out.print("【FPI】" + coilDllInParam.getFPI());
        System.out.print("【T_water_in】" + coilDllInParam.getTWaterIn());
        System.out.print("【DT_water】" + coilDllInParam.getDtWater());
        System.out.print("【Q_water_in】" + cwdcpcParam.getqWaterIn());
        System.out.println("【Fluid_ID】" + coilDllInParam.getFluidId());
//        System.out.println("=================outParamPrint end=======================");

    }

    //入参参数打印
    private void inParamPrint(CoilInParam coilInParam) {
        System.out.println("====================输入条件begin========================");
        System.out.print("【盘管类型】" + coilInParam.getCoilType());
        System.out.print("【排数】" + coilInParam.getRows());
        System.out.print("【片距】" + coilInParam.getFinDensity());
        System.out.print("【回路】" + coilInParam.getCircuit());
        System.out.print("【季节】" + coilInParam.getSeason());
        System.out.print("【介质】" + coilInParam.getCoolant());
//        System.out.print("【面风速】" + coilInParam.getVelocity());
//        System.out.print("【迎风面积】" + coilInParam.getArea());
        System.out.print("【进风干球温度】" + coilInParam.getInDryBulbT());
        System.out.print("【进风湿球温度】" + coilInParam.getInWetBulbT());
        System.out.print("【进风相对湿度】" + coilInParam.getInRelativeT());
        System.out.print("【进水温度】" + coilInParam.getEnteringFluidTemperature());
        System.out.print("【最大水阻】" + coilInParam.getMaxWPD());
        System.out.print("【水温升】" + coilInParam.getWTAScend());
        System.out.print("【水流量】" + coilInParam.getWaterFlow());
        System.out.println();
        System.out.println("====================输入条件end==========================");
    }

    //封装DLL返回数据
    private CoilWaterDllInfo packageCWDColdWaterDllResult(CoilDllInParam coilDllInParam, CWDCPCParam cwdcpcParam, WaterCoilResult r, CoilInParam coilInParam) {
        int tubeDiameter = 1;
        if (COOLINGCOIL_TUBEDIAMETER_3_8.equals(coilInParam.getTubeDiameter())) {
            tubeDiameter = 2;
        }
        CoilWaterDllInfo coilWaterDllInfo = new CoilWaterDllInfo();
        coilWaterDllInfo.setRows(cwdcpcParam.gettRow());//排数
        coilWaterDllInfo.setFpi((new Double(coilDllInParam.getFPI())).intValue());//片距
        coilWaterDllInfo.setCir(coilDllInParam.getCIR());//回路
        coilWaterDllInfo.setVelocity(coilDllInParam.getVFace());//面风速
        coilWaterDllInfo.setFaceArea(coilDllInParam.getFaceArea());//迎风面积
        coilWaterDllInfo.setEaDB(coilDllInParam.getTdbAirIn());//进风干球温度
        coilWaterDllInfo.setEaWB(coilDllInParam.getTwbAirIn());//进风湿球温度
        coilWaterDllInfo.setEaRH(50.0);//进风相对湿度
        coilWaterDllInfo.setLaDB(r.getDryBulbTemperatureOut());//出风干球温度
        coilWaterDllInfo.setLaWB(r.getWetBulbTemperatureOut());//出风湿球温度
        coilWaterDllInfo.setLaRH(50.0);//出风相对湿度
        coilWaterDllInfo.setApd(r.getAirDrop());//空气阻力
        //水阻力
        double Watertemp = Math.round((coilInParam.getEnteringFluidTemperature() + r.getWaterTemperatureOut()) / 2.0);
//        coilWaterDllInfo.setWaterResistance(CalWaterDrop.calWaterDrop(Watertemp,cwdcpcParam.gettId(), cwdcpcParam.getCirNo(), cwdcpcParam.gettLen(), cwdcpcParam.gettNo(),r.getWaterTemperatureOut(),cwdcpcParam.gettRow(), coilInParam.getSerial(), coilInParam.getTubeDiameter(),coilInParam.getAhri()));
        //欧标
        coilWaterDllInfo.setWpd(CalWaterDrop.calWaterDropOne(cwdcpcParam.gettId(), cwdcpcParam.getCirNo(), cwdcpcParam.gettLen(), cwdcpcParam.gettNo(),
                r.getWaterFlowRate(), 0, coilInParam.getEnteringFluidTemperature(), r.getWaterTemperatureOut(), cwdcpcParam.gettRow(), coilInParam.getSerial(), tubeDiameter, true));
        coilWaterDllInfo.setFlow(r.getWaterFlowRate());//水流量
        coilWaterDllInfo.setEwt(coilDllInParam.getEwt());//进水温度
        coilWaterDllInfo.setTr(r.getWaterTemperatureDelta());//水温升
        coilWaterDllInfo.setFluidVelocity(r.getVwbrAve());//介质流速
        coilWaterDllInfo.setTotal(r.getTotalCap());//冷量
        coilWaterDllInfo.setSensible(r.getSensibleCap());//显冷
        coilWaterDllInfo.setBypass(r.getBpf());//Bypass
        coilWaterDllInfo.setUniType(coilDllInParam.getUnitType());//机组型号
        coilWaterDllInfo.setFluid_ID(coilDllInParam.getFluidId());//入参参数
        coilWaterDllInfo.setCONC1(coilDllInParam.getPR());//入参参数
        return coilWaterDllInfo;
    }

}

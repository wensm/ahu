package com.carrier.ahu.engine.heatRecycle.factory;

import org.junit.Test;

import com.carrier.ahu.engine.heatRecycle.HeatRecycleDllFactory;
import com.carrier.ahu.engine.heatRecycle.HeatRecycleParam;

/**
 * Created by liangd4 on 2017/12/1.
 */
public class HeatRecycleDllFactoryTest {


    @Test
    public void heatRecycleDllBrandA() throws Exception {
        HeatRecycleDllFactory heatRecycleDllFactory = new HeatRecycleDllFactory();
        HeatRecycleParam heatRecycleParam = new HeatRecycleParam();
//        机组参数
        heatRecycleParam.setSerial("39CQ1317");//机组型号
        heatRecycleParam.setSeaLevel(0);//海拔高度
//         风量参数
        heatRecycleParam.setRAVolume(15000);//回风量
        heatRecycleParam.setNAVolume(15000);//新风量
//         新风参数
        heatRecycleParam.setSInDryBulbT(40);//夏季干球温度
        heatRecycleParam.setSInWetBulbT(19.5);//夏季湿球温度
        heatRecycleParam.setSInRelativeT(13);//夏季相对湿度
//        回风参数
        heatRecycleParam.setSNewDryBulbT(35);
        heatRecycleParam.setSNewWetBulbT(19.5);
        heatRecycleParam.setSNewRelativeT(22);
        heatRecycleParam.setNSEnable(false);
        heatRecycleParam.setHeatRecoveryType("heatWheel");

        heatRecycleParam.setBrand("A");
        heatRecycleParam.setEnableWinter(false);
        heatRecycleParam.setEnableSummer(true);
        heatRecycleParam.setModel("分子筛");
        heatRecycleParam.setWheelDepth(200);//转轮厚度
        heatRecycleParam.setEfficiencyType("效率");
        heatRecycleDllFactory.heatRecycleDll(heatRecycleParam);
    }

    @Test
    public void heatRecycleDllBrandC() throws Exception {
        HeatRecycleDllFactory heatRecycleDllFactory = new HeatRecycleDllFactory();
        HeatRecycleParam heatRecycleParam = new HeatRecycleParam();
//        机组参数
        heatRecycleParam.setSerial("39CQ1317");//机组型号
        heatRecycleParam.setSeaLevel(0);//海拔高度
//         风量参数
        heatRecycleParam.setRAVolume(15000);//回风量
        heatRecycleParam.setNAVolume(15000);//新风量
//         新风参数
        heatRecycleParam.setSInDryBulbT(40);//夏季干球温度
        heatRecycleParam.setSInWetBulbT(19.5);//夏季湿球温度
        heatRecycleParam.setSInRelativeT(13);//夏季相对湿度
//        回风参数
        heatRecycleParam.setSNewDryBulbT(35);
        heatRecycleParam.setSNewWetBulbT(19.5);
        heatRecycleParam.setSNewRelativeT(22);
        heatRecycleParam.setNSEnable(false);
        heatRecycleParam.setHeatRecoveryType("heatWheel");

        heatRecycleParam.setBrand("C");
        heatRecycleParam.setEnableWinter(false);
        heatRecycleParam.setEnableSummer(true);
        heatRecycleParam.setModel("1"); //热回收型号 1：分子筛 2：硅胶 3：显热
        heatRecycleParam.setWheelDepth(200);//转轮厚度
        heatRecycleParam.setEfficiencyType("效率");
        heatRecycleDllFactory.heatRecycleDll(heatRecycleParam);
    }

    @Test
    public void heatRecycleDllBrandB() throws Exception {
        HeatRecycleDllFactory heatRecycleDllFactory = new HeatRecycleDllFactory();
        HeatRecycleParam heatRecycleParam = new HeatRecycleParam();
//        机组参数
        heatRecycleParam.setSerial("39CQ2025");//机组型号
        heatRecycleParam.setSeaLevel(0);//海拔高度
//         风量参数
        heatRecycleParam.setRAVolume(18000);//回风量
        heatRecycleParam.setNAVolume(18000);//新风量
//         新风参数
        heatRecycleParam.setSInDryBulbT(40);//夏季干球温度
        heatRecycleParam.setSInWetBulbT(19.5);//夏季湿球温度
        heatRecycleParam.setSInRelativeT(13);//夏季相对湿度
//        回风参数
        heatRecycleParam.setSNewDryBulbT(35);
        heatRecycleParam.setSNewWetBulbT(19.5);
        heatRecycleParam.setSNewRelativeT(22);
        heatRecycleParam.setNSEnable(false);
        heatRecycleParam.setHeatRecoveryType("heatWheel");

        heatRecycleParam.setBrand("B");
        heatRecycleParam.setEnableWinter(false);
        heatRecycleParam.setEnableSummer(true);
        heatRecycleParam.setModel("2"); //热回收型号 1：分子筛 2：硅胶 3：显热
        heatRecycleParam.setWheelDepth(200);//转轮厚度
        heatRecycleParam.setEfficiencyType("效率");
        heatRecycleDllFactory.heatRecycleDll(heatRecycleParam);
    }

}
package com.carrier.ahu.engine.coilWater4d.factory;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by liangd4 on 2018/2/7.
 */
public class ThreadTest {

    public static void main(String[] args) throws InterruptedException,ExecutionException {

        final ExecutorService exec = Executors.newFixedThreadPool(1);
        Callable<String> call = new Callable<String>() {
            public String call() throws Exception {
                //开始执行耗时操作
                System.out.println(System.currentTimeMillis());
                Thread.sleep(1000 * 5);
                System.out.println(System.currentTimeMillis());
                return "start";
            }
        };

        try {
            Future<String> future = exec.submit(call);
            String obj = future.get(4999 , TimeUnit.MILLISECONDS); //任务处理超时时间设为 1 秒
            System.out.println("任务成功返回:" + obj);
        } catch (TimeoutException ex) {
            System.out.println("处理超时啦....");
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println("处理失败.");
            e.printStackTrace();
        } finally {
            // 关闭线程池
            exec.shutdown();
        }
    }

}

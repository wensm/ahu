package com.carrier.ahu.engine.fan;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;

import com.carrier.ahu.engine.fanYld4k.entity.FanYldDllInfo;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllEx3Factory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllInEx2RParamFactory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllInEx3ParamFactory;
import com.carrier.ahu.engine.fanYld4k.factory.FanYldResultFactory;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.engine.fanYld4k.param.FanYldDllInParam;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by liangd4 on 2017/8/29.
 */
public class FanYldServiceTest implements SystemCalculateConstants {


    private static final FanYldDllInEx2RParamFactory FAN_YLD_DLL_IN_EX_2_PARAM_FACTORY = new FanYldDllInEx2RParamFactory();
    private static final FanYldDllEx3Factory FAN_YLD_DLL_EX_3_FACTORY = new FanYldDllEx3Factory();
    private static final FanYldResultFactory fanYldResultFactory = new FanYldResultFactory();

    @Test
    public void windMachineEngineCalculation() throws Exception {
        System.out.println("packageParam01 test-windMachineEngineCalculation begin");
        FanInParam fanInParam = new FanInParam();
        fanInParam.setAltitude(1000);//海拔高度
        fanInParam.setSairvolume(40000);//风量(m3/h)
        fanInParam.setEairvolume(40000);//风量(m3/h)
        fanInParam.setTotalStatic(666.6);//风压（Pa）
        fanInParam.setSerial("39CQ1822");//机组型号 39G0608
        fanInParam.setFanSupplier("A");//供应商
        fanInParam.setOutlet(FAN_OPTION_OUTLET_FOB);//风机形式 0：前弯或后弯 1：前弯 2：后弯 3:机翼型 7:无蜗壳风机 outlet 暂时不支持0
        System.out.println("packageParam01 package fanInParam success");

        FanYldDllInEx3ParamFactory fanYldDllInEx3ParamFactory = new FanYldDllInEx3ParamFactory();
        FanYldDllInParam fanYldDllInParam = fanYldDllInEx3ParamFactory.packageParam(fanInParam);

        //根据封装的值调用DLL文件
        FanYldDllEx3Factory wmkWindMachineDll = new FanYldDllEx3Factory();
        List<FanYldDllInfo> fanYldDllInfoList = wmkWindMachineDll.fanSelectionEx3(fanYldDllInParam,fanInParam);

        //根据DLL返回结果封装页面返回参数
//        FanYldResultFactory packageResultFactory = new FanYldResultFactory();
//        List<FanInfo> fanInfoList = packageResultFactory.packageCountResultEx3(fanYldDllInfoList);

        //根据海拔高度计算空气密度 需要调用DLl完成后放入pAirDensity字段
//        wmkWindMachineOutParam.setPAirDensity("空气密度需要计算值");

//        System.out.println("packageParam01 test-windMachineEngineCalculation end");
//        assertEquals(wmkWindMachineOutParam, wmkWindMachineOutParam);
    }

    @Test
    // 能出东西即可，具体含义还待整合
    public void testFanSelection() {
        int count = FanLib.INSTANCE.FanSelection(18000, 67.6, 0);
        System.out.println(count);
    }

    @Test
    public void windMachineEngineCalculation2() throws Exception {
        System.out.println("packageParam01 test-windMachineEngineCalculation begin");
        FanInParam fanInParam = new FanInParam();
        fanInParam.setAltitude(20);//海拔高度
        fanInParam.setSairvolume(60000);//风量(m3/h)
        fanInParam.setEairvolume(60000);//风量(m3/h)
        fanInParam.setTotalStatic(300);//风压（Pa）
        fanInParam.setSerial("39CQ2226");//机组型号 39G0608
        fanInParam.setFanSupplier("A");//供应商
        fanInParam.setOutlet(FAN_OPTION_OUTLET_FOB);//风机形式 0：前弯或后弯 1：前弯 2：后弯 3:机翼型 7:无蜗壳风机 outlet 暂时不支持0
        System.out.println("packageParam01 package fanInParam success");
        FanYldDllInEx3ParamFactory fanYldDllInEx3ParamFactory = new FanYldDllInEx3ParamFactory();
        FanYldDllInParam fanYldDllInParam = fanYldDllInEx3ParamFactory.packageParam(fanInParam);
        List<FanYldDllInfo> fanYldDllInfoList = FAN_YLD_DLL_EX_3_FACTORY.fanSelectionEx3(fanYldDllInParam,fanInParam);
        System.out.println("");
    }


    @Test
    public void FanSelectionEx2Test() {
        Lock fanLock = new ReentrantLock();
        try {
            double altitude = 0;//海拔高度 旧系统0-8000
            String pSeriesNames = "SYD";//系列名称（为空时不限制），用分号相隔如SYQ或SYD;SYQ
            String pSubTypeNames = "R";//子系列名称（为空时不限制），用分号相隔如K或K;K2
            double pFlow = 40000;//风量(m3/h)
            double pPres = 566.6;//风压（Pa）
            int pPresType = 1;//风压类型 0-全压1-静压
            int pAirDesityType = 1;//空气密度类型 0-自定义1-直接输入空气密度 当EX2空气密度类型为1
            double pB = 120000;//大气压力
            double pT = 25;//空气温度
            double pV = 50;//相对湿度
            double pMotorSafeCoff = 10;//电机容量安全系数(%)
            boolean pUserSetMotorSafeCoff = false;//用户设定了电机容量安全系数true-是 false-否
            fanLock.lock();
            FanLib fl = FanLib.INSTANCE;
            double density = fl.CalAirDensity2(altitude, 21);
            /*int count = fl.FanSelectionEx3(pSeriesNames, pSubTypeNames, pFlow, pPres, pPresType,
                    pOutFanType, pAirDesityType, density, pB, pT,
                    pV, pMotorSafeCoff, pUserSetMotorSafeCoff);*/
            int count = fl.FanSelectionEx2(pSeriesNames, pSubTypeNames, pFlow, pPres, pPresType,
                     pAirDesityType, density, pB, pT,pV,
                    pMotorSafeCoff, pUserSetMotorSafeCoff);
            for (int i = 0; i < count; i++) {
                byte[] bData = new byte[100 * 255];
                double[] pDoubleData = new double[500];
                fl.GetLargeResultData(i, bData, pDoubleData);
                String seriesNames = new String(bData, 0, 10).trim();
                String subTypeNames = new String(bData, 255, 10).trim();
                String fanDia = new String(bData, 510, 10).trim();
                String motorType = new String(bData, 765, 10).trim();// 电机类型"A" "三级能效电机";"B" "防爆电机";"C""双速电机";"D" "变频电机(IC416)";如果是‘无蜗壳风机’="变频电机直联(IC416)
                String rpm = String.valueOf(pDoubleData[0]);// 风机转速
                String efficiency = String.valueOf(pDoubleData[1]);// 全压内效率
                String absorberPower = String.valueOf(pDoubleData[3]);// 内功率
                String outletVelocity = String.valueOf(pDoubleData[6]);// 静压
                Double motor = pDoubleData[19];// 实际噪声
                String noise = String.valueOf(pDoubleData[38]);// 总A声压级
                String pt = String.valueOf(pDoubleData[8]);// 全压
                String maxSpeed = String.valueOf(pDoubleData[45]);// 风机最高转速R/MIN
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>");
                System.out.println("seriesNames:" + seriesNames);
                System.out.println("subTypeNames:" + subTypeNames);
                System.out.println("fanDia:" + fanDia);
                System.out.println("motorType:" + motorType);
                System.out.println("rpm:" + rpm);
                System.out.println("efficiency:" + efficiency);
                System.out.println("absorberPower:" + absorberPower);
                System.out.println("outletVelocity:" + outletVelocity);
                System.out.println("motor:" + motor);
                System.out.println("noise:" + noise);
                System.out.println("pt:" + pt);
                System.out.println("maxSpeed:" + maxSpeed);
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            }
        } finally {
            fanLock.unlock();
        }
    }

    @Test
    public void FanSelectionEx3Test() {
        Lock fanLock = new ReentrantLock();
        try {
            double altitude = 0;//海拔高度 旧系统0-8000
            String pSeriesNames = "";//系列名称（为空时不限制），用分号相隔如SYQ或SYD;SYQ
            String pSubTypeNames = "";//子系列名称（为空时不限制），用分号相隔如K或K;K2
            double pFlow = 0.00;//风量(m3/h)
            double pPres = 0.00;//风压（Pa）
            int pPresType = 0;//风压类型 0-全压1-静压
            int pOutFanType = 0;//出风方式 0-管道出风1-自由出风
            int pAirDesityType = 1;//空气密度类型 0-自定义1-直接输入空气密度
            double pB = 0;//大气压力
            double pT = 0;//空气温度
            double pV = 0;//相对湿度
            double pMotorSafeCoff = 0;//电机容量安全系数(%)
            boolean pUserSetMotorSafeCoff = false;//用户设定了电机容量安全系数true-是 false-否

            fanLock.lock();
            FanLib fl = FanLib.INSTANCE;
            double density = fl.CalAirDensity2(altitude, 21);
            int count = fl.FanSelectionEx3(pSeriesNames, pSubTypeNames, pFlow, pPres, pPresType,
                    pOutFanType, pAirDesityType, density, pB, pT,
                    pV, pMotorSafeCoff, pUserSetMotorSafeCoff);
            for (int i = 0; i < count; i++) {
                byte[] bData = new byte[100 * 255];
                double[] pDoubleData = new double[500];
                fl.GetLargeResultData(i, bData, pDoubleData);
                String seriesNames = new String(bData, 0, 10).trim();
                String subTypeNames = new String(bData, 255, 10).trim();
                String fanDia = new String(bData, 510, 10).trim();
                String motorType = new String(bData, 765, 10).trim();// 电机类型"A" "三级能效电机";"B" "防爆电机";"C""双速电机";"D" "变频电机(IC416)";如果是‘无蜗壳风机’="变频电机直联(IC416)
                String rpm = String.valueOf(pDoubleData[0]);// 风机转速
                String efficiency = String.valueOf(pDoubleData[1]);// 全压内效率
                String absorberPower = String.valueOf(pDoubleData[3]);// 内功率
                String outletVelocity = String.valueOf(pDoubleData[6]);// 静压
                Double motor = pDoubleData[19];// 实际噪声
                String noise = String.valueOf(pDoubleData[38]);// 总A声压级
                String pt = String.valueOf(pDoubleData[8]);// 全压
                String maxSpeed = String.valueOf(pDoubleData[45]);// 风机最高转速R/MIN
                System.out.println("seriesNames:" + seriesNames);
                System.out.println("subTypeNames:" + subTypeNames);
                System.out.println("fanDia:" + fanDia);
                System.out.println("motorType:" + motorType);
                System.out.println("rpm:" + rpm);
                System.out.println("efficiency:" + efficiency);
                System.out.println("absorberPower:" + absorberPower);
                System.out.println("outletVelocity:" + outletVelocity);
                System.out.println("motor:" + motor);
                System.out.println("noise:" + noise);
                System.out.println("pt:" + pt);
                System.out.println("maxSpeed:" + maxSpeed);
            }
        } finally {
            fanLock.unlock();
        }
    }
}
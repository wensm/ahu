package com.carrier.ahu.engine.fan.factory;

import org.junit.Test;

import com.carrier.ahu.engine.fanYld4k.factory.FanYldDllInEx3ParamFactory;
import com.carrier.ahu.engine.fanYld4k.param.FanInParam;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by liangd4 on 2017/7/6.
 */
public class FanYldDllInEx3ParamFactoryTest implements SystemCalculateConstants {

    @Test //供应商为A 风机形式：无蜗壳风机 风机高度小于23
    public void packageParam01_test() throws Exception {
        System.out.println("packageParam01 test-FanYldDllInEx3ParamFactoryTest begin");
        FanYldDllInEx3ParamFactory fanYldDllInEx3ParamFactory = new FanYldDllInEx3ParamFactory();
        FanInParam fanInParam = new FanInParam();
        fanInParam.setAltitude(8000);//海拔高度
//        fanInParam.setAirVolume(18000);//风量(m3/h)
        fanInParam.setTotalStatic(300);//风压（Pa）
        fanInParam.setSerial("39G0608");//机组型号 39G0608
        fanInParam.setFanSupplier("A");//供应商
        fanInParam.setOutlet(FAN_OPTION_OUTLET_WWK);//风机形式 0：前弯或后弯 1：前弯 2：后弯 3:机翼型 7:无蜗壳风机 outlet 暂时不支持0
        System.out.println("packageParam01 package fanInParam success");
//        FanYldDllInParam wmkWindMachineOutParam = fanYldDllInEx3ParamFactory.packageParam(fanInParam);

        //根据海拔高度计算空气密度 需要调用DLl完成后放入pAirDensity字段
//        wmkWindMachineOutParam.setPAirDensity("空气密度需要计算值");

        System.out.println("packageParam01 test-FanYldDllInEx3ParamFactoryTest end");
//        assertEquals(wmkWindMachineOutParam, wmkWindMachineOutParam);
    }

    @Test //1.供应商为A 2.风机形式：前弯或后弯 3.风机系列<=7.1 4.风机高度小于23
    public void packageParam02_test() throws Exception {
        System.out.println("packageParam02 test-FanYldDllInEx3ParamFactoryTest begin");
        FanYldDllInEx3ParamFactory fanYldDllInEx3ParamFactory = new FanYldDllInEx3ParamFactory();
        FanInParam fanInParam = new FanInParam();
        fanInParam.setAltitude(8000);//海拔高度
//        fanInParam.setAirVolume(18000);//风量(m3/h)
        fanInParam.setTotalStatic(300);//风压（Pa）
        fanInParam.setSerial("39G0608");//机组型号 39G0608
        fanInParam.setFanSupplier("A");//供应商
        //KTQ系列<=7.1
        fanInParam.setOutlet("1");//风机形式 0：前弯或后弯 1：前弯 2：后弯 3:机翼型 7:无蜗壳风机 outlet 暂时不支持0
        System.out.println("packageParam02 package fanInParam success");
//        FanYldDllInParam wmkWindMachineOutParam = fanYldDllInEx3ParamFactory.packageParam(fanInParam);

        //根据海拔高度计算空气密度 需要调用DLl完成后放入pAirDensity字段
//        wmkWindMachineOutParam.setPAirDensity("空气密度需要计算值");

        System.out.println("packageParam02 test-FanYldDllInEx3ParamFactoryTest end");
//        assertEquals(wmkWindMachineOutParam, wmkWindMachineOutParam);
    }

    @Test //1.供应商为A 2.风机形式：前弯或后弯 3.风机系列>=8.0 4.风机高度小于23
    public void packageParam03_test() throws Exception {
        System.out.println("packageParam03 test-FanYldDllInEx3ParamFactoryTest begin");
        FanYldDllInEx3ParamFactory fanYldDllInEx3ParamFactory = new FanYldDllInEx3ParamFactory();
        FanInParam fanInParam = new FanInParam();
        fanInParam.setAltitude(0);//海拔高度
//        fanInParam.setAirVolume(18000);//风量(m3/h)
        fanInParam.setTotalStatic(300);//风压（Pa）
        fanInParam.setSerial("39CQ2532");//机组型号 39G2333
        fanInParam.setFanSupplier("A");//供应商
        //KTQ系列>=8.0
        fanInParam.setOutlet("2");//风机形式 0：前弯或后弯 1：前弯 2：后弯 3:机翼型 7:无蜗壳风机 outlet 暂时不支持0
        System.out.println("packageParam03 package fanInParam success");
//        FanYldDllInParam wmkWindMachineOutParam = fanYldDllInEx3ParamFactory.packageParam(fanInParam);

    }


    //2.1供应商：A、风机形式：不是无蜗壳风机、风机高度：小于23 风机系列为KTQ 系列<=7.1
    //2.2供应商：A、风机形式：不是无蜗壳风机、风机高度：小于23 风机系列为KTQ 系列>=8.0
    //2.3供应商：A、风机形式：不是无蜗壳风机、风机高度：小于23 风机系列为KTQ 系列>=8.0 系列<=7.1 调用谷科风机引擎
}
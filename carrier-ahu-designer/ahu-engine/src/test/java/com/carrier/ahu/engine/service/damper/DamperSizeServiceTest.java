package com.carrier.ahu.engine.service.damper;

import static com.carrier.ahu.common.enums.DamperMaterialEnum.AL;
import static com.carrier.ahu.common.enums.DamperMaterialEnum.FD;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39CQ;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.carrier.ahu.common.exception.engine.EngineException;
import com.carrier.ahu.vo.DamperPosSizeInputVO;
import com.carrier.ahu.vo.DamperPosSizeResultVO;

public class DamperSizeServiceTest {

    private DamperSizeService service;

    @Before
    public void setUp() {
        this.service = new DamperSizeServiceImpl();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAdjustDamperSize_throwIllegalArgumentException_givenNonProduct() {
        DamperPosSizeInputVO inputVo = new DamperPosSizeInputVO();
        this.service.adjustDamperSize(inputVo);
    }

    @Test
    public void testAdjustDamperSize_givenAL() {
        DamperPosSizeInputVO inputVo = new DamperPosSizeInputVO();
        inputVo.setProduct(AHU_PRODUCT_39CQ);
        inputVo.setDamperMaterial(AL);
        inputVo.setUnitLengthMode(23);
        inputVo.setModeA(4);
        inputVo.setModeB(4);
        inputVo.setUnitWidthMode(22);
        inputVo.setModeC(4);
        inputVo.setModeD(4);

        DamperPosSizeResultVO resultVo = this.service.adjustDamperSize(inputVo);
        assertNotNull(resultVo);

        assertEquals("466.0", String.valueOf(resultVo.getPosSizeA()));
        assertEquals("466.0", String.valueOf(resultVo.getPosSizeB()));
        assertEquals("471.0", String.valueOf(resultVo.getPosSizeC()));
        assertEquals("471.0", String.valueOf(resultVo.getPosSizeD()));

        assertEquals("1458.0", String.valueOf(resultVo.getSizeX()));
        assertEquals("1348.0", String.valueOf(resultVo.getSizeY()));
    }

    @Test
    public void testAdjustDamperSize_givenFD() {
        DamperPosSizeInputVO inputVo = new DamperPosSizeInputVO();
        inputVo.setProduct(AHU_PRODUCT_39CQ);
        inputVo.setDamperMaterial(FD);
        inputVo.setUnitLengthMode(14);
        inputVo.setModeA(4);
        inputVo.setModeB(4);
        inputVo.setUnitWidthMode(18);
        inputVo.setModeC(4);
        inputVo.setModeD(4);

        DamperPosSizeResultVO resultVo = this.service.adjustDamperSize(inputVo);
        assertNotNull(resultVo);

        assertEquals("467.0", String.valueOf(resultVo.getPosSizeA()));
        assertEquals("467.0", String.valueOf(resultVo.getPosSizeB()));
        assertEquals("547.5", String.valueOf(resultVo.getPosSizeC()));
        assertEquals("547.5", String.valueOf(resultVo.getPosSizeD()));

        assertEquals("556.0", String.valueOf(resultVo.getSizeX()));
        assertEquals("795.0", String.valueOf(resultVo.getSizeY()));
    }

    @Test(expected = EngineException.class)
    public void testAdjustDamperSize_throwException() {
        DamperPosSizeInputVO inputVo = new DamperPosSizeInputVO();
        this.service.adjustDamperSize(inputVo);
    }

}

package com.carrier.ahu.engine.fan;

import com.carrier.ahu.engine.fan.util.Utils;
import com.carrier.ahu.unit.CCRDFileUtil;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

/**
 * Created by LIANGD4 on 2017/12/10.
 */
public class FanGKServiceTest {
    // 能出东西即可，具体含义还待整合
    public static void main(String args[]) {
        Native.setProtected(false);
        for (int k = 1; k <= 1; k++) {
            try {
//                String record1 = "D:\\123";// 曲线图路径
                String path = "asserts/bmp/gk/";// 创建目录
                CCRDFileUtil.createDir(path);// 调用方法创建目录

                String record1 = path;// 曲线图路径

                String fn = "YFB(K)630";// 风机型号
                double Sealevel = 0;
                double airtemperature = 293;
                double pst = 566.6;// 总静压
                double ptt = 566.6;//
                double qvt = 40000.0;// 风量
                String motorposition = "H";// 电机位置 C:侧置 H:后置
                String fanoutlet = "A";// 风机出风方向 A: 0 B:90A C:90B D:180

                double Souspe1 = 0;
                double Souspe2 = 0;
                double Souspe3 = 0;
                double Souspe4 = 0;
                double Souspe5 = 0;
                double Souspe6 = 0;
                double Souspe7 = 0;
                double Souspe8 = 0;
                double Souspep1 = 0;
                double Souspep2 = 0;
                double Souspep3 = 0;
                double Souspep4 = 0;
                double Souspep5 = 0;
                double Souspep6 = 0;
                double Souspep7 = 0;
                double Souspep8 = 0;

                double nst = 0.0;// 转速
                boolean etr = false;// 转速,静压,全压判定
                boolean npt = true;// 转速,静压,全压判定
                double tpw = 0.0;// 电机功率
                double pwt = 0.0;
                double eft = 0.0;

                double co = 0.0;
                double js = 0.0;// TODO +

                boolean suc = true;
                double rla = 0.0;
                double ptt1 = 0.0;
                double maxmotorpower = 0.0;
                double maxspeed = 0.0;
                double outw = 0.0;
                double outH = 0.0;
                double fanL = 0.0;
                double FanW = 0.0;
                double fanH = 0.0;
                double PL = 0.0;

                int flg = 1;
                PointerByReference flcode = new PointerByReference();

                PointerByReference nflg = new PointerByReference(new Memory(4 * Native.getNativeSize(Boolean.TYPE)));

                double imped = 0.0;
                DoubleByReference dr = new DoubleByReference();

                DoubleByReference TEST = Utils.dRef(PL);

                DoubleByReference pwt1 = Utils.dRef(pwt);
                DoubleByReference eft1 = Utils.dRef(eft);
                IntByReference succ = new IntByReference();

                GKFanLib.GUKE_FAN_INSTANCE.Calculate(
                        record1,
                        Sealevel,
                        airtemperature,
                        fn,
                        pst,
                        qvt,
                        Utils.dRef(nst),
                        pwt1,
                        eft1,
                        Utils.dRef(ptt),
                        Utils.dRef(co),
                        Utils.dRef(tpw),
                        Utils.dRef(js),
                        motorposition,
                        fanoutlet,
                        Utils.dRef(Souspe1),
                        Utils.dRef(Souspe2),
                        Utils.dRef(Souspe3),
                        Utils.dRef(Souspe4),
                        Utils.dRef(Souspe5),
                        Utils.dRef(Souspe6),
                        Utils.dRef(Souspe7),
                        Utils.dRef(Souspe8),
                        Utils.dRef(Souspep1),
                        Utils.dRef(Souspep2),
                        Utils.dRef(Souspep3),
                        Utils.dRef(Souspep4),
                        Utils.dRef(Souspep5),
                        Utils.dRef(Souspep6),
                        Utils.dRef(Souspep7),
                        Utils.dRef(Souspep8),
                        true,
                        succ,
                        Utils.dRef(rla),
                        nflg,
                        Utils.dRef(ptt1),
                        Utils.dRef(maxmotorpower),
                        Utils.dRef(maxspeed),
                        Utils.dRef(outw),
                        Utils.dRef(outH),
                        Utils.dRef(fanL),
                        Utils.dRef(FanW),
                        Utils.dRef(fanH),
                        TEST,
                        flcode,
                        new PointerByReference(new Memory(k * Native.getNativeSize(Byte.TYPE))), dr);

                System.out.println(dr.getValue());
                System.out.println(pwt1.getValue());
                System.out.println(eft1.getValue());
                System.out.println(succ.getValue());
                return;
            } catch (Error e) {
                e.printStackTrace();
                System.out.println(Native.getLastError());
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(k);
            }
        }
    }
}

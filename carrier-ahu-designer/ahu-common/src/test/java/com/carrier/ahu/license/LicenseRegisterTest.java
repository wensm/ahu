package com.carrier.ahu.license;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Map;

import com.carrier.ahu.common.util.LOCALMAC;

/**
 * Created by liangd4 on 2018/2/28.
 */
public class LicenseRegisterTest {

    public static void main(String[] args) {
        try {
            String mac = LOCALMAC.getMAC();
            System.out.println("本机MAC地址：" + mac);
            Map<String, String> map = System.getenv();
            String userName = map.get("USERNAME");// 获取用户名
            String computerName = map.get("COMPUTERNAME");// 获取计算机名
            String userDomain = map.get("USERDOMAIN");// 获取计算机域名
            System.out.println("本机userName：" + userName);
            System.out.println("本机computerName：" + computerName);
            System.out.println("本机userDomain：" + userDomain);

            String localValue = LicenseRegister.getLocalValue(mac, userName, userDomain);
            // TODO LicenseRegister
            String target = LicenseRegister.publicKeyDecode("");
            System.out.println("注册验证是否通过：" + LicenseRegister.verify(target, localValue));


            LicenseInfo licenseInfo = new LicenseInfo();
            licenseInfo.setMac(mac);
            licenseInfo.setUserName(userName);
            licenseInfo.setDomain(userDomain);
//            licenseInfo.setDate();
//            licenseInfo.setFactory();
//            licenseInfo.setRole();
            LicenseGenerator.privateKeyEncode(licenseInfo);

            String licensePath = "D:\\Work\\ahu_20180203\\CarrierAHU\\carrier-ahu-designer\\ahu-common\\build\\classes\\main\\license.dat";
            LicenseGeneratorTester.publicKeyDecode(licensePath);
        } catch (UnknownHostException | SocketException e) {
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
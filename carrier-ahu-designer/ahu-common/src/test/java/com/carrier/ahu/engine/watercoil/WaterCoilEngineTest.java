package com.carrier.ahu.engine.watercoil;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by liaoyw on 2017/3/28.
 */
public class WaterCoilEngineTest {
    @Test
    public void calculate() {
        WaterCoilResult result = WaterCoilEngine.calculate("../cpc/2R FL/CW014_02R_FL.cpc", 2.5, 27, 19.5, 0,
                0, 0.99, 1, 1, 1, 1, 1380, 13.3096, 0.3302, 13.589,
                12.3952, 31.75, 18.8374, 339.08, 211,
                0.11938, 221.4, 8, 7.2, 5.5, 0, 1, 0, 0);
        Assert.assertNotNull(result);
        Assert.assertTrue(result.vwbrAve.getValue() - 0.23086492507435552 < 0.00001);
    }
}

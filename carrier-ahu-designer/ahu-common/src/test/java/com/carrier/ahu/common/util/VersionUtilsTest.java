package com.carrier.ahu.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.carrier.ahu.common.util.VersionUtils.VersionPair;

public class VersionUtilsTest {

    @Test
    public void testCompare() {
        assertEquals(0, VersionUtils.compare("1.0.0", "1.0.0"));
        assertEquals(1, VersionUtils.compare("1.0.1", "1.0.0"));
        assertEquals(-1, VersionUtils.compare("1.0.0", "1.0.1"));

        assertEquals(0, VersionUtils.compare("1.0", "1.0.0"));
        assertEquals(1, VersionUtils.compare("1.0.1", "1.0"));
        assertEquals(-1, VersionUtils.compare("1.1.1", "1.2"));

        assertEquals(0, VersionUtils.compare("1.10.0", "1.10"));
        assertEquals(1, VersionUtils.compare("2", "1.0.0"));
        assertEquals(-1, VersionUtils.compare("2.0", "2.0.1.1"));
    }

    @Test
    public void testParseVersionPair() {
        List<VersionPair> versionPairs = VersionUtils.parseVersionPairs("2.0.0");
        assertEquals(1, versionPairs.size());
        assertEquals("2.0.0", versionPairs.get(0).getStartVersion());
        assertEquals("2.0.0", versionPairs.get(0).getEndVersion());

        versionPairs = VersionUtils.parseVersionPairs("2.0.0-2.0.0");
        assertEquals(1, versionPairs.size());
        assertEquals("2.0.0", versionPairs.get(0).getStartVersion());
        assertEquals("2.0.0", versionPairs.get(0).getEndVersion());

        versionPairs = VersionUtils.parseVersionPairs("2.0.0-2.0.4");
        assertEquals(1, versionPairs.size());
        assertEquals("2.0.0", versionPairs.get(0).getStartVersion());
        assertEquals("2.0.4", versionPairs.get(0).getEndVersion());

        versionPairs = VersionUtils.parseVersionPairs("2.0.0,2.0.1-2.0.4,2.0.5");
        assertEquals(3, versionPairs.size());
        assertEquals("2.0.0", versionPairs.get(0).getStartVersion());
        assertEquals("2.0.0", versionPairs.get(0).getEndVersion());
        assertEquals("2.0.1", versionPairs.get(1).getStartVersion());
        assertEquals("2.0.4", versionPairs.get(1).getEndVersion());
        assertEquals("2.0.5", versionPairs.get(2).getStartVersion());
        assertEquals("2.0.5", versionPairs.get(2).getEndVersion());
    }

    @Test
    public void testWithinVersionPair() {
        List<VersionPair> versionPairs = VersionUtils.parseVersionPairs("2.0.0-2.0.4");
        assertEquals(1, versionPairs.size());
        assertEquals("2.0.0", versionPairs.get(0).getStartVersion());
        assertEquals("2.0.4", versionPairs.get(0).getEndVersion());
        assertFalse(VersionUtils.withinVersionPair("1.10.0", versionPairs.get(0)));
        assertTrue(VersionUtils.withinVersionPair("2.0.0", versionPairs.get(0)));
        assertTrue(VersionUtils.withinVersionPair("2.0.1", versionPairs.get(0)));
        assertTrue(VersionUtils.withinVersionPair("2.0.2", versionPairs.get(0)));
        assertTrue(VersionUtils.withinVersionPair("2.0.4", versionPairs.get(0)));
        assertFalse(VersionUtils.withinVersionPair("2.0.5", versionPairs.get(0)));
    }

    @Test
    public void testWithinVersionPairs() {
        List<VersionPair> versionPairs = VersionUtils.parseVersionPairs("2.0.0-2.0.4,2.0.6-2.10.4");
        assertEquals(2, versionPairs.size());

        assertFalse(VersionUtils.withinVersionPairs("1.10.0", versionPairs));
        assertTrue(VersionUtils.withinVersionPairs("2.0.0", versionPairs));
        assertTrue(VersionUtils.withinVersionPairs("2.0.1", versionPairs));
        assertTrue(VersionUtils.withinVersionPairs("2.0.2", versionPairs));
        assertTrue(VersionUtils.withinVersionPairs("2.0.4", versionPairs));
        assertFalse(VersionUtils.withinVersionPairs("2.0.5", versionPairs));
        assertTrue(VersionUtils.withinVersionPairs("2.0.64", versionPairs));
        assertTrue(VersionUtils.withinVersionPairs("2.10.1", versionPairs));
        assertTrue(VersionUtils.withinVersionPairs("2.10.4", versionPairs));
        assertFalse(VersionUtils.withinVersionPairs("2.10.5", versionPairs));
    }
}

package com.carrier.ahu.util;

import org.junit.Test;

/**
 * Created by liaoyw on 2017/4/16.
 */
public class ExecUtilsTest {
    @Test
    public void TestCreatebmp() {
        System.out.println("starting now...");
        long start = System.currentTimeMillis();
        ExecUtils.CommandResult r = ExecUtils.executeCmd("D:/workspace/delphi/cc/Project2.exe d:/ot.bmp d:/UnitConig - old.ini");
        System.out.println(r);
        System.out.printf("create bmp cost %d ms\n", System.currentTimeMillis() - start);
    }
}

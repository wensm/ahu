package com.carrier.ahu.license;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LicenseManagerTest {

    @Test
    public void testValidateLicenseVersionStringString() {
        assertFalse(LicenseManager.validateLicenseVersion("0.9", "1.0"));
        assertTrue(LicenseManager.validateLicenseVersion("1.0", "1.0"));
        assertTrue(LicenseManager.validateLicenseVersion("1.0", "1.0.0"));
        assertTrue(LicenseManager.validateLicenseVersion("1.0.0", "1.0"));
        assertTrue(LicenseManager.validateLicenseVersion("1.0.1", "1.0"));
        assertTrue(LicenseManager.validateLicenseVersion("1.0.12", "1.0"));
        assertTrue(LicenseManager.validateLicenseVersion("1.0.2", "1.0.1"));
        assertFalse(LicenseManager.validateLicenseVersion("1.0", "1.0.10"));
        assertFalse(LicenseManager.validateLicenseVersion("1.0.9", "1.0.10"));
        assertTrue(LicenseManager.validateLicenseVersion("1.0.11", "1.0.10"));
    }

}

package com.carrier.ahu.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;

import com.carrier.ahu.unit.CCRDFileUtil;

/**
 * 文件工具类
 * 
 * @author Simon
 */
public class FileUtil {

    /**
     * 文件读取缓冲区大小
     */
    private static final int CACHE_SIZE = 1024;

    /**
     * 获得类的基路径，打成jar包也可以正确获得路径
     * 
     * @return
     */
    public static String getBasePath() {
        String filePath = FileUtil.class.getProtectionDomain().getCodeSource().getLocation().getFile();

        if (filePath.endsWith(".jar")) {
            filePath = filePath.substring(0, filePath.lastIndexOf("/"));
            try {
                filePath = URLDecoder.decode(filePath, "UTF-8"); // 解决路径中有空格%20的问题
            } catch (UnsupportedEncodingException ex) {

            }

        }
        File file = new File(filePath);
        filePath = file.getAbsolutePath();
        return filePath;
    }

    /** */
    /**
     * <p>
     * 文件转换为二进制数组
     * </p>
     * 
     * @param filePath 文件路径
     * @return
     * @throws Exception
     */
    public static byte[] fileToByte(String filePath) throws Exception {
        byte[] data = new byte[0];
        File file = new File(filePath);
        if (file.exists()) {
            FileInputStream in = new FileInputStream(file);
            ByteArrayOutputStream out = new ByteArrayOutputStream(2048);
            byte[] cache = new byte[CACHE_SIZE];
            int nRead = 0;
            while ((nRead = in.read(cache)) != -1) {
                out.write(cache, 0, nRead);
                out.flush();
            }
            out.close();
            in.close();
            data = out.toByteArray();
        }
        return data;
    }

    /** */
    /**
     * <p>
     * 二进制数据写文件
     * </p>
     * 
     * @param bytes 二进制数据
     * @param filePath 文件生成目录
     */
    public static void byteArrayToFile(byte[] bytes, String filePath) throws Exception {
        File destFile = new File(filePath);
        byteArrayToFile(bytes, destFile);
    }

    public static void byteArrayToFile(byte[] bytes, File destFile) throws Exception {
        InputStream in = new ByteArrayInputStream(bytes);
        CCRDFileUtil.createFile(destFile.getPath());
        // destFile.createNewFile();

        OutputStream out = new FileOutputStream(destFile);
        byte[] cache = new byte[CACHE_SIZE];
        int nRead = 0;
        while ((nRead = in.read(cache)) != -1) {
            out.write(cache, 0, nRead);
            out.flush();
        }
        out.close();
        in.close();
    }

    public static void writeZipFile(String zipFilePath, List<File> srcFiles) throws IOException {
        FileOutputStream fos = new FileOutputStream(zipFilePath);
        ZipOutputStream zipOut = new ZipOutputStream(fos);

        for (File srcFile : srcFiles) {
            FileInputStream fis = new FileInputStream(srcFile);
            ZipEntry zipEntry = new ZipEntry(srcFile.getName());
            zipOut.putNextEntry(zipEntry);

            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
        }

        zipOut.close();
        fos.close();
    }

    public static File writeFile(String filePath, String content) throws IOException {
        File file = new File(filePath);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream o = new FileOutputStream(file);
        o.write(content.getBytes(Charset.forName("UTF-8")));
        o.close();
        return file;
    }

    /**
     * Unzip .zip file into dest path.
     * 
     * @param destPath
     * @param zipInStream
     * @throws Exception
     */
    public static void unzipFile(String destPath, InputStream zipInStream) throws Exception {
        ZipInputStream zipIn = null;
        FileOutputStream out = null;
        byte[] buffer = new byte[1024];
        try {
            zipIn = new ZipInputStream(zipInStream);
            ZipEntry entry = zipIn.getNextEntry();
            while (entry != null) {
                if (!entry.isDirectory()) { // ignore directory
                    File file = new File(destPath + entry.getName());
                    File dir = file.getParentFile();
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    file.createNewFile();
                    out = new FileOutputStream(file);
                    int len = 0;
                    while ((len = zipIn.read(buffer)) > 0) {
                        out.write(buffer, 0, len);
                    }
                    out.close();
                }
                entry = zipIn.getNextEntry();
            }
        } finally {
            IOUtils.closeQuietly(zipIn);
            IOUtils.closeQuietly(out);
        }
    }

    /**
     * Zip files and directors.
     * 
     * @param sourceFiles
     * @param zipFileName
     * @throws Exception
     */
    public static void zipFiles(List<File> sourceFiles, String zipFileName) throws Exception {
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
        BufferedOutputStream bos = new BufferedOutputStream(out);
        for (File sourceFile : sourceFiles) {
            compress(out, bos, sourceFile, sourceFile.getName());
        }
        bos.close();
        out.close();
    }

    private static void compress(ZipOutputStream out, BufferedOutputStream bos, File sourceFile, String base)
            throws Exception {
        if (sourceFile.exists()) {
            if (sourceFile.isDirectory()) {
                File[] files = sourceFile.listFiles();
                if (files.length == 0) {
                	try {
                    out.putNextEntry(new ZipEntry(base + "/"));
                	}catch(Exception e) {
                		System.out.println(e.getMessage());
                	}catch(Error er) {
                		System.out.println(er.getMessage());
                	}
                } else {
                    for (int i = 0; i < files.length; i++) {
                        compress(out, bos, files[i], base + "/" + files[i].getName());
                    }
                }
            } else {
                out.putNextEntry(new ZipEntry(base));
                FileInputStream fos = new FileInputStream(sourceFile);
                BufferedInputStream bis = new BufferedInputStream(fos);
                int tag;
                while ((tag = bis.read()) != -1) {
                    out.write(tag);
                }
                bis.close();
                fos.close();
            }
        }
    }

}

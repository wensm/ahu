package com.carrier.ahu.length.param;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class AddtionalParam {

   
    private String serial;//机组型号
    private boolean uvLamp;//空段是否有检修灯

}

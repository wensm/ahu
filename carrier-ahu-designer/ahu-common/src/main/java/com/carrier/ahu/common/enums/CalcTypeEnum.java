package com.carrier.ahu.common.enums;

/**
 * 批量配置类型</br>
 */
public enum CalcTypeEnum {

    PERFORMANCE_KEEP("performanceKeep", "性能保持", "Performance Keep", "3"),

    OPTIMUM("optimum", "性能最优", "Optimal Performance", "2"),

    ECONOMICAL("economical", "最经济", "Most Economical", "1"),

    NONE("none", "无要求", "None", "0");

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    private String id;// code标识
    private String cnName;// 中文名称
    private String name;// 英文名称
    private String value;// 值

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCnName() {
        return this.cnName;
    }

    CalcTypeEnum(String id, String cnName, String name, String value) {
        this.id = id;
        this.cnName = cnName;
        this.name = name;
        this.value = value;
    }

    public static CalcTypeEnum getById(String id) {
        for (CalcTypeEnum code : values()) {
            if (code.getId().equals(id)) {
                return code;
            }
        }
        return null;
    }

    public static CalcTypeEnum getByValue(String value) {
        for (CalcTypeEnum code : values()) {
            if (code.getValue().equals(value)) {
                return code;
            }
        }
        return null;
    }
}

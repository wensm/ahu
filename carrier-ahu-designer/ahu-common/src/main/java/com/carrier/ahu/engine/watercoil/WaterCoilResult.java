package com.carrier.ahu.engine.watercoil;

import com.sun.jna.ptr.DoubleByReference;

import static com.carrier.ahu.engine.watercoil.Utils.dRef;

/**
 * Created by liaoyw on 2017/3/27.
 */
public class WaterCoilResult {
    DoubleByReference waterTemperatureDelta;//水温升
    DoubleByReference waterFlowRate;//水流量
    DoubleByReference totalCap = dRef();//冷量
    DoubleByReference sensibleCap = dRef();//显冷量
    DoubleByReference airDrop = dRef();// 空气阻力
    DoubleByReference waterDrop = dRef();//水阻
    DoubleByReference dryBulbTemperatureOut = dRef();//出风干球温度
    DoubleByReference wetBulbTemperatureOut = dRef();//出风湿球温度
    DoubleByReference waterTemperatureOut = dRef();//出水温度
    DoubleByReference bpf = dRef();//bypass
    DoubleByReference vwbrAve = dRef();//介质流速

    WaterCoilResult(double waterTemperatureDelta, double waterFlowRate) {
        this.waterTemperatureDelta = dRef(waterTemperatureDelta);
        this.waterFlowRate = dRef(waterFlowRate);
    }

    public double getWaterTemperatureDelta() {
        return waterTemperatureDelta.getValue();
    }

    public double getWaterFlowRate() {
        return waterFlowRate.getValue();
    }

    public double getTotalCap() {
        return totalCap.getValue();
    }

    public double getSensibleCap() {
        return sensibleCap.getValue();
    }

    public double getAirDrop() {
        return airDrop.getValue();
    }

    public double getWaterDrop() {
        return waterDrop.getValue();
    }

    public double getDryBulbTemperatureOut() {
        return dryBulbTemperatureOut.getValue();
    }

    @Override
    public String toString() {
        return "WaterCoilResult{" +
                "waterTemperatureDelta=" + waterTemperatureDelta +
                ", waterFlowRate=" + waterFlowRate +
                ", totalCap=" + totalCap +
                ", sensibleCap=" + sensibleCap +
                ", airDrop=" + airDrop +
                ", waterDrop=" + waterDrop +
                ", dryBulbTemperatureOut=" + dryBulbTemperatureOut +
                ", wetBulbTemperatureOut=" + wetBulbTemperatureOut +
                ", waterTemperatureOut=" + waterTemperatureOut +
                ", bpf=" + bpf +
                ", vwbrAve=" + vwbrAve +
                '}';
    }

    public double getWetBulbTemperatureOut() {
        return wetBulbTemperatureOut.getValue();
    }

    public double getWaterTemperatureOut() {
        return waterTemperatureOut.getValue();
    }

    public double getBpf() {
        return bpf.getValue();
    }

    public double getVwbrAve() {
        return vwbrAve.getValue();
    }
}

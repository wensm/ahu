package com.carrier.ahu.license;

import static com.carrier.ahu.constant.CommonConstant.ROLE_Design;
import static com.carrier.ahu.constant.CommonConstant.SYS_PATH_LICENSE_FILE;
import static com.carrier.ahu.constant.CommonConstant.SYS_PATH_LICENSE_PATH;

import java.io.File;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Map;

import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.common.util.LOCALMAC;
import com.carrier.ahu.common.util.VersionUtils;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.util.DateUtil;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by Braden Zhou on 2018/04/25.
 */
public class LicenseManager {

    private static LicenseManager MANAGER = new LicenseManager();

    private LicenseInfo licenseInfo = null;

    private LicenseManager() {
        loadLicenseInfo();
    }

    private void loadLicenseInfo() {
        licenseInfo = LicenseParser.parseUserLicenseFile(SYS_PATH_LICENSE_PATH + SYS_PATH_LICENSE_FILE);
    }

    public static LicenseInfo getLicenseInfo() {
        return MANAGER.licenseInfo;
    }
    public static boolean isDesign() {
        return MANAGER.licenseInfo.getRole().equals(ROLE_Design);
    }

    public static LicenseInfo loadLicenseInfo(String ahuVersion) {
        if (MANAGER.licenseInfo == null || !validateLicenseVersion(ahuVersion)) {
            MANAGER.loadLicenseInfo();
        }
        return MANAGER.licenseInfo;
    }

    public static String getUserRole() {
        if (MANAGER.licenseInfo != null) {
            return MANAGER.licenseInfo.getRole();
        }
        return null;
    }

    public static String getUserName() {
        if (MANAGER.licenseInfo != null) {
            return MANAGER.licenseInfo.getUserName();
        }
        return null;
    }

    public static String getFactory() {
        if (MANAGER.licenseInfo != null) {
            return MANAGER.licenseInfo.getFactory();
        }
        return null;
    }

    public static String getVersion() {
        if (MANAGER.licenseInfo != null) {
            return MANAGER.licenseInfo.getVersion();
        }
        return null;
    }
    public static String getPatchVersion() {
        if (MANAGER.licenseInfo != null) {
            return MANAGER.licenseInfo.getPatchVersion();
        }
        return null;
    }

    /**
     * 申请证书加密字符串, 经过Base64和URL编码转换
     * 
     * @param licenseInfo
     * @return
     * @throws Exception
     */
    public static String encodeLicenseApplication(LicenseInfo licenseInfo) throws Exception {
        String licenseString = LicenseGenerator.encodeLicenseString(licenseInfo);
        return URLEncoder.encode(licenseString, "UTF-8");
    }

    public static LicenseInfo getMachineLicenseInfo() throws Exception {
        Map<String, String> map = System.getenv();
        LicenseInfo licenseInfo = new LicenseInfo();
        licenseInfo.setMac(LOCALMAC.getMAC());
        licenseInfo.setComputerName(map.get(CommonConstant.SYS_MAP_COMPUTERNAME));
        licenseInfo.setUserName(map.get(CommonConstant.SYS_MAP_USERNAME));
        licenseInfo.setDomain(map.get(CommonConstant.SYS_MAP_USERDOMAIN));
        return licenseInfo;
    }

    public static LicenseInfo getLicenseInfo(byte[] encodedLicense) {
        if (encodedLicense != null) {
            return LicenseParser.parseUserLicenseFile(encodedLicense);
        }
        return null;
    }

    public static LicenseInfo getLicenseInfo(String encodedLicense) {
        return getLicenseInfo(Base64.getDecoder().decode(encodedLicense));
    }

    public static LicenseInfo getLicenseInfo(File licenseFile) {
        return LicenseParser.parseUserLicenseFile(licenseFile);
    }

    public static void installLicense(byte[] bytes) throws Exception {
        FileUtil.byteArrayToFile(bytes, SYS_PATH_LICENSE_PATH + SYS_PATH_LICENSE_FILE);
        MANAGER.loadLicenseInfo(); // load again
    }

    public static void generateLicenseFile(LicenseInfo licenseInfo, File licenseFile) throws Exception {
        LicenseGenerator.privateKeyEncode(licenseInfo, licenseFile);
    }

    /**
     * Valid license version 1.101 - 1.110 </br>
     * Invalid license version 1.101 - 1.21, 1.9 - 2.0
     * 
     * @param ahuVersion
     * @return
     */
    public static boolean validateLicenseVersion(String ahuVersion) {
        String licenseVersion = MANAGER.licenseInfo.getVersion();
        return validateLicenseVersion(licenseVersion, ahuVersion);
    }

    public static boolean validateLicenseVersion(String licenseVersion, String ahuVersion) {
        return VersionUtils.compare(licenseVersion, ahuVersion) >= 0;
    }

    /**
     * 校验license 是否有效 开发人员不需要考虑版本问题。
     * 
     * @param licenseInfo
     * @param ahuVersion
     * @param isDevUser
     * @return
     * @throws Exception
     */
    public static boolean validateLicenseInfo(LicenseInfo licenseInfo, String ahuVersion, boolean isDevUser)
            throws Exception {
        boolean retBool = validateLicenseInfoWithOutVersion(licenseInfo, ahuVersion);
        if (isDevUser) {
            return retBool;
        } else {
            return retBool && LicenseManager.validateLicenseVersion(licenseInfo.getVersion(), ahuVersion);
        }
    }

    public static boolean validateLicenseInfoWithOutVersion(LicenseInfo licenseInfo, String ahuVersion)
            throws Exception {
        LicenseInfo machineInfo = getMachineLicenseInfo();
        return EmptyUtil.isNotEmpty(licenseInfo)
                && machineInfo.getComputerName().equalsIgnoreCase(licenseInfo.getComputerName())
                //&& machineInfo.getDomain().equalsIgnoreCase(licenseInfo.getDomain()) //忽略域名
                && EmptyUtil.isNotEmpty(licenseInfo.getDate())
                && (DateUtil.getNow().before(DateUtil.strToDate(licenseInfo.getDate())));
    }
}

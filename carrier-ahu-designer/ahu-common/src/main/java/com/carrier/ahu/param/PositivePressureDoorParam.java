package com.carrier.ahu.param;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public  class  PositivePressureDoorParam {
    //正压门情况S1-无需更改正压门 S2-需要更改正压门大小 S3-需要增加段长
    private String situation;

    //提高后检修门安装高度
    private int doorInstallH;

    //检修门高
    private int doorH;

    //检修门宽
    private int doorW;

    //检修门尺寸
    private String door;

    //段长
    private int segL;
}

package com.carrier.ahu.util;

import com.carrier.ahu.common.entity.UserConfigParam;
import com.google.gson.Gson;

import java.util.Map;

public class UserConfigUtil {
    public static UserConfigParam getUserConfig(String userConfigString) {
        if (!EmptyUtil.isEmpty(userConfigString)) {
            Gson gson = new Gson();
            Map<String, Object> mapUserConfig = gson.fromJson(userConfigString, Map.class);
            UserConfigParam userConfigParam = new UserConfigParam();
            userConfigParam.setMapConfig(mapUserConfig);
            return userConfigParam;
        }
        return null;
    }
}

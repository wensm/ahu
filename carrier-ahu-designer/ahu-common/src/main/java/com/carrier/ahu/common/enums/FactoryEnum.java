package com.carrier.ahu.common.enums;

/**
 * Factory enum for AHU factories in the world and support in AHU Selection Tool
 * System. Which provide some metadata helper at the moment.
 * 
 * When system load the user license, A Factory should be set into
 * AHUContext. And now we will use THC as default factory if no one have been
 * specified. This should be validated when check license file during start up.
 * 
 * Created by Braden Zhou on 2018/06/15.
 */
public enum FactoryEnum {

    THC, TICA;

}

package com.carrier.ahu.model.calunit;

import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.section.meta.MetaCodeGen;
import lombok.Data;

import java.util.Map;

/**
 * 标准的段参数，与AhuParam配置使用，作为标准的服务计算入口
 */
@Data
public class PartParam implements Comparable<PartParam> {

    private String partid;// 段ID
    private String pid;// 项目ID
    private String unitid;// AHU ID
    private String key;
    private Short orders;
    private Short types;
    private Short position;
    private Short setup;
    private String pic;
    private String groupi;
    private Double cost;
    private Short avdirection;
    private Map<String, Object> params;
    private Map<String, Object> preParams;//当前段前一个段
    private Map<String, Object> nextParams;//当前段后一个段
    private Map<String, Object> nextNextParams;//当前段后后一个段

    private String lenMixofFanInVerticalDirection;//立式机组上层的风机，和下层的顶部出风的功能段垂直方向重叠，需要提醒，本属性记录下层混合段右面所有段长的和。

    public PartParam(short s) {
        this.types = s;
    }

    public PartParam() {
    }

    @Override
    public int compareTo(PartParam o) {
        return this.position - o.position;
    }

    public <T> void setParamValue(String key, Object value) {

        this.getParams().put(genPartMetaKey(key), value);
    }
    
    public <T> void setAHUParamValue(String key, Object value) {
        this.getParams().put(MetaCodeGen.genAHUParamName(key), value);
    }

    @SuppressWarnings("unchecked")
	public <T> T getParamValue(String key, Class<T> c) {

        if (key == null) {
            return null;
        }
        
        Object val = this.getParams().get(genPartMetaKey(key));
        if(key.startsWith(CommonConstant.METASEXON_META_PREFIX)) {
        	val=this.getParams().get(key);;
        }
        
        if (val == null) {
            return null;
        }

        String strVal = val.toString().trim();

        String className = c.getName();
        if (CommonConstant.SYS_STRING_JAVA_LANG_STRING.equals(className)) {
            return c.cast(val.toString());
        } else if (CommonConstant.SYS_STRING_JAVA_LANG_BOOLEAN.equals(className)) {

            if (CommonConstant.SYS_BLANK.equals(strVal)) {
				return (T) Boolean.FALSE;
            }

            return c.cast(Boolean.parseBoolean(val.toString()));
        } else if (CommonConstant.SYS_STRING_JAVA_LANG_INTEGER.equals(className)) {
            if (CommonConstant.SYS_BLANK.equals(strVal)) {
                return (T) Integer.valueOf(0);
            }
            return c.cast(Integer.parseInt(val.toString()));
        } else if (CommonConstant.SYS_STRING_JAVA_LANG_DOUBLE.equals(className)) {
            if (CommonConstant.SYS_BLANK.equals(strVal)) {
                return (T) Double.valueOf(0.0);
            }
            return c.cast(Double.parseDouble(val.toString()));
        } else if (CommonConstant.SYS_STRING_JAVA_LANG_FLOAT.equals(className)) {
            if (CommonConstant.SYS_BLANK.equals(strVal)) {
                return (T) Float.valueOf(0f);
            }
            return c.cast(Float.parseFloat(val.toString()));
        }
        return c.cast(val);

    }

    private String genPartMetaKey(String keyNM) {
        return MetaCodeGen.calculateAttributePrefix(this.getKey()) + CommonConstant.SYS_PUNCTUATION_DOT + keyNM;
    }
    
    

//    public static final String Resistance = "Resistance";
//    public static final String sectionL = "sectionL";
//    public static final String RResistance = "RResistance";//回风侧阻力
//    public static final String IResistance = "IResistance";//进风侧阻力
}

package com.carrier.ahu.common.enums;

public enum DamperPosEnum {

    Top, Bottom, Left, Right, Back;

}

package com.carrier.ahu.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.carrier.ahu.po.AbstractPo;

import java.io.Serializable;

/**
 * Created by Zhang Zhiqiang on 2017/6/30.
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@Entity
public class AhuGroupBind extends AbstractPo implements Serializable {
	private static final long serialVersionUID = -1972700562235954895L;
	@Id
	String unitid;// 项目ID
	String groupid;// 主键ID
}

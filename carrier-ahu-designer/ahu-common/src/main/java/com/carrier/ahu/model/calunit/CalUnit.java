package com.carrier.ahu.model.calunit;

/**
 * This class describes the attribute unit, which will be called during switching the unit style;
 * 
 * @author JL
 *
 */
public enum CalUnit {
	
//	AirVolume("Air Volume", "m3/h", "ft3/min(CFM)", 1.669 , 1, "风量"),
//	WindSpeed("WindSpeed", "m/s", "ft/min(FPM)", 0.00508 , 1, "面风速"),
//	AirPressure("AirPressure", "Pa", "in wg", 249.0889 , 1, "压力(空气）"),
//	WaterPressure_FTWG("WaterPressure_FTWG", "kPa", "ft wg", 3.048 , 1, "压力（水）ft wg"),
//	WaterPressure_PSI("WaterPressure_PSI", "kPa", "PSI", 6.89 , 1, "压力（水）PSI"),
//	Power("Power", "kW", "HP", 1 , 1.34, "功率"),
//	CoolVolume("CoolVolume", "kW", "Btu/h", 0.000293, 1, "冷量"),
//	HotVolume("HotVolume", "kW", "Btu/h",  0.000293, 1, "热量"),
//	Tempreture("Tempreture", "℃", "F", 1, CalUnit.STATIC_FUN_DEGREE, "温度"),
//	WaterSpeedM("WaterSpeedM", "m/s", "ft/s", 1, 3.281, "水流量m/s"),
//
//	WaterSpeedM3("WaterSpeedM3", "m3/h", "gal/mim(GPM)", 1, 57.06, "水流量m3/h"),
//
//	HumidVolume("HumidVolume", "kg/h", "lb/min", 1, 0.03674371, "除（加）湿量"),
//	LengthIN("LengthIN", "mm", "in", 25.4, 1, "尺寸in"),
//	LengthFT("LengthFT", "mm", "ft", 304.8, 1, "尺寸ft"),
//	Weight("Weight", "kg", "lb", 0.4535924, 1, "重量"),
//	AreaFt2("AreaFt2", "m2", "ft2", 0.0929, 1, "面积ft2"),
//	AreaIN2("AreaIN2", "m2", "in2", 0.0000064516, 1, "面积in2"),
//	VOLUME("VOLUME", "m3", "gal(US)", 0.03785412, 1, "体积"),
//	Density("Density", "kg/m3", "lb/ft3", 16.0185, 1, "密度"),
//
//	Percentage("Percentage", "%", "%", 1, 1, "百分比"),
//	Length("Length", "m", "m", 1, 1, "长度");
//
//	public static final double STATIC_FUN_DEGREE = 9.9999;
//
//	private String name;
//	//公制
//	private String metric;
//	//英制
//	private String british;
//	//以英制单位的公制值
//	private double obxm;
//	//以公制为单位的英制值
//	private double omxb;
//	//描述信息
//	private String memo;
//
//	public String getName() {
//		return this.name;
//	}
//
//	private CalUnit(String name, String metric, String british, double obxm, double omxb, String memo) {
//		this.name = name;
//		this.metric = metric;
//		this.british = british;
//		this.obxm = obxm;
//		this.omxb = omxb;
//		this.memo = memo;
//	}
//
//	public String getMetric() {
//		return metric;
//	}
//
//	public String getBritish() {
//		return british;
//	}
//
//	public double getObxm() {
//		return obxm;
//	}
//
//	public double getOmxb() {
//		return omxb;
//	}
//
//	public String getMemo() {
//		return memo;
//	}
//
//	/**
//	 * 公制-》英制
//	 * @param metric
//	 * @return
//	 */
//	public double toBritish(double metric) {
//		return (metric*omxb)/obxm;
//	}
//
//	/**
//	 * 英制 -》 公制
//	 * @param british
//	 * @return
//	 */
//	public double toMetric(double british) {
//		return (british*obxm)/omxb;
//	}
//	public double resolve(double d) {
//		if (d == STATIC_FUN_DEGREE) {
//			return 5/(9*(d-32));
//		} else {
//			return d;
//		}
//	}
}

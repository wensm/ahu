package com.carrier.ahu.length;

import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.length.param.LengthParam;

/**
 * Created by liangd4 on 2017/9/11.
 * 消音段逻辑
 */
public class ClearSoundLen {

    //计算段长
    public double getLength(LengthParam lengthParam) {
        if(CommonConstant.JSON_ATTENUATOR_DEADENING_A.equals(lengthParam.getDeadening())){//一级：固定值6M
            return 6.00;
        }else{//二级：固定值6M
            return 12.00;
        }
    }

}

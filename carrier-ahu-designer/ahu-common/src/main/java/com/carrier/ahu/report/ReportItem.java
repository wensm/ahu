package com.carrier.ahu.report;

import lombok.Data;

@Data
public class ReportItem {
	private String classify;
	private String name;
	private boolean output;
}

package com.carrier.ahu.report;

import lombok.Data;

@Data
public class Report {
	private String type;
	private String name;
	private String path;
	private String classify;
	private boolean output;
	private boolean hasOutputItems;//没有打印ahu基础信息（第一次打印items 不加入newpage，第一次之后items项加入newpage）
	private ReportItem[] items;
}

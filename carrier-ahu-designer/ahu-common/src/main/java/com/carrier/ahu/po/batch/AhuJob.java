package com.carrier.ahu.po.batch;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class AhuJob {
	String unitId;// 机组ID
	String unitNo;// 机组编号
	String drawingNo;// 机组图纸编号
	String status;// 状态
	List<PartJob> data;
}

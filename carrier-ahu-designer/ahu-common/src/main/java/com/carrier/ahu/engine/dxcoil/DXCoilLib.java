package com.carrier.ahu.engine.dxcoil;

import com.carrier.ahu.constant.CommonConstant;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.ShortByReference;

/**
 * Created by liaoyw on 2017/3/27.
 */
interface DXCoilLib extends Library {

//    String DX_WATER_COIL_DLL = "asserts\\file\\coildx\\EvapAir1Dll.dll";//直接蒸发式
    

    DXCoilLib DX_INSTANCE = (DXCoilLib) Native.loadLibrary(CommonConstant.SYS_PATH_DX_WATER_COIL_DLL, DXCoilLib.class);

    void RUNEVAPAIR1_DLL(
            DoubleByReference Vel_face,
            DoubleByReference Tdb_air_in,
            DoubleByReference Twb_air_in,
            DoubleByReference P_air_in,
            ShortByReference RefIndex,
            DoubleByReference dTsh,
            DoubleByReference SST,
            DoubleByReference SCT,
            DoubleByReference Tliq,
            DoubleByReference zUA,
            DoubleByReference zdpr,
            DoubleByReference zdpa,
            ShortByReference iDllUnits,
            IntByReference itube,
            IntByReference nface,
            DoubleByReference ptube,
            DoubleByReference tlenx,
            DoubleByReference Dotube,
            DoubleByReference cuTtube,
            DoubleByReference Nrow,
            DoubleByReference Prow,
            IntByReference ncur,
            DoubleByReference Dfin,
            DoubleByReference tfin,
            IntByReference Nciri,
            IntByReference Nciro,
            DoubleByReference ntubciro,
            DoubleByReference istck,
            DoubleByReference idflow,
            DoubleByReference Cap_total,
            DoubleByReference Cap_sensible,
            DoubleByReference DP_air,
            DoubleByReference DP_ref,
            DoubleByReference Tdb_air_out,
            DoubleByReference Twb_air_out,
            DoubleByReference wmr_ref,
            IntByReference error_flag
    );

/*    void RUNEVAPAIR1_DLL(
            Pointer Vel_face,
            Pointer Tdb_air_in,
            Pointer Twb_air_in,
            Pointer P_air_in,
            Pointer RefIndex,
            Pointer dTsh,
            Pointer SST,
            Pointer SCT,
            Pointer Tliq,
            Pointer iDllUnits,
            Pointer zUA,
            Pointer zdpr,
            Pointer zdpa,
            Pointer itube,
            Pointer nface,
            Pointer ptube,
            Pointer tlenx,
            Pointer Dotube,
            Pointer cuTtube,
            Pointer Nrow,
            Pointer Prow,
            Pointer ncur,
            Pointer Dfin,
            Pointer tfin,
            Pointer Nciri,
            Pointer Nciro,
            Pointer ntubciro,
            Pointer istck,
            Pointer idflow,
            Pointer Cap_total,
            Pointer Cap_sensible,
            Pointer DP_air,
            Pointer DP_ref ,
            Pointer  Tdb_air_out,
            Pointer Twb_air_out,
            Pointer wmr_ref
    );*/


    /*void RUNEVAPAIR1_DLL(
            double Vel_face,
            double Tdb_air_in,
            double Twb_air_in,
            double P_air_in,
            int RefIndex,
            double dTsh,
            double SST,
            double SCT,
            double Tliq,
            int iDllUnits,
            double zUA,
            double zdpr,
            double zdpa,
            int itube,
            int nface,
            double ptube,
            double tlenx,
            double Dotube,
            double cuTtube,
            int Nrow,
            double Prow,
            int ncur,
            double Dfin,
            double tfin,
            int Nciri,
            int Nciro,
            int ntubciro,
            int istck,
            int idflow,
            double Cap_total,
            double Cap_sensible,
            double DP_air,
            double DP_ref ,
            double  Tdb_air_out,
            double Twb_air_out,
            double wmr_ref
    );*/

}

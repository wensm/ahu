package com.carrier.ahu.engine.fan;

import com.carrier.ahu.constant.CommonConstant;
import com.sun.jna.Native;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.win32.StdCallLibrary;

/**
 * Created by liaoyw on 2017/3/30.
 */
public interface GKFanLib extends StdCallLibrary {

    GKFanLib GUKE_FAN_INSTANCE = (GKFanLib) Native.loadLibrary(CommonConstant.SYS_PATH_GUKE_FAN_DLL, GKFanLib.class);


    /*void Calculate(String record1, String fn, DoubleByReference pst, DoubleByReference qvt, DoubleByReference nst, String motorposition, String fanoutlet, DoubleByReference pwt, DoubleByReference eft, DoubleByReference ptt, DoubleByReference co, DoubleByReference tpw,
                    Pointer qva, Pointer Pta, Pointer Efa, Pointer Pwa, Pointer souspe, Pointer souspep,
                    boolean etr, boolean suc, DoubleByReference rla, boolean npt, DoubleByReference ptt1, DoubleByReference maxmotorpower, DoubleByReference maxspeed, DoubleByReference outw, DoubleByReference outH, DoubleByReference fanL, DoubleByReference FanW,
                    DoubleByReference fanH, DoubleByReference PL, String flcode, String dlcode, DoubleByReference imped);
*/
    void Calculate(String record1, double Sealevel, double airtemperature,
                   String fn, double pst, double qvt, DoubleByReference nst,
                   DoubleByReference pwt, DoubleByReference eft,
                   DoubleByReference ptt, DoubleByReference co, DoubleByReference tpw,
                   DoubleByReference js, String motorposition, String fanoutlet,
                   DoubleByReference Souspe1, DoubleByReference Souspe2,
                   DoubleByReference Souspe3, DoubleByReference Souspe4,
                   DoubleByReference Souspe5, DoubleByReference Souspe6,
                   DoubleByReference Souspe7, DoubleByReference Souspe8,
                   DoubleByReference Souspep1, DoubleByReference Souspep2,
                   DoubleByReference Souspep3, DoubleByReference Souspep4,
                   DoubleByReference Souspep5, DoubleByReference Souspep6,
                   DoubleByReference Souspep7, DoubleByReference Souspep8,
                   boolean etr, IntByReference suc, DoubleByReference rla,
                   PointerByReference npt, DoubleByReference ptt1,
                   DoubleByReference maxmotorpower, DoubleByReference maxspeed,
                   DoubleByReference outw, DoubleByReference outH,
                   DoubleByReference fanL, DoubleByReference FanW,
                   DoubleByReference fanH, DoubleByReference PL,
                   PointerByReference flcode, PointerByReference dlcode,
                   DoubleByReference imped);
}

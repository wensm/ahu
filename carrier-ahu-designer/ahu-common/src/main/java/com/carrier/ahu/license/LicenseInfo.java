package com.carrier.ahu.license;

import lombok.Data;

/**
 * Created by liangd4 on 2018/2/28.
 */
@Data
public class LicenseInfo {
    private String mac;//物理地址
    private String userName;//用户名
    private String computerName;//计算机名
    private String domain;//域名
    private String date;//到期日期
    private String factory;//工厂
    private String role;//角色
    private String languageCode;
    private String version;
    private String patchVersion;//补丁版本
}

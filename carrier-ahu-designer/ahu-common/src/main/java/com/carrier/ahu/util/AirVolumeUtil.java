package com.carrier.ahu.util;

public class AirVolumeUtil {

    /**
     * 重新封装送风风量
     *
     * @param sairvolume      送风风量
     * @param appendAirVolume 新增风量
     * @return 重新封装送风风量
     */
    public static double packageSAirVolume(double sairvolume, int appendAirVolume) {
        return sairvolume + appendAirVolume;
    }
    
    /**
     * 重新封装送风风量
     *
     * @param sairvolume      送风风量
     * @param appendAirVolume 新增风量
     * @return 重新封装送风风量
     */
    public static int packageSAirVolumeToInt(double sairvolume, int appendAirVolume) {
        return (int) Math.round(sairvolume + appendAirVolume);
    }
    
}

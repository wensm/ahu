package com.carrier.ahu.common.enums;

import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.common.entity.UserMetaId;
import com.carrier.ahu.constant.CommonConstant;

/**
 * User Meta Keys.
 * 
 * Created by Braden Zhou on 2018/07/23.
 */
public enum UserMetaEnum {

    /**
     * 系统全局参数新增枚举Key维护：在此新增枚举和默认值
     */
    COLD_COOLING_SELECTION_SIZE(CommonConstant.SYS_STRING_NUMBER_5),
    EXPORT_MODE(CommonConstant.SYS_STRING_NUMBER_1),
    TEMPERATURE_TRANSMIT(CommonConstant.SYS_ASSERT_FALSE),
    CAL_COILFAN_LISTPRICE(CommonConstant.SYS_ASSERT_FALSE);

    public UserMetaId newUserMetaId(String userId) {
        return new UserMetaId(userId, this.name());
    }

    public UserMeta newUserMeta(String userId) {
        return new UserMeta(new UserMetaId(userId, this.name()));
    }

    //默认值
    private String defaultValue;
    private UserMetaEnum(String defaultValue) {
        this.defaultValue = defaultValue;
    }
    public String getDefaultValue() {
        return defaultValue;
    }
}

package com.carrier.ahu.common.exception.panel;

import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class PanelException extends AhuException {

    public PanelException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public PanelException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public PanelException(String message, Throwable cause) {
        super(message, cause);
    }

    public PanelException(String message) {
        super(message);
    }

}

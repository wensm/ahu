package com.carrier.ahu.calculator;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class IOUtils {
	@SuppressWarnings("rawtypes")
	public static InputStreamReader getReader(Class clazz, String priceTableName) throws IOException {
		InputStream is = clazz.getResourceAsStream(priceTableName);
		InputStreamReader reader = new InputStreamReader(is, Charset.forName("UTF-8"));
		return reader;
	}

	@SuppressWarnings("rawtypes")
	public static InputStreamReader getReaderFromClassloader(Class clazz, String priceTableName) throws IOException {
		InputStream is = clazz.getClassLoader().getResourceAsStream(priceTableName);
		InputStreamReader reader = new InputStreamReader(is, Charset.forName("UTF-8"));
		return reader;
	}
}

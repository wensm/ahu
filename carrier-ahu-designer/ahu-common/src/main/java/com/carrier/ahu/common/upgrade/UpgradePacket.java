package com.carrier.ahu.common.upgrade;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;

import com.carrier.ahu.constant.CommonConstant;

import lombok.Data;

/**
 * Created by Braden Zhou on 2018/07/18.
 */
@Data
public class UpgradePacket {

    private String header;
    private String bodyLength;
    private String body;
    private String md5;

    public UpgradePacket() {

    }

    public UpgradePacket(String body) {
        this.body = body;
        this.md5 = getMD5String(body);
    }

    private String getMD5String(String string) {
        StringBuilder md5 = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            byte[] bytes = md.digest();
            for (int offset = 0, len = bytes.length; offset < len; offset++) {
                String haxHex = Integer.toHexString(bytes[offset] & 0xFF);
                if (haxHex.length() < 2) {
                    md5.append("0");
                }
                md5.append(haxHex);
            }

        } catch (NoSuchAlgorithmException e) {
        }
        return md5.toString();
    }

    public byte[] getPacketBytes() {
        byte[] bodyBytes = this.body.getBytes();
        int bodyLength = bodyBytes.length;
        int socketLength = CommonConstant.SYS_UPGRADE_HEADER_LENGTH + CommonConstant.SYS_UPGRADE_BODY_LENGTH_LENGTH + bodyLength + CommonConstant.SYS_UPGRADE_MD5_LENGTH;
        int index = 0;

        byte[] messageBytes = new byte[socketLength];
        messageBytes[0] = 0x1A;
        messageBytes[1] = 0x1B;
        messageBytes[2] = 0x1C;
        index += 3;

        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMinimumIntegerDigits(8);
        numberFormat.setGroupingUsed(false);
        byte[] lengthBytes = numberFormat.format(bodyLength).getBytes();
        for (int i = 0; i < CommonConstant.SYS_UPGRADE_BODY_LENGTH_LENGTH; i++) {
            messageBytes[index++] = lengthBytes[i];
        }
        for (int i = 0; i < bodyLength; i++) {
            messageBytes[index++] = bodyBytes[i];
        }
        byte[] md5Bytes = this.md5.getBytes();
        for (int i = 0; i < md5Bytes.length; i++) {
            messageBytes[index++] = md5Bytes[i];
        }
        return messageBytes;
    }

    public static UpgradePacket getPacket(byte[] messsageBytes) {
        String header = bytesToString(messsageBytes, 0, CommonConstant.SYS_UPGRADE_HEADER_LENGTH);
        String bodyLength = bytesToString(messsageBytes, CommonConstant.SYS_UPGRADE_HEADER_LENGTH, CommonConstant.SYS_UPGRADE_HEADER_LENGTH + CommonConstant.SYS_UPGRADE_BODY_LENGTH_LENGTH);
        String body = bytesToString(messsageBytes, CommonConstant.SYS_UPGRADE_HEADER_LENGTH + CommonConstant.SYS_UPGRADE_BODY_LENGTH_LENGTH,
                messsageBytes.length - CommonConstant.SYS_UPGRADE_MD5_LENGTH);
        String md5 = bytesToString(messsageBytes, messsageBytes.length - CommonConstant.SYS_UPGRADE_MD5_LENGTH, messsageBytes.length);

        UpgradePacket message = new UpgradePacket();
        message.setHeader(header);
        message.setBodyLength(bodyLength);
        message.setBody(body);
        message.setMd5(md5);
        return message;
    }

    public static String bytesToString(byte[] bytes, int start, int end) {
        String str = "";
        int len = end - start;
        if (bytes.length < len) {
            return str;
        }
        byte[] bs = new byte[len];
        for (int i = 0; i < len; i++) {
            bs[i] = bytes[start++];
        }
        str = new String(bs);
        return str;
    }

    public static boolean validateHeader(byte[] header) {
        if (header.length == 3 && header[0] == 0x1A && header[1] == 0x1B && header[2] == 0x1C) {
            return true;
        }
        return false;
    }

}

package com.carrier.ahu.engine.fan;

import com.carrier.ahu.constant.CommonConstant;
import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KrugerFanLib {

    private static final Logger logger = LoggerFactory.getLogger(KrugerFanLib.class.getName());
    private static ActiveXComponent dotnetCom = new ActiveXComponent("CLSID:07CC2C23-BF0A-4BC4-BD3E-331C7F3ACC13");
//    private static ActiveXComponent dotnetCom = new ActiveXComponent("CLSID:E4E02431-F0C5-4330-8654-28D74584EE06");
//    private static ActiveXComponent dotnetCom = new ActiveXComponent("IID:07CC2C23-BF0A-4BC4-BD3E-331C7F3ACC13");
//    private static ActiveXComponent dotnetCom = new ActiveXComponent("Ecofresh_Intl_Dll_8_1.DRIClass");····

    private KrugerFanLib() {
    }

    private static KrugerFanLib lib = new KrugerFanLib();

    public static KrugerFanLib getInstance() {
        if (lib == null) {
            lib = new KrugerFanLib();
        }
        return lib;
    }

    private Object getSelect(KrugerSelectInfo krugerSelectInfo) throws Exception {
        return Dispatch.call(dotnetCom, "select");
    }

    public KrugerSelectInfo calculate(KrugerSelectInfo krugerSelectInfo) throws Exception {
        try {
            krugerSelectInfo.setRecordDirectory(CommonConstant.SYS_PATH_KRUGER_FAN_DLL);
            krugerSelectInfo.setVolume(18000);//volume 风量
            krugerSelectInfo.setPressure(400);//press 总静压
            krugerSelectInfo.setAltitude(0);//attitude 海拔高度 * 1000
            krugerSelectInfo.setMinStyle(00000003);//cMinstyle FDA:00000003 BDB:00000009 ADA:00000005
            krugerSelectInfo.setProductType(00000000);//msSC
            krugerSelectInfo.setVolumeUnit(00000001);//vuM3H
            krugerSelectInfo.setPressureUnit(00000000);//puPa
            krugerSelectInfo.setPressureType(00000000);//ptStatic
            krugerSelectInfo.setCallType(0);//固定参数
            krugerSelectInfo.setDebug(0);//固定参数
            krugerSelectInfo.setHz(50);//固定参数
            krugerSelectInfo.setFanSize(0);//固定参数
            krugerSelectInfo.setTemperature(20);//固定参数
            krugerSelectInfo.setMinClass(00000000);//FcI
            krugerSelectInfo.setSoundCondition(00000000);//scRoom
            krugerSelectInfo.setFanCasing(00000002);//fcsingleframe
            krugerSelectInfo.setFanWidth(00000000);//fwsingle
            krugerSelectInfo.setVelocityUnit(00000000);//vuMS
            krugerSelectInfo.setAltitudeUnit(00000001);//auM
            krugerSelectInfo.setOutletType(00000000);//otducted
            krugerSelectInfo.setServiceFactor(1.2f);//固定参数
            krugerSelectInfo.setAltitudeUnit(00000000);//auFT
            krugerSelectInfo.setSoundDistance(1);//固定参数
            krugerSelectInfo.setSoundDistanceUnit(00000001);//duM
            krugerSelectInfo.setTemperatureUnit(00000000);//degreeC
            Object o = getSelect(krugerSelectInfo);
            return (KrugerSelectInfo) o;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ComThread.Release();
        }
        return null;
    }

    public static void main(String args[]) {
        try {
            logger.info("KrugerFanLib begin");
            KrugerSelectInfo krugerSelectInfo = KrugerFanLib.getInstance().calculate(new KrugerSelectInfo());
            logger.info("KrugerFanLib success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.carrier.ahu.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * Set S_ID data
 * 
 * @author
 *
 */
public class AirConditionDvalue {

	Map<Integer, Double> map = new TreeMap<Integer, Double>();

	static AirConditionDvalue datas = new AirConditionDvalue();

	public AirConditionDvalue() {
		map.put(-20, 0.63);
		map.put(-19, 0.7);
		map.put(-18, 0.77);
		map.put(-17, 0.85);
		map.put(-16, 0.93);
		map.put(-15, 1.01);
		map.put(-14, 1.11);
		map.put(-13, 1.22);
		map.put(-12, 1.34);
		map.put(-11, 1.46);
		map.put(-10, 1.6);
		map.put(-9, 1.75);
		map.put(-8, 1.91);
		map.put(-7, 2.08);
		map.put(-6, 2.27);
		map.put(-5, 2.47);
		map.put(-4, 2.69);
		map.put(-3, 2.94);
		map.put(-2, 3.19);
		map.put(-1, 3.47);
		map.put(0, 3.78);
		map.put(1, 4.07);
		map.put(2, 4.37);
		map.put(3, 4.7);
		map.put(4, 5.03);
		map.put(5, 5.4);
		map.put(6, 5.79);
		map.put(7, 6.21);
		map.put(8, 6.65);
		map.put(9, 7.13);
		map.put(10, 7.63);
		map.put(11, 8.15);
		map.put(12, 8.75);
		map.put(13, 9.35);
		map.put(14, 9.97);
		map.put(15, 10.6);
		map.put(16, 11.4);
		map.put(17, 12.1);
		map.put(18, 12.9);
		map.put(19, 13.8);
		map.put(20, 14.7);
		map.put(21, 15.6);
		map.put(22, 16.6);
		map.put(23, 17.7);
		map.put(24, 18.8);
		map.put(25, (double) 20);
		map.put(26, 21.4);
		map.put(27, 22.6);
		map.put(28, (double) 24);
		map.put(29, 25.6);
		map.put(30, 27.2);
		map.put(31, 28.8);
		map.put(32, 30.6);
		map.put(33, 32.5);
		map.put(34, 34.4);
		map.put(35, 36.6);
		map.put(36, 38.8);
		map.put(37, 41.1);
		map.put(38, 43.5);
		map.put(39, (double) 46);
		map.put(40, 48.8);
		map.put(41, 51.7);
		map.put(42, 54.8);
		map.put(43, (double) 58);
		map.put(44, 61.3);
		map.put(45, (double) 65);
		map.put(46, 68.9);
		map.put(47, 72.8);
		map.put(48, (double) 77);
		map.put(49, 81.5);
		map.put(50, 86.2);
		map.put(55, (double) 114);
		map.put(60, (double) 152);
		map.put(65, (double) 204);
		map.put(70, (double) 276);
		map.put(75, (double) 382);
		map.put(80, (double) 545);
		map.put(85, (double) 828);
		map.put(90, (double) 1400);
		map.put(95, (double) 3120);

	}

	/**
	 * get max data less than tem
	 * 
	 * @param lVarTB
	 * @return
	 */
	public static double getBackwardValue(double lVarTB) {

		if (lVarTB > 50) {
			if (lVarTB % 5 == 0) {
				return lVarTB;
			}
			double position = Math.ceil(lVarTB / 5);
			return position * 5;
		}
		
		

		return Math.floor(lVarTB);

	}

	/**
	 * get min data greater than tem
	 * 
	 * @param tem
	 * @return
	 */
	public static double getForwardValue(double tem) {

		if (tem >= 50) {
			if (tem % 5 == 0) {
				return tem+5;
			}
			double position = Math.ceil(tem / 5);
			return (position + 1) * 5;
		}

		if(Math.ceil(tem)==tem){
			return tem+1;
		}
		return Math.ceil(tem);

	}

	/**
	 * get db value
	 * 
	 * @param lVarTB
	 * @return
	 */
	public static double getDbValue(double lVarTB) {
		return datas.map.get(Double.valueOf((Math.floor(processValue(lVarTB)))).intValue());
	}
	
	/**
	 * 
	 * @param parmTB
	 * @return
	 */
	public static double processValue(double parmTB){
		
		if (parmTB < AirConditionData.MINTEMPERATURE) {
			return AirConditionData.MINTEMPERATURE;
		}

		if (parmTB > AirConditionData.MAXTEMPERATURE) {
			return AirConditionData.MAXTEMPERATURE;
		}
		
		return parmTB;
	}

}

package com.carrier.ahu.po;

public class Unitpro {
    private String pid;

    private String unitid;

    private String product;

    private Double sairvolume;

    private Double sexternalstatic;

    private String smodel;

    private Double eairvolume;

    private Double eexternalstatic;

    private String emodel;

    private String unitconfig;

    private String pipeorientation;

    private String doororientation;

    private String inskinm;

    private String inskinw;

    private String inskincolor;

    private String exskinm;

    private String exskinw;

    private String exskincolor;

    private String panelinsulation;

    private String delivery;

    private String voltage;

    private String memo;

    private Short groups;

    private String isgroup;

    private Short isemodel;

    private String indirection;

    private String ispanel;

    private String isv1;

    private String basetype;

    private Boolean isprerain;

    private Double attitude;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getUnitid() {
        return unitid;
    }

    public void setUnitid(String unitid) {
        this.unitid = unitid == null ? null : unitid.trim();
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product == null ? null : product.trim();
    }

    public Double getSairvolume() {
        return sairvolume;
    }

    public void setSairvolume(Double sairvolume) {
        this.sairvolume = sairvolume;
    }

    public Double getSexternalstatic() {
        return sexternalstatic;
    }

    public void setSexternalstatic(Double sexternalstatic) {
        this.sexternalstatic = sexternalstatic;
    }

    public String getSmodel() {
        return smodel;
    }

    public void setSmodel(String smodel) {
        this.smodel = smodel == null ? null : smodel.trim();
    }

    public Double getEairvolume() {
        return eairvolume;
    }

    public void setEairvolume(Double eairvolume) {
        this.eairvolume = eairvolume;
    }

    public Double getEexternalstatic() {
        return eexternalstatic;
    }

    public void setEexternalstatic(Double eexternalstatic) {
        this.eexternalstatic = eexternalstatic;
    }

    public String getEmodel() {
        return emodel;
    }

    public void setEmodel(String emodel) {
        this.emodel = emodel == null ? null : emodel.trim();
    }

    public String getUnitconfig() {
        return unitconfig;
    }

    public void setUnitconfig(String unitconfig) {
        this.unitconfig = unitconfig == null ? null : unitconfig.trim();
    }

    public String getPipeorientation() {
        return pipeorientation;
    }

    public void setPipeorientation(String pipeorientation) {
        this.pipeorientation = pipeorientation == null ? null : pipeorientation.trim();
    }

    public String getDoororientation() {
        return doororientation;
    }

    public void setDoororientation(String doororientation) {
        this.doororientation = doororientation == null ? null : doororientation.trim();
    }

    public String getInskinm() {
        return inskinm;
    }

    public void setInskinm(String inskinm) {
        this.inskinm = inskinm == null ? null : inskinm.trim();
    }

    public String getInskinw() {
        return inskinw;
    }

    public void setInskinw(String inskinw) {
        this.inskinw = inskinw == null ? null : inskinw.trim();
    }

    public String getInskincolor() {
        return inskincolor;
    }

    public void setInskincolor(String inskincolor) {
        this.inskincolor = inskincolor == null ? null : inskincolor.trim();
    }

    public String getExskinm() {
        return exskinm;
    }

    public void setExskinm(String exskinm) {
        this.exskinm = exskinm == null ? null : exskinm.trim();
    }

    public String getExskinw() {
        return exskinw;
    }

    public void setExskinw(String exskinw) {
        this.exskinw = exskinw == null ? null : exskinw.trim();
    }

    public String getExskincolor() {
        return exskincolor;
    }

    public void setExskincolor(String exskincolor) {
        this.exskincolor = exskincolor == null ? null : exskincolor.trim();
    }

    public String getPanelinsulation() {
        return panelinsulation;
    }

    public void setPanelinsulation(String panelinsulation) {
        this.panelinsulation = panelinsulation == null ? null : panelinsulation.trim();
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery == null ? null : delivery.trim();
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage == null ? null : voltage.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public Short getGroups() {
        return groups;
    }

    public void setGroups(Short groups) {
        this.groups = groups;
    }

    public String getIsgroup() {
        return isgroup;
    }

    public void setIsgroup(String isgroup) {
        this.isgroup = isgroup == null ? null : isgroup.trim();
    }

    public Short getIsemodel() {
        return isemodel;
    }

    public void setIsemodel(Short isemodel) {
        this.isemodel = isemodel;
    }

    public String getIndirection() {
        return indirection;
    }

    public void setIndirection(String indirection) {
        this.indirection = indirection == null ? null : indirection.trim();
    }

    public String getIspanel() {
        return ispanel;
    }

    public void setIspanel(String ispanel) {
        this.ispanel = ispanel == null ? null : ispanel.trim();
    }

    public String getIsv1() {
        return isv1;
    }

    public void setIsv1(String isv1) {
        this.isv1 = isv1 == null ? null : isv1.trim();
    }

    public String getBasetype() {
        return basetype;
    }

    public void setBasetype(String basetype) {
        this.basetype = basetype == null ? null : basetype.trim();
    }

    public Boolean getIsprerain() {
        return isprerain;
    }

    public void setIsprerain(Boolean isprerain) {
        this.isprerain = isprerain;
    }

    public Double getAttitude() {
        return attitude;
    }

    public void setAttitude(Double attitude) {
        this.attitude = attitude;
    }
}
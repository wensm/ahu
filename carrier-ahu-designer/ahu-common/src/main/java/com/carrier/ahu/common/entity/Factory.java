package com.carrier.ahu.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.carrier.ahu.po.AbstractPo;

import java.io.Serializable;

/**
 * 工厂实体
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
public class Factory extends AbstractPo implements Serializable {
	private static final long serialVersionUID = -422508031871638L;
	@Id
	@GeneratedValue
	String id;// 工厂编码
	String cname;// 工厂全称
	String ename;// 英文名
	String address;// 地址
	String representative;// 法人代表
	String phone;// 电话
	String fax;// 传真
	String postalcode;// 邮编

}
package com.carrier.ahu.length.param;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/17.
 */
@Data
public class ModelSelectionParam {

    /*高压喷雾加湿段*/
    private double SHumidificationQ;//夏季加湿量
    private double WHumidificationQ;//冬季加湿量
    private String supplier;//供应商
    private boolean EnableSummer;//夏季生效
    private boolean EnableWinter;//冬季生效

    /*干蒸汽加湿段*/
//    private double ShumidificationQ;//夏季加湿量
//    private double WhumidificationQ;//冬季加湿量
    private String inputVaporPressure;//蒸汽压⼒


}

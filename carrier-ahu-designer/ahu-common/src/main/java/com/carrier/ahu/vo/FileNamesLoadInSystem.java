package com.carrier.ahu.vo;

/**
 * 该类记录的是所有loading进内存的文件名称及路径
 * 
 * @author jaaka
 *
 */
public class FileNamesLoadInSystem {
	/** 面板布置截图存放目录 */
	public static final String PANEL_IMG_PATH = "asserts/output/panel/{0}";
	/** 面板布置初始化数据存放目录 */
	public static final String PANEL_INIT_PATH = "asserts/input/panelarrange/39CQOperate/";

	public static final String INTL_JSON_FILE_PATH = "intl/%s.json";

	/** 段属性列表 */
	public static final String SECTIONS_XLSX_NAME = "sections/ahu.section.parameters1.3.xls";
	/** 段下拉值列表文件路径 */
	public static final String SECTIONS_VALUE_JSON_NAME = "sections/values/{0}.json";
	/* 输入框验证文件路径 */
	public static final String SECTIONS_VALIDATION_JSON_NAME = "sections/validation/{0}.json";
	/* 机组型号和段的验证 */
	public static final String SECTIONS_SERIAL_JSON_NAME = "sections/serial/{0}.json";
	/** 机组模版数据 */
	public static final String AHU_TEMPLATE_JSON = "ahu_template.json";
	/** 属性默认数据 */
	public static final String DEFAULT_PARAMS_JSON = "data/default_params.json";
	/** 空气默认数据 */
	public static final String DEFAULT_AIR_PARAM_JSON = "/data/default_air_param.json";

	/** 面板 - 侧面(操作面)数据表 */
	public static final String PANEL_BIGOPERATE_XLSX = "panel/BigOperate.xlsx";
	/** 面板 - 侧面(非操作面)数据表 */
	public static final String PANEL_NO_BIGOPERATE_XLSX = "panel/NoBigOperate.xlsx";
	/** 面板 - 端面数据表 */
	public static final String PANEL_NORMALSIDE_XLSX = "panel/NormalSide.xlsx";//39CQ普通端面无风阀及出风口
	public static final String PANEL_SIDEDAMPER_XLSX= "panel/SideDamper.xlsx";//39CQ端面带风阀
	public static final String PANEL_SIDEFANSENDFACEPOSITIONB_XLSX= "panel/SideFanSendFacePositionB.xlsx";//风机段端面出风_  后置(电机位置)
	public static final String PANEL_SIDEFANSENDFACEPOSITIONS_XLSX= "panel/SideFanSendFacePositionS.xlsx";//风机段端面出风_  侧置(电机位置)
	public static final String PANEL_SIDEFANSENDTOPPOSITIONB_XLSX= "panel/SideFanSendTopPositionB.xlsx";//风机段顶面出风_ 后置(电机位置)
	public static final String PANEL_SIDEFANSENDTOPPOSITIONS_XLSX= "panel/SideFanSendTopPositionS.xlsx";//风机段顶面出风_ 侧置(电机位置)
	public static final String PANEL_SIDEFANWWK_XLSX= "panel/SideFanwwk.xlsx";//风机段端面无蜗壳风机
	public static final String PANEL_SIDECOMBINEDMIXINGCHAMBERFIVEPANEL_XLSX = "panel/SideCombinedMixingChamberFivePanel.xlsx";//新回排端面，第五个段中间面

	/** 面板 - 顶面数据表 */
	public static final String PANEL_TOP_XLSX = "panel/Top.xlsx";
	/** 面板 - 底面数据表 */
	public static final String PANEL_BOTTOM_XLSX = "panel/Bottom.xlsx";

	/** 价格 - 编码数据表 */
	public static final String PRICE_CODERULE_XLSX = "price/coderule.xlsx";


	/** 面板 - 39CQ 39G 新回排中间面JSON 切割 */
	public static final String PANEL_CQ_G_MIDDLE_PANEL_XLSX = "panel/CQGCombinedMixingChamberFivePanel/{0}.json";

}


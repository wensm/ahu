package com.carrier.ahu.util;

public class AirConditionBean {
	// 干球温度℃
	double parmT;
	// 湿球温度
	double parmTb;
	// 相对湿度
	double parmF;
	// 含湿量
	double paramD;
	// 焓
	double paramI;
	
	// 露点温度
	double paramTd;

	public double getParamTd() {
		return paramTd;
	}

	public void setParamTd(double paramTd) {
		this.paramTd = paramTd;
	}

	boolean flg=true;

	public boolean isFlg() {
		return flg;
	}

	public void setFlg(boolean flg) {
		this.flg = flg;
	}

	public double getParmT() {
		return parmT;
	}

	public void setParmT(double parmT) {
		this.parmT = parmT;
	}

	public double getParmTb() {
		return parmTb;
	}

	public void setParmTb(double parmTb) {
		this.parmTb = parmTb;
	}

	public double getParmF() {
		return parmF;
	}

	public void setParmF(double parmF) {
		this.parmF = parmF;
	}

	public double getParamD() {
		return paramD;
	}

	public void setParamD(double paramD) {
		this.paramD = paramD;
	}

	public double getParamI() {
		return paramI;
	}

	public void setParamI(double paramI) {
		this.paramI = paramI;
	}

}

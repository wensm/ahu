package com.carrier.ahu.report;

import java.util.List;

public class ReportFile {
	private String type;
	private String name;
	private Boolean output;
	private String path;
	private List<ReportFileItem> items;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getOutput() {
		return output;
	}

	public void setOutput(Boolean output) {
		this.output = output;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<ReportFileItem> getItems() {
		return items;
	}

	public void setItems(List<ReportFileItem> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "ReportFile [type=" + type + ", name=" + name + ", output=" + output + ", path=" + path + ", items="
				+ items + "]";
	}

}

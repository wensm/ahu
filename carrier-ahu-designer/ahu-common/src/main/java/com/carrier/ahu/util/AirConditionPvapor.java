package com.carrier.ahu.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * Set S_ID data
 * 
 * @author
 *
 */
public class AirConditionPvapor {

	Map<Integer, Double> map = new TreeMap<Integer, Double>();

	static AirConditionPvapor datas = new AirConditionPvapor();

	public AirConditionPvapor() {
		map.put(-20, 1.02);
		map.put(-19, 1.13);
		map.put(-18, 1.25);
		map.put(-17, 1.37);
		map.put(-16, 1.5);
		map.put(-15, 1.65);
		map.put(-14, 1.81);
		map.put(-13, 1.98);
		map.put(-12, 2.17);
		map.put(-11, 2.37);
		map.put(-10, 2.59);
		map.put(-9, 2.83);
		map.put(-8, 3.09);
		map.put(-7, 3.36);
		map.put(-6, 3.67);
		map.put(-5, (double) 4);
		map.put(-4, 4.36);
		map.put(-3, 4.75);
		map.put(-2, 5.16);
		map.put(-1, 5.61);
		map.put(0, 6.09);
		map.put(1, 6.56);
		map.put(2, 7.04);
		map.put(3, 7.57);
		map.put(4, 8.11);
		map.put(5, 8.7);
		map.put(6, 9.32);
		map.put(7, 9.99);
		map.put(8, 10.7);
		map.put(9, 11.46);
		map.put(10, 12.25);
		map.put(11, 13.09);
		map.put(12, 13.99);
		map.put(13, 14.94);
		map.put(14, 15.95);
		map.put(15, 17.01);
		map.put(16, 18.13);
		map.put(17, 19.32);
		map.put(18, 20.59);
		map.put(19, 21.92);
		map.put(20, 23.31);
		map.put(21, 24.8);
		map.put(22, 26.37);
		map.put(23, 28.02);
		map.put(24, 29.77);
		map.put(25, 31.6);
		map.put(26, 33.53);
		map.put(27, 35.56);
		map.put(28, 37.71);
		map.put(29, 39.95);
		map.put(30, 42.32);
		map.put(31, 44.82);
		map.put(32, 47.43);
		map.put(33, 50.18);
		map.put(34, 53.07);
		map.put(35, 56.1);
		map.put(36, 59.26);
		map.put(37, 62.6);
		map.put(38, 66.09);
		map.put(39, 69.75);
		map.put(40, 73.58);
		map.put(41, 77.59);
		map.put(42, 81.8);
		map.put(43, 86.18);
		map.put(44, 90.79);
		map.put(45, 95.6);
		map.put(46, 100.61);
		map.put(47, 105.87);
		map.put(48, 111.33);
		map.put(49, 117.07);
		map.put(50, 123.04);
		map.put(55, 156.94);
		map.put(60, 198.7);
		map.put(65, 249.38);
		map.put(70, 310.82);
		map.put(75, 384.5);
		map.put(80, 472.28);
		map.put(85, 576.69);
		map.put(90, 699.31);
		map.put(95, 843.09);

	}

	/**
	 * get max data less than tem
	 * 
	 * @param lVarTB
	 * @return
	 */
	public static double getBackwardValue(double lVarTB) {

		if (lVarTB > 50) {
			if (lVarTB % 5 == 0) {
				return lVarTB;
			}
			double position = Math.floor(lVarTB / 5);
			return position * 5;
		}

		return Math.floor(lVarTB);

	}

	/**
	 * get min data greater than tem
	 * 
	 * @param tem
	 * @return
	 */
	public static double getForwardValue(double tem) {

		if (tem >= 50) {
			if (tem % 5 == 0) {
				return tem+5;
			}
			double position = Math.ceil(tem / 5);
			return position * 5;
		}

		if(Math.ceil(tem)==tem){
			return tem+1;
		}
		return Math.ceil(tem);

	}

	/**
	 * get db value
	 * 
	 * @param lVarTB
	 * @return
	 */
	public static double getDbValue(double lVarTB) {
		return datas.map.get(((Double) (Math.floor(processValue(lVarTB)))).intValue());
	}

	/**
	 * 
	 * @param parmTB
	 * @return
	 */
	public static double processValue(double parmTB) {

		if (parmTB < AirConditionData.MINTEMPERATURE) {
			return AirConditionData.MINTEMPERATURE;
		}

		if (parmTB > AirConditionData.MAXTEMPERATURE) {
			return AirConditionData.MAXTEMPERATURE;
		}

		return parmTB;
	}

}

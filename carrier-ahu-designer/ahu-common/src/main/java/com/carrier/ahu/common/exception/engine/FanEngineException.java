package com.carrier.ahu.common.exception.engine;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class FanEngineException extends EngineException {

    public FanEngineException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public FanEngineException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public FanEngineException(String message, Throwable cause) {
        super(message, cause);
    }

    public FanEngineException(String message) {
        super(message);
    }

}

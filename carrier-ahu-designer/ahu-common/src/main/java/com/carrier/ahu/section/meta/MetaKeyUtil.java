package com.carrier.ahu.section.meta;

import org.apache.commons.lang3.StringUtils;

public class MetaKeyUtil {
	/** 属性前缀[AHU] */
	public static final String META_PREF_AHU = "meta.ahu.";
	/** 属性前缀-混合段[MIX] */
	public static final String META_PREF_MIX = "meta.section.mix.";
	/** 属性前缀-单层过滤段[SINGLE] */
	public static final String META_PREF_SINGLE = "meta.section.filter.";
	/** 属性前缀-综合过滤段[COMPOSITE] */
	public static final String META_PREF_COMPOSITE = "meta.section.combinedFilter.";
	/** 属性前缀-冷水盘管段[COLD] */
	public static final String META_PREF_COLD = "meta.section.coolingcoil.";
	/** 属性前缀-风机段[FAN] */
	public static final String META_PREF_FAN = "meta.section.fan.";

	/**
	 * 生成属性键[AHU]
	 * 
	 * @param key
	 * @return
	 */
	public static String gKeyAHU(String key) {
		if (StringUtils.isBlank(key)) {
			return null;
		}
		return META_PREF_AHU + key;
	}

	/**
	 * 生成属性键-混合[MIX]
	 * 
	 * @param key
	 * @return
	 */
	public static String gKeyMIX(String key) {
		if (StringUtils.isBlank(key)) {
			return null;
		}
		return META_PREF_MIX + key;
	}

	/**
	 * 生成属性键-单层过滤[SINGLE]
	 * 
	 * @param key
	 * @return
	 */
	public static String gKeySINGLE(String key) {
		if (StringUtils.isBlank(key)) {
			return null;
		}
		return META_PREF_SINGLE + key;
	}

	/**
	 * 生成属性键-综合过滤[COMPOSITE]
	 * 
	 * @param key
	 * @return
	 */
	public static String gKeyCOMPOSITE(String key) {
		if (StringUtils.isBlank(key)) {
			return null;
		}
		return META_PREF_COMPOSITE + key;
	}

	/**
	 * 生成属性键-冷水盘管[COLD]
	 * 
	 * @param key
	 * @return
	 */
	public static String gKeyCOLD(String key) {
		if (StringUtils.isBlank(key)) {
			return null;
		}
		return META_PREF_COLD + key;
	}

	/**
	 * 生成属性键-风机[FAN]
	 * 
	 * @param key
	 * @return
	 */
	public static String gKeyFAN(String key) {
		if (StringUtils.isBlank(key)) {
			return null;
		}
		return META_PREF_FAN + key;
	}
}

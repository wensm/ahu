package com.carrier.ahu.po;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.joda.time.DateTime;

import com.carrier.ahu.length.param.HeatCoilQParam;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 一些常用的扩展字段
 * Created by Wen zhengtao on 2017/3/12.
 */
@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper=false)
public class AbstractPo implements Serializable {
    private static final long serialVersionUID = -398693247411185882L;

    protected Long createTime;//创建时间
    protected Long updateTime;//更新时间
    protected String createUser;//创建人
    protected String updateUser;//更新人
    protected String recordStatus;//记录状态
    protected Integer recordOrder;//记录排序
    protected String recordMemo;//记录备注
    protected String ext1;//扩展字段1
    protected String ext2;//扩展字段2
    protected String ext3;//扩展字段3 //项目表中 Domestic国内版 Export出口版

    @Lob
    @Basic(fetch = FetchType.EAGER)
    @Column(name="META_JSON", columnDefinition="CLOB", nullable=true)
    protected String metaJson;//元数据信息，用来存储一些附属的kv字段

    /**
     * 创建信息控制字段
     * @param userName
     */
    public void setCreateInfo(String userName){
        createTime = DateTime.now().getMillis();
        createUser = userName;
        updateTime = DateTime.now().getMillis();
        updateUser = userName;
    }

    /**
     * 更新信息控制字段
     * @param userName
     */
    public void setUpdateInfo(String userName){
        updateTime = DateTime.now().getMillis();
        updateUser = userName;
    }
}

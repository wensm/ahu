package com.carrier.ahu.engine.cad;

import com.carrier.ahu.model.calunit.AhuParam;

/**
 * Created by liaoyw on 2017/4/9.
 */
public interface AhuExporter {
	/**
	 * 根据ahu生成bmp
	 * 
	 * @param ahu
	 * @return bmp文件位置
	 */
	String createBmp(AhuParam param);

	/**
	 * 根据ahu生成dwg
	 * 
	 * @param ahu
	 * @return dwg文件位置
	 */
	String createDwg(AhuParam param);

	/**
	 * 根据ahu生成三视图
	 * 
	 * @param ahu
	 * @param fileName
	 *            指定文件名
	 * @return 文件位置
	 */
	String download(AhuParam param, String fileName);
}

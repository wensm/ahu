package com.carrier.ahu.po;

public class Combinedfilter4c {
    private String pid;

    private String partid;

    private String filterf;

    private String lmateriale;

    private String rmateriale;

    private String lrimmaterial;

    private String rrimmaterial;

    private String rimthick;

    private String plankm;

    private String framem;

    private String lsupplier;

    private String rsupplier;

    private Integer sectionl;

    private Double lresistance;

    private Double rresistance;

    private String resiflag;

    private Double weight;

    private Double price;

    private String nostandard;

    private String memo;

    private String lnosfilter;

    private String rnosfilter;

    private Boolean ycjchk;

    private String ycjtype;

    private Boolean ycjchk1;

    private String ycjtype1;

    private String loading;

    private String standard;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public String getFilterf() {
        return filterf;
    }

    public void setFilterf(String filterf) {
        this.filterf = filterf == null ? null : filterf.trim();
    }

    public String getLmateriale() {
        return lmateriale;
    }

    public void setLmateriale(String lmateriale) {
        this.lmateriale = lmateriale == null ? null : lmateriale.trim();
    }

    public String getRmateriale() {
        return rmateriale;
    }

    public void setRmateriale(String rmateriale) {
        this.rmateriale = rmateriale == null ? null : rmateriale.trim();
    }

    public String getLrimmaterial() {
        return lrimmaterial;
    }

    public void setLrimmaterial(String lrimmaterial) {
        this.lrimmaterial = lrimmaterial == null ? null : lrimmaterial.trim();
    }

    public String getRrimmaterial() {
        return rrimmaterial;
    }

    public void setRrimmaterial(String rrimmaterial) {
        this.rrimmaterial = rrimmaterial == null ? null : rrimmaterial.trim();
    }

    public String getRimthick() {
        return rimthick;
    }

    public void setRimthick(String rimthick) {
        this.rimthick = rimthick == null ? null : rimthick.trim();
    }

    public String getPlankm() {
        return plankm;
    }

    public void setPlankm(String plankm) {
        this.plankm = plankm == null ? null : plankm.trim();
    }

    public String getFramem() {
        return framem;
    }

    public void setFramem(String framem) {
        this.framem = framem == null ? null : framem.trim();
    }

    public String getLsupplier() {
        return lsupplier;
    }

    public void setLsupplier(String lsupplier) {
        this.lsupplier = lsupplier == null ? null : lsupplier.trim();
    }

    public String getRsupplier() {
        return rsupplier;
    }

    public void setRsupplier(String rsupplier) {
        this.rsupplier = rsupplier == null ? null : rsupplier.trim();
    }

    public Integer getSectionl() {
        return sectionl;
    }

    public void setSectionl(Integer sectionl) {
        this.sectionl = sectionl;
    }

    public Double getLresistance() {
        return lresistance;
    }

    public void setLresistance(Double lresistance) {
        this.lresistance = lresistance;
    }

    public Double getRresistance() {
        return rresistance;
    }

    public void setRresistance(Double rresistance) {
        this.rresistance = rresistance;
    }

    public String getResiflag() {
        return resiflag;
    }

    public void setResiflag(String resiflag) {
        this.resiflag = resiflag == null ? null : resiflag.trim();
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getNostandard() {
        return nostandard;
    }

    public void setNostandard(String nostandard) {
        this.nostandard = nostandard == null ? null : nostandard.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getLnosfilter() {
        return lnosfilter;
    }

    public void setLnosfilter(String lnosfilter) {
        this.lnosfilter = lnosfilter == null ? null : lnosfilter.trim();
    }

    public String getRnosfilter() {
        return rnosfilter;
    }

    public void setRnosfilter(String rnosfilter) {
        this.rnosfilter = rnosfilter == null ? null : rnosfilter.trim();
    }

    public Boolean getYcjchk() {
        return ycjchk;
    }

    public void setYcjchk(Boolean ycjchk) {
        this.ycjchk = ycjchk;
    }

    public String getYcjtype() {
        return ycjtype;
    }

    public void setYcjtype(String ycjtype) {
        this.ycjtype = ycjtype == null ? null : ycjtype.trim();
    }

    public Boolean getYcjchk1() {
        return ycjchk1;
    }

    public void setYcjchk1(Boolean ycjchk1) {
        this.ycjchk1 = ycjchk1;
    }

    public String getYcjtype1() {
        return ycjtype1;
    }

    public void setYcjtype1(String ycjtype1) {
        this.ycjtype1 = ycjtype1 == null ? null : ycjtype1.trim();
    }

    public String getLoading() {
        return loading;
    }

    public void setLoading(String loading) {
        this.loading = loading == null ? null : loading.trim();
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard == null ? null : standard.trim();
    }
}
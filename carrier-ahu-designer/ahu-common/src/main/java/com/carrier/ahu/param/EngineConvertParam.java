package com.carrier.ahu.param;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/21.
 */
@Data
public class EngineConvertParam {

	private double InRelativeT;//进风相对湿度
	private double NewRelativeT;//新风相对湿度
	
    private double OutDryBulbT;//出风干球温度
    private double OutWetBulbT;//出风湿球温度
    private double OutRelativeT;//出风相对湿度

    /*蒸汽盘管段、电加热盘管段*/
    private double HeatQ;//加热量
    private double HumDryBulbT;//目标温度

    /*加湿段*/
    private double HumidificationQ;//加湿量
    private double JSBRelativeT;//相对湿度

    private String aperture;//孔径
    private int nozzleN;//喷嘴数
    private String Typer;//规格
    private double hq;//最大喷雾量

    /*混合段*/
    private double NARatio;//新风比
    private double NAVolume;//新风量

}

package com.carrier.ahu.resistance.count;

/**
 * Created by liangd4 on 2017/9/14.
 */
public class CountParam {

    private boolean fInterpolation;
    private double resistance;

    public boolean isfInterpolation() {
        return fInterpolation;
    }

    public void setfInterpolation(boolean fInterpolation) {
        this.fInterpolation = fInterpolation;
    }

    public double getResistance() {
        return resistance;
    }

    public void setResistance(double resistance) {
        this.resistance = resistance;
    }
}

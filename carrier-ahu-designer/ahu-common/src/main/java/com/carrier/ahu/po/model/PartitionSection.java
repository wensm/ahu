package com.carrier.ahu.po.model;

import lombok.Data;

@Data
public class PartitionSection {
	String metaId;
	int pos;
	int sectionL;
}

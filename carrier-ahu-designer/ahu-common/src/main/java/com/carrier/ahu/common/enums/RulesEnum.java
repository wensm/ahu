package com.carrier.ahu.common.enums;

/**
 * Created by liangd4 on 2018/2/2.
 */
public enum RulesEnum {

    RANGE("range", "范围"),
    EQUALS("equals", "等于"),
    NOEQUALS("noEquals", "不等于");

    private String code;//code
    private String cnName;// 中文名称

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }


    RulesEnum(String code, String cnName) {
        this.code = code;
        this.cnName = cnName;
    }
}

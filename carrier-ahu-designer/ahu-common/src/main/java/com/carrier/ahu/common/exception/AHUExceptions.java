package com.carrier.ahu.common.exception;

import com.carrier.ahu.common.exception.engine.EngineException;
import com.carrier.ahu.constant.CommonConstant;

/**
 * Predefined Ahu exceptions used in service layers.
 * 
 * Created by Braden Zhou on 2018/04/17.
 */
public final class AHUExceptions {

    public static EngineException ENGIN_EXCEPTION = new EngineException(CommonConstant.SYS_BLANK);

}

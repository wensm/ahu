package com.carrier.ahu.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

import com.carrier.ahu.po.AbstractPo;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
/**
 * 分段信息的存储，与ahu关联
 */
public class Partition extends AbstractPo{
    private static final long serialVersionUID = 7084687735207371059L;
    @Id
    @GeneratedValue
    private String partitionid;//分段ID
    private String pid;//项目ID
    private String unitid;//AHU ID
    private String saveFrom;//分段界面partion,面板切割页面panel
    @Lob
    @Basic(fetch = FetchType.EAGER)
    @Column(name="PARTITION_JSON", columnDefinition="CLOB", nullable=true)
    protected String partitionJson;//分段的信息，每一个机组可以存在分段信息
    @Lob
    @Basic(fetch = FetchType.EAGER)
    @Column(name="SUMMARY_JSON", columnDefinition="CLOB", nullable=true)
    protected String summaryJson;//面板箱体零部件清单统计
}
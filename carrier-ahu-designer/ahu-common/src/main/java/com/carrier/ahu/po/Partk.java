package com.carrier.ahu.po;

public class Partk {
    private String pid;

    private String partid;

    private String wmstatus;

    private String wmtype;

    private String airflowo;

    private String beltwp;

    private String mlevel;

    private String insulationl;

    private String protectl;

    private String startt;

    private String power;

    private String typer;

    private Integer wmvelocity;

    private String dooro;

    private Double otherpressure;

    private Double spressure;

    private Integer sectionl;

    private Double weight;

    private Double price;

    private Double resistance;

    private String nonstandard;

    private String memo;

    private String brand;

    private String chkcheck;

    private String controltype;

    private String fansupplier;

    private String fanframe;

    private String motortype;

    private String voltage;

    private String chkdoor;

    private String dualmotor;

    private String seismic;

    private String beltbrand;

    private String olocation;

    private String outlet;

    private String motorlocation;

    private String saltspray;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public String getWmstatus() {
        return wmstatus;
    }

    public void setWmstatus(String wmstatus) {
        this.wmstatus = wmstatus == null ? null : wmstatus.trim();
    }

    public String getWmtype() {
        return wmtype;
    }

    public void setWmtype(String wmtype) {
        this.wmtype = wmtype == null ? null : wmtype.trim();
    }

    public String getAirflowo() {
        return airflowo;
    }

    public void setAirflowo(String airflowo) {
        this.airflowo = airflowo == null ? null : airflowo.trim();
    }

    public String getBeltwp() {
        return beltwp;
    }

    public void setBeltwp(String beltwp) {
        this.beltwp = beltwp == null ? null : beltwp.trim();
    }

    public String getMlevel() {
        return mlevel;
    }

    public void setMlevel(String mlevel) {
        this.mlevel = mlevel == null ? null : mlevel.trim();
    }

    public String getInsulationl() {
        return insulationl;
    }

    public void setInsulationl(String insulationl) {
        this.insulationl = insulationl == null ? null : insulationl.trim();
    }

    public String getProtectl() {
        return protectl;
    }

    public void setProtectl(String protectl) {
        this.protectl = protectl == null ? null : protectl.trim();
    }

    public String getStartt() {
        return startt;
    }

    public void setStartt(String startt) {
        this.startt = startt == null ? null : startt.trim();
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power == null ? null : power.trim();
    }

    public String getTyper() {
        return typer;
    }

    public void setTyper(String typer) {
        this.typer = typer == null ? null : typer.trim();
    }

    public Integer getWmvelocity() {
        return wmvelocity;
    }

    public void setWmvelocity(Integer wmvelocity) {
        this.wmvelocity = wmvelocity;
    }

    public String getDooro() {
        return dooro;
    }

    public void setDooro(String dooro) {
        this.dooro = dooro == null ? null : dooro.trim();
    }

    public Double getOtherpressure() {
        return otherpressure;
    }

    public void setOtherpressure(Double otherpressure) {
        this.otherpressure = otherpressure;
    }

    public Double getSpressure() {
        return spressure;
    }

    public void setSpressure(Double spressure) {
        this.spressure = spressure;
    }

    public Integer getSectionl() {
        return sectionl;
    }

    public void setSectionl(Integer sectionl) {
        this.sectionl = sectionl;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getResistance() {
        return resistance;
    }

    public void setResistance(Double resistance) {
        this.resistance = resistance;
    }

    public String getNonstandard() {
        return nonstandard;
    }

    public void setNonstandard(String nonstandard) {
        this.nonstandard = nonstandard == null ? null : nonstandard.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand == null ? null : brand.trim();
    }

    public String getChkcheck() {
        return chkcheck;
    }

    public void setChkcheck(String chkcheck) {
        this.chkcheck = chkcheck == null ? null : chkcheck.trim();
    }

    public String getControltype() {
        return controltype;
    }

    public void setControltype(String controltype) {
        this.controltype = controltype == null ? null : controltype.trim();
    }

    public String getFansupplier() {
        return fansupplier;
    }

    public void setFansupplier(String fansupplier) {
        this.fansupplier = fansupplier == null ? null : fansupplier.trim();
    }

    public String getFanframe() {
        return fanframe;
    }

    public void setFanframe(String fanframe) {
        this.fanframe = fanframe == null ? null : fanframe.trim();
    }

    public String getMotortype() {
        return motortype;
    }

    public void setMotortype(String motortype) {
        this.motortype = motortype == null ? null : motortype.trim();
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage == null ? null : voltage.trim();
    }

    public String getChkdoor() {
        return chkdoor;
    }

    public void setChkdoor(String chkdoor) {
        this.chkdoor = chkdoor == null ? null : chkdoor.trim();
    }

    public String getDualmotor() {
        return dualmotor;
    }

    public void setDualmotor(String dualmotor) {
        this.dualmotor = dualmotor == null ? null : dualmotor.trim();
    }

    public String getSeismic() {
        return seismic;
    }

    public void setSeismic(String seismic) {
        this.seismic = seismic == null ? null : seismic.trim();
    }

    public String getBeltbrand() {
        return beltbrand;
    }

    public void setBeltbrand(String beltbrand) {
        this.beltbrand = beltbrand == null ? null : beltbrand.trim();
    }

    public String getOlocation() {
        return olocation;
    }

    public void setOlocation(String olocation) {
        this.olocation = olocation == null ? null : olocation.trim();
    }

    public String getOutlet() {
        return outlet;
    }

    public void setOutlet(String outlet) {
        this.outlet = outlet == null ? null : outlet.trim();
    }

    public String getMotorlocation() {
        return motorlocation;
    }

    public void setMotorlocation(String motorlocation) {
        this.motorlocation = motorlocation == null ? null : motorlocation.trim();
    }

    public String getSaltspray() {
        return saltspray;
    }

    public void setSaltspray(String saltspray) {
        this.saltspray = saltspray == null ? null : saltspray.trim();
    }
}
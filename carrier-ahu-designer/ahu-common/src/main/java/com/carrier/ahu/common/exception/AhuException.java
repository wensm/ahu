package com.carrier.ahu.common.exception;

/**
 * Base AHU Exception class.
 * 
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class AhuException extends RuntimeException {

    private ErrorCode errorCode;
    private Object[] params;

    public AhuException(String message, Throwable cause) {
        super(message, cause);
    }

    public AhuException(String message) {
        super(message);
    }

    public AhuException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public AhuException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode.getMessage(), cause);
        this.errorCode = errorCode;
        this.params = params;
    }

    public ErrorCode getErrorCode() {
        return this.errorCode;
    }

    public Object[] getParams() {
        return this.params;
    }

}

package com.carrier.ahu.po.model;

import lombok.Data;

@Data
public class PartitionPanel {
	int width;
	int length;
	int pos;
	//0:不拆分；1:横向拆分；2:纵向拆分
	int direction;
	//不拆分是，数组为空
	PartitionPanel[] subPanels[];
}

package com.carrier.ahu.engine.watercoil;

import com.carrier.ahu.constant.CommonConstant;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.LongByReference;

/**
 * Created by liaoyw on 2017/3/27.
 */
interface WaterCoilLib extends Library {
//    String CommonConstant.SYS_PATH_WATER_COIL_DLL = "Sim_WCoil_THC";//冷水盘管
//    String CommonConstant.SYS_PATH_WATER_COIL_DLL = "Sim_WCoil_Dll";//冷水盘管

    WaterCoilLib INSTANCE = (WaterCoilLib) Native.loadLibrary(CommonConstant.SYS_PATH_WATER_COIL_DLL, WaterCoilLib.class);

    void RunWaterCoil_SI(String cpcContent, //cpc文件内容,
                         DoubleByReference vface, // 迎风风量? 风机属性/Velocity
                         DoubleByReference inDryBulbT, // 进风干球温度  //DB TMP
                         DoubleByReference inWetBulbT, // 进风湿球温度 WB TMP
                         DoubleByReference inAirPressure, // 空气阻力 .5  AHU.s_coil_cal
                         DoubleByReference foulingFactor, // 污垢系数 // db
                         DoubleByReference surfEff, //? 盘管性能系数 -db

                         DoubleByReference radMult, // Dry air-side resistances multiplier -db
                         DoubleByReference dpdMult, // Dry air-side pressure drops multiplier -db
                         DoubleByReference rawMult, // Wet air-side resistances multiplier .10 -db
                         DoubleByReference dpwMult, // Wet air-side pressure drops multiplier -db
                         DoubleByReference lbts,  // Tube length between tube sheets -db ahu.tube-len
                         DoubleByReference odexp, // Tube expanded outside diameter -db_sg S_surface_Geometrics
                         DoubleByReference tw,  // Tube wall thickness -db_sg
                         DoubleByReference idBend,  // Inside diameter of return bends .15 -db_sg
                         DoubleByReference idHairpin, // -db_sg
                         DoubleByReference pt, // Tube face spacing管间距 -db_sg
                         DoubleByReference pr, // Tube row spacing排间距 -db_sg
                         DoubleByReference TCond, // Tube thermal conductivity -db
                         LongByReference curveNum, // Curve number .20 -db
                         DoubleByReference fThick, // Fin thickness -db
                         DoubleByReference fCond, // Fin thermal conductivity -db

                         DoubleByReference fpi, // 风扇密度 -ui.fin desity
                         DoubleByReference inWaterT, // 进水温度
                         DoubleByReference waterTemperatureChange, //out|in 水温升, WTAsend(dephi source) .25
                         DoubleByReference waterFlowRate, // 介质流速 //out|in
                         DoubleByReference fluidId, // 介质类型?
                         DoubleByReference fluidConcentration, // 介质浓度
                         DoubleByReference totalCap, // out
                         DoubleByReference sensibleCap, // out .30
                         DoubleByReference airDrop, // out
                         DoubleByReference waterDrop, // out
                         DoubleByReference outDryBulbT, // out
                         DoubleByReference outWebBulbT, // out
                         DoubleByReference outWaterT, // out .35
                         DoubleByReference height, //
                         DoubleByReference bpf, // out
                         DoubleByReference vwbrAve, // out
                         LongByReference errFlag,
                         byte[] errMsg); //. 40
}

package com.carrier.ahu.common.enums;

public enum DamperMaterialEnum {

    FD, // 法兰
    GI, // 镀锌板
    AL// 铝合金;

}

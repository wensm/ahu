package com.carrier.ahu.engine.cad;

import com.carrier.ahu.constant.CommonConstant;

/**
 * @ClassName: ConfigMapUtil 
 * @Description: (导出dwg计算参数处理util) 
 * @author dongxiangxiang <dong.xiangxiang@carries.utc.com>   
 * @date 2018年2月6日 下午2:21:01 
 *
 */
public class IniConfigMapUtil {
	//分段间隔
	static final int Interval_39CBFI = 100;
	static final int Interval_39G = 50;
	static final int Interval_39XT = 104;
	static final int Interval_Other = 90;
	
	/**
	 * 获取默认DIMENSION_D
	 * @param product 机组系列
	 * @return
	 */
	public static double getDefaultDIMENSION_D(String product){
		if (product.equals(CommonConstant.SYS_UNIT_SERIES_39G)) {
			return 87;
		}else if (product.equals(CommonConstant.SYS_UNIT_SERIES_39CBF) || product.equals(CommonConstant.SYS_UNIT_SERIES_39CQ) || product.equals(CommonConstant.SYS_UNIT_SERIES_39CQEC)) {
			return 67;
		}else{
			return 112;
		}
	}
	/**
	 * 获取计算DIMENSION_C 的增量
	 * @param product 机组系列
	 * @return
	 */
	public static int getDIMENSION_C_calPara(String product){
		if (product.equals(CommonConstant.SYS_UNIT_SERIES_39G) || product.equals(CommonConstant.SYS_UNIT_SERIES_39CBFI)) {
			return 48;
		}else if (product.equals(CommonConstant.SYS_UNIT_SERIES_39CBF) || product.equals(CommonConstant.SYS_UNIT_SERIES_39CQ) || product.equals(CommonConstant.SYS_UNIT_SERIES_39CQEC)) {
			return 128;
		}else if(product.equals(CommonConstant.SYS_UNIT_SERIES_39XT) || product.equals(CommonConstant.SYS_UNIT_SERIES_39XTEC)){
			return 76;
		}else{
			return 128;//默认采用39CQ的计算量
		}
	}
	/**
	 * 获取计算DIMENSION_D 的增量
	 * @param product 机组系列
	 * @return
	 */
	public static int getDIMENSION_D_calPara(String product){
		if (product.equals(CommonConstant.SYS_UNIT_SERIES_39G)) {
			return 50;
		}else if (product.equals(CommonConstant.SYS_UNIT_SERIES_39CBF) || product.equals(CommonConstant.SYS_UNIT_SERIES_39CQ) || product.equals(CommonConstant.SYS_UNIT_SERIES_39CQEC)) {
			return 90;
		}else{
			return 104;//默认采用
		}
	}
	/**
	 * 获取DOORDRAWING -开门类型key
	 * @param product 机组系列
	 * @return
	 */
	public static String getDOORDRAWING_key(String product){
		if (product.indexOf(CommonConstant.SYS_UNIT_SERIES_39C)>-1) {
			return CommonConstant.SYS_UNIT_SERIES_39CQ;
		}
		if (product.indexOf(CommonConstant.SYS_UNIT_SERIES_39G)>-1) {
			return CommonConstant.SYS_UNIT_SERIES_39G;
		}
		if (product.indexOf(CommonConstant.SYS_UNIT_SERIES_39XT)>-1) {
			return CommonConstant.SYS_UNIT_SERIES_XT;
		}
		return CommonConstant.SYS_UNIT_SERIES_39CQ;
	}
	/**
	 * 获取DOORDIMENSION
	 * @param product 机组系列
	 * @return
	 */
	public static String getDOORDIMENSION(String dooris,int sectionL ,int iFrmThick){
		String doordimensionA = dooris;
		
		if(Integer.parseInt(dooris.substring(0,3))>(sectionL*100-iFrmThick)){
			doordimensionA = (sectionL*100-iFrmThick-2)+CommonConstant.SYS_PUNCTUATION_STAR+dooris.split("\\*")[1];
		}
		return doordimensionA;
	}
	/**
	 * 获取WMtype
	 * @param fanModel 风机型号
	 * @return
	 */
	public static String getFantype(String fanModel){
		fanModel = fanModel.replaceAll("[^0-9]", "");
		String wmtype = "";
		switch (fanModel) {
		case "160":
			wmtype = "A";
			break;
		case "180":
			wmtype = "B";
			break;
		case "200":
			wmtype = "C";
			break;
		case "225":
			wmtype = "D";
			break;
		case "250":
			wmtype = "E";
			break;
		case "280":
			wmtype = "F";
			break;
		case "315":
			wmtype = "G";
			break;
		case "355":
			wmtype = "H";
			break;
		case "400":
			wmtype = "I";
			break;
		case "450":
			wmtype = "J";
			break;
		case "500":
			wmtype = "K";
			break;
		case "560":
			wmtype = "L";
			break;
		case "630":
			wmtype = "M";
			break;
		case "710":
			wmtype = "N";
			break;
		case "800":
			wmtype = "O";
			break;
		case "900":
			wmtype = "P";
			break;
		case "1000":
			wmtype = "Q";
			break;
		case "1120":
			wmtype = "R";
			break;
		case "1250":
			wmtype = "S";
			break;
		case "1400":
			wmtype = "T";
			break;
		case "1600":
			wmtype = "U";
			break;
		default:
			break;
		}
		return wmtype;
	}
}

package com.carrier.ahu.util;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

import com.carrier.ahu.constant.CommonConstant;

public class ExcelCellReader {

	public final static String BLANK = CommonConstant.SYS_BLANK;

	/**
	 * 忽略excel数据类型读取数据
	 * 
	 * @param cell
	 * @return
	 */

	public static String getCellContext(Cell cell) {

		if (EmptyUtil.isEmpty(cell)) {
			return BLANK;
		}

		CellType ct = cell.getCellTypeEnum();

		switch (ct) {
		case NUMERIC:
			//优化数据形式，将数字后面的。0去掉，避免模板导入后，由于数据类型不匹配而出现选项错误
			Double cellValue=cell.getNumericCellValue();
			if(cellValue.intValue()==cellValue) {
				return String.valueOf(cellValue.intValue());
			}
			return String.valueOf(cellValue);
		case BOOLEAN:
			return String.valueOf(cell.getBooleanCellValue());
		case BLANK:
			return BLANK;
		case FORMULA:
			try {
				Double cellFormulaValue=cell.getNumericCellValue();
				if(cellFormulaValue.intValue()==cellFormulaValue) {
					return String.valueOf(cellFormulaValue.intValue());
				}
				return String.valueOf(cellFormulaValue);
			} catch (Exception e) {
				cell.setCellType(Cell.CELL_TYPE_STRING);//强转为string格式
				return String.valueOf(cell.getStringCellValue());
			}
		default:
			return String.valueOf(cell.getStringCellValue());
			
		}

	}
}

package com.carrier.ahu.po.batch;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class JobItem {
	String key;//
	String value;//
	String staus;// 成功，失败
	String failed;// 失败原因
	String status;// 状态
	long time;// 结束时间（成功、失败）
}

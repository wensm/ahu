package com.carrier.ahu.util;

import java.io.IOException;
import java.text.MessageFormat;

/**
 * Extract from PDFTableGen class.
 * 
 * Created by Braden Zhou on 2018/05/07.
 */
public class PDFUtils {

    public final static String ContentsErrorMsg = "Generate PdfTabe Contents Error.Reason :>> [{0}]";

    /**
     * 检查绘制表格的数据
     * 
     * @param content
     * @throws IOException
     */
    public static void checkTableContents(String[][] content) throws IOException {
        if (EmptyUtil.isEmpty(content)) {
            throw new IOException(MessageFormat.format(ContentsErrorMsg, new Object[] { "content is Null" }));
        }
        int rows = content.length;
        if (rows == 0) {
            throw new IOException(MessageFormat.format(ContentsErrorMsg, new Object[] { "Rows is 0" }));
        }
        int cols = content[0].length;
        if (cols == 0) {
            throw new IOException(MessageFormat.format(ContentsErrorMsg, new Object[] { "Column counts is 0" }));
        }
        for (int i = 0; i < rows; i++) {
            if (content[i].length != cols) {
                throw new IOException(MessageFormat.format(ContentsErrorMsg,
                        new Object[] { "Column counts is different at row " + (i + 1) }));
            }
        }
    }

}

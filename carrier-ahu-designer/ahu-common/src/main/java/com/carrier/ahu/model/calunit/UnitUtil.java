package com.carrier.ahu.model.calunit;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;

public class UnitUtil {
	
	public static <T extends Serializable> T clone(Map<String, String> map) {
		T clonedObj = null;
		try {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(map);
		oos.close();
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bais);
		clonedObj = (T) ois.readObject();
		ois.close();

		} catch (Exception e) {
		e.printStackTrace();
		}

		return clonedObj;
		}
	
}

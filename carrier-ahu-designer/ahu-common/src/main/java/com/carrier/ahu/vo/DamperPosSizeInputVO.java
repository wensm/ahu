package com.carrier.ahu.vo;

import com.carrier.ahu.common.enums.DamperMaterialEnum;

import lombok.Data;
import lombok.ToString;

/**
 * Created by Braden Zhou on 2019/08/13.
 */
@Data
@ToString(callSuper = true)
public class DamperPosSizeInputVO {

    private String product;
    private DamperMaterialEnum damperMaterial;
    private int unitLengthMode;
    private int unitWidthMode;
    private int modeA;
    private int modeB;
    private int modeC;
    private int modeD;

}

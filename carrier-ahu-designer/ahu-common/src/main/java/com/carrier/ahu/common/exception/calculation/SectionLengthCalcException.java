package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class SectionLengthCalcException extends CalculationException {

    public SectionLengthCalcException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public SectionLengthCalcException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public SectionLengthCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public SectionLengthCalcException(String message) {
        super(message);
    }

}

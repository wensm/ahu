package com.carrier.ahu.length.param;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/11/22.
 */
@Data
public class HumidificationBean {
	double inRelativeT;// 相对湿度 进风、加湿比 相对湿度相同
    double outDryBulbT;// 干球温度
    double outWetBulbT;// 湿球温度
    double outRelativeT;// 相对湿度 出风、加湿比 相对湿度相同
    double humidificationQ;// 加湿量
}

package com.carrier.ahu.vo;

import com.carrier.ahu.common.entity.Unit;

import lombok.Data;
import lombok.ToString;

/**
 * Created by Braden Zhou on 2019/08/02.
 */
@Data
@ToString(callSuper = true)
public class UpdateUnitVO {

    String unitid;
    String pid;
    String groupId;
    String unitNo;
    String drawingNo;
    String product;
    Short mount;
    String name;
    String isPrintDiffUnitName;
    public Unit toUnit() {
        Unit unit = new Unit();
        unit.setUnitid(unitid);
        unit.setPid(pid);
        unit.setGroupId(groupId);
        unit.setUnitNo(unitNo);
        unit.setDrawingNo(drawingNo);
        unit.setProduct(product);
        unit.setMount(mount);
        unit.setName(name);
        unit.setIsPrintDiffUnitName(isPrintDiffUnitName);
        return unit;
    }

}

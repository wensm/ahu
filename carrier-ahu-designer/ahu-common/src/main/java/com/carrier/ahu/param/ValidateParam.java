package com.carrier.ahu.param;

import com.carrier.ahu.common.enums.AirDirectionEnum;
import lombok.Data;
import lombok.ToString;

import java.util.Map;

@Data
@ToString
public class ValidateParam {

    /*所有段必传参数*/
    private String sectionId;
    private String sectionType;//sectionType A:混合段 B:过滤段 C:综合过滤段 D:冷水盘管段 E:热水盘管段 F:蒸汽盘管段 G:电加热盘管段 H:干蒸汽加湿段 I:湿膜加湿段 J:高压喷雾加湿段 K:风机段 L:新回排风段 M:消音段 N:出风段 O:空段 V:高效过滤段 W:热回收段 X:电极加湿 Y:静电过滤器 Z:控制段
    private String serial;//机组型号
    private String airDirection = AirDirectionEnum.SUPPLYAIR.getCode();//风向
    private String prePartKey;//上一个段的key
    private String nextPartKey;//下一个段的key
    private Map<String, Object> params;//当前段的json数据
    private Map<String, Object> preParams;//上一个段的json数据
    private Map<String, Object> nextParams;//下一个段的json数据
    private String sectionL;//段长

    /* 风机 */
    private String outlet;//风机形式
    private String motorPower;//电机功率
    private String type;//电机类型
    private String startStyle;//启动方式
    /* 空段 */
    private String Function;//功能选择(空段、均流器)

}

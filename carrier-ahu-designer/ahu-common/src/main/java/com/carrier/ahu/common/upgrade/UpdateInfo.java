package com.carrier.ahu.common.upgrade;

import lombok.Data;

/**
 * Created by Braden Zhou on 2018/07/16.
 */
@Data
public class UpdateInfo {

    private String ipAddress;
    private String updateVersion;
    private String ahuVersion;
    private boolean downloaded;
    private String factory;
    private String languageCode;

}

package com.carrier.ahu.po.meta;

public enum ValueType {
	StringV("string"), IntV("int"), DoubleV("double"), BooleanV("boolean"), Double2V("double"), LongStringV("string");

	private String name;

	public String getName() {
		return this.name;
	}

	private ValueType(String name) {
		this.name = name;
	}
}

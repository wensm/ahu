package com.carrier.ahu.vo;

import java.util.List;

import lombok.Data;

@Data
@SuppressWarnings("rawtypes")
public class HeatRecycleResultVO {

    private List lines;

    public HeatRecycleResultVO(List lines) {
        this.lines = lines;
    }
}

package com.carrier.ahu.license;

import java.io.File;
import java.util.Base64;

import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.common.util.RSAUtils;
import com.carrier.ahu.constant.CommonConstant;

/**
 * 生成license
 *
 * @author Simon 2018.01.28
 */
class LicenseGenerator {
    /**
     * localValue format
     */
    private static final String MSG = "mac=%s;name=%s;computerName=%s;domain=%s;date=%s;factory=%s;role=%s;version=%s;patchVersion=%s;";

    
    /**
     * RSA算法 公钥和私钥是一对，此处只用私钥加密
     */
    public static final String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKJCrRPljC9Fix3H8caDXMAhLIjk\r\n"
            + "xnvzyqKzRcZ9p/hCytKxppy3E3Ucfc299nuTr4aJr/tyieJMRpte3Eb3oJjVxwhFrHjtjEkRfhAl\r\n"
            + "x4BxMSvgCYDPgNHoxLcGrgOKyQzWzPIyXSLgHsPLsbRXKmA5vokWF2ZoHQqQadGkyHI7AgMBAAEC\r\n"
            + "gYBywZFC9x+z9w0ynMxDx3f7cfrbPmGaHIt9kiiQ/yGilSp45KefQmOCegoHTohaUdLb5dkOjq0x\r\n"
            + "3Rw6mlZT4qIzFL+ScmeOUdQBq6IbIMiDs6XL6SzBoivO/RUhNJ2v28olAn70lxSkUv6oGHVYx/rR\r\n"
            + "7CaaKaucbtY4YdSjq4Tv8QJBANTSwNxMZis3jCYFl7l6ZE5JJo3A+Vcn5+b33YgaGTBbLp4z0b39\r\n"
            + "2STzk8FleHVxm+YfBDxfPbkvvU6+dgHezs0CQQDDLeEWzbRkqrnOhO0wglQm1KAyixnt1Cb3lOXm\r\n"
            + "pBAJDOjVR8DK7MX+cmL64U8g1Prbd/txZgdJoMQEFILiILUnAkEAtUgQH5a0MHvQVVLOOp+WcVi+\r\n"
            + "DmEiAqtrTcI0YYpZimcg/oEFV+Cy5lbUtRfJ08BCG3KwGPzHhsazai+yNUWkwQJAJC8/SbD9PqSA\r\n"
            + "Kg9uwc2HuGM3B+fN+M+aIdBTBv1nk6X3DINu9LAMzO17+DMJLncNwt5tmUpnE3lgnbkHwtB5hQJA\r\n"
            + "e5L8P2p6k0ibWNJHCK4JWXrmrkF35vvkUgN7BF7zr8s00BXfpxtOdPcFHqGx2E9noq7jdWU4dKsH\r\n" + "k5rwpW0nVA==";

    /**
     * 私钥加密
     *
     * @param licenseString
     * @return
     * @throws Exception
     */
    public static void privateKeyEncode(String licenseString, File licenseFile) throws Exception {
        System.out.println("原文字：\r\n" + licenseString);
        byte[] data = licenseString.getBytes();
        byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
        System.out.println("加密后：\r\n" + new String(encodedData)); // 加密后乱码是正常的
        // byteArrayToFile(encodedData, getBasePath() + File.separator + CommonConstant.SYS_PATH_LICENSE_FILE);
        if (licenseFile == null) {
            licenseFile = new File(CommonConstant.SYS_PATH_LICENSE_PATH + File.separator + CommonConstant.SYS_PATH_LICENSE_FILE);
        }
        FileUtil.byteArrayToFile(encodedData, licenseFile);
    }

    protected static String encodeLicenseString(String licenseString) throws Exception {
        byte[] encodedData = RSAUtils.encryptByPrivateKey(licenseString.getBytes(), privateKey);
        return Base64.getEncoder().encodeToString(encodedData);
    }

    protected static String encodeLicenseString(LicenseInfo licenseInfo) throws Exception {
        String licenseString = String.format(MSG, licenseInfo.getMac(), licenseInfo.getUserName(),
                licenseInfo.getComputerName(), licenseInfo.getDomain(), licenseInfo.getDate(), licenseInfo.getFactory(),
                licenseInfo.getRole(), licenseInfo.getVersion(), licenseInfo.getPatchVersion());
        return encodeLicenseString(licenseString);
    }

    public static void privateKeyEncode(LicenseInfo licenseInfo) throws Exception {
        privateKeyEncode(String.format(MSG, licenseInfo.getMac(), licenseInfo.getUserName(),
                licenseInfo.getComputerName(), licenseInfo.getDomain(), licenseInfo.getDate(), licenseInfo.getFactory(),
                licenseInfo.getRole()), null);
    }

    public static void privateKeyEncode(LicenseInfo licenseInfo, File licenseFile) throws Exception {
        privateKeyEncode(String.format(MSG, licenseInfo.getMac(), licenseInfo.getUserName(),
                licenseInfo.getComputerName(), licenseInfo.getDomain(), licenseInfo.getDate(), licenseInfo.getFactory(),
                licenseInfo.getRole(), licenseInfo.getVersion(),licenseInfo.getPatchVersion()), licenseFile);
    }

    public static void privateKeyEncodeTest(LicenseInfo licenseInfo, String path) throws Exception {
        privateKeyEncodeTest(String.format(MSG, licenseInfo.getMac(), licenseInfo.getUserName(),
                licenseInfo.getComputerName(), licenseInfo.getDomain(), licenseInfo.getDate(), licenseInfo.getFactory(),
                licenseInfo.getRole(), licenseInfo.getVersion(),licenseInfo.getPatchVersion()), path);
    }

    public static void privateKeyEncodeTest(String licenseString, String path) throws Exception {
        System.out.println("原文字：\r\n" + licenseString);
        byte[] data = licenseString.getBytes();
        byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
        System.out.println("加密后：\r\n" + new String(encodedData)); // 加密后乱码是正常的
        FileUtil.byteArrayToFile(encodedData, new File(path + CommonConstant.SYS_PATH_LICENSE_FILE));
    }

}
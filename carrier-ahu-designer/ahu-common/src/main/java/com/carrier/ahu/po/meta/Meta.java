package com.carrier.ahu.po.meta;

import java.util.HashMap;
import java.util.Map;

import com.carrier.ahu.constant.CommonConstant;

import lombok.extern.slf4j.Slf4j;

/**
 * @author JL
 * 
 *         Abstract class for Meta data which support load operation
 *
 */
@Slf4j
public abstract class Meta {
	private String name;
	private String path;
	private String version;
	private String metaId;
	private String cName;
	private Map<String, MetaParameter> parameters = new HashMap<>();

	public Meta(String path) {
		this.path = path;
	}

	public Meta() {
	}

	protected void load() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	// @JsonGetter("parameters")
	// @JsonIgnore()
	// public MetaParameter[] getParameterAsArray() {
	// return (MetaParameter[]) parameters.toArray(new MetaParameter[0]);
	// }

	void setPath(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}

	public void appendParameter(MetaParameter metaParameter) {
		this.parameters.put(metaParameter.getKey(), metaParameter);

		//强制覆盖方式加载1.3excel（commonSheet 内容可以被1.3 段sheet内容覆盖。例如commen阻力被板式sheet阻力覆盖）
		/*if (!this.parameters.containsKey(metaParameter.getKey())) {
			this.parameters.put(metaParameter.getKey(), metaParameter);
		} else {
			log.info(String.format("Sufferred duplicated parameter : %s", metaParameter.getKey()));
		}*/
		
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setParameters(Map<String, MetaParameter> parameters) {
		this.parameters = parameters;
	}

	public Map<String, MetaParameter> getParameters() {
		return this.parameters;
	}

	public String getMetaId() {
		return metaId;
	}

	public void setMetaId(String metaId) {
		this.metaId = metaId;
	}

	public String getFullName() {
		return getName() + CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN + (this.version == null ? CommonConstant.SYS_BLANK : this.version);
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

}

package com.carrier.ahu.common.enums;

/**
 * Created by Simon on 2018-02-27.
 */
public enum TOrFEnum {
	T("true"), F("false");

	public String name;

	TOrFEnum(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name();
	}
}

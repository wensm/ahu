package com.carrier.ahu.po.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JL
 * 
 *         Describe a ahu model
 * 
 *
 */
public class Ahu extends AbstractModel {

	private static final long serialVersionUID = -1176515133212996890L;
	private List<Section> sections = new ArrayList<>();

	public Section[] getSections() {
		return (Section[]) sections.toArray(new Section[0]);
	}

	public void appendSection(int index, Section section) {
		sections.add(index, section);
	}

	public void appendSection(Section section) {
		sections.add(section);
	}

	public void removeSection(int index) {
		sections.remove(index);
	}

}

package com.carrier.ahu.engine.heatrecycle;

import com.carrier.ahu.constant.CommonConstant;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.win32.StdCallLibrary;

/**
 * Heatex32 engine
 */
public interface HeaterxLib extends StdCallLibrary {


    HeaterxLib HEATERX_INSTANCE = (HeaterxLib) Native.loadLibrary(CommonConstant.SYS_PATH_HEATERX_DLL, HeaterxLib.class);

    /**
     * @param s1  无效
     * @param s2  无效
     * @param in  输入数组，长度20
     * @param KEY 型号
     * @param z1  无效
     * @param z2  无效
     * @param OUT 输出数组，长度40
     */
    void GET_CALCULATION(byte s1, byte s2, Pointer in, String KEY, byte z1, byte z2, Pointer OUT);


    /**
     * @param s1  无效
     * @param s2  无效
     * @param in  输入数组，长度20
     * @param KEY 型号
     * @param z1  无效
     * @param z2  无效
     * @param OUT 输出数组，长度40
     */
    void GET_WHEELCALC(byte s1, byte s2, Pointer in, String KEY, int z1, int z2, Pointer OUT);

    void GET_PRICE(byte s1, byte s2, Pointer in, String KEY, byte z1, byte z2, Pointer OUT,String code);

    void GET_WHEELPRICE(byte s1, byte s2, Pointer in, String KEY, byte z1, byte z2, Pointer OUT,String code);





/*    procedure GET_CALCULATION(_s1,_s2 : Byte;RRIN : TArrayIn; RRName : PCHar;z1,_z2 : pShort;var RROUT_ : TarrayOut;stdcall;

                          Short = Byte; pShort = ^Short; TArrayIn = array [0..20] of Double; TArrayOut = array [0..50] of Double;


            GET_CALCULATION (unsigned char s1, *//* *) *//*
                              unsigned char s2, *//* *) *//*
                              double IN[ ], *//* points at an array [0..20] of double*//*
                              char* KEY
                              unsigned char* z1, *//* *) *//*
                              unsigned char* z2, *//* *) *//*
                              double OUT[ ] *//* points at an array [0..50] of double*//*)


    GET_WHEELCALC (unsigned char s1, *//* *) *//*
                   unsigned char s2, *//* *) *//*
                   double IN[ ], *//* points at an array [0..20] of double*//*
                   char* KEY
                   unsigned char* z1, *//* *) *//*
                   unsigned char* z2, *//* *) *//*
                   double OUT[ ] *//* points at an array [0..50] of double*//*)*/

}

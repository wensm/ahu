package com.carrier.ahu.util;

/**
 * @ClassName: RegexNumberValidate
 * @Description: java判断字符串是否为数字。
 * @author Simon
 * @date 2017-08-23
 */
public class RegexNumberValidate {
	/**
	 * 十进制
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isOctNumber(String str) {
		boolean flag = false;
		for (int i = 0, n = str.length(); i < n; i++) {
			char c = str.charAt(i);
			if (c == '0' | c == '1' | c == '2' | c == '3' | c == '4' | c == '5' | c == '6' | c == '7' | c == '8'
					| c == '9') {
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * 十六进制
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isHexNumber(String str) {
		boolean flag = false;
		for (int i = 0; i < str.length(); i++) {
			char cc = str.charAt(i);
			if (cc == '0' || cc == '1' || cc == '2' || cc == '3' || cc == '4' || cc == '5' || cc == '6' || cc == '7'
					|| cc == '8' || cc == '9' || cc == 'A' || cc == 'B' || cc == 'C' || cc == 'D' || cc == 'E'
					|| cc == 'F' || cc == 'a' || cc == 'b' || cc == 'c' || cc == 'd' || cc == 'e'
					|| cc == 'f') {
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * 十进制 正则表达式
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isOctNumberRex(String str) {
		String validate = "\\d+";
		return str.matches(validate);
	}

	/**
	 * 十六进制 正则表达式
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isHexNumberRex(String str) {
		String validate = "(?i)[0-9a-f]+";
		return str.matches(validate);
	}

	/**
	 * 十六进制字符串转二进制字符串
	 * 
	 * @param hexString
	 * @return
	 * @throws NumberFormatException
	 */
	public static String hexString2binaryString(String hexString) throws NumberFormatException {
		if (EmptyUtil.isEmpty(hexString))
			return null;
		String bString = "", tmp;
		for (int i = 0; i < hexString.length(); i++) {
			tmp = "0000" + Integer.toBinaryString(Integer.parseInt(hexString.substring(i, i + 1), 16));
			bString += tmp.substring(tmp.length() - 4);
		}
		return bString;
	}
}
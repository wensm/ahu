package com.carrier.ahu.model.calunit;

import java.util.List;
import java.util.Map;

import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.po.AhuLayout;

import lombok.Data;

/**
 * AHU计算参数，该类把来自前端，或者数据的的对象，封装为统一的计算参数，所有的Service都需要以AhuParam作为标准的计算参数
 */
@Data
public class AhuParam {
    // 0: single style -- for section confirmation
    public static final int CAL_STYLE_SECTION = 0;
    // 1: full style -- for full calculation on ahu sections
    public static final int CAL_STYLE_FULL = 1;

    private String unitid;// 主键ID
    private String pid;// 项目ID
    private String groupId;// 组ID
    private String unitNo;// 机组编号
    private String drawingNo;// 图纸编号
    private String series;// 机组型号
    private String panelSeries;// 机组变形型号
    private String product;
    private Short mount;// 数量
    private String name;// AHU名称
    private Double weight;// 重量
    private Double price;// 价格
    private String modifiedno;
    private String paneltype;// 类型
    private Double nsprice;
    private Double cost;
    private Double lb;
    private byte[] drawing;
    private String customerName;// 客户名称
    private String projectName;// 项目名称
    private Map<String, Object> params;
    // 需要计算的Part，当为多个Part时，需要依次计算
    private List<PartParam> partParams;
    private AhuLayout layout;
    private int calStyle = CAL_STYLE_SECTION;// 气流方向，1反向 0正向

    private Partition partition;// 分段信息
    private boolean doubleLayer;
    private List<AhuPartition> topPartitions;
    private List<AhuPartition> bottomPartitions;
    private String allSectionsStr;// 所有段json信息
    private String validateSections;// 按照风向排列段位置json信息
    private String sectionrelations;//按照风向排列段位置json数组（仅包含position 从0开始）

    private String isNS;// 是否从非标确认按钮点击事件

    private Boolean needReload3View=false;// 是否需要重载三视图

}

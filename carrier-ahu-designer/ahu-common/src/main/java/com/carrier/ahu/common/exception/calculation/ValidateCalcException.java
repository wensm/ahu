package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class ValidateCalcException extends CalculationException {

    public ValidateCalcException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public ValidateCalcException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public ValidateCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidateCalcException(String message) {
        super(message);
    }

}

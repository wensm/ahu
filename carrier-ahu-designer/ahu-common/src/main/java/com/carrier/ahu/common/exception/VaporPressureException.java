package com.carrier.ahu.common.exception;

public class VaporPressureException extends Exception {
    private static final long serialVersionUID = -7941760891978134268L;

    public VaporPressureException(String message) {
        super(message);
    }

    public VaporPressureException(String message, Exception e) {
        super(message, e);
    }
}

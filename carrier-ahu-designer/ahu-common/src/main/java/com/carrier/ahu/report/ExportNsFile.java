package com.carrier.ahu.report;

import com.carrier.ahu.po.AbstractPo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class ExportNsFile extends AbstractPo implements Serializable {
    private static final long serialVersionUID = -4242243289899265182L;

    String unitNo;// 机组编号

    String unitSeries;// 机组型号

    String unitName;// AHU名称

    Short unitMount;// 数量

    String segNo;//功能段序号

    String segName;// 功能段名称

    String nsPrice;//非标价格

    String nsMemo;//非标内容
}

package com.carrier.ahu.engine.heatrecycle;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;

public class MS270Lib {

    private ActiveXComponent dotnetCom = new ActiveXComponent(
            "CLSID:e4e02431-f0c5-4330-8654-28d74584ee06");

    private MS270Lib() {
    }

    private static MS270Lib lib = new MS270Lib();

    public static MS270Lib getInstance() {
        if (lib == null) {
            lib = new MS270Lib();
        }
        return lib;
    }

    private void setDRIinputModel(final double model) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputModel", new Double(model));
    }

    private double getDRIoutSaEfficiencySummerTotal() throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutSaEfficiencySummerTotal").getDouble();
    }

    private double getDRIoutSaEfficiencyWinterTotal() throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutSaEfficiencyWinterTotal").getDouble();
    }

    private double getDRIoutSaPDropWinter() throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutSaPDropWinter").getDouble();
    }

    private String getDRIOutECOFRESHModel() throws Exception {
        return Dispatch.call(dotnetCom, "DRIOutECOFRESHModel").getString();
    }

    private void setDRIunitSI(final boolean si) throws Exception {
        Dispatch.put(dotnetCom, "DRIunitSI", new Boolean(si));
    }

    private void setDRIinputSupplyAir(final double supplyAir) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputSupplyAir", new Double(supplyAir));
    }

    private void setDRIinputReturnAir(final double returnAir) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputReturnAir", new Double(returnAir));
    }

    private void setDRIinputOAS_DBT(final double oas_dbt) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputOAS_DBT", new Double(oas_dbt));
    }

    private void setDRIinputOAS_WBT(final double oas_wbt) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputOAS_WBT", new Double(oas_wbt));
    }

    private void setDRIinputRaS_DBT(final double raS_dbt) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputRaS_DBT", new Double(raS_dbt));
    }

    private void setDRIinputRaS_RH(final double raS_rh) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputRaS_RH", new Double(raS_rh));
    }

    private void setDRIwheelSensible(final boolean wheelSensible) throws Exception {
        Dispatch.put(dotnetCom, "DRIwheelSensible", new Boolean(wheelSensible));
    }

    //计算
    private void calculateOutput() throws Exception {
        Dispatch.call(dotnetCom, "CalculateOutput");
    }

    private void calculateOutputModel() throws Exception {
        Dispatch.call(dotnetCom, "CalculateOutputModel");
    }

    public MS270Bean calculate(boolean DRIunitSI, double DRIinputSupplyAir, double DRIinputReturnAir, double DRIinputOAS_DBT, double DRIinputOAS_WBT,
                               double DRIinputRaS_DBT, double DRIinputRaS_RH, boolean DRIwheelSensible, Double DRIinputModel) throws Exception {

        try {
            ComThread.InitSTA();
            dotnetCom = new ActiveXComponent("CLSID:e4e02431-f0c5-4330-8654-28d74584ee06");
            lib.setDRIunitSI(DRIunitSI);
            lib.setDRIinputSupplyAir(DRIinputSupplyAir);
            lib.setDRIinputReturnAir(DRIinputReturnAir);
            lib.setDRIinputOAS_DBT(DRIinputOAS_DBT);
            lib.setDRIinputOAS_WBT(DRIinputOAS_WBT);
            lib.setDRIinputRaS_DBT(DRIinputRaS_DBT);
            lib.setDRIinputRaS_RH(DRIinputRaS_RH);
            lib.setDRIwheelSensible(DRIwheelSensible);
            if (null == DRIinputModel) {
                lib.calculateOutput();
            } else {
                lib.setDRIinputModel(DRIinputModel);
                lib.calculateOutputModel();
            }
            MS270Bean ms270Bean = new MS270Bean();
            ms270Bean.setDRIoutSaEfficiencySummerTotal(lib.getDRIoutSaEfficiencySummerTotal());
            ms270Bean.setDRIoutSaEfficiencyWinterTotal(lib.getDRIoutSaEfficiencyWinterTotal());
            ms270Bean.setDRIoutSaPDropWinter(lib.getDRIoutSaPDropWinter());
            ms270Bean.setDRIOutECOFRESHModel(lib.getDRIOutECOFRESHModel());
            return ms270Bean;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            ComThread.Release();
        }
        return null;
    }

    public static void main(String args[]) {

        try {
            System.out.println("begin");
//            MS270Lib.getInstance().calculate(1300);
            System.out.println("success");

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}

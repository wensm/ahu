package com.carrier.ahu.common.enums;

import org.apache.commons.lang3.ArrayUtils;

/**
 * 10: 通用类型</br>
 * 11: 新回排风</br>
 * 21: 轮式</br>
 * 22: 板式</br>
 * 31: 立式机组1</br>
 * 32: 立式机组2
 * 41：双层转向1
 * 42: 双层转向2
 * 43: 并排转向1
 * 44: 并排转向2
 *
 * Created by Braden Zhou on 2018/06/28.
 */
public enum LayoutStyleEnum {

    // @formatter:off
    COMMON(10),
    NEW_RETURN(11),
    WHEEL(21, true),
    PLATE(22, true),
    VERTICAL_UNIT_1(31, true), VERTICAL_UNIT_2(32, true),
    DOUBLE_RETURN_1(41, true), DOUBLE_RETURN_2(42, true),
    SIDE_BY_SIDE_RETURN_1(43, true), SIDE_BY_SIDE_RETURN_2(44, true);;
    // @formatter:on

    private static final int[][] WHEEL_DEFAULT_LAYOUT = new int[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9, 10 },
            { 11, 12, 13, 14, 15 } };

    private static final int[][] PLATE_DEFAULT_LAYOUT = new int[][] { { 1, 2, 3 }, { 4, 5 }, { 6, 7, 8 },
            { 9, 10, 11, 12 } };

    private static final int[][] VERTICAL_DEFAULT_LAYOUT = new int[][] { { 1 }, {}, { 2, 3, 4, 5, 6 }, {} };

    private static final int[][] DOUBLE_RETURN1_DEFAULT_LAYOUT = new int[][] { { 1, 2, 3, 4, 5 }, {},
            { 6, 7, 8, 9 }, {} };

    private static final int[][] DOUBLE_RETURN2_DEFAULT_LAYOUT = new int[][] { { 1, 2, 3, 4 }, {}, { 5,6, 7, 8 },
            {} };

    private static final int[][] SIDE_BY_SIDE_RETURN1_DEFAULT_LAYOUT = new int[][] { { 1, 2, 3, 4 }, {}, { 5, 6, 7, 8 },
            {} };

    private static final int[][] SIDE_BY_SIDE_RETURN2_DEFAULT_LAYOUT = new int[][] { { 1, 2, 3, 4 }, {}, { 5, 6, 7 },
            {} };

    int style;
    int[][] layoutData;
    boolean doubleLayer;

    private LayoutStyleEnum(int style) {
        this(style, false);
    }

    private LayoutStyleEnum(int style, boolean doubleLayer) {
        this.style = style;
        this.doubleLayer = doubleLayer;
    }

    public int style() {
        return this.style;
    }

    public boolean isDoubleLayer() {
        return this.doubleLayer;
    }

    public int[][] getInitLayoutData(int sectionSize) {
        switch (this) {
        case COMMON:
        case NEW_RETURN:
            return generateLayout(sectionSize);
        case PLATE:
            return PLATE_DEFAULT_LAYOUT;
        case WHEEL:
            return WHEEL_DEFAULT_LAYOUT;
        case VERTICAL_UNIT_1:
        case VERTICAL_UNIT_2:
            return VERTICAL_DEFAULT_LAYOUT;
        case DOUBLE_RETURN_1:
            return DOUBLE_RETURN1_DEFAULT_LAYOUT;
        case DOUBLE_RETURN_2:
            return DOUBLE_RETURN2_DEFAULT_LAYOUT;
        case SIDE_BY_SIDE_RETURN_1:
            return SIDE_BY_SIDE_RETURN1_DEFAULT_LAYOUT;
        case SIDE_BY_SIDE_RETURN_2:
            return SIDE_BY_SIDE_RETURN2_DEFAULT_LAYOUT;
        }
        return new int[][] {};
    }

    private static int[][] generateLayout(int size) {
        int[][] layoutData = new int[1][];
        layoutData[0] = new int[size];
        for (int i = 0; i < size; i++) {
            layoutData[0][i] = i + 1;
        }
        return layoutData;
    }

    public boolean isInReturnAirLayer(int sectionPosition) {
        if (WHEEL.equals(this)) {
            return ArrayUtils.contains(WHEEL_DEFAULT_LAYOUT[0], sectionPosition)
                    || ArrayUtils.contains(WHEEL_DEFAULT_LAYOUT[1], sectionPosition);
        } else if (PLATE.equals(this)) {
            return ArrayUtils.contains(PLATE_DEFAULT_LAYOUT[1], sectionPosition)
                    || ArrayUtils.contains(PLATE_DEFAULT_LAYOUT[2], sectionPosition);
        } else if (DOUBLE_RETURN_1.equals(this)) {
            return ArrayUtils.contains(DOUBLE_RETURN1_DEFAULT_LAYOUT[0], sectionPosition);
        } else if (DOUBLE_RETURN_2.equals(this)) {
            return ArrayUtils.contains(DOUBLE_RETURN2_DEFAULT_LAYOUT[0], sectionPosition);
        } else if (SIDE_BY_SIDE_RETURN_1.equals(this)) {
            return ArrayUtils.contains(SIDE_BY_SIDE_RETURN1_DEFAULT_LAYOUT[0], sectionPosition);
        } else if (SIDE_BY_SIDE_RETURN_2.equals(this)) {
            return ArrayUtils.contains(SIDE_BY_SIDE_RETURN2_DEFAULT_LAYOUT[0], sectionPosition);
        }
        return false;
    }

    public static LayoutStyleEnum valueStyleOf(int style) {
        for (LayoutStyleEnum layoutStyle : LayoutStyleEnum.values()) {
            if (layoutStyle.style == style) {
                return layoutStyle;
            }
        }
        return COMMON;
    }

}

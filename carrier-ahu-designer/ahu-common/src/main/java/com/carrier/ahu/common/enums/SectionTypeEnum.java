package com.carrier.ahu.common.enums;

/**
 * 段类型枚举 Created by Wen zhengtao on 2017/3/28.
 * 
 * A 001 混合段 ahu.mix<br>
 * B 002 单层过滤段 ahu.filter<br>
 * C 003 综合过滤段 ahu.combinedFilter<br>
 * D 004 冷水盘管段 ahu.coolingCoil<br>
 * D 018 直接蒸发式盘管段 ahu.directExpensionCoil<br>
 * E 005 热水盘管段 ahu.heatingCoil<br>
 * F 006 蒸汽盘管段 ahu.steamCoil<br>
 * G 007 电加热盘管段 ahu.electricHeatingCoil<br>
 * H 008 干蒸加湿段 ahu.steamHumidifier<br>
 * I 009 湿膜加湿段 ahu.wetfilmHumidifier<br>
 * J 010 高压喷雾加湿段 ahu.sprayHumidifier<br>
 * K 011 风机段 ahu.fan<br>
 * L 012 二次回风段 ahu.combinedMixingChamber<br>
 * M 013 消音段 ahu.attenuator<br>
 * N 014 出风段 ahu.discharge<br>
 * O 015 空段 ahu.access<br>
 * V 022 高效过滤段 ahu.HEPAFilter<br>
 * X 024 电极加湿器 ahu.electrodeHumidifier<br>
 * Y 025 静电过滤段 ahu.electrostaticFilter<br>
 * Z 026 CTR控制段 ahu.ctr<br>
 * W 043 转轮热回收 ahu.wheelHeatRecycle<br>
 * W 023 板式热回收 ahu.heatRecycle<br>
 * <br>
 */
public enum SectionTypeEnum {
	/**
	 * 机组 ID:ahu
	 */
	TYPE_AHU("ahu", "Ahu", "unit_group", "1.0.0", "", ""),

	/**
	 * A 混合段 ID:ahu.mix
	 */
	TYPE_MIX("ahu.mix", "Mix", "mix_section", "1.0.0", "A", "001"),

	/**
	 * B 单层过滤段 ID:ahu.filter
	 */
	TYPE_SINGLE("ahu.filter", "Filter", "single_filtration_section", "1.0.0", "B", "002"),

	/**
	 * C 综合过滤段 ID:ahu.combinedFilter
	 */
	TYPE_COMPOSITE("ahu.combinedFilter", "Combined Filter", "combined_filtration_section", "1.0.0", "C", "003"),

	/**
	 * D 冷水盘管段 ID:ahu.coolingCoil
	 */
	TYPE_COLD("ahu.coolingCoil", "Cooling Coil", "cooling_coil_section", "1.0.0", "D", "004"),

	/**
	 * 直接蒸发式盘管段 ID:ahu.directExpensionCoil
	 */
	TYPE_DIRECTEXPENSIONCOIL("ahu.directExpensionCoil", "Direct Expension Coil", "direct_evaporation_coil", "1.0.0", "D",
			"018"),

	/**
	 * E 热水盘管段 ID:ahu.heatingCoil
	 */
	TYPE_HEATINGCOIL("ahu.heatingCoil", "Heating Coil", "hot_water_coil_section", "1.0.0", "E", "005"),

	/**
	 * F 蒸汽盘管段 ID:ahu.steamCoil
	 */
	TYPE_STEAMCOIL("ahu.steamCoil", "Steam Coil", "steam_coil_section", "1.0.0", "F", "006"),

	/**
	 * G 电加热盘管段 ID:ahu.electricHeatingCoil
	 */
	TYPE_ELECTRICHEATINGCOIL("ahu.electricHeatingCoil", "Electric Heating Coil", "electric_heating_coil_section", "1.0.0", "G",
			"007"),

	/**
	 * H 干蒸加湿段 ID:ahu.steamHumidifier
	 */
	TYPE_STEAMHUMIDIFIER("ahu.steamHumidifier", "Steam Humidifier", "dry_steam_humidification_section", "1.0.0", "H", "008"),

	/**
	 * I 湿膜加湿段 ID:ahu.wetfilmHumidifier
	 */
	TYPE_WETFILMHUMIDIFIER("ahu.wetfilmHumidifier", "Wet film Humidifier", "wet_film_humidification_section", "1.0.0", "I", "009"),

	/**
	 * J 高压喷雾加湿段ID:ahu.sprayHumidifier
	 */
	TYPE_SPRAYHUMIDIFIER("ahu.sprayHumidifier", "Spray Humidifier", "high_pressure_spray_humidification_section", "1.0.0", "J", "010"),

	/**
	 * K 风机段 ID:ahu.fan
	 */
	TYPE_FAN("ahu.fan", "Fan", "fan_section", "1.0.0", "K", "011"),
	
	   /**
     * K 风机段 ID:ahu.fan
     */
    TYPE_WWKFAN("ahu.wwkFan", "Plug Fan", "wwk_fan_section", "1.0.0", "K", "016"),

	/**
	 * L 二次回风段 ID:ahu.combinedMixingChamber
	 */
	TYPE_COMBINEDMIXINGCHAMBER("ahu.combinedMixingChamber", "Combined Mixing Chamber", "fresh_return_air_section", "1.0.0",
			"L", "012"),

	/**
	 * M 消音段 ID:ahu.attenuator
	 */
	TYPE_ATTENUATOR("ahu.attenuator", "Attenuator", "silence_section", "1.0.0", "M", "013"),

	/**
	 * N 出风段 ID:ahu.discharge
	 */
	TYPE_DISCHARGE("ahu.discharge", "Discharge", "outlet_air_section", "1.0.0", "N", "014"),

	/**
	 * O 空段 ID:ahu.access
	 */
	TYPE_ACCESS("ahu.access", "Access", "empty_section", "1.0.0", "O", "015"),

	/**
	 * V 高效过滤段 ID:ahu.HEPAFilter
	 */
	TYPE_HEPAFILTER("ahu.HEPAFilter", "HEP AFilter", "high_performance_filtration_section", "1.0.0", "V", "022"),

	/**
	 * X 电极加湿器 ID:ahu.electrodeHumidifier
	 */
	TYPE_ELECTRODEHUMIDIFIER("ahu.electrodeHumidifier", "Electrode Humidifier", "electrode_humidifier", "1.0.0", "X",
			"024"),

	/**
	 * Y 静电过滤段 ID:ahu.electrostaticFilter
	 */
	TYPE_ELECTROSTATICFILTER("ahu.electrostaticFilter", "Electrostatic Filter", "electrostatic_filter_section", "1.0.0", "Y",
			"025"),

	/**
	 * Z CTR控制段 ID:ahu.ctr
	 */
	TYPE_CTR("ahu.ctr", "CTR", "control_section", "1.0.0", "Z", "026"),

	/**
	 * W 热回收 ID:ahu.heatRecycle
	 */
	TYPE_HEATRECYCLE("ahu.heatRecycle", "Heat Recycle", "heat_recycle", "1.0.0", "W", ""),

	/**
	 * 转轮热回收 ID:ahu.wheelHeatRecycle
	 */
	TYPE_WHEELHEATRECYCLE("ahu.wheelHeatRecycle", "Wheel Heat Recycle", "wheel_heat_recycle", "1.0.0", "W2", "043"),

	/**
	 * 板式热回收 ID:ahu.heatRecycle
	 */
	TYPE_PLATEHEATRECYCLE("ahu.plateHeatRecycle", "Plate Heat Recycle", "plate_heat_recycle", "1.0.0", "W1", "023"),

	/**
	 * 热回收 ID:ahu.heatRecycle
	 */
	META_DEFAULT_PARAMETER("meta.defaultPara", "Default Parameter", "default_parameter", "1.0.0", "", "");

	private String id;// 编码
	private String name;// 英文名
	private String cnName;// 中文名称
	private String version;// 版本编号
	private String code;// 代码,计算用
	private String codeNum;// 代码，组编码用

	/**
	 * Get section ID<br>
	 * 编码
	 *
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * Get section name<br>
	 * 名称
	 *
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Get section cnName<br>
	 * 中文名称
	 *
	 * @return
	 */
	public String getCnName() {
		return this.cnName;
	}

	/**
	 * Get section version<br>
	 * 版本号
	 *
	 * @return
	 */
	public String getVersion() {
		return this.version;
	}

	public String getCode() {
		return code;
	}

	/**
	 * Get section codeNum<br>
	 * 版本号
	 *
	 * @return
	 */
	public String getCodeNum() {
		return codeNum;
	}

	SectionTypeEnum(String id, String name, String cnName, String version, String code, String codeNum) {
		this.id = id;
		this.name = name;
		this.cnName = cnName;
		this.version = version;
		this.code = code;
		this.codeNum = codeNum;
	}

	public static SectionTypeEnum getSectionTypeFromId(String id) {
		for (SectionTypeEnum code : values()) {
			if (code.getId().equals(id)) {
				return code;
			}
		}
		return null;
	}

	public static SectionTypeEnum getSectionTypeByCodeNum(String codeNum) {
		for (SectionTypeEnum enu : values()) {
			if (enu.getCodeNum().equals(fixTypeNoCode(codeNum))) {
				return enu;
			}
		}
		return null;
	}

    public static SectionTypeEnum getSectionTypeByCode(String code) {
        for (SectionTypeEnum sectionType : values()) {
            if (sectionType.getCode().equals(code)) {
                return sectionType;
            }
        }
        return null;
    }

	public static boolean isFilter(String key) {
		if (key.toLowerCase().endsWith("filter")) {
			return true;
		}
		return false;
	}

	public static String fixTypeNoCode(String typeNoCode) {
		if (typeNoCode.length() < 3) {
			typeNoCode = "000" + typeNoCode;
			typeNoCode = typeNoCode.substring(typeNoCode.length() - 3);
		}
		return typeNoCode;
	}

	public static String cutTypeNoCode(String typeNoCode) {
		while (typeNoCode.startsWith("0")) {
			typeNoCode = typeNoCode.substring(1);
		}
		return typeNoCode;
	}

    public static String simpleSectionName(String sectionType) {
        return sectionType.replace("ahu.", "");
    }

}

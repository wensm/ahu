package com.carrier.ahu.po.model;

/**
 * Describe carries's factory information<br>
 * It has relationship with product Serials
 * 
 * @author JL
 *
 */
public class Factory extends AbstractModel {
	private static final long serialVersionUID = -3303689439011378676L;
	private String name;
	private String address;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}

package com.carrier.ahu.po.model;

/**
 * @author JL
 *
 */
public enum ModelType {
	WORKSPACE("workspace"), PROJECT("project"), AHU("ahu"), SECTION("section");

	private String name;

	public String getName() {
		return this.name;
	}

	private ModelType(String name) {
		this.name = name;
	}
}

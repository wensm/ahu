package com.carrier.ahu.common.util;

import java.io.UnsupportedEncodingException;

import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.util.EmptyUtil;

/**
 * 转换字符串的编码
 */
public class ChangeCharset {
	/**
	 * 将字符编码转换成US-ASCII码
	 */
	public static String toASCII(String str) throws UnsupportedEncodingException {
		return changeCharset(str, CommonConstant.SYS_ENCODING_US_ASCII);
	}

	/**
	 * 将字符编码转换成ISO-8859-1码
	 */
	public static String toISO_8859_1(String str) throws UnsupportedEncodingException {
		return changeCharset(str, CommonConstant.SYS_ENCODING_ISO_8859_1);
	}

	/**
	 * 将字符编码转换成UTF-8码
	 */
	public static String toUTF_8(String str) throws UnsupportedEncodingException {
		return changeCharset(str, CommonConstant.SYS_ENCODING_UTF_8);
	}

	/**
	 * 将字符编码转换成UTF-16BE码
	 */
	public static String toUTF_16BE(String str) throws UnsupportedEncodingException {
		return changeCharset(str, CommonConstant.SYS_ENCODING_UTF_16BE);
	}

	/**
	 * 将字符编码转换成UTF-16LE码
	 */
	public static String toUTF_16LE(String str) throws UnsupportedEncodingException {
		return changeCharset(str, CommonConstant.SYS_ENCODING_UTF_16LE);
	}

	/**
	 * 将字符编码转换成UTF-16码
	 */
	public static String toUTF_16(String str) throws UnsupportedEncodingException {
		return changeCharset(str, CommonConstant.SYS_ENCODING_UTF_16);
	}

	/**
	 * 将字符编码转换成GBK码
	 */
	public static String toGBK(String str) throws UnsupportedEncodingException {
		return changeCharset(str, CommonConstant.SYS_ENCODING_GBK);
	}

	/**
	 * 字符串编码转换的实现方法
	 * 
	 * @param str
	 *            待转换编码的字符串
	 * @param newCharset
	 *            目标编码
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String changeCharset(String str, String newCharset) throws UnsupportedEncodingException {
		if (EmptyUtil.isNotEmpty(str)) {
			// 用默认字符编码解码字符串。
			byte[] bs = str.getBytes();
			// 用新的字符编码生成字符串
			return new String(bs, newCharset);
		}
		return null;
	}

	/**
	 * 字符串编码转换的实现方法
	 * 
	 * @param str
	 *            待转换编码的字符串
	 * @param oldCharset
	 *            原编码
	 * @param newCharset
	 *            目标编码
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String changeCharset(String str, String oldCharset, String newCharset)
			throws UnsupportedEncodingException {
		if (EmptyUtil.isNotEmpty(str)) {
			// 用旧的字符编码解码字符串。解码可能会出现异常。
			byte[] bs = str.getBytes(oldCharset);
			// 用新的字符编码生成字符串
			return new String(bs, newCharset);
		}
		return null;
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		String str = "This is a 中文的 String!";
		System.out.println("str: " + str);
		String gbk = ChangeCharset.toGBK(str);
		System.out.println("转换成GBK码: " + gbk);
		System.out.println();
		String ascii = ChangeCharset.toASCII(str);
		System.out.println("转换成US-ASCII码: " + ascii);
		gbk = ChangeCharset.changeCharset(ascii, CommonConstant.SYS_ENCODING_US_ASCII, CommonConstant.SYS_ENCODING_GBK);
		System.out.println("再把ASCII码的字符串转换成GBK码: " + gbk);
		System.out.println();
		String iso88591 = ChangeCharset.toISO_8859_1(str);
		System.out.println("转换成ISO-8859-1码: " + iso88591);
		gbk = ChangeCharset.changeCharset(iso88591, CommonConstant.SYS_ENCODING_ISO_8859_1, CommonConstant.SYS_ENCODING_GBK);
		System.out.println("再把ISO-8859-1码的字符串转换成GBK码: " + gbk);
		System.out.println();
		String utf8 = ChangeCharset.toUTF_8(str);
		System.out.println("转换成UTF-8码: " + utf8);
		gbk = ChangeCharset.changeCharset(utf8, CommonConstant.SYS_ENCODING_UTF_8, CommonConstant.SYS_ENCODING_GBK);
		System.out.println("再把UTF-8码的字符串转换成GBK码: " + gbk);
		System.out.println();
		String utf16be = ChangeCharset.toUTF_16BE(str);
		System.out.println("转换成UTF-16BE码:" + utf16be);
		gbk = ChangeCharset.changeCharset(utf16be, CommonConstant.SYS_ENCODING_UTF_16BE, CommonConstant.SYS_ENCODING_GBK);
		System.out.println("再把UTF-16BE码的字符串转换成GBK码: " + gbk);
		System.out.println();
		String utf16le = ChangeCharset.toUTF_16LE(str);
		System.out.println("转换成UTF-16LE码:" + utf16le);
		gbk = ChangeCharset.changeCharset(utf16le, CommonConstant.SYS_ENCODING_UTF_16LE, CommonConstant.SYS_ENCODING_GBK);
		System.out.println("再把UTF-16LE码的字符串转换成GBK码: " + gbk);
		System.out.println();
		String utf16 = ChangeCharset.toUTF_16(str);
		System.out.println("转换成UTF-16码:" + utf16);
		gbk = ChangeCharset.changeCharset(utf16, CommonConstant.SYS_ENCODING_UTF_16LE, CommonConstant.SYS_ENCODING_GBK);
		System.out.println("再把UTF-16码的字符串转换成GBK码: " + gbk);
		String s = new String("中文".getBytes("UTF-8"), "UTF-8");
		System.out.println(s);
	}
}
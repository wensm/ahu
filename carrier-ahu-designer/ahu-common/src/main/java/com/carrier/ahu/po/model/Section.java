package com.carrier.ahu.po.model;

/**
 * @author JL
 * 
 *         Describe a section model, which will be part of AHU
 *
 */
public class Section extends AbstractModel {
	private static final long serialVersionUID = -3303689439011378676L;

	private String id;
	private String version;

	public Section(String id, String version) {
		this.id = id;
		this.version = version;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}

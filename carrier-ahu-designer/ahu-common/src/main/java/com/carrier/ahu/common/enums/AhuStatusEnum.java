package com.carrier.ahu.common.enums;

/**
 * AHU的初始状态为：正在选型（after 新建项目），导入完成（after 导入项目）</br>
 * 后三步依次为：选型完成，分段完成，面板布置完成 </br>
 * 最终为已归档 </br>
 * </br>
 * 同时，项目的状态与其中的AHU的状态同步，取完成度最低的机组的状态 </br>
 * 
 */
public enum AhuStatusEnum {
	SELECTING("selecting", "正在选型", "Selecting"), // 初始状态
	IMPORT_COMPLETE("imported", "导入完成", "Import Complete"), // 初始状态
	SELECT_COMPLETE("selected", "选型完成", "Select Complete"), // 一般销售员会做很多轮项目选型，所以在选型完成以后，还可以继续变为正在选型，重新进行选型<br>
	UNSPLIT("unsplit", "未分段", "UnSplit"), // 未分段
	SPLIT_COMPLETE("splitted", "分段完成", "Split Complete"), // 分段完成<br>
	PANEL_COMPLETE("panelled", "面板布置完成", "Panel Complete"), // 面板布置完成<br>
	ARCHIVED("archieved", "已归档", "Archived");// 选型生命周期结束，之后只能查看<br>

	private String cnName;// 中文名称
	private String name;// 英文名称
	private String id;// id

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCnName() {
		return this.cnName;
	}

	AhuStatusEnum(String id, String cnName, String name) {
		this.id = id;
		this.cnName = cnName;
		this.name = name;
	}
}

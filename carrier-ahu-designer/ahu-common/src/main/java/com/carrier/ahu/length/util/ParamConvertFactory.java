package com.carrier.ahu.length.util;

import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.resistance.param.ResistanceParam;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by LIANGD4 on 2017/11/20.
 */
public class ParamConvertFactory implements SystemCalculateConstants {
    public static LengthParam lengthParam(LengthParam lengthParam) {
        //页面boolean对象转换
        StringBuffer mixTypeSBF = new StringBuffer();
        if (lengthParam.isReturnTop()) {
            mixTypeSBF.append(CommonConstant.SYS_STRING_NUMBER_1);
        }
        if (lengthParam.isReturnButtom()) {
            mixTypeSBF.append(CommonConstant.SYS_STRING_NUMBER_1);
        }
        if (lengthParam.isReturnBack()) {
            mixTypeSBF.append(CommonConstant.SYS_STRING_NUMBER_2);
        }
        if (lengthParam.isReturnLeft()) {
            mixTypeSBF.append(CommonConstant.SYS_STRING_NUMBER_3);
        }
        if (lengthParam.isReturnRight()) {
            mixTypeSBF.append(CommonConstant.SYS_STRING_NUMBER_3);
        }
        lengthParam.setMixType(mixTypeSBF.toString());
        return lengthParam;
    }

    public static ResistanceParam resistanceParam(ResistanceParam resistanceParam){
		if ((CommonConstant.SYS_SEXON_ALIAS_FILTER + CommonConstant.SYS_SEXON_ALIAS_COMBINEDFILTER
				+ CommonConstant.SYS_SEXON_ALIAS_HEPAFILTER + CommonConstant.SYS_SEXON_ALIAS_ELECTROSTATICFILTER)
						.contains(resistanceParam.getSectionType())) {
			if (FILTER_FITLERSTANDARD_GB.equals(resistanceParam.getFitlerStandard())) {
				resistanceParam.setFitlerStandard(FILTER_FITLERSTANDARD_J_ALL);
			} else if (FILTER_FITLERSTANDARD_ALL.equals(resistanceParam.getFitlerStandard())) {
				resistanceParam.setFitlerStandard(FILTER_FITLERSTANDARD_J_GB);
			} else if (FILTER_FITLERSTANDARD_EN.equals(resistanceParam.getFitlerStandard())) {
				resistanceParam.setFitlerStandard(FILTER_FITLERSTANDARD_J_EN);
			}
		}
        return resistanceParam;
    }

}

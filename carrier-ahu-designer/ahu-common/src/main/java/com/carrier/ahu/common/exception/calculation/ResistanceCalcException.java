package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class ResistanceCalcException extends CalculationException {

    public ResistanceCalcException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public ResistanceCalcException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public ResistanceCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResistanceCalcException(String message) {
        super(message);
    }

}

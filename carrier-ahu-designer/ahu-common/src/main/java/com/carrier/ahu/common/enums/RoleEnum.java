package com.carrier.ahu.common.enums;

/**
 * Created by Braden Zhou on 2019/07/17.
 */
public enum RoleEnum {

    Sales, Engineer, Factory, Design, Admin;

}

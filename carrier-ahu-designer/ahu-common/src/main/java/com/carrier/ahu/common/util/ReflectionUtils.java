package com.carrier.ahu.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * Extract from InvokeTool class.
 * 
 * Created by Braden Zhou on 2018/06/15.
 */
@Slf4j
public class ReflectionUtils {

    /**
     * 
     * @param fieldName
     * @param o
     * @return
     */
    public static Object getFieldValueByName(String fieldName, Class<?> o) {
        try {
            Field field = o.getDeclaredField(fieldName);
            return field.get(o);
        } catch (NoSuchFieldException | SecurityException e) {
            log.warn(String.format("Parameter value option item cannot be found on %s", fieldName));
        } catch (IllegalArgumentException e) {
            log.warn(String.format("Parameter value option item cannot be found on %s", fieldName));
        } catch (IllegalAccessException e) {
            log.warn(String.format("Parameter value option item cannot be found on %s", fieldName));
        }
        return null;

    }

    /**
     * 根据属性名获取属性值
     */
    public static Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[] {});
            Object value = method.invoke(o, new Object[] {});
            return value;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 获取属性名数组
     */
    public static String[] getFiledName(Object o) {
        Field[] fields = o.getClass().getDeclaredFields();
        String[] fieldNames = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            fieldNames[i] = fields[i].getName();
        }
        return fieldNames;
    }

    /**
     * 获取对象的所有属性值，返回一个对象数组
     */
    public static Object[] getFiledValues(Object o) {
        String[] fieldNames = getFiledName(o);
        Object[] value = new Object[fieldNames.length];
        for (int i = 0; i < fieldNames.length; i++) {
            value[i] = getFieldValueByName(fieldNames[i], o);
        }
        return value;
    }

    public static String getProperty(Object obj, String name) {
        return getProperty(obj, name, StringUtils.EMPTY);
    }

    public static String getProperty(Object obj, String name, String defaultValue) {
        try {
            String value = BeanUtils.getProperty(obj, name);
            if (value == null || "null".equals(value)) {
                return defaultValue;
            }
            return value;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    @SuppressWarnings("rawtypes")
    public static List<String> getFieldNames(Class clazz) {
        List<String> fieldNames = Lists.newArrayList();
        Field[] fields = clazz.getDeclaredFields();
        Arrays.asList(fields).forEach(f -> fieldNames.add(f.getName()));
        return fieldNames;
    }

}

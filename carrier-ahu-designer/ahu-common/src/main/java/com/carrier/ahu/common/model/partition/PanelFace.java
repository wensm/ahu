package com.carrier.ahu.common.model.partition;

import lombok.Data;

/**
 * Created by liujianfeng on 2017/9/1.
 */
@Data
public class PanelFace {
	

	/*  - 没有切割：0;
		- 横向切割：1;
		- 纵向切割：2;*/
	private int direction;
	// Only two sub panels would be defined
	private PanelFace[] subPanels;
	private int panelWidth;
	private int panelLength;
	private String id;
	private String panelType;
	private String cutDirection;//好像被弃用了
	private FrameLine frameLine;
	private String memo = "";
	/** 面板连接器00,0A,0B;A0,B0... */
    private String panelConnector = "";
    private String rootPanelId;


    public PanelFace() {
	}

    /**
     * Use mold size unit. Or use AhuPartitionGenerator.createPanelFace().
     *  
     * @param length
     * @param width
     */
	public PanelFace(int length, int width) {
		this.panelWidth = width;
		this.panelLength = length;
	}
	
	public boolean isLeafPanel() {
		if(this.subPanels==null||this.subPanels.length==0) {
			return true;
		}
		
		return false;
	}


	/**
	 * 后台计算相关参数
	 */
	//额外计算参数
	private String sectionMetaId;//当前面板属于什么段
	private String sectionDoorOpendAndViewport;//段是否开门/观察窗格式：（true;true）
	private String parentsId;
}

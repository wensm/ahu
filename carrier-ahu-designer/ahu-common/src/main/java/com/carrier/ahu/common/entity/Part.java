package com.carrier.ahu.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.carrier.ahu.po.AbstractPo;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
public class Part extends AbstractPo {
    private static final long serialVersionUID = 7084687735207371058L;
    @Id
    private String partid;//段ID
    private String pid;//项目ID
    private String unitid;//AHU ID
    @Column(name="key")
    private String sectionKey;
    private Short orders;
    private Short types;
    private Short position;
    private Short pos;//所属分段索引，从1开始
    private Short setup;
    private String pic;
    private String groupi;
    private Double cost;
    private Short avdirection;
    private Boolean topLayer;//是否顶层

    public String getSectionKey() {
        return sectionKey;
    }

    public void setSectionKey(String key) {
        this.sectionKey = key;
    }
}
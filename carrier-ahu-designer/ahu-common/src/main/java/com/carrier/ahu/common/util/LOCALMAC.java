package com.carrier.ahu.common.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/*
 * 物理地址是48位，别和ipv6搞错了
 */
public class LOCALMAC {

    public static String getMAC() throws UnknownHostException, SocketException {
        InetAddress ia = InetAddress.getLocalHost();
        byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
        return parseMacAddress(mac);
    }

    public static boolean isLocalMac(String mac) {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                String localMac = parseMacAddress(networkInterface.getHardwareAddress());
                if (mac.equalsIgnoreCase(localMac) && !networkInterface.isVirtual()) {
                    return true;
                }
            }
        } catch (Exception exp) {
            return false;
        }
        return false;
    }

    private static String parseMacAddress(byte[] mac) {
        if (mac != null) {
            StringBuffer sb = new StringBuffer("");
            for (int i = 0; i < mac.length; i++) {
                if (i != 0) {
                    sb.append("-");
                }
                // 字节转换为整数
                int temp = mac[i] & 0xff;
                String str = Integer.toHexString(temp);
                if (str.length() == 1) {
                    sb.append("0" + str);
                } else {
                    sb.append(str);
                }
            }
            return sb.toString();
        }
        return "";
    }

}

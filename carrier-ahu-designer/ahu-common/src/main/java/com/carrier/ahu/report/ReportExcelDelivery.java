package com.carrier.ahu.report;

import lombok.Data;

@Data
public class ReportExcelDelivery {
	private String unitId;// 序号,机组编号
	private String serial;// 机组型号，序列号
	private String ahuName;// 客户机组名称
	
	private String[] delivery;// 长交货期选项
	private String[] days;// 交货期(天
}

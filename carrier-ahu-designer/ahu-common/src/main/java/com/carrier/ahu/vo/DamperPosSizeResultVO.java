package com.carrier.ahu.vo;

import lombok.Data;
import lombok.ToString;

/**
 * Created by Braden Zhou on 2019/08/13.
 */
@Data
@ToString(callSuper = true)
public class DamperPosSizeResultVO {

    public DamperPosSizeResultVO(double[] outputValues) {
        this.posSizeA = outputValues[0];
        this.posSizeB = outputValues[1];
        this.posSizeC = outputValues[2];
        this.posSizeD = outputValues[3];
        this.sizeX = outputValues[4];
        this.sizeY = outputValues[5];
    }

    private double posSizeA;
    private double posSizeB;
    private double posSizeC;
    private double posSizeD;
    private double sizeX;
    private double sizeY;
    private double fanSpeed;

}

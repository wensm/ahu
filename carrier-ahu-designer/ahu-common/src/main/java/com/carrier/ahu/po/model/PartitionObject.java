package com.carrier.ahu.po.model;

import java.util.Map;

import lombok.Data;

@Data
public class PartitionObject {
	int pos;
	int length;
	int width;
	int height;
	int casingWidth;
	Map<Integer, PartitionPanel> panelMap;
	PartitionSection[] sections;
}

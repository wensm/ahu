package com.carrier.ahu.length.param;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/9.
 */
@Data
public class FilterParam {

    private String sectionType;//段类别 BCVY
    private String MediaLoading;//抽取方式 单层过滤器 综合过滤器 mediaLoading TODO 板式和袋式的区别

    /*备注：*/
    //1.初效过滤 段类别：单层过滤器B、综合过滤器C、静电过滤器Y 抽取方式：正抽、外抽、侧抽
    //2.高效过滤 段类别：高效过滤器V

}

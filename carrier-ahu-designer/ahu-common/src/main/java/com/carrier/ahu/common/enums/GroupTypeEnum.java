package com.carrier.ahu.common.enums;

/**
 * 组类型枚举 Created by Wen zhengtao on 2017/3/25.
 */
public enum GroupTypeEnum {
    /** 基本型 ID:tp1 */
    TYPE_BASIC("tp1", "Basic Unit", "basic_model", true, true),

    /** 基本型 ID:tp1 */
    TYPE_BASIC_2("tp1_2", "Basic Unit 2", "basic_model_2", true, true),

    /** 二管制舒适型 ID:tp2 */
    TYPE_DOUBLE_PIPE("tp2", "Two-Pipes Unit", "two_pipes_comfort_model", true, true),

    /** 四管制舒适型 ID:tp3 */
    TYPE_FOUR_PIPE("tp3", "Four-Pipes Unit", "four_pipes_unit_comfort", true, true),

    /** 全年节能调节的四管制舒适型 ID:tp4 */
    TYPE_FULL_YEAR("tp4", "Energy-saving Four-Pipes Unit", "full_year_energy_saving_four_pipe_comfort", true, true),

    //Eurovent update begin
    /** 转轮能量回收器的四管制舒适型 ID:tp5 */
//    TYPE_RUNNER("tp5", "Enthalpy Recovery Wheels Unit", "wheel_energy_recycler_comfort", true, false),
    TYPE_RUNNER("tp5", "Heat Recovery System Unit", "wheel_energy_recycler_comfort_Euro", true, false),

    /** 板式能量回收器的四管制舒适型 ID:tp6 */
    TYPE_PLATE("tp6", "Enthalpy Recovery Plate Unit", "panel_energy_recycler_comfort_model", true, true),
    //Eurovent update end

    /** 立式机组1 ID:tp7 */
    TYPE_VERTICAL_1("tp7", "Vertical Unit 1", "vertical_unit_1", true, true),

    /** 立式机组2 ID:tp8 */
    TYPE_VERTICAL_2("tp8", "Vertical Unit 2", "vertical_unit_2", false, true), // hide it at the moment

    /** 双层转向1 ID:tp9 */
    TYPE_DOUBLE_RETURN_1("tp9", "Double Return 1", "double_return_1", true, false),

    /** 双层转向2 ID:tp9_2 */
    TYPE_DOUBLE_RETURN_2("tp9_2", "Double Return 2", "double_return_2", true, false),

    /** 并排转向 ID:tp10 */
    TYPE_SIDE_BY_SIDE_RETURN_1("tp10", "Side by Side Return 1", "side_by_side_return_1", true, false),

    /** 并排转向 ID:tp10_2 */
    TYPE_SIDE_BY_SIDE_RETURN_2("tp10_2", "Side by Side Return 2", "side_by_side_return_2", true, false),

    /** 空白 ID:tp */
    TYPE_BLANK("tpb", "Blank Unit", "blank", false, false),

    /** 未分组 ID:tp */
    TYPE_UNGROUP("tpu", "Non-Grouped Unit", "unclassified", false, false),

    /** 自定义 ID:tp0 */
    TYPE_CUSTOM("tp0", "Self-Defined Unit", "customize", false, true);

    private String id;
    private String templateName;
    private String cnName;// 中文名称
    private boolean predefined;
    private boolean groupable;

    public String getId() {
        return this.id;
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public String getCnName() {
        return this.cnName;
    }

    public boolean isPredefined() {
        return this.predefined;
    }

    public boolean isGroupable() {
        return this.groupable;
    }

    GroupTypeEnum(String id, String templateName, String cnName, boolean predefined, boolean groupable) {
        this.id = id;
        this.templateName = templateName;
        this.cnName = cnName;
        this.predefined = predefined;
        this.groupable = groupable;
    }

    public static GroupTypeEnum valueIdOf(String id) {
        for (GroupTypeEnum groupType : GroupTypeEnum.values()) {
            if (groupType.getId().equals(id)) {
                return groupType;
            }
        }
        return null;
    }

}

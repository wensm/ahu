package com.carrier.ahu.report;

import lombok.Data;

@Data
public class ReportExcelCRMList {
	private String unitId;// 机组编号
	private String unitType;// 机组类型
	private String serial;// 机组型号
	private int mount;// 数量
	private double facePrice;//机组面价
	private String productionPlace;//产地
	private String contract;//合同号
	private String selectionTimes;//选型次数
	private String contractSerial;//合同型号
	private String drawingNo;// 图纸编号

	private double stdPrice;// 标准单价
	private String factoryForm;// 出厂形式
	private String isHealthBuilding;// 绿色建筑 0否1是
	private String healthBuilding;// 绿色建筑

}

package com.carrier.ahu.license;

import java.io.File;

import com.carrier.ahu.common.util.FileUtil;
import com.carrier.ahu.common.util.RSAUtils;
import com.carrier.ahu.constant.CommonConstant;

/**
 * 生成license
 * 
 * @author Simon 2018.01.28
 */
public class LicenseGeneratorTester {

	private static final String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCiQq0T5YwvRYsdx/HGg1zAISyI5MZ788qis0XG\r\n"
			+ "faf4QsrSsaactxN1HH3NvfZ7k6+Gia/7coniTEabXtxG96CY1ccIRax47YxJEX4QJceAcTEr4AmA\r\n"
			+ "z4DR6MS3Bq4DiskM1szyMl0i4B7Dy7G0VypgOb6JFhdmaB0KkGnRpMhyOwIDAQAB";

	/**
	 * RSA算法 公钥和私钥是一对，此处只用私钥加密
	 */
	public static final String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKJCrRPljC9Fix3H8caDXMAhLIjk\r\n"
			+ "xnvzyqKzRcZ9p/hCytKxppy3E3Ucfc299nuTr4aJr/tyieJMRpte3Eb3oJjVxwhFrHjtjEkRfhAl\r\n"
			+ "x4BxMSvgCYDPgNHoxLcGrgOKyQzWzPIyXSLgHsPLsbRXKmA5vokWF2ZoHQqQadGkyHI7AgMBAAEC\r\n"
			+ "gYBywZFC9x+z9w0ynMxDx3f7cfrbPmGaHIt9kiiQ/yGilSp45KefQmOCegoHTohaUdLb5dkOjq0x\r\n"
			+ "3Rw6mlZT4qIzFL+ScmeOUdQBq6IbIMiDs6XL6SzBoivO/RUhNJ2v28olAn70lxSkUv6oGHVYx/rR\r\n"
			+ "7CaaKaucbtY4YdSjq4Tv8QJBANTSwNxMZis3jCYFl7l6ZE5JJo3A+Vcn5+b33YgaGTBbLp4z0b39\r\n"
			+ "2STzk8FleHVxm+YfBDxfPbkvvU6+dgHezs0CQQDDLeEWzbRkqrnOhO0wglQm1KAyixnt1Cb3lOXm\r\n"
			+ "pBAJDOjVR8DK7MX+cmL64U8g1Prbd/txZgdJoMQEFILiILUnAkEAtUgQH5a0MHvQVVLOOp+WcVi+\r\n"
			+ "DmEiAqtrTcI0YYpZimcg/oEFV+Cy5lbUtRfJ08BCG3KwGPzHhsazai+yNUWkwQJAJC8/SbD9PqSA\r\n"
			+ "Kg9uwc2HuGM3B+fN+M+aIdBTBv1nk6X3DINu9LAMzO17+DMJLncNwt5tmUpnE3lgnbkHwtB5hQJA\r\n"
			+ "e5L8P2p6k0ibWNJHCK4JWXrmrkF35vvkUgN7BF7zr8s00BXfpxtOdPcFHqGx2E9noq7jdWU4dKsH\r\n" + "k5rwpW0nVA==";

	public static String privateKeyEncode(String licenseString) throws Exception {
		System.out.println("原文字：\r\n" + licenseString);
		byte[] data = licenseString.getBytes();
		byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
		System.out.println("加密后：\r\n" + new String(encodedData)); // 加密后乱码是正常的

		FileUtil.byteArrayToFile(encodedData, FileUtil.getBasePath() + File.separator + CommonConstant.SYS_FILE_NAME_LICENSE_DAT);
		String licensePath = FileUtil.getBasePath() + File.separator + CommonConstant.SYS_FILE_NAME_LICENSE_DAT;
		System.out.println("license.dat：\r\n" + licensePath);
		return licensePath;
	}

	public static String publicKeyDecode(String licensePath) throws Exception {
		byte[] encodedData = FileUtil.fileToByte(licensePath);
		byte[] decodedData = RSAUtils.decryptByPublicKey(encodedData, publicKey);
		String target = new String(decodedData);
		System.out.println("解密后: \r\n" + target);
		return target;
	}

	public static void main(String[] args) throws Exception {
		String licensePath = CommonConstant.SYS_PATH_LICENSE_DAT;
		publicKeyDecode(licensePath);
	}
}
package com.carrier.ahu.util;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.sun.org.apache.bcel.internal.generic.ReturnaddressType;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

/**
 * Created by liujianfeng on 2017/7/30.
 */
public class NumberUtil {

    public static double scale(double d, int scale) {
        BigDecimal bg = new BigDecimal(d);
        double d1 = bg.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
        return d1;
    }

    public static double scale(float f, int scale) {
        BigDecimal bg = new BigDecimal(f);
        double f1 = bg.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
        return f1;
    }

    public static String ordinal(int i) {
        String[] sufixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
        switch (i % 100) {
        case 11:
        case 12:
        case 13:
            return i + "th";
        default:
            return i + sufixes[i % 10];
        }
    }

    public static int extractInt(String str) {
        StringBuilder intStr = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            String ch = String.valueOf(str.charAt(i));
            if (StringUtils.isNumeric(ch)) {
                intStr.append(ch);
            }
        }
        return NumberUtils.toInt(intStr.toString());
    }

    public static String onlyNumbers(String value) {
        String regEx = "[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(value);
        return m.replaceAll("").trim();
    }

    public static String scale(String value, int scale) {
        if (NumberUtils.isParsable(value)) {
            return String.format("%." + scale + "f", NumberUtils.toDouble(value));
        }
        return value;
    }
    
    public static Double convertStringToDouble(String doubleStr) {
    	if(EmptyUtil.isEmpty(doubleStr)) {
    		return 0.0;
    	}
    	
    	return Double.parseDouble(doubleStr);
    }
    /**
     * 解析转换数字字符串为int型数字，规则如下：
     *
     * 1、按照10进制转换
     * 2、空串返回0
     * 3、.123形式返回0（按照小于0的小数对待）
     * 4、123.56截取小数点之前的数字，忽略小数部分
     *
     * @param number 数字
     * @return int
     * @throws NumberFormatException 数字格式异常
     * @since 4.1.4
     */
    public static int convertStringToDoubleInt(String number) throws NumberFormatException {
        if (StrUtil.isBlank(number)) {
            return 0;
        }
        // 对于带小数转换为整数采取去掉小数的策略
        number = StrUtil.subBefore(number, CharUtil.DOT, false);
        if (StrUtil.isEmpty(number)) {
            return 0;
        }
        return Integer.parseInt(number);
    }
}

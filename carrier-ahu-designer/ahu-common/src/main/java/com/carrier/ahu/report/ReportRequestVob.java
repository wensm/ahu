package com.carrier.ahu.report;

import lombok.Data;

@Data
public class ReportRequestVob {
	private String projectId;
	private String[] ahuIds;
	private Report[] reports;
}

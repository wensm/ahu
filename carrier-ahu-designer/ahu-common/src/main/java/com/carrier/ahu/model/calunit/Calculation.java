package com.carrier.ahu.model.calunit;

import java.util.Map;

public class Calculation {
	private Short position;
	private String sectionKey;
	private Map<String,Object> values;
	public Short getPosition() {
		return position;
	}
	public void setPosition(Short position) {
		this.position = position;
	}
	public String getSectionKey() {
		return sectionKey;
	}
	public void setSectionKey(String sectionKey) {
		this.sectionKey = sectionKey;
	}
	public Map<String, Object> getValues() {
		return values;
	}
	public void setValues(Map<String, Object> values) {
		this.values = values;
	}
	
	
	
	
}

package com.carrier.ahu.common.enums;

/**
 * Created by LIANGD4 on 2017/12/12.
 */
public enum AirDirectionEnum {

    SUPPLYAIR("S", "送风"),// 送风
    RETURNAIR("R", "回风"); //回风

    private String code;//值
    private String name;//名称

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    AirDirectionEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }
}

package com.carrier.ahu.common.model.partition;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by dxx on 2019/1/17.
 */
@Data
public class XTSummary implements Serializable {

	private String partName;//名称
	private String partWM;//宽度
	private String partLM;//长度
	private String quantity;//数量
}

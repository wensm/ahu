package com.carrier.ahu.calculator.price;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.engine.price.AHUPriceCalFactory;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SysConstants;

public class PriceCalRunner extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(PriceCalRunner.class.getName());

    // input
    private final static BlockingQueue<String> bq = new ArrayBlockingQueue<String>(1);
    // output
    private final static Map<String, String> results = new HashMap<String, String>();

    private boolean flg = true;
    private boolean runningFlg = false;
    private String priceBase;
    private int language;

    private PriceCalRunner() {
    }

    private final static PriceCalRunner runner = new PriceCalRunner();

    public static PriceCalRunner getInstance() {
        return runner;
    }

    public void begin(String priceBase, int language) {
        if (!PriceCalRunner.getInstance().runningFlg) {
            PriceCalRunner.getInstance().runningFlg = true;
            PriceCalRunner.getInstance().priceBase = priceBase;
            PriceCalRunner.getInstance().language = language;
            PriceCalRunner.getInstance().setDaemon(true);
            PriceCalRunner.getInstance().start();
        }
    }

    @SuppressWarnings("static-access")
    public void run() {
        File priceResultFile = new File(String.format(SysConstants.FILE_AHU_PRICE_TXT, priceBase));
        String destFileName = null;
        while (PriceCalRunner.getInstance().flg) {
            try {
                PriceCalRunner.getInstance().runningFlg = true;
                String key = bq.take();
                if (priceResultFile.exists()) {
                    priceResultFile.delete();
                }
                
                //retry 3 times
                int retryTimes=0;
				while (retryTimes < 3) {
					try {
						AHUPriceCalFactory.getAHUPriceCal(priceBase).PriceCAL(language);
						Thread.sleep(800);
						if (priceResultFile.exists()) {
							destFileName = SysConstants.FILE_AHU_PRICE_PATH.concat(key);
							priceResultFile.renameTo(new File(destFileName));
							PriceCalRunner.getInstance().results.put(key, destFileName);
							break;
						}
						retryTimes++;
					} catch (Error e) {
						logger.error(e.getMessage(), e);
						retryTimes++;
						logger.debug("tryed run times "+retryTimes);
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
					}
				}
                
            } catch (InterruptedException e) {
                logger.error("Error in Price RUNNER", e);
            }
        }
    }

    public boolean end() {
        PriceCalRunner.getInstance().flg = true;
        PriceCalRunner.getInstance().runningFlg = false;
        return true;
    }

    public void setInput(String timestamp) {
        try {
            bq.put(timestamp);
        } catch (InterruptedException e) {
            logger.error("Error in Price RUNNER to set input", e);
        }
    }

	public String getOutput(String key) {
		try {

			if (PriceCalRunner.getInstance().results.containsKey(key)) {
				String value = PriceCalRunner.getInstance().results.get(key);
				PriceCalRunner.getInstance().results.remove(key);
				return value;
			}

		} catch (Exception e) {
			logger.error("Error in price calculation to get output", e);
		}
		return null;
	}
}

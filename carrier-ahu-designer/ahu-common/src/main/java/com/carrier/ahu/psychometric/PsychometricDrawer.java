package com.carrier.ahu.psychometric;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SysConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import sun.font.FontDesignMetrics;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.List;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.intl.I18NConstants.HUMIDITY_RATIO;

/**
 * Created by liujianfeng on 2017/8/30.
 */
@Slf4j
@Service
public class PsychometricDrawer {
	private static String baseImage = "Psychrometric";
	private static int left_x = 306;
	private static int left_y = 2377;
	private static int right_x = 3341;
	private static int right_y = 2377;
	private static int top_x = 3341;
	private static int top_y = 220;
	private static int[] x_range = new int[] { -15, 75 };
	private static int[] y_range = new int[] { 0, 45 };
	private static List<String> lsABCD = Arrays.asList("A","B","C","D","E","F","G","H","I","J","K");

	public static String genPsychometric(String projId, String ahuId, List<PsyCalBean> beans) {
		String fileName = ahuId;
		String destPath = String.format(SysConstants.PSYCHOMETRIC_DIR + File.separator + "%s" + File.separator + "%s"
				+ File.separator + "%s.png", projId, ahuId, fileName);
		File file = new File(SysConstants.ASSERT_DIR + destPath);
		try {
			InputStream is = PsychometricDrawer.class.getClassLoader()
					.getResourceAsStream(SysConstants.TEMPLATE_JAR_DIR + CommonConstant.SYS_PUNCTUATION_SLASH + baseImage + CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN + AHUContext.getLanguage() + CommonConstant.SYS_PNG_EXTENSION);
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			// InputStream is = new FileInputStream(new
			// File("C:/Users/jaaka/Desktop/psychometric_cn.jpg"));
			BufferedImage image = ImageIO.read(is);
			Graphics2D g2d = image.createGraphics();
			//g2d.rotate(90);//屏蔽
			// int imgDim = 100;
			g2d.fillRect(0, 0, 100, 100);
			BasicStroke bs = new BasicStroke(3);
			g2d.setStroke(bs);

			int lineHeight = 0;
			Font font = new Font("宋体", Font.BOLD, 40);
			FontDesignMetrics metrics = FontDesignMetrics.getMetrics(font);
			if (EmptyUtil.isNotEmpty(beans) && beans.size() != 0) {

				Map<String,String> abcd = new HashMap<String,String>();
				for (PsyCalBean bean : beans) {

					String type = bean.getSectionType();
					if (type == null) {
						continue;
					}
					lineHeight += metrics.getAscent();

					if (type.endsWith(CommonConstant.METASEXON_COOLINGCOIL)) {
						drawSection(bean, g2d, Color.BLUE,abcd);
						drawBeanInfo(g2d, font, Color.BLUE,lineHeight, bean);
					}

					else if (type.endsWith(CommonConstant.METASEXON_HEATINGCOIL)) {
						drawSection(bean, g2d, Color.RED,abcd);
						drawBeanInfo(g2d, font, Color.RED,lineHeight, bean);
					}

					else if (type.endsWith(CommonConstant.METASEXON_HUMIDIFIER)) {
						drawSection(bean, g2d, Color.GREEN,abcd);
						drawBeanInfo(g2d, font, Color.GREEN,lineHeight, bean);
					}


				}
			}

			ImageIO.write(image, CommonConstant.SYS_PNG, file);
			log.info(String.format("File was generated at : %s", file.getAbsolutePath()));

		} catch (FileNotFoundException e) {
			log.error("Failed to find template file: ", e);
		} catch (IOException e) {
			log.error("Failed to read template file: ", e);
		}
		return destPath.replace(File.separator, CommonConstant.SYS_PUNCTUATION_SLASH)+"?dt="+System.currentTimeMillis();
	}

	/**
	 * 往图片写文字
	 * @param g2d
	 * @param font
	 * @param color
	 * @param lineHeight
	 * @param bean
	 */
	private static void drawBeanInfo(Graphics2D g2d, Font font, Color color, int lineHeight, PsyCalBean bean) {
		g2d.setColor(color);
		g2d.setFont(font);
		g2d.drawString(getIntlString(HUMIDITY_RATIO)+": "+bean.getHumidityA()+">"+bean.getHumidityB(), 0, lineHeight);
	}

	private static void drawSection(PsyCalBean bean, Graphics2D g2d, Color lineColor, Map<String, String> abcd) {
		g2d.setColor(lineColor);
		float xLen = (float) (right_x - left_x) / (float) (x_range[1] - x_range[0]);
		float yLen = (float) (top_y - right_y) / (float) (y_range[1] - y_range[0]);
		int pointAx = new Double(xLen * (bean.getDryBulbTempA() - x_range[0])).intValue() + left_x;
		int pointAy = new Double(yLen * (bean.getHumidityA() - y_range[0])).intValue() + right_y;
		int pointBx = new Double(xLen * (bean.getDryBulbTempB() - x_range[0])).intValue() + left_x;
		int pointBy = new Double(yLen * (bean.getHumidityB() - y_range[0])).intValue() + right_y;
		g2d.drawLine(pointAx, pointAy, pointBx, pointBy);
		
		String begin = CommonConstant.SYS_BLANK;
		if(null != abcd.get(pointAx + CommonConstant.SYS_PUNCTUATION_COLON + pointAy + 55)){
			begin = abcd.get(pointAx + CommonConstant.SYS_PUNCTUATION_COLON + pointAy + 55);
		}else{
			begin = lsABCD.get(abcd.size());
			abcd.put(pointAx + CommonConstant.SYS_PUNCTUATION_COLON + pointAy + 55, begin);
		}
		String end = CommonConstant.SYS_BLANK;
		if(null != abcd.get(pointBx + CommonConstant.SYS_PUNCTUATION_COLON + pointBy + 55)){
			end = abcd.get(pointBx + CommonConstant.SYS_PUNCTUATION_COLON + pointBy + 55);
		}else{
			end = lsABCD.get(abcd.size());
			abcd.put(pointBx + CommonConstant.SYS_PUNCTUATION_COLON + pointBy + 55, end);
		}
		
		Font font = new Font(CommonConstant.SYS_FONT_ARIAL, Font.BOLD, 50);
		g2d.setFont(font);
		g2d.setColor(Color.BLACK);
		g2d.fillOval(pointAx - 10, pointAy - 10, 20, 20);
		g2d.drawString(begin, pointAx, pointAy + 55);
		g2d.fillOval(pointBx - 10, pointBy - 10, 20, 20);
		g2d.drawString(end, pointBx, pointBy + 55);
		
	}

	public static List<PsyCalBean> drawPsy(Iterator<PartParam> it) {
		double inDryBT = 20;
		double outDryBT = 20;
		double inHumidify = 10;
		double outHumidify = 10;
		boolean first = true;
		List<PsyCalBean> datas = new ArrayList<PsyCalBean>();
		String preKey = CommonConstant.SYS_BLANK;
		String enableWinterMetaKey = ".EnableWinter";//加湿段冬季生效
		while (it.hasNext()) {
			PartParam partParam = it.next();
			String key = partParam.getKey();
			if (key.endsWith("Coil") || key.endsWith(CommonConstant.METASEXON_HUMIDIFIER)) {
				String prefix = MetaCodeGen.calculateAttributePrefix(key);
               
				Double di = partParam.getParamValue(
						prefix + (CommonConstant.SYS_PUNCTUATION_DOT + CommonConstant.METACOMMON_STRING_SINDRYBULBT),
						Double.class);
				Double dwi = partParam.getParamValue(
						prefix + (CommonConstant.SYS_PUNCTUATION_DOT + CommonConstant.METACOMMON_STRING_SINWETBULBT),
						Double.class);
				Double dout = partParam.getParamValue(
						prefix + (CommonConstant.SYS_PUNCTUATION_DOT + CommonConstant.METACOMMON_STRING_SOUTDRYBULBT), Double.class);
				Double dwout = partParam.getParamValue(
						prefix + (CommonConstant.SYS_PUNCTUATION_DOT + CommonConstant.METACOMMON_STRING_SOUTWETBULBT), Double.class);
				
				Boolean userWinBulbT = false;
				if(key.endsWith(CommonConstant.METASEXON_HUMIDIFIER)){
					String enableWinter = partParam.getParamValue(prefix + enableWinterMetaKey, String.class);
					if(CommonConstant.SYS_ASSERT_TRUE.equals(enableWinter)){//加湿段，如果启用冬季生效，使用冬季温度计算
						userWinBulbT = true;
					}
				}
				
				// 人如果是热水盘管，选冬季的数据
				if (key.endsWith(CommonConstant.METASEXON_HEATINGCOIL) || userWinBulbT) {
					di = partParam.getParamValue(prefix
							+ (CommonConstant.SYS_PUNCTUATION_DOT + CommonConstant.METACOMMON_STRING_WINDRYBULBT),
							Double.class);
					dwi = partParam.getParamValue(prefix
							+ (CommonConstant.SYS_PUNCTUATION_DOT + CommonConstant.METACOMMON_STRING_WINWETBULBT),
							Double.class);
					dout = partParam.getParamValue(prefix
							+ (CommonConstant.SYS_PUNCTUATION_DOT + CommonConstant.METACOMMON_STRING_WOUTDRYBULBT),
							Double.class);
					dwout = partParam.getParamValue(prefix
							+ (CommonConstant.SYS_PUNCTUATION_DOT + CommonConstant.METACOMMON_STRING_WOUTWETBULBT),
							Double.class);
				}
				if (dout != null && dwout != null) {
					AirConditionBean bean = AirConditionUtils.FAirParmCalculate1(dout, dwout);
					outHumidify = bean.getParamD();
				}

				if (di != null && dwi != null) {
					AirConditionBean bean = AirConditionUtils.FAirParmCalculate1(di, dwi);
					inHumidify = bean.getParamD();
				}
				// if (first) {
				if (di != null) {
					inDryBT = di;
				}
				if (dout != null) {
					outDryBT = dout;
				}
				// if (dout != null && dwout != null) {
				// inHumidify = tmpHum;
				// outHumidify = tmpHum;
				// }
				// first = false;
				// } else {
				// if (dout != null) {
				// outDryBT = dout;
				// }
				// if (dout != null && dwout != null) {
				// outHumidify = tmpHum;
				// }
				// }

			}
			PsyCalBean bean = new PsyCalBean();
			bean.setDryBulbTempA(inDryBT);
			bean.setHumidityA(inHumidify);
			bean.setDryBulbTempB(outDryBT);
			bean.setHumidityB(outHumidify);
			bean.setSectionType(key);
			datas.add(bean);
			preKey = partParam.getKey();
		}
		return datas;
	}

}

package com.carrier.ahu.po.meta;
/**
 * system default param
 * @author 
 *
 */
public class AirParam {
	public AirParam(){
		
	}
	
	public AirParam(String seasonType, String paramType, String dryBulbTem, String wetBulbTem, String relativeTem) {
		super();
		this.seasonType = seasonType;
		this.paramType = paramType;
		this.dryBulbTem = dryBulbTem;
		this.wetBulbTem = wetBulbTem;
		this.relativeTem = relativeTem;
	}


	/**
	 * SUMMER 夏季  ,WINTER 冬季
	 * @author
	 *
	 */
	public enum SeasonType{
		SUMMER,WINTER
	}
	
	/**
	 * FRESHAIR 新风,RETURNAIR 回风, OUTAIR 出风
	 * @author
	 *
	 */
	public enum ParamType{
		FRESHAIR,RETURNAIR,OUTAIR
	}
	private String seasonType;
	private String paramType;
	/**
	 * 干球温度
	 */
	private String dryBulbTem;
	
	/**
	 * 湿球温度
	 */
	private String wetBulbTem;
	
	/**
	 * 相对温度
	 */
	private String relativeTem;
	
	public String getSeasonType() {
		return seasonType;
	}

	public void setSeasonType(String seasonType) {
		this.seasonType = seasonType;
	}

	public String getWetBulbTem() {
		return wetBulbTem;
	}

	public void setWetBulbTem(String wetBulbTem) {
		this.wetBulbTem = wetBulbTem;
	}

	public String getRelativeTem() {
		return relativeTem;
	}

	public void setRelativeTem(String relativeTem) {
		this.relativeTem = relativeTem;
	}

	public String getDryBulbTem() {
		return dryBulbTem;
	}

	public void setDryBulbTem(String dryBulbTem) {
		this.dryBulbTem = dryBulbTem;
	}

	public String getParamType() {
		return paramType;
	}

	public void setParamType(String paramType) {
		this.paramType = paramType;
	}
	
	
	
}

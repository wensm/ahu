package com.carrier.ahu.vo;

import static com.carrier.ahu.constant.CommonConstant.SYS_FILES_DIR;

/**
 * 系统常量定义 Created by liaoyw on 2017/4/16.
 */
public interface SysConstants {

    String CHARSET_UTF8 = "UTF-8";


	/** win斜杠 */
	String SLASH_WIN = "\\";
	/** linux斜杠 */
	String SLASH_LINUX = "/";
	/** 资源目录位置 */
	String ASSERT_DIR = "asserts/";
	/** 焓湿图存放目录 */
	String PSYCHOMETRIC_DIR = "output/psychometric";
	/** CAD文件的存放目录 */
	String CAD_DIR = "output/cad";
	/** 给类模版文件的存放目录 */
	String TEMPLATE_DIR = "input/template";
	/** 给jar包类模版文件的存放目录 */
	String TEMPLATE_JAR_DIR = "template";
	/** 导出报告目录 **/
	String REPORT_DIR = "output/report/";
	/** 报告图片资源目录 **/
	String REPORT_IMG_DIR = "static/reportimg/";
	/** 报告图片资源目录 **/
	String TEST_CASE = "testcase/";
	/***/
	CharSequence FILES_DIR = SYS_FILES_DIR;
	String LOG_DIR = "logs/";
	/** ahu导出文件名,集成ahuID */
	String NAME_EXPORT_AHU = "ExportUnit.{0}.ahu";
	/** 项目导出文件名,集成项目ID */
	String NAME_EXPORT_PROJECT = "{0}_{1}_ExportProject.zip";
	/** 项目导出文件名,集成分组ID */
	String NAME_EXPORT_GROUP = "ExportGroup.{0}.xls";

    /**非标文件导出文件名*/
    String NS_EXPORT_FILENAME = "NSFileExportTemplate.{0}.xlsx";
    /**非标文件导出路径*/
    String NS_EXPORT_DIR = "asserts/output/export/nsfile/";

    /**非标文件导出路径*/
    String RECORD_STATUS_SELECTED = "selected";

	/** 项目导出文件名,集成分组ID */
	String DIR_EXPORT = "asserts/output/export/pro1/";
	   /** 项目导入文件路径 */
    String DIR_IMPORT = "asserts/output/import/pro1/";
	/** 三视图导出文件路径,集成机组ID */
	String DIR_EXPORT_THREE = "asserts/output/export/{0}/";
	/** 机组价格编码存放路径 */
	String PRICE_CODE_OUTPUT = "asserts/dll/price/%s/PartList.ini"; // fix file name PartList.ini
	String TEST_PRICE_CODE_OUTPUT = "asserts/dll/price/PartList_Test.ini"; // fix file name PartList.ini
	String FILE_AHU_PRICE_TXT = "asserts/dll/price/%s/temp.txt"; // the result file of price calculation
	String FILE_AHU_PRICE_PATH = "asserts/dll/price/";

	String PART_PREVIOUS = "previous";
	String PART_NEXT = "next";
	
	String EXCEL = "excel";
	String PDF = "pdf";
	String WORD = "word";
	
	String PDF_EXTENSION = ".pdf";
	String PRJ_EXTENSION = ".prj";
	String EXCEL2007_EXTENSION = ".xlsx";
	String WORD2007_EXTENSION = ".docx";
	String ZIP_EXTENSION = ".zip";
	String PNG_EXTENSION = ".png";
	String LOG_EXTENSION = ".log";
	String ERROR_FILE_PREFIX = "error_";

	String COLON = ": ";
	int UNIT_NO_LENGTH = 3;
	
	String SHUTDOWN_BAT = "shutdown.bat";
	String SHUTDOWN_PID = "shutdown.pid";

	String IMAGE_CAR_LOGO = "CAR_LOGO";
	String IMAGE_LANG_CH = "_ch";
	String IMAGE_LANG_EN = "_en";

	String PROJECT_DOMESTIC = "Domestic";
	String PROJECT_EXPORT = "Export";


}

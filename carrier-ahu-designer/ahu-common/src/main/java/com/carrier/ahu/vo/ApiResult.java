package com.carrier.ahu.vo;

import java.io.Serializable;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.constant.CommonConstant;

import lombok.Data;

/**
 * RESTFUL API返回值载体
 *
 * @param <T>
 */
@Data
public class ApiResult<T> implements Serializable {
    private static final long serialVersionUID = -4944438886435626007L;

    String code;
    String msg;
    T data;

    public ApiResult() {

    }

    public ApiResult(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <K> ApiResult<K> success(K data) {
        return success(CommonConstant.SYS_MSG_RESPONSE_SUCCESS, data);
    }

    public static <K> ApiResult<K> success(String msg, K data) {
        return new ApiResult<>(ErrorCodeEnum.SUCCESS.getCode(), msg, data);
    }

    public static <K> ApiResult<K> success() {
        return success(CommonConstant.SYS_MSG_RESPONSE_SUCCESS, null);
    }

    public static <K> ApiResult<K> error(String msg) {
        return new ApiResult<>(ErrorCodeEnum.ERROR.getCode(), msg, null);
    }

    public static <K> ApiResult<K> error(ErrorCodeEnum errorCode, String msg) {
        return new ApiResult<>(errorCode.getCode(), msg, null);
    }

    public static <K> ApiResult<K> error(Integer errorCode, String msg) {
        return new ApiResult<>(String.valueOf(errorCode), msg, null);
    }

    public static <K> ApiResult<K> error(String errorCode, String msg) {
        return new ApiResult<>(errorCode, msg, null);
    }

    public static <K> ApiResult<K> error(String errorCode, String msg, K data) {
        return new ApiResult<>(errorCode, msg, data);
    }

    /**
     * json converter
     *
     * @return
     */
    @Override
    public String toString() {
        return JSONObject.toJSONString(this, true);
    }

    public enum ErrorCodeEnum {
        SUCCESS("0"), ERROR("1");

        String code;

        ErrorCodeEnum(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }
}

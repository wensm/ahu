package com.carrier.ahu.po.batch;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class PartJob {
	String partid;// 段ID
	String sectionKey;
	Short types;
	String status;// 状态
	List<CalcJob> data;
}

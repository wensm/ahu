package com.carrier.ahu.common.model.partition;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by liujianfeng on 2017/9/1.
 */
@Data
public class PanelCalculationObj implements Serializable {

	private String category;//名称key
	private String partName;//名称
	private String partLM;//长度
	private String partWM;//宽度
	private String quantity;//数量
	private String memo="";//备注
	private String pro="";//0A 0B 00 ...
	private String instSection;//所在分段编号
	private String instPostion;//面编号

	//计算参数
	private String metaId;//段id

	//InstPostion:面编号定义
	//  /** 顶面 1*/
	public static String top = "1";
	//	/** 底面 2*/
	public static String bootom = "2";
	//	/** 右面 3*/
	public static String right = "3";
	//	/** 左面 4*/
	public static String left = "4";
	//	/** 端面 5*/
	public static String side = "5";
	//	/** 端面 6*/
}

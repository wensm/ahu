package com.carrier.ahu.po;

public class Parte {
    private String pid;

    private String partid;

    private Integer sectionl;

    private Double price;

    private Double weight;

    private String nonstandard;

    private Double sindrybulbt;

    private Double sinwetbulbt;

    private Double sinrelativet;

    private Double windrybulbt;

    private Double winwetbulbt;

    private Double winrelativet;

    private Double soutdrybulbt;

    private Double soutwetbulbt;

    private Double soutrelativet;

    private Double woutdrybulbt;

    private Double woutwetbulbt;

    private Double woutrelativet;

    private String memo;

    private String season;

    private Double runweight;

    private Double sheatq;

    private Double wheatq;

    private Double sinflowt;

    private Double winflowt;

    private Double swtascend;

    private Double wwtascend;

    private String rownum;

    private String loop;

    private String segmentp;

    private Double swaterresistance;

    private Double wwaterresistance;

    private Double swaterflow;

    private Double wwaterflow;

    private Double airresistance;

    private String aluminumfm;

    private String headerm;

    private String flange;

    private String bracketm;

    private Short fluididc;

    private Double concc;

    private Short fluididh;

    private Double conch;

    private String coilframe;

    private String watersave;

    private Short watersavenum;

    private Boolean frostp;

    private String dia;

    private Double sbrave;

    private Double wbrave;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public Integer getSectionl() {
        return sectionl;
    }

    public void setSectionl(Integer sectionl) {
        this.sectionl = sectionl;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getNonstandard() {
        return nonstandard;
    }

    public void setNonstandard(String nonstandard) {
        this.nonstandard = nonstandard == null ? null : nonstandard.trim();
    }

    public Double getSindrybulbt() {
        return sindrybulbt;
    }

    public void setSindrybulbt(Double sindrybulbt) {
        this.sindrybulbt = sindrybulbt;
    }

    public Double getSinwetbulbt() {
        return sinwetbulbt;
    }

    public void setSinwetbulbt(Double sinwetbulbt) {
        this.sinwetbulbt = sinwetbulbt;
    }

    public Double getSinrelativet() {
        return sinrelativet;
    }

    public void setSinrelativet(Double sinrelativet) {
        this.sinrelativet = sinrelativet;
    }

    public Double getWindrybulbt() {
        return windrybulbt;
    }

    public void setWindrybulbt(Double windrybulbt) {
        this.windrybulbt = windrybulbt;
    }

    public Double getWinwetbulbt() {
        return winwetbulbt;
    }

    public void setWinwetbulbt(Double winwetbulbt) {
        this.winwetbulbt = winwetbulbt;
    }

    public Double getWinrelativet() {
        return winrelativet;
    }

    public void setWinrelativet(Double winrelativet) {
        this.winrelativet = winrelativet;
    }

    public Double getSoutdrybulbt() {
        return soutdrybulbt;
    }

    public void setSoutdrybulbt(Double soutdrybulbt) {
        this.soutdrybulbt = soutdrybulbt;
    }

    public Double getSoutwetbulbt() {
        return soutwetbulbt;
    }

    public void setSoutwetbulbt(Double soutwetbulbt) {
        this.soutwetbulbt = soutwetbulbt;
    }

    public Double getSoutrelativet() {
        return soutrelativet;
    }

    public void setSoutrelativet(Double soutrelativet) {
        this.soutrelativet = soutrelativet;
    }

    public Double getWoutdrybulbt() {
        return woutdrybulbt;
    }

    public void setWoutdrybulbt(Double woutdrybulbt) {
        this.woutdrybulbt = woutdrybulbt;
    }

    public Double getWoutwetbulbt() {
        return woutwetbulbt;
    }

    public void setWoutwetbulbt(Double woutwetbulbt) {
        this.woutwetbulbt = woutwetbulbt;
    }

    public Double getWoutrelativet() {
        return woutrelativet;
    }

    public void setWoutrelativet(Double woutrelativet) {
        this.woutrelativet = woutrelativet;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season == null ? null : season.trim();
    }

    public Double getRunweight() {
        return runweight;
    }

    public void setRunweight(Double runweight) {
        this.runweight = runweight;
    }

    public Double getSheatq() {
        return sheatq;
    }

    public void setSheatq(Double sheatq) {
        this.sheatq = sheatq;
    }

    public Double getWheatq() {
        return wheatq;
    }

    public void setWheatq(Double wheatq) {
        this.wheatq = wheatq;
    }

    public Double getSinflowt() {
        return sinflowt;
    }

    public void setSinflowt(Double sinflowt) {
        this.sinflowt = sinflowt;
    }

    public Double getWinflowt() {
        return winflowt;
    }

    public void setWinflowt(Double winflowt) {
        this.winflowt = winflowt;
    }

    public Double getSwtascend() {
        return swtascend;
    }

    public void setSwtascend(Double swtascend) {
        this.swtascend = swtascend;
    }

    public Double getWwtascend() {
        return wwtascend;
    }

    public void setWwtascend(Double wwtascend) {
        this.wwtascend = wwtascend;
    }

    public String getRownum() {
        return rownum;
    }

    public void setRownum(String rownum) {
        this.rownum = rownum == null ? null : rownum.trim();
    }

    public String getLoop() {
        return loop;
    }

    public void setLoop(String loop) {
        this.loop = loop == null ? null : loop.trim();
    }

    public String getSegmentp() {
        return segmentp;
    }

    public void setSegmentp(String segmentp) {
        this.segmentp = segmentp == null ? null : segmentp.trim();
    }

    public Double getSwaterresistance() {
        return swaterresistance;
    }

    public void setSwaterresistance(Double swaterresistance) {
        this.swaterresistance = swaterresistance;
    }

    public Double getWwaterresistance() {
        return wwaterresistance;
    }

    public void setWwaterresistance(Double wwaterresistance) {
        this.wwaterresistance = wwaterresistance;
    }

    public Double getSwaterflow() {
        return swaterflow;
    }

    public void setSwaterflow(Double swaterflow) {
        this.swaterflow = swaterflow;
    }

    public Double getWwaterflow() {
        return wwaterflow;
    }

    public void setWwaterflow(Double wwaterflow) {
        this.wwaterflow = wwaterflow;
    }

    public Double getAirresistance() {
        return airresistance;
    }

    public void setAirresistance(Double airresistance) {
        this.airresistance = airresistance;
    }

    public String getAluminumfm() {
        return aluminumfm;
    }

    public void setAluminumfm(String aluminumfm) {
        this.aluminumfm = aluminumfm == null ? null : aluminumfm.trim();
    }

    public String getHeaderm() {
        return headerm;
    }

    public void setHeaderm(String headerm) {
        this.headerm = headerm == null ? null : headerm.trim();
    }

    public String getFlange() {
        return flange;
    }

    public void setFlange(String flange) {
        this.flange = flange == null ? null : flange.trim();
    }

    public String getBracketm() {
        return bracketm;
    }

    public void setBracketm(String bracketm) {
        this.bracketm = bracketm == null ? null : bracketm.trim();
    }

    public Short getFluididc() {
        return fluididc;
    }

    public void setFluididc(Short fluididc) {
        this.fluididc = fluididc;
    }

    public Double getConcc() {
        return concc;
    }

    public void setConcc(Double concc) {
        this.concc = concc;
    }

    public Short getFluididh() {
        return fluididh;
    }

    public void setFluididh(Short fluididh) {
        this.fluididh = fluididh;
    }

    public Double getConch() {
        return conch;
    }

    public void setConch(Double conch) {
        this.conch = conch;
    }

    public String getCoilframe() {
        return coilframe;
    }

    public void setCoilframe(String coilframe) {
        this.coilframe = coilframe == null ? null : coilframe.trim();
    }

    public String getWatersave() {
        return watersave;
    }

    public void setWatersave(String watersave) {
        this.watersave = watersave == null ? null : watersave.trim();
    }

    public Short getWatersavenum() {
        return watersavenum;
    }

    public void setWatersavenum(Short watersavenum) {
        this.watersavenum = watersavenum;
    }

    public Boolean getFrostp() {
        return frostp;
    }

    public void setFrostp(Boolean frostp) {
        this.frostp = frostp;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia == null ? null : dia.trim();
    }

    public Double getSbrave() {
        return sbrave;
    }

    public void setSbrave(Double sbrave) {
        this.sbrave = sbrave;
    }

    public Double getWbrave() {
        return wbrave;
    }

    public void setWbrave(Double wbrave) {
        this.wbrave = wbrave;
    }
}
package com.carrier.ahu.engine.fan;

import com.carrier.ahu.constant.CommonConstant;
import com.sun.jna.Library;
import com.sun.jna.Native;

/**
 * Created by liaoyw on 2017/3/30.
 */
public interface FanLib extends Library {
    
//    String GUKE_FAN_DLL = "GukeFan";//谷科风机

    FanLib INSTANCE = (FanLib) Native.loadLibrary(CommonConstant.SYS_PATH_FAN_DLL, FanLib.class);
//    FanLib GUKE_FAN_INSTANCE = (FanLib) Native.loadLibrary(GUKE_FAN_DLL, FanLib.class);


    /**
     * 风机选型
     *
     * @param pFlow     风量（m^3/h）
     * @param pPres     风压(Pa)
     * @param pPresType 风压类型 0-全压 1-静压
     * @return 选型结果个数
     */
    int FanSelection(double pFlow, double pPres, int pPresType);

    /**
     * 风机选型(详细版)
     *
     * @param pSeriesNames          系列名称（为空时不限制），用分号相隔  如 SYQ  或 SYD;SYQ
     * @param pSubTypeNames         子系列名称（为空时不限制），用分号相隔  如 K 或 K;K2
     * @param pFlow                 风量(m3/h)
     * @param pPres                 风压（Pa）
     * @param pPresType             风压类型 0-全压 1-静压
     * @param pAirDesityType        空气密度类型 0-自定义  1-直接输入空气密度
     * @param pAirDensity           空气密度
     * @param pB                    大气压力
     * @param pT                    空气温度
     * @param pV                    相对湿度
     * @param pMotorSafeCoff        电机容量安全系数(%)
     * @param pUserSetMotorSafeCoff 用户设定了电机容量安全系数  true-是 false-否
     * @return 选型结果个数
     */
    int FanSelectionEx2(String pSeriesNames, String pSubTypeNames, double pFlow, double pPres, int pPresType,
                        int pAirDesityType, double pAirDensity, double pB, double pT, double pV,
                        double pMotorSafeCoff, boolean pUserSetMotorSafeCoff);

    /**
     * 同FanSelectionEx3， 多了出风方式
     *
     * @param pOutFanType 出风方式 0-管道出风 1-自由出风
     * @return
     */
    int FanSelectionEx3(String pSeriesNames, String pSubTypeNames, double pFlow, double pPres, int pPresType,
                        int pOutFanType, int pAirDesityType, double pAirDensity, double pB, double pT, double pV,
                        double pMotorSafeCoff, boolean pUserSetMotorSafeCoff);

    /**
     * 获得选型结果(上次选型的结果，适用于风机选型和配件选型)
     *
     * @param pIndex      记录索引(以0为起始)
     * @param ptStrData   用于返回字符串的字符串数组
     * @param pDoubleData 用于返回数值型的数组
     */
    void GetLargeResultData(int pIndex, byte[] ptStrData, double[] pDoubleData);

    boolean GetOtherConditionData(
            String pSeriesName,//系列名称(第一次选型出来的结果)
            String pSubSeriesName,//子系列名称(第一次选型出来的结果)
            String pModelName,//型号名称(第一次选型出来的结果)
            int pOutFanType,//出风方式 0-管道出风 1-自由出风
            int pDirection,//出风角度 0-0度 1-90度 2-180度 3-270度
            double pAirDensity,//空气密度
            double pMotorSafeCoff,//电机容量安全系数(%)
            boolean pUserSetMotorSafeCoff,//用户设定了电机容量安全系数
            int pSelType,//选型类型
            int pInWindType,//进风类型(1-单进风，2-双进风 -1表示不区别)
            double pExPres,//额外阻力（风机箱阻力）
            double pUserFlow,//用户风量
            double[] pFirstResult, //第一次选型出来的数值型数据, 500
            double pConditionFlow,//指定工况风量
            double[] pConditionResult//定工况的数据 500
    );

    /**
     * 导出曲线图到指定文件（为bmp格式）
     *
     * @param pFlow       风量（m^3/h）
     * @param pPres       风压(Pa)
     * @param pStrData    返回字符串的字符串数组
     * @param pDoubleData 返回数值型的数组
     * @param pWidth      图形宽度
     * @param pHeight     图形高度
     * @param pFileName   保存的图形文件名称
     */
    void ExportPicture(double pFlow, double pPres, byte[] pStrData, double[] pDoubleData,
                       int pWidth, int pHeight, String pFileName);

    /**
     * @param pMultiCurve 是否是多转速曲线（true-多转速 false-单转速）
     * @see ExportPicture
     */
    void ExportPictureEx(double pFlow, double pPres, byte[] pStrData, double[] pDoubleData,
                         int pWidth, int pHeight, String pFileName, boolean pMultiCurve);

    /**
     * 配件选型
     *
     * @param ReDataStr          风机选型的字符串型结果
     * @param ReDataDouble       风机选型的数值型结果
     * @param pHz_DataType       频率选择 0-50HZ 1-60HZ(界面选择)
     * @param pRotation          出风旋转角度 0-R0,1-R90,2-R180,3-R270
     * @param pPole              用户选择的极数 2,4,6,8
     * @param pBeltType          皮带类型 SPZ SPA SPB SPC
     * @param pBeSetFanSped      用户设定了风机转速
     * @param pFanSpedRt_User    用户设定的风机转速差
     * @param pBeSetBeltMoveSped 用户设定最大皮带运动速度
     * @param pBeltMoveSped_User 用户设定的最大皮带运动速度
     * @param pCenterDistance    中心距 (mm)
     * @return
     */
    int BeltSelection(byte[] ReDataStr, double[] ReDataDouble, int pHz_DataType, int pRotation,
                      boolean[] pPole, boolean[] pBeltType, boolean pBeSetFanSped, double pFanSpedRt_User,
                      boolean pBeSetBeltMoveSped, double pBeltMoveSped_User, double pCenterDistance);

    /**
     * SYW配件选型（针对SYW增加的配件选型，皮带驱动）
     *
     * @param pMotorBrand 电机品牌
     * @return
     * @see FanLib.BeltSelection
     */
    int SYWBeltSelection(byte[] ReDataStr, double[] ReDataDouble, String pMotorBrand, int pHz_DataType,
                         int pRotation, boolean[] pPole, boolean[] pBeltType, boolean pBeSetFanSped,
                         double pFanSpedRt_User, boolean pBeSetBeltMoveSped, double pBeltMoveSped_User,
                         double pCenterDistance);

    /**
     * SYW电机选型(直联)
     *
     * @param pMotorStyle 电机形式 1-普通电机直驱、2-变频电机直驱
     * @param pMotorBrand 电机品牌
     * @param pFanSped    风机转速
     * @param pPower      推荐功率（轴功率*安全系数）
     * @param pSTDPower   标准电机功率
     * @return
     */
    int SYWMotorSelection(int pMotorStyle, String pMotorBrand, double pFanSped, double pPower, double pSTDPower);

    /**
     * 根据海拔和大气密度计算空气密度
     *
     * @param pH    海拔高度
     * @param pTemp 大气温
     * @return 空气密度 （单位:kg/m3）
     */
    double CalAirDensity2(double pH, double pTemp);


}

package com.carrier.ahu.resistance.param;

import com.carrier.ahu.common.enums.AirDirectionEnum;

import lombok.Data;
import lombok.ToString;

/**
 * Created by liangd4 on 2017/10/16.
 */
@Data
@ToString
public class ResistanceParam {

    /*所有段必传参数*/
    private String sectionType;//sectionType A:混合段 B:过滤段 C:综合过滤段 D:冷水盘管段 E:热水盘管段 F:蒸汽盘管段 G:电加热盘管段 H:干蒸汽加湿段 I:湿膜加湿段 J:高压喷雾加湿段 K:风机段 L:新回排风段 M:消音段 N:出风段 O:空段 V:高效过滤段 W:热回收段 X:电极加湿 Y:静电过滤器 Z:控制段
    private String version;//版本 CN-HTC-1.0 国家二字码-工厂简称-版本号
    private String serial;//机组型号
    private int sairvolume;//送风风量
    private int appendAirVolume; //调整风量
    private int eairvolume;//回风风量
    private String airDirection = AirDirectionEnum.SUPPLYAIR.getCode();//风向

    /*以下段需要传入段长：混合段 单层过滤 综合过滤 空段*/
    private int sectionL;//段长

    /*混合段*/
    private String damperOutlet;//风阀材料 flange:法兰

    /*出风段*/
    private String AInterface;//风阀材料 flange:法兰

    /*单层过滤器*/ /*高效过滤器 fitetF fitlerStandard filterEfficiency*/ /*静电过滤器 使用所有段必传参数*/
    private String fitetF;//过滤形式 out:外抽、panel:板式、bag:袋式 高效过滤器过滤形式：P:箱型 V:V型
    private String rimThickness;//边框厚度 50 25
    private String mediaLoading;//frontLoading:正面抽、sideLoading:侧面抽 other:其它
    private String fitlerStandard;//过滤标准 任意选项:Any GB/T13554:GB EN1822:EN
    private String filterEfficiency;//过滤器效率"H11", "H13", "A", "B"
    private String filterOptions;//过滤器选项

    /*综合过滤器*/
    private String FilterF;//过滤器形式
    private String RimThick;//边框厚度
    private String MediaLoading;//过滤器抽取方式 frontLoading:正面抽、sideLoading:侧面抽 other:其它
    private String FilterStandard;//过滤标准 任意选项:Any GB/T13554:GB EN1822:EN
    private String RMaterialE;//过滤效率H11", "H13", "A", "B"
    private String LMaterialE;//过滤器效率
    private String LMaterial;//板式过滤器选项
    private String RMaterial;//袋式过滤器选项
    
    //高效过滤段
    private String Supplier;//供应商 无过滤器

    /*空段*/
    private String Function;//是否为均流段

    /*湿膜加湿段*/
    private String supplier;//供应商
    private String season;//季节
    private double SHumidificationQ;//夏季加湿量
    private double WHumidificationQ;//冬季加湿量
    private double SInDryBulbT;//夏季干球温度
    private double SInWetBulbT;//夏季湿球温度
    private double WInDryBulbT;//冬季干球温度
    private double WInWetBulbT;//冬季湿球温度
    private boolean EnableWinter;//冬季是否生效
    private String wetfilmMaterial;//湿膜材质

    /*盘管段*/
    private double SAirResistance;
    private double WAirResistance;
    private double driftEliminatorResistance;
    private String eliminator;
    private boolean EnableSummer;
    
    /*热水盘管段*/


    /*消音段*/
    private String Deadening;//消⾳级数
}

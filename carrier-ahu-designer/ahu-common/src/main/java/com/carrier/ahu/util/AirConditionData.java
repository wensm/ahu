package com.carrier.ahu.util;

/**
 * Set S_ID data
 * 
 * @author
 *
 */
public class AirConditionData {



	final static int MINTEMPERATURE = -20;
	final static int MAXTEMPERATURE = 95;

	final static int IETERATIONNUM = 100;
	final static double PRECISIOND = 0.01; // |d-dx|<0.01
	final static double PRECISIONI = 0.01; // |i-ix|<0.01
	final static double MINSTEPSIZE = 0.0001;
	final static double MAXSTEPSIZE = 20;
	final static double PRECSIONTMP = 0.0001;
	final static double CALCULATION_FAIL = 404;

}

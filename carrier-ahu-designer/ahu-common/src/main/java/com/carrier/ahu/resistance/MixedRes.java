package com.carrier.ahu.resistance;

/**
 * Created by liangd4 on 2017/9/27.
 * 混合段
 */
public class MixedRes {

    public double getResistance() {
        //•	风阀数据为[混合段尺寸数据]表中，按照型号和段长取得。A列，B列

        /*a.	如果 [表段长]>[实际段长]
        阻力=[空段阻力（表段长）] +（[表段长]-[实际段长]）
        b.	反之，则
        阻力=[空段阻力（实际段长）] + 阀门阻力
        阀门材料与阻力关系：法兰 / 0 Pa；其他 / 10 Pa*/

        return 0.00;
    }


}

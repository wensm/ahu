package com.carrier.ahu.length.param;

import com.carrier.ahu.util.PublicParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2017/12/12.
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class HeatCoilQParam extends PublicParam {

    private String season;//季节
    private double InDryBulbT;//进风干球温度
    private double InWetBulbT;//进风湿球温度
    private double InRelativeT;//进风相对湿度
    private double airVolume;//风量
    private String sectionKey;//段Key
    private double HeatQ;//热量
    private String serial;//机组型号

}

package com.carrier.ahu.resistance.service;

/**
 * Created by LIANGD4 on 2017/9/18.
 */
public interface ResistanceService {

    //TODO 参数定义
    //sectionType 段
    //version：版本 CN-HTC-1.0 国家二字码-工厂简称-版本号
    double getResistance(String sectionType, String version);

}

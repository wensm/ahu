package com.carrier.ahu.engine.dxcoil;

import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.IntByReference;


/**
 * Created by liaoyw on 2017/3/27.
 */
public class DXCoilResult {

    DoubleByReference capTotal = new DoubleByReference();
    DoubleByReference capSensible = new DoubleByReference();
    DoubleByReference dpAir = new DoubleByReference();
    DoubleByReference dpRef = new DoubleByReference();
    DoubleByReference tdbAirOut = new DoubleByReference();
    DoubleByReference twbAirOut = new DoubleByReference();
    DoubleByReference wmrRef = new DoubleByReference();
    IntByReference errorFlag = new IntByReference();

    public double getCapTotal() {
        return capTotal.getValue();
    }

    public double getCapSensible() {
        return capSensible.getValue();
    }

    public double getDpAir() {
        return dpAir.getValue();
    }

    public double getDpRef() {
        return dpRef.getValue();
    }

    public double getTdbAirOut() {
        return tdbAirOut.getValue();
    }

    public double getTwbAirOut() {
        return twbAirOut.getValue();
    }

    public double getWmrRef() {
        return wmrRef.getValue();
    }

    public int getErrorFlag() {
        return errorFlag.getValue();
    }
}

package com.carrier.ahu.common.enums;

/**
 * 批量计算状态</br>
 * 正在计算（计算开始时） </br>
 * 计算成功（所有计算均正常结束） </br>
 * 计算结束有异常（有非正常结果的计算） </br>
 * 计算失败（所有计算结果均异常） </br>
 */
public enum CalcStatusEnum {
	CALCULATING("calculating", "正在计算", "Calculating"),

	SUCCESS("success", "计算结束", "Calculated Successfully"),

	FAILED("failed", "计算失败", "Calculated Failed"),

	ERROR("error", "计算有异常", "Calculated With Error");

	private String id;// id
	private String cnName;// 中文名称
	private String name;// 英文名称

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCnName() {
		return this.cnName;
	}

	CalcStatusEnum(String id, String cnName, String name) {
		this.id = id;
		this.cnName = cnName;
		this.name = name;
	}
}

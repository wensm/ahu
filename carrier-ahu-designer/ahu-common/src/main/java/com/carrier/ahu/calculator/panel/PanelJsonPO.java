package com.carrier.ahu.calculator.panel;

import com.carrier.ahu.common.model.partition.PanelFace;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PanelJsonPO {
	private String sectionTypeId;
	private String sectionTypeNo;
	private int sectionL;
	private PanelFace panelFace;

	public PanelJsonPO() {
	}

	public PanelJsonPO(String sectionTypeId, String sectionTypeNo, int sectionL, PanelFace panelFace) {
		this.sectionTypeId = sectionTypeId;
		this.sectionTypeNo = sectionTypeNo;
		this.sectionL = sectionL;
		this.panelFace = panelFace;
	}
}

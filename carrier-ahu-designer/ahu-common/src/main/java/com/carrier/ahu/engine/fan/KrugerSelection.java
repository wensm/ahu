package com.carrier.ahu.engine.fan;

import lombok.Data;

/**
 * Kruger Select Rest API response object.
 * 
 * Created by Braden Zhou on 2019/02/20.
 */
@Data
public class KrugerSelection {

    private boolean Debug;
    private float AirVelocity;
    private float AMCAPressure;
    private float AMCAVolume;
    private int BearingLoad;
    private int BladeType;
    private int DataPoint;
    private float Density;
    private int DimA;
    private int DimB;
    private int DimC;
    private int DimD;
    private int DimE;
    private int DimF;
    private int DimF1;
    private int DimF2;
    private int DimG;
    private int DimH;
    private int DimH1;
    private int DimH2;
    private int DimH3;
    private int DimI;
    private int DimJ;
    private int DimK;
    private int DimL;
    private int DimM;
    private int DimN;
    private int DimN1;
    private int DimN2;
    private String DimNxS;
    private int DimO;
    private int DimOD;
    private int DimP;
    private int DimPhi;
    private int DimQ;
    private int DimR;
    private int DimS;
    private int DimSetCentres;
    private int DimSetHeight;
    private int DimSetLength;
    private int DimSetWidth;
    private int DimT;
    private int DimT1m;
    private int DimTm;
    private String DimUxS;
    private int DimV;
    private int DimV1;
    private int DimV2;
    private int DimW;
    private int DimX;
    private int DimY;
    private int DimZ;
    private String Drawing;
    private float DrivenPower;
    private float FanArea;
    private int FanBlades;
    private int FanClass;
    private String FanCode;
    private String FanDescription;
    private int FanIndex;
    private String FanKey;
    private float FanMotorFLC;
    private String FanMotorFrame;
    private int FanMotorHz;
    private int FanMotorPoles;
    private float FanMotorRating;
    private int FanMotorSize;
    private float FanMotorSpeed;
    private float FanMotorSTC;
    private int FanMotorWeight;
    private int FanShaft;
    private float FanSize;
    private float FanSpeed;
    private int FanStyle;
    private float FanWeight;
    private int FanWidth;
    private float ImpellerDiameter;
    private float InletDuctDiameter;
    private float Inlet_LPA_Overall;
    private float Inlet_LWA_Overall;
    private float Inlet_LwLin_Overall;
    private int JFan;
    private float MaxPower;
    private int MaxSpeed;
    private float Outlet_LPA_Overall;
    private float Outlet_LWA_Overall;
    private float Outlet_LwLin_Overall;
    private float PressureFactor;
    private int Product;
    private float PwrCondition;
    private float PwrStandard;
    private int Speed;
    private String SelectInfo;
    private float StaticEff;
    private float StaticPressure;
    private String TempRange;
    private float TipSpeed;
    private float TotalEff;
    private float TotalPressure;
    private float VelPressure;
    private String chartFilePath;
    private float[] outletLwaSpectrum;
    private float[] inletLwLinSpectrum;
    private float[] outletLwLinSpectrum;
}

package com.carrier.ahu.common.util;

import static com.carrier.ahu.constant.CommonConstant.SYS_DOT_SEPARATOR;
import static com.carrier.ahu.constant.CommonConstant.SYS_STRING_NUMBER_0;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

import com.carrier.ahu.util.EmptyUtil;
import com.google.common.collect.Lists;

/**
 * Created by Braden Zhou on 2019/01/03.
 */
public class VersionUtils {

    /**
     * Compare two versions, as format 1.0.0.
     * 
     * @param newVersion
     * @param oldVersion
     * @return
     */
    public static int compare(String newVersion, String oldVersion) {
        if (!EmptyUtil.isEmpty(newVersion) && !EmptyUtil.isEmpty(oldVersion)) {
            String[] newVersionChunks = newVersion.split(SYS_DOT_SEPARATOR);
            String[] oldVersionChunks = oldVersion.split(SYS_DOT_SEPARATOR);
            int size = newVersionChunks.length > oldVersionChunks.length ? newVersionChunks.length
                    : oldVersionChunks.length;

            for (int i = 0; i < size; i++) {
                String newVersionChunk = i < newVersionChunks.length ? newVersionChunks[i] : SYS_STRING_NUMBER_0;
                String oldVersionChunk = i < oldVersionChunks.length ? oldVersionChunks[i] : SYS_STRING_NUMBER_0;
                if (!newVersionChunk.equals(oldVersionChunk)) {
                    if (NumberUtils.toInt(newVersionChunk) > NumberUtils.toInt(oldVersionChunk)) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            }
            return 0;
        }
        return -1;
    }

    public static List<VersionPair> parseVersionPairs(String version) {
        List<VersionPair> versionPairs = Lists.newArrayList();
        String[] pairs = version.split(",");
        Arrays.stream(pairs).forEach(p -> versionPairs.add(parseVersionPair(p)));
        return versionPairs;
    }

    private static VersionPair parseVersionPair(String version) {
        String[] pairs = version.split("-");

        if (pairs.length == 1) {
            return new VersionPair(pairs[0], pairs[0]);
        } else {
            return new VersionPair(pairs[0], pairs[1]);
        }
    }

    public static boolean withinVersionPairs(String version, List<VersionPair> versionPairs) {
        for (VersionPair versionPair : versionPairs) {
            if (withinVersionPair(version, versionPair)) {
                return true;
            }
        }
        return false;
    }

    public static boolean withinVersionPair(String version, VersionPair versionPair) {
        return compare(version, versionPair.startVersion) >= 0 && compare(version, versionPair.endVersion) <= 0;
    }

    public static class VersionPair {
        private String startVersion;
        private String endVersion;

        VersionPair(String startVersion, String endVersion) {
            this.startVersion = startVersion;
            this.endVersion = endVersion;
        }

        public String getStartVersion() {
            return startVersion;
        }

        public String getEndVersion() {
            return endVersion;
        }

    }

}

package com.carrier.ahu.po.model;

import java.io.Serializable;

/**
 * @author JL
 * Abstract class to describe a AHU Model
 *
 */
public abstract class AbstractModel implements Serializable{

    private static final long serialVersionUID = 8648586235074633880L;
}

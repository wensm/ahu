package com.carrier.ahu.common.util;

public class PreferenceStore {
	
	public static PreferenceStore istance = new PreferenceStore();
	public static final int UNIT_STYLE_METRIC = 0;
	public static final int UNIT_STYLE_BRITISH = 1;
	
	private int calUnitSytle = UNIT_STYLE_METRIC;
	
	
	private PreferenceStore() {
		
	}

	public int getCurrentUnitStyle() {
		return this.calUnitSytle;
	}

	public int getCalUnitSytle() {
		return calUnitSytle;
	}

	public void setCalUnitSytle(int calUnitSytle) {
		this.calUnitSytle = calUnitSytle;
	}

}

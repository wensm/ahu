package com.carrier.ahu.common.entity;

import com.carrier.ahu.po.AbstractPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * 登录退出日志 Created by Wen zhengtao on 2017/6/20.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class LoginoutLog extends AbstractPo implements Serializable {

	private static final long serialVersionUID = -3303689439011378676L;
	@Id
	@GeneratedValue
	String logId;// 日志ID
	String userName;// 用户名
	String userIp;// ip地址
	String userAgent;// 浏览器代理
	String logType;// 日志类型
	String errMsg;// 错误描述
	String requestUrl;// 请求url
	String requestMethod;// 请求方式
	String cookies;// 浏览器cookie
}

package com.carrier.ahu.common.exception;

/**
 * Created by LIANGD4 on 2017/12/12.
 */
public class TempCalErrorException extends Exception {
	private static final long serialVersionUID = -7941760891978133157L;

	public TempCalErrorException(String message) {
		super(message);
	}

	public TempCalErrorException(String message, Exception e) {
		super(message, e);
	}
}

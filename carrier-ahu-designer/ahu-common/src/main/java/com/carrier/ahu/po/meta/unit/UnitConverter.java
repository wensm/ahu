package com.carrier.ahu.po.meta.unit;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.constant.CommonConstant;

/**
 * This class handle the unit converting. It is key feature of AHU designer,
 * comparing with the current version.
 * 
 * @author liujianfeng
 *
 */
public class UnitConverter {

	public final static String UNIT = CommonConstant.SYS_UNIT;

	public static String genUnitKey(Unit unit, Part part) {
		String partID = String.valueOf(part.getPosition()) + CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN
				+ unit.getUnitid() + CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN + part.getSectionKey();
		return partID;
	}

	public static String genUnitKey(String position, String unitid, String sectionKey) {
		String partID = position + CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN + unitid
				+ CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN + sectionKey;
		return partID;
	}

}

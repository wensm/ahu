package com.carrier.ahu.engine.price;

import com.sun.jna.win32.StdCallLibrary;

/**
 * AHU Price CAL engine
 * 
 * Created by Braden Zhou on 2018/08/31.
 */
public interface AHUPriceCal extends StdCallLibrary {

    void PriceCAL(int S_language);

}

package com.carrier.ahu.po.meta.layout;

import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.po.meta.MetaParameter;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@Data
public class Group implements Serializable {
	/**
	 * Describe how many columns in one line
	 */
	private int columns = 1;
	private String name = CommonConstant.SYS_BLANK;
	private String cname = CommonConstant.SYS_BLANK;
	
	private List<String> keys = new ArrayList<>();
	private List<MetaParameter> paras = new ArrayList<>();
	
	public Group(String name, String cname) {
		this.name = name;
		this.cname = cname;
	}
	
	
}

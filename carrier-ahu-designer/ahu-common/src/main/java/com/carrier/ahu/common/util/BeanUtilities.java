package com.carrier.ahu.common.util;

import org.springframework.beans.BeanUtils;

import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.common.exception.ErrorCode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BeanUtilities {

    @SuppressWarnings("unchecked")
    public static <T> T clone(T source) {
        try {
            T t = (T) source.getClass().newInstance();
            BeanUtils.copyProperties(source, t);
            return t;
        } catch (Exception e) {
            log.error("error happened", e);
            throw new AhuException(ErrorCode.INTERNAL_SERVER_ERROR, e);
        }
    }

}

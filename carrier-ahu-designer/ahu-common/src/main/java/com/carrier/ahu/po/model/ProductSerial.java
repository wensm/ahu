package com.carrier.ahu.po.model;

import java.util.List;

/**
 * Serial is defined to handle different AHU/Section, belongs to one/more
 * factory
 * 
 * @author JL
 *
 */
public class ProductSerial extends AbstractModel {
	private static final long serialVersionUID = -3303689439011378676L;

	private String serial;
	private String name;
	private List<String> factories;

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getFactories() {
		return factories;
	}

	public void setFactories(List<String> factories) {
		this.factories = factories;
	}
}

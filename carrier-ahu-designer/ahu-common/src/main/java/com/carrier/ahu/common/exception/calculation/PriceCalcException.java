package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/04/17.
 */
@SuppressWarnings("serial")
public class PriceCalcException extends CalculationException {

    public PriceCalcException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public PriceCalcException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public PriceCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public PriceCalcException(String message) {
        super(message);
    }

}

package com.carrier.ahu.length.param;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/9.
 */
@Data
public class FilterPanelBean {

    private String Option;//规格
    private String Count;//数量

    private String LOption;//规格（袋式）
    private String LCount;//数量（袋式）
    private String POption;//规格（板式）
    private String PCount;//数量（板式）

}

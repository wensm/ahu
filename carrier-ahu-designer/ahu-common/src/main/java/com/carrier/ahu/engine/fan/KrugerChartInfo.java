package com.carrier.ahu.engine.fan;

import org.apache.commons.beanutils.BeanUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Kruger Generate Chart Parameter.
 * 
 * Created by Braden Zhou on 2019/02/25.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class KrugerChartInfo extends KrugerSelection {

    private int width;
    private int height;
    private String chartPath;
    private String chartName;
    private int chartType;

    public KrugerChartInfo() {
    }

    public KrugerChartInfo(KrugerSelection krugerSelection) {
        try {
            BeanUtils.copyProperties(this, krugerSelection);
        } catch (Exception exp) {
            //
        }
    }

}

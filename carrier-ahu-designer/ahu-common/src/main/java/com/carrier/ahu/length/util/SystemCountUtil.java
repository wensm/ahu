package com.carrier.ahu.length.util;

import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.util.NumberUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by liangd4 on 2017/9/27.
 */
public class SystemCountUtil {
    //特殊型号：2435，2635，2735三个型号
    public static final List<Integer> UNIT_SPECIAL = new ArrayList<Integer>(Arrays.asList(2435,2635,2735));
    //特殊型号高度：24/26/27
    public static final List<Integer> UNIT_SPECIAL_HeightS = new ArrayList<Integer>(Arrays.asList(24,26,27));


    //机组高度
    public static int getUnitHeight(String unitModel) {
        return Integer.valueOf(unitModel.trim().substring(unitModel.trim().length() - 4, unitModel.trim().length() - 2));//机组高度;

    }

    //机组宽度
    public static int getUnitWidth(String unitModel) {
        return Integer.valueOf(unitModel.trim().substring(unitModel.trim().length() - 2, unitModel.trim().length()));//机组宽度;
    }

    //机组型号
    public static String getUnit(String unitModel) {
        return unitModel.trim().substring(unitModel.trim().length() - 4, unitModel.trim().length());//机组型号;
    }

    //机组中的G替换成CQ
    public static String getGReplaceCQ(String unitModel) {
       return unitModel.replace(CommonConstant.SYS_UNIT_SERIES_G,CommonConstant.SYS_UNIT_SERIES_CQ);
    }


    //a/b c：保留几位小数
    public static double getDivide(double a, double b, int c) {
        return new BigDecimal((float) a / b).setScale(c, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static double velfacecal(double airVolume, double rotorDia, String volumeUnit) {
        switch (volumeUnit) {
            case CommonConstant.SYS_STRING_NUMBER_1:
                return airVolume / 3600 / ((Math.PI * (rotorDia / 2 / 1000) * (rotorDia / 2 / 1000)) / 2.1);
            case CommonConstant.SYS_STRING_NUMBER_2:
                return airVolume / ((Math.PI * (rotorDia / 2 / 1000) * (rotorDia / 2 / 1000)) / 2.1);
        }
        return 0;
    }
    
    public static double plateVelFaceCal(double airVolume, double width,double height, String volumeUnit) {
        switch (volumeUnit) {
            case CommonConstant.SYS_STRING_NUMBER_1:
                return airVolume / 3600 / (height/1000*width/1000);
            case CommonConstant.SYS_STRING_NUMBER_2:
                return airVolume / height/1000*width/1000;
        }
        return 0;
    }

    public static void main(String[] args) {
        System.out.println(getUnitHeight("39CQ0608"));
        System.out.println(getUnitWidth("39CQ0608"));
    }

    public static boolean isEuroUnit(Unit unit) {

        //39G 也拿到欧标认证。
    	/*if("39G".equalsIgnoreCase(unit.getProduct())) {
    		return false;
    	}*/
    	
    	return true;
    }
    public static boolean isAeroUnit(Unit unit) {

    	if("39CQ".equalsIgnoreCase(unit.getProduct())) {
    		return true;
    	}

    	return false;
    }
    /**
     * 取得精简序列号<br>
     *
     * 例子：39CQ1215 返回1215<br>
     * 例子：39G1215 返回1215<br>
     * 例子：G1215 返回1215<br>
     *
     * @param unitModel
     * @return
     */
    public static int getUnitNoInt(String unitModel) {
        String unitNo = unitModel;
        if (unitNo.length() > 4) {
            unitNo = unitNo.substring(unitNo.length() - 4);
        }
        int uno = NumberUtil.convertStringToDoubleInt(unitNo);
        return uno;
    }
    /*CQ 系列特殊型号：2435，2635，2735三个型号，包含在小于等于2333的逻辑的校验方法*/

    //高度<=23:小机组
    public static boolean lteSmallUnitHeight(int unitHeight) {
        return unitHeight <= 23 || UNIT_SPECIAL_HeightS.contains(unitHeight);
    }
    //高度<25：小机组
    public static boolean ltBigUnitHeight(int unitHeight) {
        return unitHeight < 25 || UNIT_SPECIAL_HeightS.contains(unitHeight);
    }

    //高度>=25：大机组
    public static boolean gteBigUnitHeight(int unitHeight) {
        return unitHeight >= 25 && !UNIT_SPECIAL_HeightS.contains(unitHeight);
    }
    //高度>23：大机组
    public static boolean gtSmallUnitHeight(int unitHeight) {
        return unitHeight > 23 && !UNIT_SPECIAL_HeightS.contains(unitHeight);
    }

    //机组型号<=2333:小机组
    public static boolean lteSmallUnit(int unitModel) {
        return unitModel <= 2333 || UNIT_SPECIAL.contains(unitModel);
    }
    //机组型号<2532：小机组
    public static boolean ltBigUnit(int unitModel) {
        return unitModel < 2532 || UNIT_SPECIAL.contains(unitModel);
    }

    //机组型号>=2532：大机组
    public static boolean gteBigUnit(int unitModel) {
        return unitModel >= 2532 && !UNIT_SPECIAL.contains(unitModel);
    }
    //机组型号>2333：大机组
    public static boolean gtSmallUnit(int unitModel) {
        return unitModel > 2333 && !UNIT_SPECIAL.contains(unitModel);
    }

    //高度<=23:小机组:尺寸方面
    public static boolean isSmallUnitHeightByRealSize(int unitHeight) {
        return unitHeight <= 23;
    }
    //高度>=25：大机组:尺寸方面
    public static boolean isBigUnitHeightByRealSize(int unitHeight) {
        return unitHeight >= 25;
    }
}

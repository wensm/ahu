package com.carrier.ahu.resistance.count;

import com.carrier.ahu.resistance.param.ResistanceBCVYBean;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by LIANGD4 on 2017/11/17.
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class WetFilmResultInfo extends ResistanceBCVYBean {
    private double resistance;//阻力
    private double thickness;//厚度
}

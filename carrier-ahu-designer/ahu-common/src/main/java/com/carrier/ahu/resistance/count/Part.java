package com.carrier.ahu.resistance.count;

/**
 * Created by liangd4 on 2017/9/14.
 */
public class Part {
    private String partId;

    public String getPartId() {
        return partId;
    }

    public void setPartId(String partId) {
        this.partId = partId;
    }
}

package com.carrier.ahu.common.configuration;

import java.io.InputStream;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.carrier.ahu.common.entity.User;
import com.carrier.ahu.common.entity.UserMeta;
import com.carrier.ahu.common.enums.FactoryEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.enums.UserMetaEnum;
import com.carrier.ahu.common.enums.VersionTypeEnum;
import com.carrier.ahu.common.exception.AhuException;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.license.LicenseManager;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.vo.SysConstants;

/**
 * This class provides global access to variables through out the application.
 * And it should be updated if it is used in multiple users server application.
 * 
 * @author Braden Zhou
 *
 */
public final class AHUContext {

    private static class Context {
        private static Context INSTANCE = new Context();

        private User user;
        private FactoryEnum factory;
        private VersionTypeEnum versionType;
        private String ahuVersion;
        private String ahuPatchVersion;
        private Map<String, UserMeta> userMetaMap = new HashMap<String, UserMeta>();

        private Context() {
            this.setFactory(LicenseManager.getFactory());
        }

        private Context(User user, FactoryEnum factory) {
            this.user = user;
            this.factory = factory;
        }

        private void setFactory(String factory) {
            try {
                FactoryEnum factoryEnum = FactoryEnum.valueOf(factory);
                if (factory != null) {
                    this.factory = factoryEnum;
                }
            } catch (Exception exp) {
                // ignore
            }
            if (this.factory == null) { // set default factory
                this.factory = FactoryEnum.THC;
            }
        }

        private void setUserMetas(List<UserMeta> userMetaList) {
            this.userMetaMap.clear();
            for (UserMeta userMeta : userMetaList) {
                this.userMetaMap.put(userMeta.getUserMetaId().getMetaKey(), userMeta);
            }
        }

        public UserMeta getUserMeta(UserMetaEnum userMetaEnum) {
            return this.userMetaMap.get(userMetaEnum.name());
        }

        public String getAhuVersion() {
            return ahuVersion;
        }

        public void setAhuVersion(String ahuVersion) {
            this.ahuVersion = ahuVersion;
        }

        public String getAhuPatchVersion() {
            return ahuPatchVersion;
        }

        public void setAhuPatchVersion(String ahuPatchVersion) {
            this.ahuPatchVersion = ahuPatchVersion;
        }

        public VersionTypeEnum getVersionType() {
            return versionType;
        }

        public void setVersionType(VersionTypeEnum versionType) {
            this.versionType = versionType;
        }

    }

    private AHUContext() {

    }

    public static User getUser() {
        return Context.INSTANCE.user;
    }

    public static void setUser(User user) {
        Context.INSTANCE.user = user;
        Context.INSTANCE.setFactory(user.getUserFactory());
    }

    public static String getUserName() {
        if (Context.INSTANCE.user == null) {
            return StringUtils.EMPTY;
        }
        return Context.INSTANCE.user.getUserName();
    }

    public static String getUserRole() {
        if (Context.INSTANCE.user == null) {
            return StringUtils.EMPTY;
        }
        return Context.INSTANCE.user.getUserRole();
    }

    public static String getFactoryName() {
        return Context.INSTANCE.factory.name().toUpperCase();
    }

    public static LanguageEnum getLanguage() {
        LanguageEnum language = null;
        if (Context.INSTANCE.user != null) {
            language = LanguageEnum.byId(Context.INSTANCE.user.getPreferredLocale());
            Locale.setDefault(Locale.US);
        }
        if (language == null) {
            // use system default locale as default language
            if (Locale.getDefault().getLanguage() == Locale.CHINESE.getLanguage()) {
                language = LanguageEnum.Chinese;
            } else {
                language = LanguageEnum.English;
                Locale.setDefault(Locale.US);
            }
        }
        return language;
    }

    public static String getIntlString(String resourceId) {
        return I18NBundle.getString(resourceId, getLanguage());
    }

    public static String getIntlString(String resourceId, Locale locale) {
        if (Locale.SIMPLIFIED_CHINESE.equals(locale)) {
            System.out.println(locale);
            return I18NBundle.getString(resourceId, LanguageEnum.Chinese);
        }
        return I18NBundle.getString(resourceId, getLanguage());
    }

    public static String getIntlString(String resourceId, Object... params) {
        return MessageFormat.format(getIntlString(resourceId), params == null ? new Object[0] : params);
    }

    public static String getIntlNumber(int number) {
        LanguageEnum language = getLanguage();
        if (LanguageEnum.English.equals(language)) {
            return NumberUtil.ordinal(number);
        }
        return String.valueOf(number);
    }

    public static UnitSystemEnum getUnitSystem() {
        if (Context.INSTANCE.user == null) {
            return UnitSystemEnum.M;
        }
        UnitSystemEnum unitSystem = UnitSystemEnum.byId(Context.INSTANCE.user.getUnitPreferCode());
        if (unitSystem == null) {
            unitSystem = UnitSystemEnum.M;
        }
        return unitSystem;
    }

    public static void setUserMeta(List<UserMeta> userMetaList) {
        Context.INSTANCE.setUserMetas(userMetaList);
    }

    public static String getUserMetaValue(UserMetaEnum userMetaEnum) {
        UserMeta userMeta = Context.INSTANCE.getUserMeta(userMetaEnum);
        if (userMeta != null) {
            return userMeta.getMetaValue();
        }
        return StringUtils.EMPTY;
    }

    public static UserMeta getUserMeta(UserMetaEnum userMetaEnum) {
        return Context.INSTANCE.getUserMeta(userMetaEnum);
    }

    public static void setAhuVersion(String ahuVersion) {
        Context.INSTANCE.setAhuVersion(ahuVersion);
    }

    public static String getAhuVersion() {
        return Context.INSTANCE.getAhuVersion();
    }

    public static void setAhuPatchVersion(String ahuPatchVersion) {
        Context.INSTANCE.setAhuPatchVersion(ahuPatchVersion);
    }

    public static String getAhuPatchVersion() {
        return Context.INSTANCE.getAhuPatchVersion();
    }

    public static void setVersionType(VersionTypeEnum versionType) {
        Context.INSTANCE.setVersionType(versionType);
    }

    public static boolean isExportVersion() {
        return Context.INSTANCE.getVersionType() == VersionTypeEnum.EXPORT;
    }

    /**
     * Load  json for export or default.
     * 
     * @param filePath
     * @return
     */
    public static InputStream getJsonInputStream(String fileName) {
        InputStream jsonStream = null;
        if (isExportVersion()) {
            String exportFileName = fileName.replaceAll(CommonConstant.SYS_JSON_EXTENSION,
                    CommonConstant.SYS_EXPORT_JSON_EXTENSION);
            jsonStream = AHUContext.class.getClassLoader().getResourceAsStream(exportFileName);
        }
        if (jsonStream == null) {
            jsonStream = AHUContext.class.getClassLoader().getResourceAsStream(fileName);
        }
        return jsonStream;
    }

    public static String getImagePath(String name) {
        String extension = (getLanguage() == LanguageEnum.Chinese ? SysConstants.IMAGE_LANG_CH
                : SysConstants.IMAGE_LANG_EN) + SysConstants.PNG_EXTENSION;
        return SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR + name + extension;
    }

    public static String getFontFamily() {
        if (LanguageEnum.Chinese.equals(getLanguage())) {
            return CommonConstant.SYS_FONT_SONGTI;
        }
        return CommonConstant.SYS_FONT_ARIAL;
    }

    public static String getErrorMessage(AhuException exception) {
        if (exception.getErrorCode() == null) {
            return exception.getMessage();
        }

        return getIntlString(exception.getErrorCode().getMessage(), exception.getParams());
    }

}

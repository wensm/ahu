package com.carrier.ahu.engine.heatrecycle;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

public class HeaterxWTest {
    // 能出东西即可，具体含义还待整合
    public static void main(String args[]) {
        getWheelCalc();
        getWheelPrice();
    }
    
    public static void getWheelCalc() {
        byte s1 = 0;
        byte s2 = 0;
        byte z1 = 0;
        byte z2 = 0;
        String key = "E0200";
//        String key = "E/0.200/";
        Pointer in = new Memory(20 * Double.SIZE);
        in.setDouble(1 * Native.getNativeSize(Double.TYPE), 2.5);
        in.setDouble(2 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(3 * Native.getNativeSize(Double.TYPE), 20);
        in.setDouble(4 * Native.getNativeSize(Double.TYPE), 0.006);
        in.setDouble(5 * Native.getNativeSize(Double.TYPE), 2.5);
        in.setDouble(6 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(7 * Native.getNativeSize(Double.TYPE), -10);
        in.setDouble(8 * Native.getNativeSize(Double.TYPE), 0.0016);
        in.setDouble(9 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(10 * Native.getNativeSize(Double.TYPE), 1.3);
        in.setDouble(11 * Native.getNativeSize(Double.TYPE), 12);
        in.setDouble(12 * Native.getNativeSize(Double.TYPE), 0.002);
        in.setDouble(13 * Native.getNativeSize(Double.TYPE), 0.200);
        in.setDouble(14 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(15 * Native.getNativeSize(Double.TYPE), 0.00007);
        in.setDouble(16 * Native.getNativeSize(Double.TYPE), 101325);
        in.setDouble(17 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(18 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(19 * Native.getNativeSize(Double.TYPE), 1);
        Pointer out = new Memory(50 * Double.SIZE);
        HeaterxLib.HEATERX_INSTANCE.GET_WHEELCALC(s1, s2, in, key, z1, z2,out);//转轮
        for (int i = 0; i <= 40; i++) {
            System.out.println(i + "-----" + out.getDouble(i * Native.getNativeSize(Double.TYPE)));
        }
    }
    
    public static void getWheelPrice() {
        byte s1 = 0;
        byte s2 = 0;
        byte z1 = 0;
        byte z2 = 0;
        String key = "W0200";
        Pointer in = new Memory(20 * Double.SIZE);
        in.setDouble(1 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(2 * Native.getNativeSize(Double.TYPE), 1.3);
        in.setDouble(3 * Native.getNativeSize(Double.TYPE), 0.002);
        in.setDouble(4 * Native.getNativeSize(Double.TYPE), 0.200);
        in.setDouble(5 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(6 * Native.getNativeSize(Double.TYPE), 3);
        in.setDouble(7 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(8 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(9 * Native.getNativeSize(Double.TYPE), 2);
        in.setDouble(10 * Native.getNativeSize(Double.TYPE), 4);
        in.setDouble(11 * Native.getNativeSize(Double.TYPE), 1);
        in.setDouble(12 * Native.getNativeSize(Double.TYPE), 4);
        in.setDouble(13 * Native.getNativeSize(Double.TYPE), 1);
        in.setDouble(14 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(15 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(16 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(17 * Native.getNativeSize(Double.TYPE), 1);
        in.setDouble(18 * Native.getNativeSize(Double.TYPE), 0);
        in.setDouble(19 * Native.getNativeSize(Double.TYPE), 0);
        Pointer out = new Memory(50 * Double.SIZE);
        // HeaterxLib.HEATERX_INSTANCE.GET_WHEELCALC(s1, s2, in, key, z1, z2,out);//转轮
        HeaterxLib.HEATERX_INSTANCE.GET_WHEELPRICE(s1, s2, in, key, z1, z2,out,"code");//转轮
        for (int i = 0; i <= 40; i++) {
            System.out.println(i + "-----" + out.getDouble(i * Native.getNativeSize(Double.TYPE)));
        }
    }
}

package com.carrier.ahu.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.carrier.ahu.po.AbstractPo;

/**
 * 批量配置进度记录
 * 
 * @author Simon
 * @since 2017-12-09
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
public class BatchCfgProgress extends AbstractPo {
	private static final long serialVersionUID = -197270056223412395L;
	@Id
	@GeneratedValue
	String progressId;// 进度编码
	String projectId;// 项目ID
	String groupId;// 组ID
	String groupCode;// 组类型,comes from AHU Type Code
	@Lob
	@Basic(fetch = FetchType.EAGER)
	@Column(name = "JOBJSON", columnDefinition = "CLOB", nullable = true)
	String jobJson;// 进度计算数据
}

package com.carrier.ahu.report;

import java.util.List;

import com.carrier.ahu.common.entity.Project;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ExportProject extends Project {
	private static final long serialVersionUID = 1270058162998038295L;
	private List<ExportGroup> groups;
	/** AHUs not in any group */
	private List<ExportUnit> units;

	private String version;//版本
	private String patchVersion;//补丁版本

}
package com.carrier.ahu.unit;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.lang3.StringUtils;

import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.util.EmptyUtil;

public class KeyGenerator {
	private static final String PROJECT = "P";
	private static final String GROUP = "G";
	private static final String UNIT = "A";
	private static final String PART = "S";

	private static final ReentrantLock lock = new ReentrantLock();

	private static HashMap<String, AtomicInteger> tssCache = new HashMap<String, AtomicInteger>(1);
	// 因为有锁，所以是变成了线程安全的，省去每次 new
	private static final SimpleDateFormat sdf = new SimpleDateFormat(CommonConstant.SYS_FORMAT_yyMMddHHmmss);

	/**
	 * Generate projectid<br>
	 * format:P+facId+userId+time+NO.
	 * 
	 * @param facId
	 *            if(null):CommonConstant.SYS_BLANK
	 * @param userId
	 *            if(null):CommonConstant.SYS_BLANK
	 * @return
	 */
	public static String genProjectId(String facId, String userId) {
		return padPrefix(facId, userId, PROJECT) + getTimeStampSequence();
	}

	/**
	 * Generate groupid<br>
	 * format:G+facId+userId+time+NO.
	 * 
	 * @param facId
	 *            if(null):CommonConstant.SYS_BLANK
	 * @param userId
	 *            if(null):CommonConstant.SYS_BLANK
	 * @return
	 */
	public static String genGroupId(String facId, String userId) {
		return padPrefix(facId, userId, GROUP) + getTimeStampSequence();
	}

	/**
	 * Generate unitid<br>
	 * format:A+facId+userId+time+NO.
	 * 
	 * @param facId
	 *            if(null):CommonConstant.SYS_BLANK
	 * @param userId
	 *            if(null):CommonConstant.SYS_BLANK
	 * @return
	 */
	public static String genUnitId(String facId, String userId) {
		return padPrefix(facId, userId, UNIT) + getTimeStampSequence();
	}

	/**
	 * Generate partid<br>
	 * format:S+facId+userId+time+NO.
	 * 
	 * @param facId
	 *            if(null):CommonConstant.SYS_BLANK
	 * @param userId
	 *            if(null):CommonConstant.SYS_BLANK
	 * @return
	 */
	public static String genPartId(String facId, String userId) {
		return padPrefix(facId, userId, PART) + getTimeStampSequence();
	}

	/**
	 * format prefix code
	 * 
	 * @param facId
	 * @param userId
	 * @param type
	 * @return
	 */
	private static String padPrefix(String facId, String userId, String type) {
		if (EmptyUtil.isEmpty(facId)) {
			facId = CommonConstant.SYS_BLANK;
		}
		if (EmptyUtil.isEmpty(userId)) {
			userId = CommonConstant.SYS_BLANK;
		}
		StringBuffer buffer = new StringBuffer();
		buffer.append(type);
		buffer.append(StringUtils.leftPad(facId, 3, "000"));
		buffer.append(StringUtils.leftPad(userId, 4, "-000"));
		buffer.append(CommonConstant.SYS_PUNCTUATION_HYPHEN);
		return buffer.toString();
	}

	/**
	 * 枷锁获取时间戳
	 * 
	 * @return
	 */
	public static String getTimeStampSequence() {
		String timestamp = null;
		String inc = null;
		lock.lock();
		try {
			timestamp = sdf.format(new Date());
			AtomicInteger value = tssCache.get(timestamp);
			if (EmptyUtil.isEmpty(value)) {
				tssCache.clear();
				int defaultStartValue = 0;
				tssCache.put(timestamp, new AtomicInteger(defaultStartValue));
				inc = String.valueOf(defaultStartValue);
			} else {
				inc = String.valueOf(value.addAndGet(1));
			}
		} finally {
			lock.unlock();
		}
		return timestamp + StringUtils.leftPad(inc, 5, "-0000");
	}
}
package com.carrier.ahu.common.enums;

/**
 * 批量配置进度状态为： 配置中（after 新建配置记录）</br>
 * 配置完成成功（after 成功配置）</br>
 * 配置完成失败（after 配置过程出现异常）</br>
 * 
 */
public enum ProgressStatusEnum {
	PROGRESSING("progressing", "配置中", "Progressing"),

	PROGRESS_DONE("compelete", "配置完成", "Progress Compelete"),

	PROGRESS_FAILED("failed", "配置失败", "Progress Failed");

	private String cnName;// 中文名称
	private String name;// 英文名称
	private String id;// id

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCnName() {
		return this.cnName;
	}

	ProgressStatusEnum(String id, String cnName, String name) {
		this.id = id;
		this.cnName = cnName;
		this.name = name;
	}
}

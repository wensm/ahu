package com.carrier.ahu.common.exception.calculation;

import com.carrier.ahu.common.exception.ErrorCode;

/**
 * Created by Braden Zhou on 2018/06/07.
 */
@SuppressWarnings("serial")
public class HeatQCalcException extends CalculationException {

    public HeatQCalcException(ErrorCode errorCode, Throwable cause, String... params) {
        super(errorCode, cause, params);
    }

    public HeatQCalcException(ErrorCode errorCode, String... params) {
        this(errorCode, null, params);
    }

    public HeatQCalcException(String message, Throwable cause) {
        super(message, cause);
    }

    public HeatQCalcException(String message) {
        super(message);
    }

}

package com.carrier.ahu.po.batch;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class BatchConfigJob {
	String jobId;
	String status;// 状态
	List<AhuJob> data;
}

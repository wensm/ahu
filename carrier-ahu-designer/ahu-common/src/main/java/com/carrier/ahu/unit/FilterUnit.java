package com.carrier.ahu.unit;

import java.util.LinkedList;

/**
 * Created by liangd4 on 2017/7/5.
 */
public class FilterUnit {

	/**
	 * 
	 * @param windSpeed
	 *            实际风速
	 * @param Xvalue
	 * @param Yvalue
	 * @param js
	 * @return
	 */
	public static Double qxnh2(double windSpeed, LinkedList<Double> Xvalue, LinkedList<Double> Yvalue, int js) {
		Double[] xs = new Double[js + 1];
		Double[][] a = new Double[11][11];
		Integer m;
		Integer n;
		Double[] d = new Double[11];
		Double[] z = new Double[7];
		if (Xvalue == null || Xvalue.isEmpty()) {
			return 0.0;
		}

		if (Yvalue == null || Yvalue.isEmpty()) {
			return 0.0;
		}
		m = (new Double(Xvalue.get(Xvalue.size() - 1) - Xvalue.get(0))).intValue();
		for (int x = 1; x <= 10; x++) {
			d[x] = 0.0;
		}
		for (int x = 0; x < m; x++) {
			d[1] = d[1] + Xvalue.get(x);
			a[1][2] = d[1];
			d[2] = d[2] + Math.pow(Xvalue.get(x), 2);
			a[1][3] = d[2];
			d[3] = d[3] + Yvalue.get(x);
			a[1][4] = d[3];
			d[4] = d[4] + Math.pow(Xvalue.get(x), 3);
			a[2][3] = d[4];
			d[5] = d[5] + Xvalue.get(x) * Yvalue.get(x);
			a[2][4] = d[5];
			d[6] = d[6] + Math.pow(Xvalue.get(x), 4);
			a[3][3] = d[6];
			d[7] = d[7] + Math.pow(Xvalue.get(x), 2) * Yvalue.get(x);
			a[3][4] = d[7];
		}
		a[1][1] = new Double(m);
		a[2][1] = a[1][2];
		a[2][2] = a[1][3];
		a[3][1] = a[1][3];
		a[3][2] = a[2][3];
		n = 3;
		m = 4;
		for (int x = 1; x <= n - 1; x++) {
			for (int y = x + 1; y <= n; y++) {
				for (int w = x + 1; w <= m; w++) {
					a[y][w] = a[y][w] - a[y][x] * a[x][w] / a[x][x];
				}
			}
		}
		z[n] = a[n][m] / a[n][n];
		for (int x = n - 1; x >= 1; x--) {
			z[x] = 0.0;
			for (int y = n; y >= x + 1; y--) {
				z[x] = z[x] + a[x][y] * z[y];
				z[x] = (a[x][m] - z[x]) / a[x][x];
			}
		}

		for (int i = 0; i <= js; i++) {
			xs[i] = z[i + 1];
		}

		if (js == 2) {
			return xs[0] + windSpeed * xs[1] + Math.pow(windSpeed, 2) * xs[2];
		}
		return xs[0] + windSpeed * xs[1] + Math.pow(windSpeed, 2) * xs[2] + Math.pow(windSpeed, 3) * xs[3];
	}

}

package com.carrier.ahu.common.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import lombok.extern.slf4j.Slf4j;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;

import static com.carrier.ahu.constant.CommonConstant.template_word;

@Slf4j
public class WordUtil {
	/**
	 * 用一个docx文档作为模板，然后替换其中的内容，再写入目标文档中。
	 * 
	 * @throws Exception
	 */
	public  void testTemplateWrite() throws Exception {
        String path = "";
//        String path = SysConstants.ASSERT_DIR + SysConstants.REPORT_DIR + reportData.getProject().getPid();

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("date", "2014-02-28");
		params.put("time", "13:32");
		params.put("projectname", "项目ahu");
		params.put("reportDate", "2014-02-28");
		params.put("totalAmt", "300.00");

		InputStream is = new FileInputStream(template_word);
		XWPFDocument doc = new XWPFDocument(is);

		// 替换header 段落变量
		this.replaceInHeaderPara(doc, params);
		// 替换header 表格变量
		this.replaceInHeaderTable(doc, params);
		//生成非标明细数据
		createSimpleTableWithBdColor(doc);

		OutputStream os = new FileOutputStream("D:\\speciallist.docx");
		doc.write(os);
		this.close(os);
		this.close(is);
	}

	// 表格自定义边框 请忽略这么丑的颜色样式,主要说明可以自定义样式
	public void createSimpleTableWithBdColor(XWPFDocument doc) throws Exception {
		XWPFTable table = doc.createTable(3, 4);

		CTTblBorders borders = table.getCTTbl().getTblPr().addNewTblBorders();
		CTBorder hBorder = borders.addNewInsideH();
		hBorder.setVal(STBorder.Enum.forString("none"));
		hBorder.setSz(new BigInteger("1"));
		hBorder.setColor("0000FF");

		CTBorder vBorder = borders.addNewInsideV();
		vBorder.setVal(STBorder.Enum.forString("none"));
		vBorder.setSz(new BigInteger("1"));
		vBorder.setColor("00FF00");

		CTBorder lBorder = borders.addNewLeft();
		lBorder.setVal(STBorder.Enum.forString("none"));
		lBorder.setSz(new BigInteger("1"));
		lBorder.setColor("3399FF");

		CTBorder rBorder = borders.addNewRight();
		rBorder.setVal(STBorder.Enum.forString("none"));
		rBorder.setSz(new BigInteger("1"));
		rBorder.setColor("F2B11F");

		CTBorder tBorder = borders.addNewTop();
		tBorder.setVal(STBorder.Enum.forString("single"));
		tBorder.setSz(new BigInteger("2"));
		tBorder.setSpace(new BigInteger("2"));
		tBorder.setColor("C3599D");

		CTBorder bBorder = borders.addNewBottom();
		bBorder.setVal(STBorder.Enum.forString("none"));
		bBorder.setSz(new BigInteger("1"));
		bBorder.setColor("BF6BCC");

		CTTbl ttbl = table.getCTTbl();
		CTTblPr tblPr = ttbl.getTblPr() == null ? ttbl.addNewTblPr() : ttbl
				.getTblPr();
		CTTblWidth tblWidth = tblPr.isSetTblW() ? tblPr.getTblW() : tblPr
				.addNewTblW();
		CTJc cTJc = tblPr.addNewJc();
		cTJc.setVal(STJc.Enum.forString("center"));
		tblWidth.setW(new BigInteger("8000"));
		tblWidth.setType(STTblWidth.DXA);

		String[] strs = { "产品：", "39CQ", "序号", "001" };
		for (int i = 0; i < 3; i++) {
			XWPFTableRow theRow = table.getRow(i);
			for (int j = 0; j < 4; j++) {
				theRow.getCell(j).setText(strs[j]);
			}
		}
	}

	/**
	 * 替换段落里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
	private void replaceInPara(XWPFDocument doc, Map<String, Object> params) {
		Iterator<XWPFParagraph> iterator = doc.getParagraphsIterator();
		XWPFParagraph para;
		while (iterator.hasNext()) {
			para = iterator.next();
			this.replaceInPara(para, params);
		}
	}

	/**
	 * 替换段落里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
	private void replaceInHeaderPara(XWPFDocument doc,
			Map<String, Object> params) {
		List<XWPFParagraph> headParas = doc.getHeaderFooterPolicy()
				.getDefaultHeader().getParagraphs();
		for (XWPFParagraph headPara : headParas) {
			this.replaceInPara(headPara, params);
		}
	}

	/**
	 * 替换段落里面的变量
	 * 
	 * @param para
	 *            要替换的段落
	 * @param params
	 *            参数
	 */
	private void replaceInPara(XWPFParagraph para, Map<String, Object> params) {
		List<XWPFRun> runs = para.getRuns();
		for (int i = 0; i < runs.size(); i++) {
			XWPFRun run = runs.get(i);
			String runText = run.toString().trim();
			System.out.println(">>" + runText);
			for (Entry e : params.entrySet()) {
				if (runText.equals(String.valueOf(e.getKey()))) {
					// 直接调用XWPFRun的setText()方法设置文本时，在底层会重新创建一个XWPFRun，把文本附加在当前文本后面，
					// 所以我们不能直接设值，需要先删除当前run,然后再自己手动插入一个新的run。
					// para.insertNewRun(i).setText(String.valueOf(e.getValue()));
					para.removeRun(i);
					XWPFRun paragraphRun = para.createRun();
					paragraphRun.setText(String.valueOf(e.getValue()));
				}
			}

		}

	}

	/**
	 * 替换表格里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
	private void replaceInHeaderTable(XWPFDocument doc,
			Map<String, Object> params) {
		List<XWPFTable> headTables = doc.getHeaderFooterPolicy()
				.getDefaultHeader().getTables();
		List<XWPFTableRow> rows;
		List<XWPFTableCell> cells;
		List<XWPFParagraph> paras;
		for (XWPFTable table : headTables) {
			rows = table.getRows();
			for (XWPFTableRow row : rows) {
				cells = row.getTableCells();
				for (XWPFTableCell cell : cells) {
					paras = cell.getParagraphs();
					for (XWPFParagraph para : paras) {
						this.replaceInPara(para, params);
					}
				}
			}
		}
	}

	/**
	 * 替换表格里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
	private void replaceInTable(XWPFDocument doc, Map<String, Object> params) {
		Iterator<XWPFTable> iterator = doc.getTablesIterator();
		XWPFTable table;
		List<XWPFTableRow> rows;
		List<XWPFTableCell> cells;
		List<XWPFParagraph> paras;
		while (iterator.hasNext()) {
			table = iterator.next();
			rows = table.getRows();
			for (XWPFTableRow row : rows) {
				cells = row.getTableCells();
				for (XWPFTableCell cell : cells) {
					paras = cell.getParagraphs();
					for (XWPFParagraph para : paras) {
						this.replaceInPara(para, params);
					}
				}
			}
		}
	}

	/**
	 * 关闭输入流
	 * 
	 * @param is
	 */
	private void close(InputStream is) {
		if (is != null) {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 关闭输出流
	 * 
	 * @param os
	 */
	private void close(OutputStream os) {
		if (os != null) {
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}

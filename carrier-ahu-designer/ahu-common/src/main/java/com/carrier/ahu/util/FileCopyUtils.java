package com.carrier.ahu.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.Date;

import org.apache.commons.io.FileUtils;

public class FileCopyUtils {
	public static void main(String[] args) throws InterruptedException, IOException {

		File source = new File("C:\\Users\\nikos7\\Desktop\\files\\sourcefile1.txt");
		File dest = new File("C:\\Users\\nikos7\\Desktop\\files\\destfile1.txt");

		// copy file using FileStreams
		long start = System.nanoTime();
		long end;
		copyFileUsingFileStreams(source, dest);
		System.out.println("Time taken by FileStreams Copy = " + (System.nanoTime() - start));

		// copy files using java.nio.FileChannel
		source = new File("C:\\Users\\nikos7\\Desktop\\files\\sourcefile2.txt");
		dest = new File("C:\\Users\\nikos7\\Desktop\\files\\destfile2.txt");
		start = System.nanoTime();
		copyFileUsingFileChannels(source, dest);
		end = System.nanoTime();
		System.out.println("Time taken by FileChannels Copy = " + (end - start));

		// copy file using Java 7 Files class
		source = new File("C:\\Users\\nikos7\\Desktop\\files\\sourcefile3.txt");
		dest = new File("C:\\Users\\nikos7\\Desktop\\files\\destfile3.txt");
		start = System.nanoTime();
		copyFileUsingJava7Files(source, dest);
		end = System.nanoTime();
		System.out.println("Time taken by Java7 Files Copy = " + (end - start));

		// copy files using apache commons io
		source = new File("C:\\Users\\nikos7\\Desktop\\files\\sourcefile4.txt");
		dest = new File("C:\\Users\\nikos7\\Desktop\\files\\destfile4.txt");
		start = System.nanoTime();
		copyFileUsingApacheCommonsIO(source, dest);
		end = System.nanoTime();
		System.out.println("Time taken by Apache Commons IO Copy = " + (end - start));

	}
	
	public static String getBackupFileName(String sourceFileName){
		return sourceFileName + ".bak." + new Date().getTime();
	}

	/**
	 * 使用FileStreams复制
	 * <p>
	 * 最经典的方式将一个文件的内容复制到另一个文件中。
	 * 使用FileInputStream读取文件A的字节，使用FileOutputStream写入到文件B
	 * <p>
	 * 如你所见，执行几个读和写操作try的数据,所以这应该是一个低效率的
	 * <p>
	 * Time taken by FileStreams Copy = 127572360
	 * 
	 * @param source
	 * @param dest
	 * @throws IOException
	 */
	public static void copyFileUsingFileStreams(File source, File dest) throws IOException {
		InputStream input = null;
		OutputStream output = null;
		try {
			input = new FileInputStream(source);
			output = new FileOutputStream(dest);
			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = input.read(buf)) > 0) {
				output.write(buf, 0, bytesRead);
			}
		} finally {
			if(input!=null) {
				input.close();
			}
			if(output!=null) {
			     output.close();
			}
		}
	}

	/**
	 * 使用FileChannel复制
	 * <p>
	 * Java NIO包括transferFrom方法,根据文档应该比文件流复制的速度更快。
	 * <p>
	 * Time taken by FileChannels Copy = 10449963
	 * <p>
	 * 
	 * @param source
	 * @param dest
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public static void copyFileUsingFileChannels(File source, File dest) throws IOException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		try {
			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} finally {
			if(inputChannel!=null) {
				inputChannel.close();
			}
			if(outputChannel!=null) {
				outputChannel.close();
			}
			
		}
	}

	/**
	 * 使用Java7的Files类复制
	 * <p>
	 * 如果你有一些经验在Java 7中你可能会知道,可以使用复制方法的Files类文件,从一个文件复制到另一个文件
	 * <p>
	 * Time taken by Java7 Files Copy = 10808333
	 * <p>
	 * 
	 * @param source
	 * @param dest
	 * @throws IOException
	 */
	public static void copyFileUsingJava7Files(File source, File dest) throws IOException {
		Files.copy(source.toPath(), dest.toPath());
	}

	/**
	 * 使用Commons IO复制
	 * <p>
	 * Apache Commons IO提供拷贝文件方法在其FileUtils类,可用于复制一个文件到另一个地方
	 * <p>
	 * 基本上,这个类使用Java NIO FileChannel内部。
	 * <p>
	 * Time taken by Apache Commons IO Copy = 17971677
	 * 
	 * @param source
	 * @param dest
	 * @throws IOException
	 */
	public static void copyFileUsingApacheCommonsIO(File source, File dest) throws IOException {
		FileUtils.copyFile(source, dest);
	}
}

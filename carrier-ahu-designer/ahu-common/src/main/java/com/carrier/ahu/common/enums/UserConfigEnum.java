package com.carrier.ahu.common.enums;

public enum UserConfigEnum {

    ROW("row", "排数"),
    MAXIMUM_COIL_FACE_VELOCITY("maximumCoilFaceVelocity", "最大迎面风速"),
    MAXIMUM_FAN_OUTLET_VELOCITY("maximumFanOutletVelocity", "风机最大出口风速"),
    MINIMUM_EFFICIENCY_OF_FAN("minimumEfficiencyOfFan", "最小风机效率");

    private String code;//值
    private String name;//名称

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    UserConfigEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }
}

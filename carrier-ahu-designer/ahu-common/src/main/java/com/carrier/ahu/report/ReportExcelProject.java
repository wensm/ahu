package com.carrier.ahu.report;

import java.util.List;

import com.carrier.ahu.common.entity.Project;

import lombok.Data;

@Data
public class ReportExcelProject {
	private Project project;
	private List<ProjAhu> projAhus;

	@Data
	public static class ProjAhu {
		private String unitId;// 序号,机组编号
		private String serial;// 机组型号，序列号
		private String ahuName;// 客户机组名称
		private int amount;// 数量
		private double packingPrice;// 装箱单价
		private double stdPrice;// 标准单价
		private double nStdpackingPrice;// 非标单价
		private double totalPrice;// 总价
		private double weight;// 重量
		private String factoryForm;// 出厂形式
		private String deformation;// 是否变形
		private String deformationSerial;// 变形后型号

	}

}

package com.carrier.ahu.engine.fan;

import lombok.Data;

@Data
public class KrugerSelectInfo {
    private float volume;
    private float pressure;
    private int pressureType;
    private int volumeUnit;
    private int pressureUnit;
    private int callType;
    private int fanSize;
    private int minStyle;
    private int minClass;
    private float soundDistance;
    private int hz;
    private float temperature;
    private int temperatureUnit;
    private int soundDistanceUnit;
    private float altitude;
    private int altitudeUnit;
    private float serviceFactor;
    private int outletType;
    private int bladeType;
    private int productType;
    private int fanCasing;
    private int fanWidth;
    private int velocityUnit;
    private int soundCondition;
    private String recordDirectory;
    private int fanGroup;
    private int fanArrangement;
    private int driveType;
    private int debug;
    private int phase;
    private int plenumChamberDesign;
    private int plenumOutletDesign;
    private int volts;
    private int plenumOutletDiameter;
    private int plenumChamberHeight;
    private int plenumChamberWidth;
    private int plenumChamberLength;
    private int plenumOutletType;
    private int plenumOutletWidth;
    private int plenumOutletHeight;
    private int speed;
    private int inverter;
    private int debugSize;
    private String debugType;
}

package com.carrier.ahu.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by liangd4 on 2017/9/28.
 */
public class CoilLen {

    public static Logger logger = LoggerFactory.getLogger(CoilLen.class.getName());

    /**
     * 冷水盘管段段长计算
     *
     * @param lengthParam
     * @param sectionId
     * @return
     */
    public Integer getLength(LengthParam lengthParam, String sectionId) {
        String eliminator = lengthParam.getEliminator();//挡水器

        try {
            if (sectionId.equals(SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId())) {
                if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator)) {//直接蒸发式盘管选择无挡水器时，段长6M
                    return 6;//直接蒸发式盘管6 段长8的无特殊逻辑
                }
                //直接蒸发式盘管后面有湿膜或高压喷雾，段长为12M
                if (null != lengthParam.getNextPartKey() &&
                        (SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER) ||
                                SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER))) {
                    return 12;
                }
            }
//            if (SystemCalculateConstants.COOLINGCOIL_DRAINPANTYPE_SLOPING.equals(drainpanType)) {
//                return 6;//斜水盘段长为6
//            }
            Integer ser = SystemCountUtil.getUnitNoInt(lengthParam.getSerial());
            String rows = lengthParam.getRows();
            if (isMergeLen(lengthParam, sectionId)
                    ) {
                String hRows = lengthParam.getHRows();
                return packCoilRowLength(ser,rows,hRows)-3;
            }
            //盘管为10R/12R,且后面没有湿膜加湿或者热水盘管组合，同时挡水器为无挡水器或者铝网挡水器时，段长为6M
			
            if (SystemCountUtil.gteBigUnit(ser)) { //如果机型大于2532 12M
                return 12;
            } else {
            	
            	if ((rows.equals("10") || rows.equals("12"))) {

    				if (null == lengthParam.getNextPartKey()) {
    					if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator)) {
    						return 8;
    					} else if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
    						return 6;
    					}

    				}

    				if (null != lengthParam.getNextPartKey()
    						&& !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey())
    								.equals(SectionTypeEnum.TYPE_HEATINGCOIL)
    						&& !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey())
    								.equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER))
    					if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator)) {
    						return 8;
    					} else if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
    						return 6;
    					}
    			}
                
                
                //盘管为10R/12R,且后面有湿膜加湿器组合时，段长为8M
                if ((rows.equals("10") || rows.equals("12"))
                        && null != lengthParam.getNextPartKey() && SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER)) {
                    if (!SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator) || SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
                        return 8;
                    }
                    return 8;
                }
                //盘管为10R/12R,且后面没有湿膜加湿或者热水盘管组合，同时挡水器为其他挡水器时，段长为8M
                if ((rows.equals("10") || rows.equals("12"))
                        && null != lengthParam.getNextPartKey()
                        && !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)
                        && !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER)) {
                    if (!SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator) || SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
                        return 8;
                    }
                }
                
                //单独冷水盘管+其他挡水器 段长为8M
                if ((rows.equals("10") || rows.equals("12"))
                        && null != lengthParam.getNextPartKey()
                        && !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)
                        && !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER)) {
                    if (!SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator) && !SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
                        return 8;
                    }
                }
                
                //后面有湿膜加湿器组合时，段长为6M
                if (null != lengthParam.getNextPartKey() && SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER)) {
                    if ((rows.equals("1") || rows.equals("2"))){//后面有湿膜加湿器组合时,且冷水1R 2R，段长为5M
                        return 5;
                    }
                    return 6;
                }
                //后面有高压喷雾加湿器组合时，段长为5M
                if (null != lengthParam.getNextPartKey() && (SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER)
                        /*|| (SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL))*/)) {//1-8排冷水单独 铝网5其他6
                    if(rows.equals("12")){
                        return 6;
                    }
                    return 5;
                }
                if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
                    return 5;
                } else {
                    return 6;
                }
            }
        } catch (Exception e) {
            logger.error("Failed to calculate sectionL for Cooling Coil: " + lengthParam.getSerial() + " & " + eliminator, e);
            return 0;
        }
    }

    /**
     * 冷+热：是否需要合并冷热段长
     * 1、盘管不为直膨，同时机组高度<20,后面是热水盘管的组合，段长为9M（旧软件中，冷水盘管后有热水盘管，总长为9M，其中冷水为6M，热水为3M）
     * 2、冷热都没有法兰连接
     * 3、冷水没有挡水器
     * 4、冷+热+湿膜不能使用冷热合并逻辑处理段长
     * @param lengthParam
     * @param sectionId
     * @return
     */
    public boolean isMergeLen(LengthParam lengthParam, String sectionId) {
        String eliminator = lengthParam.getEliminator();//挡水器
        Integer ser = SystemCountUtil.getUnitNoInt(lengthParam.getSerial());

        return sectionId.equals(SectionTypeEnum.TYPE_COLD.getId()) && SystemCountUtil.ltBigUnit(ser)
                && null != lengthParam.getNextPartKey()
                && SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)
                && (null == lengthParam.getNextNextPartKey() ||
                        (null != lengthParam.getNextNextPartKey() && !SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER))
                    )//冷+热+湿膜不能使用冷热合并逻辑处理段长。
                && !isCoolHeatHasFL(lengthParam)//冷热都没有法兰连接
                && SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator);//冷水没有挡水器
    }

    /**
     * 冷+热，如果有一个法兰连接，小机组不用选择合并冷热处理段长
     * @param lengthParam
     * @return
     */
    private boolean isCoolHeatHasFL(LengthParam lengthParam) {

        String coolingConnections = lengthParam.getMeta_section_coolingCoil_connections();//冷水连接方式
        String heatingConnections = lengthParam.getMeta_section_heatingCoil_connections();//热水连接方式

        if(SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_PAIRSFLANGE.equals(coolingConnections)){
            return true;
        }

        if (null != lengthParam.getNextPartKey()
                && SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_HEATINGCOIL)) {
            if(SystemCalculateConstants.HEATINGCOIL_CONNECTIONS_PAIRSFLANGE.equals(heatingConnections)){
                return true;
            }
        }
        return false;
    }

    /**
     * 热水盘管段段长计算
     *
     * @param lengthParam
     * @return
     */
    public Integer getHLength(LengthParam lengthParam) {
        String unitModel = lengthParam.getSerial();
        String eliminator = lengthParam.getEliminator();
        String rows = lengthParam.getRows();
        int sectionL = 3;//排数3R-8R,且没有组合的湿膜加湿时，段长5M
        try {
            String cutSerial = unitModel.substring(unitModel.length() - 4, unitModel.length());
            Integer ser = Integer.parseInt(cutSerial);

            //热水+湿膜：热水盘管，段长特殊处理
            if (null != lengthParam.getNextPartKey() && SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER)) {
                //有组合的湿膜加湿时，段长6M
                return 6;
            }//冷水+热水：热水盘管，段长特殊处理：0+3
            if (SystemCountUtil.ltBigUnit(ser) && null != lengthParam.getPrePartKey() && SectionTypeEnum.getSectionTypeFromId(lengthParam.getPrePartKey()).equals(SectionTypeEnum.TYPE_COLD)) {
                //有组合的湿膜加湿时，段长6M
                return 0 + 3;
            }
            if (rows.equals("2") || rows.equals("1")) {//排数1R/2R,且没有组合的湿膜加湿时，段长3M
                return 3;
            } else {
                return 5;
            }
        } catch (Exception e) {
            logger.error("Failed to calculate sectionL for Cooling Coil: " + unitModel + " & " + eliminator, e);
            return 0;
        }
    }

    private Integer packCoilRowLength(Integer serial, String cRow, String hRow) {
        Integer base = 10;
        Integer base600 = 600;
        Integer base700 = 700;
        Integer base800 = 800;
        Integer R1R2 = 138;
        Integer R3R6 = 208;
        Integer R3R6H = 258;
        Integer R7R8 = 258;
        Integer R10 = 313;
        Integer R12 = 368;
        Integer cLength;
        Integer hLength;

        if (cRow.equals(SystemCalculateConstants.COOLINGCOIL_ROWS_1) || cRow.equals(SystemCalculateConstants.COOLINGCOIL_ROWS_2)) {
            cLength = R1R2;
        } else if (cRow.equals(SystemCalculateConstants.COOLINGCOIL_ROWS_7) || cRow.equals(SystemCalculateConstants.COOLINGCOIL_ROWS_8)) {
            cLength = R7R8;
        } else if (cRow.equals(SystemCalculateConstants.COOLINGCOIL_ROWS_10)) {
            cLength = R10;
        } else if (cRow.equals(SystemCalculateConstants.COOLINGCOIL_ROWS_12)) {
            cLength = R12;
        } else {
            if (serial <= 1825) {
                cLength = R3R6;
            } else {
                cLength = R3R6H;
            }
        }

//        if (hRow.equals(SystemCalculateConstants.HEATINGCOIL_ROWS_1) || hRow.equals(SystemCalculateConstants.HEATINGCOIL_ROWS_2)) {
//            hLength = R1R2;
//        } else {
//            if (serial <= 1825) {
//                hLength = R3R6;
//            } else {
//                hLength = R3R6H;
//            }
//        }
        hLength=R3R6H;

        if (base600 - base - cLength - hLength >= 100) {
            return 7;
        }
        if (base700 - base - cLength - hLength >= 100) {
            return 8;
        }
        if (base800 - base - cLength - hLength >= 100) {
            return 9;
        }
        return 9;
    }
}

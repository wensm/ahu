package com.carrier.ahu.po.batch;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class CalcJob {
	/**
	 * 批量计算基础类型</br>
	 * 重力计算：</br>
	 * 阻力计算：</br>
	 * 段长计算：</br>
	 * 价格计算：</br>
	 */
	String calcType;
	String status;// 状态
	List<JobItem> data;
}

package com.carrier.ahu.common.model.partition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.math.NumberUtils;

import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.enums.FaceTypeEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.CommonConstant;

import lombok.Data;

/**
 * 描述一个AHU的分段，包含位置，段，长，宽，高，以及各面的面板信息
 *
 * Created by liujianfeng on 2017/9/1.
 */
@Data
public class AhuPartition {

    /** 冷水+湿膜加湿段-016 */
    public static final String CODE_016 = SectionTypeEnum.TYPE_COLD.getId() + CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN + SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId();
    /** 热水+湿膜加湿段-017 */
    public static final String CODE_017 = SectionTypeEnum.TYPE_HEATINGCOIL.getId() + CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN + SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId();
    /** 冷水+高压喷雾加湿段-018 */
    public static final String CODE_018 = SectionTypeEnum.TYPE_COLD.getId() + CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN + SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId();
    /** 热水+高压喷雾加湿段-019 */
    public static final String CODE_019 = SectionTypeEnum.TYPE_HEATINGCOIL.getId() + CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN + SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId();
    /** 冷水+热水-021 */
    public static final String CODE_021 = SectionTypeEnum.TYPE_COLD.getId() + CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN + SectionTypeEnum.TYPE_HEATINGCOIL.getId();

    public static final String S_MKEY_METAID = "metaId";
    public static final String S_MKEY_POS = "pos";
    public static final String S_MKEY_SECTIONL = "sectionL";
    public static final String S_MKEY_WEIGHT = "weight";
    public static final String S_MKEY_METAJSON = "metaJson";

    public static final String C_MKEY_METAID = "id";
    public static final String C_MKEY_POS = "pos";
    public static final String C_MKEY_SECTIONL = "sectionL";
    public static final String C_MKEY_CODE = "code";

    private int pos;
    private LinkedList<Map<String, Object>> sections;
    private Map<String, Object> ahuParameters;
    private int length;
    private int width;
    private int height;
    private int casingWidth;
    private String base;//底座
    private String panelJson;
    private Map<String, PanelFace> panels;
    // private LinkedList<Map<String, Object>> coupleSections;
    // private Map<Integer, Map<String, Object>> integerSections;
    
    /**
     *  only enabled when parse json through method AhuPartitionUtils.parseAhuPartition()
     */
    private boolean topLayer;
    /**
     * Partition face type.
     * Must call AhuPartitionUtils.parseAhuPartition(Unit, Partition) before use this property
     */
    private FaceTypeEnum faceType;

    String series;// 机组型号

    /**
     * 获取合并后的段
     * @return
     */
    public LinkedList<Map<String, Object>> getCoupleSections() {

        // if (EmptyUtil.isNotEmpty(coupleSections)) {
        // return coupleSections;
        // }
        LinkedList<Map<String, Object>> list = new LinkedList<>();
        LinkedList<Integer> posList = getPosList();

        int csPos = 1;

        for (int i = 0; i < posList.size(); i++) {
            int pos = posList.get(i);
            Map<String, Object> section = maps().get(pos);
            String metaId = MapValueUtils.getStringValue(S_MKEY_METAID, section);
            Integer sectionL = MapValueUtils.getIntegerValue(S_MKEY_SECTIONL, section);

            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);

            Map<String, Object> nextSection = null;
            if (i < posList.size() - 1) {
                nextSection = maps().get(posList.get(i + 1));
            }
            String nextMetaId = MapValueUtils.getStringValue(S_MKEY_METAID, nextSection);
            Integer nextSectionL = MapValueUtils.getIntegerValue(S_MKEY_SECTIONL, nextSection);

            SectionTypeEnum nextSectionType = SectionTypeEnum.getSectionTypeFromId(nextMetaId);

            Map<String, Object> coupleSection = new HashMap<>();
            if (null == sectionType) {
                System.out.println("在解析分段数据时候出错了");
            } else if (SectionTypeEnum.TYPE_COLD == sectionType) {
                if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER == nextSectionType) {
                    coupleSection.put(C_MKEY_METAID, CODE_016);
                    coupleSection.put(C_MKEY_SECTIONL, sectionL + nextSectionL);
                    coupleSection.put(C_MKEY_CODE, "16");
                    i++;
                } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER == nextSectionType) {
                    coupleSection.put(C_MKEY_METAID, CODE_018);
                    coupleSection.put(C_MKEY_SECTIONL, sectionL + nextSectionL);
                    coupleSection.put(C_MKEY_CODE, "18");
                    i++;
                } else if (SectionTypeEnum.TYPE_HEATINGCOIL == nextSectionType) {
                    coupleSection.put(C_MKEY_METAID, CODE_021);
                    coupleSection.put(C_MKEY_SECTIONL, sectionL + nextSectionL);
                    coupleSection.put(C_MKEY_CODE, "21");
                    i++;
                } else {
                    coupleSection.put(C_MKEY_METAID, metaId);
                    coupleSection.put(C_MKEY_SECTIONL, sectionL);
                    coupleSection.put(C_MKEY_CODE, sectionType.getCodeNum());
                }
            } else if (SectionTypeEnum.TYPE_HEATINGCOIL == sectionType) {
                if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER == nextSectionType) {
                    coupleSection.put(C_MKEY_METAID, CODE_017);
                    coupleSection.put(C_MKEY_SECTIONL, sectionL + nextSectionL);
                    coupleSection.put(C_MKEY_CODE, "17");
                    i++;
                } else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER == nextSectionType) {
                    coupleSection.put(C_MKEY_METAID, CODE_019);
                    coupleSection.put(C_MKEY_SECTIONL, sectionL + nextSectionL);
                    coupleSection.put(C_MKEY_CODE, "19");
                    i++;
                } else {
                    coupleSection.put(C_MKEY_METAID, metaId);
                    coupleSection.put(C_MKEY_SECTIONL, sectionL);
                    coupleSection.put(C_MKEY_CODE, sectionType.getCodeNum());
                }
            } else {
                coupleSection.put(C_MKEY_METAID, metaId);
                coupleSection.put(C_MKEY_SECTIONL, sectionL);
                coupleSection.put(C_MKEY_CODE, sectionType.getCodeNum());
            }
            coupleSection.put(C_MKEY_POS, csPos++);
            list.add(coupleSection);
        }
        // setCoupleSections(list);
        return list;
    }

    /**
     * 取得当前分段内功能段排序
     * 
     * @return
     */
    public String getSectionsKeyString() {
        StringBuffer sb = new StringBuffer();

        for (Integer i : getPosList()) {
            Map<String, Object> map = maps().get(i);
            sb.append(CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN).append(map.get(S_MKEY_METAID));
        }
        if (sb.length() < 1) {
            return CommonConstant.SYS_BLANK;
        }
        System.out.println(sb.toString());
        return sb.toString().substring(1);
    }

    private LinkedList<Integer> getPosList() {
        Set<Integer> keySet = maps().keySet();
        LinkedList<Integer> list = new LinkedList<Integer>(keySet);
        Collections.sort(list);
        return list;
    }

    private Map<Integer, Map<String, Object>> maps() {
        // if (EmptyUtil.isEmpty(integerSections)) {
        Map<Integer, Map<String, Object>> maps = new HashMap<>();
        for (Map<String, Object> map : sections) {
            Integer pos = MapValueUtils.getIntegerValue(S_MKEY_POS, map);
            maps.put(pos, map);
        }
        // setIntegerSections(maps);
        // return integerSections;
        return maps;
        // } else {
        // return integerSections;
        // }
    }

    public List<String> getSectionTypeNoList() {
        String sectionsKeyString = getSectionsKeyString();
        while (true) {
            if (sectionsKeyString.contains(CODE_016)) {
                sectionsKeyString = sectionsKeyString.replace(CODE_016, "16");
            } else if (sectionsKeyString.contains(CODE_017)) {
                sectionsKeyString = sectionsKeyString.replace(CODE_017, "17");
            } else if (sectionsKeyString.contains(CODE_018)) {
                sectionsKeyString = sectionsKeyString.replace(CODE_018, "18");
            } else if (sectionsKeyString.contains(CODE_019)) {
                sectionsKeyString = sectionsKeyString.replace(CODE_019, "19");
            } else if (sectionsKeyString.contains(CODE_021)) {
                sectionsKeyString = sectionsKeyString.replace(CODE_021, "21");
            } else {
                break;
            }
        }
        sectionsKeyString = sectionsKeyString.replaceAll(CommonConstant.SYS_PUNCTUATION_LOW_HYPHEN, CommonConstant.SYS_PUNCTUATION_COMMA);
        String[] array = sectionsKeyString.split(CommonConstant.SYS_PUNCTUATION_COMMA);
        List<String> list = new ArrayList<>();

        for (String no : array) {
            list.add(getTypeNo(no));
        }
        return list;
    }

    private String getTypeNo(String no) {
        SectionTypeEnum type = SectionTypeEnum.getSectionTypeFromId(no);
        if (null == type) {
            return no;
        } else {
            String rtp = type.getCodeNum();
            while (rtp.startsWith(CommonConstant.SYS_STRING_NUMBER_0)) {
                rtp = rtp.substring(1);
            }
            return rtp;
        }
    }

    public static int getSectionLOfSection(Map<String, Object> section) {
        return NumberUtils.toInt(String.valueOf(section.get(S_MKEY_SECTIONL)));
    }

    public static String getMetaIdOfSection(Map<String, Object> section) {
        return String.valueOf(section.get(S_MKEY_METAID));
    }

    public static double getWeightOfSection(Map<String, Object> section) {
        return NumberUtils.toDouble(String.valueOf(section.get(S_MKEY_WEIGHT)));
    }

    public static String getMetaJsonOfSection(Map<String, Object> section) {
        return String.valueOf(section.get(S_MKEY_METAJSON));
    }

    /**
     * position + 1, start from 1.
     * 
     * @param section
     * @return
     */
    public static short getPosOfSection(Map<String, Object> section) {
        short position = NumberUtils.toShort(String.valueOf(section.get(S_MKEY_POS)));
        position++;
        return position;
    }

    public static Map<String, Object> createPartitionSection(Part part, int pos, int sectionL, double weight) {
        Map<String, Object> section = new HashMap<>();
        section.put(S_MKEY_METAID, part.getSectionKey());
        section.put(S_MKEY_POS, pos);
        section.put(S_MKEY_SECTIONL, sectionL);
        section.put(S_MKEY_WEIGHT, weight);
        section.put(S_MKEY_METAJSON,part.getMetaJson());
        return section;
    }

}

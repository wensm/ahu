package com.carrier.ahu.po;

public class Partw {
    private String pid;

    private String partid;

    private String wheelmodel;

    private String wheeltype;

    private Double wheelefficiency;

    private String bracketmaterial;

    private Double naratio;

    private Double navolume;

    private Double sectionl;

    private Double price;

    private Double weight;

    private Double resistance;

    private Double sindrybulbt;

    private Double sinwetbulbt;

    private Double sinrelativet;

    private Double windrybulbt;

    private Double winwetbulbt;

    private Double winrelativet;

    private Double soutdrybulbt;

    private Double soutwetbulbt;

    private Double soutrelativet;

    private Double woutdrybulbt;

    private Double woutwetbulbt;

    private Integer woutrelativet;

    private Double sindoordrybulbt;

    private Double sindoorwetbulbt;

    private Double sindoorrelativet;

    private Double windoordrybulbt;

    private Double windoorwetbulbt;

    private Double windoorrelativet;

    private Double sexhaustdrybulbt;

    private Double sexhaustwetbulbt;

    private Double sexhaustrelativet;

    private Double wexhaustdrybulbt;

    private Double wexhaustwetbulbt;

    private Double wexhaustrelativet;

    private String memo;

    private String season;

    private String efficiencytype;

    private Double sqt;

    private Double sqs;

    private Double sql;

    private Double wqt;

    private Double wqs;

    private Double wql;

    private String notmount;

    private Integer wheeldia;

    private String wheeldepth;

    private Integer platewidth;

    private String fieldinstall;

    private String country;

    private String supplier;

    private Double sexhaustdrop;

    private Double wexhaustdrop;

    private Double soutdrop;

    private Double woutdrop;

    private Double wexhaustefficiency;

    private Double sexhaustefficiency;

    private Double woutefficiency;

    private Double soutefficiency;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public String getWheelmodel() {
        return wheelmodel;
    }

    public void setWheelmodel(String wheelmodel) {
        this.wheelmodel = wheelmodel == null ? null : wheelmodel.trim();
    }

    public String getWheeltype() {
        return wheeltype;
    }

    public void setWheeltype(String wheeltype) {
        this.wheeltype = wheeltype == null ? null : wheeltype.trim();
    }

    public Double getWheelefficiency() {
        return wheelefficiency;
    }

    public void setWheelefficiency(Double wheelefficiency) {
        this.wheelefficiency = wheelefficiency;
    }

    public String getBracketmaterial() {
        return bracketmaterial;
    }

    public void setBracketmaterial(String bracketmaterial) {
        this.bracketmaterial = bracketmaterial == null ? null : bracketmaterial.trim();
    }

    public Double getNaratio() {
        return naratio;
    }

    public void setNaratio(Double naratio) {
        this.naratio = naratio;
    }

    public Double getNavolume() {
        return navolume;
    }

    public void setNavolume(Double navolume) {
        this.navolume = navolume;
    }

    public Double getSectionl() {
        return sectionl;
    }

    public void setSectionl(Double sectionl) {
        this.sectionl = sectionl;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getResistance() {
        return resistance;
    }

    public void setResistance(Double resistance) {
        this.resistance = resistance;
    }

    public Double getSindrybulbt() {
        return sindrybulbt;
    }

    public void setSindrybulbt(Double sindrybulbt) {
        this.sindrybulbt = sindrybulbt;
    }

    public Double getSinwetbulbt() {
        return sinwetbulbt;
    }

    public void setSinwetbulbt(Double sinwetbulbt) {
        this.sinwetbulbt = sinwetbulbt;
    }

    public Double getSinrelativet() {
        return sinrelativet;
    }

    public void setSinrelativet(Double sinrelativet) {
        this.sinrelativet = sinrelativet;
    }

    public Double getWindrybulbt() {
        return windrybulbt;
    }

    public void setWindrybulbt(Double windrybulbt) {
        this.windrybulbt = windrybulbt;
    }

    public Double getWinwetbulbt() {
        return winwetbulbt;
    }

    public void setWinwetbulbt(Double winwetbulbt) {
        this.winwetbulbt = winwetbulbt;
    }

    public Double getWinrelativet() {
        return winrelativet;
    }

    public void setWinrelativet(Double winrelativet) {
        this.winrelativet = winrelativet;
    }

    public Double getSoutdrybulbt() {
        return soutdrybulbt;
    }

    public void setSoutdrybulbt(Double soutdrybulbt) {
        this.soutdrybulbt = soutdrybulbt;
    }

    public Double getSoutwetbulbt() {
        return soutwetbulbt;
    }

    public void setSoutwetbulbt(Double soutwetbulbt) {
        this.soutwetbulbt = soutwetbulbt;
    }

    public Double getSoutrelativet() {
        return soutrelativet;
    }

    public void setSoutrelativet(Double soutrelativet) {
        this.soutrelativet = soutrelativet;
    }

    public Double getWoutdrybulbt() {
        return woutdrybulbt;
    }

    public void setWoutdrybulbt(Double woutdrybulbt) {
        this.woutdrybulbt = woutdrybulbt;
    }

    public Double getWoutwetbulbt() {
        return woutwetbulbt;
    }

    public void setWoutwetbulbt(Double woutwetbulbt) {
        this.woutwetbulbt = woutwetbulbt;
    }

    public Integer getWoutrelativet() {
        return woutrelativet;
    }

    public void setWoutrelativet(Integer woutrelativet) {
        this.woutrelativet = woutrelativet;
    }

    public Double getSindoordrybulbt() {
        return sindoordrybulbt;
    }

    public void setSindoordrybulbt(Double sindoordrybulbt) {
        this.sindoordrybulbt = sindoordrybulbt;
    }

    public Double getSindoorwetbulbt() {
        return sindoorwetbulbt;
    }

    public void setSindoorwetbulbt(Double sindoorwetbulbt) {
        this.sindoorwetbulbt = sindoorwetbulbt;
    }

    public Double getSindoorrelativet() {
        return sindoorrelativet;
    }

    public void setSindoorrelativet(Double sindoorrelativet) {
        this.sindoorrelativet = sindoorrelativet;
    }

    public Double getWindoordrybulbt() {
        return windoordrybulbt;
    }

    public void setWindoordrybulbt(Double windoordrybulbt) {
        this.windoordrybulbt = windoordrybulbt;
    }

    public Double getWindoorwetbulbt() {
        return windoorwetbulbt;
    }

    public void setWindoorwetbulbt(Double windoorwetbulbt) {
        this.windoorwetbulbt = windoorwetbulbt;
    }

    public Double getWindoorrelativet() {
        return windoorrelativet;
    }

    public void setWindoorrelativet(Double windoorrelativet) {
        this.windoorrelativet = windoorrelativet;
    }

    public Double getSexhaustdrybulbt() {
        return sexhaustdrybulbt;
    }

    public void setSexhaustdrybulbt(Double sexhaustdrybulbt) {
        this.sexhaustdrybulbt = sexhaustdrybulbt;
    }

    public Double getSexhaustwetbulbt() {
        return sexhaustwetbulbt;
    }

    public void setSexhaustwetbulbt(Double sexhaustwetbulbt) {
        this.sexhaustwetbulbt = sexhaustwetbulbt;
    }

    public Double getSexhaustrelativet() {
        return sexhaustrelativet;
    }

    public void setSexhaustrelativet(Double sexhaustrelativet) {
        this.sexhaustrelativet = sexhaustrelativet;
    }

    public Double getWexhaustdrybulbt() {
        return wexhaustdrybulbt;
    }

    public void setWexhaustdrybulbt(Double wexhaustdrybulbt) {
        this.wexhaustdrybulbt = wexhaustdrybulbt;
    }

    public Double getWexhaustwetbulbt() {
        return wexhaustwetbulbt;
    }

    public void setWexhaustwetbulbt(Double wexhaustwetbulbt) {
        this.wexhaustwetbulbt = wexhaustwetbulbt;
    }

    public Double getWexhaustrelativet() {
        return wexhaustrelativet;
    }

    public void setWexhaustrelativet(Double wexhaustrelativet) {
        this.wexhaustrelativet = wexhaustrelativet;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season == null ? null : season.trim();
    }

    public String getEfficiencytype() {
        return efficiencytype;
    }

    public void setEfficiencytype(String efficiencytype) {
        this.efficiencytype = efficiencytype == null ? null : efficiencytype.trim();
    }

    public Double getSqt() {
        return sqt;
    }

    public void setSqt(Double sqt) {
        this.sqt = sqt;
    }

    public Double getSqs() {
        return sqs;
    }

    public void setSqs(Double sqs) {
        this.sqs = sqs;
    }

    public Double getSql() {
        return sql;
    }

    public void setSql(Double sql) {
        this.sql = sql;
    }

    public Double getWqt() {
        return wqt;
    }

    public void setWqt(Double wqt) {
        this.wqt = wqt;
    }

    public Double getWqs() {
        return wqs;
    }

    public void setWqs(Double wqs) {
        this.wqs = wqs;
    }

    public Double getWql() {
        return wql;
    }

    public void setWql(Double wql) {
        this.wql = wql;
    }

    public String getNotmount() {
        return notmount;
    }

    public void setNotmount(String notmount) {
        this.notmount = notmount == null ? null : notmount.trim();
    }

    public Integer getWheeldia() {
        return wheeldia;
    }

    public void setWheeldia(Integer wheeldia) {
        this.wheeldia = wheeldia;
    }

    public String getWheeldepth() {
        return wheeldepth;
    }

    public void setWheeldepth(String wheeldepth) {
        this.wheeldepth = wheeldepth == null ? null : wheeldepth.trim();
    }

    public Integer getPlatewidth() {
        return platewidth;
    }

    public void setPlatewidth(Integer platewidth) {
        this.platewidth = platewidth;
    }

    public String getFieldinstall() {
        return fieldinstall;
    }

    public void setFieldinstall(String fieldinstall) {
        this.fieldinstall = fieldinstall == null ? null : fieldinstall.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier == null ? null : supplier.trim();
    }

    public Double getSexhaustdrop() {
        return sexhaustdrop;
    }

    public void setSexhaustdrop(Double sexhaustdrop) {
        this.sexhaustdrop = sexhaustdrop;
    }

    public Double getWexhaustdrop() {
        return wexhaustdrop;
    }

    public void setWexhaustdrop(Double wexhaustdrop) {
        this.wexhaustdrop = wexhaustdrop;
    }

    public Double getSoutdrop() {
        return soutdrop;
    }

    public void setSoutdrop(Double soutdrop) {
        this.soutdrop = soutdrop;
    }

    public Double getWoutdrop() {
        return woutdrop;
    }

    public void setWoutdrop(Double woutdrop) {
        this.woutdrop = woutdrop;
    }

    public Double getWexhaustefficiency() {
        return wexhaustefficiency;
    }

    public void setWexhaustefficiency(Double wexhaustefficiency) {
        this.wexhaustefficiency = wexhaustefficiency;
    }

    public Double getSexhaustefficiency() {
        return sexhaustefficiency;
    }

    public void setSexhaustefficiency(Double sexhaustefficiency) {
        this.sexhaustefficiency = sexhaustefficiency;
    }

    public Double getWoutefficiency() {
        return woutefficiency;
    }

    public void setWoutefficiency(Double woutefficiency) {
        this.woutefficiency = woutefficiency;
    }

    public Double getSoutefficiency() {
        return soutefficiency;
    }

    public void setSoutefficiency(Double soutefficiency) {
        this.soutefficiency = soutefficiency;
    }
}
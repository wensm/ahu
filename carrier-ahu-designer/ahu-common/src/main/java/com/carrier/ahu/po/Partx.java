package com.carrier.ahu.po;

public class Partx {
    private String pid;

    private String partid;

    private Double sindrybulbt;

    private Double sinwetbulbt;

    private Double sinrelativet;

    private Double windrybulbt;

    private Double winwetbulbt;

    private Double winrelativet;

    private Double shumidificationq;

    private Double whumidificationq;

    private String supplier;

    private Double soutdrybulbt;

    private Double soutwetbulbt;

    private Double soutrelativet;

    private Double woutdrybulbt;

    private Double woutwetbulbt;

    private Double woutrelativet;

    private Short htypes;

    private Integer sectionl;

    private Double resistance;

    private Double weight;

    private Double price;

    private String nostandard;

    private String memo;

    private String season;

    private String material;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public Double getSindrybulbt() {
        return sindrybulbt;
    }

    public void setSindrybulbt(Double sindrybulbt) {
        this.sindrybulbt = sindrybulbt;
    }

    public Double getSinwetbulbt() {
        return sinwetbulbt;
    }

    public void setSinwetbulbt(Double sinwetbulbt) {
        this.sinwetbulbt = sinwetbulbt;
    }

    public Double getSinrelativet() {
        return sinrelativet;
    }

    public void setSinrelativet(Double sinrelativet) {
        this.sinrelativet = sinrelativet;
    }

    public Double getWindrybulbt() {
        return windrybulbt;
    }

    public void setWindrybulbt(Double windrybulbt) {
        this.windrybulbt = windrybulbt;
    }

    public Double getWinwetbulbt() {
        return winwetbulbt;
    }

    public void setWinwetbulbt(Double winwetbulbt) {
        this.winwetbulbt = winwetbulbt;
    }

    public Double getWinrelativet() {
        return winrelativet;
    }

    public void setWinrelativet(Double winrelativet) {
        this.winrelativet = winrelativet;
    }

    public Double getShumidificationq() {
        return shumidificationq;
    }

    public void setShumidificationq(Double shumidificationq) {
        this.shumidificationq = shumidificationq;
    }

    public Double getWhumidificationq() {
        return whumidificationq;
    }

    public void setWhumidificationq(Double whumidificationq) {
        this.whumidificationq = whumidificationq;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier == null ? null : supplier.trim();
    }

    public Double getSoutdrybulbt() {
        return soutdrybulbt;
    }

    public void setSoutdrybulbt(Double soutdrybulbt) {
        this.soutdrybulbt = soutdrybulbt;
    }

    public Double getSoutwetbulbt() {
        return soutwetbulbt;
    }

    public void setSoutwetbulbt(Double soutwetbulbt) {
        this.soutwetbulbt = soutwetbulbt;
    }

    public Double getSoutrelativet() {
        return soutrelativet;
    }

    public void setSoutrelativet(Double soutrelativet) {
        this.soutrelativet = soutrelativet;
    }

    public Double getWoutdrybulbt() {
        return woutdrybulbt;
    }

    public void setWoutdrybulbt(Double woutdrybulbt) {
        this.woutdrybulbt = woutdrybulbt;
    }

    public Double getWoutwetbulbt() {
        return woutwetbulbt;
    }

    public void setWoutwetbulbt(Double woutwetbulbt) {
        this.woutwetbulbt = woutwetbulbt;
    }

    public Double getWoutrelativet() {
        return woutrelativet;
    }

    public void setWoutrelativet(Double woutrelativet) {
        this.woutrelativet = woutrelativet;
    }

    public Short getHtypes() {
        return htypes;
    }

    public void setHtypes(Short htypes) {
        this.htypes = htypes;
    }

    public Integer getSectionl() {
        return sectionl;
    }

    public void setSectionl(Integer sectionl) {
        this.sectionl = sectionl;
    }

    public Double getResistance() {
        return resistance;
    }

    public void setResistance(Double resistance) {
        this.resistance = resistance;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getNostandard() {
        return nostandard;
    }

    public void setNostandard(String nostandard) {
        this.nostandard = nostandard == null ? null : nostandard.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season == null ? null : season.trim();
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material == null ? null : material.trim();
    }
}
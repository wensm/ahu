package com.carrier.ahu.po;

public class Mixed4a {
    private String pid;

    private String partid;

    private String mixf;

    private String airh;

    private Double ainterfacer;

    private String dooro;

    private Double sindrybulbt;

    private Double sinwetbulbt;

    private Double sinrelativet;

    private Double windrybulbt;

    private Double winwetbulbt;

    private Double winrelativet;

    private Double snewdrybulbt;

    private Double snewwetbulbt;

    private Double snewrelativet;

    private Double wnewdrybulbt;

    private Double wnewwetbulbt;

    private Double wnewrelativet;

    private Double naratio;

    private Double navolume;

    private Double soutdrybulbt;

    private Double soutwetbulbt;

    private Double soutrelativet;

    private Double woutdrybulbt;

    private Double woutwetbulbt;

    private Double woutrelativet;

    private Integer sectionl;

    private Double resistance;

    private Double weight;

    private Double price;

    private String nostandard;

    private String memo;

    private String season;

    private String doortype;

    private String doordirection;

    private String vavlem;

    private Boolean uvc;

    private String actuator;

    private Boolean fac;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public String getMixf() {
        return mixf;
    }

    public void setMixf(String mixf) {
        this.mixf = mixf == null ? null : mixf.trim();
    }

    public String getAirh() {
        return airh;
    }

    public void setAirh(String airh) {
        this.airh = airh == null ? null : airh.trim();
    }

    public Double getAinterfacer() {
        return ainterfacer;
    }

    public void setAinterfacer(Double ainterfacer) {
        this.ainterfacer = ainterfacer;
    }

    public String getDooro() {
        return dooro;
    }

    public void setDooro(String dooro) {
        this.dooro = dooro == null ? null : dooro.trim();
    }

    public Double getSindrybulbt() {
        return sindrybulbt;
    }

    public void setSindrybulbt(Double sindrybulbt) {
        this.sindrybulbt = sindrybulbt;
    }

    public Double getSinwetbulbt() {
        return sinwetbulbt;
    }

    public void setSinwetbulbt(Double sinwetbulbt) {
        this.sinwetbulbt = sinwetbulbt;
    }

    public Double getSinrelativet() {
        return sinrelativet;
    }

    public void setSinrelativet(Double sinrelativet) {
        this.sinrelativet = sinrelativet;
    }

    public Double getWindrybulbt() {
        return windrybulbt;
    }

    public void setWindrybulbt(Double windrybulbt) {
        this.windrybulbt = windrybulbt;
    }

    public Double getWinwetbulbt() {
        return winwetbulbt;
    }

    public void setWinwetbulbt(Double winwetbulbt) {
        this.winwetbulbt = winwetbulbt;
    }

    public Double getWinrelativet() {
        return winrelativet;
    }

    public void setWinrelativet(Double winrelativet) {
        this.winrelativet = winrelativet;
    }

    public Double getSnewdrybulbt() {
        return snewdrybulbt;
    }

    public void setSnewdrybulbt(Double snewdrybulbt) {
        this.snewdrybulbt = snewdrybulbt;
    }

    public Double getSnewwetbulbt() {
        return snewwetbulbt;
    }

    public void setSnewwetbulbt(Double snewwetbulbt) {
        this.snewwetbulbt = snewwetbulbt;
    }

    public Double getSnewrelativet() {
        return snewrelativet;
    }

    public void setSnewrelativet(Double snewrelativet) {
        this.snewrelativet = snewrelativet;
    }

    public Double getWnewdrybulbt() {
        return wnewdrybulbt;
    }

    public void setWnewdrybulbt(Double wnewdrybulbt) {
        this.wnewdrybulbt = wnewdrybulbt;
    }

    public Double getWnewwetbulbt() {
        return wnewwetbulbt;
    }

    public void setWnewwetbulbt(Double wnewwetbulbt) {
        this.wnewwetbulbt = wnewwetbulbt;
    }

    public Double getWnewrelativet() {
        return wnewrelativet;
    }

    public void setWnewrelativet(Double wnewrelativet) {
        this.wnewrelativet = wnewrelativet;
    }

    public Double getNaratio() {
        return naratio;
    }

    public void setNaratio(Double naratio) {
        this.naratio = naratio;
    }

    public Double getNavolume() {
        return navolume;
    }

    public void setNavolume(Double navolume) {
        this.navolume = navolume;
    }

    public Double getSoutdrybulbt() {
        return soutdrybulbt;
    }

    public void setSoutdrybulbt(Double soutdrybulbt) {
        this.soutdrybulbt = soutdrybulbt;
    }

    public Double getSoutwetbulbt() {
        return soutwetbulbt;
    }

    public void setSoutwetbulbt(Double soutwetbulbt) {
        this.soutwetbulbt = soutwetbulbt;
    }

    public Double getSoutrelativet() {
        return soutrelativet;
    }

    public void setSoutrelativet(Double soutrelativet) {
        this.soutrelativet = soutrelativet;
    }

    public Double getWoutdrybulbt() {
        return woutdrybulbt;
    }

    public void setWoutdrybulbt(Double woutdrybulbt) {
        this.woutdrybulbt = woutdrybulbt;
    }

    public Double getWoutwetbulbt() {
        return woutwetbulbt;
    }

    public void setWoutwetbulbt(Double woutwetbulbt) {
        this.woutwetbulbt = woutwetbulbt;
    }

    public Double getWoutrelativet() {
        return woutrelativet;
    }

    public void setWoutrelativet(Double woutrelativet) {
        this.woutrelativet = woutrelativet;
    }

    public Integer getSectionl() {
        return sectionl;
    }

    public void setSectionl(Integer sectionl) {
        this.sectionl = sectionl;
    }

    public Double getResistance() {
        return resistance;
    }

    public void setResistance(Double resistance) {
        this.resistance = resistance;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getNostandard() {
        return nostandard;
    }

    public void setNostandard(String nostandard) {
        this.nostandard = nostandard == null ? null : nostandard.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season == null ? null : season.trim();
    }

    public String getDoortype() {
        return doortype;
    }

    public void setDoortype(String doortype) {
        this.doortype = doortype == null ? null : doortype.trim();
    }

    public String getDoordirection() {
        return doordirection;
    }

    public void setDoordirection(String doordirection) {
        this.doordirection = doordirection == null ? null : doordirection.trim();
    }

    public String getVavlem() {
        return vavlem;
    }

    public void setVavlem(String vavlem) {
        this.vavlem = vavlem == null ? null : vavlem.trim();
    }

    public Boolean getUvc() {
        return uvc;
    }

    public void setUvc(Boolean uvc) {
        this.uvc = uvc;
    }

    public String getActuator() {
        return actuator;
    }

    public void setActuator(String actuator) {
        this.actuator = actuator == null ? null : actuator.trim();
    }

    public Boolean getFac() {
        return fac;
    }

    public void setFac(Boolean fac) {
        this.fac = fac;
    }
}
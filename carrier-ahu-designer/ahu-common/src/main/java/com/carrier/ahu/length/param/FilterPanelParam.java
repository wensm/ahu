package com.carrier.ahu.length.param;

import lombok.Data;

/**
 * Created by LIANGD4 on 2017/12/12.
 */
@Data
public class FilterPanelParam {

    private String sectionType;//段code B:单层过滤段 c:综合过滤段 Y静电过滤段 V:高效过滤段
    private String serial;//机组型号
    private String mediaLoading;//抽取方式

    /*单层过滤器*/
    private String filterEfficiency;//过滤器选项 无过滤器：No
    private String fitetF;//过滤器形式 外抽:X

    /*综合过滤器*/
    private String RMaterial;//袋式 过滤器选项 无过滤器：No TODO No Filter
    private String LMaterial;//板式 过滤器选项 无过滤器：No

    /*静电过滤器：无选项*/
    private String filterType;//蜂巢式/板式（A板式，B蜂巢式）
    /*高效过滤器*/
    private String Supplier;//供应商 无过滤器:none
}

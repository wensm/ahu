package com.carrier.ahu.engine.heatrecycle;

import lombok.Data;

@Data
public class DRIBean {
    private double DRIoutSaEfficiencySummerTotal;
    private double DRIoutSaEfficiencyWinterTotal;
    private double DRIoutEfficiencySummerExhaustCondensation;
    private double DRIoutEfficiencySummerExhaustLatent;
    private double DRIoutTempEffectiveness;
    private double DRIoutHumEffectiveness;
    private double DRIoutSaPDropWinter;
    private double DRIoutSaPDropSummer;
    private double DRIoutSaVelocity;
    private double DRIoutRaVelocity;
    private String DRIOutECOFRESHModel;
}

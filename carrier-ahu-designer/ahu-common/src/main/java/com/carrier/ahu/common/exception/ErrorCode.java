package com.carrier.ahu.common.exception;

/**
 * AHU System Error Code.
 * <p>
 * Created by Braden Zhou on 2018/04/17.
 */
public enum ErrorCode {

    // @formatter:off
    // Common Exception (0~200)
    SUCCESS(0, "success"),
    INTERNAL_SERVER_ERROR(1, "internal_server_error"),

    // API Exception (201~400)
    PROJECT_ID_IS_EMPTY(201, "project_id_is_empty"),
    PROJECT_IS_NOT_EXIST(202, "project_is_not_exist"),
    GROUP_NAME_IS_OCCUPIED(203, "group_name_is_occupied"),
    GROUP_TYPE_NOT_DEFINED(204, "group_type_not_defined"),
    UNIT_MODEL_IS_EMPTY(205, "please_select_the_unit_model_first"),
    REQUEST_DATA_NO_SECTION(206, "request_data_no_section"),
    GROUP_ID_IS_EMPTY(207, "group_id_is_empty"),
    GROUP_IS_NOT_FOUND(208, "group_is_not_found"),
    UNIT_ID_IS_EMPTY(209, "unit_id_is_empty"),
    FAN_TYPE_IS_EMPTY(210, "fan_type_is_empty"),
    FAN_FILTER_TYPE_IS_EMPTY(211, "fan_filter_type_is_empty"),
    UNIT_MODEL_NOT_SELECTED(212, "unit_model_not_selected"),
    UNIT_SECTION_INFO_INCONSISTENT(213, "unit_section_info_inconsistent"),
    UNIT_DRAWING_NO_ILLEGAL(214, "unit_drawing_no_illegal"),
    UNIT_NOT_EXISTS(215, "unit_not_exists"),
    UNIT_QUANTITY_IS_ILLEGAL(216, "unit_quantity_is_illegal"),
    UNIT_PROJECT_NUMBER_IS_ILLEGAL(217, "unit_project_number_is_illegal"),
    NO_AHU_IN_GROUP(218, "no_ahu_in_group"),
    NO_SECTION_UNDER_UNIT(219, "no_section_under_unit"),
    REQUEST_DATA_ABNORMAL(220, "request_data_abnormal"),
    GROUP_CODE_NOT_FOUND(221, "group_code_not_found"),
    WRONG_FILE_FORMAT(222, "wrong_file_format"),
    RETURN_NULL_AFTER_EXCEL_ANALYSIS(223, "return_null_after_excel_analysis"),
    TARGET_PROJECT_NOT_EXIST(224, "target_project_not_exist"),
    IMPORT_PROJECT_ID_EMPTY(225, "import_project_id_empty"),
    IMPORT_PROJECT_ID_EXISTS(226, "import_project_id_exists"),
    IMPORT_GROUP_ID_EMPTY(227, "import_group_id_empty"),
    IMPORT_UNIT_ID_EMPTY(228, "import_unit_id_empty"),
    IMPORT_SECTION_ID_EMPTY(229, "import_section_id_empty"),
    UPLOAD_FILE_IS_EMPTY(230, "upload_file_is_empty"),
    UNIT_CODE_IS_EMPTY(231, "unit_code_is_empty"),
    SECTION_CODE_IS_EMPTY(232, "section_code_is_empty"),
    FILE_TYPE_EXCEPTION(233, "file_type_exception"),
    FILE_SAVING_ERROR(234, "file_saving_error"),
    IMAGE_DATA_IS_EMPTY(235, "image_data_is_empty"),
    IMAGE_DATA_FORMAT_ILLEGAL(236, "image_data_format_illegal"),
    CONVERT_SAVING_IMAGE_FAILURE(237, "convert_saving_image_failure"),
    DO_CALCULATION_BEFORE_CONFIRM(238, "do_calculation_before_confirm"),
    DO_CALCULATION_SUMMER_BEFORE_CONFIRM(239, "do_calculation_summer_before_confirm"),
    DO_CALCULATION_WINTER_BEFORE_CONFIRM(240, "do_calculation_winter_before_confirm"),
    GET_LICENSE_FAILED(241, "get_license_failed"),
    LICENSE_FILE_IS_INVALID(242, "license_file_is_invalid"),
    INSTALL_LICENSE_FILE_FAILED(243, "install_license_file_failed"),
    INSTALL_UPGRADE_FILE_FAILED(244, "install_upgrade_file_failed"),
    PRICE_CALCULATION_INCOMPLETE(245, "price_calculation_incomplete"),
    GROUP_CAN_ONLY_BE_SELECTED(246, "group_can_only_be_selected"),
    WET_FILM_NO_FOUND(247, "wet_film_no_found"),
    SECTION_LENGTH_SHOULD_NOT_EXCEED(248, "section_length_should_not_exceed"),
    PARTITION_VALIDATE_ACCESS_LENGH(249, "partition_validate_access_lengh_msg"),
    PARTITION_VALIDATE_CTR_EXIST(250, "partition_validate_ctr_exist_msg"),
    PARTITION_VALIDATE_LEN(251, "partition_validate_len"),
    PARTITION_VALIDATE_COMBINEDMIXINGCHAMBER_SPLIT(252, "partition_validate_combinedmixingchamber_split_msg"),
    PARTITION_VALIDATE_DOOR(253, "partition_validate_door_msg"),
    SECTION_CANNOT_BE_DISMANTLED(254, "section_cannot_be_dismantled"),
    PARTITION_VALIDATE_HEAT_RECYCLE(255, "partition_validate_heat_recycle_msg"),
    PARTITION_VALIDATE_LEN_TOTAL(256, "partition_validate_len_total"),
    CAN_NOT_BE_FOLLOWED_BY(257, "can_not_be_followed_by"),
    CAN_NOT_BE_AS_FIRST_SECTION(258, "can_not_be_as_first_section"),
    NORMAL_UNIT_EXCEED_HEIGHT_NEED_CKD(259, "normal_unit_exceed_height_need_ckd"),
    UNIT_WITH_TOP_DAMPER_EXCEED_HEIGHT_NEED_CKD(260, "unit_with_top_damper_exceed_height_need_ckd"),
    UNIT_WITH_ROOF_EXCEED_HEIGHT_NEED_CKD(261, "unit_with_roof_exceed_height_need_ckd"),
    UNIT_WITH_TOP_DAMPER_AND_ROOF_EXCEED_HEIGHT_NEED_CKD(262, "unit_with_top_damper_and_roof_exceed_height_need_ckd"),
    PARTITION_LENGHT_BETWEEN_21M_TO_27M_NEED_CKD_OR_SPECIAL_CONTAINER(263, "partition_lenght_between_21m_to_27m_need_ckd_or_special_container"),
    PARTITION_LENGHT_GREATER_THAN_27M_NEED_CKD(264, "partition_lenght_greater_than_27m_need_ckd"),
    PARTITION_LENGTH_GREATER_THAN_18M_NEED_TO_INSTALL_ONSITE(265, "partition_length_greater_than_18m_need_to_install_onsite"),
    PARTITION_LENGTH_NO_LESS_THAN(266, "partition_length_no_less_than"),
    QUANTITY_OF_HEAT_IS_TOO_BIG_TRY_AGAIN(267, "quantity_of_heat_is_too_big_try_again"),
    EXPORT_UNIT_WITH_TOP_DAMPER_NEED_CKD(268, "export_unit_with_top_damper_need_ckd"),
    PARTITION_NUMBER_IS_EMPTY(269, "partition_number_is_empty"),
    PRICE_CALCULATION_FAILED(270, "price_calculation_failed"),
    PRICE_CALCULATION_ERROR(271, "price_calculation_error"),
    PRICE_CALCULATION_WITHOUT_ERROR(272, "price_calculation_without_error"),
    DO_CALCULATION_BEFORE_SELECT_CONFIRM(273, "do_calculation_before_select_confirm"),
    PANEL_INIT_ERROR(274, "panel_init_error"),
    INVALID_UPGRADE_FILE(275, "invalid_upgrade_file"),
    PANEL_INITAB_ERROR(276, "panel_initab_error"),
    PANEL_MERGEPANEL_ERROR(277, "panel_mergePanel_error"),
    INVALID_PROJECT_FILE(278, "invalid_project_file"),
    UNIT_PO_EXISTS(279, "unit_po_exists"),
    PRODUCT_IS_EMPTY(280, "product_is_empty"),
    PROJECT_NAME_IS_EMPTY(281, "project_name_is_empty"),
    INVALID_DAMPER_PANEL_MARGIN(282, "invalid_damper_panel_margin"),
    INVALID_DAMPER_BLADE_SIZE(283, "invalid_damper_blade_size"),
    DAMPER_FAN_SPEED_EXCEED_THRESHOLD(284, "damper_fan_speed_exceed_threshold"),
    CAN_NOT_BE_AS_FIRST_SECTION_G_XT_COMBINEDFILTER(285, "can_not_be_as_first_section_g_xt_combinedfilter"),
    CAN_NOT_BE_AS_FIRST_SECTION_CQ_MORE2532_COMBINEDFILTER(286, "can_not_be_as_first_section_cq_more2532_combinedfilter"),
    CAN_NOT_BE_AS_FIRST_SECTION__MORE2532_SINGLE(287, "can_not_be_as_first_section__more2532_single"),
    INVALID_SECTION_META_OPTION(288, "invalid_section_meta_option"),
    PARTITION_VALIDATE_HEAT_RECYCLE_STABILITY(289, "partition_validate_heat_recycle_stability_msg"),
    NEW_UNIT_PARTITION_VALIDATE_LEN(290, "new_unit_partition_validate_len"),
    UNIT_PARTITION_VALIDATE_LEN(291, "unit_partition_validate_len"),
    PARTITION_LENGTH_LIMITED_BY_FACEDAMPER_NUM1(292,"partition_length_limited_by_facedamper_num_1"),
    PARTITION_LENGTH_LIMITED_BY_FACEDAMPER_NUM2(293,"partition_length_limited_by_facedamper_num_2"),
    PRICE_CALCULATION_FAILED_BY_NS(294, "price_calculation_failed_by_ns"),
    TOTAL_STATIC_TOO_BIGER(295, "total_static_too_biger"),
    HAS_SECTION_NO_NS_PRICE(296, "has_section_no_ns_price"),
    PRICE_NS_DRAINPANMATERIAL_CALCULATION_FAILED(297, "price_ns_drainpanmaterial_calculation_failed"),
    NEW_UNIT_PARTITION_VALIDATE_LEN_MIN(298, "new_unit_partition_validate_len_min"),
    PARTITION_VALIDATE_LAYOUT_JSON_NULL_MSG(299, "partition_validate_layout_json_null_msg"),
    // Report Exception  (401~600)

    // Calculation Exception  (601~800)
    SUITABLE_FAN_NOT_FOUND(601, "suitable_fan_not_found"),

    // Engine Exception  (801~1000)
    RECYCLE_AVOLUME_SOBIG(801, "recycle_avolume_sobig"),
    RECYCLE_PARAM_EQUAL(802, "recycle_param_equal"),
    RECYCLE_HEATX_RANGE(803, "recycle_heatX_range"),
    RECYCLE_INCLUDE_FAN(804, "recycle_include_fan"),
    RECYCLE_INCLUDE_FILTER(805, "recycle_include_filter"),
    COIL_NEED_AFTER_RECYCLE(806, "coil_need_after_recycle"),
    COIL_NOT_CERTIFIED_AHRI(807, "coil_not_certified_ahri"),
    COIL_NOT_CERTIFIED_AHRI_410(808, "coil_not_certified_ahri_410"),
    COIL_NOT_FIND_RESULT(809, "coil_not_find_result"),
    COIL_CALCULATION_PARAM_WRONG(810, "coil_calculation_param_wrong"),
    IMPORT_VERSION_NOT_EQUALS(811, "import_version_not_equals"),
    KRUGER_SELECT_FAILED(812, "kruger_select_failed"),
    KRUGER_CURVE_POINTS_FAILED(813, "kruger_curve_points_failed"),
    KRUGER_SOUND_SPECTRUM_EX_FAILED(814, "kruger_sound_spectrum_ex_failed"),
    KRUGER_GENERATE_CHART_FAILED(815, "kruger_generate_chart_failed"),
    HUMIDIFICATION_TOO_MUCH_WITH_MAX_HUMIDIFICATION_FAILED(816, "humidification_too_much_with_max_humidification"),

    COIL_NOT_CERTIFIED_AHRI_FACE_VELOCITY(816, "coil_not_certified_ahri_face_velocity"),
    COIL_NOT_CERTIFIED_AHRI_INDRYBULBT(817, "coil_not_certified_ahri_indrybulbt"),
    COIL_NOT_CERTIFIED_AHRI_INWETBULBT(818, "coil_not_certified_ahri_inwetbulbt"),
    COIL_NOT_CERTIFIED_AHRI_FLUID_VELOCITY(819, "coil_not_certified_ahri_fluid_velocity"),
    COIL_NOT_CERTIFIED_AHRI_ENTERING_FLUID_TEM(820, "coil_not_certified_ahri_entering_fluid_tem"),
    COIL_NOT_CERTIFIED_AHRI_FLUID_NOT_WATER(821, "coil_not_certified_ahri_fluid_not_water"),

    ADJUST_DAMPER_SIZE_FAILED(822, "adjust_damper_size_failed"),
    ;

    // @formatter:on

    private final Integer code;
    private final String message;
    // private final HttpStatus status;

    ErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static ErrorCode parseCode(int code) {
        for (ErrorCode resultCode : ErrorCode.values()) {
            if (resultCode.code == code) {
                return resultCode;
            }
        }
        return null;
    }

}
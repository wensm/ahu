package com.carrier.ahu.engine.price;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Created by Braden Zhou on 2018/11/12.
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface AhuPrice {

    /**
     * version string, for example, 20190101, 2020010
     * 
     * @return
     */
    public String value() default "";

}

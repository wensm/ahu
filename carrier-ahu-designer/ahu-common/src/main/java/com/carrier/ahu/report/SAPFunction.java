package com.carrier.ahu.report;

import lombok.Data;

/**
 * Created by Braden Zhou on 2018/08/31.
 */
@Data
public class SAPFunction {

    private String funcName;
    private String funcCode;
    private int funcList;
    private int instSection;
    private String sectionPosition;
    private String sectionWeight;
    private String specLabor;
    private String mark;

}

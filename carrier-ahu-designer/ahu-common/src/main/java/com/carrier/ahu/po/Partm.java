package com.carrier.ahu.po;

public class Partm {
    private String pid;

    private String partid;

    private String deadening;

    private Integer sectionl;

    private Double weight;

    private Double price;

    private Double resistance;

    private String nonstandard;

    private String memo;

    private String clearstype;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getPartid() {
        return partid;
    }

    public void setPartid(String partid) {
        this.partid = partid == null ? null : partid.trim();
    }

    public String getDeadening() {
        return deadening;
    }

    public void setDeadening(String deadening) {
        this.deadening = deadening == null ? null : deadening.trim();
    }

    public Integer getSectionl() {
        return sectionl;
    }

    public void setSectionl(Integer sectionl) {
        this.sectionl = sectionl;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getResistance() {
        return resistance;
    }

    public void setResistance(Double resistance) {
        this.resistance = resistance;
    }

    public String getNonstandard() {
        return nonstandard;
    }

    public void setNonstandard(String nonstandard) {
        this.nonstandard = nonstandard == null ? null : nonstandard.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getClearstype() {
        return clearstype;
    }

    public void setClearstype(String clearstype) {
        this.clearstype = clearstype == null ? null : clearstype.trim();
    }
}
package com.carrier.ahu.engine.dxcoil;


import com.carrier.ahu.unit.BaseDataUtil;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.ShortByReference;

import static com.carrier.ahu.engine.dxcoil.DXUtils.dRef;


public class DXCoilEngine {

    public static DXCoilResult DX_calculate(double velFace, double tdbAirIn, double twbAirIn, double pAirIn, int refindex,
                                            double dtsh, double sst, double sct, double tliq, int idllunits,
                                            double zua, double zdpr, double zdpa, int itube, int nface,
                                            double ptube, double tlenx, double dotube, double ttube, int nrow,
                                            double prow, int ncur, double dfin, double tfin, int nciri,
                                            int nciro, int ntubciro, int istck, int idflow) {

        System.out.print(" velFace: " + velFace);
        System.out.print(" tdbAirIn: " + tdbAirIn);
        System.out.print(" twbAirIn: " + twbAirIn);
        System.out.print(" pAirIn: " + pAirIn);
        System.out.println("refindex: " + refindex);
        System.out.print(" dtsh :" + dtsh);
        System.out.print(" sst: " + sst);
        System.out.print(" sct: " + sct);
        System.out.print(" tliq: " + tliq);
        System.out.println(" idllunits: " + idllunits);
        System.out.print(" zua: " + zua);
        System.out.print(" zdpr: " + zdpr);
        System.out.print(" zdpa: " + zdpa);
        System.out.print(" itube: " + itube);
        System.out.println(" nface: " + nface);
        System.out.print(" ptube: " + ptube);
        System.out.print(" tlenx: " + tlenx);
        System.out.print(" dotube: " + dotube);
        System.out.print(" ttube: " + ttube);
        System.out.println("nrow: " + nrow);
        System.out.print(" prow: " + prow);
        System.out.print(" ncur: " + ncur);
        System.out.print(" dfin: " + dfin);
        System.out.print(" tfin: " + tfin);
        System.out.println(" nciri: " + nciri);
        System.out.print(" nciro: " + nciro);
        System.out.print(" ntubciro: " + ntubciro);
        System.out.print(" istck: " + istck);
        System.out.print(" idflow: " + idflow);
        sst = 4.0;//特殊字段处理
        DXCoilResult result = new DXCoilResult();
        DXCoilLib.DX_INSTANCE.RUNEVAPAIR1_DLL(
                dRef(velFace), dRef(tdbAirIn), dRef(twbAirIn), dRef(pAirIn), new ShortByReference(BaseDataUtil.integerConversionShort(refindex)),
                dRef(dtsh), dRef(sst), dRef(sct), dRef(tliq), dRef(idllunits),
                dRef(zua), dRef(zdpr), new ShortByReference(BaseDataUtil.doubleConversionShort(zdpa)), new IntByReference(itube), new IntByReference(nface),
                dRef(ptube), dRef(tlenx), dRef(dotube), dRef(ttube), dRef(nrow),
                dRef(prow), new IntByReference(ncur), dRef(dfin), dRef(tfin), new IntByReference(nciri),
                new IntByReference(nciro), dRef(ntubciro), dRef(istck), dRef(idflow),
                result.capTotal, result.capSensible, result.dpAir, result.dpRef, result.tdbAirOut, result.twbAirOut, result.wmrRef, result.errorFlag);
        return result;
    }

    /*public static DXCoilResult DX_calculate(double velFace, double tdbAirIn, double twbAirIn, double pAirIn, int refindex,
                                            double dtsh, double sst, double sct, double tliq, int idllunits,
                                            double zua, double zdpr, double zdpa, int itube, int nface,
                                            double ptube, double tlenx, double dotube, double ttube, int nrow,
                                            double prow, int ncur, double dfin, double tfin, int nciri,
                                            int nciro, int ntubciro, int istck, int idflow) {
        DXCoilResult result = new DXCoilResult();
        DXCoilLib.DX_INSTANCE.RUNEVAPAIR1_DLL(
                dRef(2.535365),
                dRef(31.7),
                dRef(25.3),
                dRef(101.0398),
                new ShortByReference(Short.valueOf("2")),
                dRef(5.55556),
                dRef(4.0),
                dRef(60),
                dRef(55),
                dRef(1),
                dRef(1),
                dRef(1),
                new ShortByReference(Short.valueOf("1")),
                new IntByReference(101),
                new IntByReference(16),
                dRef(31.75),
                dRef(889),
                dRef(13.208),
                dRef(0.4064),
                dRef(10),
                dRef(19.8374),
                new IntByReference(96),
                dRef(10),
                dRef(0.11989),
                new IntByReference(16),
                new IntByReference(16),
                dRef(0),
                dRef(0),
                dRef(0),
                result.capTotal, result.capSensible, result.dpAir, result.dpRef, result.tdbAirOut, result.twbAirOut, result.wmrRef, result.errorFlag);
        return result;
    }*/

}

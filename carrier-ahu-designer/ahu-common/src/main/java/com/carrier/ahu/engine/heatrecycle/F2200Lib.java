package com.carrier.ahu.engine.heatrecycle;

import com.carrier.ahu.util.EmptyUtil;
import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;

public class F2200Lib {

    private ActiveXComponent dotnetCom = new ActiveXComponent(
            "CLSID:bdf6c835-61ba-44a8-a887-afc66e9c0cf2");

    private F2200Lib() {
    }

    private static F2200Lib lib = new F2200Lib();

    public static F2200Lib getInstance() {
        if (lib == null) {
            lib = new F2200Lib();
        }
        return lib;
    }

    private void setDRIinputModel(final double model) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputModel", new Double(model));
    }

    private double getDRIoutEfficiencySummerExhaustTotal() throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutEfficiencySummerExhaustTotal").getDouble();
    }
    
    private double getDRIoutEfficiencySummerExhaustCondensation() throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutEfficiencySummerExhaustCondensation").getDouble();
    }
    
    private double getDRIoutEfficiencySummerExhaustLatent() throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutEfficiencySummerExhaustLatent").getDouble();
    }

    private double getDRIoutTotalEffectiveness() throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutTotalEffectiveness").getFloat();
    }
    
    private double getDRIoutTempEffectiveness () throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutTempEffectiveness").getFloat();
    }
    private double getDRIoutHumEffectiveness() throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutHumEffectiveness").getFloat();
    }

    private double getDRIoutPDActualExhaustW() throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutSaPDrop").getDouble();
    }
    
    private double getDRIoutPDActualExhaustS() throws Exception {
        return Dispatch.call(dotnetCom, "DRIoutSaPDrop").getDouble();
    }

    private String getDRIOutECOFRESHModel() throws Exception {
        return Dispatch.call(dotnetCom, "DRIOutECOFRESHModel").getString();
    }

    private void setDRIunitSI(final boolean si) throws Exception {
        Dispatch.put(dotnetCom, "DRIunitSI", new Boolean(si));
    }

    private void setDRIinputSupplyAir(final double supplyAir) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputSupplyAir", new Double(supplyAir));
    }

    private void setDRIinputReturnAir(final double returnAir) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputReturnAir", new Double(returnAir));
    }

    private void setDRIinputOAS_DBT(final double oas_dbt) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputOAS_DBT", new Double(oas_dbt));
    }

    private void setDRIinputOAS_WBT(final double oas_wbt) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputOAS_WBT", new Double(oas_wbt));
    }

    private void setDRIinputRaS_DBT(final double raS_dbt) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputRaS_DBT", new Double(raS_dbt));
    }

    private void setDRIinputRaS_RH(final double raS_rh) throws Exception {
        Dispatch.put(dotnetCom, "DRIinputRaS_RH", new Double(raS_rh));
    }
    
    private double getDRIoutSaVelocity() throws Exception {
    	 return Dispatch.call(dotnetCom, "DRIoutSaVelocity").getDouble();
    }
    
    private double getDRIoutRaVelocity() throws Exception {
    	return Dispatch.call(dotnetCom, "DRIoutRaVelocity").getDouble();
    }

//    private void setDRIwheelSensible(final boolean wheelSensible) throws Exception {
//        Dispatch.put(dotnetCom, "DRIwheelSensible", new Boolean(wheelSensible));
//    }

    //计算
    private void calculateOutput() throws Exception {
        Dispatch.call(dotnetCom, "CalculateOutput");
    }

    private void calculateOutputModel() throws Exception {
        Dispatch.call(dotnetCom, "CalculateOutputModel");
    }

    public F2200Bean calculate(boolean DRIunitSI, double DRIinputSupplyAir, double DRIinputReturnAir, double DRIinputOAS_DBT, double DRIinputOAS_WBT,
                               double DRIinputRaS_DBT, double DRIinputRaS_RH, boolean DRIwheelSensible, Double DRIinputModel) throws Exception {

        try {
            ComThread.InitSTA();
            dotnetCom = new ActiveXComponent("CLSID:bdf6c835-61ba-44a8-a887-afc66e9c0cf2");
            lib.setDRIunitSI(DRIunitSI);
            lib.setDRIinputSupplyAir(DRIinputSupplyAir);
            lib.setDRIinputReturnAir(DRIinputReturnAir);
            lib.setDRIinputOAS_DBT(DRIinputOAS_DBT);
            lib.setDRIinputOAS_WBT(DRIinputOAS_WBT);
            lib.setDRIinputRaS_DBT(DRIinputRaS_DBT);
            lib.setDRIinputRaS_RH(DRIinputRaS_RH);
//            lib.setDRIwheelSensible(DRIwheelSensible);
            if (EmptyUtil.isEmpty(DRIinputModel)) {
                lib.calculateOutput();
            } else {
                lib.setDRIinputModel(DRIinputModel);
                lib.calculateOutputModel();
            }
            F2200Bean f2200Bean = new F2200Bean();
            f2200Bean.setDRIoutSaEfficiencySummerTotal(lib.getDRIoutEfficiencySummerExhaustTotal());
            f2200Bean.setDRIoutSaEfficiencyWinterTotal(lib.getDRIoutTotalEffectiveness());
            f2200Bean.setDRIoutEfficiencySummerExhaustCondensation(lib.getDRIoutEfficiencySummerExhaustCondensation());;
            f2200Bean.setDRIoutEfficiencySummerExhaustLatent(lib.getDRIoutEfficiencySummerExhaustLatent());
            f2200Bean.setDRIoutTempEffectiveness(lib.getDRIoutTempEffectiveness());
            f2200Bean.setDRIoutHumEffectiveness(lib.getDRIoutHumEffectiveness());
            f2200Bean.setDRIoutSaPDropWinter(lib.getDRIoutPDActualExhaustW());
            f2200Bean.setDRIoutSaPDropSummer(lib.getDRIoutPDActualExhaustS());
            f2200Bean.setDRIOutECOFRESHModel(lib.getDRIOutECOFRESHModel());
            f2200Bean.setDRIoutSaVelocity(lib.getDRIoutSaVelocity());
            f2200Bean.setDRIoutRaVelocity(lib.getDRIoutRaVelocity());
            return f2200Bean;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            ComThread.Release();
        }
        return null;
    }

    public static void main(String args[]) {

        try {
            System.out.println("begin");
//            MS270Lib.getInstance().calculate(1300);
            System.out.println("success");

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}

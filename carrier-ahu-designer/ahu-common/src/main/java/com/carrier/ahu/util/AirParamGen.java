package com.carrier.ahu.util;

import com.carrier.ahu.po.meta.AirParam;
import com.carrier.ahu.po.meta.AirParam.ParamType;
import com.carrier.ahu.po.meta.AirParam.SeasonType;

import java.util.ArrayList;
import java.util.List;

public class AirParamGen {
	
	public static List<AirParam>  getAirParam(){
		List<AirParam> airParams = new ArrayList<AirParam>();
		//夏季新风
		AirParam sumFreshAirParam = new AirParam(SeasonType.SUMMER.name(), ParamType.FRESHAIR.name(), "27", "19", "22");
		airParams.add(sumFreshAirParam);
		//夏季回风
		AirParam sumReturnAirParam = new AirParam(SeasonType.SUMMER.name(), ParamType.RETURNAIR.name(), "25", "17.2", "22.3");
		airParams.add(sumReturnAirParam);
		//夏季出风
		AirParam sumOutAirParam = new AirParam(SeasonType.SUMMER.name(), ParamType.OUTAIR.name(), "25", "17.2", "22.3");
		airParams.add(sumOutAirParam);
		
		//冬季新风
		AirParam winFreshAirParam = new AirParam(SeasonType.SUMMER.name(), ParamType.FRESHAIR.name(), "20", "14", "17");
		airParams.add(winFreshAirParam);
		//冬季回风
		AirParam winReturnAirParam = new AirParam(SeasonType.SUMMER.name(), ParamType.RETURNAIR.name(), "18", "13.2", "15.6");
		airParams.add(winReturnAirParam);
		//冬季出风
		AirParam winOutAirParam = new AirParam(SeasonType.SUMMER.name(), ParamType.OUTAIR.name(), "19", "15.2", "17.7");
		airParams.add(winOutAirParam);
		return airParams;
	}
	
}

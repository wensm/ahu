package com.carrier.ahu.engine.heatrecycle;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.win32.StdCallFunctionMapper;
import com.sun.jna.win32.StdCallLibrary;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static com.carrier.ahu.constant.CommonConstant.SYS_PATH_YUFENGDLL;

/**
 * Yufeng Dll Tester
 */
public class YuFengLib implements StdCallLibrary {

    static {
        Map<String, StdCallFunctionMapper> options = new HashMap<String, StdCallFunctionMapper>();
        options.put(Library.OPTION_FUNCTION_MAPPER,
                new StdCallFunctionMapper() {
                    HashMap<String, String> map = new HashMap<String, String>() {
                        private static final long serialVersionUID = -8719854872616221602L;

                        {
                            /*接口计算*/
                            put("SetWorkStatus", "?SetWorkStatus@@YAXHHNHH@Z");
                            put("CalcSummerCondition", "?CalcSummerCondition@@YGHXZ");
                            put("CalcWinterCondition", "?CalcWinterCondition@@YGHXZ");

                            /*夏季*/
                            put("SetSumParam", "?SetSumParam@@YAXNNNNNN@Z");
                            put("GetSumPreDropOA", "?GetSumPreDropOA@@YGNXZ");
                            put("GetSumPreDropRA", "?GetSumPreDropRA@@YGNXZ");
                            put("GetSumAirflowSA", "?GetSumAirflowSA@@YGNXZ");
                            put("GetSumAirflowEA", "?GetSumAirflowEA@@YGNXZ");
                            put("GetSumDrybulbSA", "?GetSumDrybulbSA@@YGNXZ");
                            put("GetSumDrybulbEA", "?GetSumDrybulbEA@@YGNXZ");
                            put("GetSumRHSA", "?GetSumRHSA@@YGNXZ");
                            put("GetSumRHEA", "?GetSumRHEA@@YGNXZ");
                            put("GetSumQuanreXiaolv", "?GetSumQuanreXiaolv@@YAXAAN0@Z");
                            put("GetSumVelocityOA", "?GetSumVelocityOA@@YGNXZ");
                            put("GetSumVelocityRA", "?GetSumVelocityRA@@YGNXZ");

                            /*冬季*/
                            put("SetWinParam", "?SetWinParam@@YAXNNNNNN@Z");
                            put("GetWinPreDropOA", "?GetWinPreDropOA@@YGNXZ");
                            put("GetWinPreDropRA", "?GetWinPreDropRA@@YGNXZ");
                            put("GetWinAirflowSA", "?GetWinAirflowSA@@YGNXZ");
                            put("GetWinAirflowEA", "?GetWinAirflowEA@@YGNXZ");
                            put("GetWinDrybulbSA", "?GetWinDrybulbSA@@YGNXZ");
                            put("GetWinDrybulbEA", "?GetWinDrybulbEA@@YGNXZ");
                            put("GetWinRHSA", "?GetWinRHSA@@YGNXZ");
                            put("GetWinRHEA", "?GetWinRHEA@@YGNXZ");
                            put("GetWinQuanreXiaolv", "?GetWinQuanreXiaolv@@YAXAAN0@Z");
                            put("GetWinVelocityOA", "?GetWinVelocityOA@@YGNXZ");
                            put("GetWinVelocityRA", "?GetWinVelocityRA@@YGNXZ");

                        }
                    };

                    // Override
                    public String getFunctionName(NativeLibrary library,Method method) {
                        String methodName = method.getName();
                        return map.get(methodName);
                    }
                });

        Native.register(NativeLibrary.getInstance(SYS_PATH_YUFENGDLL, options));
    }

    /**
     * dBV21：夏季回风风量
     * dBV11：夏季新风风量
     * dDB21：夏季回风进口干球温度
     * dDB11：夏季新风进口干球温度
     * dRH21：夏季回风进口相对湿度
     * dRH11：夏季新风进口相对湿度
     */
    public static native void SetSumParam(double dBV21, double dBV11, double dDB21, double dDB11, double dRH21, double dRH11);

    /**
     * dBV21：冬季回风风量
     * dBV11：冬季新风风量
     * dDB21：冬季回风进口干球温度
     * dDB11：冬季新风进口干球温度
     * dRH21：冬季回风进口相对湿度
     * dRH11：冬季新风进口相对湿度
     */
    public static native void SetWinParam(double dBV21, double dBV11, double dDB21, double dDB11, double dRH21, double dRH11);

    /**
     * nDiameter：转轮直径
     * nWorkStatus：转轮类型，全热为0，显热为1
     * dPSI：海拔高度
     * nRotorType：转轮类型，270为0, 200为1
     * nPurge：清洁扇  w/o purge为0，w/purge为1
     */
    public static native void SetWorkStatus(int nDiameter, int nWorkStatus, double dPSI, int nRotorType, int nPurge);


    //获得入口新风压力降
    public static native double GetSumPreDropOA();
    public static native double GetWinPreDropOA();

    //获得入口回风压力降
    public static native double GetSumPreDropRA();
    public static native double GetWinPreDropRA();

    //获得出口送风风量
    public static native double GetSumAirflowSA();
    public static native double GetWinAirflowSA();

    //获得出口排风风量
    public static native double GetSumAirflowEA();
    public static native double GetWinAirflowEA();

    //获得出口送风干球温度
    public static native double GetSumDrybulbSA();
    public static native double GetWinDrybulbSA();

    //获得出口排风干球温度
    public static native double GetSumDrybulbEA();
    public static native double GetWinDrybulbEA();

    //获得出口送风相对湿度
    public static native double GetSumRHSA();
    public static native double GetWinRHSA();

    //获得出口排风相对湿度
    public static native double GetSumRHEA();
    public static native double GetWinRHEA();

    //获得全热效率
    public static native void GetSumQuanreXiaolv(DoubleByReference dXin, DoubleByReference dHui);
    public static native void GetWinQuanreXiaolv(DoubleByReference dXin, DoubleByReference dHui);

    //获得入口新风迎面风速
    public static native double GetSumVelocityOA();
    public static native double GetWinVelocityOA();

    //获得入口回风迎面风速
    public static native double GetSumVelocityRA();
    public static native double GetWinVelocityRA();

    //返回信息列表
    public static native int CalcSummerCondition();
    public static native int CalcWinterCondition();

    /*返回信息列表*/
    //0  表示计算完成
    //1	新风温度请设置在-40—50之间
    //2	冬季新风温度必须低于回风温度
    //3	夏季新风温度必须高于回风温度
    //4	相对湿度请设置在0—100之间
    //5	迎面风速不在1.0-6m/s范围内
    //6	绝对湿度应小于85g/kg
    //7	风量比不在0.6-2.0之间,建议使用旁通
    //8	此种应用可能有结霜危险
    //9	工况参数输入有误

    public static void main(String[] args) {
        SetSumParam(15000.0, 15000.0, 35.0, 40.0, 22.0, 13);
        SetWorkStatus(1400, 1, 0, 0, 0);
        System.out.println(CalcSummerCondition());
        System.out.println(GetSumPreDropOA());
        System.out.println(GetSumVelocityRA());
        System.out.println(GetSumAirflowEA());
        DoubleByReference dXin = new DoubleByReference(0.0);
        DoubleByReference dHui = new DoubleByReference(0.0);
        GetSumQuanreXiaolv(dXin,dHui);
        System.out.println(dXin.getValue());
        System.out.println(dHui.getValue());
    }
}
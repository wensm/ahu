package com.carrier.ahu.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.carrier.ahu.common.entity.GroupType;

/**
 * Created by Simon on 2018-01-27.
 */
public interface GroupTypeDao extends PagingAndSortingRepository<GroupType,String>,JpaSpecificationExecutor<GroupType> {
}

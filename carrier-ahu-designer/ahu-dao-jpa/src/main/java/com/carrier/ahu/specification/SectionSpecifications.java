package com.carrier.ahu.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.carrier.ahu.common.entity.Part;

/**
 * Created by Braden Zhou on 2018/09/29.
 */
public final class SectionSpecifications {

    /**
     * Filter parts based on unit id and order by position.
     * 
     * @param unitid
     * @return
     */
    public static Specification<Part> sectionsOfUnit(String unitid) {
        return new Specification<Part>() {
            @Override
            public Predicate toPredicate(Root<Part> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                query.where(cb.equal(root.get("unitid"), unitid)).orderBy(cb.asc(root.get("position")));
                return null;
            }
        };
    }

}

package com.carrier.ahu.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.carrier.ahu.common.entity.Factory;

/**
 * @author Simon
 */
public interface FactoryDao extends PagingAndSortingRepository<Factory, String>, JpaSpecificationExecutor<Factory> {
}

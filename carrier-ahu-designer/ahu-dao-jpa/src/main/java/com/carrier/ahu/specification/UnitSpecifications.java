package com.carrier.ahu.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.carrier.ahu.common.entity.Unit;

/**
 * Created by Braden Zhou on 2019/08/07.
 */
public final class UnitSpecifications {

    public static Specification<Unit> unitsOfProject(String projectId) {
        return new Specification<Unit>() {
            @Override
            public Predicate toPredicate(Root<Unit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                query.where(cb.equal(root.get("pid"), projectId));
                return null;
            }
        };
    }

}

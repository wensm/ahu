package com.carrier.ahu.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.carrier.ahu.common.entity.AhuGroupBind;
import com.carrier.ahu.common.entity.GroupInfo;

/**
 * Created by Braden Zhou on 2019/08/07.
 */
public final class GroupSpecifications {

    public static Specification<GroupInfo> groupsOfProject(String projectId) {
        return new Specification<GroupInfo>() {
            @Override
            public Predicate toPredicate(Root<GroupInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                query.where(cb.equal(root.get("projectId"), projectId));
                return null;
            }
        };
    }

    public static Specification<AhuGroupBind> groupBindOfGroup(String groupId) {
        return new Specification<AhuGroupBind>() {
            @Override
            public Predicate toPredicate(Root<AhuGroupBind> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                query.where(cb.equal(root.get("groupid"), groupId));
                return null;
            }
        };
    }

}

package com.carrier.ahu.wpd;

import static com.carrier.ahu.util.NumberUtil.scale;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class WaterDropMacroTest {

    @Test
    public void testRun4ClicksInSequence() {
        WaterDropDetail detail = WaterDropMacro.run4ClicksInSequence("", 12.7, 65, 22, 988, 22, 3, 1.908, 55.1);
        assertEquals("0.36", String.valueOf(scale(detail.getTotal(), 2)));

        detail = WaterDropMacro.run4ClicksInSequence("", 12.7, 65, 22, 988, 22, 3, 30.81, 55.1);
        assertEquals("70.07", String.valueOf(scale(detail.getTotal(), 2)));

        detail = WaterDropMacro.run4ClicksInSequence("", 12.7, 65, 22, 988, 22, 3, 30.81, 20.1);
        assertEquals("74.31", String.valueOf(scale(detail.getTotal(), 2)));
    }

    @Test
    public void testCalculate() {
        WaterDropDetail detail = WaterDropMacro.calculate("", 12.7, 65, 26, 1368, 26, 8, 40.327, 9);
        assertEquals("173.71", String.valueOf(scale(detail.getTotal(), 2)));

        detail = WaterDropMacro.calculate("", 12.7, 65, 26, 1368, 26, 8, 20.12, 9);
        assertEquals("48.25", String.valueOf(scale(detail.getTotal(), 2)));

        detail = WaterDropMacro.calculate("", 12.7, 65, 26, 1368, 26, 8, 30.45, 18);
        assertEquals("99.53", String.valueOf(scale(detail.getTotal(), 2)));
    }

}

package com.carrier.ahu.po.model.partition;

import com.carrier.ahu.util.partition.AhuPartitionGeneratorUtil;
import org.junit.Test;

import com.carrier.ahu.common.model.partition.PanelFace;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.google.gson.Gson;

/**
 * Created by liujianfeng on 2017/9/1.
 */
public class AhuPartitionTest {

	@Test
	public void testPartitionJson() {
		// Unit unit = AhuPartitionGenerator.generateUnitStringWithPartition();
		// System.out.println(unit.getPartitionJson());
	}

	@Test
	public void testDevidePanelByRule01() {
		Gson gson = new Gson();
		PanelFace panelFace = AhuPartitionGeneratorUtil.createPanelFace(44, 44);
		panelFace.setId("2");
		System.out.println("The initial json");
		System.out.println(gson.toJson(panelFace));
		// 测试代码，rule需要从excel中读取
		Integer[] horizontals = new Integer[] { 44 };
		Integer[] verticals = new Integer[] { 13, 14, 17 };
		Integer[] horizontalTypes = new Integer[] { 0 };
		Integer[] verticalTypes = new Integer[] { 0, 0, 0 };
		AhuPartitionGeneratorUtil.slicePanelByRule(panelFace, horizontals, horizontalTypes, verticals, verticalTypes);
		System.out.println("The final json");
		System.out.println(gson.toJson(panelFace));
		System.out.println("The shuffled Panel json");
		AhuPartitionGeneratorUtil.slicePanelByBitreeRule(panelFace, "");
		System.out.println(gson.toJson(panelFace));
		System.out.println();
	}

	@Test
	public void testDevidePanelByRule02() {
		Gson gson = new Gson();
		PanelFace panelFace = AhuPartitionGeneratorUtil.createPanelFace(44, 44);
		panelFace.setId("0_1");
		System.out.println("The initial json");
		System.out.println(gson.toJson(panelFace));
		// 测试代码，rule需要从excel中读取
		Integer[] horizontals = new Integer[] {};
		Integer[] verticals = new Integer[] { 9, 9, 9, 9, 8 };
		Integer[] horizontalTypes = new Integer[] { 0, 0, 0 };
		Integer[] verticalTypes = new Integer[] { 0, 0, 0, 0, 0 };
		AhuPartitionGeneratorUtil.slicePanelByRule(panelFace, horizontals, horizontalTypes, verticals, verticalTypes);
		System.out.println("The final json");
		System.out.println(gson.toJson(panelFace));
		System.out.println("The shuffled Panel json");
		AhuPartitionGeneratorUtil.slicePanelByBitreeRule(panelFace, "");
		System.out.println(gson.toJson(panelFace));
		System.out.println();
	}

	@Test
	public void testDevidePanelByRule03() {
		Gson gson = new Gson();
		PanelFace panelFace = AhuPartitionGeneratorUtil.createPanelFace(44, 44);
		panelFace.setId("0_1");
		System.out.println("The initial json");
		System.out.println(gson.toJson(panelFace));
		// 测试代码，rule需要从excel中读取
		Integer[] horizontals = new Integer[] { 13, 14, 17 };
		Integer[] verticals = new Integer[] {};
		Integer[] horizontalTypes = new Integer[] { 0, 0, 0 };
		Integer[] verticalTypes = new Integer[] { 0, 0, 0, 0, 0 };
		AhuPartitionGeneratorUtil.slicePanelByRule(panelFace, horizontals, horizontalTypes, verticals, verticalTypes);
		System.out.println("The final json");
		System.out.println(gson.toJson(panelFace));
		System.out.println("The shuffled Panel json");
		AhuPartitionGeneratorUtil.slicePanelByBitreeRule(panelFace, "");
		System.out.println(gson.toJson(panelFace));
		System.out.println();
	}

	@Test
	public void testUnitArrayCalculation() {
        PanelFace[] panelArr = new PanelFace[] { AhuPartitionGeneratorUtil.createPanelFace(44, 44),
				AhuPartitionGeneratorUtil.createPanelFace(9, 50), AhuPartitionGeneratorUtil.createPanelFace(19, 49),
				AhuPartitionGeneratorUtil.createPanelFace(10, 66), AhuPartitionGeneratorUtil.createPanelFace(120, 100),
				AhuPartitionGeneratorUtil.createPanelFace(150, 8), };

		Gson gson = new Gson();
		for (int i = 0; i < panelArr.length; i++) {
			PanelFace panelFace = panelArr[i];

			System.out.println("The initial Panel");
			System.out.println(gson.toJson(panelFace));
			// 测试代码，rule需要从excel中读取
			Integer[] widthArr = AhuPartitionGeneratorUtil.calculatePanelSliceUnits(panelFace.getPanelWidth(),
					AhuPartitionGenerator.PREFER_PANEL_WIDTH);
			Integer[] lengthArr = AhuPartitionGeneratorUtil.calculatePanelSliceUnits(panelFace.getPanelLength(),
					AhuPartitionGenerator.PREFER_PANEL_LENGTH);
			System.out.println("The width json");
			System.out.println(gson.toJson(widthArr));
			System.out.println("The length json");
			System.out.println(gson.toJson(lengthArr));
			System.out.println();
		}
	}

}

package com.carrier.ahu.util.partition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by Braden Zhou on 2018/06/26.
 */
public class AhuPartitionValidatorTest {

    @Test
    public void testGetLayeredAhuPartition() {
        int[][] layoutData = { { 1, 2, 3, 4 } };
        List<AhuPartition> ahuPartitions = new ArrayList<>();
        AhuPartition ahuPartition = new AhuPartition();
        ahuPartition.setPos(1);

        int pos = 1;
        LinkedList<Map<String, Object>> section1s = new LinkedList<Map<String, Object>>();
        section1s.add(createSection(SectionTypeEnum.TYPE_FAN, pos++));
        section1s.add(createSection(SectionTypeEnum.TYPE_ACCESS, pos++));
        ahuPartition.setSections(section1s);
        ahuPartitions.add(ahuPartition);

        ahuPartition = new AhuPartition();
        ahuPartition.setPos(2);

        LinkedList<Map<String, Object>> section2s = new LinkedList<Map<String, Object>>();
        section2s.add(createSection(SectionTypeEnum.TYPE_FAN, pos++));
        section2s.add(createSection(SectionTypeEnum.TYPE_ACCESS, pos++));
        ahuPartition.setSections(section2s);
        ahuPartitions.add(ahuPartition);

        List<List<AhuPartition>> layeredAhuPartitions = AhuPartitionValidator.getLayeredAhuPartitions(ahuPartitions,
                layoutData);
        assertEquals(1, layeredAhuPartitions.size());

        int[][] layoutData1 = { { 1 }, { 2 }, { 3 }, { 4 } };
        layeredAhuPartitions = AhuPartitionValidator.getLayeredAhuPartitions(ahuPartitions, layoutData1);
        assertEquals(2, layeredAhuPartitions.size());
    }

    @Test
    public void testValidatePartitionLength() {
        AhuPartition ahuPartition = new AhuPartition();
        LinkedList<Map<String, Object>> sections = new LinkedList<Map<String, Object>>();
        ahuPartition.setSections(sections);
        assertTrue(AhuPartitionValidator.validatePartitionLength(ahuPartition));

        ahuPartition.setLength(AhuPartitionGenerator.getMaxPartitionLength() + 1);
        assertTrue(AhuPartitionValidator.validatePartitionLength(ahuPartition));

        Map<String, Object> section = new HashMap<>();
        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ACCESS.getId());
        sections.add(section);
        assertFalse(AhuPartitionValidator.validatePartitionLength(ahuPartition));

        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ATTENUATOR.getId());
        assertTrue(AhuPartitionValidator.validatePartitionLength(ahuPartition));

        section = new HashMap<>();
        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ACCESS.getId());
        sections.add(section);
        assertFalse(AhuPartitionValidator.validatePartitionLength(ahuPartition));

        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ATTENUATOR.getId());
        assertTrue(AhuPartitionValidator.validatePartitionLength(ahuPartition));
    }

    @Test
    public void testValidateUnsplittableSection() {
        assertTrue(AhuPartitionValidator.validateUnsplittableSection(SectionTypeEnum.TYPE_ATTENUATOR.getId(),
                SectionTypeEnum.TYPE_ACCESS.getId()));
        assertTrue(AhuPartitionValidator.validateUnsplittableSection("", SectionTypeEnum.TYPE_ACCESS.getId()));
        assertTrue(AhuPartitionValidator.validateUnsplittableSection("", ""));
        assertTrue(AhuPartitionValidator.validateUnsplittableSection(null, null));
        assertFalse(AhuPartitionValidator.validateUnsplittableSection(SectionTypeEnum.TYPE_ATTENUATOR.getId(),
                SectionTypeEnum.TYPE_COLD.getId()));
    }

    @Test
    public void testValidateAccessSectionWithODoor() {
        AhuPartition ahuPartition = new AhuPartition();
        AhuParam ahuParam = new AhuParam();
        ahuParam.setProduct(SystemCalculateConstants.AHU_PRODUCT_39XT);
        LinkedList<Map<String, Object>> sections = new LinkedList<Map<String, Object>>();
        ahuPartition.setSections(sections);

        Map<String, Object> section = new HashMap<>();
        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ACCESS.getId());
        sections.add(section);

        Map<String, Object> params = new HashMap<>();
        ahuParam.setParams(params);

        List<PartParam> partParams = new ArrayList<>();
        PartParam partParam = new PartParam();
        partParam.setPosition((short) 1);
        partParam.setKey(SectionTypeEnum.TYPE_ACCESS.getId());

        Map<String, Object> paramOfPart = new HashMap<>();
        paramOfPart.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_ACCESS, MetaKey.KEY_ODOOR),
                SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT);
        partParam.setParams(paramOfPart);
        partParams.add(partParam);
        ahuParam.setPartParams(partParams);
        assertTrue(AhuPartitionValidator.validateAccessSectionWithODoor(ahuPartition, ahuParam));

        ahuParam.setProduct(SystemCalculateConstants.AHU_PRODUCT_39CQ);
        section.put(AhuPartition.S_MKEY_SECTIONL, AhuPartitionGenerator.getMinAccessLength(ahuParam.getProduct()) - 1);
        assertFalse(AhuPartitionValidator.validateAccessSectionWithODoor(ahuPartition, ahuParam));

        section.put(AhuPartition.S_MKEY_SECTIONL, AhuPartitionGenerator.getMinAccessLength(ahuParam.getProduct()));
        assertTrue(AhuPartitionValidator.validateAccessSectionWithODoor(ahuPartition, ahuParam));
    }

    @Test
    public void testValidateSingleControlSection() {
        AhuPartition ahuPartition = new AhuPartition();
        LinkedList<Map<String, Object>> sections = new LinkedList<Map<String, Object>>();
        ahuPartition.setSections(sections);

        Map<String, Object> section = new HashMap<>();
        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ACCESS.getId());
        sections.add(section);
        assertTrue(AhuPartitionValidator.validateSingleControlSection(ahuPartition));

        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_CTR.getId());
        assertFalse(AhuPartitionValidator.validateSingleControlSection(ahuPartition));

        section = new HashMap<>();
        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ACCESS.getId());
        sections.add(section);
        assertTrue(AhuPartitionValidator.validateSingleControlSection(ahuPartition));
    }

    @Test
    public void testValidateRoofWithTopOutlet() {
        AhuPartition ahuPartition = new AhuPartition();
        LinkedList<Map<String, Object>> sections = new LinkedList<Map<String, Object>>();
        ahuPartition.setSections(sections);

        AhuParam ahuParam = new AhuParam();
        Map<String, Object> params = new HashMap<>();
        params.put(MetaKey.META_AHU_ISPRERAIN, "false");
        params.put(MetaKey.META_AHU_SERIAL, "39CQ0618");
        params.put(MetaKey.META_AHU_DELIVERY, SystemCalculateConstants.AHU_DELIVERY_SECTION);
        ahuParam.setParams(params);

        List<PartParam> partParams = new ArrayList<>();
        PartParam partParam = new PartParam();
        partParam.setPosition((short) 1);
        partParam.setKey(SectionTypeEnum.TYPE_MIX.getId());

        Map<String, Object> paramOfPart = new HashMap<>();
        paramOfPart.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, MetaKey.KEY_RETURNTOP), "true");
        partParam.setParams(paramOfPart);
        partParams.add(partParam);
        ahuParam.setPartParams(partParams);
        assertTrue(AhuPartitionValidator.validateRoofWithTopOutlet(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_ISPRERAIN, "true");
        Map<String, Object> section = new HashMap<>();
        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_MIX.getId());
        section.put(AhuPartition.S_MKEY_POS, 0);
        sections.add(section);
        assertFalse(AhuPartitionValidator.validateRoofWithTopOutlet(ahuPartition, ahuParam));

        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_DISCHARGE.getId());
        partParam.setKey(SectionTypeEnum.TYPE_DISCHARGE.getId());
        paramOfPart.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, MetaKey.KEY_OUTLETDIRECTION),
                SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_T);
        assertFalse(AhuPartitionValidator.validateRoofWithTopOutlet(ahuPartition, ahuParam));

        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_FAN.getId());
        partParam.setKey(SectionTypeEnum.TYPE_FAN.getId());
        paramOfPart.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_oUTLETDIRECTION),
                SystemCalculateConstants.FAN_OUTLETDIRECTION_UBF);
        assertFalse(AhuPartitionValidator.validateRoofWithTopOutlet(ahuPartition, ahuParam));

        paramOfPart.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_oUTLETDIRECTION),
                SystemCalculateConstants.FAN_OUTLETDIRECTION_UBR);
        assertFalse(AhuPartitionValidator.validateRoofWithTopOutlet(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_DELIVERY, SystemCalculateConstants.AHU_DELIVERY_CKD);
        assertTrue(AhuPartitionValidator.validateRoofWithTopOutlet(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_DELIVERY, SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR);
        assertTrue(AhuPartitionValidator.validateRoofWithTopOutlet(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_SERIAL, "39CQ0617");
        assertTrue(AhuPartitionValidator.validateRoofWithTopOutlet(ahuPartition, ahuParam));
    }

    @Test
    public void testValidateRoofOnsiteInstallation() {
        AhuPartition ahuPartition = new AhuPartition();
        ahuPartition.setLength(AhuUtil.getRealSize(AhuPartitionValidator.ONSITE_INSTALLATION_LENGTH_THRESHOLD));
        LinkedList<Map<String, Object>> sections = new LinkedList<Map<String, Object>>();
        ahuPartition.setSections(sections);

        AhuParam ahuParam = new AhuParam();
        Map<String, Object> params = new HashMap<>();
        params.put(MetaKey.META_AHU_ISPRERAIN, "false");
        params.put(MetaKey.META_AHU_SERIAL, "39CQ0618");
        ahuParam.setParams(params);
        assertTrue(AhuPartitionValidator.validateRoofOnsiteInstallation(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_ISPRERAIN, "true");
        assertFalse(AhuPartitionValidator.validateRoofOnsiteInstallation(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_SERIAL, "39CQ0617");
        assertTrue(AhuPartitionValidator.validateRoofOnsiteInstallation(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_SERIAL, "39CQ0618");
        ahuPartition.setLength(AhuUtil.getRealSize(AhuPartitionValidator.ONSITE_INSTALLATION_LENGTH_THRESHOLD) - 1);
        assertTrue(AhuPartitionValidator.validateRoofOnsiteInstallation(ahuPartition, ahuParam));
    }

    @Test
    public void testValidateSingleCombinedMixingChamberSection() {
        AhuPartition ahuPartition = new AhuPartition();
        AhuParam ahuParam = new AhuParam();
        Map<String, Object> params = new HashMap<>();
        params.put(MetaKey.META_AHU_SERIAL, "39CQ0608");
        ahuParam.setParams(params);
        assertTrue(AhuPartitionValidator.validateSingleCombinedMixingChamberSection(ahuPartition, ahuParam));

        LinkedList<Map<String, Object>> sections = new LinkedList<Map<String, Object>>();
        ahuPartition.setSections(sections);

        ahuParam.setProduct(SystemCalculateConstants.AHU_PRODUCT_39CQ);
        Map<String, Object> section = new HashMap<>();
        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
        sections.add(section);
        assertFalse(AhuPartitionValidator.validateSingleCombinedMixingChamberSection(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_SERIAL, "39CQ0609");
        assertFalse(AhuPartitionValidator.validateSingleCombinedMixingChamberSection(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_SERIAL, "39CQ1015");
        assertFalse(AhuPartitionValidator.validateSingleCombinedMixingChamberSection(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_SERIAL, "39CQ1016");
        assertTrue(AhuPartitionValidator.validateSingleCombinedMixingChamberSection(ahuPartition, ahuParam));

        params.put(MetaKey.META_AHU_SERIAL, "39CQ0608");
        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ACCESS.getId());
        assertTrue(AhuPartitionValidator.validateSingleCombinedMixingChamberSection(ahuPartition, ahuParam));

        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
        assertFalse(AhuPartitionValidator.validateSingleCombinedMixingChamberSection(ahuPartition, ahuParam));
    }

    @Test
    public void testValidateSingleSectionWithAccessDoor() {
        AhuPartition ahuPartition = new AhuPartition();
        AhuParam ahuParam = new AhuParam();

        List<PartParam> partParams = new ArrayList<>();
        PartParam partParam = new PartParam();
        partParam.setPosition((short) 1);
        partParam.setKey(SectionTypeEnum.TYPE_MIX.getId());
        Map<String, Object> params = new HashMap<>();
        partParam.setParams(params);
        partParams.add(partParam);
        ahuParam.setPartParams(partParams);
        assertTrue(AhuPartitionValidator.validateSingleSectionWithAccessDoor(ahuPartition, ahuParam));

        LinkedList<Map<String, Object>> sections = new LinkedList<Map<String, Object>>();
        ahuPartition.setSections(sections);

        ahuParam.setProduct(SystemCalculateConstants.AHU_PRODUCT_39XT);
        partParam.setKey(SectionTypeEnum.TYPE_MIX.getId());
        params.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, MetaKey.KEY_DOORO),
                SystemCalculateConstants.MIX_DOORO_DOORNOVIEWPORT);
        Map<String, Object> section = new HashMap<>();
        section.put(AhuPartition.S_MKEY_POS, (short) 0);
        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_MIX.getId());
        section.put(AhuPartition.S_MKEY_SECTIONL, AhuPartitionGenerator.getMinPartitionWithDoorLength() - 1);
        sections.add(section);
        assertFalse(AhuPartitionValidator.validateSingleSectionWithAccessDoor(ahuPartition, ahuParam));

        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_DISCHARGE.getId());
        partParam.setKey(SectionTypeEnum.TYPE_DISCHARGE.getId());
        params.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, MetaKey.KEY_ODOOR),
                SystemCalculateConstants.DISCHARGE_ODOOR_DN);
        assertFalse(AhuPartitionValidator.validateSingleSectionWithAccessDoor(ahuPartition, ahuParam));

        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_FAN.getId());
        partParam.setKey(SectionTypeEnum.TYPE_FAN.getId());
        params.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_ACCESSDOOR),
                SystemCalculateConstants.FAN_ACCESSDOOR_DOORWITHWINDOW);
        assertFalse(AhuPartitionValidator.validateSingleSectionWithAccessDoor(ahuPartition, ahuParam));

        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ACCESS.getId());
        partParam.setKey(SectionTypeEnum.TYPE_ACCESS.getId());
        params.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_ACCESS, MetaKey.KEY_ODOOR),
                SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT);
        assertFalse(AhuPartitionValidator.validateSingleSectionWithAccessDoor(ahuPartition, ahuParam));

        section.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
        partParam.setKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
        params.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, MetaKey.KEY_OSDOOR),
                SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY);
        assertFalse(AhuPartitionValidator.validateSingleSectionWithAccessDoor(ahuPartition, ahuParam));

        section.put(AhuPartition.S_MKEY_SECTIONL, AhuPartitionGenerator.getMinPartitionWithDoorLength());
        assertTrue(AhuPartitionValidator.validateSingleSectionWithAccessDoor(ahuPartition, ahuParam));

        section.put(AhuPartition.S_MKEY_SECTIONL, AhuPartitionGenerator.getMinPartitionWithDoorLength() + 1);
        assertTrue(AhuPartitionValidator.validateSingleSectionWithAccessDoor(ahuPartition, ahuParam));

        sections.add(section);
        assertTrue(AhuPartitionValidator.validateSingleSectionWithAccessDoor(ahuPartition, ahuParam));
    }

    @Test
    public void testValidateAccessDoorOfAdjacentSection() {
        AhuParam ahuParam = new AhuParam();

        List<PartParam> partParams = new ArrayList<>();
        PartParam partParamA = new PartParam();
        PartParam partParamB = new PartParam();
        Map<String, Object> paramsA = new HashMap<>();
        Map<String, Object> paramsB = new HashMap<>();

        partParamA.setPosition((short) 1);
        partParamA.setKey(SectionTypeEnum.TYPE_SINGLE.getId());
        partParamA.setParams(paramsA);

        partParamB.setPosition((short) 2);
        partParamB.setKey(SectionTypeEnum.TYPE_MIX.getId());
        partParamB.setParams(paramsB);

        partParams.add(partParamA);
        partParams.add(partParamB);
        ahuParam.setPartParams(partParams);

        Map<String, Object> preLastSection = new HashMap<>();
        preLastSection.put(AhuPartition.S_MKEY_POS, (short) 0);
        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_SINGLE.getId());

        Map<String, Object> nextFirstSection = new HashMap<>();
        nextFirstSection.put(AhuPartition.S_MKEY_POS, (short) 1);
        nextFirstSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_MIX.getId());
        assertTrue(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        partParamA.setKey(SectionTypeEnum.TYPE_MIX.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, MetaKey.KEY_DOORO),
                SystemCalculateConstants.MIX_DOORO_DOORNOVIEWPORT);
        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_MIX.getId());
        assertTrue(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_DISCHARGE.getId());
        partParamA.setKey(SectionTypeEnum.TYPE_DISCHARGE.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, MetaKey.KEY_ODOOR),
                SystemCalculateConstants.DISCHARGE_ODOOR_DN);
        assertTrue(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_FAN.getId());
        partParamA.setKey(SectionTypeEnum.TYPE_FAN.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_ACCESSDOOR),
                SystemCalculateConstants.FAN_ACCESSDOOR_DOORWITHWINDOW);
        assertTrue(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ACCESS.getId());
        partParamA.setKey(SectionTypeEnum.TYPE_ACCESS.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_ACCESS, MetaKey.KEY_ODOOR),
                SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT);
        assertTrue(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
        partParamA.setKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, MetaKey.KEY_OSDOOR),
                SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY);
        assertTrue(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, MetaKey.KEY_OSDOOR),
                SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_ND);
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, MetaKey.KEY_ISDOOR),
                SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY);
        assertTrue(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        partParamA.setKey(SectionTypeEnum.TYPE_MIX.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, MetaKey.KEY_DOORO),
                SystemCalculateConstants.MIX_DOORO_NODOOR);
        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_MIX.getId());
        assertFalse(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_DISCHARGE.getId());
        partParamA.setKey(SectionTypeEnum.TYPE_DISCHARGE.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, MetaKey.KEY_ODOOR),
                SystemCalculateConstants.DISCHARGE_ODOOR_ND);
        assertFalse(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_FAN.getId());
        partParamA.setKey(SectionTypeEnum.TYPE_FAN.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_ACCESSDOOR), "");
        assertFalse(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ACCESS.getId());
        partParamA.setKey(SectionTypeEnum.TYPE_ACCESS.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_ACCESS, MetaKey.KEY_ODOOR),
                SystemCalculateConstants.ACCESS_ODOOR_W_O_DOOR);
        assertFalse(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));

        preLastSection.put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
        partParamA.setKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, MetaKey.KEY_OSDOOR),
                SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_ND);
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, MetaKey.KEY_ISDOOR),
                SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_ND);
        assertFalse(
                AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preLastSection, nextFirstSection, ahuParam));
    }

    @Test
    public void testValidateSplittedFilterSection() {
    }

    @Test
    public void testValidateSplittedCtrAndFanSection() {
        assertTrue(AhuPartitionValidator.validateSplittedCtrAndFanSection("", SectionTypeEnum.TYPE_ACCESS.getId()));
        assertTrue(AhuPartitionValidator.validateSplittedCtrAndFanSection("", ""));
        assertTrue(AhuPartitionValidator.validateSplittedCtrAndFanSection(null, null));
        assertTrue(AhuPartitionValidator.validateSplittedCtrAndFanSection(SectionTypeEnum.TYPE_CTR.getId(), ""));
        assertTrue(AhuPartitionValidator.validateSplittedCtrAndFanSection("", SectionTypeEnum.TYPE_FAN.getId()));
        assertFalse(AhuPartitionValidator.validateSplittedCtrAndFanSection(SectionTypeEnum.TYPE_CTR.getId(),
                SectionTypeEnum.TYPE_FAN.getId()));
        assertFalse(AhuPartitionValidator.validateSplittedCtrAndFanSection(SectionTypeEnum.TYPE_FAN.getId(),
                SectionTypeEnum.TYPE_CTR.getId()));
    }

    @Test
    public void testValidateLeaveFactoryUseCKD() {
        AhuParam ahuParam = new AhuParam();
        Map<String, Object> params = new HashMap<>();
        params.put(MetaKey.META_AHU_DELIVERY, SystemCalculateConstants.AHU_DELIVERY_SECTION);
        ahuParam.setParams(params);

        int partitionLength = AhuPartitionGenerator.getMinPartitionLength();
        assertTrue(AhuPartitionValidator.validateLeaveFactoryUseCKD(ahuParam, partitionLength));

        ahuParam.setProduct(SystemCalculateConstants.AHU_PRODUCT_39CQ);
        assertTrue(AhuPartitionValidator.validateLeaveFactoryUseCKD(ahuParam, partitionLength));

        partitionLength = AhuPartitionGenerator.getMinPartitionLength() - 1;
        assertFalse(AhuPartitionValidator.validateLeaveFactoryUseCKD(ahuParam, partitionLength));

        params.put(MetaKey.META_AHU_DELIVERY, SystemCalculateConstants.AHU_DELIVERY_CKD);
        assertTrue(AhuPartitionValidator.validateLeaveFactoryUseCKD(ahuParam, partitionLength));
    }

    @Test
    public void testValidatePlateHeatRecycleBypass() {
        AhuLayout ahuLayout = AhuLayoutUtils.getInitAhuLayout(LayoutStyleEnum.PLATE);
        List<AhuPartition> ahuPartitions = new ArrayList<>();

        // partition 1
        int pos = 0;
        LinkedList<Map<String, Object>> section1s = new LinkedList<Map<String, Object>>();
        section1s.add(createSection(SectionTypeEnum.TYPE_MIX, pos++));
        section1s.add(createSection(SectionTypeEnum.TYPE_COMPOSITE, pos++));
        section1s.add(createSection(SectionTypeEnum.TYPE_PLATEHEATRECYCLE, pos++));

        AhuPartition ahuPartition = new AhuPartition();
        ahuPartition.setSections(section1s);
        ahuPartitions.add(ahuPartition);

        // partition 2
        LinkedList<Map<String, Object>> section2s = new LinkedList<Map<String, Object>>();
        section2s.add(createSection(SectionTypeEnum.TYPE_SINGLE, pos++));
        section2s.add(createSection(SectionTypeEnum.TYPE_MIX, pos++));

        ahuPartition = new AhuPartition();
        ahuPartition.setSections(section2s);
        ahuPartitions.add(ahuPartition);

        // partition 3
        LinkedList<Map<String, Object>> section3s = new LinkedList<Map<String, Object>>();
        section3s.add(createSection(SectionTypeEnum.TYPE_FAN, pos++));
        section3s.add(createSection(SectionTypeEnum.TYPE_ACCESS, pos++));

        ahuPartition = new AhuPartition();
        ahuPartition.setSections(section3s);
        ahuPartitions.add(ahuPartition);

        // partition 4
        LinkedList<Map<String, Object>> section4s = new LinkedList<Map<String, Object>>();
        section4s.add(createSection(SectionTypeEnum.TYPE_PLATEHEATRECYCLE, pos++));

        ahuPartition = new AhuPartition();
        ahuPartition.setSections(section4s);
        ahuPartitions.add(ahuPartition);

        // partition 5
        LinkedList<Map<String, Object>> section5s = new LinkedList<Map<String, Object>>();
        section5s.add(createSection(SectionTypeEnum.TYPE_COLD, pos++));
        section5s.add(createSection(SectionTypeEnum.TYPE_HEATINGCOIL, pos++));
        section5s.add(createSection(SectionTypeEnum.TYPE_STEAMHUMIDIFIER, pos++));
        section5s.add(createSection(SectionTypeEnum.TYPE_FAN, pos++));

        ahuPartition = new AhuPartition();
        ahuPartition.setSections(section5s);
        ahuPartitions.add(ahuPartition);

        assertTrue(AhuPartitionValidator.validatePlateHeatRecycleBypass(ahuLayout, ahuPartitions));

        // not single section
        section4s.add(0, section3s.remove(section3s.size() - 1));
        section4s.add(section5s.remove(0));
        assertTrue(AhuPartitionValidator.validatePlateHeatRecycleBypass(ahuLayout, ahuPartitions));

        // reset to single section
        section3s.add(section4s.remove(0));
        section5s.add(0, section4s.remove(section4s.size() - 1));

        section5s.get(0).put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_MIX.getId());
        assertFalse(AhuPartitionValidator.validatePlateHeatRecycleBypass(ahuLayout, ahuPartitions));

        section5s.get(0).put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_DISCHARGE.getId());
        assertFalse(AhuPartitionValidator.validatePlateHeatRecycleBypass(ahuLayout, ahuPartitions));

        section5s.get(0).put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_COLD.getId()); // reset

        section3s.get(section3s.size() - 1).put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_MIX.getId());
        assertFalse(AhuPartitionValidator.validatePlateHeatRecycleBypass(ahuLayout, ahuPartitions));

        section3s.get(section3s.size() - 1).put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_DISCHARGE.getId());
        assertFalse(AhuPartitionValidator.validatePlateHeatRecycleBypass(ahuLayout, ahuPartitions));
    }

    @Test
    public void testValidatePlateHeatRecycleBypass2() {
        AhuLayout ahuLayout = AhuLayoutUtils.getInitAhuLayout(LayoutStyleEnum.PLATE);
        List<AhuPartition> ahuPartitions = new ArrayList<>();

        // partition 1
        int pos = 1;
        LinkedList<Map<String, Object>> section1s = new LinkedList<Map<String, Object>>();
        section1s.add(createSection(SectionTypeEnum.TYPE_MIX, pos++));
        section1s.add(createSection(SectionTypeEnum.TYPE_COMPOSITE, pos++));
        section1s.add(createSection(SectionTypeEnum.TYPE_PLATEHEATRECYCLE, pos++));

        AhuPartition ahuPartition = new AhuPartition();
        ahuPartition.setSections(section1s);
        ahuPartitions.add(ahuPartition);

        // partition 2
        LinkedList<Map<String, Object>> section2s = new LinkedList<Map<String, Object>>();
        section2s.add(createSection(SectionTypeEnum.TYPE_SINGLE, pos++));
        section2s.add(createSection(SectionTypeEnum.TYPE_MIX, pos++));

        ahuPartition = new AhuPartition();
        ahuPartition.setSections(section2s);
        ahuPartitions.add(ahuPartition);

        // partition 3
        LinkedList<Map<String, Object>> section3s = new LinkedList<Map<String, Object>>();
        section3s.add(createSection(SectionTypeEnum.TYPE_PLATEHEATRECYCLE, pos++));

        ahuPartition = new AhuPartition();
        ahuPartition.setSections(section3s);
        ahuPartitions.add(ahuPartition);

        int[][] layoutData = ahuLayout.getLayoutData();
        layoutData = new int[][] { { 1, 2, 3 }, { 4, 5 }, { 6 }, {} };
        ahuLayout.setLayoutData(layoutData);
        assertTrue(AhuPartitionValidator.validatePlateHeatRecycleBypass(ahuLayout, ahuPartitions));
    }

    private Map<String, Object> createSection(SectionTypeEnum sectionType, int position) {
        Map<String, Object> section = new HashMap<>();
        section.put(AhuPartition.S_MKEY_METAID, sectionType.getId());
        section.put(AhuPartition.S_MKEY_POS, position);
        return section;
    }

    @Test
    public void testValidateWheelHeatRecycleBypass() {
        AhuLayout ahuLayout = AhuLayoutUtils.getInitAhuLayout(LayoutStyleEnum.WHEEL);
        List<AhuPartition> ahuPartitions = new ArrayList<>();

        // partition 1
        int pos = 0;
        LinkedList<Map<String, Object>> section1s = new LinkedList<Map<String, Object>>();
        section1s.add(createSection(SectionTypeEnum.TYPE_DISCHARGE, pos++));
        section1s.add(createSection(SectionTypeEnum.TYPE_FAN, pos++));
        section1s.add(createSection(SectionTypeEnum.TYPE_WHEELHEATRECYCLE, pos++));

        AhuPartition ahuPartition = new AhuPartition();
        ahuPartition.setSections(section1s);
        ahuPartitions.add(ahuPartition);

        // partition 2
        LinkedList<Map<String, Object>> section2s = new LinkedList<Map<String, Object>>();
        section2s.add(createSection(SectionTypeEnum.TYPE_MIX, pos++));
        section2s.add(createSection(SectionTypeEnum.TYPE_SINGLE, pos++));
        section2s.add(createSection(SectionTypeEnum.TYPE_MIX, pos++));

        ahuPartition = new AhuPartition();
        ahuPartition.setSections(section2s);
        ahuPartitions.add(ahuPartition);

        // partition 3
        LinkedList<Map<String, Object>> section3s = new LinkedList<Map<String, Object>>();
        section3s.add(createSection(SectionTypeEnum.TYPE_MIX, pos++));
        section3s.add(createSection(SectionTypeEnum.TYPE_SINGLE, pos++));
        section3s.add(createSection(SectionTypeEnum.TYPE_ACCESS, pos++));
        section3s.add(createSection(SectionTypeEnum.TYPE_WHEELHEATRECYCLE, pos++));

        ahuPartition = new AhuPartition();
        ahuPartition.setSections(section3s);
        ahuPartitions.add(ahuPartition);

        // partition 4
        LinkedList<Map<String, Object>> section4s = new LinkedList<Map<String, Object>>();
        section4s.add(createSection(SectionTypeEnum.TYPE_MIX, pos++));
        section4s.add(createSection(SectionTypeEnum.TYPE_COLD, pos++));
        section4s.add(createSection(SectionTypeEnum.TYPE_HEATINGCOIL, pos++));
        section4s.add(createSection(SectionTypeEnum.TYPE_STEAMHUMIDIFIER, pos++));

        ahuPartition = new AhuPartition();
        ahuPartition.setSections(section4s);
        ahuPartitions.add(ahuPartition);

        // partition 5
        LinkedList<Map<String, Object>> section5s = new LinkedList<Map<String, Object>>();
        section5s.add(createSection(SectionTypeEnum.TYPE_FAN, pos++));

        ahuPartition = new AhuPartition();
        ahuPartition.setSections(section5s);
        ahuPartitions.add(ahuPartition);

        assertFalse(AhuPartitionValidator.validateWheelHeatRecycleBypass(ahuLayout, ahuPartitions));

        section4s.get(0).put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_DISCHARGE.getId());
        assertFalse(AhuPartitionValidator.validateWheelHeatRecycleBypass(ahuLayout, ahuPartitions));

        section4s.get(0).put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_ACCESS.getId());
        assertTrue(AhuPartitionValidator.validateWheelHeatRecycleBypass(ahuLayout, ahuPartitions));

        section4s.get(0).put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_MIX.getId());

        section3s.add(section4s.remove(0));
        assertTrue(AhuPartitionValidator.validateWheelHeatRecycleBypass(ahuLayout, ahuPartitions));

        section1s.get(1).put(AhuPartition.S_MKEY_METAID, SectionTypeEnum.TYPE_DISCHARGE.getId());
        assertTrue(AhuPartitionValidator.validateWheelHeatRecycleBypass(ahuLayout, ahuPartitions));

        section2s.add(0, section1s.remove(2));
        assertFalse(AhuPartitionValidator.validateWheelHeatRecycleBypass(ahuLayout, ahuPartitions));
    }

    @Test
    public void testValidateUnitHeightAgainstContainer() {
        AhuParam ahuParam = new AhuParam();
        ahuParam.setSeries("39CQ2225");
        Map<String, Object> params = new HashMap<>();
        params.put(MetaKey.META_AHU_ISPRERAIN, "false");
        params.put(MetaKey.META_AHU_DELIVERY, SystemCalculateConstants.AHU_DELIVERY_SECTION);
        ahuParam.setParams(params);

        List<PartParam> partParams = new ArrayList<>();
        ahuParam.setPartParams(partParams);
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam, null));

        ahuParam.setSeries("39CQ2226");
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam, ErrorCode.NORMAL_UNIT_EXCEED_HEIGHT_NEED_CKD));

        ahuParam.setSeries("39CQ2227");
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam, ErrorCode.NORMAL_UNIT_EXCEED_HEIGHT_NEED_CKD));

        params.put(MetaKey.META_AHU_ISPRERAIN, "true");
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam, ErrorCode.UNIT_WITH_ROOF_EXCEED_HEIGHT_NEED_CKD));

        params.put(MetaKey.META_AHU_DELIVERY, SystemCalculateConstants.AHU_DELIVERY_CKD);
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam, null));

        PartParam partParamA = new PartParam();
        Map<String, Object> paramsA = new HashMap<>();
        partParamA.setPosition((short) 1);
        partParamA.setParams(paramsA);
        partParams.add(partParamA);

        params.put(MetaKey.META_AHU_ISPRERAIN, "false");
        partParamA.setKey(SectionTypeEnum.TYPE_MIX.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, MetaKey.KEY_RETURNTOP), "true");
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam, null));

        params.put(MetaKey.META_AHU_DELIVERY, SystemCalculateConstants.AHU_DELIVERY_SECTION);
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam,
                ErrorCode.UNIT_WITH_TOP_DAMPER_EXCEED_HEIGHT_NEED_CKD));

        partParamA.setKey(SectionTypeEnum.TYPE_DISCHARGE.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, MetaKey.KEY_OUTLETDIRECTION),
                SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_T);
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam,
                ErrorCode.UNIT_WITH_TOP_DAMPER_EXCEED_HEIGHT_NEED_CKD));

        partParamA.setKey(SectionTypeEnum.TYPE_FAN.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_oUTLETDIRECTION),
                SystemCalculateConstants.FAN_OUTLETDIRECTION_UBF);
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam,
                ErrorCode.UNIT_WITH_TOP_DAMPER_EXCEED_HEIGHT_NEED_CKD));

        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_oUTLETDIRECTION),
                SystemCalculateConstants.FAN_OUTLETDIRECTION_UBR);
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam,
                ErrorCode.UNIT_WITH_TOP_DAMPER_EXCEED_HEIGHT_NEED_CKD));

        params.put(MetaKey.META_AHU_ISPRERAIN, "true");
        partParamA.setKey(SectionTypeEnum.TYPE_MIX.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, MetaKey.KEY_RETURNTOP), "true");
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam,
                ErrorCode.UNIT_WITH_TOP_DAMPER_AND_ROOF_EXCEED_HEIGHT_NEED_CKD));

        partParamA.setKey(SectionTypeEnum.TYPE_DISCHARGE.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, MetaKey.KEY_OUTLETDIRECTION),
                SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_T);
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam,
                ErrorCode.UNIT_WITH_TOP_DAMPER_AND_ROOF_EXCEED_HEIGHT_NEED_CKD));

        partParamA.setKey(SectionTypeEnum.TYPE_FAN.getId());
        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_oUTLETDIRECTION),
                SystemCalculateConstants.FAN_OUTLETDIRECTION_UBF);
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam,
                ErrorCode.UNIT_WITH_TOP_DAMPER_AND_ROOF_EXCEED_HEIGHT_NEED_CKD));

        paramsA.put(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_oUTLETDIRECTION),
                SystemCalculateConstants.FAN_OUTLETDIRECTION_UBR);
        assertTrue(assertValidateUnitHeightAgainstContainer(ahuParam,
                ErrorCode.UNIT_WITH_TOP_DAMPER_AND_ROOF_EXCEED_HEIGHT_NEED_CKD));
    }

    private boolean assertValidateUnitHeightAgainstContainer(AhuParam ahuParam, ErrorCode errorCode) {
        try {
            AhuPartitionValidator.validateUnitHeightAgainstContainer(ahuParam);
        } catch (ApiException exp) {
            if (errorCode != null && errorCode.equals(exp.getErrorCode())) {
                return true;
            } else {
                return false;
            }
        }
        if (errorCode != null) {
            return false;
        } else {
            return true;
        }
    }

    @Test
    public void testValidateMinimumPartitionLength() {
        AhuPartition ahuPartition = new AhuPartition();
        AhuParam ahuParam = new AhuParam();
        Map<String, Object> params = new HashMap<>();
        ahuParam.setParams(params);

        String[][] testcases = { { "0607", "2", "true" }, { "0607", "3", "true" }, { "0607", "4", "true" },
                { "0608", "2", "false" }, { "0608", "3", "true" }, { "0608", "4", "true" }, { "0711", "2", "false" },
                { "0711", "3", "true" }, { "0711", "4", "true" }, { "0712", "2", "true" }, { "0712", "3", "true" },
                { "0712", "4", "true" }, { "0810", "3", "true" }, { "0810", "4", "true" }, { "0810", "5", "true" },
                { "0811", "3", "false" }, { "0811", "4", "true" }, { "0811", "5", "true" }, { "0914", "3", "false" },
                { "0914", "4", "true" }, { "0914", "5", "true" }, { "0915", "3", "true" }, { "0915", "4", "true" },
                { "0915", "5", "true" }, { "1014", "4", "true" }, { "1014", "5", "true" }, { "1014", "6", "true" },
                { "1015", "4", "false" }, { "1015", "5", "true" }, { "1015", "6", "true" }, { "1117", "4", "false" },
                { "1117", "5", "true" }, { "1117", "6", "true" }, { "1118", "4", "true" }, { "1118", "5", "true" },
                { "1118", "6", "true" }, { "1316", "5", "true" }, { "1316", "6", "true" }, { "1316", "7", "true" },
                { "1317", "5", "false" }, { "1317", "6", "true" }, { "1317", "7", "true" }, { "1420", "5", "false" },
                { "1420", "6", "true" }, { "1420", "7", "true" }, { "1421", "5", "true" }, { "1421", "6", "true" },
                { "1421", "7", "true" }, { "1620", "5", "true" }, { "1620", "6", "true" }, { "1620", "7", "true" },
                { "1621", "5", "false" }, { "1621", "6", "true" }, { "1621", "7", "true" }, { "2333", "5", "false" },
                { "2333", "6", "true" }, { "2333", "7", "true" }, { "2334", "5", "true" }, { "2334", "6", "true" },
                { "2334", "7", "true" } };

        for (String[] testcase : testcases) {
            params.put(MetaKey.META_AHU_SERIAL, "39CQ" + testcase[0]);
            ahuPartition.setLength(AhuUtil.getRealSize(Integer.valueOf(testcase[1])));
            if (Boolean.valueOf(testcase[2])) {
                assertTrue(AhuPartitionValidator.validateMinimumPartitionLength(ahuPartition, ahuParam));
            } else {
                assertFalse(AhuPartitionValidator.validateMinimumPartitionLength(ahuPartition, ahuParam));
            }
        }

    }

}

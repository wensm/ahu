package com.carrier.ahu.calculator;

import java.util.List;

import org.junit.Test;

import com.carrier.ahu.length.param.HumidificationBean;
import com.carrier.ahu.metadata.entity.humidifier.S4HHumiQ;
import com.carrier.ahu.metadata.entity.humidifier.SJSupplier;

/**
 * Created by LIANGD4 on 2017/10/25.
 */
public class PerformanceCalculatorTest {

    @Test
    public void rHConvertHumidification() throws Exception {
        HumidificationBean humidification = PerformanceCalculator.rHConvert(30.0, 15.0, 23, 18000);
        System.out.println(humidification);
    }

    @Test
    public void rHConvertHumidification1() throws Exception {
        HumidificationBean humidification = PerformanceCalculator.rHConvert(40.0, 15.0, 20, 18000);
        System.out.println(humidification);
    }

    @Test
    public void humidificationConvert() throws Exception {
        HumidificationBean humidification = PerformanceCalculator.humidificationConvert(30.0, 15.0, 20, 18000);
        System.out.println(humidification);
    }

    //干蒸汽加湿段
    @Test
    public void getPerformanceH() throws Exception {
        List<S4HHumiQ> s4HHumiQPoList = PerformanceCalculator.getPerformanceH("S", 50, 50, "B");
        System.out.println(s4HHumiQPoList);
    }

    //高压喷雾加湿段
    @Test
    public void getPerformanceJ() throws Exception {
        List<SJSupplier> sjSupplierPoList = PerformanceCalculator.getPerformanceJ("S", 50, 50, "B");
        System.out.println(sjSupplierPoList);
    }


}
package com.carrier.ahu.calculator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.carrier.ahu.calculator.price.PriceCodePO;
import com.carrier.ahu.calculator.price.PriceCodeXSLXPO;
import com.carrier.ahu.util.meta.SectionMetaUtils;

public class ExcelForPriceCodeUtilsTest {

    @Test
    public void testGetPriceCode() {
        PriceCodePO priceCodePo = new PriceCodePO();
        String seqNo = "";
        String sectionType = "section";
        String metaKeyA = SectionMetaUtils.getMetaSectionKey(sectionType, "metakeya");
        String metaKeyB = SectionMetaUtils.getMetaSectionKey(sectionType, "metakeyb");
        String firstCode = "@";
        Map<String, Object> allMetaMap = new HashMap<>();
        List<PriceCodeXSLXPO> priceCodePOs = new ArrayList<>();
        String actualPriceCode = ExcelForPriceCodeUtils.getPriceCode(priceCodePo, seqNo, priceCodePOs, sectionType,
                allMetaMap);

        String expectedPriceCode = ExcelForPriceCodeUtils.DEFAULT_PRICE_CODE;
        assertEquals(expectedPriceCode, actualPriceCode);

        //
        allMetaMap.put(metaKeyA, "123.21");

        PriceCodeXSLXPO priceCode = new PriceCodeXSLXPO();
        priceCode.setMetaKey(metaKeyA);
        priceCode.setMetaValue("123.22");
        priceCode.setCode(firstCode);
        priceCodePOs.add(priceCode);

        expectedPriceCode = "A";
        priceCode = new PriceCodeXSLXPO();
        priceCode.setMetaKey(metaKeyA);
        priceCode.setMetaValue("123.21");
        priceCode.setCode(expectedPriceCode);
        priceCodePOs.add(priceCode);

        actualPriceCode = ExcelForPriceCodeUtils.getPriceCode(priceCodePo, seqNo, priceCodePOs, sectionType,
                allMetaMap);
        assertEquals(expectedPriceCode, actualPriceCode);

        allMetaMap.put(metaKeyA, "123.2");
        priceCode.setMetaValue("123.20");
        actualPriceCode = ExcelForPriceCodeUtils.getPriceCode(priceCodePo, seqNo, priceCodePOs, sectionType,
                allMetaMap);
        assertEquals(expectedPriceCode, actualPriceCode);

        //
        priceCode.setMetaValue("1###");
        actualPriceCode = ExcelForPriceCodeUtils.getPriceCode(priceCodePo, seqNo, priceCodePOs, sectionType,
                allMetaMap);
        assertEquals(firstCode, actualPriceCode);

        priceCode.setMetaValue("#1##");
        actualPriceCode = ExcelForPriceCodeUtils.getPriceCode(priceCodePo, seqNo, priceCodePOs, sectionType,
                allMetaMap);
        assertEquals(expectedPriceCode, actualPriceCode);

        allMetaMap.put(metaKeyA, "123");
        allMetaMap.put(metaKeyB, "234");
        priceCode.setMetaKey(metaKeyA + "/" + metaKeyB);
        priceCode.setMetaValue("123/234");
        actualPriceCode = ExcelForPriceCodeUtils.getPriceCode(priceCodePo, seqNo, priceCodePOs, sectionType,
                allMetaMap);
        assertEquals(expectedPriceCode, actualPriceCode);

        //
        allMetaMap.put(metaKeyA, "123");
        allMetaMap.put(metaKeyB, "234");
        priceCode.setMetaKey(metaKeyA);
        priceCode.setMetaValue("123|234");
        actualPriceCode = ExcelForPriceCodeUtils.getPriceCode(priceCodePo, seqNo, priceCodePOs, sectionType,
                allMetaMap);
        assertEquals(expectedPriceCode, actualPriceCode);

        //
        allMetaMap.put(metaKeyA, "234");
        allMetaMap.put(metaKeyB, "234");
        priceCode.setMetaKey(metaKeyA);
        priceCode.setMetaValue("123|234");
        actualPriceCode = ExcelForPriceCodeUtils.getPriceCode(priceCodePo, seqNo, priceCodePOs, sectionType,
                allMetaMap);
        assertEquals(expectedPriceCode, actualPriceCode);

        allMetaMap.put(metaKeyA, "123.22");
        priceCode.setMetaKey(metaKeyA + "/" + metaKeyB);
        priceCode.setMetaValue("123/234");
        actualPriceCode = ExcelForPriceCodeUtils.getPriceCode(priceCodePo, seqNo, priceCodePOs, sectionType,
                allMetaMap);
        assertEquals(firstCode, actualPriceCode);
    }

}

package com.carrier.ahu.util.ahu;

import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_ACCESS;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_DISCHARGE;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_MIX;
import static com.carrier.ahu.common.enums.SectionTypeEnum.*;
import static com.carrier.ahu.util.meta.SectionMetaUtils.getMetaSectionKey;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_DOORO;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_MEDIALOADING;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.*;
import static com.carrier.ahu.vo.SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT;
import static com.carrier.ahu.vo.SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39CQ;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39G;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39XT;
import static com.carrier.ahu.vo.SystemCalculateConstants.DISCHARGE_ODOOR_DN;
import static com.carrier.ahu.vo.SystemCalculateConstants.DISCHARGE_ODOOR_DY;
import static com.carrier.ahu.vo.SystemCalculateConstants.FILTER_MEDIALOADING_SIDELOADING;
import static com.carrier.ahu.vo.SystemCalculateConstants.MIX_DOORO_DOORNOVIEWPORT;
import static com.carrier.ahu.vo.SystemCalculateConstants.MIX_DOORO_DOORWITHVIEWPORT;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.metadata.entity.AhuSizeDetail;
import com.carrier.ahu.util.meta.AhuMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.google.gson.Gson;

/**
 * Created by Braden Zhou on 2019/08/02.
 */
public class AhuProductUtils {

    private static final int MIN_SECTION_LENGHT_39G = 4;
    private static final int MIN_SECTION_LENGHT_39CQ = 5;
    private static final int MIN_SECTION_LENGHT_39XT = 5;

    /**
     * 1。39G/39XT: 单层过滤段无侧抽 </br>
     * 2。空段，混合段和出风段开门情况下最小段长：39G=4，39CQ=5，39XT=5
     * 
     * @param part
     * @param product
     * @return
     */
    @SuppressWarnings({ "unchecked" })
    public static boolean needsConfirmIfSwitchProduct(Part part, String product) {
        Map<String, Object> partMeta = new Gson().fromJson(part.getMetaJson(), Map.class);

        if (TYPE_SINGLE.getId().equals(part.getSectionKey()) || TYPE_COMPOSITE.getId().equals(part.getSectionKey())
                && (AHU_PRODUCT_39G.equals(product) || AHU_PRODUCT_39XT.equals(product))) {
        	String filterFormatter = partMeta.get(getMetaSectionKey(part.getSectionKey(), KEY_FITETF)).toString();
        	if("X".equalsIgnoreCase(filterFormatter)) {
        		return false;
        	}
            String mediaLoading = partMeta.get(getMetaSectionKey(part.getSectionKey(), KEY_MEDIALOADING)).toString();
            return FILTER_MEDIALOADING_SIDELOADING.equals(mediaLoading);
        }

        boolean hasDoor = false;
        if (TYPE_ACCESS.getId().equals(part.getSectionKey())) {
            String accessDoor = partMeta.get(getMetaSectionKey(part.getSectionKey(), KEY_ODOOR)).toString();
            hasDoor = ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(accessDoor)
                    || ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(accessDoor);
        } else if (TYPE_MIX.getId().equals(part.getSectionKey())) {
            String accessDoor = partMeta.get(getMetaSectionKey(part.getSectionKey(), KEY_DOORO)).toString();
            hasDoor = MIX_DOORO_DOORNOVIEWPORT.equals(accessDoor) || MIX_DOORO_DOORWITHVIEWPORT.equals(accessDoor);
        } else if (TYPE_DISCHARGE.getId().equals(part.getSectionKey())) {
            String accessDoor = partMeta.get(getMetaSectionKey(part.getSectionKey(), KEY_ODOOR)).toString();
            hasDoor = DISCHARGE_ODOOR_DN.equals(accessDoor) || DISCHARGE_ODOOR_DY.equals(accessDoor);
        }

        if (hasDoor) {
            int sectionLength = AhuUtil.getSectionL(part.getSectionKey(), partMeta);
            return (AHU_PRODUCT_39G.equals(product) && sectionLength < MIN_SECTION_LENGHT_39G)
                    || (AHU_PRODUCT_39CQ.equals(product) && sectionLength < MIN_SECTION_LENGHT_39CQ)
                    || (AHU_PRODUCT_39XT.equals(product) && sectionLength < MIN_SECTION_LENGHT_39XT);
        }
        return false;
    }

    /**
     * 从39CQ机组切换机组系列，需要清空正压门属性
     * 
     * @param part
     * @param oldProduct
     * @return
     */
    @SuppressWarnings("unchecked")
    public static boolean needsClearPositivePressureDoor(Part part, String oldProduct, String newProduct) {
        if ((AHU_PRODUCT_39CQ.equals(oldProduct)||AHU_PRODUCT_39XT.equals(oldProduct)) && AHU_PRODUCT_39G.equals(newProduct)) {
            if (TYPE_ACCESS.getId().equals(part.getSectionKey()) || TYPE_FAN.getId().equals(part.getSectionKey())
                    || TYPE_MIX.getId().equals(part.getSectionKey())
                    || TYPE_DISCHARGE.getId().equals(part.getSectionKey())
                    || TYPE_COMBINEDMIXINGCHAMBER.getId().equals(part.getSectionKey())
                    || TYPE_WWKFAN.getId().equals(part.getSectionKey())) {
                Map<String, Object> partMeta = new Gson().fromJson(part.getMetaJson(), Map.class);
                String possitivePressureDoor = partMeta
                        .get(getMetaSectionKey(part.getSectionKey(), KEY_POSITIVE_PRESSURE_DOOR)).toString();
                return Boolean.valueOf(possitivePressureDoor);
            }
        }
        return false;
    }

    public static AhuSizeDetail getAhuSizeDetail(String series, String unitModel) {
        List<AhuSizeDetail> ahuSizeDetails = AhuMetaUtils.getAhuSizeDetails(series);
        return ahuSizeDetails.stream()
                .filter(s -> unitModel.equals(s.getAhu()))
                .findFirst()
                .orElseThrow(IllegalStateException::new);
    }
    
    /**
     * 从39CQ到39XT，需要清空面板厚度和材质
     * @param series
     * @param unitModel
     * @return
     */
    public static void resetXTCasingMaterials(JSONObject ahuJson, String oldProduct, String newProduct) {
    	if (AHU_PRODUCT_39XT.equals(newProduct)){
    		ahuJson.put(CommonConstant.METAHU_INSKINM, "GlSteel");
    		ahuJson.put(CommonConstant.METAHU_INSKINW, "0.5mm");
    		ahuJson.put(CommonConstant.METAHU_EXSKINM, "prePaintedSteal");
    		ahuJson.put(CommonConstant.METAHU_EXSKINW, "0.5mm");
    		
    	}

    	
    	
    	
    }

}

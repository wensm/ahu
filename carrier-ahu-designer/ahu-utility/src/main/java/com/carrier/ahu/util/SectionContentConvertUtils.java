package com.carrier.ahu.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.engine.cad.IniConfigMapUtil;
import com.carrier.ahu.entity.HeatRecycleInfo;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.cad.Damper;
import com.carrier.ahu.metadata.entity.cad.DamperDimension;
import com.carrier.ahu.metadata.entity.coil.STakeoversize;
import com.carrier.ahu.metadata.entity.fan.FanCodeElectricSize;
import com.carrier.ahu.metadata.entity.fan.FanCodeElectricSizeBig;
import com.carrier.ahu.metadata.entity.fan.STwoSpeedMotor;
import com.carrier.ahu.metadata.entity.heatrecycle.Rotorpara;
import com.carrier.ahu.metadata.entity.humidifier.S4xhumid;
import com.carrier.ahu.metadata.entity.report.AdjustAirDoor;
import com.carrier.ahu.metadata.entity.report.XtDoor;
import com.carrier.ahu.metadata.entity.section.SectionArea;
import com.carrier.ahu.param.PositivePressureDoorParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.positivepressuredoor.PositivePressureDoorUtil;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.report.content.ReportContent;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.common.reflect.TypeToken;
import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.apache.commons.collections4.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.configuration.AHUContext.getLanguage;
import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.common.model.partition.AhuPartition.S_MKEY_METAID;
import static com.carrier.ahu.common.model.partition.AhuPartition.S_MKEY_METAJSON;
import static com.carrier.ahu.constant.CommonConstant.JSON_FILTER_FILTEROPTIONS_NO;
import static com.carrier.ahu.constant.CommonConstant.JSON_FILTER_FILTEROPTIONS_NOFILTER;
import static com.carrier.ahu.constant.CommonConstant.JSON_HEPAFILTER_SUPPLIER_NONE;
import static com.carrier.ahu.constant.CommonConstant.METAHU_SERIAL;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_COMBINEDFILTER_LMATERIAL;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_COMBINEDFILTER_RMATERIAL;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_FANMODEL;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_MOTORBASENO;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_OUTLET;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_PTC;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_STARTSTYLE;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_SUPPLIER;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_TYPE;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FILTER_FILTEROPTIONS;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_HEPAFILTER_SUPPLIER;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_WHEELHEATRECYCLE_TYPE_B;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_WHEELHEATRECYCLE_TYPE_C;
import static com.carrier.ahu.constant.CommonConstant.SYS_ASSERT_FALSE;
import static com.carrier.ahu.constant.CommonConstant.SYS_ASSERT_TRUE;
import static com.carrier.ahu.constant.CommonConstant.SYS_BLANK;
import static com.carrier.ahu.constant.CommonConstant.SYS_PUNCTUATION_SLIGHT_PAUSE;
import static com.carrier.ahu.report.common.ReportConstants.*;
import static com.carrier.ahu.util.DateUtil.YYYY_MM_DD;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_PRODUCT_PACKING_LIST_NAME;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_SERIAL;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;


/**
 * Created by LIANGD4 on 2018/1/3.
 * 报告使用
 */
@Component
public class SectionContentConvertUtils {
	
	private static String PatchVersion;
	
	@Value("${ahu.patch.version}")
	private void setPatchVersion(String pv) {
		PatchVersion = pv;
	}	

	private static Logger logger = LoggerFactory.getLogger(SectionContentConvertUtils.class);
	
	private static double outsideTemp = 27.0;
	private static double winterTemperature = 10.0;
	
	private static Map<String, Map<String, Double>> table2 = new HashMap<String, Map<String, Double>>();

//    @Autowired
//    private static PositivePressureDoorUtil positivePressureDoorUtil;

    private static SectionContentConvertUtils sectionContentConvertUtils;

//    @PostConstruct
//    public void init() {
//        sectionContentConvertUtils = this;
//        sectionContentConvertUtils.positivePressureDoorUtil = this.positivePressureDoorUtil;
//    }

	/**
	 * 欧标table2表格
	 */
	private static void initTable2() {
		Map<String, Double> table2AP = new HashMap<String, Double>();
		table2AP.put("Velocity", 1.4);
		table2AP.put("Efficiency", 0.83);
		table2AP.put("Pclass", 250.0);
		table2AP.put("Ngref", 64.0);
		table2.put("A+", table2AP);

		Map<String, Double> table2A = new HashMap<String, Double>();
		table2A.put("Velocity", 1.6);
		table2A.put("Efficiency", 0.78);
		table2A.put("Pclass", 230.0);
		table2A.put("Ngref", 62.0);
		table2.put("A", table2A);

		Map<String, Double> table2B = new HashMap<String, Double>();
		table2B.put("Velocity", 1.8);
		table2B.put("Efficiency", 0.73);
		table2B.put("Pclass", 210.0);
		table2B.put("Ngref", 60.0);
		table2.put("B", table2B);

		Map<String, Double> table2C = new HashMap<String, Double>();
		table2C.put("Velocity", 2.0);
		table2C.put("Efficiency", 0.68);
		table2C.put("Pclass", 190.0);
		table2C.put("Ngref", 57.0);
		table2.put("C", table2C);

		Map<String, Double> table2D = new HashMap<String, Double>();
		table2D.put("Velocity", 2.2);
		table2D.put("Efficiency", 0.63);
		table2D.put("Pclass", 170.0);
		table2D.put("Ngref", 52.0);
		table2.put("D", table2D);
	}
	
	/**
	 * 封装技术说明header，头部内容
	 * 
	 * @param contents
	 * @param unit
	 * @param language
	 * @return
	 */
	public static String[][] getHeader(String[][] contents, Unit unit, LanguageEnum language) {
		String SystemVersion = contents[0][4];
		if (AHUContext.isExportVersion()) {// 判断是否出口版
			contents[0][4] = SystemVersion + UtilityConstant.SYS_BLANK_SPACE + UtilityConstant.SYS_VERSION_EXPORT
					+ UtilityConstant.SYS_BLANK_SPACE + (PatchVersion.contains("-")?PatchVersion.split("-")[0]:PatchVersion);
		} else {
			contents[0][4] = SystemVersion + UtilityConstant.SYS_BLANK_SPACE + (PatchVersion.contains("-")?PatchVersion.split("-")[0]:PatchVersion);
		}
		return contents;
	}
	
	/**
	 * 封装技术说明header头部内容（Euro）
	 * @param contents
	 * @param unit
	 * @param language
	 * @return
	 */
	public static String[][] getHeaderEuro(String[][] contents, Unit unit, LanguageEnum language,
                                           Map<String, Map<String, String>> allMap, List<PartPO> partList, Date d) {
		contents[0][0] = MessageFormat.format(" Date:{0}", DateUtil.getDateTimeString(d, YYYY_MM_DD));
		
		// 计算能效
		calEnergyEfficiency(contents, unit, allMap, partList);
		
		String series = unit.getSeries();
		if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39CQ)) {// 39CQ
			contents[1][1] = "Carrier Air-Conditioning and Refrigeration System (Shanghai) Co., Ltd. is participating in the EUROVENT CERTIFICATION Programme for Air Handling Units. The range 39CQ unit is certified under the number 19.05.032 and presented on www.eurovent-certification.com.";
		} else if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39G)) {// 39G
			contents[1][1] = "Carrier Air-Conditioning and Refrigeration System (Shanghai) Co., Ltd. is participating in the EUROVENT CERTIFICATION Programme for Air Handling Units. The range 39G unit is certified under the number 19.05.032 and presented on www.eurovent-certification.com.";
		} else if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {// 39XT
			contents[1][1] = "Carrier Air-Conditioning and Refrigeration System (Shanghai) Co., Ltd. is participating in the EUROVENT CERTIFICATION Programme for Air Handling Units. The range 39XT unit is certified under the number 19.05.033 and presented on www.eurovent-certification.com.";
		}
		
		return contents;
	}

	/**
	 * 计算内档CQ尺寸
	 * @return
	 */
	public static double rebuBuildND(double wh){
		wh = (wh*100-10)/100;
		return wh;
	}
	public static String[][] getAhuOverall(String[][] contents, Partition partition, Unit unit, LanguageEnum language,
                                           Map<String, String> allMap) {
		AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());

		// Eurovent Change contents[1][2] = UtilityConstant.JSON_UNIT_NAME_AHU;
		String isprerain = BaseDataUtil.constraintString(allMap.get(UtilityConstant.METAHU_ISPRERAIN));
		if (isprerain == null || !UtilityConstant.SYS_ASSERT_TRUE.equals(isprerain)) {
			contents[19][0] = UtilityConstant.SYS_BLANK;// Eurovent Change 17-->19
			contents[19][2] = UtilityConstant.SYS_BLANK;// Eurovent Change 17-->19
		}

		// Eurovent update begin
		List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);
		boolean isDouble = false;
		for (AhuPartition ap : ahuPartitionList) {
			isDouble = Boolean.valueOf(ap.isTopLayer()) ? true : false;
			if (isDouble) {
				break;
			}
		}
		if (SystemCountUtil.isEuroUnit(unit)) {// 判断是否欧标
			contents[1][0] = getIntlString(ERP_IDENTIFICATION_CODE);
			int airVolume = BaseDataUtil
					.stringConversionInteger(String.valueOf(allMap.get(UtilityConstant.METAHU_SAIRVOLUME)));
			if (airVolume >= 250 && airVolume <= 1000) {
				contents[1][2] = "RVU";
			} else if (airVolume > 1000) {
				if (isDouble) {// 判断是否是双层机组
					contents[1][2] = "NRVU-BVU";// 双层机组
				} else {
					contents[1][2] = "NRVU-UVU";// 单层机组
				}
			}
			double width = AhuUtil.getWidthOfAHU(unit.getSeries());
			double height = AhuUtil.getHeightOfAHU(unit.getSeries());
			if (unit.getSeries().contains(UtilityConstant.SYS_UNIT_SERIES_39CQ)) {
				contents[7][2] = String.valueOf(BaseDataUtil.decimalConvert(Double.valueOf(airVolume) / ((rebuBuildND(height) / 10) * (rebuBuildND(width) / 10)) / 3600, 2));//面风速
			}else if (unit.getSeries().contains(UtilityConstant.SYS_UNIT_SERIES_39XT)){
				contents[7][2] = String.valueOf(BaseDataUtil.decimalConvert(Double.valueOf(airVolume) / ((height / 10) * (width / 10)) / 3600, 2));//面风速
			}
			//打印model box信息
			if (unit.getSeries().contains(UtilityConstant.SYS_UNIT_SERIES_39CQ)) {
				contents[22][2] = "TB2";
				contents[22][6] = "T2";
			}else if (unit.getSeries().contains(UtilityConstant.SYS_UNIT_SERIES_39G)) {
				contents[22][2] = "TB3";
				contents[22][6] = "T3";
			}else if (unit.getSeries().contains(UtilityConstant.SYS_UNIT_SERIES_39XT)) {
				contents[22][2] = "TB1";
				contents[22][6] = "T1";
			}
		
		} else {// 如果不是欧标，则将第16、17行清空
			contents[1][2] = UtilityConstant.JSON_UNIT_NAME_AHU;
			contents[16][0] = UtilityConstant.SYS_BLANK;
			contents[17][0] = UtilityConstant.SYS_BLANK;
			ArrayList<String[]> templist = new ArrayList<String[]>(Arrays.asList(contents));
    		ArrayList<String[]> templist1 = new ArrayList<>();
    		for (int i = 0; i < templist.size(); i++) {
				if(i!=20 && i!=21 && i!=22 && i!=23) {//去除欧标中的Model Box信息
					templist1.add(templist.get(i));
				}
    		}

    		String[][] newContent = new String[templist1.size()][contents[0].length];
    		templist1.toArray(newContent);
    		contents = newContent;
		}
		// Eurovent update end

		//只有一个风机回风机参数为空
		if (hasOneOrNoFan(unit,ahuLayout)){
			contents[5][6] = "--";
			contents[6][6] = "--";
		}


		//检修灯打印文字修改:(混合段、空段、出风段、新回排段、离心风机、无蜗壳风机)均不选检修灯的时候，技术说明上不要打印相关信息
		boolean hasJXD = false;
		for (AhuPartition ap : ahuPartitionList) {
			LinkedList<Map<String, Object>> sections = ap.getSections();
			for (Map<String, Object> section : sections) {
				String metaId = String.valueOf(section.get(S_MKEY_METAID));
				if(SectionTypeEnum.TYPE_MIX.getId().equals(metaId)
						|| SectionTypeEnum.TYPE_ACCESS.getId().equals(metaId)
						|| SectionTypeEnum.TYPE_DISCHARGE.getId().equals(metaId)
						|| SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(metaId)
						|| SectionTypeEnum.TYPE_FAN.getId().equals(metaId)){
					String sectionMetaJson = String.valueOf(section.get(S_MKEY_METAJSON));
					Map<String, String> sectionMap = new CaseInsensitiveMap();
					sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));

					String fixRepairLampKey = MetaCodeGen.calculateAttributePrefix(metaId)+ ".fixRepairLamp";
					String fixRepairLampVal = String.valueOf(sectionMap.get(fixRepairLampKey));
					if("true".equalsIgnoreCase(fixRepairLampVal)){
						hasJXD = true;
					}
				}
			}
		}
		if(!hasJXD){
			contents[18][0] = UtilityConstant.SYS_BLANK;
			contents[18][2] = UtilityConstant.SYS_BLANK;
		}/*else{
			if(AHUContext.isExportVersion()){
				contents[18][2] = I18NBundle.getString("light_transformer_input_output_valtage_export", language);
			}
		}*/
		return contents;
	}

	/**
	 * 风机数量0或者1
	 * @param unit
	 * @param ahuLayout
	 * @return
	 */
	private static boolean hasOneOrNoFan(Unit unit, AhuLayout ahuLayout) {
		boolean hasOne = false;

		String groupCode = unit.getGroupCode();

		if (EmptyUtil.isEmpty(groupCode)) {
			if(ahuLayout != null && (LayoutStyleEnum.COMMON.style() == ahuLayout.getStyle())){
				hasOne = true;
			}
		}else{
			if(countInnerStr(groupCode,"011")<2){
				hasOne = true;
			}
		}

		return hasOne;
	}

	/**
	 * 字符串str包含 patternStr数量
	 * @param str
	 * @param patternStr
	 * @return
	 */
	private static int countInnerStr(final String str, final String patternStr) {
		int count = 0;
		Pattern r = Pattern.compile(patternStr);
		Matcher m = r.matcher(str);
		while (m.find()) {

			count++;

		}
		return count;

	}
	/**
	 * 技术说明封装ahu非标数据
	 * @param contents
	 * @param partition
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getAhuNS(String[][] contents, Partition partition,
                                      Unit unit, LanguageEnum language, Map<String, String> allMap) {
		contents = filterContents(contents, SectionTypeEnum.TYPE_AHU);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;
//
//		contents[3][6] = UtilityConstant.SYS_BLANK;
//		contents[3][7] = UtilityConstant.SYS_BLANK;

		return contents;
	}

	/**
	 * 技术说明封装单层过滤段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getSingleNS(String[][] contents, Unit unit, LanguageEnum language,
                                         Map<String, String> allMap) {
		contents = filterContents(contents, SectionTypeEnum.TYPE_SINGLE);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;

		return contents;
	}

	/**
	 * 技术说明封装综合过滤段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getCompositeNS(String[][] contents, Unit unit, LanguageEnum language,
                                            Map<String, String> allMap) {
		contents = filterContents(contents, SectionTypeEnum.TYPE_COMPOSITE);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;

		return contents;
	}

	/**
	 * 技术说明封装高效过滤段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getHepaNS(String[][] contents, Unit unit, LanguageEnum language,
                                       Map<String, String> allMap) {
		contents = filterContents(contents, SectionTypeEnum.TYPE_HEPAFILTER);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;

		return contents;
	}
	/**
	 * 技术说明封装风机段tech.fan.remark.json
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getFanRemark(String[][] contents, Unit unit, LanguageEnum language,
                                          Map<String, String> allMap) {
		String ptc = BaseDataUtil.constraintString(allMap.get(UtilityConstant.METASEXON_FAN_PTC));//是否用PTC热敏电阻保护继电器
		if(!Boolean.parseBoolean(ptc)){
			contents[2][0] = "";
		}



		//TODO暂时只保留最后的：meta.section.fan.remark
		if("do".equals("do")) {
			String[][] retContents = new String[2][1];
			retContents[0][0] = contents[3][0];
			retContents[1][0] = contents[4][0];
			return retContents;
		}

		return contents;
	}
	/**
	 * 技术说明封装风机段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getFanNS(String[][] contents, Unit unit, LanguageEnum language,
                                      Map<String, String> allMap) {

		contents = filterContents(contents, SectionTypeEnum.TYPE_FAN);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;
//
//		contents[3][6] = UtilityConstant.SYS_BLANK;
//		contents[3][7] = UtilityConstant.SYS_BLANK;
//
//		contents[4][6] = UtilityConstant.SYS_BLANK;
//		contents[4][7] = UtilityConstant.SYS_BLANK;
		return contents;
	}
	/**
	 * 技术说明封装风机段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getAccessNS(String[][] contents, Unit unit, LanguageEnum language,
                                         Map<String, String> allMap) {

		contents = filterContents(contents, SectionTypeEnum.TYPE_ACCESS);
//		contents[1][6] = UtilityConstant.SYS_BLANK;
//		contents[1][7] = UtilityConstant.SYS_BLANK;
//
//		contents[2][6] = UtilityConstant.SYS_BLANK;
//		contents[2][7] = UtilityConstant.SYS_BLANK;
//
//		contents[3][6] = UtilityConstant.SYS_BLANK;
//		contents[3][7] = UtilityConstant.SYS_BLANK;
//
//		contents[4][6] = UtilityConstant.SYS_BLANK;
//		contents[4][7] = UtilityConstant.SYS_BLANK;
		return contents;
	}
	/**
	 * 技术说明封装高压喷雾段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getSprayhumidifierNS(String[][] contents, Unit unit, LanguageEnum language,
                                                  Map<String, String> allMap) {

		contents = filterContents(contents, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER);

		return contents;
	}

	/**
	 * 技术说明封装湿膜加湿段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getWethumidifierNS(String[][] contents, Unit unit, LanguageEnum language,
                                                Map<String, String> allMap) {

		contents = filterContents(contents, SectionTypeEnum.TYPE_WETFILMHUMIDIFIER);

		return contents;
	}

	/**
	 * 技术说明封装干蒸加湿段非标数据
	 * @param contents
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getSteamhumidifierNS(String[][] contents, Unit unit, LanguageEnum language,
                                                  Map<String, String> allMap) {

		contents = filterContents(contents, SectionTypeEnum.TYPE_STEAMHUMIDIFIER);

		return contents;
	}

	/**
	 * 封装ahu报告特殊数据
	 * @param partition
	 * @param unit
	 * @param language
	 * @param allMap
	 * @return
	 */
	public static String[][] getAhu(Partition partition,
                                    Unit unit, LanguageEnum language, Map<String, String> allMap) {

		String[][] titleContents = ReportContent.getReportContent(REPORT_TECH_AHU_DELIVERY);
		String partitionJson = partition.getPartitionJson();
		if (EmptyUtil.isNotEmpty(partitionJson)) {
			List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);
			String[][] partitionContents = new String[titleContents.length + ahuPartitionList.size()][6];
			partitionContents[0] = titleContents[0];
			partitionContents[1] = titleContents[1];
			partitionContents[2] = titleContents[2];
			int posSetp = 3;
			//组装装箱段数据
			for (AhuPartition ap : ahuPartitionList) {
				String[] temContents = null;
				temContents = SectionContentConvertUtils.getMPStrs(unit, ap, ahuPartitionList);
				partitionContents[posSetp+ap.getPos()] = temContents;
			}
			return partitionContents;
		}
		return null;
	}

    //封装综合过滤段报告数据
    public static String[][] getCombinedFilter(String[][] contents, Map<String, String> map) {

        return contents;
    }

    /**
     * 封装混合段报告特殊数据
     * @param contents
     * @param map
     * @return
     */
    public static String[][] getMix(String[][] contents, Map<String, String> noChangeMap,Map<String, String> map,LanguageEnum language) {

        int length = contents.length;
        //封装混合形式
        String returnTop = UtilityConstant.METASEXON_MIX_RETURNTOP;//1
        String returnBack = UtilityConstant.METASEXON_MIX_RETURNBACK;//2
        String returnLeft = UtilityConstant.METASEXON_MIX_RETURNLEFT;//3
        String returnRight = UtilityConstant.METASEXON_MIX_RETURNRIGHT;//4
        String returnButtom = UtilityConstant.METASEXON_MIX_RETURNBUTTOM;//5
        String uvLamp = UtilityConstant.METASEXON_MIX_UVLAMP;
        String damperExecutorKey = UtilityConstant.METASEXON_MIX_DAMPEREXECUTOR;



        int returnTopWF = 1;
        int returnBackWF = 2;
        int returnLeftWF = 3;
        int returnRightWF = 4;
        int returnButtomWF = 5;

        String dampertype = noChangeMap.get(UtilityConstant.METASEXON_MIX_DAMPEROUTLET);//风口接件
        String doorDirection = noChangeMap.get(UtilityConstant.METASEXON_MIX_DOORDIRECTION);//开门方向
        String serial = noChangeMap.get(UtilityConstant.METAHU_SERIAL);
        String damperMeterial = noChangeMap.get(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL);//风阀材料
        String mixSectionL = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_MIX_SECTIONL));//段长
		String damperExecutor = noChangeMap.get(damperExecutorKey);//执行器
		damperExecutor = MetaSelectUtil.getTextForSelect(damperExecutorKey, damperExecutor, language);


        int valvem = 1;//镀锌板
		if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(damperMeterial)) {
			valvem = 2;//铝合金
		}

        String product = noChangeMap.get(UtilityConstant.METAHU_PRODUCT);

        boolean boolFL =false;
        Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));

		if (SystemCalculateConstants.MIX_DAMPEROUTLET_FD.equals(dampertype)) {
			boolFL = true;
		}

		//风量设置
//		setAirVolume(contents, map, 1, 2);

		//室外温度
//		contents[12][6] = BaseDataUtil.constraintString(winterTemperature);

		//风阀电动+(执行器)情况
        if (map.containsKey(UtilityConstant.METASEXON_MIX_DAMPEROUTLET)) {
            String value = contents[2][2];
            if (EmptyUtil.isNotEmpty(damperExecutor)) {
            	if ((EmptyUtil.isNotEmpty(dampertype) && UtilityConstant.JSON_MIX_DAMPEROUTLET_FD.equals(dampertype))) {
            		contents[2][2] = value;
				} else if (EmptyUtil.isNotEmpty(dampertype)) {
					contents[2][2] = value + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + damperExecutor
							+ UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
				}
			}
        }


		String accessDoor = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_MIX_DOORO));//是否开门:
		if (!SystemCalculateConstants.MIX_DOORO_NODOOR.equals(accessDoor)) {//开门
			//添加：
//        	String doorOnBothSide = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_MIX_DOORDIRECTION));//两侧开门
			String value = contents[2][6];
			String xtDoorDirection = "";
			if(doorDirection.equals(MIX_DOORDIRECTION_RIGHT)){
				xtDoorDirection = getIntlString(TO_LEFT);
			}else if(doorDirection.equals(MIX_DOORDIRECTION_LEFT)){
				xtDoorDirection = getIntlString(TO_RIGHT);
			}else if(doorDirection.equals(MIX_DOORDIRECTION_BOTH)){
				xtDoorDirection = getIntlString(BOTH_SIDES);
			}

//            if (!value.equals(UtilityConstant.SYS_BLANK)) {
//            	if(UtilityConstant.SYS_ASSERT_TRUE.equals(doorOnBothSide)){
//            		value = getIntlString(BOTH_SIDES)+value;
//            	}
//            	contents[2][6] = value;
//            }

			String PositivePressureDoor = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_MIX_POSITIVEPRESSUREDOOR));//正压门
			if(Boolean.parseBoolean(PositivePressureDoor)){
				PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;
				String serials = noChangeMap.get(UtilityConstant.METAHU_SERIAL);
				XtDoor xtDoor = AhuMetadata.findOne(XtDoor.class, serials);
				String xtDoorSize = "";
				if(EmptyUtil.isNotEmpty(xtDoor)){
					xtDoorSize = xtDoor.getXtDoorw()+"*"+xtDoor.getXtDoorh();
				}
//                if(doorDirection.equals(MIX_DOORDIRECTION_RIGHT)){
//                    xtDoorDirection = getIntlString(TO_RIGHT);
//                }else if(doorDirection.equals(MIX_DOORDIRECTION_LEFT)){
//                    xtDoorDirection = getIntlString(TO_LEFT);
//                }
				PositivePressureDoor += xtDoorDirection +xtDoorSize+SYS_PUNCTUATION_SLIGHT_PAUSE;
				//正压门时，尺寸读取
				String doorSize = xtDoor.getXtDoorwSize() +"*"+ xtDoor.getXtDoorhSize();
				contents[4][6] = doorSize;
			}else{
				PositivePressureDoor = UtilityConstant.SYS_BLANK;
			}
			String value26 = contents[2][6];
			String fixRepairLamp = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_MIX_FIXREPAIRLAMP));//安装检修灯
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)){
				value26 = value26 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP) + UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
			}else{
				value26 = value26 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP) + UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
			}
			contents[2][6] = value26;
		}else{
			contents[4][6] = "--";
		}

        if (map.containsKey(returnBack)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnBack));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnButtom)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnButtom));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnLeft)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnLeft));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnRight)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnRight));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnTop)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnTop));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }

        if (map.containsKey(uvLamp)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(uvLamp));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
            	length--;
            }
        } else {
            length--;
        }

        int k=0;
        String[][] newContents = new String[length][7];
        for (int i = 0; i < contents.length; i++) {

        	int returnWF = 1;
			if (i == 6) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnTop));
                if (map.containsKey(returnTop) && value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                    returnWF = returnTopWF;
                }else{
                    continue;
                }
            }

            if (i == 7) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnButtom));
                if (map.containsKey(returnButtom) && value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                    returnWF = returnButtomWF;
                }else{
                    continue;
                }
            }

            if (i == 8) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnLeft));
                if (map.containsKey(returnLeft) && value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                    returnWF = returnLeftWF;
                }else{
                    continue;
                }
            }

            if (i == 9) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnRight));
                if (map.containsKey(returnRight) && value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                    returnWF = returnRightWF;
                }else{
                    continue;
                }
            }

            if (i == 10) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnBack));
                if (map.containsKey(returnBack) && value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                    returnWF = returnBackWF;
                }else{
                    continue;
                }
            }

            if(i == 6 || i == 7 || i == 8 || i == 9 || i == 10){
                DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class,
                        serial.substring(0, serial.length() - 4), returnWF + UtilityConstant.SYS_BLANK, 2==returnWF?damper.getSectionL():mixSectionL);
                String A = UtilityConstant.SYS_BLANK;
                String C = UtilityConstant.SYS_BLANK;

	            int lenth = serial.length();
				String heightStr = serial.substring(lenth - 4, lenth - 2);
				String weightStr = serial.substring(lenth - 2);
				int height = Integer.parseInt(heightStr);
				int width = Integer.parseInt(weightStr);

	    		if (EmptyUtil.isEmpty(nsion)) {
	    			logger.error("查表未获取到DamperDimension @封装综合过滤段报告数据 报告使用");
	    		} else {
	    			String metrial = noChangeMap.get(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL);
	    			if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(metrial)) {
	    				double c1 = Double.parseDouble(nsion.getC1());
	    				A = UtilityConstant.SYS_BLANK+(c1 - 2 * 40);
	    			} else {
	    				A = nsion.getA();
	    			}
	    			int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);

	    			if (returnWF == 1 || returnWF == 2) {
	    				C = UtilityConstant.SYS_BLANK+((width - 1) * 100 + DIMENSION_C_calPara - 72);
	    			}
	    			if (returnWF == 3 || returnWF == 4) {
	    				C = UtilityConstant.SYS_BLANK + ((height - 1) * 100 + DIMENSION_C_calPara - 72);
	    			}
	    			if (returnWF == 5) {
	    				C = UtilityConstant.SYS_BLANK+((width - 5) * 100 + DIMENSION_C_calPara - 72);
	    			}
	    		}
	    		if(!UtilityConstant.SYS_BLANK.equals(A) && !UtilityConstant.SYS_BLANK.equals(C)){
	    			contents[i][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
	    		}
                AdjustAirDoor ad = AhuMetadata.findOne(AdjustAirDoor.class, noChangeMap.get(UtilityConstant.METAHU_SERIAL), "A", UtilityConstant.SYS_BLANK + returnWF);
                if (EmptyUtil.isEmpty(ad)) {
					logger.error("查表未获取到AdjustairDoor @封装综合过滤段报告数据 报告使用"+returnWF);
				} else {
					if(!boolFL){
						if(2 == valvem){
							contents[i][6] = ad.getWringqAl();//回风阀门扭矩
						}else{
							contents[i][6] = ad.getWringq();//回风阀门扭矩
						}
					}
				}
            }

            if (i == 14) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(uvLamp));
                if (map.containsKey(uvLamp) && value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                        continue;
                }
            }
            for (int j = 0; j < 7; j++) {
                newContents[k][j] = contents[i][j];
            }
            k++;
        }
        return newContents;
    }

    /**
     * 封装单层过滤段数据
     * add by gaok2 20210107,板式或者袋式过滤器选择无过滤器时，过滤器效率不应该打印在技术说明中
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getSingle(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//设置风量
//    	setAirVolume(contents, map, 1, 2);
        if(JSON_FILTER_FILTEROPTIONS_NO.equals(map.get(METASEXON_FILTER_FILTEROPTIONS))){
            contents[2][2] = "";
        }
    	return contents;
    }

    /**
     * 封装综合过滤段
	 * add by gaok2 20210107,板式或者袋式过滤器选择无过滤器时，过滤器效率不应该打印在技术说明中
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getComposite(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//设置风量
//    	setAirVolume(contents, map, 1, 2);
        //板式过滤器选择无过滤器时，过滤器效率置为空
    	if(JSON_FILTER_FILTEROPTIONS_NO.equals(map.get(METASEXON_COMBINEDFILTER_LMATERIAL))){
            contents[2][2] = "";
        }
        //袋式过滤器选择无过滤器时，过滤器效率置为空
		if(JSON_FILTER_FILTEROPTIONS_NOFILTER.equals(map.get(METASEXON_COMBINEDFILTER_RMATERIAL))){
            contents[6][2] = "";
        }
    	return contents;
    }

    /**
     * 该方法需要重构
     */
    public static String[][] getMix4Sale(String[][] contents, Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        int length = contents.length;
        // 封装混合形式
        String returnTop = UtilityConstant.METASEXON_MIX_RETURNTOP;// 1
        String returnBack = UtilityConstant.METASEXON_MIX_RETURNBACK;// 2
        String returnLeft = UtilityConstant.METASEXON_MIX_RETURNLEFT;// 3
        String returnRight = UtilityConstant.METASEXON_MIX_RETURNRIGHT;// 4
        String returnButtom = UtilityConstant.METASEXON_MIX_RETURNBUTTOM;// 5
        int returnTopWF = 1;
        int returnBackWF = 2;
        int returnLeftWF = 3;
        int returnRightWF = 4;
        int returnButtomWF = 5;

        String serial = noChangeMap.get(UtilityConstant.METAHU_SERIAL);
        String damperMeterial = noChangeMap.get(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL);//风阀材料
        String mixSectionL = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_MIX_SECTIONL));//段长
        int valvem = 1;//镀锌板
        if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(damperMeterial)) {
            valvem = 2;//铝合金
        }

        String product = noChangeMap.get(UtilityConstant.METAHU_PRODUCT);

        boolean boolFL =false;
        Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));

        if (map.containsKey(returnBack)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnBack));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnButtom)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnButtom));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnLeft)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnLeft));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnRight)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnRight));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        if (map.containsKey(returnTop)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnTop));
            if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                length--;
            }
        } else {
            length--;
        }
        int k = 0;
        String[][] newContents = new String[length][7];
        for (int i = 0; i < contents.length; i++) {
            int returnWF = 1;
            if (map.containsKey(returnTop)) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnTop));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 2) {
                        continue;
                    }
                }else{
                    returnWF = returnTopWF;
                }
            } else {
                if (i == 2) {
                    continue;
                }
            }

            if (map.containsKey(returnButtom)) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnButtom));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 3) {
                        continue;
                    }
                }else{
                    returnWF = returnButtomWF;
                }
            } else {
                if (i == 3) {
                    continue;
                }
            }

            if (map.containsKey(returnLeft)) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnLeft));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 4) {
                        continue;
                    }
                }else{
                    returnWF = returnLeftWF;
                }
            } else {
                if (i == 4) {
                    continue;
                }
            }

            if (map.containsKey(returnRight)) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnRight));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 5) {
                        continue;
                    }
                }else{
                    returnWF = returnRightWF;
                }
            } else {
                if (i == 5) {
                    continue;
                }
            }

            if (map.containsKey(returnBack)) {
                String value = BaseDataUtil.constraintString(noChangeMap.get(returnBack));
                if (value.equals(UtilityConstant.SYS_ASSERT_FALSE)) {
                    if (i == 6) {
                        continue;
                    }
                }else{
                    returnWF = returnBackWF;
                }
            } else {
                if (i == 6) {
                    continue;
                }
            }

            if(i == 2 || i == 3 || i == 4 || i == 5 || i == 6){
                DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class,
                        serial.substring(0, serial.length() - 4), returnWF + UtilityConstant.SYS_BLANK, 2==returnWF?damper.getSectionL():mixSectionL);
                String A = UtilityConstant.SYS_BLANK;
                String C = UtilityConstant.SYS_BLANK;

                int lenth = serial.length();
                String heightStr = serial.substring(lenth - 4, lenth - 2);
                String weightStr = serial.substring(lenth - 2);
                int height = Integer.parseInt(heightStr);
                int width = Integer.parseInt(weightStr);

                if (EmptyUtil.isEmpty(nsion)) {
                    logger.error("查表未获取到DamperDimension @封装综合过滤段报告数据 报告使用");
                } else {
                    String metrial = noChangeMap.get(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL);
                    if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(metrial)) {
                        double c1 = Double.parseDouble(nsion.getC1());
                        A = UtilityConstant.SYS_BLANK+(c1 - 2 * 40);
                    } else {
                        A = nsion.getA();
                    }
                    int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);

                    if (returnWF == 1 || returnWF == 2) {
                        C = UtilityConstant.SYS_BLANK+((width - 1) * 100 + DIMENSION_C_calPara - 72);
                    }
                    if (returnWF == 3 || returnWF == 4) {
                        C = UtilityConstant.SYS_BLANK + ((height - 1) * 100 + DIMENSION_C_calPara - 72);
                    }
                    if (returnWF == 5) {
                        C = UtilityConstant.SYS_BLANK+((width - 5) * 100 + DIMENSION_C_calPara - 72);
                    }
                }
                if(!UtilityConstant.SYS_BLANK.equals(A) && !UtilityConstant.SYS_BLANK.equals(C)){
                    contents[i][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
                }
                AdjustAirDoor ad = AhuMetadata.findOne(AdjustAirDoor.class, noChangeMap.get(UtilityConstant.METAHU_SERIAL), "A", UtilityConstant.SYS_BLANK + returnWF);
                if (EmptyUtil.isEmpty(ad)) {
                    logger.error("查表未获取到AdjustairDoor @封装综合过滤段报告数据 报告使用"+returnWF);
                } else {
                    if(!boolFL){
                        if(2 == valvem){
                            contents[i][6] = ad.getWringqAl();//回风阀门扭矩
                        }else{
                            contents[i][6] = ad.getWringq();//回风阀门扭矩
                        }
                    }
                }
            }
            for (int j = 0; j < 7; j++) {
                newContents[k][j] = contents[i][j];
            }
            k++;
        }
        return newContents;
    }

    /**
     * 封装风机段报告特殊数据
     * @param contents
     * @param map
     * @return
     */
    public static String[][] getFan(String[][] contents, Map<String, String> noChangeMap, Map<String, String> map,LanguageEnum language) {

        String accessDoor = UtilityConstant.METASEXON_FAN_ACCESSDOOR;//是否开门:

        //是否开门需要添加：
        String doorOnBothSide = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_DOORONBOTHSIDE));//两侧开门
        String fixRepairLamp = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_FIXREPAIRLAMP));//安装检修灯
		String ahuDoorDirection = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METAHU_DOORORIENTATION));//开门方向
		String fanSectionL = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_SECTIONL));//风机段长

		if (map.containsKey(accessDoor)) {
            String value = contents[1][7];
            if (!value.equals(UtilityConstant.SYS_BLANK)) {
            	if(UtilityConstant.SYS_ASSERT_TRUE.equals(doorOnBothSide)){
            		value = getIntlString(BOTH_SIDES)+value;
            	}
            	contents[1][7] = value;
            }
        }

        String sSerial = noChangeMap.get(METAHU_SERIAL);//是否开门:
        //如果为无蜗壳风机段且选中安装正压门，则判断是否需要增加段长
        PositivePressureDoorUtil positivePressureDoorUtil = new PositivePressureDoorUtil();
        PositivePressureDoorParam positivePressureDoorParam = positivePressureDoorUtil.getPositivePressureDoorSituation(sSerial,noChangeMap);
        if(EmptyUtil.isNotEmpty(positivePressureDoorParam)){
            //设置是否开门属性
            String PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;
            String xtDoorDirection = "";
            if(ahuDoorDirection.equals(AHU_DOORORIENTATION_LEFT)){
                xtDoorDirection = getIntlString(TO_RIGHT);
            }else if(ahuDoorDirection.equals(AHU_DOORORIENTATION_RIGHT)){
                xtDoorDirection = getIntlString(TO_LEFT);
            }
			String doorSizeDesc = positivePressureDoorParam.getDoor();
//					(positivePressureDoorParam.getDoorW()>100?positivePressureDoorParam.getDoorW()/100:positivePressureDoorParam.getDoorW())
//					+"*"+ (positivePressureDoorParam.getDoorH()>100?positivePressureDoorParam.getDoorH()/100:positivePressureDoorParam.getDoorH());
            PositivePressureDoor += xtDoorDirection + doorSizeDesc + SYS_PUNCTUATION_SLIGHT_PAUSE;
            String value17 = contents[1][7];
            if(UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)){
                value17 = value17 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
            }else{
                value17 = value17 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
            }
            contents[1][7] = value17;
            //设置检修门尺寸
            String doorSize = positivePressureDoorParam.getDoorW()+"*"+ positivePressureDoorParam.getDoorH();
            contents[2][7] = doorSize;
            //设置检修门安装高度
            contents[2][0] = AHUContext.getIntlString(DOOR_INSTALL_HEIGHT);
            contents[2][1] = "mm";
            contents[2][2] = positivePressureDoorParam.getDoorInstallH() + "";
        }else{
            String PositivePressureDoor = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_POSITIVEPRESSUREDOOR));//正压门

            if(Boolean.parseBoolean(PositivePressureDoor)){
                PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;

                /*请在选型报告中增加门尺寸及方向，其中尺寸数据从表格中读取
                段长等于5M时，读取SECTIONL=5
                段长大于5M时,读取SECTIONL=6*/

                XtDoor xtDoor = AhuMetadata.findOne(XtDoor.class, noChangeMap.get(UtilityConstant.METAHU_SERIAL), String.valueOf(NumberUtil.convertStringToDoubleInt(fanSectionL)>5?6:5));
                String xtDoorSize = "";
                if(EmptyUtil.isNotEmpty(xtDoor)){
                    xtDoorSize = xtDoor.getXtDoorw()+"*"+xtDoor.getXtDoorh();
                }
                String xtDoorDirection = "";
                if(ahuDoorDirection.equals(AHU_DOORORIENTATION_LEFT)){
                    xtDoorDirection = getIntlString(TO_RIGHT);
                }else if(ahuDoorDirection.equals(AHU_DOORORIENTATION_RIGHT)){
                    xtDoorDirection = getIntlString(TO_LEFT);
                }
                PositivePressureDoor += xtDoorDirection +xtDoorSize + SYS_PUNCTUATION_SLIGHT_PAUSE;
				//正压门时，尺寸读取
				String doorSize = xtDoor.getXtDoorwSize() +"*"+ xtDoor.getXtDoorhSize();
				contents[2][7] = doorSize;
            }else{
                PositivePressureDoor = UtilityConstant.SYS_BLANK;
            }

            String value17 = contents[1][7];
            if(UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)){
                value17 = value17 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
            }else{
                value17 = value17 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
            }
            contents[1][7] = value17;
        }

        //重新计算电机偏移距离，因为之前的电机偏移有问题，导致已经流转到工厂的订单，生成的技术报告有问题，现在重新生成技术报告
        String motorP = calMotorP(map);
        //contents[14][7] = motorP.split("\\|\\|")[0];
        contents[17][7] = motorP;

//    	//计算轴功率
//    	double airVolume = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
//    	double totalPressure = Double.parseDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
//    	double efficiency = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
//    	double shaftPower = (airVolume*totalPressure*100)/(3600*efficiency*1000);
//    	BigDecimal shaftPowerBd = new BigDecimal(shaftPower);//四舍五入，保留两位小数
//    	shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
//    	contents[4][7] = String.valueOf(shaftPowerBd);
//
//    	//计算输入功率
//    	double motorSafetyFacor = new Double(0);
//    	double motorPower = BaseDataUtil.stringConversionDouble((String)noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
//    	if(motorPower<0.5) {
//    		motorSafetyFacor = 1.5;
//    	}else if (motorPower>0.5 && motorPower<1) {
//			motorSafetyFacor = 1.4;
//		}else if (motorPower>1 && motorPower<2) {
//			motorSafetyFacor = 1.3;
//		}else if (motorPower>2 && motorPower<5) {
//			motorSafetyFacor = 1.2;
//		}else if(motorPower>5) {
//			motorSafetyFacor = 1.15;
//		}
//    	double actualMotorPower = (shaftPower*motorSafetyFacor)/(1);
//    	System.out.println("#############################actualMotorPower=========="+actualMotorPower);
//    	String motorEfficiencyStr = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFF));
//    	NumberFormat nf=NumberFormat.getPercentInstance();
//    	Number motorEfficiencyNum;
//		try {
//			if(EmptyUtil.isNotEmpty(motorEfficiencyStr)) {
//				motorEfficiencyNum = nf.parse(motorEfficiencyStr);
//			}else {
//				motorEfficiencyNum = 0;
//			}
//
//			double motorEfficiency = motorEfficiencyNum.doubleValue();
//			if(motorEfficiency==0) {
//	    		motorEfficiency = 0.94;
//	    	}
//			double inputPower = (motorPower) / (motorEfficiency);
//			BigDecimal inputPowerBd = new BigDecimal(inputPower);// 四舍五入，保留两位小数
//			inputPowerBd = inputPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
//			contents[6][7] = String.valueOf(inputPowerBd);
//			if (UtilityConstant.SYS_STRING_NUMBER_4.equals(noChangeMap.get(UtilityConstant.METASEXON_FAN_TYPE))) {// 双速电机输入功率从表s_twospeedmotor表中取
//				int fanPole = BaseDataUtil
//						.stringConversionInteger(noChangeMap.get(UtilityConstant.META_SECTION_FAN_POLE));
//				double twoSpeedMotorPower = BaseDataUtil
//						.stringConversionDouble(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
//				List<STwoSpeedMotor> sTwoSpeedMotorList = AhuMetadata.findAll(STwoSpeedMotor.class);
//				for (STwoSpeedMotor sTwoSpeedMotor : sTwoSpeedMotorList) {
//					if (sTwoSpeedMotor.getJs() == fanPole && sTwoSpeedMotor.getGl() == twoSpeedMotorPower) {
//						contents[6][7] = sTwoSpeedMotor.getInputPower();
//						break;
//					}
//				}
//			}
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}

		//添加电机效率
		String oldValue = contents[12][7];
		String outlet = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_OUTLET)));//风机形式
		String type = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_TYPE));//电机类型
		String motorEff = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFF));//电机效率
		String hz = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_HZ)));//变频频率
		String twoSpeed = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFFPOLE)));//变频频率
		String runningRage=BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_FREQUENCYRANGE)));
		if (UtilityConstant.SYS_STRING_NUMBER_6.equals(type)) {//无蜗壳风机，电机类型后边的括号中展示变频频率
			if (EmptyUtil.isNotEmpty(hz)) {
				contents[12][7]= oldValue + "（"+ getIntlString(I18NConstants.FAN_RUNNING_FREQUENCY) + hz + "）";
			}
		}else if(UtilityConstant.SYS_STRING_NUMBER_5.equals(type)){
				contents[12][7]= oldValue + "（"+ getIntlString(I18NConstants.FAN_RUNNING_RANGE) + runningRage + getIntlString(I18NConstants.FAN_VMOTOR_COMMENTS)+ "）";
		}else if(UtilityConstant.SYS_STRING_NUMBER_4.equals(type)) {
			contents[12][7]=oldValue + "（" + twoSpeed + "）";
		}else{//其他形式风机，电机类型后边的括号中展示电机效率
			contents[12][7]= oldValue + "（" + motorEff + "）";
		}

		//根据风向（送/回），分别设置风量、风口接件
		String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
        if (map.containsKey(airDirection)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(airDirection));
            if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {//回风
            	//设置风量
//            	contents[4][2] = String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME));
				// 设置风口接件
				String[] temp = contents[14][2].split(UtilityConstant.SYS_PUNCTUATION_SLASH);
				contents[14][2] = temp[2] + UtilityConstant.SYS_PUNCTUATION_SLASH + temp[3];
            }else {//送风
            	//设置风量
//            	contents[4][2] = String.valueOf(noChangeMap.get(UtilityConstant.METAHU_SAIRVOLUME));
				// 设置风口接件
				String[] temp = contents[14][2].split(UtilityConstant.SYS_PUNCTUATION_SLASH);
				contents[14][2] = temp[0] + UtilityConstant.SYS_PUNCTUATION_SLASH + temp[1];
            }
        }

		//设置风量
//        setAirVolume(contents, map, 4, 2);

        if (AHUContext.isExportVersion()) {// 判断是否出口版   出口版没有ptc选项
    		ArrayList<String[]> templist = new ArrayList<String[]>(Arrays.asList(contents));
    		ArrayList<String[]> templist1 = new ArrayList<>();
    		for (int i = 0; i < templist.size(); i++) {
				if (i == 17) {
					String[] noPtc = templist.get(i);
					noPtc[0]= noPtc[5];
					noPtc[1]= noPtc[6];
					noPtc[2]= noPtc[7];
					noPtc[5]= UtilityConstant.SYS_BLANK;
					noPtc[6]= UtilityConstant.SYS_BLANK;
					noPtc[7]= UtilityConstant.SYS_BLANK;
					templist1.add(noPtc);// 删除转轮电机功率
				}else {
					templist1.add(templist.get(i));
				}
    		}

    		String[][] newContent = new String[templist1.size()][contents[0].length];
    		templist1.toArray(newContent);
    		contents = newContent;
		}

		// 1117以下机型，并且以BDB开头的风机系列需要添加补偿静压200Pa
		String serial = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METAHU_SERIAL));
		int serialNum = BaseDataUtil.stringConversionInteger(serial.substring(serial.length() - 4, serial.length()));// 获取机组序列号整数部分
		if (serialNum < 1117 && serialNum < 1117 && (noChangeMap.get(UtilityConstant.METASEXON_FAN_FANMODEL).startsWith("BDB")||noChangeMap.get(UtilityConstant.METASEXON_FAN_FANMODEL).startsWith("ADA")||noChangeMap.get(UtilityConstant.METASEXON_FAN_FANMODEL).startsWith("FDA"))) {
			ArrayList<String[]> templist = new ArrayList<String[]>(Arrays.asList(contents));
			ArrayList<String[]> templist1 = new ArrayList<>();
			for (int i = 0; i < templist.size(); i++) {
				if (i == 7) {
					// 添加一行显示补偿静压
					templist1.add(templist.get(i));
					templist1.add(new String[] { getIntlString(I18NConstants.COMPENSATION_STATIC_PRESSURE), "Pa", "200",
							"", "", "", "", "" });
				} else if (i == 9) {
					// 全静压+200Pa
					String tspStr = templist.get(i)[2];
					tspStr = BaseDataUtil.doubleToString(BaseDataUtil.stringConversionDouble(tspStr), 1);
					templist.get(i)[2] = tspStr;
					templist1.add(templist.get(i));
				} else if (i == 8) {
					// 全静压+200Pa
					String tspStr = templist.get(i)[2];
					tspStr = BaseDataUtil.doubleToString(BaseDataUtil.stringConversionDouble(tspStr)+200, 1);
					templist.get(i)[2] = tspStr;
					templist1.add(templist.get(i));
				}else {
					templist1.add(templist.get(i));
				}
			}
			if (templist1.size() > 0) {
				String[][] newContent = new String[templist1.size()][contents[0].length];
				templist1.toArray(newContent);
				contents = newContent;
			}
		}


        return contents;
    }

	public static String calMotorP(Map<String, String> map) {
		String motorP = "";
		String fanType = BaseDataUtil.constraintString(map.get(METAHU_SERIAL));//机组型号
		String supplier = BaseDataUtil.constraintString(map.get(METASEXON_FAN_SUPPLIER));//电机品牌
		int fanHeight = SystemCountUtil.getUnitHeight(fanType);// 风机高度
		String motorBaseNo = BaseDataUtil.constraintString(map.get(METASEXON_FAN_MOTORBASENO));//机座号

		String code = BaseDataUtil.StringConversionNumber(BaseDataUtil.constraintString(map.get(METASEXON_FAN_FANMODEL)));
		String startStyle = BaseDataUtil.constraintString(map.get(METASEXON_FAN_STARTSTYLE));
		String fanTypeCode = "39CQ" + SystemCountUtil.getUnit(fanType);
		String fanOutlet = BaseDataUtil.constraintString(map.get(METASEXON_FAN_OUTLET));
		String motorPosition = BaseDataUtil.constraintString(map.get(METASEXON_FAN_MOTORPOSITION));

		if(SystemCalculateConstants.FAN_MOTORPOSITION_BACK.equals(motorPosition)){
			return motorP;
		}

		if(!SystemCalculateConstants.FAN_OPTION_OUTLET_WWK.equals(fanOutlet)) {

			String motorType = BaseDataUtil.constraintString(map.get(METASEXON_FAN_TYPE));
			//String motorPosition = "";

			if (SystemCountUtil.lteSmallUnit(Integer.parseInt(SystemCountUtil.getUnit(fanType)))) {//fanHeight <= 23
				FanCodeElectricSize fanCodeElectricSize = MotorPositionUtil.getSmallFanSize(code, fanTypeCode);
				double inWidth = MotorPositionUtil.getSmallMotorSize(motorType, code, supplier, motorBaseNo, startStyle,
						fanCodeElectricSize);
				Integer INNERWIDTH = fanCodeElectricSize.getINNERWIDTH();
				Integer P = fanCodeElectricSize.getP();

				if (inWidth > INNERWIDTH) {
					motorPosition = SystemCalculateConstants.FAN_MOTORPOSITION_BACK;
				} else {
					motorPosition = SystemCalculateConstants.FAN_MOTORPOSITION_SIDE;
					//motorP = BaseDataUtil.integerConversionString(0);
				}
				/*2.当电机为后置时增加是否偏移计算逻辑*/
				if (null != motorPosition && SystemCalculateConstants.FAN_MOTORPOSITION_BACK.equals(motorPosition)) {
					if (inWidth - P <= INNERWIDTH) {
						motorPosition = SystemCalculateConstants.FAN_MOTORPOSITION_SIDE;
						motorP = BaseDataUtil.integerConversionString(P);
					}

				}
			} else {//大机组侧后置逻辑
				/*风机型号大于等于1400以上只能后置*/
				if (BaseDataUtil.stringConversionInteger(code) >= 1400) {
					motorPosition = SystemCalculateConstants.FAN_MOTORPOSITION_BACK;
				} else {
					FanCodeElectricSizeBig fanCodeElectricSizeBig = MotorPositionUtil.getBigFanSIze(code, fanTypeCode);

					double P = fanCodeElectricSizeBig.getP();
					double inWidth = MotorPositionUtil.getBigUnitMotorSize(motorType, code, supplier, motorBaseNo, code, startStyle,
							fanTypeCode, fanCodeElectricSizeBig);
					if (inWidth < 100) {
						motorPosition = SystemCalculateConstants.FAN_MOTORPOSITION_BACK;
					} else {
						motorPosition = SystemCalculateConstants.FAN_MOTORPOSITION_SIDE;
						//motorP = BaseDataUtil.doubleConversionString(0.0);
					}
					/*2.当电机为后置时增加是否偏移计算逻辑*/
					if (null != motorPosition && SystemCalculateConstants.FAN_MOTORPOSITION_BACK.equals(motorPosition)) {
						if (inWidth - P < 100) {
							motorPosition = SystemCalculateConstants.FAN_MOTORPOSITION_SIDE;
							motorP = BaseDataUtil.doubleConversionString(P);
						}
					}
				}
			}
		}
		return motorP;
	}

    /**
     * 封装风机段报告特殊数据（欧标）
     * @param contents
     * @param noChangeMap
     * @param map
     * @param language
     * @return
     */
    public static String[][] getFanEuro(String[][] contents, Map<String, String> noChangeMap, Map<String, String> map,LanguageEnum language) {
    	String accessDoor = UtilityConstant.METASEXON_FAN_ACCESSDOOR;//是否开门:

        //是否开门需要添加：
        String doorOnBothSide = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_DOORONBOTHSIDE));//两侧开门
        String fixRepairLamp = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_FIXREPAIRLAMP));//安装检修灯
		String ahuDoorDirection = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METAHU_DOORORIENTATION));//开门方向
		String fanSectionL = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_SECTIONL));//风机段长

        if (map.containsKey(accessDoor)) {
//            String value = contents[1][7];
            String value = contents[1][7];
            if (!value.equals(UtilityConstant.SYS_BLANK)) {
            	if(UtilityConstant.SYS_ASSERT_TRUE.equals(doorOnBothSide)){
            		value = getIntlString(BOTH_SIDES)+value;
            	}
            	contents[1][7]=value;
            }
        }

        //重新计算电机偏移距离，因为之前的电机偏移有问题，导致已经流转到工厂的订单，生成的技术报告有问题，现在重新生成技术报告
        String motorP = calMotorP(map);
        contents[19][7] = motorP;

        String sSerial = noChangeMap.get(METAHU_SERIAL);//是否开门:
        //如果为无蜗壳风机段且选中安装正压门，则判断是否需要增加段长
        PositivePressureDoorUtil positivePressureDoorUtil = new PositivePressureDoorUtil();
        PositivePressureDoorParam positivePressureDoorParam = positivePressureDoorUtil.getPositivePressureDoorSituation(sSerial,noChangeMap);
        if(EmptyUtil.isNotEmpty(positivePressureDoorParam)){
            //设置是否开门属性
            String PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;
            String xtDoorDirection = "";
            if(ahuDoorDirection.equals(AHU_DOORORIENTATION_LEFT)){
                xtDoorDirection = getIntlString(TO_RIGHT);
            }else if(ahuDoorDirection.equals(AHU_DOORORIENTATION_RIGHT)){
                xtDoorDirection = getIntlString(TO_LEFT);
            }
			String doorSizeDesc = positivePressureDoorParam.getDoor();
//                    (positivePressureDoorParam.getDoorW()>100?positivePressureDoorParam.getDoorW()/100:positivePressureDoorParam.getDoorW())
//					+"*"+ (positivePressureDoorParam.getDoorH()>100?positivePressureDoorParam.getDoorH()/100:positivePressureDoorParam.getDoorH());
            PositivePressureDoor += xtDoorDirection+doorSizeDesc  + SYS_PUNCTUATION_SLIGHT_PAUSE;
            String value17 = contents[1][7];
            if(UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)){
                value17 = value17 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
            }else{
                value17 = value17 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
            }
            contents[1][7] = value17;
            //设置检修门尺寸
            String doorSize = positivePressureDoorParam.getDoorW() +"*"+ positivePressureDoorParam.getDoorH();
            contents[2][7] = doorSize;
            //设置检修门安装高度
            contents[2][0] = AHUContext.getIntlString(DOOR_INSTALL_HEIGHT);
            contents[2][1] = "mm";
            contents[2][2] = positivePressureDoorParam.getDoorInstallH() + "";
        }else{
            String PositivePressureDoor = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_POSITIVEPRESSUREDOOR));//正压门
            if(Boolean.parseBoolean(PositivePressureDoor)){
                PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;

            /*请在选型报告中增加门尺寸及方向，其中尺寸数据从表格中读取
			段长等于5M时，读取SECTIONL=5
			段长大于5M时,读取SECTIONL=6*/

                XtDoor xtDoor = AhuMetadata.findOne(XtDoor.class, noChangeMap.get(UtilityConstant.METAHU_SERIAL), String.valueOf(NumberUtil.convertStringToDoubleInt(fanSectionL)>5?6:5));
                String xtDoorSize = "";
                if(EmptyUtil.isNotEmpty(xtDoor)){
                    xtDoorSize = xtDoor.getXtDoorw()+"*"+xtDoor.getXtDoorh();
                }
                String xtDoorDirection = "";
                if(ahuDoorDirection.equals(AHU_DOORORIENTATION_LEFT)){
                    xtDoorDirection = getIntlString(TO_RIGHT);
                }else if(ahuDoorDirection.equals(AHU_DOORORIENTATION_RIGHT)){
                    xtDoorDirection = getIntlString(TO_LEFT);
                }
                PositivePressureDoor += xtDoorDirection +xtDoorSize + UtilityConstant.SYS_PUNCTUATION_SLIGHT_PAUSE;
				//正压门时，尺寸读取
				String doorSize = xtDoor.getXtDoorwSize() +"*"+ xtDoor.getXtDoorhSize();
				contents[2][7] = doorSize;
            }else{
                PositivePressureDoor = UtilityConstant.SYS_BLANK;
            }

//        String value17 = contents[1][7];
            String value17 = contents[1][7];
            if(UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)){
                value17 = value17 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
            }else{
                value17 = value17 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+ PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP)+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
            }
            contents[1][7] = value17;
        }


		//添加电机效率
		String oldValue = contents[13][7];
//		String oldValue = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_TYPE));
		String outlet = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_OUTLET)));//风机形式
		String type = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_TYPE));//电机类型
		String motorEff = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFF));//电机效率
		String hz = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_HZ)));//变频频率
		String twoSpeed = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFFPOLE)));//变频频率
		String runningRage=BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_FREQUENCYRANGE)));
		if (UtilityConstant.SYS_STRING_NUMBER_6.equals(type)) {//无蜗壳风机，电机类型后边的括号中展示变频频率
			if (EmptyUtil.isNotEmpty(hz)) {
				contents[13][7]= oldValue + "（"+ getIntlString(I18NConstants.FAN_RUNNING_FREQUENCY) + hz + "）";
			}
		}else if(UtilityConstant.SYS_STRING_NUMBER_5.equals(type)){
				contents[13][7]= oldValue + "（"+ getIntlString(I18NConstants.FAN_RUNNING_RANGE) + runningRage + getIntlString(I18NConstants.FAN_VMOTOR_COMMENTS)+ "）";
		}else if(UtilityConstant.SYS_STRING_NUMBER_4.equals(type)) {
			contents[13][7]=oldValue + "（" + twoSpeed + "）";
		}else{//其他形式风机，电机类型后边的括号中展示电机效率
			contents[13][7]= oldValue + "（" + motorEff + "）";
		}

		//根据风向（送/回），分别设置风量、风口接件
		String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
        if (map.containsKey(airDirection)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(airDirection));
            if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {//回风
            	//设置风量
//            	contents[4][2] = String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME));
				// 设置风口接件
				String[] temp = contents[16][2].split(UtilityConstant.SYS_PUNCTUATION_SLASH);//此处为“meta.section.fan.sendPosition/meta.section.fan.supplyDamper/meta.section.fan.returnPosition/meta.section.fan.returnDamper”不能从map中取
				contents[16][2] = temp[2] + UtilityConstant.SYS_PUNCTUATION_SLASH + temp[3];
            }else {//送风
            	//设置风量
//            	contents[4][2] = String.valueOf(noChangeMap.get(UtilityConstant.METAHU_SAIRVOLUME));
				// 设置风口接件
				String[] temp = contents[16][2].split(UtilityConstant.SYS_PUNCTUATION_SLASH);//同上
				contents[16][2] = temp[0] + UtilityConstant.SYS_PUNCTUATION_SLASH + temp[1];
            }
        }
        //ISP
//		double ISP = (BaseDataUtil.stringConversionDouble(contents[9][2])
//				- BaseDataUtil.stringConversionDouble(contents[5][2])) / 1.1;
//        contents[6][2] = String.valueOf(BaseDataUtil.doubleToString(ISP, 2));

        //System Effect
//        double systemEffect = ISP*0.1;
        String sysStaticStr = BaseDataUtil.constraintString(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_SYSSTATIC)));
        double ISP = BaseDataUtil.stringConversionDouble(sysStaticStr)/11*10;
//		double ISP = BaseDataUtil.stringConversionDouble(contents[6][2])/11*10;
//		double systemEffect = ISP * 0.1;
//		double systemEffect = BaseDataUtil.stringConversionDouble(contents[6][2])/11;
        double systemEffect = BaseDataUtil.stringConversionDouble(sysStaticStr)/11;
		contents[7][2] = String.valueOf(BaseDataUtil.doubleToString(systemEffect, 1));
		contents[6][2] = BaseDataUtil.constraintString(BaseDataUtil.doubleToString(ISP, 1));

        //SFPint
        double inputPower = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_INPUTPOWER)));
        String airVolumeStr = BaseDataUtil.constraintString(String.valueOf(map.get(UtilityConstant.METAHU_AIRVOLUME)));
        String outletVelocityStr = BaseDataUtil.constraintString(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_OUTLETVELOCITY)));
//        double airVolume = BaseDataUtil.stringConversionDouble(contents[4][2]);
        double airVolume = BaseDataUtil.stringConversionDouble(airVolumeStr);
        double extraStatic = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_EXTRASTATIC)));
        double totalStatic = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_TOTALSTATIC)));

//        double tp = BaseDataUtil.stringConversionDouble(contents[11][2]);
        double tp = BaseDataUtil.stringConversionDouble(outletVelocityStr);
        double SFPint = ((ISP+extraStatic)*inputPower)/((airVolume/3600)*(totalStatic));
        contents[13][2] = String.valueOf(BaseDataUtil.doubleToString(SFPint, 2));

//        //TSP = ESP +ISP+ System effect.
//		contents[9][2] = String.valueOf(BaseDataUtil.doubleToString(BaseDataUtil.stringConversionDouble(contents[5][2])
//				+ BaseDataUtil.stringConversionDouble(contents[6][2])
//				+ BaseDataUtil.stringConversionDouble(contents[7][2]), 0));

        //其他静压
//        double osp = BaseDataUtil.stringConversionDouble(contents[8][2]);
        double osp = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_EXTRASTATIC)));

		// TSP = ESP + ISP + System effect + other static pressure.
        String externalStaticStr = String.valueOf(map.get(UtilityConstant.METASEXON_FAN_EXTERNALSTATIC));
//		contents[9][2] = String.valueOf(BaseDataUtil
//				.doubleToString(BaseDataUtil.stringConversionDouble(contents[5][2]) + ISP + systemEffect + osp, 0));
        contents[9][2] = String.valueOf(BaseDataUtil
				.doubleToString(BaseDataUtil.stringConversionDouble(externalStaticStr) + ISP + systemEffect + osp, 0));

		//1117以下机型，并且以BDB开头的风机系列需要添加补偿静压200Pa
		String serial = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METAHU_SERIAL));
		int serialNum = BaseDataUtil
				.stringConversionInteger(serial.substring(serial.length()-4, serial.length()));//获取机组序列号整数部分
		if (serialNum < 1117 && serialNum < 1117 && (noChangeMap.get(UtilityConstant.METASEXON_FAN_FANMODEL).startsWith("BDB")||noChangeMap.get(UtilityConstant.METASEXON_FAN_FANMODEL).startsWith("ADA")||noChangeMap.get(UtilityConstant.METASEXON_FAN_FANMODEL).startsWith("FDA"))) {
			ArrayList<String[]> templist = new ArrayList<String[]>(Arrays.asList(contents));
			ArrayList<String[]> templist1 = new ArrayList<>();
			for (int i = 0; i < templist.size(); i++) {
				if (i == 7) {
					//添加一行显示补偿静压
					templist1.add(templist.get(i));
					templist1.add(new String[] { getIntlString(I18NConstants.COMPENSATION_STATIC_PRESSURE), "Pa", "200",
							"", "", "", "", "" });
				}else if (i == 9) {
					//全静压+200Pa
					String tspStr = templist.get(i)[2];
					tspStr = BaseDataUtil.doubleToString(BaseDataUtil.stringConversionDouble(tspStr)+200,1);
					templist.get(i)[2] = tspStr;
					templist1.add(templist.get(i));
				}else {
					templist1.add(templist.get(i));
				}
			}
			if(templist1.size()>0) {
				String[][] newContent = new String[templist1.size()][contents[0].length];
				templist1.toArray(newContent);
				contents = newContent;
			}
		}

    	return contents;
    }

	public static String[][] getFan4Sale(String[][] contents, Map<String, String> noChangeMap, Map<String, String> map,
			LanguageEnum language) {
		// 添加电机效率
		String oldValue = contents[3][7];
		// String motorEff =
		// BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_MOTOREFF));//
		// 电机效率
		// contents[3][7] = oldValue + "（" + motorEff + "）";
		String outlet = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_OUTLET)));// 风机形式
		String type = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_TYPE));// 电机类型
		String motorEff = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFF));// 电机效率
		String hz = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_HZ)));// 变频频率
		String twoSpeed = BaseDataUtil.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFFPOLE)));// 变频频率
		String runningRage = BaseDataUtil
				.constraintString((noChangeMap.get(UtilityConstant.METASEXON_FAN_FREQUENCYRANGE)));
		if (UtilityConstant.SYS_STRING_NUMBER_6.equals(type)) {// 无蜗壳风机，电机类型后边的括号中展示变频频率
			if (EmptyUtil.isNotEmpty(hz)) {
				contents[3][7] = oldValue + "（" + getIntlString(I18NConstants.FAN_RUNNING_FREQUENCY) + hz + "）";
			}
		} else if (UtilityConstant.SYS_STRING_NUMBER_5.equals(type)) {
			contents[3][7] = oldValue + "（" + getIntlString(I18NConstants.FAN_RUNNING_RANGE) + runningRage
					+ getIntlString(I18NConstants.FAN_VMOTOR_COMMENTS) + "）";
		} else if (UtilityConstant.SYS_STRING_NUMBER_4.equals(type)) {
			contents[3][7] = oldValue + "（" + twoSpeed + "）";
		} else {// 其他形式风机，电机类型后边的括号中展示电机效率
			contents[3][7] = oldValue + "（" + motorEff + "）";
		}

		return contents;
	}

    /**
     * 封装消音段
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getAttenuator(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//设置风量
    	setAirVolume(contents, map, 1, 2);
		String deadening = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_ATTENUATOR_DEADENING));
		String type = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_ATTENUATOR_TYPE));

		String[] arra = null;
		if (deadening.equals("A")) {
			if (type.equals("RS")) {
				arra = new String[]{"5", "10", "13", "14", "16", "16", "12", "10"};
			} else {
				arra = new String[]{"6.5", "9", "12", "22", "30", "29.5", "21.5", "12.5"};
			}
		} else if (deadening.equals("B")) {
			if (type.equals("RS")) {
				arra = new String[]{"7", "13", "21", "23", "26", "25", "20", "15"};
			} else {
				arra = new String[]{"7.5", "15", "20", "34", "40", "40.5", "34.5", "21.5"};
			}
		}
		if(null != arra && arra.length>0){
			contents[4][2] = arra[0];
			contents[4][3] = arra[1];
			contents[4][4] = arra[2];
			contents[4][5] = arra[3];
			contents[4][6] = arra[4];
			contents[4][7] = arra[5];
			contents[4][8] = arra[6];
			contents[4][9] = arra[7];
		}
    	return contents;
    }

    /**
     * 封装转轮热回收特殊数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getWheelHeatRecycle(String[][] contents, Map<String, String> map,LanguageEnum language) {

    	String Model = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_MODEL));
    	if(EmptyUtil.isNotEmpty(Model)) {
    		if (UtilityConstant.SYS_STRING_NUMBER_1.equals(Model)) {
				contents[4][2] = getIntlString(MOLECULAR_OPTION);
			}else if (UtilityConstant.SYS_STRING_NUMBER_2.equals(Model)) {
				contents[4][2] = getIntlString(SILICA_GEL_OPTION);
			}else if (UtilityConstant.SYS_STRING_NUMBER_3.equals(Model)) {
				contents[4][2] = getIntlString(PURE_ALUMINUM_OPTION);
			}
    	}

    	String wheelHeatRecycle = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_HEATRECYCLEDETAIL));

    	List<String> heatRecycleInfolvl0 = new ArrayList<String>();
    	List<HeatRecycleInfo> heatRecycleInfo = new ArrayList<HeatRecycleInfo>();
    	if (EmptyUtil.isNotEmpty(wheelHeatRecycle)) {

			heatRecycleInfolvl0 = JSON.parseArray(wheelHeatRecycle, String.class);//封装了两层list
			for(int a=0;a<heatRecycleInfolvl0.size();a++) {
				List<HeatRecycleInfo> tempHeatRecycleInfo = JSON.parseObject(heatRecycleInfolvl0.get(a), new TypeToken<List<HeatRecycleInfo>>() {
				}.getType());
				for (HeatRecycleInfo heatRecycleInfo2 : tempHeatRecycleInfo) {
					heatRecycleInfo.add(heatRecycleInfo2);
				}
			}

		}

    	String wheelDia = "";
    	if (heatRecycleInfo.size() > 0) {
			for (int i = 0; i < heatRecycleInfo.size(); i++) {
				HeatRecycleInfo hri = (HeatRecycleInfo)heatRecycleInfo.get(i);
				String season = hri.getReturnSeason();
				if(EmptyUtil.isNotEmpty(season) && UtilityConstant.JSON_AHU_SEASON_SUMMER.equals(season)) {//填充夏季数据
					//效率展示
			    	String[] seEff = hri.getReturnSEfficiency().split("/");//送风
			    	if (seEff.length>2) {
			    		contents[18][2] = EmptyUtil.isEmpty(seEff[1])?"":seEff[1];//temperatureEfficiency
				    	contents[19][2] = EmptyUtil.isEmpty(seEff[2])?"":seEff[2];//humidityEfficiency
					}
			    	String[] eeEff = hri.getReturnEEfficiency().split("/");//排风
			    	if (eeEff.length>2) {
				    	contents[20][2] = EmptyUtil.isEmpty(eeEff[1])?"":eeEff[1];//temperatureEfficiency
				    	contents[21][2] = EmptyUtil.isEmpty(eeEff[2])?"":eeEff[2];//humidityEfficiency
					}

					contents[6][5] = hri.getReturnWheelSize();//转轮尺寸
					wheelDia = hri.getReturnWheelSize();
//					contents[2][2] = "HRW-"+hri.getReturnWheelSize();//model
					if(EmptyUtil.isNotEmpty(hri.getExchangeModel())) {
						contents[2][2] = hri.getExchangeModel();
					}
					contents[15][2] = hri.getReturnTotalHeat();//全热
					contents[16][2] = hri.getReturnSensibleHeat();//显热
					String qianreS = String.valueOf(BaseDataUtil.stringConversionDouble(contents[15][2])
							- BaseDataUtil.stringConversionDouble(contents[16][2]));// 潜热
					BigDecimal qianreSBd = new BigDecimal(qianreS);//四舍五入，保留两位小数
					qianreSBd = qianreSBd.setScale(2, BigDecimal.ROUND_HALF_UP);
			    	contents[17][2] = String.valueOf(qianreSBd);

					contents[13][2] = hri.getReturnSDryBulbT();//送风干球温度
					contents[14][2] = hri.getReturnSWetBulbT()+UtilityConstant.SYS_PUNCTUATION_SLASH+hri.getReturnSRelativeT();// 送风湿球/相对湿度

					contents[26][2] = hri.getReturnEDryBulbT();//排风干球温度
					contents[27][2] = hri.getReturnEWetBulbT() + UtilityConstant.SYS_PUNCTUATION_SLASH + hri.getReturnERelativeT();//排风湿球/相对湿度
				}
				if (EmptyUtil.isNotEmpty(season) && UtilityConstant.JSON_AHU_SEASON_WINTER.equals(season)) {// 填充冬季数据
					//效率展示
			    	String[] seEff = hri.getReturnSEfficiency().split("/");//送风
			    	if (seEff.length>2) {
				    	contents[18][5] = EmptyUtil.isEmpty(seEff[1])?"":seEff[1];//temperatureEfficiency
				    	contents[19][5] = EmptyUtil.isEmpty(seEff[2])?"":seEff[2];//humidityEfficiency
					}
			    	String[] eeEff = hri.getReturnEEfficiency().split("/");//排风
			    	if (eeEff.length>2) {
				    	contents[20][5] = EmptyUtil.isEmpty(eeEff[1])?"":eeEff[1];//temperatureEfficiency
				    	contents[21][5] = EmptyUtil.isEmpty(eeEff[2])?"":eeEff[2];//humidityEfficiency
					}

					contents[6][5] = hri.getReturnWheelSize();//转轮尺寸
					wheelDia = hri.getReturnWheelSize();
//					contents[2][2] = "HRW-"+hri.getReturnWheelSize();//model
					if(EmptyUtil.isNotEmpty(hri.getExchangeModel())) {
						contents[2][2] = hri.getExchangeModel();
					}
					contents[15][5] = hri.getReturnTotalHeat();//全热
					contents[16][5] = hri.getReturnSensibleHeat();//显热
					String qianreW = String.valueOf(BaseDataUtil.stringConversionDouble(contents[15][5])
							- BaseDataUtil.stringConversionDouble(contents[16][5]));// 潜热
					BigDecimal qianreWBd = new BigDecimal(qianreW);//四舍五入，保留两位小数
					qianreWBd = qianreWBd.setScale(2, BigDecimal.ROUND_HALF_UP);
					contents[17][5] = String.valueOf(qianreWBd);

					contents[13][5] = hri.getReturnSDryBulbT();//送风干球温度
					contents[14][5] = hri.getReturnSWetBulbT()+UtilityConstant.SYS_PUNCTUATION_SLASH+hri.getReturnSRelativeT();// 送风湿球/相对湿度

					contents[26][5] = hri.getReturnEDryBulbT();//排风干球温度
					contents[27][5] = hri.getReturnEWetBulbT() + UtilityConstant.SYS_PUNCTUATION_SLASH + hri.getReturnERelativeT();//排风湿球/相对湿度
				}
				//获取 送风/排风侧阻力
				contents[5][2] = BaseDataUtil.constraintString(hri.getReturnSAirResistance());
				contents[5][5] = BaseDataUtil.constraintString(hri.getReturnEAirResistance());
			}
		}else if(EmptyUtil.isEmpty(wheelDia)) {
			contents[2][2] = UtilityConstant.SYS_BLANK;
			contents[2][5] = UtilityConstant.SYS_BLANK;
		}

		String faWB_RH = contents[12][2];
		String[] fawr = faWB_RH.split("/");
		contents[12][2] = fawr[0] + "/" + BaseDataUtil
				.constraintString(BaseDataUtil.doubleToString(BaseDataUtil.stringConversionDouble(fawr[1]), 1));

    	//最大漏风量
    	contents[28][5] = String.format("%.2f", BaseDataUtil.stringConversionDouble(String.valueOf(map.get("meta.section.wheelHeatRecycle.RAVolume")))/6000*1.49);

    	contents[24][2] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_SINDRYBULBT));
    	contents[24][5] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_WINDRYBULBT));

		contents[25][2] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_SINWETBULBT)) + UtilityConstant.SYS_PUNCTUATION_SLASH
				+ BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_SINRELATIVET));
		contents[25][5] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_WINWETBULBT)) + UtilityConstant.SYS_PUNCTUATION_SLASH
				+ BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_WINRELATIVET));



		Object brand = map.get(UtilityConstant.METASEXON_WHEELHEATRECYCLE_BRAND);
		String brandV = BaseDataUtil.constraintString(brand);
		//型式
		if ("B".equals(brandV)) {// 百瑞
			contents[2][5] = METASEXON_WHEELHEATRECYCLE_TYPE_B;
		} else if ("C".equals(brandV)) {// 毅科
			contents[2][5] = METASEXON_WHEELHEATRECYCLE_TYPE_C;
		}

		// 计算转轮电机功率
		if(EmptyUtil.isNotEmpty(wheelDia)) {//如果不安装转轮就不用计算
			List<Rotorpara> rotorparalist = AhuMetadata.findAll(Rotorpara.class);
			for (Rotorpara rp : rotorparalist) {
				if (rp.getRotorDia() == BaseDataUtil.stringConversionInteger(wheelDia)) {
					if ("B".equals(brandV)) {// 百瑞
						contents[8][2] = BaseDataUtil.doubleToString(rp.getPowerDri(), 2);
						break;
					} else if ("C".equals(brandV)) {// 毅科
						contents[8][2] = BaseDataUtil.doubleToString(rp.getPowerHeatX(), 2);
						break;
					}else {
						continue;
					}
				}
			}
		}

		ArrayList<String[]> templist = new ArrayList<String[]>(Arrays.asList(contents));
		ArrayList<String[]> templist1 = new ArrayList<>();
		for (int i = 0; i < templist.size(); i++) {
			if (UtilityConstant.SYS_ALPHABET_Z_UP.equals(brandV)) {// 品牌选项如果是“不安装转轮”，则不显示下部内容。
				if (i < 8) {
					templist1.add(templist.get(i));
				}
			} else {
//				if (i != 8) {
//					templist1.add(templist.get(i));// 删除转轮电机功率
//				}
				templist1.add(templist.get(i));// 删除转轮电机功率
			}
		}

		String[][] newContent = new String[templist1.size()][contents[0].length];
		templist1.toArray(newContent);
		contents = newContent;

    	return contents;
    }

    /**
     * 封装板式热回收特殊数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
	public static String[][] getPlateHeatRecycle(String[][] contents, Map<String, String> map, LanguageEnum language) {

		String plateHeatRecycle = BaseDataUtil
				.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_HEATRECYCLEDETAIL));
		if(EmptyUtil.isNotEmpty(plateHeatRecycle)) {
			JSONArray array = JSONArray.parseArray(plateHeatRecycle).getJSONArray(0);
			List<HeatRecycleInfo> heatRecycleInfo = JSONArray.parseArray(array.toJSONString(), HeatRecycleInfo.class);

			if (heatRecycleInfo.size() > 0) {
				for (int i = 0; i < heatRecycleInfo.size(); i++) {
					HeatRecycleInfo hri = (HeatRecycleInfo) heatRecycleInfo.get(i);
					String season = hri.getReturnSeason();
					if (EmptyUtil.isNotEmpty(season) && UtilityConstant.JSON_AHU_SEASON_SUMMER.equals(season)) {// 填充夏季数据
						contents[12][2] = hri.getReturnTotalHeat();// 全热
						contents[13][2] = hri.getReturnSensibleHeat();// 显热
						String qianreS = String.valueOf(BaseDataUtil.stringConversionDouble(contents[12][2])
								- BaseDataUtil.stringConversionDouble(contents[13][2]));// 潜热
						BigDecimal qianreSBd = new BigDecimal(qianreS);// 四舍五入，保留两位小数
						qianreSBd = qianreSBd.setScale(2, BigDecimal.ROUND_HALF_UP);
						contents[14][2] = String.valueOf(qianreSBd);

						contents[10][2] = hri.getReturnSDryBulbT();//送风干球温度
						contents[11][2] = hri.getReturnSWetBulbT()+UtilityConstant.SYS_PUNCTUATION_SLASH+hri.getReturnSRelativeT();// 送风湿球/相对湿度

						contents[19][2] = hri.getReturnEDryBulbT();// 排风干球温度
						contents[20][2] = hri.getReturnEWetBulbT() + UtilityConstant.SYS_PUNCTUATION_SLASH + hri.getReturnERelativeT();// 排风湿球/相对湿度
					}
					if (EmptyUtil.isNotEmpty(season) && UtilityConstant.JSON_AHU_SEASON_WINTER.equals(season)) {// 填充冬季数据
						contents[12][5] = hri.getReturnTotalHeat();// 全热
						contents[13][5] = hri.getReturnSensibleHeat();// 显热
						String qianreW = String.valueOf(BaseDataUtil.stringConversionDouble(contents[12][5])
								- BaseDataUtil.stringConversionDouble(contents[13][5]));// 潜热
						BigDecimal qianreWBd = new BigDecimal(qianreW);// 四舍五入，保留两位小数
						qianreWBd = qianreWBd.setScale(2, BigDecimal.ROUND_HALF_UP);
						contents[14][5] = String.valueOf(qianreWBd);

						contents[10][5] = hri.getReturnSDryBulbT();//送风干球温度
						contents[11][5] = hri.getReturnSWetBulbT()+UtilityConstant.SYS_PUNCTUATION_SLASH+hri.getReturnSRelativeT();// 送风湿球/相对湿度

						contents[19][5] = hri.getReturnEDryBulbT();// 排风干球温度
						contents[20][5] = hri.getReturnEWetBulbT() + UtilityConstant.SYS_PUNCTUATION_SLASH + hri.getReturnERelativeT();// 排风湿球/相对湿度
					}
				}
			}
		}

		contents[8][2] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_SNEWDRYBULBT));
		contents[8][5] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_WNEWDRYBULBT));

		contents[9][2] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_SNEWWETBULBT)) + UtilityConstant.SYS_PUNCTUATION_SLASH
				+ BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_SNEWRELATIVET));
		contents[9][5] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_WNEWWETBULBT)) + UtilityConstant.SYS_PUNCTUATION_SLASH
				+ BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_WNEWRELATIVET));

//		contents[17][2] = BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.SInDryBulbT"));
//		contents[17][5] = BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.WInDryBulbT"));
//
//		contents[18][2] = BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.SInWetBulbT")) + UtilityConstant.SYS_PUNCTUATION_SLASH
//				+ BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.SInRelativeT"));
//		contents[18][5] = BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.WInWetBulbT")) + UtilityConstant.SYS_PUNCTUATION_SLASH
//				+ BaseDataUtil.constraintString(map.get("meta.section.plateHeatRecycle.WInWetBulbT"));

		//品牌选项如果是“无板式交换”，则不显示下部内容。
		Object brand = map.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_BRAND);
		String brandV = BaseDataUtil.constraintString(brand);
		if (UtilityConstant.SYS_ALPHABET_Z_UP.equals(brandV)) {
			ArrayList<String[]> templist = new ArrayList<String[]>(Arrays.asList(contents));
			ArrayList<String[]> templist1 = new ArrayList<>();
			for (int i = 0; i < templist.size(); i++) {
				if (i < 7) {
					templist1.add(templist.get(i));
				}
			}

			String[][] newContent = new String[templist1.size()][contents[0].length];
			templist1.toArray(newContent);
			contents = newContent;
		}

		return contents;
	}

    /**
     * 封装湿膜加湿段特殊数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getWetFilmHumidifier(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//设置风量
//    	setAirVolume(contents, map, 1, 2);

    	contents[2][2] = BaseDataUtil.constraintString(BaseDataUtil.stringConversionInteger(BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_THICKNESS))));
    	return contents;
    }

    /**
     * 封装高压喷雾加湿段
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getSprayhumidifier(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//设置风量
//    	setAirVolume(contents, map, 1, 2);
    	return contents;
    }

    /**
     * 封装风机曲线图数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getFan4(String[][] contents, Map<String, String> map,LanguageEnum language) {


    	String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
    	String airVolumeStr = "";
		if (map.containsKey(airDirection)) {
			String value = BaseDataUtil.constraintString(map.get(airDirection));
			if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {// R--回风
				airVolumeStr = String.valueOf(map.get(UtilityConstant.METAHU_EAIRVOLUME));
			} else if (value.equals(UtilityConstant.SYS_ALPHABET_S_UP)) {// S--送风
				airVolumeStr = String.valueOf(map.get(UtilityConstant.METAHU_SAIRVOLUME));
			}
		}
		contents[21][1] = airVolumeStr;

		//计算轴功率
//    	double airVolume = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
		double airVolume = BaseDataUtil.stringConversionDouble(airVolumeStr);
    	double totalPressure = Double.parseDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
    	double efficiency = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
    	double shaftPower = (airVolume*totalPressure*100)/(3600*efficiency*1000);
    	BigDecimal shaftPowerBd = new BigDecimal(shaftPower);//四舍五入，保留两位小数
    	shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
    	contents[27][1] = String.valueOf(shaftPowerBd);

    	if("en-US".equals(language.getId())) {

    	}

    	//电机额定功率
    	double motorPower = BaseDataUtil.stringConversionDouble((String)map.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
    	contents[28][1] = String.valueOf(motorPower);

    	return contents;
    }

    /**
     * 封装冷水盘管报告特殊数据
     * @param contents
     * @param map
     * @param unitType
     * @return
     */
    public static String[][] getCoolingCoil(String[][] contents, Map<String, String> map, LanguageEnum language, UnitSystemEnum unitType) {

        /*String accessDoor = UtilityConstant.METASEXON_FAN_ACCESSDOOR;//是否开门:
        String serial = map.get(UtilityConstant.METAHU_SERIAL);
        CoilInfo sCoilInfo = PDFDataGen.getInstance().getSCoilInfo(serial, "6", "FL");
        double T_No = Double.parseDouble(sCoilInfo.getT_no());
        double Cube_len = Double.parseDouble(sCoilInfo.getT_len());
        double Cube_height = BaseDataUtil.decimalConvert(T_No*31.75,0);

		if(UnitSystemEnum.M.getId().equals(unitType)) {

		} else if (UnitSystemEnum.B.getId().equals(unitType)) {
			//英制替换值前面cell 的单位
			contents[2][1] = "inch";
		}




        if(sCoilInfo.isHasOther()){
        	for (int i = 0; i < sCoilInfo.getOthres().size(); i++) {
        		T_No = Double.parseDouble(sCoilInfo.getOthres().get(i).getT_no());
                Cube_len = Double.parseDouble(sCoilInfo.getOthres().get(i).getT_len());
                Cube_height = BaseDataUtil.decimalConvert(T_No*31.75,0);

			}
        }*/

    	String waterCollectingMaterial = map.get(UtilityConstant.METASEXON_COOLINGCOIL_WATERCOLLECTING);
    	String pipeMaterial = map.get(UtilityConstant.METASEXON_COOLINGCOIL_PIPE);
    	String waterCollectingM = UtilityConstant.SYS_BLANK;
    	String pipeM = UtilityConstant.SYS_BLANK;
    	String report = UtilityConstant.SYS_BLANK;
    	if(!StringUtils.isEmpty(waterCollectingMaterial)) {
    		if("steel".equals(waterCollectingMaterial)) {
    			if (LanguageEnum.Chinese.equals(language)) {
    				waterCollectingM = "钢";
				}else {
					waterCollectingM = "Steel";
				}
    		}else if ("copper".equals(waterCollectingMaterial)) {
				if (LanguageEnum.Chinese.equals(language)) {
					waterCollectingM = "铜";
				}else {
					waterCollectingM = "Copper";
				}
			}else if ("steelH".equals(waterCollectingMaterial)) {
				if (LanguageEnum.Chinese.equals(language)) {
					waterCollectingM = "钢(高耐久)";
				}else {
					waterCollectingM = "Steel(High Duration)";
				}
			}
    	}
    	if(!StringUtils.isEmpty(pipeMaterial)) {
    		if("steel".equals(pipeMaterial)) {
    			if (LanguageEnum.Chinese.equals(language)) {
        			pipeM = "钢";
				}else {
					pipeM = "Steel";
				}
    		}
    	}
    	report = pipeM+UtilityConstant.SYS_PUNCTUATION_SLASH+waterCollectingM;
		contents[10][6] = report;

		//夏季水流量单位m³/h转换成L/s，数值相应也要换算
//		String SreturnWaterFlow = String.valueOf(map.get(UtilityConstant.METASEXON_COOLINGCOIL_SRETURNWATERFLOW));
//    	double srwf = UnitConversionUtil.m3hTols(Double.parseDouble(SreturnWaterFlow), 2);
//		contents[21][5] = "L/s";
//		contents[21][6] = String.valueOf(srwf);

		//设置风量
//		setAirVolume(contents, map, 8, 2);

        return contents;
    }
	/**
	 * 封装冷水盘管报告特殊数据
	 * 空气阻力(干):  空气阻力(夏)(Pa) *  0.975
	 * 空气阻力(湿):  空气阻力(夏)(Pa)
	 * @param contents
	 * @param map
	 * @param unitType
	 * @return
	 */
	public static String[][] getCoolingCoilSummer(String[][] contents, Map<String, String> map, LanguageEnum language, UnitSystemEnum unitType) {

		//空气阻力(干):
		contents[6][2]=BaseDataUtil.constraintString(BaseDataUtil.doubleToString(BaseDataUtil.stringConversionDouble(contents[6][2])*0.975d, 1));

		return contents;
	}

    /**
     * 冷水盘管冬季报告封装特殊数据： 最新修改，冬季阻力干湿、去掉湿阻力，干阻力不用乘以系数。
	 * 空气阻力(干):  空气阻力(夏)(Pa) *  0.975
	 * 空气阻力(湿):  空气阻力(夏)(Pa)
     * @param contents
     * @param map
     * @param language
     * @param unitType
     * @return
     */
    public static String[][] getCoolingCoilWinner(String[][] contents, Map<String, String> map, LanguageEnum language, UnitSystemEnum unitType) {

    	/*//空气阻力(干):
		contents[6][2]=BaseDataUtil.constraintString(BaseDataUtil.doubleToString(BaseDataUtil.stringConversionDouble(contents[6][2])*0.975d, 1));

		//空气阻力(湿):
		String Wapd = String.valueOf(map.get(UtilityConstant.METASEXON_COOLINGCOIL_WAIRRESISTANCE));
		if (EmptyUtil.isNotEmpty(Wapd)) {
			double d_wapd = BaseDataUtil.stringConversionDouble(Wapd);
			contents[7][2] = AhuSectionMetas.getInstance().getStringValueByUnit(String.valueOf(Math.round(d_wapd)),UtilityConstant.METASEXON_COOLINGCOIL_WAIRRESISTANCE);
		}*/

		//冬季水流量单位m³/h转换成L/s，数值相应也要换算
//    	String WreturnWaterFlow = map.get(UtilityConstant.METASEXON_COOLINGCOIL_WRETURNWATERFLOW);
//    	double wrwf = UnitConversionUtil.m3hTols(Double.parseDouble(WreturnWaterFlow), 2);
//
//    	contents[5][5] = "L/s";
//    	contents[5][6] = String.valueOf(wrwf);

        return contents;
    }

    /**
     * 封装直接蒸发式盘管
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getDirectexpensioncoil(String[][] contents, Map<String, String> map,LanguageEnum language){
    	//设置风量
//    	setAirVolume(contents, map, 8, 2);

    	return contents;
    }


    /**
     * 封装热水盘管报告特殊数据
     * @param contents
     * @param map
     * @param unitType
     * @return
     */
    public static String[][] getHeatingCoil(String[][] contents, Map<String, String> map, LanguageEnum language, UnitSystemEnum unitType) {

        String pipe = UtilityConstant.METASEXON_HEATINGCOIL_PIPE;// 进水管
        String waterCollection = UtilityConstant.METASEXON_HEATINGCOIL_WATERCOLLECTION;// 集水管材质
        StringBuffer pipeWaterCollectionSBF = new StringBuffer();
        if (map.containsKey(pipe)) {
            String value = map.get(pipe).toString();
            pipeWaterCollectionSBF.append(AhuSectionMetas.getInstance().getValueByUnit(value, pipe,
                    SectionTypeEnum.TYPE_HEATINGCOIL.getId(), language));
        }
        pipeWaterCollectionSBF.append(UtilityConstant.SYS_PUNCTUATION_SLASH);
        if (map.containsKey(waterCollection)) {
            String value = map.get(waterCollection).toString();
            pipeWaterCollectionSBF.append(AhuSectionMetas.getInstance().getValueByUnit(value, waterCollection,
                    SectionTypeEnum.TYPE_HEATINGCOIL.getId(), language));
        }
        contents[11][5] = pipeWaterCollectionSBF.toString();

        //翅片类型
        String finType = UtilityConstant.METASEXON_HEATINGCOIL_FINTYPE;
//        String ft = map.get(finType);
//
//        if(!EmptyUtil.isEmpty(ft)) {
//        	if ("al".equals(ft)) {
//				ft = "普通铝翅片";
//			}else if("procoatedAl".equals(ft)) {
//				ft = "亲水铝翅片";
//			}else if("copper".equals(ft)) {
//				ft = "铜翅片";
//			}
//        }
//        contents[10][5] = ft;

        //防冻开关
        String frostprotectionoption = UtilityConstant.METASEXON_HEATINGCOIL_FROSTPROTECTIONOPTION;//防冻开关
        String content = "";
        if(map.containsKey(frostprotectionoption)) {
        	String is = BaseDataUtil.constraintString(map.get(frostprotectionoption));
        	if(is!=null && UtilityConstant.SYS_ASSERT_TRUE.equals(is)) {
				content = I18NBundle.getString("switch_capacity_colon", language)
						+ I18NBundle.getString("8A,24~250VAC", language)
						+ I18NBundle.getString("work_environment_colon", language)
						+ I18NBundle.getString("factory_only_components_no_control", language);
	        	contents[15][1]= content;
        	}else {
            	contents[15][0]= UtilityConstant.SYS_BLANK;
            	contents[15][1]= UtilityConstant.SYS_BLANK;
            }
        }

//		// 冬季空气阻力
//		String Wapd = String.valueOf(map.get(UtilityConstant.METASEXON_HEATINGCOIL_WAIRRESISTANCE));
//		String eliminatorR = String.valueOf(map.get(UtilityConstant.METASEXON_HEATINGCOIL_DRIFTELIMINATORRESISTANCE));
//		if (EmptyUtil.isNotEmpty(Wapd) && EmptyUtil.isNotEmpty(eliminatorR)) {
//			double d_wapd = BaseDataUtil.stringConversionDouble(Wapd);
//			double d_eliminatorR = BaseDataUtil.stringConversionDouble(eliminatorR);
//			contents[21][2] = String.valueOf(Math.round(d_wapd + d_eliminatorR));
//		}

        //设置风量
//        setAirVolume(contents, map, 9, 2);
        return contents;
    }

    /**
     * 组装干蒸加湿段特殊数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getSteamHumidifier(String[][] contents, Map<String, String> map,LanguageEnum language) {

    	String ControlM = map.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_CONTROLM);
    	if (EmptyUtil.isNotEmpty(ControlM)) {
			if ("EC".equals(ControlM)) {
				contents[1][5] = "电动（电磁阀）";
			}
		}

    	return contents;
    }

    /**
     * 封装柏诚报告 (filter 表格)
     * @param contents
     * @param allMap
     * @param part
     * @return
     */
    public static String[][] getBCFilter(String[][] contents, Map<String,String> allMap, SectionTypeEnum part) {
        /*[
              [ "种类", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK ],
              [ "面积（㎡）", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK ]
        ]*/

        StringBuffer partName = new StringBuffer(getIntlString(part.getCnName()));
        switch (part) {
            case TYPE_SINGLE: {
                String filterF = MetaSelectUtil.getTextForSelect(UtilityConstant.METASEXON_FILTER_FITETF,
                        allMap.get(UtilityConstant.METASEXON_FILTER_FITETF),
                        getLanguage());
                String filterEfficiency = allMap.get(UtilityConstant.METASEXON_FILTER_FILTEREFFICIENCY);
                partName.append(UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+filterF+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
                partName.append(filterEfficiency);
                break;
            }
            case TYPE_COMPOSITE: {

                String LMaterial = MetaSelectUtil.getTextForSelect(UtilityConstant.METASEXON_COMBINEDFILTER_LMATERIAL,
                        allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_LMATERIAL),
                        getLanguage());
                String LMaterialE = allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_LMATERIALE);
                partName.append(UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+LMaterial+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
                partName.append(LMaterialE);


                String RMaterial = MetaSelectUtil.getTextForSelect(UtilityConstant.METASEXON_COMBINEDFILTER_RMATERIAL,
                        allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_RMATERIAL),
                        getLanguage());
                String RMaterialE = allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_RMATERIALE);
                partName.append(";("+RMaterial+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
                partName.append(RMaterialE);

                break;
            }
            case TYPE_HEPAFILTER: {
                String filterEfficiency = allMap.get(UtilityConstant.METASEXON_HEPAFILTER_FILTEREFFICENCY);
                partName.append(filterEfficiency);
                break;
            }
            case TYPE_ELECTROSTATICFILTER: {

                String filterType = MetaSelectUtil.getTextForSelect(UtilityConstant.METASEXON_ELECTROSTATICFILTER_FILTERTYPE,
                        allMap.get(UtilityConstant.METASEXON_ELECTROSTATICFILTER_FILTERTYPE),
                        getLanguage());
                String filterEfficiency = allMap.get(UtilityConstant.METASEXON_ELECTROSTATICFILTER_FILTEREFFICIENCY);
                partName.append(UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+filterType+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
                partName.append(filterEfficiency);
                break;
            }
        }
        contents[0][3] = partName.toString();

        String filterArrange = allMap.get(UtilityConstant.METASEXON_PREFIX+(part.getId().split("\\.")[1])+".FilterArrange");
        JSONArray faArray = JSONArray.parseArray(filterArrange);
        StringBuffer filterStrs = new StringBuffer();
        for (int i = 0; i < faArray.size(); i++) {
            JSONObject faObj = faArray.getJSONObject(i);
            if(SectionTypeEnum.TYPE_COMPOSITE == part){
                filterStrs.append(getIntlString(BAG_TYPE)+":"+faObj.getString("lOption") + "," + faObj.getString("lCount") + ";");
                filterStrs.append(getIntlString(PANEL_TYPE)+":"+faObj.getString("pOption") + "," + faObj.getString("pCount") + ";");
            }else {
                filterStrs.append(faObj.getString("option") + "," + faObj.getString("count") + ";");
            }
        }
        contents[1][3] = filterStrs.toString();
        return contents;
    }
    /**
     * 封装柏诚报告 (加湿段 表格)
     * @param contents
     * @param allMap
     * @param part
     * @return
     */
    public static String[][] getBCWet(String[][] contents, Map<String,String> allMap, SectionTypeEnum part) {
        /*[
          [ "制造商/原产国家", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, "须填报" ],
          [ "型号", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, "须填报" ],
          [ "类型", UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK, "须填报" ],
          [ "进水温度", UtilityConstant.SYS_BLANK,"℃", "须填报" ],
          [ "加湿量", UtilityConstant.SYS_BLANK,"kg/hr", "须填报" ]
        ]*/

        StringBuffer partName = new StringBuffer(getIntlString("湿膜加湿"));
        contents[2][3] = partName.toString();

        String partKey = part.getId().split("\\.")[1];
//        String InDryBulbT = String.valueOf(allMap.get(UtilityConstant.METASEXON_PREFIX+partKey+".WInDryBulbT"));
//        contents[3][3] = InDryBulbT;
        String WHumidificationQ =  String.valueOf(allMap.get(UtilityConstant.METASEXON_PREFIX+partKey+".WHumidificationQ"));
        contents[4][3] = WHumidificationQ;
        return contents;
    }

    /**
     * 封装柏诚热水盘管
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getBCheatingcoil(String[][] contents, Map<String, String> map,LanguageEnum language) {

    	double WenteringFluidTemperature = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_HEATINGCOIL_WENTERINGFLUIDTEMPERATURE)));
    	double WWTAScend = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_HEATINGCOIL_WWTASCEND)));
    	String outWaterTemp = String.valueOf(WenteringFluidTemperature+WWTAScend);

    	contents[5][3] = outWaterTemp + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(WenteringFluidTemperature);
    	return contents;
    }

    /**
     * 封装柏诚风机
     * @param contents
     * @param map
     * @param language
     * @return
     */
    public static String[][] getBCfan(String[][] contents, Map<String, String> map,LanguageEnum language) {
    	//风机种类
    	String outlet = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_OUTLET));
    	if(EmptyUtil.isNotEmpty(outlet)) {
    		if (UtilityConstant.JSON_FAN_OUTLET_WWK.equals(outlet)) {
    			contents[0][3] = "离心风机";
			}else{
				contents[0][3] = "离心风机";
			}

    	}

    	//出口风速
    	contents[3][3] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_OUTLETVELOCITY));

    	//风机转速
    	contents[8][3] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_RPM));
    	contents[10][3] = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_RPM));

    	//输入功率
    	double motorPower = BaseDataUtil.stringConversionDouble((String)map.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
    	String motorEfficiencyStr = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_FAN_MOTOREFF));
    	NumberFormat nf=NumberFormat.getPercentInstance();
    	Number motorEfficiencyNum;
		try {
			if(EmptyUtil.isNotEmpty(motorEfficiencyStr)) {
				motorEfficiencyNum = nf.parse(motorEfficiencyStr);
			}else {
				motorEfficiencyNum = 0;
			}

			double motorEfficiency = motorEfficiencyNum.doubleValue();
			if(motorEfficiency==0) {
	    		motorEfficiency = BaseDataUtil.stringConversionDouble(String.valueOf(map.get("meta.section.fan.absorbedPower")));;
	    		contents[11][3] = String.valueOf(" ");
	    	}else {
				String inputPowerBdStr = "";
				//计算轴功率
				double airVolume = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
				double totalPressure = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
				double efficiency = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
				double shaftPower = (airVolume*totalPressure*100)/(3600*efficiency*1000);
				if(Double.isNaN(shaftPower)){
					BigDecimal inputPowerBd = new BigDecimal(shaftPower*1.35);// 四舍五入，保留两位小数
					inputPowerBd = inputPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
					inputPowerBdStr = String.valueOf(inputPowerBd);
				}else{
					double inputPower = (motorPower)/(motorEfficiency);
					BigDecimal inputPowerBd = new BigDecimal(inputPower);//四舍五入，保留两位小数
					inputPowerBd = inputPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
					inputPowerBdStr = String.valueOf(inputPowerBd);
				}
//		    	contents[11][3] = inputPowerBdStr;
	    	}
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return contents;
    }

    /**
     * 封装柏诚报告（ahu）
     * @param contents
     * @param ahuMap
     * @param ahuPartitionList
     * @return
     */
    public static String[][] getBCAhu(String[][] contents, Map<String,String> ahuMap, Map<String, String> mixMap,List<AhuPartition> ahuPartitionList) {

    	contents[6][3] = "开利空调冷冻系统（上海）有限公司/"+ getIntlString(PRODUCER_COUNTRY_FIELD);

    	double sairvolume = BaseDataUtil.stringConversionDouble(String.valueOf(ahuMap.get(UtilityConstant.METAHU_SAIRVOLUME)));
    	String naratioStr = String.valueOf(ahuMap.get(UtilityConstant.METASEXON_MIX_NARATIO));
    	double NARatio = BaseDataUtil.stringConversionDouble(String.valueOf(mixMap.get(UtilityConstant.METASEXON_MIX_NARATIO)));
		contents[7][3] = "     " + UtilityConstant.SYS_PUNCTUATION_SLASH
				+ BaseDataUtil.constraintString(ahuMap.get(UtilityConstant.METAHU_SAIRVOLUME));
    	return contents;
    }

    /**
     * 封装柏诚报告（ahu2）
     * @param contents
     * @param allMap
     * @return
     */
    public static String[][] getBCAhu2(String[][] contents, Map<String,String> allMap, List<AhuPartition> ahuPartitionList) {
    	/*[
    	  [ "机组总尺寸",UtilityConstant.SYS_BLANK,	"须填报", "AHU.sectionL*AHU.Height*AHU.Width"],
    	  [ "运行重量（kg)",UtilityConstant.SYS_BLANK,		"须填报","AHU.Weight"],
    	  [ "运行噪声（dB（A)）", UtilityConstant.SYS_BLANK,		"meta.section.fan.noise","meta.section.fan.noise"],
    	  ["声功率级",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK],
    	  ["八倍频程（Hz)",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,"               63     125     250     500     1k     2k     4k     8k"],
    	  ["dB",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,"               68     71     77     82     79     76     71     62"],
    	  ["备注：",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK],
    	  ["1.机组采用橡胶减震，空调箱内风机须设置弹簧减震台座；",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK],
    	  ["2.此空调箱为组合式空调箱；",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK],
    	  ["3.此空调箱变频，须自带变频控制箱。",UtilityConstant.SYS_BLANK,		UtilityConstant.SYS_BLANK,UtilityConstant.SYS_BLANK]
    	]*/
    	//计算机组总尺寸
		if(ahuPartitionList!=null && ahuPartitionList.size()>0) {
			int length = 0;
	    	int width = 0;
	    	int height = 0;
	    	//double weight = 0.0d;
	    	for(AhuPartition ap: ahuPartitionList) {
	    		length += ap.getLength()+ap.getCasingWidth();
	    		width = ap.getWidth()*100 + ap.getCasingWidth();
	    		height = ap.getHeight()*100 + ap.getCasingWidth();
	    		//weight += ap.getWeight();
	    	}

			String unitSize = String.valueOf(length + UtilityConstant.SYS_PUNCTUATION_STAR + width
					+ UtilityConstant.SYS_PUNCTUATION_STAR + height);
	        contents[0][3] = unitSize;
		}

        //计算A声功率级（A声声压级）
//		if(allMap.get(UtilityConstant.METASEXON_FAN_LW1)!=null &&
//				!UtilityConstant.SYS_BLANK.equals(allMap.get(UtilityConstant.METASEXON_FAN_LW1))) {
//			String dB = "               ";
//			for (int i = 1; i < 9; i++) {
//				dB += (String) allMap.get(UtilityConstant.METASEXON_FAN_LW + i) + "     ";
//			}
//			contents[5][3] = String.valueOf(dB);
//		}


//        String totalWeight = String.valueOf(weight);
//        contents[1][3] = totalWeight;

//        String noise = String.valueOf(allMap.get("meta.section.fan.noise"));
//        contents[2][3] = noise;
        return contents;
    }

    /**
     * 封装出风段报告特殊数据
     * @param contents
     * @param map
     * @return
     */
    public static String[][] getDischarge(String[][] contents, Map<String, String> map,LanguageEnum language) {

        String outletDirection = UtilityConstant.METASEXON_DISCHARGE_OUTLETDIRECTION;

        int returnWF = 1;
        /*
        T顶部出风
        A后出风
        L左侧出风
        R右侧出风
        F底部出风

		1上出风
		2前出风
		3左出风
		4右出风
		5底出风
        */
        switch (map.get(outletDirection).toString()) {
		case "A":
			returnWF = 2;
			break;
		case "L":
			returnWF = 3;
			break;
		case UtilityConstant.SYS_ALPHABET_R_UP:
			returnWF = 4;
			break;
		case UtilityConstant.SYS_ALPHABET_F_UP:
			returnWF = 5;
			break;
		default:
			returnWF = 1;
			break;
		}



        //封装出风段参数形式
		String doorDirection = map.get(UtilityConstant.METASEXON_DISCHARGE_DOORDIRECTION);//开门方向
		String dischargeSectionl = String.valueOf(map.get(UtilityConstant.METASEXON_DISCHARGE_SECTIONL));//段长
        String damperExecutorKey = UtilityConstant.METASEXON_DISCHARGE_DAMPEREXECUTOR;//执行器
        String dampertype = map.get(UtilityConstant.METASEXON_DISCHARGE_AINTERFACE);//风口接件
        String serial = map.get(UtilityConstant.METAHU_SERIAL);
        String damperMeterialKey = UtilityConstant.METASEXON_DISCHARGE_DAMPERMETERIAL;//风阀材料
        String product = map.get(UtilityConstant.METAHU_PRODUCT);

		String damperMeterial = map.get(damperMeterialKey);
		String damperExecutor = map.get(damperExecutorKey);
		damperExecutor = MetaSelectUtil.getTextForSelect(damperExecutorKey, damperExecutor, language);


        int valvem = 1;//镀锌板
		if (SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_ALUMINUM.equals(damperMeterial)) {
			valvem = 2;//铝合金
		}


        boolean boolFL =false;
        Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));

		if (SystemCalculateConstants.MIX_DAMPEROUTLET_FD.equals(dampertype)) {
			boolFL = true;
		}

		//风阀电动+(执行器)情况
        if (map.containsKey(UtilityConstant.METASEXON_DISCHARGE_AINTERFACE)) {
            String value = contents[1][5];
            if (SystemCalculateConstants.DISCHARGE_AINTERFACE_ELECTRICDAMPER.equals(dampertype)) {
            	contents[1][5] = value + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+damperExecutor+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
            }
        }
		String accessDoor = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_DISCHARGE_DISCHARGEODOOR));//是否开门:
		if (!SystemCalculateConstants.DISCHARGE_ODOOR_ND.equals(accessDoor)) {//开门
			//添加：
//        	String doorOnBothSide = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_MIX_DOORDIRECTION));//两侧开门
			String xtDoorDirection = "";
			if (doorDirection.equals(DISCHARGE_DOORDIRECTION_RIGHT)) {
				xtDoorDirection = getIntlString(TO_LEFT);
			} else if (doorDirection.equals(DISCHARGE_DOORDIRECTION_LEFT)) {
				xtDoorDirection = getIntlString(TO_RIGHT);
			} else if (doorDirection.equals(DISCHARGE_DOORDIRECTION_BOTH)) {
				xtDoorDirection = getIntlString(BOTH_SIDES);
			}

			String PositivePressureDoor = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_DISCHARGE_POSITIVEPRESSUREDOOR));//正压门
			if (Boolean.parseBoolean(PositivePressureDoor)) {
				PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR) + SYS_PUNCTUATION_SLIGHT_PAUSE;
            /*请在选型报告中增加门尺寸及方向，其中尺寸数据从表格中读取
			段长等于5M时，读取SECTIONL=5
			段长大于5M时,读取SECTIONL=6*/
				String serials = map.get(UtilityConstant.METAHU_SERIAL);
				XtDoor xtDoor = AhuMetadata.findOne(XtDoor.class, serials);
				String xtDoorSize = "";
				if (EmptyUtil.isNotEmpty(xtDoor)) {
					xtDoorSize = xtDoor.getXtDoorw() + "*" + xtDoor.getXtDoorh();
				}
//                String xtDoorDirection = "";
//                if (doorDirection.equals(AHU_DOORORIENTATION_LEFT)) {
//                    xtDoorDirection = getIntlString(TO_RIGHT);
//                } else if (doorDirection.equals(AHU_DOORORIENTATION_RIGHT)) {
//                    xtDoorDirection = getIntlString(TO_LEFT);
//                }
				PositivePressureDoor += xtDoorDirection + xtDoorSize + UtilityConstant.SYS_PUNCTUATION_SLIGHT_PAUSE;
				//正压门时，尺寸读取
				String doorSize = xtDoor.getXtDoorwSize() +"*"+ xtDoor.getXtDoorhSize();
				contents[4][5] = doorSize;
			} else {
				PositivePressureDoor = UtilityConstant.SYS_BLANK;
			}
			//安装检修灯
			String value25 = contents[2][5];
			String fixRepairLamp = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_DISCHARGE_FIXREPAIRLAMP));
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)) {
				value25 = value25 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP) + UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
			} else {
				value25 = value25 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP) + UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
			}
			contents[2][5] = value25;
		}else{
			contents[4][5] = "--";
		}


		if (UtilityConstant.SYS_STRING_NUMBER_2.equals(String.valueOf(returnWF))) {
			dischargeSectionl = damper.getSectionL();
		}
        DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class, serial.substring(0, serial.length() - 4),
                returnWF + UtilityConstant.SYS_BLANK, dischargeSectionl);
        String A = UtilityConstant.SYS_BLANK;
        String C = UtilityConstant.SYS_BLANK;

        int lenth = serial.length();
		String heightStr = serial.substring(lenth - 4, lenth - 2);
		String weightStr = serial.substring(lenth - 2);
		int height = Integer.parseInt(heightStr);
		int width = Integer.parseInt(weightStr);

		if (EmptyUtil.isEmpty(nsion)) {
			logger.error("查表未获取到DamperDimension @封装综合过滤段报告数据 报告使用");
		} else {
			if (SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_ALUMINUM.equals(damperMeterial)) {
                double c1 = Double.parseDouble(nsion.getC1());
                A = UtilityConstant.SYS_BLANK+(c1 - 2 * 40);
            } else {
                A = nsion.getA();
            }
			int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);

			if (returnWF == 1 || returnWF == 2) {
				C = UtilityConstant.SYS_BLANK+((width - 1) * 100 + DIMENSION_C_calPara - 72);
			}
			if (returnWF == 3 || returnWF == 4) {
				C = UtilityConstant.SYS_BLANK + ((height - 1) * 100 + DIMENSION_C_calPara - 72);
			}
			if (returnWF == 5) {
				C = UtilityConstant.SYS_BLANK+((width - 5) * 100 + DIMENSION_C_calPara - 72);
			}
		}
		if (!UtilityConstant.SYS_BLANK.equals(A) && !UtilityConstant.SYS_BLANK.equals(C)) {
			if(A.contains(UtilityConstant.SYS_PUNCTUATION_DOT)) {
				A=A.substring(0,A.indexOf(UtilityConstant.SYS_PUNCTUATION_DOT));
			}
			contents[6][2] = C + UtilityConstant.SYS_ALPHABET_X_UP + A;// 回风阀门尺寸
		}
        AdjustAirDoor ad = AhuMetadata.findOne(AdjustAirDoor.class, map.get(UtilityConstant.METAHU_SERIAL), UtilityConstant.SYS_ALPHABET_N_UP, UtilityConstant.SYS_BLANK + returnWF);
        if (EmptyUtil.isEmpty(ad)) {
			logger.error("查表未获取到AdjustairDoor @封装综合过滤段报告数据 报告使用"+returnWF);
		} else {
			if(!boolFL){
				if(2 == valvem){
					contents[6][5] = ad.getWringqAl();//回风阀门扭矩
				}else{
					contents[6][5] = ad.getWringq();//回风阀门扭矩
				}
			}
		}


        //设置风量
//        setAirVolume(contents, map, 1, 2);
        return contents;
    }

    /**
     * 封装空段报告特殊数据
     * @param contents
     * @param map
     * @param language
     * @return
     */
	public static String[][] getAccess(String[][] contents, Map<String, String> map, LanguageEnum language) {
		String accessDoor = UtilityConstant.METASEXON_ACCESS_ODOOR;//是否开门:
        //是否开门需要添加：
        String doorOnBothSide = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_ACCESS_DOORONBOTHSIDE));//两侧开门
        String fixRepairLamp = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_ACCESS_FIXREPAIRLAMP));//安装检修灯
		String ahuDoorDirection = BaseDataUtil.constraintString(map.get(UtilityConstant.METAHU_DOORORIENTATION));//开门方向
		String dischargeSectionl = String.valueOf(map.get(UtilityConstant.METASEXON_ACCESS_SECTIONL));//段长
		String uvLamp = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_ACCESS_UVLAMP));//杀菌灯
		String uvLampModel = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_ACCESS_UVMODEL));//杀菌灯型号


		if (map.containsKey(accessDoor)) {
            String value = contents[1][5];
            if (!value.equals(UtilityConstant.SYS_BLANK)) {
            	if(UtilityConstant.SYS_ASSERT_TRUE.equals(doorOnBothSide)){
            		value = getIntlString(BOTH_SIDES)+value;
            	}
            	contents[1][5] = value;
            }
        }
        String PositivePressureDoor = BaseDataUtil.constraintString(map.get(UtilityConstant.METASEXON_ACCESS_POSITIVEPRESSUREDOOR));//正压门
        if(Boolean.parseBoolean(PositivePressureDoor)){
            PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;
            /*请在选型报告中增加门尺寸及方向，其中尺寸数据从表格中读取
			段长等于5M时，读取SECTIONL=5
			段长大于5M时,读取SECTIONL=6*/

			XtDoor xtDoor = AhuMetadata.findOne(XtDoor.class, map.get(UtilityConstant.METAHU_SERIAL), String.valueOf(NumberUtil.convertStringToDoubleInt(dischargeSectionl)>5?6:5));
			String xtDoorSize = "";
			if(EmptyUtil.isNotEmpty(xtDoor)){
				xtDoorSize = xtDoor.getXtDoorw()+"*"+xtDoor.getXtDoorh();
			}
			String xtDoorDirection = "";
			if(ahuDoorDirection.equals(AHU_DOORORIENTATION_LEFT)){
				xtDoorDirection = getIntlString(TO_RIGHT);
			}else if(ahuDoorDirection.equals(AHU_DOORORIENTATION_RIGHT)){
				xtDoorDirection = getIntlString(TO_LEFT);
			}
			PositivePressureDoor += xtDoorDirection +xtDoorSize + UtilityConstant.SYS_PUNCTUATION_SLIGHT_PAUSE;
			//正压门时，尺寸读取
			String doorSize = xtDoor.getXtDoorwSize() +"*"+ xtDoor.getXtDoorhSize();
			contents[2][5] = doorSize;
        }else{
            PositivePressureDoor = UtilityConstant.SYS_BLANK;
        }

        String value15 = contents[1][5];
		if (UtilityConstant.SYS_ASSERT_TRUE.equals(fixRepairLamp)) {
			value15 = value15 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + PositivePressureDoor + getIntlString(INSTALL_FIX_REPAIR_LAMP)
					+ UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
		} else {
			value15 = value15 + UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + PositivePressureDoor + getIntlString(NO_FIX_REPAIR_LAMP)
					+ UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE;
		}
    	contents[1][5] = value15;


		//附件:无杀菌灯，屏蔽附件
		if (UtilityConstant.SYS_ASSERT_TRUE.equals(uvLamp)) {
			contents[4][2] = getIntlString(UV_LIGHT_CUVCA).concat(UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN).concat(uvLampModel.concat(UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE));

		}else{
			contents[4][0] = "";
			contents[4][2] = "";
		}

    	//设置风量
//    	setAirVolume(contents, map, 1, 2);
		return contents;
	}

	/**
	 * 封装高效过滤段
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getHepafilter(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//设置风量
//		setAirVolume(contents, map, 1, 2);
        if(JSON_HEPAFILTER_SUPPLIER_NONE.equals(map.get(METASEXON_HEPAFILTER_SUPPLIER))){
            contents[2][2] = "";
        }
		return contents;
	}

    /**
     * 封装pdf报告特殊数据
     * @param contents
     * @param allMap
     * @param type
     * @return
     */
	public static String[][] getCommonF(String[][] contents, Map<String, String> allMap ,SectionTypeEnum type) {
		String id = type.getId().replace(UtilityConstant.METAHU_AHU_NAME, UtilityConstant.SYS_BLANK);
		contents[2][1] = UtilityConstant.METANS_ID_MEMO.replace(UtilityConstant.METACOMMON_ID, id);
//		contents[2][4] = UtilityConstant.METANS_ID_PRICE.replace(UtilityConstant.METACOMMON_ID, id);
		contents[2][3] = UtilityConstant.SYS_BLANK;
		contents[2][4] = UtilityConstant.SYS_BLANK;
		String enableKey = UtilityConstant.METANS_ID_ENABLE.replace(UtilityConstant.METACOMMON_ID, id);
		if (type == SectionTypeEnum.TYPE_AHU) {
			enableKey = UtilityConstant.METANS_NS_AHU_ENABLE;
			contents[2][1] = UtilityConstant.METANS_AHU_ID_MEMO;
//			contents[2][4] = UtilityConstant.METANS_AHU_ID_PRICE;
			contents[2][3] = UtilityConstant.SYS_BLANK;
			contents[2][4] = UtilityConstant.SYS_BLANK;
		}
		String enable = String.valueOf(allMap.get(enableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			return contents;
		}else{
			return null;
		}
	}

    /**
     * 封装非标word报告特殊数据
     * @param contents
     * @param allMap
     * @param type
     * @return
     */
    public static String[][] getNsCommonF(String[][] contents, Map<String, String> allMap, SectionTypeEnum type) {
        String id = type.getId().replace(UtilityConstant.METAHU_AHU_NAME, UtilityConstant.SYS_BLANK);
        contents[0][1] = UtilityConstant.METANS_ID_MEMO.replace(UtilityConstant.METACOMMON_ID, id);
        contents[0][3] = UtilityConstant.METANS_ID_PRICE.replace(UtilityConstant.METACOMMON_ID, id);
        String enableKey = UtilityConstant.METANS_ID_ENABLE.replace(UtilityConstant.METACOMMON_ID, id);
        String enable = String.valueOf(allMap.get(enableKey));
        if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
            return contents;
        }else{
            return null;
        }
    }

	/**
	 * 封装电极加湿段特殊数据
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getTechElec1(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//计算额定功率/加湿器类型
		double SHumidificationQ = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_SHUMIDIFICATIONQ)));//夏季加湿量
		double WHumidificationQ = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WHUMIDIFICATIONQ)));//冬季加湿量
		int humidificationQ = 0;
		//加湿量优先取冬季的
		if (WHumidificationQ == 0.0) {
			humidificationQ = Integer.parseInt(new DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(SHumidificationQ));
		}else {
			humidificationQ = Integer.parseInt(new DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(WHumidificationQ));
		}

		//根据加湿量查询额定功率/加湿器类型
		S4xhumid s4xhumid = getS4xhumid(humidificationQ);
		if(EmptyUtil.isNotEmpty(s4xhumid)){
			double rpower = s4xhumid.getRpower();
			contents[5][2] = String.valueOf(rpower);
			contents[6][5] = String.valueOf(s4xhumid.getTypes());
		}

		//设置风量
//		setAirVolume(contents, map, 1, 2);

		return contents;
	}

	/**
	 * 封装电极加湿冬夏季特殊数据
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getTechElecSummerOrWinner(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//计算最大加湿量
		double SHumidificationQ = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_SHUMIDIFICATIONQ)));//夏季加湿量
		double WHumidificationQ = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WHUMIDIFICATIONQ)));//冬季加湿量
		int humidificationQ = 0;
		//加湿量优先取冬季的
		if (WHumidificationQ == 0.0) {
			humidificationQ = Integer.parseInt(new DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(SHumidificationQ));
		}else {
			humidificationQ = Integer.parseInt(new DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(WHumidificationQ));
		}

		//根据加湿量查询最大加湿量
		S4xhumid s4xhumid = getS4xhumid(humidificationQ);
		if(EmptyUtil.isNotEmpty(s4xhumid)){
			int hq = s4xhumid.getHumidq();
			contents[1][5] = String.valueOf(hq);
		}

		return contents;
	}
	//根据加湿量获取最大加湿量
	private static S4xhumid getS4xhumid(int humidificationQ) {
		S4xhumid s4xhumid = null;
		List<S4xhumid> s4xhumidList = AhuMetadata.findAll(S4xhumid.class);
		if(s4xhumidList!=null) {
			ListUtils.sort(s4xhumidList, true,"humidq");
			for (int i=0;i<s4xhumidList.size();i++) {
				s4xhumid=s4xhumidList.get(i);
				if(humidificationQ<=s4xhumid.getHumidq()) {
					break;
				}
			}
		}
		return s4xhumid;
	}

	public static String[][] getTechElec14Sale(String[][] contents, Map<String, String> map, LanguageEnum language) {
        //计算额定功率
        double SHumidificationQ = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_SHUMIDIFICATIONQ)));//夏季加湿量
        double WHumidificationQ = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WHUMIDIFICATIONQ)));//冬季加湿量
        int humidificationQ = 0;
        //加湿量优先取冬季的
        if (WHumidificationQ == 0.0) {
            humidificationQ = Integer.parseInt(new DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(SHumidificationQ));
        }else {
            humidificationQ = Integer.parseInt(new DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(WHumidificationQ));
        }

        //根据加湿量查询额定功率

        List<S4xhumid> s4xhumidList = AhuMetadata.findAll(S4xhumid.class);
		if(s4xhumidList!=null) {
			ListUtils.sort(s4xhumidList, true,"humidq");
			S4xhumid s4xhumid=null;
			for (int i=0;i<s4xhumidList.size();i++) {
				s4xhumid=s4xhumidList.get(i);
				if(humidificationQ<=s4xhumid.getHumidq()) {
					break;
				}
			}

			double rpower = s4xhumid.getRpower();
			contents[3][2] = String.valueOf(rpower);
		}
        return contents;
    }

	/**
	 * 蒸汽盘管段封装特殊数据
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getTechSte(String[][] contents, Map<String, String> map, LanguageEnum language) {
		// 计算管接头
		String fantype = (String) map.get(UtilityConstant.METAHU_SERIAL);// 机组型号
		String flange = (String) map.get(UtilityConstant.METASEXON_STEAMCOIL_CONNECTIONTYPE);// 连接形式
		if (EmptyUtil.isNotEmpty(flange)) {
			flange = "TD".equals(flange) ? UtilityConstant.SYS_STRING_NUMBER_0 : UtilityConstant.SYS_STRING_NUMBER_1;
		}

		// 根据加湿量查询额定功率
		String finalvalue = UtilityConstant.SYS_BLANK;
		STakeoversize sTakeoversize = AhuMetadata.findOne(STakeoversize.class, fantype, UtilityConstant.SYS_ALPHABET_F_UP, flange);
		if (sTakeoversize != null) {
			finalvalue = UtilityConstant.SYS_STRING_NUMBER_1.equals(flange) ? "法兰" : "螺纹" + sTakeoversize.getSizevalue();
		}

		contents[3][5] = String.valueOf(finalvalue);

		//设置风量
//		setAirVolume(contents, map, 1, 2);
		return contents;
	}

	/**
	 * 设置电子加热盘管段
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getElectricheatingcoil(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//设置风量
//		setAirVolume(contents, map, 1, 2);
		return contents;
	}

	/**
	 * 封装干蒸加湿段
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getSteamhumidifier(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//设置风量
//		setAirVolume(contents, map, 1, 2);
		return contents;
	}

	/**
	 * 封装静电过滤段
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getElectrostaticfilter(String[][] contents, Map<String, String> map, LanguageEnum language) {
		//设置风量
//		setAirVolume(contents, map, 1, 2);
		return contents;
	}

	/**
	 * 封装机组铭牌
	 * @param contents
	 * @param allMap
	 * @param language
	 * @return
	 */
	public static String[][] getUnitNamePlateData(String[][] contents, Map<String, Map<String, String>> allMap,
			Map<String, String> noChangeMap, LanguageEnum language) {

		boolean hasFan = hasSectionType(allMap,SectionTypeEnum.TYPE_FAN);
		//获取送/回风机的Map
		Map<String, String> sMap = new HashMap<String, String>();
		Map<String, String> rMap = new HashMap<String, String>();
		for (Map.Entry<String, Map<String, String>> entry : allMap.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
			if (entry.getKey().endsWith(UtilityConstant.METASEXON_FAN)) {
				if ("S".equals(entry.getValue().get(UtilityConstant.METASEXON_AIRDIRECTION))) {
					sMap = entry.getValue();
				}
				if ("R".equals(entry.getValue().get(UtilityConstant.METASEXON_AIRDIRECTION))) {
					rMap = entry.getValue();
				}
			}
		}

		// 额定风量
//		contents[3][1] = sMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME) + (rMap.size() > 0
//				? (UtilityConstant.SYS_PUNCTUATION_SLASH + rMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME))
//				: (UtilityConstant.SYS_PUNCTUATION_SLASH));
        if(hasFan){
            contents[3][1] = String.valueOf(noChangeMap.get(UtilityConstant.METAHU_SAIRVOLUME)) + (rMap.size() > 0
                    ? (UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME)))
                    : (UtilityConstant.SYS_BLANK));
        }else{//没有风机额定风量为空
            contents[3][1] = SYS_BLANK;
        }

		// 获取输入功率
		if (rMap.size()>0) {
			String sInputPower = getInputPower(sMap);
			String rInputPower = getInputPower(rMap);
			contents[4][1] = sInputPower + (rMap.size() > 0 ? (UtilityConstant.SYS_PUNCTUATION_SLASH + rInputPower)
					: (UtilityConstant.SYS_PUNCTUATION_SLASH));
		}else {
			contents[4][1] = getInputPower(noChangeMap);
		}

		//获取电机功率
		if (rMap.size()>0) {
			String sMotorPower = (String)sMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER);

			String rMotorPower = (String)rMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER);
			contents[5][1] = sMotorPower + (rMap.size() > 0 ? (UtilityConstant.SYS_PUNCTUATION_SLASH + rMotorPower)
					: (UtilityConstant.SYS_PUNCTUATION_SLASH));
		}

		// 全静压
		contents[3][4] = (EmptyUtil.isNotEmpty(sMap.get(UtilityConstant.METASEXON_FAN_TOTALSTATIC))?String.valueOf(sMap.get(UtilityConstant.METASEXON_FAN_TOTALSTATIC)):UtilityConstant.SYS_BLANK)
				+
				(rMap.size() > 0 ? (UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(rMap.get(UtilityConstant.METASEXON_FAN_TOTALSTATIC)))
				: (UtilityConstant.SYS_BLANK));

		//机外静压
        if(hasFan) {
            contents[4][4] = String.valueOf(noChangeMap.get(UtilityConstant.METAHU_SEXTERNALSTATIC)) + (rMap.size() > 0
                    ? (UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EEXTERNALSTATIC)))
                    : (UtilityConstant.SYS_BLANK));
        }else{//没有风机机外静压为空
            contents[4][4] = SYS_BLANK;
        }
		return contents;
	}

	/**
	 * 封装机组铭牌冷热量
	 * 1 蒸汽盘管/电加热/冷水盘管/热水盘管会有加热量，冷水盘管会有制冷量。
	 * 2 冷水盘管选择了夏季对应冷量， 选择冬季，对应热量
	 * 3 对于蒸汽盘管/电加热 同时选择夏季或者冬季，选大的值。
	 * @param contents
	 * @param ahuPartitionList
	 * @return
	 */
	public static String[][] getUnitNamePlateDataColdQHeatQ(String[][] contents, List<AhuPartition> ahuPartitionList, Map<String, String> noChangeMap) {

		//冷量
		String enableSummer = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_COOLINGCOIL_ENABLESUMMER));
		if (enableSummer.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
			contents[2][1] = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_COOLINGCOIL_SRETURNCOLDQ));
		}

		//热量
		Double heatQ = 0.0;
		for (AhuPartition ap : ahuPartitionList) {
			LinkedList<Map<String, Object>> sections = ap.getSections();
			for (Map<String, Object> section : sections) {
				String metaId = String.valueOf(section.get(S_MKEY_METAID));
				String sectionMetaJson = String.valueOf(section.get(S_MKEY_METAJSON));
				Map<String, String> sectionMap = new CaseInsensitiveMap();
				sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));
				if(SectionTypeEnum.TYPE_STEAMCOIL.getId().equals(metaId)
						){

					String enableSteamSummer = MapValueUtils.getStringValueByStrMap(UtilityConstant.METASEXON_STEAMCOIL_ENABLESUMMER,sectionMap);
					String enableSteamWinter = MapValueUtils.getStringValueByStrMap(UtilityConstant.METASEXON_STEAMCOIL_ENABLEWINTER,sectionMap);
					Double heatSteamSummerQ = MapValueUtils.getDoubleValueByStrMap(UtilityConstant.METASEXON_STEAMCOIL_SHEATQ, sectionMap);
					Double heatSteamWinterQ = MapValueUtils.getDoubleValueByStrMap(UtilityConstant.METASEXON_STEAMCOIL_WHEATQ, sectionMap);
					if("true".equalsIgnoreCase(enableSteamSummer) && "true".equalsIgnoreCase(enableSteamWinter)){
						heatQ += heatSteamSummerQ>heatSteamWinterQ?heatSteamSummerQ:heatSteamWinterQ;
					}else if("true".equalsIgnoreCase(enableSteamSummer)){
						heatQ += heatSteamSummerQ;
					}else if("true".equalsIgnoreCase(enableSteamWinter)){
						heatQ += heatSteamWinterQ;
					}
				}else if(SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId().equals(metaId)
						){

					String enableelectricHeatingCoilSummer = MapValueUtils.getStringValueByStrMap(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_ENABLESUMMER,sectionMap);
					String enableelectricHeatingCoilWinter = MapValueUtils.getStringValueByStrMap(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_ENABLEWINTER,sectionMap);
					Double heatelectricHeatingCoilSummerQ = MapValueUtils.getDoubleValueByStrMap(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_POWER_SHEATQ, sectionMap);
					Double heatelectricHeatingCoilWinterQ = MapValueUtils.getDoubleValueByStrMap(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_POWER_WHEATQ, sectionMap);

					if("true".equalsIgnoreCase(enableelectricHeatingCoilSummer) && "true".equalsIgnoreCase(enableelectricHeatingCoilWinter)){
						heatQ += heatelectricHeatingCoilSummerQ>heatelectricHeatingCoilWinterQ?heatelectricHeatingCoilSummerQ:heatelectricHeatingCoilWinterQ;
					}else if("true".equalsIgnoreCase(enableelectricHeatingCoilSummer)){
						heatQ += heatelectricHeatingCoilSummerQ;
					}else if("true".equalsIgnoreCase(enableelectricHeatingCoilWinter)){
						heatQ += heatelectricHeatingCoilWinterQ;
					}
				}else if(SectionTypeEnum.TYPE_COLD.getId().equals(metaId)
						){

					String enableelectricCoolingCoilWinter = MapValueUtils.getStringValueByStrMap(UtilityConstant.METASEXON_COOLINGCOIL_ENABLEWINTER,sectionMap);
					Double heatelectricCoolingCoilWinterQ = MapValueUtils.getDoubleValueByStrMap(UtilityConstant.METASEXON_COOLINGCOIL_WRETURNHEATQ, sectionMap);
					if("true".equalsIgnoreCase(enableelectricCoolingCoilWinter)){
						heatQ += heatelectricCoolingCoilWinterQ;
					}
				}else if(SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(metaId)
						){

					Double heatCoilQ = MapValueUtils.getDoubleValueByStrMap(UtilityConstant.METASEXON_HEATINGCOIL_WRETURNHEATQ, sectionMap);
					heatQ+=heatCoilQ;
				}
			}
		}

		DecimalFormat df = new DecimalFormat("#.0");
		contents[2][4] = BaseDataUtil.constraintString(df.format(heatQ));

		return contents;
	}
	private static boolean hasSectionType(Map<String, Map<String, String>> allMap, SectionTypeEnum typeEnum) {
		boolean hasType = false;
		Iterator<String> ki = allMap.keySet().iterator();
		while(ki.hasNext()){
			String key = ki.next();
			if(key.contains(typeEnum.getId())){
				hasType = true;
			}
		}
		return hasType;
	}

	/**
	 * 封装主要参数列表 风机特殊数据
	 * @param contents
	 * @param map
	 * @param language
	 * @return
	 */
	public static String[][] getFan4Paralist(String[][] contents, Map<String, String> map,LanguageEnum language) {

		//计算轴功率
    	double airVolume = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
    	double totalPressure = Double.parseDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
    	double efficiency = BaseDataUtil.stringConversionDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
    	double shaftPower = (airVolume*totalPressure*100)/(3600*efficiency*1000);
    	BigDecimal shaftPowerBd = new BigDecimal(shaftPower);//四舍五入，保留两位小数
    	shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
    	contents[7][2] = String.valueOf(shaftPowerBd);

		String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
		if (map.containsKey(airDirection)) {
			String value = BaseDataUtil.constraintString(map.get(airDirection));
			if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {// 回风
				contents[0][2] = String.valueOf(map.get(UtilityConstant.METAHU_EAIRVOLUME));
			} else if (value.equals(UtilityConstant.SYS_ALPHABET_S_UP)) {// 送风
				contents[0][2] = String.valueOf(map.get(UtilityConstant.METAHU_SAIRVOLUME));
			}
		}

		//参数汇总表中增加大功率的电机（≥30kw）电机基座型号；
//        double motorPower = Double.parseDouble(String.valueOf(map.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)));
//        if(motorPower<30){
//            contents[13][2] = "";
//        }

        return contents;
	}

	/**
	 * 封装报告中有关ahu高度的计算
	 * @param allMap
	 * @param partition
	 * @param unit
	 * @return
	 */
	public static String wrapHeight(Map<String, String> allMap, Partition partition, Unit unit, AhuPartition ap) {
		String finalHeight = UtilityConstant.SYS_BLANK;
		String series = unit.getSeries();
		int type = Integer.valueOf(series.substring(series.length() - 4, series.length()));
		String heightInMap = String.valueOf(allMap.get(UtilityConstant.METAHU_HEIGHT));
		if (EmptyUtil.isNotEmpty(heightInMap)) {
			if (608 <= type && SystemCountUtil.lteSmallUnit(type)) {
				finalHeight = String.valueOf(BaseDataUtil.decimalConvert(Double.valueOf(heightInMap) + (ap.isTopLayer()?0:100),1));
			} else if (SystemCountUtil.gteBigUnit(type) && type <= 3438) {
				finalHeight = String.valueOf(BaseDataUtil.decimalConvert(Double.valueOf(heightInMap) + (ap.isTopLayer()?0:200),1));
			}else{
				finalHeight = heightInMap;
			}
		} else {
			return heightInMap;
		}
		return finalHeight;
	}
	public static String[] getMPStrs(Unit unit, AhuPartition ap, List<AhuPartition> ahuPartitionList) {

		//吊装机组：技术说明&铭牌数据里长度尺寸: +500
		String unitMetaJson = unit.getMetaJson();
		Map<String, String> umap = new HashMap<String, String>();
		if (org.apache.commons.lang3.StringUtils.isNotBlank(unitMetaJson)) {
			umap = JSON.parseObject(unitMetaJson, Map.class);
		}
		String nsPriceEnable = String.valueOf(umap.get(SectionMetaUtils.getNSAHUKey(SectionMetaUtils.MetaKey.KEY_NS_ENABLE)));
		String nsChannelSteelBase = String.valueOf(umap.get(SectionMetaUtils.getNSAHUKey(SectionMetaUtils.MetaKey.KEY_NS_CHANNEL_STEEL_BASE)));
		int append10HBPlus500 = 0;
		if (SYS_ASSERT_TRUE.equals(nsPriceEnable) && SYS_ASSERT_TRUE.equals(nsChannelSteelBase)){//允许非标&&槽钢底座
			String nsChannelSteelBaseModel = umap.get(SectionMetaUtils.getNSAHUKey(SectionMetaUtils.MetaKey.KEY_NS_CHANNEL_STEEL_BASE_MODEL));
			boolean isBASEMODEL10HB = nsChannelSteelBaseModel.equalsIgnoreCase(AHU_DELIVERY_NSCHANNELSTEELBASEMODEL10HB);//吊装机组
			if(isBASEMODEL10HB){
				append10HBPlus500 = 500;
			}
		}

		double partitionWeight = 0;
		partitionWeight = getPartitionWeight(unit, ap, ahuPartitionList);

		String[] temContents;
		String series = unit.getSeries();
		int standtType = Integer.valueOf(series.substring(series.length() - 4, series.length()));
		//面板切割页面变形处理。
		if(EmptyUtil.isNotEmpty(unit.getPanelSeries()) && !series.equals(unit.getPanelSeries())) {
			series = unit.getPanelSeries();
		}
		int height = AhuUtil.getHeightOfAHU(series);
		if (608 <= standtType && SystemCountUtil.lteSmallUnit(standtType) || SystemCountUtil.gteBigUnit(standtType) && standtType <= 3438) {
			String[] apArray = new String[] { UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK + (ap.getPos() + 1),
					UtilityConstant.SYS_BLANK + (Integer.valueOf(ap.getLength()+append10HBPlus500 + ap.getCasingWidth())),
					UtilityConstant.SYS_BLANK + (Integer.valueOf(series.substring(series.length() - 2, series.length())) * 100 + ap.getCasingWidth()),
					String.valueOf(BaseDataUtil.stringConversionInteger(wrapHeight(standtType,height, ap))),
					UtilityConstant.SYS_BLANK + BaseDataUtil.doubleConversionInteger(partitionWeight) };
			temContents = apArray;
		} else {
			String[] apArray = new String[] { UtilityConstant.SYS_BLANK, UtilityConstant.SYS_BLANK + (ap.getPos() + 1),
					UtilityConstant.SYS_BLANK + (ap.getLength()+append10HBPlus500 + ap.getCasingWidth()),
					UtilityConstant.SYS_BLANK + (ap.getWidth() * 100 + ap.getCasingWidth()),
					UtilityConstant.SYS_BLANK + (ap.getHeight() * 100 + ap.getCasingWidth()),
					UtilityConstant.SYS_BLANK + BaseDataUtil.doubleConversionInteger(partitionWeight) };
			temContents = apArray;
		}
		return temContents;
	}

	public static double getPartitionWeight(Unit unit, AhuPartition ap, List<AhuPartition> ahuPartitionList) {
		double partitionWeight;
		if (AhuPartitionUtils.hasEndFacePanel(ap, ahuPartitionList)) {
			partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ap, ahuPartitionList.size(), true);
		} else {
			partitionWeight = AhuPartitionUtils.calculatePartitionWeight(unit, ap, ahuPartitionList.size(), false);
		}
		return partitionWeight;
	}

	/**
	 * 封装报告中有关ahu高度的计算 标准型号为小型号的进行+100 底座高度的处理
	 * @param type 必须是标准型号严禁传入非标型号
	 * @param ap
	 * @return
	 */
	public static String wrapHeight(int type,int height,AhuPartition ap) {
		height = height * 100 + ap.getCasingWidth();
		String finalHeight = UtilityConstant.SYS_BLANK;
		if (608 <= type && SystemCountUtil.lteSmallUnit(type)) {
			finalHeight = String.valueOf(BaseDataUtil.decimalConvert(height + (ap.isTopLayer()?0:100),1));
		} else if (SystemCountUtil.gteBigUnit(type) && type <= 3438) {
			finalHeight = ""+(ap.getHeight() * 100 + ap.getCasingWidth());
		}
		return finalHeight;
	}
	/**
	 * 封装段连接清单报告
	 * @param ahuMap
	 * @return
	 */
	public static String getConnectionList(Map<String, String> ahuMap) {
		String inskinm = ahuMap.get(UtilityConstant.METAHU_INSKINM);//材质（内）
		String targetStr = "";
		if ("GlSteel".equals(inskinm)) {
			targetStr = UtilityConstant.SYS_STRING_NUMBER_2;// 2=镀锌
		} else if ("prePaintedSteal".equals(inskinm) || "paintedColdRolledSteel".equals(inskinm)
				|| "HDPPrePaintedSteel".equals(inskinm)) {// 1=冷轧，彩钢板，高耐久性聚酯面板
			targetStr = UtilityConstant.SYS_STRING_NUMBER_1;
		} else if ("stainless".equals(inskinm)) {// 3=不锈钢
			targetStr = UtilityConstant.SYS_STRING_NUMBER_3;
		}
		return targetStr;
		/*if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD.equals(paneltype)) {//39G
			contents = handleReplacement(contents, 1, 1, 10, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 2, 1, 12, contents[2][1].length(), targetStr);
			contents = handleReplacement(contents, 3, 1, 12, contents[3][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_FOREPART.equals(paneltype)) {//39GForepart
			contents = handleReplacement(contents, 1, 1, 10, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 2, 1, 12, contents[2][1].length(), targetStr);
			contents = handleReplacement(contents, 3, 1, 12, contents[3][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE.equals(paneltype)) {//39GPositive
			contents = handleReplacement(contents, 1, 1, 12, contents[1][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD.equals(paneltype)) {//39CQStandard
			contents = handleReplacement(contents, 1, 1, 11, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 9, 1, 10, contents[9][1].length(), targetStr);
			contents = handleReplacement(contents, 10, 1, 10, contents[10][1].length(), targetStr);
			contents = handleReplacement(contents, 11, 1, 10, contents[11][1].length(), targetStr);
			contents = handleReplacement(contents, 12, 1, 10, contents[12][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_FOREPART.equals(paneltype)) {//39CQForepart
			contents = handleReplacement(contents, 1, 1, 11, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 8, 1, 10, contents[8][1].length(), targetStr);
			contents = handleReplacement(contents, 9, 1, 10, contents[9][1].length(), targetStr);
			contents = handleReplacement(contents, 10, 1, 10, contents[10][1].length()-8, targetStr);
			contents = handleReplacement(contents, 11, 1, 10, contents[11][1].length()-8, targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE.equals(paneltype)) {//39CQPositive
			contents = handleReplacement(contents, 1, 1, 13, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 9, 1, 10, contents[9][1].length(), targetStr);
			contents = handleReplacement(contents, 10, 1, 10, contents[10][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39XT.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD.equals(paneltype)) {//39XTStandard
			contents = handleReplacement(contents, 1, 1, contents[1][1].length()-1, contents[1][1].length(), targetStr);
			contents = handleReplacement(contents, 2, 1, contents[2][1].length()-1, contents[2][1].length(), targetStr);
			contents = handleReplacement(contents, 3, 1, contents[3][1].length()-1, contents[3][1].length(), targetStr);
			contents = handleReplacement(contents, 4, 1, contents[4][1].length()-1, contents[4][1].length(), targetStr);
			contents = handleReplacement(contents, 5, 1, contents[5][1].length()-1, contents[5][1].length(), targetStr);
		}else if (UtilityConstant.SYS_UNIT_SERIES_39XT.equals(unitSeries)
				&& UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE.equals(paneltype)) {//39XTPositive
			contents = handleReplacement(contents, 1, 1, 12, contents[1][1].length(), targetStr);
		}

		return contents;*/
	}

	/**
	 * 从二位数组中指定位置的字符串中，替换指定位置的字符。
	 * @param contents	二维数组
	 * @param row	二维数组行数
	 * @param col	二维数组列数
	 * @param start	替换字符串起始位置
	 * @param targetStr	目标字符
	 * @return	修改过后的二维数组（String[][]）
	 */
	private static String[][] handleReplacement(String[][] contents, int row, int col, int start, int end, String targetStr) {
		String content = contents[row][col];
		StringBuilder contentSB = new StringBuilder(content);
		contentSB.replace(start, end, targetStr);
		contents[row][col] = contentSB.toString();
		return contents;
	}

	/**
	 * 主要参数列表 [混合段]
	 * @param contents
	 * @param allMap
	 * @param language
	 * @return
	 */
	public static String[][] getParaMix(String[][] contents, Map<String, String> allMap, LanguageEnum language){
		String serial = allMap.get(UtilityConstant.METAHU_SERIAL);
		String returnTop = UtilityConstant.METASEXON_MIX_RETURNTOP;//1
        String returnBack = UtilityConstant.METASEXON_MIX_RETURNBACK;//2
        String returnLeft = UtilityConstant.METASEXON_MIX_RETURNLEFT;//3
        String returnRight = UtilityConstant.METASEXON_MIX_RETURNRIGHT;//4
        String returnButtom = UtilityConstant.METASEXON_MIX_RETURNBUTTOM;//5

		int returnTopWF = 1;
		int returnBackWF = 2;
		int returnLeftWF = 3;
		int returnRightWF = 4;
		int returnButtomWF = 5;

        String product = allMap.get(UtilityConstant.METAHU_PRODUCT);
        String mixSectionL = String.valueOf(allMap.get(UtilityConstant.METASEXON_MIX_SECTIONL));//段长
        Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));

        boolean hasReturnTopWF = false;
		boolean hasReturnButtomWF =false;
		boolean hasReturnLeftWF =false;
		boolean hasReturnRightWF =false;
		boolean hasReturnBackWF =false;

		for (int i = 0; i < contents.length; i++) {

        	int returnWF = 0;
			if (!hasReturnTopWF && allMap.containsKey(returnTop)
					&& BaseDataUtil.constraintString(allMap.get(returnTop)).equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                	returnWF = returnTopWF;
					hasReturnTopWF = true;
            } else if (!hasReturnButtomWF && allMap.containsKey(returnButtom)
					&& BaseDataUtil.constraintString(allMap.get(returnButtom)).equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                	returnWF = returnButtomWF;
					hasReturnButtomWF = true;
            } else if (!hasReturnLeftWF && allMap.containsKey(returnLeft)
            		&& BaseDataUtil.constraintString(allMap.get(returnLeft)).equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                	returnWF = returnLeftWF;
					hasReturnLeftWF = true;
            } else if (!hasReturnRightWF && allMap.containsKey(returnRight)
					&& BaseDataUtil.constraintString(allMap.get(returnRight)).equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                	returnWF = returnRightWF;
					hasReturnRightWF = true;
            } else if (!hasReturnBackWF && allMap.containsKey(returnBack)
					&& BaseDataUtil.constraintString(allMap.get(returnBack)).equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                	returnWF = returnBackWF;
					hasReturnBackWF = true;
            }

			//if (i == 0 || i == 1) {
            DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class,
                    serial.substring(0, serial.length() - 4), returnWF + UtilityConstant.SYS_BLANK, 2==returnWF?damper.getSectionL():mixSectionL);
            String A = UtilityConstant.SYS_BLANK;
            String C = UtilityConstant.SYS_BLANK;

            int lenth = serial.length();
            String heightStr = serial.substring(lenth - 4, lenth - 2);
            String weightStr = serial.substring(lenth - 2);
            int height = Integer.parseInt(heightStr);
            int width = Integer.parseInt(weightStr);

            if (EmptyUtil.isEmpty(nsion)) {
                logger.error("查表未获取到DamperDimension @封装综合过滤段报告数据 报告使用");
            } else {
                String metrial = allMap.get(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL);
                if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(metrial)) {
                    double c1 = Double.parseDouble(nsion.getC1());
                    A = UtilityConstant.SYS_BLANK+(c1 - 2 * 40);
                } else {
                    A = nsion.getA();
                }
                int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);

                if (returnWF == 1 || returnWF == 2) {
                    C = UtilityConstant.SYS_BLANK+((width - 1) * 100 + DIMENSION_C_calPara - 72);
                    if(!UtilityConstant.SYS_BLANK.equals(A) && returnWF == 1){
                        contents[0][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
                    }else if(!UtilityConstant.SYS_BLANK.equals(A) && returnWF == 2){
                        contents[1][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
                    }
                }
                if (returnWF == 3 || returnWF == 4) {
                    C = UtilityConstant.SYS_BLANK + ((height - 1) * 100 + DIMENSION_C_calPara - 72);
                    if(!UtilityConstant.SYS_BLANK.equals(A) && returnWF == 3){
                        contents[2][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
                    }else if(!UtilityConstant.SYS_BLANK.equals(A) && returnWF == 4){
                        contents[3][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
                    }
                }
                if (returnWF == 5) {
                    C = UtilityConstant.SYS_BLANK+((width - 5) * 100 + DIMENSION_C_calPara - 72);
                    if(!UtilityConstant.SYS_BLANK.equals(A) && returnWF == 5){
                        contents[4][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
                    }
                }
            }
//            if(!UtilityConstant.SYS_BLANK.equals(A) && !UtilityConstant.SYS_BLANK.equals(C)){
//                contents[i][2] = C+UtilityConstant.SYS_ALPHABET_X_UP+A;//回风阀门尺寸
//            }

	       // }
        }
		return contents;
	}

	/**
	 * 根据空气方向设置风量
	 * @param contents
	 * @param map
	 * @param row
	 * @param col
	 */
	private static void setAirVolume(String[][] contents, Map<String, String> map,int row,int col) {
		String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
		if (map.containsKey(airDirection)) {
			String value = BaseDataUtil.constraintString(map.get(airDirection));
			if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {// R--回风
				contents[row][col] = String.valueOf(map.get(UtilityConstant.METAHU_EAIRVOLUME));
			} else if (value.equals(UtilityConstant.SYS_ALPHABET_S_UP)) {// S--送风
				contents[row][col] = String.valueOf(map.get(UtilityConstant.METAHU_SAIRVOLUME));
			}
		}
	}

	/**
	 * 过滤AHU非标项
	 *
	 * @param contents
	 * @param sectionType
	 * @return
	 */
	private static String[][] filterContents(String[][] contents, SectionTypeEnum sectionType) {
		ArrayList<String[]> templist = new ArrayList<String[]>(Arrays.asList(contents));
		List<Integer> tempI = new ArrayList<Integer>();

		// 1.过滤：判断第一个值是否为true，将最后的价格去除，否则标记这一行
		for (int i = 0; i < templist.size(); i++) {
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(templist.get(i)[1])) {
				// 清空enable
				//templist.get(i)[1] = UtilityConstant.SYS_BLANK;
				templist.get(i)[1] = reBuildEnable(templist.get(i)[1]);
				// 清空差价
				templist.get(i)[6] = UtilityConstant.SYS_BLANK;
				templist.get(i)[7] = UtilityConstant.SYS_BLANK;
			} else {
				// 标记Enable为false的非标项到tempI中
				tempI.add(i);
			}
		}
		ArrayList<String[]> outTemplist = new ArrayList<String[]>();
		// 2.生成返回的临时list内容
		for (int m = 0; m < templist.size(); m++) {
			boolean key = true;
			// 如果不是tempI中标记的非标项，则添加到outTemplist中用来生成返回的content
			for (int a : tempI) {
				// templist.remove(a-i);
				if (m == a) {
					key = false;
//					outTemplist.add(templist.get(m));
				}
			}
			if(key) {
				outTemplist.add(templist.get(m));
			}
		}
		// 去除标题
		if (outTemplist.size() > 0) {
			//outTemplist.remove(0);
		}

		// 3.临时list转成输出的content二维数组
		String[][] returnContents = new String[outTemplist.size()][contents[0].length];
		if (outTemplist.size() > 0) {
			outTemplist.toArray(returnContents);
		}
		return returnContents;
	}

	/**
	 * 转换true false
	 * @param enable
	 * @return
	 */
	private static String reBuildEnable(String enable) {
		if(SYS_ASSERT_TRUE.equals(enable.toLowerCase())){
			return getIntlString(YES);
		}else if(SYS_ASSERT_FALSE.equals(enable.toLowerCase())){
			return getIntlString(NO);
		}else{
			return enable;
		}
	}

	private static String getInputPower(Map<String, String> noChangeMap) {
		String RtinputPower = "";

		//计算轴功率
    	double airVolume = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
    	double totalPressure = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
    	double efficiency = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
    	double shaftPower = (airVolume*totalPressure*100)/(3600*efficiency*1000);
    	if(Double.isNaN(shaftPower)){
    		return RtinputPower;
		}
    	/*BigDecimal shaftPowerBd = new BigDecimal(shaftPower);//四舍五入，保留两位小数
    	shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);*/
		//计算输入功率
    	/*double motorSafetyFacor = new Double(0);
    	double motorPower = BaseDataUtil.stringConversionDouble((String)noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
    	if(motorPower<0.5) {
    		motorSafetyFacor = 1.5;
    	}else if (motorPower>0.5 && motorPower<1) {
			motorSafetyFacor = 1.4;
		}else if (motorPower>1 && motorPower<2) {
			motorSafetyFacor = 1.3;
		}else if (motorPower>2 && motorPower<5) {
			motorSafetyFacor = 1.2;
		}else if(motorPower>5) {
			motorSafetyFacor = 1.15;
		}
    	double actualMotorPower = (shaftPower*motorSafetyFacor)/(1);
    	System.out.println("#############################actualMotorPower=========="+actualMotorPower);
    	String motorEfficiencyStr = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFF));
    	NumberFormat nf=NumberFormat.getPercentInstance();
    	Number motorEfficiencyNum;
		try {
			if(EmptyUtil.isNotEmpty(motorEfficiencyStr)) {
				motorEfficiencyNum = nf.parse(motorEfficiencyStr);
			}else {
				motorEfficiencyNum = 0;
			}

			double motorEfficiency = motorEfficiencyNum.doubleValue();
			if(motorEfficiency==0) {
	    		motorEfficiency = 0.94;
	    	}
			double inputPower = (motorPower) / (motorEfficiency);
			BigDecimal inputPowerBd = new BigDecimal(inputPower);// 四舍五入，保留两位小数
			inputPowerBd = inputPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
			RtinputPower = String.valueOf(inputPowerBd);
		} catch (ParseException e) {
			e.printStackTrace();
		}*/
		//输入功率=轴功率*1.35
		BigDecimal inputPowerBd = new BigDecimal(shaftPower*1.35);// 四舍五入，保留两位小数
		inputPowerBd = inputPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
		RtinputPower = String.valueOf(inputPowerBd);
		return RtinputPower;
	}

	/**
	 * 计算能效
	 *
	 * @param contents
	 * @param unit
	 * @param allMap
	 */
	private static void calEnergyEfficiency(String[][] contents, Unit unit, Map<String, Map<String, String>> allMap,
                                            List<PartPO> partList) {
		initTable2();
		Map<String, String> unitMap = allMap.get(UnitConverter.UNIT);// 机组数据map
		Map<String, String> rfanMap = new HashMap<String, String>();// 回风机数据map
		Map<String, String> sfanMap = new HashMap<String, String>();// 送风机数据map
		Map<String, String> filterMap = new HashMap<String, String>();// 单层过滤段数据map
		Map<String, String> heatRecoveryMap = new HashMap<String, String>();// 热回收段数据map
		Map<String, String> mixMap = new HashMap<String, String>();// 混合段数据map
		Map<String, String> steamHumidifierMap = new HashMap<String, String>();// 干蒸加湿段数据map
		Map<String, String> wetFilmHumidifierMap = new HashMap<String, String>();// 湿膜加湿段数据map
		Map<String, String> sprayHumidifierMap = new HashMap<String, String>();// 高压喷雾加湿段数据map
		Map<String, String> electrodeHumidifierMap = new HashMap<String, String>();// 电极加湿段数据map
		Map<String, String> coolingCoilMap = new HashMap<String, String>();// 冷水盘管段数据map
		Map<String, String> heatingCoilMap = new HashMap<String, String>();// 热水盘管段数据map
		Map<String, String> combinedmixingChamberMap = new HashMap<String, String>();// 新回排数据map
		Map<String, String> electricHeatingCoilMap = new HashMap<String, String>();// 电加热盘管数据map
		int order = 1;
		List<PartPO> orderedPartList = AhuPartitionUtils.getAirflowOrderedPartPOList(unit, partList);
		for (PartPO partPO : orderedPartList) {
			Part part = partPO.getCurrentPart();
			String key = part.getSectionKey();
			// 获取风机数据map
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_FAN.getId().equals(key)) {
				Map<String, String> tempFanMap = allMap.get(UnitConverter.genUnitKey(unit, part));
				//判断风机气流方向
				String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
		        if (tempFanMap.containsKey(airDirection)) {
		            String value = BaseDataUtil.constraintString(tempFanMap.get(airDirection));
		            if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {//回风
		            	rfanMap.putAll(tempFanMap);
		            }else {//送风
		            	sfanMap.putAll(tempFanMap);
		            }
		        }
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_SINGLE.getId().equals(key)) {
				filterMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(key)) {
				heatRecoveryMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_MIX.getId().equals(key)) {
				mixMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(key)) {
				steamHumidifierMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(key)) {
				wetFilmHumidifierMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(key)) {
				sprayHumidifierMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(key)) {
				electrodeHumidifierMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_COLD.getId().equals(key)) {
				coolingCoilMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(key)) {
				heatingCoilMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(key)) {
				combinedmixingChamberMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId().equals(key)) {
				electricHeatingCoilMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			order++;
		}

		//遍历各个段获取室外温度
		getOutsideTemp(heatRecoveryMap, mixMap, steamHumidifierMap, wetFilmHumidifierMap, sprayHumidifierMap,
				electrodeHumidifierMap, coolingCoilMap, heatingCoilMap, combinedmixingChamberMap);
		//计算送/回风机的输入功率
		double r_inputPower = 0f;
		double r_ISP = 0f;
		double s_ISP = 0f;
		double r_TSP = 0.0;
		double s_TSP = 0.0;
		double s_inputPower = 0f;
		double r_systemEffect = 0f;
		double s_systemEffect = 0f;
		if (EmptyUtil.isNotEmpty(rfanMap)) {
			r_inputPower = calInputPower(rfanMap);
			//获取送/回风机的ISP
			r_ISP = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(rfanMap.get("meta.section.fan.sysStatic")));
			//计算送/回风机的systemEffect
			r_systemEffect = r_ISP * 0.1;
			//获取送/回风机的TSP
			r_TSP = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(rfanMap.get("meta.section.fan.totalStatic")));
		}
		if (EmptyUtil.isNotEmpty(sfanMap)) {
			s_inputPower = calInputPower(sfanMap);
			//获取送/回风机的ISP
			s_ISP = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(sfanMap.get("meta.section.fan.sysStatic")));
			//计算送/回风机的systemEffect
			s_systemEffect = s_ISP * 0.1;
			//获取送/回风机的TSP
			s_TSP = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(sfanMap.get("meta.section.fan.totalStatic")));
		}
		// 计算风量
		String airDirectionStr = BaseDataUtil.constraintString(filterMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
		String airVolume;
		if (airDirectionStr.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {
			airVolume = BaseDataUtil.constraintString(unitMap.get(UtilityConstant.METAHU_EAIRVOLUME));
		} else {
			airVolume = BaseDataUtil.constraintString(unitMap.get(UtilityConstant.METAHU_SAIRVOLUME));
		}
		double airVolumeDB = BaseDataUtil.stringConversionDouble(airVolume);
		//计算通过过滤器风速（若没有过滤器则计算风机的面风速）  filter velocity=风量/截面积
		double s_velocity = 1f;
		double r_velocity = 1f;
		if (filterMap.size() > 0) {
			SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class,
					filterMap.get(UtilityConstant.METAHU_SERIAL));
			double area = sectionArea.getBin();
			r_velocity = airVolumeDB /3600 / area;// 过滤器风速
			s_velocity = airVolumeDB /3600 / area;// 过滤器风速
		} else {
			r_velocity = BaseDataUtil.stringConversionDouble(rfanMap.get(UtilityConstant.METAHU_EVELOCITY));// 过滤器风速
			s_velocity = BaseDataUtil.stringConversionDouble(sfanMap.get(UtilityConstant.METAHU_SVELOCITY));// 过滤器风速
		}

		double r_a = BaseDataUtil.stringConversionDouble(rfanMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)) > 10 ? 1.1f
				: 4.56f;
		double s_a = BaseDataUtil.stringConversionDouble(sfanMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)) > 10 ? 1.1f
				: 4.56f;
		double r_b = BaseDataUtil.stringConversionDouble(rfanMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)) > 10 ? 2.6f
				: 10.5f;
		double s_b = BaseDataUtil.stringConversionDouble(sfanMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)) > 10 ? 2.6f
				: 10.5f;
		double heatRecoveryEfficiency = BaseDataUtil
				.stringConversionDouble(heatRecoveryMap.get("meta.section.wheelHeatRecycle.HeatRecoveryEfficiency"));

//		// step1:
//		double fpe = 8.1 - (0.0035 * outsideTemp + 0.79) * outsideTemp;
//		// step2:（internal static pressure - system effect - HRS pressure drop）x （
//		// 1-（table2 风速/ 通过过滤器风速）^1.4）
//		double DeltaPx = (ISP - systemEffect
//				- (((EmptyUtil.isNotEmpty(heatRecoveryMap))
//						? (BaseDataUtil.stringConversionDouble(heatRecoveryMap.get("")))// HRS pressure
//																						// drop怎么取？？？？？？？？？？？？？？？？
//						: 0)))
//				* (1 - Math.pow((1 / velocity), 1.4));// table2 风速怎么取？循环？？？？？？？？？？？？？？？？？？？？
//		// step3:HRS pressure drop - tabe2里的P class
//		double DeltaPy = BaseDataUtil.stringConversionDouble(heatRecoveryMap.get("")) - 1f;// HRS pressure
//																							// drop怎么取？tabe2里的P
//																							// class怎么取？
//		// step4:（Table2效率 - 冬季转轮效率 + 5x 0 或1 ） x （1- 新风比/100） x f pe
//		double DeltaPz = (1f - heatRecoveryEfficiency + 5 * 1) * (1 - 1f / 100) * fpe;
//		// step5:
//		double Pairside_ref = (Math.abs(ISP - systemEffect - (DeltaPx + DeltaPy + DeltaPz)) * airVolumeDB)
//				/ (a * Math.log(inputPower) - b + 1f);// a、b取值？ table2里Ngref？？？？？？？
//		// step6:
//		// double Fs-Pref = ()/();

		int time = 1;
		for (String key : table2.keySet()) {
//			String energyEfficiencyClass = entry.getKey();
//			System.out.println("---------能效等级----------"+energyEfficiencyClass);
//			Map<String, Double> paraMap = entry.getValue();
			Map<String, Double> paraMap = table2.get(key);

			//开始计算步骤：
			double r_DeltaPx = 0f;
			double r_DeltaPy = 0f;
			double r_DeltaPz = 0f;
			double s_DeltaPx = 0f;
			double s_DeltaPy = 0f;
			double s_DeltaPz = 0f;
			double Pairside_ref_extract;
			double Pairside_ref_supply;
			double fpe = step1();
			if (heatRecoveryMap.size() > 0) {
				r_DeltaPx = step2(r_ISP, r_systemEffect, BaseDataUtil.objectToDouble(paraMap.get("Velocity")),
						r_velocity, heatRecoveryMap);
				r_DeltaPy = step3(BaseDataUtil.objectToDouble(paraMap.get("Pclass")), heatRecoveryMap);
				r_DeltaPz = step4(fpe, BaseDataUtil.objectToDouble(paraMap.get("Efficiency")), heatRecoveryEfficiency,
						electricHeatingCoilMap, outsideTemp, heatRecoveryMap, rfanMap, 1, 50);

				s_DeltaPx = step2(s_ISP, s_systemEffect, BaseDataUtil.objectToDouble(paraMap.get("Velocity")),
						s_velocity, heatRecoveryMap);
				s_DeltaPy = step3(BaseDataUtil.objectToDouble(paraMap.get("Pclass")), heatRecoveryMap);
				s_DeltaPz = step4(fpe, BaseDataUtil.objectToDouble(paraMap.get("Efficiency")), heatRecoveryEfficiency,
						electricHeatingCoilMap, outsideTemp, heatRecoveryMap, rfanMap, 0, 50);
			} else {
				r_DeltaPx = step2(r_ISP, r_systemEffect, BaseDataUtil.objectToDouble(paraMap.get("Velocity")), r_velocity,
						heatRecoveryMap);
				s_DeltaPx = step2(s_ISP, s_systemEffect, BaseDataUtil.objectToDouble(paraMap.get("Velocity")), s_velocity,
						heatRecoveryMap);
			}
			Pairside_ref_extract = EmptyUtil.isNotEmpty(rfanMap)?step5(r_ISP, r_systemEffect, r_DeltaPx, r_DeltaPy, r_DeltaPz,
					(BaseDataUtil.objectToDouble(rfanMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)) / 3600), r_a,
					r_inputPower, r_b, BaseDataUtil.objectToDouble(paraMap.get("Ngref")), r_TSP):0f;
			Pairside_ref_supply = EmptyUtil.isNotEmpty(sfanMap)?step5(s_ISP, s_systemEffect, s_DeltaPx, s_DeltaPy, s_DeltaPz,
					(BaseDataUtil.objectToDouble(sfanMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)) / 3600), s_a,
					s_inputPower, s_b, BaseDataUtil.objectToDouble(paraMap.get("Ngref")), s_TSP):0f;

			double Fs_Pref = step6(s_inputPower, r_inputPower, Pairside_ref_supply, Pairside_ref_extract);
			if (Fs_Pref < 1) {
				contents[0][1] = "             EUROVENT Energy Efficiency Class " + key + " (2016)";
				break;
			}else if (time == 5) {
				contents[0][1] = "             EUROVENT Energy Efficiency Class " + "E" + " (2016)";
			}
			time++;
		}

	}

	/**
	 * 计算能效
	 * @param unit
	 * @param allMap
	 * @param partList
	 * @return
	 */
	public static List<String> calEnergyEfficiency(Unit unit, Map<String, Map<String, String>> allMap,
                                                   List<PartPO> partList) {
		List<String> resultList = new ArrayList<String>();
		initTable2();
		Map<String, String> unitMap = allMap.get(UnitConverter.UNIT);// 机组数据map
		Map<String, String> rfanMap = new HashMap<String, String>();// 回风机数据map
		Map<String, String> sfanMap = new HashMap<String, String>();// 送风机数据map
		Map<String, String> filterMap = new HashMap<String, String>();// 单层过滤段数据map
		Map<String, String> heatRecoveryMap = new HashMap<String, String>();// 热回收段数据map
		Map<String, String> mixMap = new HashMap<String, String>();// 混合段数据map
		Map<String, String> steamHumidifierMap = new HashMap<String, String>();// 干蒸加湿段数据map
		Map<String, String> wetFilmHumidifierMap = new HashMap<String, String>();// 湿膜加湿段数据map
		Map<String, String> sprayHumidifierMap = new HashMap<String, String>();// 高压喷雾加湿段数据map
		Map<String, String> electrodeHumidifierMap = new HashMap<String, String>();// 电极加湿段数据map
		Map<String, String> coolingCoilMap = new HashMap<String, String>();// 冷水盘管段数据map
		Map<String, String> heatingCoilMap = new HashMap<String, String>();// 热水盘管段数据map
		Map<String, String> combinedmixingChamberMap = new HashMap<String, String>();// 新回排数据map
		Map<String, String> electricHeatingCoilMap = new HashMap<String, String>();// 电加热盘管数据map
		int order = 1;
		List<PartPO> orderedPartList = AhuPartitionUtils.getAirflowOrderedPartPOList(unit, partList);
		for (PartPO partPO : orderedPartList) {
			Part part = partPO.getCurrentPart();
			String key = part.getSectionKey();
			// 获取风机数据map
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_FAN.getId().equals(key)) {
				Map<String, String> tempFanMap = allMap.get(UnitConverter.genUnitKey(unit, part));
				//判断风机气流方向
				String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
		        if (tempFanMap.containsKey(airDirection)) {
		            String value = BaseDataUtil.constraintString(tempFanMap.get(airDirection));
		            if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {//回风
		            	rfanMap.putAll(tempFanMap);
		            }else {//送风
		            	sfanMap.putAll(tempFanMap);
		            }
		        }
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_SINGLE.getId().equals(key)) {
				filterMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(key)) {
				heatRecoveryMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_MIX.getId().equals(key)) {
				mixMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(key)) {
				steamHumidifierMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(key)) {
				wetFilmHumidifierMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(key)) {
				sprayHumidifierMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(key)) {
				electrodeHumidifierMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_COLD.getId().equals(key)) {
				coolingCoilMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(key)) {
				heatingCoilMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(key)) {
				combinedmixingChamberMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			if (com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId().equals(key)) {
				electricHeatingCoilMap = allMap.get(UnitConverter.genUnitKey(unit, part));
			}
			order++;
		}	
		//遍历各个段获取室外温度
		getOutsideTemp(heatRecoveryMap, mixMap, steamHumidifierMap, wetFilmHumidifierMap, sprayHumidifierMap,
				electrodeHumidifierMap, coolingCoilMap, heatingCoilMap, combinedmixingChamberMap);
		//计算送/回风机的输入功率
		double r_inputPower = 0.0;
		double r_ISP = 0.0;
		double s_ISP = 0.0;
		double r_TSP = 0.0;
		double s_TSP = 0.0;
		double s_inputPower = 0.0;
		double r_systemEffect = 0.0;
		double s_systemEffect = 0.0;
		if (EmptyUtil.isNotEmpty(rfanMap)) {
			r_inputPower = calInputPower(rfanMap);	
			double r_ESP = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(rfanMap.get("meta.section.fan.externalStatic")));//余压  其他静压
			//获取送/回风机的TSP
			r_TSP = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(rfanMap.get("meta.section.fan.totalStatic")));
			//获取送/回风机的ISP
//			r_ISP = (r_TSP-r_ESP)/1.1;
			r_ISP = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(rfanMap.get("meta.section.fan.sysStatic")));
			//计算送/回风机的systemEffect
			r_systemEffect = r_ISP * 0.1;			
		}
		if (EmptyUtil.isNotEmpty(sfanMap)) {
			s_inputPower = calInputPower(sfanMap);
			double s_ESP = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(sfanMap.get("meta.section.fan.externalStatic")));//余压  其他静压
			//获取送/回风机的TSP
			s_TSP = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(sfanMap.get("meta.section.fan.totalStatic")));
			//获取送/回风机的ISP
			s_ISP = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(sfanMap.get("meta.section.fan.sysStatic")));
			//计算送/回风机的systemEffect
			s_systemEffect = s_ISP * 0.1;
			
		}
		// 计算风量
		String airDirectionStr = BaseDataUtil.constraintString(filterMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
		String airVolume;
		if (airDirectionStr.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {
			airVolume = BaseDataUtil.constraintString(unitMap.get(UtilityConstant.METAHU_EAIRVOLUME));
		} else {
			airVolume = BaseDataUtil.constraintString(unitMap.get(UtilityConstant.METAHU_SAIRVOLUME));
		}
		double airVolumeDB = BaseDataUtil.stringConversionDouble(airVolume);
		//计算通过过滤器风速（若没有过滤器则计算风机的面风速）  filter velocity=风量/截面积
		double s_velocity = 1.0;
		double r_velocity = 1.0;
		if (filterMap.size() > 0) {
			SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class,
					filterMap.get(UtilityConstant.METAHU_SERIAL));
			double area = sectionArea.getBin();
			r_velocity = airVolumeDB /3600.0 / area;// 过滤器风速
			s_velocity = airVolumeDB /3600.0 / area;// 过滤器风速
		} else {
			r_velocity = BaseDataUtil.stringConversionDouble(rfanMap.get(UtilityConstant.METAHU_EVELOCITY));// 过滤器风速
			s_velocity = BaseDataUtil.stringConversionDouble(sfanMap.get(UtilityConstant.METAHU_SVELOCITY));// 过滤器风速
		}
		double r_a = BaseDataUtil.stringConversionDouble(rfanMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)) > 10 ? 1.1
				: 4.56;
		double s_a = BaseDataUtil.stringConversionDouble(sfanMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)) > 10 ? 1.1
				: 4.56;
		double r_b = BaseDataUtil.stringConversionDouble(rfanMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)) > 10 ? 2.6
				: 10.5;
		double s_b = BaseDataUtil.stringConversionDouble(sfanMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)) > 10 ? 2.6
				: 10.5;
		double heatRecoveryEfficiency = BaseDataUtil
				.stringConversionDouble(BaseDataUtil.constraintString(heatRecoveryMap.get("meta.section.wheelHeatRecycle.HeatRecoveryEfficiency")).split("/")[0]);
		int time = 1;
		for (String key : table2.keySet()) {
			Map<String, Double> paraMap = table2.get(key);		
			//开始计算步骤：
			double r_DeltaPx = 0.0;
			double r_DeltaPy = 0.0;
			double r_DeltaPz = 0.0;
			double s_DeltaPx = 0.0;
			double s_DeltaPy = 0.0;
			double s_DeltaPz = 0.0;
			double Pairside_ref_extract;
			double Pairside_ref_supply;
			double fpe = step1();
			if (heatRecoveryMap.size() > 0) {
				r_DeltaPx = step2(r_ISP*1.1, r_systemEffect, BaseDataUtil.objectToDouble(paraMap.get("Velocity")), r_velocity,
						heatRecoveryMap);
				r_DeltaPy = step3(BaseDataUtil.objectToDouble(paraMap.get("Pclass")), heatRecoveryMap);
				r_DeltaPz = step4(fpe, BaseDataUtil.objectToDouble(paraMap.get("Efficiency")), heatRecoveryEfficiency,
						electricHeatingCoilMap, outsideTemp, heatRecoveryMap, rfanMap, 1, 50);
				
				s_DeltaPx = step2(s_ISP*1.1, s_systemEffect, BaseDataUtil.objectToDouble(paraMap.get("Velocity")), s_velocity,
						heatRecoveryMap);
				s_DeltaPy = step3(BaseDataUtil.objectToDouble(paraMap.get("Pclass")), heatRecoveryMap);
				s_DeltaPz = step4(fpe, BaseDataUtil.objectToDouble(paraMap.get("Efficiency")), heatRecoveryEfficiency,
						electricHeatingCoilMap, outsideTemp, heatRecoveryMap, null, 0, 50);
			} else {//无转轮时，只计算DeltaPx
				r_DeltaPx = step2(r_ISP*1.1, r_systemEffect, BaseDataUtil.objectToDouble(paraMap.get("Velocity")), r_velocity,
						heatRecoveryMap);
				s_DeltaPx = step2(s_ISP*1.1, s_systemEffect, BaseDataUtil.objectToDouble(paraMap.get("Velocity")), s_velocity,
						heatRecoveryMap);
			}
			Pairside_ref_extract = EmptyUtil.isNotEmpty(rfanMap)?step5(r_ISP*1.1, r_systemEffect, r_DeltaPx, r_DeltaPy, r_DeltaPz,
					(BaseDataUtil.objectToDouble(rfanMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)) / 3600.0), r_a,
					r_inputPower, r_b, BaseDataUtil.objectToDouble(paraMap.get("Ngref")), r_TSP):0f;
			Pairside_ref_supply = EmptyUtil.isNotEmpty(sfanMap)?step5(s_ISP*1.1, s_systemEffect, s_DeltaPx, s_DeltaPy, s_DeltaPz,
					(BaseDataUtil.objectToDouble(sfanMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)) / 3600.0), s_a,
					s_inputPower, s_b, BaseDataUtil.objectToDouble(paraMap.get("Ngref")), s_TSP):0f;
		
			double Fs_Pref = step6(s_inputPower, r_inputPower, Pairside_ref_supply, Pairside_ref_extract);
			if (Fs_Pref < 1) {
				resultList.add("EUROVENT Energy Efficiency Class " + key);
				break;
			}else if (time == 5) {
				resultList.add("EUROVENT Energy Efficiency Class " + "E");
			}
			time++;
		}
		
		if (winterTemperature < 9) {
			resultList.add("");
		} else if (winterTemperature >= 9) {
			resultList.add("N");
		} else if (!ifContainDouble(unit, partList)) {
			resultList.add("h");
		}
		return resultList;
	}
	
	private static void getOutsideTemp(Map<String, String> heatRecoveryMap, Map<String, String> mixMap,
			Map<String, String> steamHumidifierMap, Map<String, String> wetFilmHumidifierMap,
			Map<String, String> sprayHumidifierMap, Map<String, String> electrodeHumidifierMap,
			Map<String, String> coolingCoilMap, Map<String, String> heatingCoilMap,
			Map<String, String> combinedmixingChamberMap) {
		if (EmptyUtil.isNotEmpty(mixMap.get("meta.section.mix.SNewDryBulbT"))) {
			outsideTemp = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(mixMap.get("meta.section.mix.SNewDryBulbT")));
		} else if (EmptyUtil
				.isNotEmpty(combinedmixingChamberMap.get("meta.section.combinedMixingChamber.SNewDryBulbT"))) {
			outsideTemp = BaseDataUtil.stringConversionDouble(BaseDataUtil
					.constraintString(combinedmixingChamberMap.get("meta.section.combinedMixingChamber.SNewDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(steamHumidifierMap.get("meta.section.steamHumidifier.SInDryBulbT"))) {
			outsideTemp = BaseDataUtil.stringConversionDouble(
					BaseDataUtil.constraintString(steamHumidifierMap.get("meta.section.steamHumidifier.SInDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(wetFilmHumidifierMap.get("meta.section.wetfilmHumidifier.SInDryBulbT"))) {
			outsideTemp = BaseDataUtil.stringConversionDouble(BaseDataUtil
					.constraintString(wetFilmHumidifierMap.get("meta.section.wetfilmHumidifier.SInDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(electrodeHumidifierMap.get("meta.section.electrodeHumidifier.SInDryBulbT"))) {
			outsideTemp = BaseDataUtil.stringConversionDouble(BaseDataUtil
					.constraintString(electrodeHumidifierMap.get("meta.section.electrodeHumidifier.SInDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(sprayHumidifierMap.get("meta.section.sprayHumidifier.SInDryBulbT"))) {
			outsideTemp = BaseDataUtil.stringConversionDouble(
					BaseDataUtil.constraintString(sprayHumidifierMap.get("meta.section.sprayHumidifier.SInDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(heatRecoveryMap.get("meta.section.wheelHeatRecycle.SNewDryBulbT"))) {
			outsideTemp = BaseDataUtil.stringConversionDouble(
					BaseDataUtil.constraintString(heatRecoveryMap.get("meta.section.wheelHeatRecycle.SNewDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(coolingCoilMap.get("meta.section.coolingCoil.SAmbientDryBulbT"))) {
			outsideTemp = BaseDataUtil.stringConversionDouble(
					BaseDataUtil.constraintString(coolingCoilMap.get("meta.section.coolingCoil.SAmbientDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(heatingCoilMap.get("meta.section.heatingCoil.SAmbientDryBulbT"))) {
			outsideTemp = BaseDataUtil.stringConversionDouble(
					BaseDataUtil.constraintString(heatingCoilMap.get("meta.section.heatingCoil.SAmbientDryBulbT")));
		}
		
		winterTemperature = 10.0;
		if (EmptyUtil.isNotEmpty(mixMap.get("meta.section.mix.WNewDryBulbT"))
				&& "true".equals(String.valueOf(mixMap.get("meta.section.mix.EnableWinter")))) {
			winterTemperature = BaseDataUtil
					.stringConversionDouble(BaseDataUtil.constraintString(mixMap.get("meta.section.mix.WNewDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(steamHumidifierMap.get("meta.section.steamHumidifier.WInDryBulbT"))
				&& "true".equals(String.valueOf(steamHumidifierMap.get("meta.section.steamHumidifier.EnableWinter")))) {
			winterTemperature = BaseDataUtil.stringConversionDouble(
					BaseDataUtil.constraintString(steamHumidifierMap.get("meta.section.steamHumidifier.WInDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(wetFilmHumidifierMap.get("meta.section.wetfilmHumidifier.WInDryBulbT"))
				&& "true".equals(String.valueOf(wetFilmHumidifierMap.get("meta.section.wetfilmHumidifier.EnableWinter")))) {
			winterTemperature = BaseDataUtil.stringConversionDouble(BaseDataUtil
					.constraintString(wetFilmHumidifierMap.get("meta.section.wetfilmHumidifier.WInDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(electrodeHumidifierMap.get("meta.section.electrodeHumidifier.WInDryBulbT"))
				&& "true".equals(String.valueOf(electrodeHumidifierMap.get("meta.section.electrodeHumidifier.EnableWinter")))) {
			winterTemperature = BaseDataUtil.stringConversionDouble(BaseDataUtil
					.constraintString(electrodeHumidifierMap.get("meta.section.electrodeHumidifier.WInDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(sprayHumidifierMap.get("meta.section.sprayHumidifier.WInDryBulbT"))
				&& "true".equals(String.valueOf(sprayHumidifierMap.get("meta.section.sprayHumidifier.EnableWinter")))) {
			winterTemperature = BaseDataUtil.stringConversionDouble(
					BaseDataUtil.constraintString(sprayHumidifierMap.get("meta.section.sprayHumidifier.WInDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(heatRecoveryMap.get("meta.section.wheelHeatRecycle.WNewDryBulbT"))
				&& "true".equals(String.valueOf(heatRecoveryMap.get("meta.section.wheelHeatRecycle.EnableWinter")))) {
			winterTemperature = BaseDataUtil.stringConversionDouble(
					BaseDataUtil.constraintString(heatRecoveryMap.get("meta.section.wheelHeatRecycle.WNewDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(coolingCoilMap.get("meta.section.coolingCoil.WInDryBulbT"))
				&& "true".equals(String.valueOf(coolingCoilMap.get("meta.section.coolingCoil.EnableWinter")))) {
			winterTemperature = BaseDataUtil.stringConversionDouble(
					BaseDataUtil.constraintString(coolingCoilMap.get("meta.section.coolingCoil.WInDryBulbT")));
		} else if (EmptyUtil.isNotEmpty(heatingCoilMap.get("meta.section.heatingCoil.WInDryBulbT"))
				&& "true".equals(String.valueOf(heatingCoilMap.get("meta.section.heatingCoil.EnableWinter")))) {
			winterTemperature = BaseDataUtil.stringConversionDouble(
					BaseDataUtil.constraintString(heatingCoilMap.get("meta.section.heatingCoil.WInDryBulbT")));
		}
		
	}

	/**
	 * step1:(-0.0035*室外温度-0.79)*室外温度+8.1
	 * 
	 * @return
	 */
	private static double step1() {
		double fpe = 0;
		fpe = 8.1 - (0.0035 * outsideTemp + 0.79) * outsideTemp;
		return fpe;
	}

	/**
	 * step2:（internal static pressure - system effect - HRS pressure drop）x （ 1-
	 * （table2 风速/ 通过过滤器风速）^1.4）
	 * 
	 * @param ISP
	 * @param systemEffect
	 * @param table2Velocity
	 * @param velocity
	 * @param heatRecoveryMap
	 * @return
	 */
	private static double step2(double ISP, double systemEffect, double table2Velocity, double velocity,
			Map<String, String> heatRecoveryMap) {
		double DeltaPx = 0.0;
		DeltaPx = (ISP /*- systemEffect*/ - (((EmptyUtil.isNotEmpty(heatRecoveryMap))
				? (BaseDataUtil.stringConversionDouble(BaseDataUtil.constraintString(heatRecoveryMap.get("meta.section.wheelHeatRecycle.Resistance"))))
				: 0.0))) * (1.0 - Math.pow((table2Velocity / velocity), 1.4));
		return DeltaPx;
	}

	/**
	 * step3: HRS pressure drop - tabe2里的P class
	 * 
	 * @param table2PClass
	 * @param heatRecoveryMap
	 * @return
	 */
	private static double step3(double table2PClass, Map<String, String> heatRecoveryMap) {
		double DeltaPy = 0.0;
		DeltaPy = (EmptyUtil.isNotEmpty(heatRecoveryMap)
				? BaseDataUtil.stringConversionDouble(BaseDataUtil.constraintString(heatRecoveryMap.get("meta.section.wheelHeatRecycle.Resistance")))
				: 0.0) - table2PClass;
		return DeltaPy;
	}

	/**
	 * step4:（Table2效率 - 冬季转轮效率 + 5x 0 或1 ） x （1- 新风比/100） x f pe
	 * @param fpe
	 * @param table2HeatRecoveryEfficiency
	 * @param heatRecoveryEfficiency
	 * @param electricHeatingCoilMap
	 * @param outsideTemp
	 * @param heatRecoveryMap
	 * @param rfanMap
	 * @param action 送风-0 回风-1
	 * @return
	 */
	private static double step4(double fpe, double table2HeatRecoveryEfficiency, double heatRecoveryEfficiency,
			Map<String, String> electricHeatingCoilMap, double outsideTemp, Map<String, String> heatRecoveryMap, Map<String, String> rfanMap, int action, double mixingRatio) {
		double DeltaPz = 0.0;
		//公式
//		DeltaPz = (table2HeatRecoveryEfficiency - heatRecoveryEfficiency
//				+ (electricHeatingCoilMap.size() > 0 ? 1.0 : 0.0) * 5.0) * (1.0 - 50.0 / 100.0) * fpe;// 热回收新风量=回风量，新风比=50%
		
		// 能效校验excel版本
		if (action == 0) {// 送风
			DeltaPz = (((outsideTemp <= 9.0) ? table2HeatRecoveryEfficiency : 0.0)
					- ((heatRecoveryMap.size() > 0 ? 65.7 - ((electricHeatingCoilMap.size() > 0) ? 5.0 : 0.0)
							: 0.0)))
					* (1 - (mixingRatio < 85.0 ? mixingRatio / 100 : 0.85)) * fpe;
		} else if (action == 1) {// 回风
			if (!(rfanMap.size() > 0)) {
				return 0.0;
			} else {
				DeltaPz = (((outsideTemp <= 9.0) ? table2HeatRecoveryEfficiency : 0.0)
						- ((heatRecoveryMap.size() > 0 ? 65.7 - ((electricHeatingCoilMap.size() > 0) ? 5.0 : 0.0)
								: 0.0)))
						* (1 - (mixingRatio < 85.0 ? mixingRatio / 100 : 0.85)) * fpe;
			}
		}
		
		return DeltaPz;
	}

	/**
	 * step5:计算参考功率值
	 * @param ISP
	 * @param systemEffect
	 * @param DeltaPx
	 * @param DeltaPy
	 * @param DeltaPz
	 * @param airVolumeDB
	 * @param a
	 * @param inputPower
	 * @param b
	 * @param table2Ngref
	 * @param TSP
	 * @return
	 */
	private static double step5(double ISP, double systemEffect, double DeltaPx, double DeltaPy, double DeltaPz,
			double airVolumeDB, double a, double inputPower, double b, double table2Ngref, double TSP) {
		double Pairside_ref = 0.0;
		//公式版
		Pairside_ref = (Math.abs(ISP - systemEffect - (DeltaPx + DeltaPy + DeltaPz)) * airVolumeDB)
				/ (a * Math.log(inputPower) - b + table2Ngref);
		// 能效检查excel版
		double c = table2Ngref;
		double DP = ((TSP - (DeltaPx + DeltaPy + DeltaPz)) > 0) ? TSP - (DeltaPx + DeltaPy + DeltaPz) : 0.1;
		double qv = airVolumeDB;
		double Pref_i = 0.0;
		double Effic_iplus1 = 0.0;
		double Pref_iplus1 = 0.0;

		for (int i = 0; i < 5; i++) {
			if (i == 0) {
				Pref_i = DP * qv / (table2Ngref / 100.0);
				if ((Pref_i/1000)<=10) {
					a=4.56;
					b=10.5;
				}else if ((Pref_i/1000)>10) {
					a=1.1;
					b=2.6;
				}
				Effic_iplus1 = a * Math.log(Pref_i / 100.0) - b + c;
				Pref_iplus1 = DP * qv / (Effic_iplus1 / 100.0);
			}
			Pref_i = Pref_iplus1;
			Effic_iplus1 = a * Math.log(Pref_i / 100.0) - b + c;
			Pref_iplus1 = DP * qv / (Effic_iplus1 / 100.0);
		}
		Pairside_ref = Pref_iplus1 / 1000;
		return Pairside_ref;
	}

	/**
	 * 判断吸收功率因数
	 * 
	 * @param sup_inputPower
	 * @param ext_inputPower
	 * @param Psup_Ref
	 * @param Pext_Ref
	 * @return
	 */
	private static double step6(double sup_inputPower, double ext_inputPower, double Psup_Ref, double Pext_Ref) {
		double Fs_Pref = 0.0;
		Fs_Pref = (sup_inputPower + ext_inputPower) / (Psup_Ref + Pext_Ref);
		return Fs_Pref;
	}

	private static double calInputPower(Map<String, String> fanMap) {
		double inputPower = new Double(0);
		double shaftPowerDb = new Double(0);
		double airVolume = BaseDataUtil
				.stringConversionDouble(String.valueOf(fanMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
		double totalPressure = Double
				.parseDouble(String.valueOf(fanMap.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
		double efficiency = BaseDataUtil
				.stringConversionDouble(String.valueOf(fanMap.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
		shaftPowerDb = (airVolume * totalPressure * 100) / (3600 * efficiency * 1000);

		double motorSafetyFacor = new Double(0);
		double motorPower = BaseDataUtil
				.stringConversionDouble((String) fanMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
		if (motorPower < 0.5) {
			motorSafetyFacor = 1.5;
		} else if (motorPower > 0.5 && motorPower < 1) {
			motorSafetyFacor = 1.4;
		} else if (motorPower > 1 && motorPower < 2) {
			motorSafetyFacor = 1.3;
		} else if (motorPower > 2 && motorPower < 5) {
			motorSafetyFacor = 1.2;
		} else if (motorPower > 5) {
			motorSafetyFacor = 1.15;
		}
		double actualMotorPower = (shaftPowerDb * motorSafetyFacor) / (1);
		String motorEfficiencyStr = BaseDataUtil.constraintString(fanMap.get(UtilityConstant.METASEXON_FAN_MOTOREFF));
		NumberFormat nf = NumberFormat.getPercentInstance();
		Number motorEfficiencyNum;
		try {
			if (EmptyUtil.isNotEmpty(motorEfficiencyStr)) {
				motorEfficiencyNum = nf.parse(motorEfficiencyStr);
			} else {
				motorEfficiencyNum = 0;
			}

			double motorEfficiency = motorEfficiencyNum.doubleValue();
			if (motorEfficiency == 0) {
				motorEfficiency = 0.94;
			}
			inputPower = (motorPower) / (motorEfficiency);
			if (UtilityConstant.SYS_STRING_NUMBER_4.equals(fanMap.get(UtilityConstant.METASEXON_FAN_TYPE))) {// 双速电机输入功率从表s_twospeedmotor表中取
				int fanPole = BaseDataUtil.stringConversionInteger(fanMap.get(UtilityConstant.META_SECTION_FAN_POLE));
				double twoSpeedMotorPower = BaseDataUtil
						.stringConversionDouble(fanMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
				String motorType = fanMap.get(UtilityConstant.METASEXON_FAN_SUPPLIER);
				List<STwoSpeedMotor> sTwoSpeedMotorList = AhuMetadata.findAll(STwoSpeedMotor.class);
				for (STwoSpeedMotor sTwoSpeedMotor : sTwoSpeedMotorList) {
					if (sTwoSpeedMotor.getJs() == fanPole && sTwoSpeedMotor.getGl() == twoSpeedMotorPower && sTwoSpeedMotor.getSupplier().equalsIgnoreCase(motorType)) {
						// map.put(inputPower, sTwoSpeedMotor.getInputPower());
						//TODO 需要进行双电机逻辑的判断。。。。。。
						if(sTwoSpeedMotor.getInputPower().contains("/")){
							inputPower = BaseDataUtil.stringConversionDouble(sTwoSpeedMotor.getInputPower().split("/")[0]);
						}else{
							inputPower = BaseDataUtil.stringConversionDouble(sTwoSpeedMotor.getInputPower());
						}
						break;
					}
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return inputPower;
	}
	
	/**
	 * 判断是否存在热转轮
	 * @param unit
	 * @param partList
	 * @return
	 */
	private static boolean ifContainDouble(Unit unit, List<PartPO> partList){
		boolean isDouble = false;
		for (PartPO partPO : partList) {
			Part part = partPO.getCurrentPart();
			String key = part.getSectionKey();
			if ("ahu.wheelHeatRecycle".equals(key)) {
				isDouble = true;
				break;
			}
		}
		return isDouble;
	}
	/**
	 * 生成产品装箱单
	 * @param ahuMap
	 * @param allMap
	 * @param project
	 * @param unitType
	 * @param unit
	 * @param language
	 * @param connectionNum
	 * @param partList
	 * @return
	 * @throws IOException
	 */
	public static String[][] getProductPackingList(Map<String, Map<String, String>> ahuMap, Map<String, String> allMap, Project project, UnitSystemEnum unitType, Unit unit, LanguageEnum language, int connectionNum, List<PartPO> partList) throws IOException {
		String productPackingListName = SectionMetaUtils.getMetaAHUKey(KEY_PRODUCT_PACKING_LIST_NAME);
		String serial = SectionMetaUtils.getMetaAHUKey(KEY_SERIAL);
		allMap.put(productPackingListName,
				BaseDataUtil.constraintString(allMap.get(serial) + getIntlString(I18NConstants.PRODUCT_PACKING_LIST)));
		String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(
				AHUContext.isExportVersion() ? REPORT_TECH_PRODUCTPACKINGLIST_EXPORT : REPORT_TECH_PRODUCTPACKINGLIST),
				project, unit, allMap, language, unitType);

		int PTMNum=0;
		List<Part> parts=unit.getParts();
		Map<String,Integer> startStyleCount = new HashedMap<>();
		for(PartPO part:partList) {

			if(SectionTypeEnum.TYPE_FAN.getId().equals(part.getCurrentPart().getSectionKey())) {
				Map<String, String> fanMap= ahuMap.get(UnitConverter.genUnitKey(unit, part.getCurrentPart()));
				if(Boolean.valueOf(String.valueOf(fanMap.get(METASEXON_FAN_PTC)))) {
					PTMNum++;
				}

				/*启动柜：
					"VFD变频启动柜用户安装",
					"星-三角启动-用户安装",
					"直接启动-用户安装",
				如果风机选择了上面三个，这三个需要在“产品装箱清单”体现。*/
				String startStyle = String.valueOf(fanMap.get(METASEXON_FAN_STARTSTYLE));

				if(EmptyUtil.isNotEmpty(startStyle)
						&&(startStyle.equals(FAN_STARTSTYLE_VFD) || startStyle.equals(FAN_STARTSTYLE_STAR) || startStyle.equals(FAN_STARTSTYLE_DIRECT))){
					if(startStyleCount.containsKey(startStyle)){
						startStyleCount.put(startStyle,startStyleCount.get(startStyle)+1);
					}else{
						startStyleCount.put(startStyle,1);
					}
				}
			}
		}

		String paneltype = unit.getPaneltype();
		//最新版本段连接清单逻辑，如果进行自定义复选段连接类型，连接个数也重新获取，而不是从分段个数得到
		if(String.valueOf(paneltype).contains("[")){
			connectionNum=0;//重置段连接个数
			List<String> types = JSONArray.parseArray(paneltype, String.class);
			for (String type : types) {
				String[] tempType = type.split(":");
				int theCount = Integer.parseInt(tempType[1]);
				connectionNum+=theCount;
			}

		}

		if (!AHUContext.isExportVersion()) {//非出口版
			if(PTMNum==0) {
				for(int i=0;i<ahuContents[0].length;i++) {
					ahuContents[5][i] = UtilityConstant.SYS_BLANK;
				}
			}else {
				ahuContents[5][3]=String.valueOf(PTMNum);
			}

			if (connectionNum==0) {//没有分段的时候，去掉段连接信息
				for(int i=0;i<ahuContents[0].length;i++) {
					ahuContents[6][i] = UtilityConstant.SYS_BLANK;
				}
			}else if (PTMNum==0) {//无ptc的情况
				for(int i=0;i<ahuContents[6].length;i++) {//第7行数据移到第6行（段连接清单数据上移1行）
					ahuContents[5][i] = ahuContents[6][i];
					ahuContents[6][i] = UtilityConstant.SYS_BLANK;
				}
				ahuContents[5][0] = UtilityConstant.SYS_STRING_NUMBER_4;
				ahuContents[5][3]=String.valueOf(connectionNum);
			}else{
				ahuContents[6][3]=String.valueOf(connectionNum);
			}
		}else {//出口版不考虑ptc
			if (connectionNum==0) {//没有分段的时候，去掉段连接信息
				for(int i=0;i<ahuContents[0].length;i++) {
					ahuContents[5][i] = UtilityConstant.SYS_BLANK;
				}
			}else {
				ahuContents[5][3]=String.valueOf(connectionNum);
			}
		}

		//启动柜
		int startStyeIndex = 6;
		if(connectionNum == 0){
			startStyeIndex = 5;
		}
		Iterator<String> it = startStyleCount.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			int count = startStyleCount.get(key);
			ahuContents[startStyeIndex][0] = String.valueOf(startStyeIndex-1);
			ahuContents[startStyeIndex][1] = getIntlString(key+"_colon");
			ahuContents[startStyeIndex][2] = getIntlString("piece");
			ahuContents[startStyeIndex][3] = String.valueOf(count);
			startStyeIndex++;
		}
		return ahuContents;
	}
}

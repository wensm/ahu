package com.carrier.ahu.length;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.length.param.LengthParam;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.SFanLength2;
import com.carrier.ahu.metadata.entity.section.SectionLength;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Created by liangd4 on 2017/9/12.
 * 空段
 */
public class NullLen {

	//计算段长
	public double getLength(LengthParam lengthParam) {
		String function = lengthParam.getFunction();//功能选择
		boolean uvLamp = lengthParam.isUvLamp();//是否安装杀菌灯
		String oDoor = lengthParam.getODoor();//开门
		String serial = lengthParam.getSerial();//机组型号
		/*均流器,段长*/
		if(SystemCalculateConstants.ACCESS_FUNCTION_DIFFUSER.equals(function)){
			double diffuserLen = 6;
			SectionLength len = AhuMetadata.findOne(SectionLength.class, lengthParam.getSerial());
			diffuserLen = len.getDIF();
			return diffuserLen;
		}else{
			if(serial.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {
				if(SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(oDoor)){//开门无观察窗
					if(uvLamp) {//安装杀菌灯
						return 7.00;
					}
					return 6.00;
				}
				if(SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(oDoor)){//开门有观察窗
					if(uvLamp) {//安装杀菌灯
						return 7.00;
					}
					return 6.00;
				}
				return 6;
			}

			if("true".equals(lengthParam.getPositivePressureDoor())){
				SFanLength2 sFanLength = AhuMetadata.findOne(SFanLength2.class, serial, UtilityConstant.SYS_STRING_NUMBER_1,
						lengthParam.getFanModel());
				double retlength = 0.00;
				if (!EmptyUtil.isEmpty(sFanLength)) {
					retlength = (double)sFanLength.getInletLen();
				}
				if(uvLamp){
					if(retlength < 7){
						return 7;
					}
				}else{
					if(retlength < 6){
						return 6;
					}
				}
				return retlength;
			}

			/*后面是无蜗壳风机时，默认段长为7*/
			if (null != lengthParam.getNextPartKey()
					&& SectionTypeEnum.TYPE_FAN
					.equals(SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()))
					&& SystemCalculateConstants.FAN_OPTION_OUTLET_WWK.equals(lengthParam.getOutlet())) {
				SFanLength2 sfanLength2 = AhuMetadata.findOne(SFanLength2.class, lengthParam.getSerial(),CommonConstant.SYS_STRING_NUMBER_1, lengthParam.getFanModel());
				if (EmptyUtil.isNotEmpty(sfanLength2)) {
					return sfanLength2.getInletLen();
				} else {
					sfanLength2 = AhuMetadata.findOne(SFanLength2.class, lengthParam.getSerial());
					if (EmptyUtil.isNotEmpty(sfanLength2)) {
						double retlength = sfanLength2.getInletLen();
						if(uvLamp && retlength<6) {//安装杀菌灯
							return 6.00;
						}
						return retlength;
					}else {
						return 7.00;
					}
				}
			}
			return 6.00;
		}
	}

	/**
	 * 39CQ/39G
	 * 最小段长：	均流器最小：6
	 * 吸风段（空段+无蜗壳）
	 * 	1、安装杀菌灯从s_fan_length2.csv 小于6需要变成6（？yanglin 确认是7 还是6）；
	 * 	2、不安装杀菌灯从s_fan_length2.csv 读取
	 *
	 * 空段不开门最小：（1:安装杀菌灯2  2:不安装杀菌灯1）
	 * 空段开门无观察窗最小：（1:安装杀菌灯6  2:不安装杀菌灯4）
	 * 空段开门有观察窗最小：（1:安装杀菌灯5  2:不安装杀菌灯4）
	 * @param lengthParam
	 * @return
	 */
	public double getCalLength(LengthParam lengthParam) {
		boolean uvLamp = lengthParam.isUvLamp();//是否安装杀菌灯
		String oDoor = lengthParam.getODoor();//开门
		String function = lengthParam.getFunction();//功能选择
		String serial = lengthParam.getSerial();//机组型号
		if (serial.startsWith(UtilityConstant.SYS_UNIT_SERIES_39CQ)
				|| serial.startsWith(UtilityConstant.SYS_UNIT_SERIES_39G)) {
			/* 均流器,段长不可小于7M */
			if (SystemCalculateConstants.ACCESS_FUNCTION_DIFFUSER.equals(function)) {
				double retlength = 6.00;
//				SectionLength slength = AhuMetadata.findOne(SectionLength.class, serial);
//				if (!EmptyUtil.isEmpty(slength)) {
//					retlength = (double) slength.getDIF();
//				}
				return retlength;
			}else{
//                //CQ机组选中了正压门，则最小段长为6M，如果选了杀菌灯，最小段长为7m
//                if (null != lengthParam.getNextPartKey()
//                    && SectionTypeEnum.TYPE_FAN
//                    .equals(SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()))) {
				if("true".equals(lengthParam.getPositivePressureDoor())){
					SFanLength2 sFanLength = AhuMetadata.findOne(SFanLength2.class, serial, UtilityConstant.SYS_STRING_NUMBER_1,
							lengthParam.getFanModel());
					double retlength = 0.00;
					if (!EmptyUtil.isEmpty(sFanLength)) {
						retlength = (double)sFanLength.getInletLen();
					}
					if(uvLamp){
						if(retlength < 7){
							return 7;
						}
					}else{
						if(retlength < 6){
							return 6;
						}
					}
					return retlength;
				}
//                }

				//如果风机为无蜗壳风机，则和风机前面相连的空段的段长，从s_fan_length2表里面的InletLen字段获取
				if (null != lengthParam.getNextPartKey()
						&& SectionTypeEnum.TYPE_FAN
						.equals(SectionTypeEnum.getSectionTypeFromId(lengthParam.getNextPartKey()))
						&& SystemCalculateConstants.FAN_OPTION_OUTLET_WWK.equals(lengthParam.getOutlet())) {
					double retlength = 0.00;
					SFanLength2 sFanLength = AhuMetadata.findOne(SFanLength2.class, serial, UtilityConstant.SYS_STRING_NUMBER_1,
							lengthParam.getFanModel());
					if (!EmptyUtil.isEmpty(sFanLength)) {
						retlength = (double)sFanLength.getInletLen();
					}
					if(uvLamp && retlength<6) {//安装杀菌灯
						return 6.00;
					}
					return retlength;
				}

				if (SystemCalculateConstants.ACCESS_ODOOR_W_O_DOOR.equals(oDoor)) {//不开门
					if(uvLamp) {//安装杀菌灯
						return 2.00;
					}else{
						return 1.00;//不开门默认最小1
					}
				}

				if(SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(oDoor)){//开门无观察窗
					if(uvLamp) {//安装杀菌灯
						return 6.00;
					}else {
						if(serial.startsWith(UtilityConstant.SYS_UNIT_SERIES_39CQ)){
							return 5.00;//cq 最小都是5
						}
						return 4.00;//G 最小都是4
					}
				}
				if(SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(oDoor)){//开门有观察窗
					if(serial.startsWith(UtilityConstant.SYS_UNIT_SERIES_39CQ)){
						return 5.00;//cq 最小都是5
					}
					return 4.00;//G 最小都是4
				}

			}
		}else if(serial.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {
			if(SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(oDoor)){//开门无观察窗
				if(uvLamp) {//安装杀菌灯
					return 7.00;
				}
				return 6.00;
			}
			if(SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(oDoor)){//开门有观察窗
				if(uvLamp) {//安装杀菌灯
					return 7.00;
				}
				return 6.00;
			}
			if (SystemCalculateConstants.ACCESS_ODOOR_W_O_DOOR.equals(oDoor)) {//不开门
				if(uvLamp) {//安装杀菌灯
					return 2.00;
				}else{
					return 1.00;//不开门默认最小1
				}
			}
		}

		return 4.00;//开门默认最小4
	}
}

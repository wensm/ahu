package com.carrier.ahu.wpd;

import lombok.Data;

/**
 * Created by Braden Zhou on 2019/07/12.
 */

@Data
public class WaterDropDetail {

    private String model;
    private double t_od;
    private TubeDetail mainTube;
    private TubeDetail distributeTube;
    private CoilTubeDetail coilTube;
    private double w_flow;
    private CalcDetail headerCalc;
    private CalcDetail coilCalc;
    private double header_w_pd;
    private double coil_w_pd;
    private double total;
    private double waterTemp;
    private double density;

    public WaterDropDetail() {
        this.t_od = 0;
        this.mainTube = new TubeDetail();
        this.distributeTube = new TubeDetail();
        this.coilTube = new CoilTubeDetail();
        this.w_flow = 0;
        this.headerCalc = new CalcDetail();
        this.coilCalc = new CalcDetail();
        this.header_w_pd = 0;
        this.coil_w_pd = 0;
        this.total = 0;
        this.waterTemp = 0;
        this.density = 0;
        this.initialize();
    }

    // not able to find these two formula, so consider as constants
    private void initialize() {
        this.distributeTube.setT_len(90);
    }

    public void setArguments(String model, double t_od, int t_id, int cir_no, int t_len, int t_no, int row,
            double w_flow, double waterTemp) {
        this.model = model;
        this.t_od = t_od;
        this.mainTube.setT_id(t_id);
        this.coilTube.setCir_no(cir_no);
        this.coilTube.setT_len(t_len);
        this.coilTube.setT_no(t_no);
        this.coilTube.setRow(row);
        this.w_flow = w_flow;
        this.waterTemp = waterTemp;
    }

    public void calculateFormula() {
        // Main Tube: T_len
        double main_t_len = 0;
        if (t_od == 9.52) {
            main_t_len = 25.4 * this.coilTube.getT_no() + 2 * 160 - 61 * 2;
        } else if (t_od == 12.7) {
            main_t_len = 31.75 * this.coilTube.getT_no() + 2 * 160 - 61 * 2;
        }
        this.mainTube.setT_len(main_t_len);

        // Main Tube: W_Vel = Q7*4/E7/E7/PI()*1000*1000/3600
        double w_vel = w_flow * 4 / this.mainTube.getT_id() / this.mainTube.getT_id() / Math.PI * 1000 * 1000 / 3600;
        this.mainTube.setW_vel(w_vel);

        // density = -0.085866 * Y7 ^ 1.4802525 + 0.1655361 * Y7 ^ 1.1737852 + 999.81127
        double density = -0.085866 * Math.pow(waterTemp, 1.4802525) + 0.1655361 * Math.pow(waterTemp, 1.1737852)
                + 999.81127;
        this.density = density;

        Coeffs.NINE_DEGREE_VISCOSITY_COEFF = 0.01775 / (1 + 0.0337 * waterTemp + 0.000221 * Math.pow(waterTemp, 2))
                / 100 / 100;
    }

    static class Coeffs {

        static double NINE_DEGREE_VISCOSITY_COEFF; // 9度水温下水的运动粘滞系数 (m2/s)
        static double MAIN_TUBE_RE; // 主管段雷诺数 Re
        static final double MAIN_TUBE_RISE_HEIGHT = 0.046; // 主管突起高度 Δ (mm)
        static double MAIN_TUBE_RE_DELTA_D; // Re*Δ/d
        static double MAIN_TUBE_DROP_COEFF; // 主管段沿程阻力系数λ
        static double DISTRIBUTE_TUBE_RE; // 分配管段雷诺数 Re
        static final double DISTRIBUTE_TUBE_RISE_HEIGHT = 0.01; // 分配管突起高度 Δ (mm)
        static double DISTRIBUTE_TUBE_RE_DELTA_D; // Re*Δ/d
        static double DISTRIBUTE_TUBE_DROP_COEFF; // 分配管段沿程阻力系数λ
        static double COIL_TUBE_RE; // 盘管段雷诺数 Re
        static final double COIL_TUBE_RISE_HEIGHT = 0.01; // 盘管突起高度 Δ (mm)
        static double COIL_TUBE_RE_DELTA_D; // Re*Δ/d
        static double COIL_TUBE_DROP_COEFF; // 盘管段沿程阻力系数λ

        static void calculateFormula(WaterDropDetail detail) {
            // 9度水温下水的运动粘滞系数 = 0.01775/(1+0.0337*Y7+0.000221*Y7^2)/100/100
            NINE_DEGREE_VISCOSITY_COEFF = 0.01775
                    / (1 + 0.0337 * detail.waterTemp + 0.000221 * Math.pow(detail.waterTemp, 2)) / 100 / 100;

            // 分配管段雷诺数 = H7*I7/AA7/1000
            DISTRIBUTE_TUBE_RE = detail.distributeTube.getT_id() * detail.distributeTube.getW_vel()
                    / NINE_DEGREE_VISCOSITY_COEFF / 1000;

            // 分配管段沿程阻力系数 = 0.3164/AG7^0.25
            DISTRIBUTE_TUBE_DROP_COEFF = 0.3164 / Math.pow(DISTRIBUTE_TUBE_RE, 0.25);

            // 主管段雷诺数 = F7*E7/AA7/1000
            MAIN_TUBE_RE = detail.mainTube.getW_vel() * detail.mainTube.getT_id() / NINE_DEGREE_VISCOSITY_COEFF / 1000;

            // 主管段沿程阻力系数 = 0.1*(100/AB7)^0.25
            MAIN_TUBE_DROP_COEFF = 0.1 * Math.pow((100 / MAIN_TUBE_RE), 0.25);

            // 盘管段雷诺数 = O7*L7/AA7/1000
            COIL_TUBE_RE = detail.coilTube.getW_vel() * detail.coilTube.getT_id() / NINE_DEGREE_VISCOSITY_COEFF / 1000;

            // 盘管段沿程阻力系数 = IF(AL7>2000,4*(1/(1.7372*LN(AL7/(1.964*LN(AL7)-3.8215))))^2,0.078)
            if (COIL_TUBE_RE > 2000) {
                COIL_TUBE_DROP_COEFF = 4 * Math
                        .pow((1 / (1.7372 * Math.log(COIL_TUBE_RE / (1.964 * Math.log(COIL_TUBE_RE) - 3.8215)))), 2);
            } else {
                COIL_TUBE_DROP_COEFF = 0.078;
            }
        }
    }

}

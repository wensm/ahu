package com.carrier.ahu.constant;

public class UtilityConstant extends CommonConstant {
    /** 报告所有段通用key开头标识 **/
    public static final String COMMON_KEY_PREFIX = "common.key";
    public static final String COMMON_KEY_DOOR_DIMENSION = "common.key.door.dimension";// 开门尺寸

    public static final double PI = 3.14;

}
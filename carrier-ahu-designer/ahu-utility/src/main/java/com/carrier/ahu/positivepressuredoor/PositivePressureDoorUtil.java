package com.carrier.ahu.positivepressuredoor;

import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.DoorInstallHeight;
import com.carrier.ahu.metadata.entity.fan.MotorSize;
import com.carrier.ahu.metadata.entity.fan.WwkFanSize;
import com.carrier.ahu.metadata.entity.report.SPaneldXtDoor;
import com.carrier.ahu.param.PositivePressureDoorParam;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.NumberUtil;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.carrier.ahu.constant.CommonConstant.*;
import static com.carrier.ahu.constant.CommonConstant.SITUATION_S2;

/**
 * 正压门接口实现类
 * Created by gaok2 on 2020/10/21.
 *
 */

@Component
public class PositivePressureDoorUtil {
    /**
     * 获取安装正压门时，正压门是否需要更改大小，是否需要增加段长
     * @param serial
     * @param sectionMap
     * @return
     */
    public PositivePressureDoorParam getPositivePressureDoorSituation(String serial, Map sectionMap) {
        //参数初始化
        String sSituation = null;
        String fanModel = null;
        String fanType = null;
        SPaneldXtDoor sPaneldXtDoor = null;
        int boxW = 0;
        int doorW = 0;
        String motorPower = null;
        String pole = null;
        String supplier = null;
        String type = null;
        String motorBaseNo = null;
        MotorSize motorSize = null;
        int motorW = 0;
        double motorWInstallSize = 0;
        int doorFu = 0;
        int fanBaseW = 0;
        double baseToPanel = 0;
        PositivePressureDoorParam doorParam = new PositivePressureDoorParam();

        //获取参数，为计算安装正压门的各种情况做准备
        //获取风机类型：无蜗壳还是普通风机
        String fanOutlet = (String) sectionMap.get(METASEXON_FAN_OUTLET);//风机形式
        //获取是否安装正压门
        String positivePressureDoor = String.valueOf(sectionMap.get(METASEXON_FAN_POSITIVEPRESSUREDOOR));

        //如果是无蜗壳风机且选择安装正压门的话，才继续往下执行
        if(JSON_FAN_OUTLET_WWK.equals(fanOutlet) && "true".equals(positivePressureDoor)){
            //获取风机型号
            fanModel = (String) sectionMap.get(METASEXON_FAN_FANMODEL);
            //加工正压门型号，格式为39XT+serial后四位
            fanType = SYS_UNIT_SERIES_39XT + serial.substring(serial.length()-4);
            //计算箱体内宽
            boxW = Integer.parseInt(serial.substring(serial.length()-2)) * 100 - 10;
            //获取检修门尺寸对象
            sPaneldXtDoor = AhuMetadata.findOne(SPaneldXtDoor.class,fanType ,AHU_PART_TYPE);
            if(EmptyUtil.isNotEmpty(sPaneldXtDoor)){
                //获取门宽
                doorW = Integer.parseInt(sPaneldXtDoor.getDoorW());
            }
            //获取电机功率
            motorPower = (String) sectionMap.get(METASEXON_FAN_MOTORPOWER);
            //获取电机级数
            pole = (String) sectionMap.get(META_SECTION_FAN_POLE);
            //根据电机功率和电机级数确定基座号
            motorBaseNo = (String) sectionMap.get(METASEXON_FAN_MOTORBASENO);
            //获取电机品牌
            supplier = (String) sectionMap.get(METASEXON_FAN_SUPPLIER);
            //获取电机类型
            type = (String) sectionMap.get(METASEXON_FAN_TYPE);
            //根据基座号以及电机品牌，电机类型，确定电机宽度
            motorSize = AhuMetadata.findOne(MotorSize.class,motorBaseNo,supplier,type);
            motorW = motorSize.getMotorW();

            //计算电机宽度安装方向尺寸（0.5*箱体宽度W-0.5电机宽度Wm）
            motorWInstallSize = boxW/2 - motorW/2;
            //计算门幅(门宽Wd+50mm)
            doorFu = doorW + 50;

            //获取无蜗壳风机安装尺寸
            WwkFanSize wwkFanSize = AhuMetadata.findOne(WwkFanSize.class, fanModel);
            //获取检修门安装高度
            DoorInstallHeight doorInstallHeight = AhuMetadata.findOne(DoorInstallHeight.class, fanModel);
            int doorInstallH = doorInstallHeight.getDoorInstallH();
            //判断正压门情况，是否需要增加段长或者修改门尺寸
            if(motorWInstallSize > doorFu){//如果电机安装尺寸大于门幅，则继续判断是否需要修改门尺寸
                //获取风机底座宽度
                fanBaseW = wwkFanSize.getFanBaseW();
                //计算底座距离面板的距离
                baseToPanel = boxW/2 - fanBaseW/2;

                //判断正压门尺寸是否需要调整
                if(baseToPanel > doorFu){//如果底座与面板的距离大于门幅则不需要调整，直接返回S1
                    doorParam.setSituation(SITUATION_S1);
                    doorParam.setDoorH(Integer.parseInt(sPaneldXtDoor.getDoorH()));
                    doorParam.setDoorW(doorW);
                    doorParam.setDoor(sPaneldXtDoor.getDoor().split(SYS_PANEL_XT_X)[1].replace("M","") + "*" + sPaneldXtDoor.getDoor().split(SYS_PANEL_XT_X)[0].replace("M",""));
                    doorParam.setDoorInstallH(50);//不需要调整时门高默认为50
                }else{//否则需要调整正压门

                    //计算检修门距离底边的距离（机组型号的5，6两位*100-50-门高-90）
                    int doorToBase = Integer.parseInt(serial.substring(4,6))*100 - 140 - Integer.parseInt(sPaneldXtDoor.getDoorH());
                    //计算检修门需要调整的尺寸
                    int doorReduce = getRoundUpLeft2(doorInstallH - doorToBase);

                    //计算实际检修门高度
                    int actualDoorH = 0;
                    int doorHM = 0;
                    if(doorReduce > 0){
                        actualDoorH = Integer.parseInt(sPaneldXtDoor.getDoorH()) - doorReduce;
                        doorHM = NumberUtil.convertStringToDoubleInt(sPaneldXtDoor.getDoor().split(SYS_PANEL_XT_X)[0].replace("M","")) - doorReduce/100;
                    }else{
                        actualDoorH = Integer.parseInt(sPaneldXtDoor.getDoorH());
                        doorHM = NumberUtil.convertStringToDoubleInt(sPaneldXtDoor.getDoor().split(SYS_PANEL_XT_X)[0].replace("M",""));
                    }
                    doorParam.setDoorH(actualDoorH);
                    doorParam.setDoorW(doorW);
                    doorParam.setDoor(sPaneldXtDoor.getDoor().split(SYS_PANEL_XT_X)[1].replace("M","") + "*" + doorHM);
                    doorParam.setDoorInstallH(doorInstallH);
                    doorParam.setSituation(SITUATION_S2);
                }
            }else{//如果电机安装尺寸小于门幅，则需要增加段长
                //获取无蜗壳风机长度
                double fanL = wwkFanSize.getFanL();
                //获取电机直联安装长度
                int motorL = motorSize.getMotorL();
                //计算安装正压门需要段长（风机长度Lf+电机直联安装长度Ldm+门宽Wd+140）向上取整
                int installDoorL = getRoundUpLeft2((int)fanL + motorL +  doorW + 140)/100;
                doorParam.setSegL(installDoorL);
                doorParam.setDoorH(Integer.parseInt(sPaneldXtDoor.getDoorH()));
                doorParam.setDoorW(doorW);
                doorParam.setDoor(sPaneldXtDoor.getDoor().split(SYS_PANEL_XT_X)[1].replace("M","") + "*" + sPaneldXtDoor.getDoor().split(SYS_PANEL_XT_X)[0].replace("M",""));
                doorParam.setDoorInstallH(50);//不需要调整时门高默认为50
                doorParam.setSituation(SITUATION_S3);
            }
            return doorParam;
        }else{
            return null;
        }
    }

    /**
     * 朝着远离 0（零）的方向将数字进行向上舍入，小数点左边两位，对应excel中的roundUp函数
     * @param num
     * @return
     */
    private int getRoundUpLeft2(int num){
        String sNum = num + "";
        String firstNum = null;
        String resultNum = null;

        if(-100 <= num && num < 0){//如果是负数
            return -100;
        }else if(num <= 100  && num > 0){
            return 100;
        }else if(num == 0){
            return 0;
        }else if(num > 100){
            firstNum = Integer.parseInt(sNum.substring(sNum.length()-3,sNum.length()-2)) + 1+"";
            resultNum = sNum.substring(0,sNum.length()-3) + firstNum + "00";
            return Integer.parseInt(resultNum);
        }else if(num < -100){
            sNum = sNum.replace("-","");
            firstNum = Integer.parseInt(sNum.substring(sNum.length()-3,sNum.length()-2)) + 1+"";
            resultNum = "-" + sNum.substring(0,sNum.length()-3) + firstNum + "00";
            return Integer.parseInt(resultNum);
        }
        return 0;
    }
}

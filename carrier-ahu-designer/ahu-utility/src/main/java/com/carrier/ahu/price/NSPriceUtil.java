package com.carrier.ahu.price;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.price.NonstdHigh;
import com.carrier.ahu.metadata.entity.price.NonstdWet;
import com.carrier.ahu.util.EmptyUtil;

import java.util.List;

public class NSPriceUtil {
    /**
     * 获取湿膜价格单条记录
     * @param oldUnit
     * @param thinkness
     * @return
     */
    public NonstdWet getWetNsPrice(String oldUnit,String thinkness) {
        NonstdWet nsWet = AhuMetadata.findOne(NonstdWet.class, oldUnit,thinkness);
        return nsWet;
    }

    /**
     * 获取所有高压喷雾加湿段价格记录
     * @return
     */
    public List<NonstdHigh> getAllHighNsPrice() {
        List<NonstdHigh> nshighs = AhuMetadata.findList(NonstdHigh.class);
        return nshighs;
    }
}

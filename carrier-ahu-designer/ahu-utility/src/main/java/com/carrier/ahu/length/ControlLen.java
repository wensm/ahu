package com.carrier.ahu.length;

import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.section.SectionLength;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by liangd4 on 2017/9/12. 控制端
 */
public class ControlLen {

	public double getLength(String type) {
		// 从[功能段段长信息]表中查找，第Z列的数据
		SectionLength length = AhuMetadata.findOne(SectionLength.class, type);
		if (EmptyUtil.isNotEmpty(length)) {
			return length.getZ();
		}
		return 0.00;
	}

}

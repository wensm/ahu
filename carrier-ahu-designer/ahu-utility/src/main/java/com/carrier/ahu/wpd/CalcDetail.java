package com.carrier.ahu.wpd;

import lombok.Data;

/**
 * Created by Braden Zhou on 2019/07/12.
 */
@Data
public class CalcDetail {

    private double on_way;
    private double local;

    public CalcDetail() {
        this.on_way = 0;
        this.local = 0;
    }

}

package com.carrier.ahu.util.ahu;

import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.po.AhuLayout;
import com.google.gson.Gson;

/**
 * Ahu Layout Utility class.
 * 
 * Created by Braden Zhou on 2018/06/28.
 */
public final class AhuLayoutUtils {

    public static AhuLayout getInitAhuLayout(LayoutStyleEnum layoutStyle) {
        return getInitAhuLayout(layoutStyle, 0);
    }

    public static AhuLayout getInitAhuLayout(LayoutStyleEnum layoutStyle, int size) {
        AhuLayout ahuLayout = new AhuLayout();
        ahuLayout.setStyle(layoutStyle.style());
        ahuLayout.setLayoutData(layoutStyle.getInitLayoutData(size));
        return ahuLayout;
    }

    public static String getInitAhuLayoutJsonString(LayoutStyleEnum layoutStyle) {
        return getInitAhuLayoutJsonString(layoutStyle, 0);
    }

    public static String getInitAhuLayoutJsonString(LayoutStyleEnum layoutStyle, int size) {
        return new Gson().toJson(getInitAhuLayout(layoutStyle, size));
    }

    public static AhuLayout parse(String layoutJson) {
        return new Gson().fromJson(layoutJson, AhuLayout.class);
    }

    /**
     * Find a close group type for the unit.
     * 
     * @param unit
     * @return
     */
    public static GroupTypeEnum findGroupType(Unit unit) {
        if (unit != null) {
            AhuLayout ahuLayout = parse(unit.getLayoutJson());
            if (ahuLayout != null) { // if no layout after first AHU creation
                LayoutStyleEnum layoutStyle = LayoutStyleEnum.valueStyleOf(ahuLayout.getStyle());
                switch (layoutStyle) {
                case COMMON:
                    // other group types with same layout, just return basic for now
                    return GroupTypeEnum.TYPE_BASIC;
                case NEW_RETURN:
                    return GroupTypeEnum.TYPE_FULL_YEAR;
                case WHEEL:
                    return GroupTypeEnum.TYPE_RUNNER;
                case PLATE:
                    return GroupTypeEnum.TYPE_PLATE;
                case VERTICAL_UNIT_1:
                    return GroupTypeEnum.TYPE_VERTICAL_1;
                case VERTICAL_UNIT_2:
                    return GroupTypeEnum.TYPE_VERTICAL_2;
                case DOUBLE_RETURN_1:
                    return GroupTypeEnum.TYPE_DOUBLE_RETURN_1;
                case DOUBLE_RETURN_2:
                    return GroupTypeEnum.TYPE_DOUBLE_RETURN_2;
                case SIDE_BY_SIDE_RETURN_1:
                    return GroupTypeEnum.TYPE_SIDE_BY_SIDE_RETURN_1;
                case SIDE_BY_SIDE_RETURN_2:
                    return GroupTypeEnum.TYPE_SIDE_BY_SIDE_RETURN_2;
                }
            }
        }
        return GroupTypeEnum.TYPE_BASIC;
    }

}

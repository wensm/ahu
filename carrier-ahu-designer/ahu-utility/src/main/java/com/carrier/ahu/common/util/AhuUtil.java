package com.carrier.ahu.common.util;

import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39;
import static com.carrier.ahu.vo.SystemCalculateConstants.ALL_PRODUCT_TYPE;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;

import lombok.extern.slf4j.Slf4j;

/**
 * General util method of AHU related calculations
 *
 * Created by liujianfeng on 2017/9/20.
 */
@Slf4j
public class AhuUtil {

    public static final String COIL_SECTION_COUNT_AS_ONE_CHECK_UNIT_NO = "2532";

    /**
     * 根据UNIT型号，返回正确的机组高度和宽度
     * 
     * @param unit
     * @return TODO get height and width of AHU
     */
    public static int[] getHeightAndWidthOfAHU(Unit unit) {
        String unitNo = getUnitNo(unit.getSeries());
        if (EmptyUtil.isNotEmpty(unitNo)) {
            int height = Integer.parseInt(unitNo.substring(0, UtilityConstant.SYS_INT_HW_SIZE));
            int width = Integer.parseInt(unitNo.substring(UtilityConstant.SYS_INT_HW_SIZE));
            return new int[] { height, width };
        }
        return new int[] { UtilityConstant.SYS_INT_DEFAULT_HEIGHT, UtilityConstant.SYS_INT_DEFAULT_WIDTH };
    }
    public static int[] getHeightAndWidthOfAHU(String Series) {
        String unitNo = getUnitNo(Series);
        if (EmptyUtil.isNotEmpty(unitNo)) {
            int height = Integer.parseInt(unitNo.substring(0, UtilityConstant.SYS_INT_HW_SIZE));
            int width = Integer.parseInt(unitNo.substring(UtilityConstant.SYS_INT_HW_SIZE));
            return new int[] { height, width };
        }
        return new int[] { UtilityConstant.SYS_INT_DEFAULT_HEIGHT, UtilityConstant.SYS_INT_DEFAULT_WIDTH };
    }
    public static int getHeightOfAHU(String unitModel) {
        String unitNo = getUnitNo(unitModel);
        if (EmptyUtil.isNotEmpty(unitNo)) {
            return Integer.parseInt(unitNo.substring(0, UtilityConstant.SYS_INT_HW_SIZE));
        }
        return UtilityConstant.SYS_INT_DEFAULT_WIDTH;
    }
    public static int getWidthOfAHU(String unitModel) {
        String unitNo = getUnitNo(unitModel);
        if (EmptyUtil.isNotEmpty(unitNo)) {
            return Integer.parseInt(unitNo.substring(UtilityConstant.SYS_INT_HW_SIZE));
        }
        return UtilityConstant.SYS_INT_DEFAULT_WIDTH;
    }

    /**
     * 返回实际段长，单位毫米(mm)。
     * 
     * @param part
     * @return
     */
    public static int getRealSectionLength(Part part) {
        int sectionL = getSectionL(part);
        return sectionL * UtilityConstant.SYS_INT_MOLD_UNIT_SIZE;
    }

    /**
     * Return real size in mm.
     * 
     * @param moldSize
     * @return
     */
    public static int getRealSize(int moldSize) {
        return moldSize * UtilityConstant.SYS_INT_MOLD_UNIT_SIZE;
    }

    /**
     * Get mold size for width and length of AHU stuffs.
     * 
     * @param size
     * @return
     */
    public static int getMoldSize(int size) {
        if (size >= UtilityConstant.SYS_INT_MOLD_UNIT_SIZE) {
            size = size / UtilityConstant.SYS_INT_MOLD_UNIT_SIZE;
        }
        return size;
    }

    /**
     * 返回对应段的段长数据
     * 
     * @param part
     * @return TODO Implementation
     */
    public static int getSectionL(Part part) {
        String metaJson = part.getMetaJson();
        if (EmptyUtil.isNotEmpty(metaJson)) {
            Map<String, Object> map = (Map<String, Object>) JSONObject.parseObject(metaJson);
            return getSectionL(part.getSectionKey(), map);
        }
        return UtilityConstant.SYS_INT_DEFAULT_SECTION_LENGTH;
    }

    public static int getSectionL(String sectionKey, Map<String, Object> partMeta) {
        try {
            String sectionLKey = SectionMetaUtils.getMetaSectionKey(sectionKey, MetaKey.KEY_SECTIONL);
            Integer value = MapValueUtils.getIntegerValue(sectionLKey, partMeta);
            if (value != null) {
                return value;
            }
        } catch (Exception e) {
            log.error("failed to find section length from meta data");
        }

        return UtilityConstant.SYS_INT_DEFAULT_SECTION_LENGTH;
    }

    /**
     * 返回对应段的重量数据
     * 
     * @param part
     * @return TODO Implementation
     */
    public static double getWeight(Part part) {
        String metaJson = part.getMetaJson();
        String weightKey = SectionMetaUtils.getMetaSectionKey(part.getSectionKey(), MetaKey.KEY_WEIGHT);
        if (EmptyUtil.isNotEmpty(metaJson)) {
            Map<String, Object> map = (Map<String, Object>) JSONObject.parseObject(metaJson);
            try {
                return MapValueUtils.getDoubleValue(weightKey, map);
            } catch (Exception e) {
                return UtilityConstant.SYS_INT_DEFAULT_WEIGHT;
            }
        }
        return UtilityConstant.SYS_INT_DEFAULT_WEIGHT;
    }

    /**
     * 取得精简序列号<br>
     * 
     * 例子：39CQ1215 返回1215<br>
     * 例子：39G1215 返回1215<br>
     * 例子：G1215 返回1215<br>
     * 
     * @param serial
     * @return
     */
    public static String getUnitNo(String unitModel) {
        unitModel = reBuildUnitModel(unitModel);
        String unitNo = unitModel;
        if (unitNo.length() > UtilityConstant.SYS_INT_UNIT_NO_SIZE) {
            unitNo = unitNo.substring(unitNo.length() - UtilityConstant.SYS_INT_UNIT_NO_SIZE);
        }
        return unitNo;
    }

    /**
     * 机组变形宽高都是个位数出现变形后为：39CQ69 正确应该为：39CQ0609，此方法重构错误型号为正确型号
     * @param unitModel
     * @return
     */
    private static String reBuildUnitModel(String unitModel) {
        if(unitModel.startsWith(AHU_PRODUCT_39)){
            String ProductStr = "";
            String noProductStr = "";

            for (String s : ALL_PRODUCT_TYPE) {
                if(unitModel.contains(s)){
                    ProductStr = s;
                    noProductStr = unitModel.replace(s,"");
                    break;
                }
            }
            if(noProductStr.length()==2){//处理历史数据个位数变形后出现型号为两位数问题例如：39CQ69 正确应该为：39CQ0609
                return ProductStr + "0"+noProductStr.substring(0,1)+"0"+noProductStr.substring(1,2);
            }

        }
        return unitModel;
    }

    /**
     * Get unit model from serial number.<br>
     * 
     * 39CQ1215: 39CQ<br>
     * 39G1215: 39G<br>
     * G1215: G<br>
     * 
     * @param serial
     * @return
     */
    public static String getUnitSeries(String unitModel) {
        unitModel = reBuildUnitModel(unitModel);
        String unitSeries = unitModel;
        if (unitModel.length() > UtilityConstant.SYS_INT_UNIT_NO_SIZE) {
            unitSeries = unitModel.substring(0, unitModel.length() - UtilityConstant.SYS_INT_UNIT_NO_SIZE);
        }
        return unitSeries;
    }

    /**
     * 
     * @param airflowParts
     * @return
     */
    public static int getAhuSectionCount(String unitModel, List<Part> airflowParts) {
        int ahuSectionCount = 0;
        SectionTypeEnum lastSectionType = null;
        for (Part part : airflowParts) {
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(part.getSectionKey());
            if (lastSectionType != null) {
                // 冷、热水盘管段后的湿膜加湿段和高压喷雾加湿段不单独计算段数
                // 冷水+热水盘管段不单独计算
                if (((SectionTypeEnum.TYPE_COLD.equals(lastSectionType)
                        || SectionTypeEnum.TYPE_HEATINGCOIL.equals(lastSectionType))
                        && (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.equals(sectionType)
                                || SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.equals(sectionType)))) {
                    lastSectionType = sectionType;
                    continue;
                }
                if ((SectionTypeEnum.TYPE_COLD.equals(lastSectionType)
                        && SectionTypeEnum.TYPE_HEATINGCOIL.equals(sectionType))) {
                    String unitNo = getUnitNo(unitModel);
                    if (SystemCountUtil.ltBigUnit(Integer.parseInt(unitNo))) { // 2532以下机组按一个段计算
                        lastSectionType = sectionType;
                        continue;
                    }
                }
            }
            ahuSectionCount++;
            lastSectionType = sectionType;
        }
        return ahuSectionCount;
    }

    public static boolean isValidProduct(String product) {
        return Arrays.asList(ALL_PRODUCT_TYPE).contains(product);
    }

}

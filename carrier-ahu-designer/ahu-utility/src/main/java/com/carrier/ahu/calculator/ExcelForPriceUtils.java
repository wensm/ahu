package com.carrier.ahu.calculator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.carrier.ahu.ResourceConstances;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ExcelUtils;

public class ExcelForPriceUtils {
	public static final String DATANU_PATH = "price/datanu.xlsx";
	public static final String PRICE_PATH = "/com/carrier/ahu/service/res/price.xlsx";
	public static final String SPEC_PATH = "/com/carrier/ahu/service/res/spec.xlsx";
	/**
	 * 重量计算[附件]<br>
	 * 会参与到各个段的计算
	 */
	public static final String PRICESPEC_AHUCAL_01 = "0_1";
	/** 重量计算[分段] */
	public static final String PRICESPEC_AHUCAL_21 = "2_1";
	/**
	 * 重量计算[机组/段，作用于一个段]<br>
	 * <br>
	 * 同一个值，有多条spec，需要通过条件判断，当前spec是否生效
	 */
	public static final String PRICESPEC_AHUCAL_11 = "1_1";
	/**
	 * 重量计算[机组/段，作用于多种类型的段]<br>
	 * <br>
	 * ahucal的值为多个段的ID的组合，之后以机组型号为基础，其它属性的组合作为选择条件
	 */
	public static final String PRICESPEC_AHUCAL_12 = "1_2";
	/**
	 * 重量计算[机组/段，作用于段]<br>
	 * <br>
	 * 以机组型号为基础，其它属性组合作为选择条件
	 */
	public static final String PRICESPEC_AHUCAL_13 = "1_3";

	private static final String PRICE_FIX_KEY = "39CQ";

	/**
	 * 根据文件名读取excel文件
	 *
	 * @param filePath
	 * @return
	 */
	public static Map<String, Map<String, String>> read2map(String filePath, CalculatorModel priceModel) {
		InputStream is = null;
		Map<String, Map<String, String>> priceMap = new HashMap<String, Map<String, String>>();
		try {
			if (EmptyUtil.isEmpty(filePath)) {
				is = ExcelForPriceUtils.class.getClassLoader().getResourceAsStream(UtilityConstant.SYS_PATH_PRICE_DATA_PATH);
			} else {
				File file = new File(filePath);
				is = new FileInputStream(file);
			}

			/* 验证文件是否合法 */
			// if (!ExcelUtils.validateExcel(DATA_PATH)) {
			// return null;
			// }
			/* 判断文件的类型，是2003还是2007 */
			boolean isExcel2003 = true;
			if (ExcelUtils.isExcel2007(UtilityConstant.SYS_PATH_PRICE_DATA_PATH)) {
				isExcel2003 = false;
			}

			/* 调用本类提供的根据流读取的方法 */

			priceMap = read2map(is, isExcel2003, priceModel);
			// priceModel.setPriceMap(priceMap);
			is.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (EmptyUtil.isNotEmpty(is)) {
				try {
					is.close();
				} catch (IOException e) {
					is = null;
					e.printStackTrace();
				}
			}
		}
		/* 返回最后读取的结果 */
		return priceMap;

	}

	/**
	 * 根据流读取Excel文件
	 *
	 * @param inputStream
	 * @param isExcel2003
	 *            区分2003和2007版本
	 * @return
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 */
	private static Map<String, Map<String, String>> read2map(InputStream inputStream, boolean isExcel2003,
			CalculatorModel priceModel) throws IOException, EncryptedDocumentException, InvalidFormatException {
		/* 根据版本选择创建Workbook的方式 */
		Workbook wb = null;
		if (isExcel2003) {
			wb = ExcelUtils.openExcelByPOIFSFileSystem(inputStream);
		} else {
			wb = ExcelUtils.openExcelByFactory(inputStream);
		}
		Map<String, Map<String, String>> priceMap = read2map(wb, priceModel);
		return priceMap;
	}

	/**
	 * 读取数据
	 *
	 * @param wb
	 * @param priceModel
	 * @return
	 */
	private static Map<String, Map<String, String>> read2map(Workbook wb, CalculatorModel priceModel) {
		Map<String, Map<String, String>> priceMap = new HashMap<String, Map<String, String>>();

		int sheetCount = wb.getNumberOfSheets();
		for (int i = 0; i < sheetCount; i++) {
			/* 得到第一个shell */
			Sheet sheet = wb.getSheetAt(i);
			Row row0 = sheet.getRow(0);
			/* 得到Excel的行数 */
			int totalRows = sheet.getPhysicalNumberOfRows();
			/* 得到Excel的列数 */
			int totalCells = 0;
			if (totalRows >= 1 && null != row0) {
				totalCells = row0.getPhysicalNumberOfCells();
			}
			/* 获取该sheet的属性Key列表 */
			String sheetName = sheet.getSheetName();
			String cellKeyPrefix = getCellKeyPrefix(sheetName, priceModel);

			/* 循环Excel的行 */
			for (int r = 1; r < totalRows; r++) {
				Row row = sheet.getRow(r);
				if (EmptyUtil.isEmpty(row)) {
					continue;
				}
				String priceMapKey = row.getCell(0).getStringCellValue();
				priceMapKey = fixPriceKey(priceMapKey);
				Map<String, String> map = new HashMap<String, String>();
				/* 循环Excel的列 */
				for (int c = 1; c < totalCells; c++) {
					String cellKey = fixKey(row0.getCell(c).getStringCellValue(), cellKeyPrefix);
					Cell cell = row.getCell(c);
					String cellValue = UtilityConstant.SYS_BLANK;
					if (EmptyUtil.isNotEmpty(cell)) {
						cellValue = cell.getNumericCellValue() + UtilityConstant.SYS_BLANK;
					}
					map.put(cellKey, cellValue);
				}
				/* 保存第r行的第c列 */
				if (priceMap.containsKey(priceMapKey)) {
					Map<String, String> tempMap = priceMap.get(priceMapKey);
					tempMap.putAll(map);
					priceMap.put(priceMapKey, tempMap);
				} else {
					priceMap.put(priceMapKey, map);
				}
			}
		}
		return priceMap;
	}

	private static String getCellKeyPrefix(String sheetName, CalculatorModel priceModel) {
		Map<String, String> fileSpecTypeMap = priceModel.getFileSpecTypeMap();
		if (fileSpecTypeMap.containsKey(sheetName)) {
			if (PRICESPEC_AHUCAL_13.equals(fileSpecTypeMap.get(sheetName))) {
				return getPrefixName(sheetName);
			}
		}
		return null;
	}

	private static String fixKey(String key, String prefix) {
		if (EmptyUtil.isEmpty(prefix)) {
			return key;
		} else {
			return prefix + UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN + key;
		}
	}

	public static String fixPriceKey(String key) {
		if (EmptyUtil.isEmpty(key)) {
			return null;
		}
		try {
			Integer.parseInt(key.trim());
			return PRICE_FIX_KEY + key.trim();
		} catch (Exception q) {
			return key;
		}
	}

	public static String getPrefixName(String sheetName) {
		return sheetName.substring(sheetName.lastIndexOf(UtilityConstant.SYS_PUNCTUATION_DOT) + 1);
	}

	public static void main(String[] args) {
		CalculatorModel priceModel = new CalculatorModel();
		List<CalculatorSpec> ll = new ArrayList<>();
		CalculatorSpec sp = new CalculatorSpec();
		sp.setAhucal(PRICESPEC_AHUCAL_13);
		sp.setTab("A配件");
		ll.add(sp);
		priceModel.setSpec(ll);
		String jj = "C:\\Users\\jaaka\\Desktop\\data\\access.xlsx";
		System.out.println(ExcelForPriceUtils.read2map(jj, priceModel));
	}
}

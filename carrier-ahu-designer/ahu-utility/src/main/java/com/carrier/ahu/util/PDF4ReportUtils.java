package com.carrier.ahu.util;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.calculator.ExcelForPriceCodeUtils;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.*;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.engine.cad.CadUtils;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.ReportMetadata;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.model.calunit.UnitUtil;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.psychometric.PsyCalBean;
import com.carrier.ahu.psychometric.PsychometricDrawer;
import com.carrier.ahu.report.*;
import com.carrier.ahu.report.common.ReportConstants;
import com.carrier.ahu.report.content.ReportContent;
import com.carrier.ahu.report.pdf.PDFTableGenerator;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.FileNamesLoadInSystem;
import com.carrier.ahu.vo.SysConstants;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.*;
import com.lowagie.text.pdf.draw.LineSeparator;
import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.common.model.partition.AhuPartition.S_MKEY_METAID;
import static com.carrier.ahu.common.model.partition.AhuPartition.S_MKEY_METAJSON;
import static com.carrier.ahu.constant.CommonConstant.METAHU_AHU_INPUT_NAME;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_AIRDIRECTION;
import static com.carrier.ahu.report.common.ReportConstants.*;
import static com.carrier.ahu.util.DateUtil.HH_MM;
import static com.carrier.ahu.util.DateUtil.YYYY_MM_DD;

public class PDF4ReportUtils {
	protected static Logger logger = LoggerFactory.getLogger(PDF4ReportUtils.class);

	/** PDF类型 - 奥雅纳工程格式报告 */
	public static final String T_ARUPPF = "aruppf";
	/** PDF类型 - 柏诚工程格式报告 */
	public static final String T_BOCHENGPF = "bochengpf";
	/** PDF类型 - 技术说明(工程版) */
	public static final String T_TECHPROJ = "techprojPdf";
	/** PDF类型 - 技术说明(销售版) */
	public static final String T_TECHSALER = "techsaler";

	/** 字体大小 文档 大标题 */
	public final static Integer TITLE_BIGGER = 32;
	/** 字体大小 表格 大表头 */
	public final static Integer TABLE_TITLE_BIGGER = 15;
	/** 字体大小 表格 常规表头 */
	public final static Integer TABLE_TITLE_NOMAL = 10;
	/** 字体大小 常规内容 */
	public final static Integer CONTENT_NOMAL = 10;
	/** 字体大小 页眉 */
	public final static Integer HEADER_TITLE = 11;
	/** 字体大小 页眉 bold */
	public final static Integer HEADER_TITLE_BOLD = 12;
	/** 字体大小 页脚 */
	public final static Integer FOOTER_TITLE = 12;

//	/** 汉字字体 宋体 */
//	private static final String UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH = "asserts/font/simsun.ttf";
//	/** 汉字字体 微软雅黑 */
//	private static final String UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH = "asserts/font/msyh.ttf";

	/**
	 * 奥雅纳工程格式报告
	 * 
	 * @param file
	 * @param report
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void genPdf4Aruppf(File file, Report report, ReportData reportData, LanguageEnum language,
                                     UnitSystemEnum unitType) throws DocumentException, IOException {
        FileOutputStream fos = new FileOutputStream(file.getPath());
        Document document = new Document(PageSize.A3);// 文档
        PdfWriter writer = PdfWriter.getInstance(document, fos);// PDF文档
        writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
        writer.setTagged();
        writer.setViewerPreferences(PdfWriter.DisplayDocTitle);
        writer.createXmpMetadata();
        document.open();// 写入数据之前要打开文档

        addTitle(document, report.getName());
        List<ReportUnitContent> units = reportData2ReportUnitContent(reportData, T_ARUPPF, language, unitType);
        for (int i = 0; i < units.size(); i++) {
            ReportUnitContent u = units.get(i);
            addLine(document);
            addTableTitle(document, getIntlString(EQUIPMENT_DATA_TABLE) + u.getUnit().getDrawingNo());
            try {
                document.add(PDFTableGenerator.generate(u.getContent(), ReportConstants.REPORT_T_ARUPPF));
            } catch (Exception e) {
                logger.error("奥雅纳工程格式报告》报错", e);
            }
            if (i < units.size() - 1) {
                document.newPage();
            }
        }
        close(document, writer, fos);
    }

	/**
	 * 柏诚工程格式报告
	 * 
	 * @param file
	 * @param report
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void genPdf4Bochengpf(File file, Report report, ReportData reportData, LanguageEnum language,
                                        UnitSystemEnum unitType) throws DocumentException, IOException {
		FileOutputStream fos = new FileOutputStream(file.getPath());
		Document document = new Document(PageSize.A4);// 文档
		PdfWriter writer = PdfWriter.getInstance(document, fos);// PDF文档
		writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
		writer.setTagged();
		writer.setViewerPreferences(PdfWriter.DisplayDocTitle);
		writer.createXmpMetadata();


		//writer.setPageEvent(new TechHeader());// 页头黑色边框
		Phrase phrase = new Phrase();
		Date d = new Date();
        Chunk timeChunk = new Chunk(format("  Date: {0} Time: {1}", DateUtil.getDateTimeString(d, YYYY_MM_DD),
                DateUtil.getDateTimeString(d, HH_MM)), getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL));
        phrase.add(timeChunk);
		Chunk wenzi = new Chunk("          " + report.getName(),
				getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE_BOLD));
		phrase.add(wenzi);
		phrase.add(Chunk.NEWLINE);// 换行
		String[][] headerContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_HEADER), reportData.getProject(),
				null, null, language, unitType);
		for (int i = 0; i < headerContents.length; i++) {
			phrase.add(new Chunk(fixHeaderLine(headerContents[i]),
					getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL)));
		}
		phrase.add(Chunk.NEWLINE);// 换行
		HeaderFooter header = new HeaderFooter(phrase, false);
		header.setBorder(Rectangle.BOX); // 设置为没有边框
		header.setAlignment(Element.ALIGN_LEFT);
		document.setHeader(header);
		document.open();// 写入数据之前要打开文档

        int pageIndex = 0;
		for (Unit unit : reportData.getUnitList()) {
			if (0 != pageIndex) {// 当前机组遍历完毕后，重新打开一页，第一次遍历的时候无需开新一页
				document.newPage();
			}
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}
			Project project = reportData.getProject();
			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
			Map<String, String> allMap2 = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);
			allMap2.put(METAHU_AHU_INPUT_NAME, unit.getName());

			Map<String, Partition> partitionMap = reportData.getPartitionMap();
			Partition partition = null;
			String partitionJson =UtilityConstant.SYS_BLANK;
			List<AhuPartition> ahuPartitionList = new ArrayList<>();
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}
			if (EmptyUtil.isNotEmpty(partition)) {
				partitionJson = partition.getPartitionJson();
			}
			if (EmptyUtil.isNotEmpty(partitionJson)) {
				ahuPartitionList = JSONArray.parseArray(partitionJson, AhuPartition.class);
			}

			addBr(document);
			String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_SPEC), reportData.getProject(), unit,
					allMap2, language, unitType);
			Map<String, String> mixMap = new HashMap<String,String>();
			for (PartPO partPO : partList) {
				Part part = partPO.getCurrentPart();
				String key = part.getSectionKey();
				if ("ahu.mix".equals(key)) {
					mixMap = allMap.get(UnitConverter.genUnitKey(unit, part));
				}else {
					continue;
				}
			}
			ahuContents = SectionContentConvertUtils.getBCAhu(ahuContents, allMap.get(UnitConverter.UNIT), mixMap, null);
			document.add(PDFTableGenerator.generate(ahuContents, ReportConstants.REPORT_T_BOCHENG_SPEC));

			String groupCode = unit.getGroupCode();
			if (null == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
				logger.warn(MessageFormat.format("柏诚工程格式报告-AHU组编码信息不合法，报告忽略声称对应段信息 - unitNo：{0},GroupCode:{1}", new Object[] { unit.getUnitNo(), unit.getGroupCode() }));
			} else {
				int order = 1;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					ExcelForPriceCodeUtils.setPartitionOfPart(part, ahuPartitionList);
					String key = part.getSectionKey();
					addLine(document);
					Map<String, String> noChangeMap=allMap.get(UnitConverter.genUnitKey(unit, part));
	                Map<String, String> cloneMap=UnitUtil.clone(noChangeMap);
					PDFTableGen.addSectionBoCheng(document, key,String.valueOf(part.getPos()), order, noChangeMap, cloneMap, language, unitType, project, unit);
					order++;
				}
			}
			//增加柏诚报告下部内容
			
			String[][] ahu2Contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_AHU), reportData.getProject(), unit,
					allMap.get(UnitConverter.UNIT), language, unitType);
			for (PartPO partPO : partList) {
				Part part = partPO.getCurrentPart();
				String key = part.getSectionKey();
				if(key.contains(UtilityConstant.METASEXON_FAN)) {
//					ahu2Contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_AHU), reportData.getProject(), unit,
//							allMap.get(UnitConverter.genUnitKey(unit, part)), language, unitType);
//					ahu2Contents = SectionContentConvertUtils.getBCAhu2(ahu2Contents, allMap.get(UnitConverter.genUnitKey(unit, part)), null);
				}else {
					continue;
				}
			}
			// 封装过滤段特殊格式数据
			ahu2Contents = SectionContentConvertUtils.getBCAhu2(ahu2Contents, allMap.get(UnitConverter.UNIT), ahuPartitionList);
			document.add(PDFTableGenerator.generate(ahu2Contents, ReportConstants.REPORT_T_BOCHENG_AHU));
			
			document.newPage();
            addBr(document);
            addLine(document);
		}
		close(document, writer, fos);
	}

	/**
	 * 转化工程格式数据
	 * @param reportData
	 * @param pdfClassify
	 * @param language
	 * @param unitType
	 * @return
	 * @throws IOException
	 */
    public static List<ReportUnitContent> reportData2ReportUnitContent(ReportData reportData, String pdfClassify,
                                                                       LanguageEnum language, UnitSystemEnum unitType) throws IOException {
        List<ReportUnitContent> reportUnitContents = new ArrayList<>();

        Project project = reportData.getProject();

        for (Unit unit : reportData.getUnitList()) {
            List<PartPO> partList = new ArrayList<>();

            for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
                if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
                    partList.add(e.getValue());
                }
            }
            //获取所有段、ahu 的值
            Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
            Map<String, String> allMap2 = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);
            
            ReportUnitContent content = new ReportUnitContent();
            content.setUnit(unit);
            if (PDF4ReportUtils.T_ARUPPF.equals(pdfClassify)) {// 奥雅纳工程格式报告
                String[][] contents = ReportContent.getReportContent(REPORT_T_ARUPPF);
                
                //-----------------ahu数据 特殊处理start-----------------
                BigDecimal weightBd = new BigDecimal(BaseDataUtil.constraintString(unit.getWeight()));//四舍五入，保留两位小数
            	weightBd = weightBd.setScale(2, BigDecimal.ROUND_HALF_UP);
                allMap2.put(UtilityConstant.METAHU_WEIGHT1, String.valueOf(weightBd));//设备运作重量
                //最大尺寸(长*宽*高)
				Map<String, String> ahuMap = allMap.get(UnitConverter.UNIT);
				double totalL = 0;
				double sectionL = 0;
				double weight = 0;//临时存储重量
				double MaxWeight = 0;//最重段重量
				String MaxWtName= UtilityConstant.SYS_BLANK;//最重段名
				for(Map<String, String> entityMap : allMap.values()) {//循环遍历每个段，收集段长和重量数据
					for(String mapKey : entityMap.keySet()) {
						if (mapKey.contains(UtilityConstant.METACOMMON_POSTFIX_SECTIONL) && !mapKey.contains(UtilityConstant.SYS_UNIT_SPEC_SECTION_AHU)) {
							sectionL += BaseDataUtil.stringConversionDouble(String.valueOf(entityMap.get(mapKey)));
						}
						if (mapKey.contains(UtilityConstant.METACOMMON_POSTFIX_WEIGHT) && !mapKey.contains(UtilityConstant.SYS_UNIT_SPEC_SECTION_AHU)) {
							weight = BaseDataUtil.stringConversionDouble(String.valueOf(entityMap.get(mapKey)));
							if (weight>MaxWeight) {
								MaxWeight = weight;
								MaxWtName = mapKey;
							}
						}
					}
					totalL = sectionL*100+90;
				}
				BigDecimal sectionLBd = new BigDecimal(BaseDataUtil.constraintString(totalL));// 四舍五入，保留两位小数
				sectionLBd = sectionLBd.setScale(0, BigDecimal.ROUND_HALF_UP);
				
				String length = BaseDataUtil.constraintString(String.valueOf(sectionLBd));
				String width = BaseDataUtil.constraintString(ahuMap.get(UtilityConstant.METAHU_WIDTH));
				String height = BaseDataUtil.constraintString(ahuMap.get(UtilityConstant.METAHU_HEIGHT));
				String ahuSize = length + UtilityConstant.SYS_PUNCTUATION_STAR + width + UtilityConstant.SYS_PUNCTUATION_STAR + height;
				allMap2.put(UtilityConstant.METAHU_AHUSIZE, ahuSize);
				MaxWtName = UtilityConstant.SYS_UNIT_SPEC_SECTION_AHU + MaxWtName.substring(MaxWtName.indexOf(UtilityConstant.METACOMMON_POSTFIX_SECTION)+8, MaxWtName.indexOf(UtilityConstant.METACOMMON_POSTFIX_WEIGHT));
				for(SectionTypeEnum e : SectionTypeEnum.values()) {
					if (MaxWtName.equals(e.getId())) {
						MaxWtName = getIntlString(e.getCnName());
					}
				}
				//				getIntlString(type.getCnName());
				allMap2.put("meta.section.heaviestSection.name", MaxWtName);
				allMap2.put(METAHU_AHU_INPUT_NAME, unit.getName());
				BigDecimal MaxWeightBd = new BigDecimal(BaseDataUtil.constraintString(MaxWeight));//四舍五入，保留两位小数
				MaxWeightBd = MaxWeightBd.setScale(2, BigDecimal.ROUND_HALF_UP);
				allMap2.put("meta.section.heaviestSection.weight", String.valueOf(MaxWeightBd));
				//-----------------ahu数据 特殊处理end-----------------
                
                //送风、回风、冷水盘管、热水盘管 特殊处理
                for (PartPO partPO : partList) {
                    Part part = partPO.getCurrentPart();
                    String key = part.getSectionKey();
                    //风机段
                    if(SectionTypeEnum.TYPE_FAN.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                        Map<String, String> fanMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        String airDir = UtilityConstant.METASEXON_AIRDIRECTION;//风向
                        if (fanMap.containsKey(airDir)) {
                            String value = BaseDataUtil.constraintString(String.valueOf(fanMap.get(airDir)));
                            if (value.equals("S")) {//送风map 处理
                                for(int i=0;i<contents.length;i++){
                                    String[] contenti = contents[i];
                                    for(int j=0;j<contenti.length;j++){
                                        String contentj = contenti[j];
                                        if(contentj.indexOf(UtilityConstant.METASEXON_FAN_SFAN)!=-1){
                                            allMap2.put(contentj, String.valueOf(fanMap.get(contentj.replace(".sfan", ".fan"))));
                                        }
                                    }                                   
                                }
                            }
                            if (value.equals("R")) {//回风map 处理
                                for(int i=0;i<contents.length;i++){
                                    String[] contenti = contents[i];
                                    for(int j=0;j<contenti.length;j++){
                                        String contentj = contenti[j];
                                        if(contentj.indexOf(UtilityConstant.METASEXON_FAN_RFAN)!=-1){
                                            allMap2.put(contentj, String.valueOf(fanMap.get(contentj.replace(".rfan", ".fan"))));
                                        }
                                    }                                   
                                }
                            }
                        }
                    }
                    //冷水盘管
                    if(SectionTypeEnum.TYPE_COLD.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                        Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> coilMap=UnitUtil.clone(noChangedMap);
                        coilMap = SectionDataConvertUtils.getCoolingCoil(noChangedMap,coilMap, language);
                        allMap2.putAll(coilMap);
                    }
                    //热水盘管
                    if(SectionTypeEnum.TYPE_HEATINGCOIL.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                    	 Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                         Map<String, String> heatMap=UnitUtil.clone(noChangedMap);
                         
                        heatMap = SectionDataConvertUtils.getHeatingcoil(noChangedMap,heatMap, language);
                        allMap2.putAll(heatMap);
                    }
                    //综合过滤段
                    if(SectionTypeEnum.TYPE_COMPOSITE.equals(SectionTypeEnum.getSectionTypeFromId(key))){
                        
                        Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> combinedFilterMap=UnitUtil.clone(noChangedMap);
                        
                        combinedFilterMap = SectionDataConvertUtils.getCombinedFilter(noChangedMap,combinedFilterMap, language);
                        allMap2.putAll(combinedFilterMap);
                    }
					// 加湿
					Map<String, String> humidifiers = new HashMap<String, String>();
					if (SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.equals(SectionTypeEnum.getSectionTypeFromId(key))) {
						
						Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> wetFilmHumidifierMap=UnitUtil.clone(noChangedMap);
                        
						wetFilmHumidifierMap = SectionDataConvertUtils.getARUPPFWetFilmHumidifier(noChangedMap,wetFilmHumidifierMap,
								language);
						humidifiers.putAll(wetFilmHumidifierMap);
					} else if (SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.equals(SectionTypeEnum.getSectionTypeFromId(key))) {
						
						Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> sprayHumidifierMap=UnitUtil.clone(noChangedMap);
                        
						sprayHumidifierMap = SectionDataConvertUtils.getARUPPFSprayHumidifier(noChangedMap,sprayHumidifierMap,
								language);
						humidifiers.putAll(sprayHumidifierMap);
					} else if (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.equals(SectionTypeEnum.getSectionTypeFromId(key))) {
						Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> steamHumidifierMap=UnitUtil.clone(noChangedMap);
                        
						steamHumidifierMap = SectionDataConvertUtils.getARUPPFSteamHumidifier(noChangedMap,steamHumidifierMap,
								language);
						humidifiers.putAll(steamHumidifierMap);
					} else if (SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER
							.equals(SectionTypeEnum.getSectionTypeFromId(key))) {
						
						Map<String, String> noChangedMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                        Map<String, String> electrodeHumidifierMap=UnitUtil.clone(noChangedMap);
                        
						electrodeHumidifierMap = SectionDataConvertUtils
								.getARUPPFElectrodeHumidifier(noChangedMap,electrodeHumidifierMap, language);
						humidifiers.putAll(electrodeHumidifierMap);
					}
					allMap2.putAll(humidifiers);
                }
                
                
                content.setContent(
                        ValueFormatUtil.transReport(contents, project, unit, allMap2, language, unitType));
            } else if (PDF4ReportUtils.T_BOCHENGPF.equals(pdfClassify)) {// 柏诚工程格式报告
                String[][] contents = ReportContent.getReportContent(REPORT_T_BOCHENG_SPEC);
                content.setContent(
                        ValueFormatUtil.transReport(contents, project, unit, allMap2, language, unitType));
            } else if (PDF4ReportUtils.T_TECHPROJ.equals(pdfClassify)) {// 技术说明(工程版)
            } else if (PDF4ReportUtils.T_TECHSALER.equals(pdfClassify)) {// 技术说明(销售版)
            } else {
                logger.warn("Failed to generate pdf for te reason : Classify is undefined !  >>PDFClassify : "
                        + pdfClassify);
            }
            reportUnitContents.add(content);
        }

        return reportUnitContents;
    }

	/**
	 * 技术说明(工程版)
	 * 
	 * @param file
	 * @param report
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void genPdf4Techproj(File file, Report report, ReportData reportData, LanguageEnum language,
                                       UnitSystemEnum unitType) throws DocumentException, IOException {
		FileOutputStream fos = new FileOutputStream(file.getPath());
		Document document = new Document(PageSize.A4);// 文档
		PdfWriter writer = PdfWriter.getInstance(document, fos);// PDF文档
		writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
		writer.setTagged();
		writer.setViewerPreferences(PdfWriter.DisplayDocTitle);
		writer.createXmpMetadata();

        writer.setPageEvent(new TechHeader(getIntlString(CARRIER_CORPORATION_FULL_NAME),
                getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL)));// 页头黑色边框

        Date d = new Date();
		// setFooter(document, "Page ");
		// addTitle(document, report.getName());
		Project project = reportData.getProject();
		Map<String, Partition> partitionMap = reportData.getPartitionMap();
		List<BookMark> bookMark1 = new ArrayList<BookMark>();//第一层

		if(!report.isOutput()){
			report.setHasOutputItems(false);//没有打印ahu基础信息（第一次打印items 不加入newpage，第一次之后items项加入newpage）
		}else{
			report.setHasOutputItems(true);//打印ahu基础信息，正常newpage展示风机曲线
		}
		// 添加机组信息
		int pageIndex = 0;
        for (Unit unit : reportData.getUnitList()) {
			Partition partition = null;
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}
			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
			Map<String, String> allMap2 = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);
			List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);

			Phrase phrase = new Phrase();
			// Eurovent update begin
            String euroLogo = "";
			if (!SystemCountUtil.isEuroUnit(unit)) {//非欧标部分
	            Chunk timeChunk = new Chunk(
	                    format("  Date: {0} Time: {1}", DateUtil.getDateTimeString(d, YYYY_MM_DD),
	                            DateUtil.getDateTimeString(d, HH_MM)),
	                    getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL));
	            phrase.add(timeChunk);
	            Chunk wenzi = new Chunk("          " + getIntlString(TECHNICAL_SPECIFICATION),
	                    getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE_BOLD));
	            phrase.add(wenzi);
			}else {
				String series = unit.getSeries();	
				if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39CQ) ||series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39G)) {// 39CQ 39G同一家认证体系
//					String[][] headerEuroContents = ValueFormatUtil.transReport(
//							ReportContent.getReportContent(REPORT_TECH_HEADER_EURO_39CQ), reportData.getProject(), unit,
//							null, language, unitType);
//					headerEuroContents = SectionContentConvertUtils.getHeaderEuro(headerEuroContents, unit, language,
//							allMap, partList, d);
//					for (int i = 0; i < headerEuroContents.length; i++) {
//						if (i==1) {
//							phrase.add(new Chunk(fixHeaderLine(headerEuroContents[i]),
//									getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL)));
//							
//						}else {
//							phrase.add(new Chunk(fixHeaderLine(headerEuroContents[i]),
//									getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, 11)));						
//						}
//					}
					euroHeader(phrase, unit, allMap, partList, d);//添加欧标头部内容
//					phrase.add(Chunk.NEWLINE);// 换行
					Chunk wenzi = new Chunk(
							"                                 " + getIntlString(TECHNICAL_SPECIFICATION),
							getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE_BOLD));
					phrase.add(wenzi);
					euroLogo = "Label_ECC_39CQ_ch.png";
				} else if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {// 39XT
//					String[][] headerEuroContents = ValueFormatUtil.transReport(
//							ReportContent.getReportContent(REPORT_TECH_HEADER_EURO_39XT), reportData.getProject(), unit,
//							null, language, unitType);
//					headerEuroContents = SectionContentConvertUtils.getHeaderEuro(headerEuroContents, unit, language,
//							allMap, partList, d);
//					for (int i = 0; i < headerEuroContents.length; i++) {
//						if (i==1) {
//							phrase.add(new Chunk(fixHeaderLine(headerEuroContents[i]),
//									getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL)));
//							
//						}else {
//							phrase.add(new Chunk(fixHeaderLine(headerEuroContents[i]),
//									getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, 11)));						
//						}
//					}
					euroHeader(phrase, unit, allMap, partList, d);//添加欧标头部内容
//					phrase.add(Chunk.NEWLINE);// 换行
					Chunk wenzi = new Chunk(
							"                                 " + getIntlString(TECHNICAL_SPECIFICATION),
							getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE_BOLD));
					phrase.add(wenzi);
					euroLogo = "Label_ECC_39XT_ch.png";
				}
			}
			

            String[][] headerContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEADER),
                    reportData.getProject(), unit, null, language, unitType);
            headerContents = SectionContentConvertUtils.getHeader(headerContents, unit, language);
            for (int i = 0; i < headerContents.length; i++) {
                phrase.add(new Chunk(fixHeaderLine(headerContents[i]),
                        getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL)));
            }
			AhuParam ahuParam = ValueFormatUtil.transDBData2AhuParam(project, unit, partList, partition);

            HeaderFooter header = new HeaderFooter(phrase, false);
//            header.setBorder(Rectangle.NO_BORDER); // 设置为没有边框
//            header.setAlignment(Element.ALIGN_LEFT);
            header.setBorder(Rectangle.BOX); 
            header.setAlignment(Element.ALIGN_LEFT); 
            header.setBorderColor(Color.black);
            document.setHeader(header);

			if (!document.isOpen()) {
				document.open();// 写入数据之前要打开文档
			}
			//1. AHU 信息打印
            BookMark bookMark1Temp = new BookMark();//第一层子项
            List<BookMark> bookMark2 = new ArrayList<BookMark>();//第二层
            List<BookMark> bookMark3 = new ArrayList<BookMark>();//第三层
            if(report.isOutput()) {
            	if (SystemCountUtil.isEuroUnit(unit)) {
            		if (0 == pageIndex) {// 第一个机组打印logo图
    					addImage(document, euroLogo, 7.3f, 36, 60, ifContainFan(unit, partList) ? 18 : 0);
    					addImageLogo(document, language, unit);
    					if (!ifContainFan(unit, partList)) {// 若没有风机进行提示
    						Paragraph noFanLine = new Paragraph();
    						noFanLine.add(Chunk.NEWLINE);// 换行
    						noFanLine.add(Chunk.NEWLINE);// 换行
    						noFanLine.add(new Chunk(getIntlString(ECP_NO_FAN),
    								getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, 8)));
    						document.add(noFanLine);
    					}
    					addLine(document);
    				} else {// 当前机组遍历完毕后，重新打开一页，第一次遍历的时候无需开新一页
    					document.newPage();
    					addBr(document);
    					addImage(document, euroLogo, 7.3f, 36, 60, ifContainFan(unit, partList) ? 18 : 0);
    					addImageLogo(document, language, unit);
    					if (!ifContainFan(unit, partList)) {// 若没有风机进行提示
    						Paragraph noFanLine = new Paragraph();
    						noFanLine.add(Chunk.NEWLINE);// 换行
    						noFanLine.add(Chunk.NEWLINE);// 换行
    						noFanLine.add(new Chunk(getIntlString(ECP_NO_FAN),
    								getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, 8)));
    						document.add(noFanLine);
    					}
    					addLine(document);
    				}
				}else {
					if (0 != pageIndex) {// 当前机组遍历完毕后，重新打开一页，第一次遍历的时候无需开新一页
						document.newPage();
					}
					if (!document.isOpen()) {
		                document.open();// 写入数据之前要打开文档
		                addImageLogo(document, language, unit);
		                addLine(document);
		            }else {
		            	addImageLogo(document, language, unit);
		                addLine(document);
					}

				}
				
			// Eurovent update end
				pageIndex++;
				String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_AHU_OVERALL), reportData.getProject(), unit,
						allMap.get(UnitConverter.UNIT), language, unitType);
				ahuContents = SectionContentConvertUtils.getAhuOverall(ahuContents, partition, unit, language, allMap.get(UnitConverter.UNIT));
				document.add(PDFTableGenerator.generate(ahuContents, ReportConstants.REPORT_TECH_AHU_OVERALL));

				//记录书签页码和客户PO号
                bookMark1Temp.setTitle(EmptyUtil.isNotEmpty(unit.getCustomerName())?unit.getCustomerName():unit.getName());
                bookMark1Temp.setNum(writer.getPageNumber());
                bookMark1Temp.setLevel(1);

                //添加ahu信息书签：起始页
                int bookMark3Temp_ahu_start = writer.getPageNumber();

				//TODO把這段代碼提取到共通方法中，和Word版本的報告公用

				if (EmptyUtil.isNotEmpty(partition)) {
//				String[][] titleContents = arrayMap.get("TechAhu2");
					String[][] ahuboxContents = SectionContentConvertUtils.getAhu(partition, unit, language,
							allMap.get(UnitConverter.UNIT));
					String[][] ahu2Contents = ValueFormatUtil.transReport(ahuboxContents, reportData.getProject(),
							unit, allMap.get(UnitConverter.UNIT), language, unitType);
					document.add(PDFTableGenerator.generate(ahu2Contents, ReportConstants.REPORT_TECH_AHU_DELIVERY));
				}

				//添加ahu非标
				PDFTableGen.addCommonF(document, allMap.get(UnitConverter.UNIT), language, unitType, project, unit, SectionTypeEnum.TYPE_AHU);
				String enable = String.valueOf(allMap.get(UnitConverter.UNIT).get(UtilityConstant.METANS_NS_AHU_ENABLE));//ahu是否非标
				if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
					String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_AHU_NONSTANDARD), project, unit, allMap.get(UnitConverter.UNIT), language,
							unitType);
					nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_AHU.getId()).getCnName(), language) + nsAHUcontents[0][0];//替换段名称
					nsAHUcontents = SectionContentConvertUtils.getAhuNS(nsAHUcontents, partition, unit, language, allMap.get(UnitConverter.UNIT));
					if (EmptyUtil.isNotEmpty(nsAHUcontents)) {
						document.add(PDFTableGenerator.generate(nsAHUcontents, ReportConstants.REPORT_NS_AHU_NONSTANDARD));
					}
				}

				Paragraph paragraph = getChineseParagraph(getIntlString(UNIT_INCLUDE_BASED_ON_AIR_FLOW_DIRECTION), CONTENT_NOMAL);
				paragraph.setAlignment(Element.ALIGN_LEFT);
				document.add(paragraph);

				//添加ahu信息书签
                BookMark bookMark3Temp_ahu = new BookMark();//第三层子项
                int bookMark3Temp_ahu_end = writer.getPageNumber();
                bookMark3Temp_ahu.setNum(bookMark3Temp_ahu_start);
                bookMark3Temp_ahu.setLevel(3);
                bookMark3Temp_ahu.setTitle("机组信息:"+bookMark3Temp_ahu_start+"-"+bookMark3Temp_ahu_end);
                bookMark3.add(bookMark3Temp_ahu);

				String groupCode = unit.getGroupCode();
				if (null == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
					logger.warn(MessageFormat.format("技术说明报告-AHU组编码信息不合法，报告忽略声称对应段信息 - unitNo：{0},GroupCode:{1}", new Object[]{unit.getUnitNo(), unit.getGroupCode()}));
				} else {
                    document.newPage();//段信息头、新建页面供pdf 工具完整切割分段使用
					int order = 1;
					List<PartPO> orderedPartList = AhuPartitionUtils.getAirflowOrderedPartPOListForReport(unit, partList);
					String partIndex = "1";
                    int partIndexOrder = 1;
                    int bookMark3Temp_partition_temp_start = writer.getPageNumber();
					for (PartPO partPO : orderedPartList) {
						Part part = partPO.getCurrentPart();
						ExcelForPriceCodeUtils.setPartitionOfPart(part, ahuPartitionList);
						String key = part.getSectionKey();

                        if(!partIndex.equals(String.valueOf(part.getPos()))){

                            //添加第二层分段信息书签
                            BookMark bookMark2Temp_partition_temp = new BookMark();//第二层子项
                            bookMark2Temp_partition_temp.setNum(bookMark3Temp_partition_temp_start);
                            bookMark2Temp_partition_temp.setLevel(2);
                            bookMark2Temp_partition_temp.setTitle(String.valueOf(partIndexOrder));
                            bookMark2.add(bookMark2Temp_partition_temp);


                            //添加第三层每个分段信息书签
                            int bookMark3Temp_partition_temp_end = writer.getPageNumber();
                            BookMark bookMark3Temp_partition_temp = new BookMark();
                            bookMark3Temp_partition_temp.setNum(bookMark3Temp_partition_temp_start);
                            bookMark3Temp_partition_temp.setLevel(3);
                            bookMark3Temp_partition_temp.setTitle("分段"+String.valueOf(partIndexOrder)+":"+bookMark3Temp_partition_temp_start+"-"+bookMark3Temp_partition_temp_end);
                            bookMark3.add(bookMark3Temp_partition_temp);
                            partIndexOrder++;

                            document.newPage();//分段结尾的段、新建页面供pdf 工具完整切割分段使用
                        }
						addLine(document);
						Map<String, String> noChangeMap = allMap.get(UnitConverter.genUnitKey(unit, part));
						Map<String, String> cloneMap = UnitUtil.clone(noChangeMap);
						PDFTableGen.addSection(document, key,String.valueOf(part.getPos()), order, noChangeMap, cloneMap,
								language, unitType, project, unit,ahuParam);
                        if(!partIndex.equals(String.valueOf(part.getPos()))){

                            partIndex = String.valueOf(part.getPos());
                            bookMark3Temp_partition_temp_start = writer.getPageNumber();
                        }
						order++;
					}

					if(partIndexOrder == ahuPartitionList.size()){
                        //添加第二层分段信息书签
                        BookMark bookMark2Temp_partition_temp = new BookMark();//第二层子项
                        bookMark2Temp_partition_temp.setNum(bookMark3Temp_partition_temp_start);
                        bookMark2Temp_partition_temp.setLevel(2);
                        bookMark2Temp_partition_temp.setTitle(String.valueOf(partIndexOrder));
                        bookMark2.add(bookMark2Temp_partition_temp);


                        //添加第三层每个分段信息书签
                        int bookMark3Temp_partition_temp_end = writer.getPageNumber();
                        BookMark bookMark3Temp_partition_temp = new BookMark();
                        bookMark3Temp_partition_temp.setNum(bookMark3Temp_partition_temp_start);
                        bookMark3Temp_partition_temp.setLevel(3);
                        bookMark3Temp_partition_temp.setTitle("分段"+String.valueOf(partIndexOrder)+":"+bookMark3Temp_partition_temp_start+"-"+bookMark3Temp_partition_temp_end);
                        bookMark3.add(bookMark3Temp_partition_temp);
				}
			}
			}
			//2. 工程报告项打印
			for (ReportItem item : report.getItems()) {
				if (!item.isOutput()) {
					continue;
				}

				if (ReportConstants.CLASSIFY_TVIEW.equals(item.getClassify())) {
					try {
                        document.newPage();

                        BookMark bookMark3Temp_3View = new BookMark();//第三层子项
                        int bookMark3Temp_3View_start = writer.getPageNumber();

						addImage3View(document, ahuParam, language);

                        int bookMark3Temp_3View_end = writer.getPageNumber();
                        bookMark3Temp_3View.setNum(bookMark3Temp_3View_start);
                        bookMark3Temp_3View.setLevel(3);
                        bookMark3Temp_3View.setTitle("三视图:"+bookMark3Temp_3View_start+"-"+bookMark3Temp_3View_end);
                        bookMark3.add(bookMark3Temp_3View);

					} catch (Exception e) {
						logger.error("工程版报告》添加三视图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_HANSHI.equals(item.getClassify())) {
					try {
                        document.newPage();
						addImagePsychometric(document, ahuParam,
								language);
					} catch (Exception e) {
						logger.error("工程版报告》添加焓湿图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_FAN.equals(item.getClassify())) {
					try {
						AhuParam param = ahuParam;
						List<PartParam> partParams = param.getPartParams();
						int fanCount = 0;
						for (PartParam partParam : partParams) {
							if (SectionTypeEnum.TYPE_FAN.getId().equals(partParam.getKey())) {
								fanCount++;
							}
						}

						if(fanCount>0){
								if(report.isHasOutputItems()) {
									document.newPage();
								}else{
									report.setHasOutputItems(true);
								}
                        }

                        for (PartParam partParam : partParams) {
							if (SectionTypeEnum.TYPE_FAN.getId().equals(partParam.getKey())) {
								addImageFan(document, allMap, language, unitType, project, unit, partParam,fanCount);
							}
						}
					} catch (Exception e) {
						logger.error("工程版报告》添加风机曲线图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_PANEL_SCREENSHOTS.equals(item.getClassify())) {
					try {
						addImagePanelScreenshots(document , unit , partition , language,bookMark3,writer);
					} catch (Exception e) {
						logger.error("工程版报告》添加面板布置图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_UNIT_NAMEPLATE_DATA.equals(item.getClassify())) {
					try {
                        List<Part> parts = new ArrayList<>();
                        
                        for (PartPO partPO : partList) {
                        	parts.add(partPO.getCurrentPart());
                        }
                        //机组段数
//                        List<Part> airflowParts = AhuPartitionUtils.getAirflowOrderedPartList(unit, parts);
//                        allMap2.put(UtilityConstant.INFO_AHU_SECTION_COUNT,
//                                String.valueOf(AhuUtil.getAhuSectionCount(unit.getSeries(), airflowParts)));
                        String partitionJson = partition.getPartitionJson();
                        if (EmptyUtil.isNotEmpty(partitionJson)) {
                            allMap2.put(UtilityConstant.INFO_AHU_SECTION_COUNT,
                                    String.valueOf(ahuPartitionList.size()));                        	
                        }else {
                            allMap2.put(UtilityConstant.INFO_AHU_SECTION_COUNT,
                                    String.valueOf(UtilityConstant.SYS_STRING_NUMBER_1));
                        }
                        document.newPage();

                        BookMark bookMark3Temp_NameplateData = new BookMark();//第三层子项
                        int bookMark3Temp_NameplateData_start = writer.getPageNumber();

                        addUnitNameplateData(document, allMap, allMap2, project, unitType, unit, partition, language);

                        int bookMark3Temp_NameplateData_end = writer.getPageNumber();
                        bookMark3Temp_NameplateData.setNum(bookMark3Temp_NameplateData_start);
                        bookMark3Temp_NameplateData.setLevel(3);
                        bookMark3Temp_NameplateData.setTitle("机组铭牌:"+bookMark3Temp_NameplateData_start+"-"+bookMark3Temp_NameplateData_end);
                        bookMark3.add(bookMark3Temp_NameplateData);

					} catch (Exception e) {
						logger.error("工程版报告》机组铭牌数据》报错",e);
					}
				}
				int connectionNum=ahuPartitionList.size()-1;
				if (ReportConstants.CLASSIFY_SECTION_CONNECTION_LIST.equals(item.getClassify())) {
					try {

                        BookMark bookMark3Temp_SectionConnectionList = new BookMark();//第三层子项
                        int bookMark3Temp_SectionConnectionList_start = writer.getPageNumber();


						//第一个段连接处使用页面下拉的类型，后面的段使用“标准”类型
						String paneltype = unit.getPaneltype();
						//最新版本段连接清单逻辑
						if(String.valueOf(paneltype).contains("[") && !String.valueOf(paneltype).equals("[]")){
							List<String> types = JSONArray.parseArray(paneltype, String.class);
							for (String type : types) {
								String[] tempType = type.split(":");
								String theType = tempType[0];
								int theCount = Integer.parseInt(tempType[1]);
								for(int i=0;i<theCount;i++) {
                                    document.newPage();
                                    if(i==0){
										bookMark3Temp_SectionConnectionList_start = writer.getPageNumber();
									}
									addSectionConnectionList(theType, document, unit, language, allMap.get(UnitConverter.UNIT));
								}
							}

						}else if(connectionNum > 0){
							if (EmptyUtil.isEmpty(paneltype) || AhuUtil.isValidProduct(paneltype.toUpperCase()) || String.valueOf(paneltype).contains("\"") || String.valueOf(paneltype).contains("[]")) {//paneltype为空的时候默认“标准”,panelType 莫名其妙变成39CQ 的情况也处理为默认“标准”
								paneltype = UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD;
							}
							for (int i = 1; i <= connectionNum; i++) {
								if (i > 1) {
									paneltype = UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD;
								}
                                document.newPage();
								if(i==1){
									bookMark3Temp_SectionConnectionList_start = writer.getPageNumber();
								}
								addSectionConnectionList(paneltype, document, unit, language, allMap.get(UnitConverter.UNIT));
							}
						}


                        int bookMark3Temp_SectionConnectionList_end = writer.getPageNumber();
                        bookMark3Temp_SectionConnectionList.setNum(bookMark3Temp_SectionConnectionList_start);
                        bookMark3Temp_SectionConnectionList.setLevel(3);
                        bookMark3Temp_SectionConnectionList.setTitle("段连接清单:"+bookMark3Temp_SectionConnectionList_start+"-"+bookMark3Temp_SectionConnectionList_end);
                        if(ahuPartitionList.size()>1){
                            bookMark3.add(bookMark3Temp_SectionConnectionList);
                        }

					} catch (Exception e) {
						logger.error("工程版报告》段连接清单》报错", e);
					}
				}
				if (ReportConstants.CLASSIFY_PRODUCT_PACKING_LIST.equals(item.getClassify())) {
					boolean connectionFlag = false;
					if (ahuPartitionList.size() > 1) {
						connectionFlag = true;
					} else {
						connectionFlag = false;
					}
					try {
                        document.newPage();

                        BookMark bookMark3Temp_ProductPackingList = new BookMark();//第三层子项
                        int bookMark3Temp_ProductPackingList_start = writer.getPageNumber();


						addProductPackingList(document, allMap,allMap2, project, unitType, unit, partition, language, connectionNum,partList);

                        int bookMark3Temp_ProductPackingList_end = writer.getPageNumber();
                        bookMark3Temp_ProductPackingList.setNum(bookMark3Temp_ProductPackingList_start);
                        bookMark3Temp_ProductPackingList.setLevel(3);
                        bookMark3Temp_ProductPackingList.setTitle("产品装箱单:"+bookMark3Temp_ProductPackingList_start+"-"+bookMark3Temp_ProductPackingList_end);
                        bookMark3.add(bookMark3Temp_ProductPackingList);

					} catch (Exception e) {
						logger.error("工程版报告》产品装箱单》报错",e);
					}
				}
			}
			//获取送风机所在位置
            int supplyFanPos = getSupplyFanPos(ahuParam.getLayout(),ahuPartitionList);
			//填充第二层
            for (BookMark bookMark2Temp : bookMark2) {

                List<BookMark> bookMark3Temp = ObjectUtil.clone(bookMark3);//深拷贝

                String bk2Title = bookMark2Temp.getTitle();
                Iterator<BookMark> it = bookMark3Temp.iterator();
                while (it.hasNext()){
                    BookMark bookmark = it.next();
                    if(bookmark.getTitle().contains(":")){
                        String bk3TitlePreFix = bookmark.getTitle().split(":")[0];
                        if(bookmark.getTitle().startsWith("分段") && !bk3TitlePreFix.equals("分段"+bk2Title)){
                            it.remove();
                        }
                        if(bookmark.getTitle().startsWith("面板布置图") && !bk3TitlePreFix.equals("面板布置图"+bk2Title)){
                            it.remove();
                        }

                        if(bookmark.getTitle().startsWith("产品装箱单") && !(""+supplyFanPos).equals(bk2Title)){
                            it.remove();
                        }
                        if(bookmark.getTitle().startsWith("段连接清单") && !(""+supplyFanPos).equals(bk2Title)){
                            it.remove();
                        }
                        if(bookmark.getTitle().startsWith("机组铭牌") && !(""+supplyFanPos).equals(bk2Title)){
                            it.remove();
                        }
                    }
                }
                bookMark2Temp.setKids(bookMark3Temp);
            }
            //填充第一层
            bookMark1Temp.setKids(bookMark2);
            bookMark1.add(bookMark1Temp);
		}
		//批量更新书签
		try {
			PdfOutline root = writer.getRootOutline();
			for(BookMark bookMark :bookMark1){

                PdfOutline levelOneOutLine = new PdfOutline(root, PdfAction.gotoLocalPage(bookMark.getNum(), new PdfDestination(PdfDestination.XYZ), writer), bookMark.getTitle(), false);
				List<BookMark> levelTwoBooksmarks = bookMark.getKids();

                for (BookMark levelTwoBooksmark : levelTwoBooksmarks) {

                    PdfOutline levelTwoOutLine = new PdfOutline(levelOneOutLine, PdfAction.gotoLocalPage(levelTwoBooksmark.getNum(), new PdfDestination(PdfDestination.XYZ), writer), levelTwoBooksmark.getTitle(), false);
                    List<BookMark> levelThreeBooksmarks = levelTwoBooksmark.getKids();

                    for (BookMark levelThreeBooksmark : levelThreeBooksmarks) {

                        PdfOutline levelThreeOutLine = new PdfOutline(levelTwoOutLine, PdfAction.gotoLocalPage(levelThreeBooksmark.getNum(), new PdfDestination(PdfDestination.XYZ), writer), levelThreeBooksmark.getTitle(), false);

                    }

                }

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		close(document, writer, fos);
	}
	
    private static int getSupplyFanPos(AhuLayout layout, List<AhuPartition> ahuPartitionList) {
        try {
            for (AhuPartition ap : ahuPartitionList) {
                LinkedList<Map<String, Object>> sections = ap.getSections();
                for (Map<String, Object> section : sections) {
                    String metaId = String.valueOf(section.get(S_MKEY_METAID));
                    String sectionMetaJson = String.valueOf(section.get(S_MKEY_METAJSON));
                    Map<String, Object> sectionMap = new CaseInsensitiveMap();
                    sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));
                    if (SectionTypeEnum.TYPE_FAN.getId().equals(metaId)
                            ) {

                        String airDirection = MapValueUtils.getStringValue(METASEXON_AIRDIRECTION, sectionMap);
                        String doubleReturnAirDirection = MapValueUtils.getStringValue(METASEXON_DOUBLERETURN_AIRDIRECTION, sectionMap);//双层机组airDirection

                        boolean isSupplyFan = false;
                        //带回风机组，根据风机airDirection 判断outlet方向
                        if (EmptyUtil.isNotEmpty(layout)) {
                            if (layout.getStyle() > 20) {
                                //双层机组airDirection
                                if (LayoutStyleEnum.DOUBLE_RETURN_1.style() == layout.getStyle()
                                        || LayoutStyleEnum.DOUBLE_RETURN_2.style() == layout.getStyle()) {
                                    if (AirDirectionEnum.SUPPLYAIR.getCode().equals(doubleReturnAirDirection)) {//回风机组
                                        isSupplyFan = true;
                                    } else {
                                        if (AirDirectionEnum.SUPPLYAIR.getCode().equals(airDirection)) {//回风机组
                                            isSupplyFan = true;
                                        }
                                    }
                                }
                            } else {
                                if (AirDirectionEnum.SUPPLYAIR.getCode().equals(airDirection)) {//回风机组
                                    isSupplyFan = true;
                                }
                            }
                        }
                        if (isSupplyFan) {
                            return ap.getPos()+1;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

	/**
	 * 产品装箱单
	 * @param document
	 * @param unit
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
    private static void addProductPackingList(Document document, Map<String, Map<String, String>> ahuMap, Map<String, String> allMap, Project project,
                                              UnitSystemEnum unitType, Unit unit, Partition partition, LanguageEnum language, int connectionNum, List<PartPO> partList)
            throws IOException, DocumentException {
        logger.info("开始添加产品装箱单：unitid:" + unit.getUnitid() + " unitName:" + unit.getName());
        addLine(document);
        addBr(document);

		String[][] ahuContents = SectionContentConvertUtils.getProductPackingList(ahuMap, allMap, project, unitType, unit, language, connectionNum, partList);


        document.add(PDFTableGenerator.generate(ahuContents, REPORT_TECH_PRODUCTPACKINGLIST));
    }

	/**
	 * 段连接清单
	 * @param document
	 * @param unit
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
	private static void addSectionConnectionList(String paneltype, Document document, Unit unit, LanguageEnum language, Map<String, String> ahuMap) throws IOException, DocumentException {
		logger.info("开始添加段连接清单：unitid:"+unit.getUnitid()+" unitName:"+unit.getName());
		String unitSeries = AhuUtil.getUnitSeries(unit.getSeries());
		int unitWidth = AhuUtil.getWidthOfAHU(unit.getSeries());
		int unitHeight = AhuUtil.getHeightOfAHU(unit.getSeries());
		String unitModel = unit.getSeries();

		//面板切割页面变形处理。
		if(EmptyUtil.isNotEmpty(unit.getPanelSeries()) && !unit.getSeries().equals(unit.getPanelSeries())) {
			unitWidth = AhuUtil.getWidthOfAHU(unit.getPanelSeries());
			unitHeight = AhuUtil.getHeightOfAHU(unit.getPanelSeries());
			unitModel = unit.getPanelSeries();
		}

		String inskinm = SectionContentConvertUtils.getConnectionList(ahuMap);
		String[][] sectionConnectionListC = ReportMetadata.getTechSpecSectionConnection(unitSeries, unitModel,unitWidth,unitHeight, paneltype,inskinm);
		addLine(document);
		
		String connectionStr=getIntlString(SECTION_CONNECTION_LIST);
		if(UtilityConstant.JSON_UNIT_PANELTYPE_FOREPART.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONFOREPART);
		}else if(UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONPOSITIVE);
			
		}else if(UtilityConstant.JSON_UNIT_PANELTYPE_VERTICAL.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONVERTICAL);
			
		}
		Paragraph paragraph = new Paragraph(getIntlString(UNIT_MODEL_COLON) + unit.getSeries() +" "+ connectionStr
				, getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		document.add(paragraph);
		addBr(document);
		document.add(PDFTableGenerator.generate(sectionConnectionListC, REPORT_TECH_SECTION_CONNECTION_LIST
				+ UtilityConstant.SYS_PUNCTUATION_DOT + paneltype.toLowerCase()));
	}

	/**
	 * 机组铭牌数据
	 * @param document
	 * @param allMap
	 * @param allMap2
	 * @param project
	 * @param unitType
	 * @param unit
	 * @param partition
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
    private static void addUnitNameplateData(Document document, Map<String, Map<String, String>> allMap, Map<String, String> allMap2, Project project,
                                             UnitSystemEnum unitType, Unit unit, Partition partition, LanguageEnum language)
            throws IOException, DocumentException {
		List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);

        logger.info("开始添加机组铭牌数据：unitid:" + unit.getUnitid() + " unitName:" + unit.getName());
		addLine(document);
		Paragraph paragraph = new Paragraph(getIntlString(UNIT_NAMEPLATE_DATA) , getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		document.add(paragraph);
		addBr(document);

		String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_UNITNAMEPLATEDATA), project, unit,
				allMap2, language, unitType);
		ahuContents = SectionContentConvertUtils.getUnitNamePlateData(ahuContents, allMap, allMap2, language);
		ahuContents = SectionContentConvertUtils.getUnitNamePlateDataColdQHeatQ(ahuContents,ahuPartitionList, allMap2);//铭牌冷热量
		document.add(PDFTableGenerator.generate(ahuContents, ReportConstants.REPORT_TECH_UNITNAMEPLATEDATA));


//		String[][] titleContents = arrayMap.get("TechUnitnameplatedata2");
		String[][] titleContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_UNITNAMEPLATEDATA_SHAPE), project, unit,
				allMap2, language, unitType);
		String partitionJson = partition.getPartitionJson();
		if (EmptyUtil.isNotEmpty(partitionJson)) {
			String[][] partitionContents = new String[titleContents.length + ahuPartitionList.size()][6];
			partitionContents[0] = titleContents[0];
			partitionContents[1] = titleContents[1];
			int i = 2;
			for (AhuPartition ap : ahuPartitionList) {
				String[] temContents = null;
				temContents = SectionContentConvertUtils.getMPStrs(unit, ap, ahuPartitionList);
				partitionContents[i] = temContents;
				i++;
			}
			document.add(PDFTableGenerator.generate(partitionContents, ReportConstants.REPORT_TECH_UNITNAMEPLATEDATA_SHAPE));
		}
	}

    /**
	 * 封装报告中有关ahu高度的计算
	 * @param allMap
	 * @param partition
	 * @param unit
	 * @return
	 */
	public static String wrapHeight(Map<String, String> allMap, Partition partition, Unit unit, AhuPartition ap) {
		String finalHeight = UtilityConstant.SYS_BLANK;
		String series = unit.getSeries();
		int type = Integer.valueOf(series.substring(series.length() - 4, series.length()));
		String heightInMap = String.valueOf(allMap.get(UtilityConstant.METAHU_HEIGHT));
		if (EmptyUtil.isNotEmpty(heightInMap)) {
			if (608 <= type && SystemCountUtil.lteSmallUnit(type)) {
				finalHeight = String.valueOf(BaseDataUtil.decimalConvert(Double.valueOf(heightInMap) + (ap.isTopLayer()?0:100),1));
			} else if (SystemCountUtil.gteBigUnit(type)&& type <= 3438) {
				finalHeight = String.valueOf(BaseDataUtil.decimalConvert(Double.valueOf(heightInMap) + (ap.isTopLayer()?0:200),1));
			}
		} else {
			return finalHeight;
		}
		return finalHeight;
	}
    
	/**
	 * 技术说明(销售版)
	 * 
	 * @param file
	 * @param report
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void genPdf4Techsaler(File file, Report report, ReportData reportData, LanguageEnum language,
                                        UnitSystemEnum unitType) throws DocumentException, IOException {
		FileOutputStream fos = new FileOutputStream(file.getPath());
		Document document = new Document(PageSize.A4);// 文档
		PdfWriter writer = PdfWriter.getInstance(document, fos);// PDF文档
		writer.setPdfVersion(PdfWriter.PDF_VERSION_1_7);
		writer.setTagged();
		writer.setViewerPreferences(PdfWriter.DisplayDocTitle);
		writer.createXmpMetadata();

        writer.setPageEvent(new TechHeader(getIntlString(CARRIER_CORPORATION_FULL_NAME),
                getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL)));
        Date d = new Date();

		Project project = reportData.getProject();
		Map<String, Partition> partitionMap = reportData.getPartitionMap();

		// 添加机组信息
		int pageIndex = 0;
		for (Unit unit : reportData.getUnitList()) {
            Phrase phrase = new Phrase();
            
            Chunk timeChunk = new Chunk(
                    format("Date: {0} Time: {1}", DateUtil.getDateTimeString(d, YYYY_MM_DD),
                            DateUtil.getDateTimeString(d, HH_MM)),
                    getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL));
            phrase.add(timeChunk);

            Chunk wenzi = new Chunk("                    " + getIntlString(TECHNICAL_SPECIFICATION),
                    getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE));
            phrase.add(wenzi);
            phrase.add(Chunk.NEWLINE);

            String[][] headerContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEADER),
                    reportData.getProject(), unit, null, language, unitType);
            for (int i = 0; i < headerContents.length; i++) {
                phrase.add(new Chunk(fixHeaderLine(headerContents[i]),
                        getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL)));
            }
            phrase.add(Chunk.NEWLINE);

            HeaderFooter header = new HeaderFooter(phrase, false);
            header.setBorder(Rectangle.NO_BORDER); // 设置为没有边框
            header.setAlignment(Element.ALIGN_LEFT);
            document.setHeader(header);
            if (!document.isOpen()) {
                document.open();// 写入数据之前要打开文档
                addTitle(document, report.getName());
            }
			if (0 != pageIndex) {// 当前机组遍历完毕后，重新打开一页，第一次遍历的时候无需开新一页
				document.newPage();
			}
			pageIndex++;

			logger.info("开始添加技术说明(销售版)：unitid:"+unit.getUnitid()+" unitName:"+unit.getName());
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}
			Partition partition = null;
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}
			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
			List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);
			addLine(document);
			String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_AHU_OVERALL), reportData.getProject(),
					unit, allMap.get(UnitConverter.UNIT), language, unitType);
			document.add(PDFTableGenerator.generate(ahuContents, ReportConstants.REPORT_TECH_AHU_OVERALL));

			Paragraph paragraph = getChineseParagraph(getIntlString(UNIT_INCLUDE_BASED_ON_AIR_FLOW_DIRECTION), CONTENT_NOMAL);
			paragraph.setAlignment(Element.ALIGN_LEFT);
			document.add(paragraph);
			AhuParam ahuParam = ValueFormatUtil.transDBData2AhuParam(project, unit, partList, partition);

			String groupCode = unit.getGroupCode();
			if (null == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
				logger.warn(MessageFormat.format("技术说明报告-AHU组编码信息不合法，报告忽略生成对应段信息 - unitNo：{0},GroupCode:{1}",
						new Object[] { unit.getUnitNo(), unit.getGroupCode() }));
			} else {
				int order = 1;
				List<PartPO> orderedPartList = AhuPartitionUtils.getAirflowOrderedPartPOListForReport(unit, partList);
                for (PartPO partPO : orderedPartList) {
                	if(partPO==null) {
                		continue;
                	}
					Part part = partPO.getCurrentPart();
					ExcelForPriceCodeUtils.setPartitionOfPart(part, ahuPartitionList);
					String key = part.getSectionKey();
					addLine(document);
					Map<String, String> noChangeMap=allMap.get(UnitConverter.genUnitKey(unit, part));
	                Map<String, String> cloneMap=UnitUtil.clone(noChangeMap);
                    PDFTableGen.addSection4Saler(document, key,String.valueOf(part.getPos()), order, noChangeMap,cloneMap,
                            language, unitType, project, unit,ahuParam);
                    order++;
				}
			}
			try {
				addImage3View(document, ahuParam, language);
			} catch (Exception e) {
				logger.error("销售版报告》添加三视图》报错",e);
			}

			try {
				addImagePsychometric(document, ahuParam, language);
			} catch (Exception e) {
				logger.error("销售版报告》添加焓湿图》报错",e);
			}
		}
		close(document, writer, fos);
	}

	/**
	 * 添加段落标题
	 * 
	 * @param document
	 * @param title
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addTitle(Document document, String title) throws DocumentException, IOException {
		Paragraph titleParagraph = getChineseParagraph(title, TITLE_BIGGER);
		titleParagraph.setSpacingAfter(10f);
		titleParagraph.setAlignment(Element.ALIGN_CENTER);
		document.add(titleParagraph);
	}

	/**
	 * 获取中文字体
	 * 
	 * @param fontType
	 *            字体类型
	 * @param fontSize
	 *            字体大小
	 * 
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static Font getBaseFontChinese(String fontType, Integer fontSize) throws DocumentException, IOException {
		BaseFont bfChinese = BaseFont.createFont(fontType, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		return new Font(bfChinese, fontSize, Font.NORMAL); // 设置为中文
	}

	/**
	 * 获取中文字体-加粗
	 * 
	 * @param fontType
	 *            字体类型
	 * @param fontSize
	 *            字体大小
	 * 
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static Font getBaseFontChineseBold(String fontType, Integer fontSize) throws DocumentException, IOException {
		BaseFont bfChinese = BaseFont.createFont(fontType, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		return new Font(bfChinese, fontSize, Font.BOLD); // 设置为中文
	}

	/**
	 * 中文转换工具
	 * 
	 * @param str
	 * @param fontSize
	 *            字体大小
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static Paragraph getChineseParagraph(String str, Integer fontSize) throws DocumentException, IOException {
		Paragraph pragraph = new Paragraph(str, getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, fontSize));
		return pragraph;
	}

	/**
	 * 中文转换工具
	 * 
	 * @param str
	 * @param fontSize
	 *            字体大小
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static Paragraph getChineseBoldParagraph(String str, Integer fontSize)
			throws DocumentException, IOException {
		Paragraph pragraph = new Paragraph(str, getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, fontSize));
		return pragraph;
	}

	/**
	 * 设置页眉
	 * 
	 * @param document
	 * @param headerStr
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void setHeader(Document document, String headerStr) throws DocumentException, IOException {
		Paragraph para = new Paragraph(headerStr, getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE));
		HeaderFooter header = new HeaderFooter(para, false);
		header.setBorder(Rectangle.NO_BORDER); // 设置为没有边框
		header.setAlignment(1);
		document.setHeader(header);
	}

	/**
	 * 添加品牌图
	 * 
	 * @param document
	 * @param param
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImageLogo(Document document, LanguageEnum locale, Unit unit) throws DocumentException, IOException {
		String aeroPath = SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR;
		String carrierPath = SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR;

		if (LanguageEnum.Chinese.equals(locale)) {
			aeroPath += "Aero_ch.png";
			carrierPath += "CAR_LOGO_ch.png";
		} else {
			aeroPath += "Aero_en.bmp";
			carrierPath += "CAR_LOGO_en.png";
		}

		// Eurovent update begin
		Image imgAero = Image.getInstance(aeroPath);
		imgAero.scalePercent(4);
		if (!LanguageEnum.Chinese.equals(locale)) {
			imgAero.scalePercent(20);
		}
		Image imgCarrier = null;
		if (SystemCountUtil.isEuroUnit(unit)) {
			float x = 36;
			// float y = (PageSize.A4.getHeight() - imgAero.getScaledHeight()) - 150;
			float y = true ? ((PageSize.A4.getHeight() - imgAero.getScaledHeight()) - 210)
					: ((PageSize.A4.getHeight() - imgAero.getScaledHeight()) - 150);
			imgAero.setAbsolutePosition(x, y);

			imgCarrier = Image.getInstance(carrierPath);
			imgCarrier.scalePercent(25);
			float x1 = PageSize.A4.getWidth() - imgCarrier.getScaledWidth() - 36;
			// float y1 = (PageSize.A4.getHeight() - imgCarrier.getScaledHeight()) - 135;
			float y1 = true ? ((PageSize.A4.getHeight() - imgCarrier.getScaledHeight()) - 195)
					: ((PageSize.A4.getHeight() - imgCarrier.getScaledHeight()) - 135);
			imgCarrier.setAbsolutePosition(x1, y1);			
		}else {
			float x = 36;
			float y = (PageSize.A4.getHeight() - imgAero.getScaledHeight()) - 150;
			imgAero.setAbsolutePosition(x, y);

			imgCarrier = Image.getInstance(carrierPath);
			imgCarrier.scalePercent(25);
			float x1 = PageSize.A4.getWidth() - imgCarrier.getScaledWidth() - 36;
			float y1 = (PageSize.A4.getHeight() - imgCarrier.getScaledHeight()) - 135;
			imgCarrier.setAbsolutePosition(x1, y1);
		}


		Paragraph info = new Paragraph();
		info.add(new Chunk(UtilityConstant.SYS_BLANK));
		info.setSpacingAfter(SystemCountUtil.isEuroUnit(unit)?48f:90f);
		// info.setSpacingAfter(90f);// 设置段落下空白，放置图片
//		info.setSpacingAfter(48f);// 设置段落下空白，放置图片
		document.add(info);
		//只有CQ才打印aero图标
		if(SystemCountUtil.isAeroUnit(unit)){
			document.add(imgAero);
		}
		document.add(imgCarrier);
		// Eurovent update end
	}

	/**
	 * 添加图片
	 * @param document
	 * @param imgName		图片文件名
	 * @param scale			图片尺寸
	 * @param xPosition		x轴坐标
	 * @param yOffset		y轴偏移量
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImage(Document document, String imgName, float scale, float xOffset, float yOffset, float spacingAfter) throws DocumentException, IOException {
		String imgPath = SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR;

		imgPath += imgName;

		Image img = Image.getInstance(imgPath);
		img.scalePercent(scale);
		
		float x = PageSize.A4.getWidth() - img.getScaledWidth() - xOffset;
		float y = PageSize.A4.getHeight() - img.getScaledHeight() - yOffset;
		img.setAbsolutePosition(x, y);
		
		Paragraph info = new Paragraph();
		info.add(new Chunk(UtilityConstant.SYS_BLANK));
		info.setSpacingAfter(spacingAfter);// 设置段落下空白，放置图片
		document.add(info);
		document.add(img);

	}
	
	/**
	 * 设置页脚
	 * 
	 * @param document
	 * @param footerStr
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void setFooter(Document document, String footerStr) throws DocumentException, IOException {
		HeaderFooter footer = new HeaderFooter(
				new Phrase(footerStr, getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, FOOTER_TITLE)), true);
		footer.setBorder(Rectangle.NO_BORDER);
		/**
		 * 0是靠左 1是居中 2是居右
		 */
		footer.setAlignment(2);
		document.setFooter(footer);
	}

	/**
	 * 添加段落
	 * 
	 * @param document
	 * @param title
	 * @param content
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addParagraph(Document document, String title, String content, Integer alignment)
			throws DocumentException, IOException {
		// 定义段落
		Paragraph paragraph = getChineseParagraph(title, TABLE_TITLE_BIGGER);
		paragraph.setAlignment(Element.ALIGN_CENTER);
		document.add(paragraph);

		Paragraph paragraphContent = getChineseParagraph(content, CONTENT_NOMAL);
		// Phrase phrase = new Phrase();
		// // 插入十条文本块到段落中
		// int i = 0;
		// for (i = 0; i < 10; i++) {
		// Chunk chunk = new Chunk(content + i + ".\n ");
		// phrase.add(chunk);
		// }
		// paragraphContent.add(phrase);
		// 设置一个段落前后的间距
		paragraphContent.setSpacingAfter(50);
		paragraphContent.setSpacingBefore(50);
		paragraphContent.setAlignment(alignment);

		// 添加段落
		document.add(paragraphContent);
	}

	/**
	 * 添加直线
	 * @param document
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addLine(Document document) throws DocumentException, IOException {
		Paragraph line = new Paragraph();
		line.add(new Chunk(new LineSeparator()));
		line.setSpacingAfter(8f);
		document.add(line);
	}

	/**
	 * 添加换行
	 * @param document
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addBr(Document document) throws DocumentException, IOException {
		Paragraph line = new Paragraph();
		line.add(new Chunk(UtilityConstant.SYS_PUNCTUATION_NEWLINE));
		document.add(line);
	}

	/**
	 * 添加表格名称
	 * @param document
	 * @param titleStr
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addTableTitle(Document document, String titleStr) throws DocumentException, IOException {
		Paragraph title = getChineseParagraph(titleStr, TABLE_TITLE_BIGGER);
		title.setSpacingAfter(20);
		title.setAlignment(Element.ALIGN_CENTER);
		document.add(title);
	}

	/**
	 * 添加工程格式报告段名称
	 * @param document
	 * @param partName
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addPartName(Document document, String partName) throws DocumentException, IOException {
		Paragraph paragraph = new Paragraph(partName, getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, HEADER_TITLE));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		paragraph.setSpacingAfter(10f);
		document.add(paragraph);
	}

	/**
	 * 修正页眉格式
	 * 
	 * @param strs
	 * @return
	 */

	private static String fixHeaderLine(String[] strs) {
		StringBuffer sb = new StringBuffer();
		int leftHalf = String_length(strs[0]) + String_length(strs[1]) + String_length(strs[2]);

		sb.append("\n  ").append(strs[0]).append(strs[1]).append(strs[2]);

		if (leftHalf < 60) {
			for (int i = leftHalf; i < 60; i++) {
				sb.append(" ");
			}
			sb.append(strs[3]).append(strs[4]).append(strs[5]);
		} else {
			sb.append(UtilityConstant.SYS_PUNCTUATION_NEWLINE).append(strs[3]).append(strs[4]).append(strs[5]);
		}
		return sb.toString();
	}

	/**
	 * 判断字段真实长度的实例(中文2个字符,英文1个字符)
	 * 
	 * @param value
	 * @return
	 */
	public static int String_length(String value) {
		int valueLength = 0;
		String chinese = "[\u4e00-\u9fa5]";
		for (int i = 0; i < value.length(); i++) {
			String temp = value.substring(i, i + 1);
			if (temp.matches(chinese)) {
				valueLength += 2;
			} else {
				valueLength += 1;
			}
		}
		return valueLength;
	}

	/**
	 * 关闭文件相关
	 * 
	 * @param document
	 * @param writer
	 * @param fos
	 * @throws IOException
	 */
	private static void close(Document document, PdfWriter writer, FileOutputStream fos) throws IOException {
		if (EmptyUtil.isNotEmpty(document)) {
			document.close();
		}
		if (EmptyUtil.isNotEmpty(writer)) {
			writer.close();
		}
		if (EmptyUtil.isNotEmpty(fos)) {
			fos.close();
		}
	}

	/**
	 * 添加三视图
	 * 
	 * @param document
	 * @param param
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImage3View(Document document, AhuParam param, LanguageEnum language)
			throws DocumentException, IOException {
		logger.info("开始生成三视图：unitid:"+param.getUnitid()+" unitName:"+param.getName());
		String fileName = UtilityConstant.SYS_BLANK+System.currentTimeMillis();
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName();
		}
		
		String destPath = String.format(
				SysConstants.CAD_DIR + File.separator + "%s" + File.separator + "%s" + File.separator + "%s.bmp", param.getPid(),
				param.getUnitid(), fileName);
		CadUtils.exportBmp(param, destPath);
		Image img = Image.getInstance(SysConstants.ASSERT_DIR + File.separator + destPath);
		img.scalePercent(45);
		float x = (PageSize.A4.getWidth() - img.getScaledWidth()) / 2;
		float y = (PageSize.A4.getHeight() - img.getScaledHeight()) / 2;
		img.setAbsolutePosition(x, y);
		addLine(document);
		Paragraph paragraph = new Paragraph(getIntlString(AHU_TRIPLE_VIEW_SEQUENCE_NUMBER) + param.getUnitNo(),
				getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		document.add(paragraph);
		document.add(img);
	}

	/**
	 * 添加面板布置图
	 * @param document
	 * @param unit
	 * @param partition
	 * @param language
	 * @param bookMark3
     * @param writer
	 * @throws DocumentException
	 * @throws IOException
	 */
	private static void addImagePanelScreenshots(Document document, Unit unit, Partition partition, LanguageEnum language, List<BookMark> bookMark3, PdfWriter writer) throws DocumentException, IOException {
		logger.info("开始添加面板切割布置图：unitid:"+unit.getUnitid()+" unitName:"+unit.getName());
		String fileName = UtilityConstant.SYS_BLANK+System.currentTimeMillis();
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName();
		}
		
		List<AhuPartition> partitions = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
		for(int i=0;i<partitions.size();i++) {
			String destPath = MessageFormat.format(FileNamesLoadInSystem.PANEL_IMG_PATH, unit.getUnitid()) + File.separator + partition.getPartitionid()+ (i+1) + ".png";
			Image img = null;
			try {
				img = Image.getInstance(destPath);
			} catch (Exception e) {
				continue;
			}

			float contentRatio = 0.71f;
			float blWidth = 0f;
			float blHeight = 0f;
			float pagewidth = PageSize.A4.getWidth();
			float pageheight = PageSize.A4.getHeight();
			float imgwdith = img.getScaledWidth();
			float imgheight = img.getScaledHeight();

			if(pagewidth < imgwdith){
				blWidth = (pagewidth /imgwdith)*100-2;
			}
			if ((pageheight*contentRatio - imgheight)<0) {//排除表头
				double scale = (pageheight*contentRatio/imgheight)*100;
				BigDecimal scaleBd = new BigDecimal(scale);//四舍五入，保留两位小数
				scaleBd = scaleBd.setScale(2, BigDecimal.ROUND_HALF_UP);
				blHeight = Float.parseFloat(String.valueOf(scaleBd));
			}
			float thesPercent = blWidth <= blHeight ? blWidth : blHeight;
			img.scalePercent(blWidth<=blHeight?blWidth:blHeight);


			float x = 0;
			float y = (float) (pageheight*contentRatio - imgheight/100*thesPercent);
			img.setAbsolutePosition(x, y);
			document.newPage();


			//书签
            BookMark bookMark3Temp_PanelScreenshots = new BookMark();//第三层子项
            int bookMark3Temp_PanelScreenshots_start = writer.getPageNumber();
            //书签

			addLine(document);
			Paragraph paragraph = new Paragraph(getIntlString(AHU_PANEL_SCREENSHOTS_SEQUENCE_NUMBER) + unit.getUnitNo() + "-" + (i+1),
					getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
			paragraph.setAlignment(Element.ALIGN_LEFT);
			document.add(paragraph);
			document.add(img);


            //书签
            int bookMark3Temp_PanelScreenshots_end = writer.getPageNumber();
            bookMark3Temp_PanelScreenshots.setNum(bookMark3Temp_PanelScreenshots_start);
            bookMark3Temp_PanelScreenshots.setLevel(3);
            bookMark3Temp_PanelScreenshots.setTitle("面板布置图"+(i+1)+":"+bookMark3Temp_PanelScreenshots_start+"-"+bookMark3Temp_PanelScreenshots_end);
            bookMark3.add(bookMark3Temp_PanelScreenshots);
            //书签

		}
		
	}

	/**
	 * 添加焓湿图
	 * 
	 * @param document
	 * @param param
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImagePsychometric(Document document, AhuParam param, LanguageEnum language)
			throws DocumentException, IOException {
		logger.info("开始生成焓湿图：unitid:"+param.getUnitid()+" unitName:"+param.getName());
		Iterator<PartParam> it = param.getPartParams().iterator();

		List<PsyCalBean> datas = PsychometricDrawer.drawPsy(it);

		String path = PsychometricDrawer.genPsychometric(param.getPid(), param.getUnitid(), datas);
		
		Image img = Image.getInstance(SysConstants.ASSERT_DIR + path.substring(0,path.indexOf("?")));
		img.scalePercent(13);
		float x = (PageSize.A4.getWidth() - img.getScaledWidth()) / 2;
		float y = (PageSize.A4.getHeight() - img.getScaledHeight()) / 2;
		img.setAbsolutePosition(x, y);
		addLine(document);
		Paragraph paragraph = new Paragraph(getIntlString(AHU_PSY_CHART_SEQUENCE_NUMBER) + param.getUnitNo(),
				getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
		paragraph.setAlignment(Element.ALIGN_LEFT);
		document.add(paragraph);
		document.add(img);

	}

	/**
	 * 添加风机曲线图
	 *
	 * @param document
	 * @param allMap
     *
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param fanCount
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void addImageFan(Document document, Map<String, Map<String, String>> allMap,
                                   LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, PartParam partParam, int fanCount)
            throws DocumentException, IOException {
		
				Map<String, Object> params = partParam.getParams();
				String path = String.valueOf(params.get(UtilityConstant.METASEXON_FAN_CURVE));
				path = path.replace("files/", UtilityConstant.SYS_BLANK);// 页面使用的为files 后端路径去掉files
				if (EmptyUtil.isNotEmpty(path)) {
					Image img = Image.getInstance(SysConstants.ASSERT_DIR + path);
					img.scalePercent(83);
					float x = (PageSize.A4.getWidth() - img.getScaledWidth()) - 60;
					float y = (PageSize.A4.getHeight() - img.getScaledHeight()) / 2;
					img.setAbsolutePosition(x, y);
					addLine(document);

					String fanName = getIntlString(SUPPLY_FAN);// 默认送风机
					String airDirection = MapValueUtils.getStringValue(METASEXON_AIRDIRECTION, params);
					if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)) {//回风机组
						fanName =getIntlString(RETURN_FAN);
					}

					Paragraph paragraph = new Paragraph(getIntlString(AHU_FAN_CURVE_SEQUENCE_NUMBER) + unit.getUnitNo() +
							(fanCount>1?fanName:""),
							getBaseFontChineseBold(UtilityConstant.SYS_PATH_CHINESESONGTIFONTPATH, CONTENT_NOMAL));
					paragraph.setAlignment(Element.ALIGN_LEFT);
					document.add(paragraph);
					
					Map<String, String> noChangeMap = allMap.get(UnitConverter.genUnitKey(UtilityConstant.SYS_BLANK + partParam.getPosition(), partParam.getUnitid(),
							partParam.getKey()));
					Map<String, String> cloneMap = UnitUtil.clone(noChangeMap);
					Map<String, String> all = SectionDataConvertUtils.getFan(noChangeMap, cloneMap, language, null);
					String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_CURVE),
							project, unit, all, language, unitType);
					
					Map<String, String> allMap1 = new HashMap<String, String>();
					for (String key : allMap.keySet()) {
						if(key.indexOf(UtilityConstant.METASEXON_FAN)>0) {
							allMap1 = (Map<String, String>)allMap.get(key);
						}
					}
					// 封装风机段特殊字段
//					contents = SectionContentConvertUtils.getFan4(contents, allMap.get(UnitConverter.genUnitKey(UtilityConstant.SYS_BLANK + partParam.getPosition(), partParam.getUnitid(),
//							partParam.getKey())), language);
					
					addBr(document);
					document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_FAN_CURVE));
					document.add(img);
				}
			
	}
	private static String format(String pattern, Object... arguments) {
		return MessageFormat.format(pattern, arguments);
	}

	/**
	 * 判断机组中是否存在风机段
	 * @param unit
	 * @param partList
	 * @return
	 */
	private static boolean ifContainFan(Unit unit, List<PartPO> partList){
		boolean isFan = false;
		for (PartPO partPO : partList) {
			Part part = partPO.getCurrentPart();
			String key = part.getSectionKey();
			if ("ahu.fan".equals(key)) {
				isFan = true;
				break;
			}
		}
		return isFan;
	}
	
	/**
	 * 添加欧标头部内容
	 * @param phrase
	 * @param unit
	 * @param allMap
	 * @param partList
	 * @param d
	 */
	private static void euroHeader(Phrase phrase, Unit unit, Map<String, Map<String, String>> allMap,
                                   List<PartPO> partList, Date d) throws DocumentException, IOException {
		phrase.add(new Chunk(MessageFormat.format(" {0}:{1}", getIntlString(I18NConstants.DATE),DateUtil.getDateTimeString(d, YYYY_MM_DD)),
				getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL)));
		//计算能效
		List<String> euroList = SectionContentConvertUtils.calEnergyEfficiency(unit, allMap, partList);
		
		phrase.add(new Chunk("     ",
				getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, HEADER_TITLE_BOLD)));
		phrase.add(new Chunk(euroList.get(0),
				getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, HEADER_TITLE_BOLD)).setUnderline(-1, -8));
		phrase.add(new Chunk(euroList.get(1),
				getBaseFontChinese(UtilityConstant.SYS_PATH_WINGDNG3, HEADER_TITLE_BOLD)).setUnderline(-1, -8));
		phrase.add(new Chunk(" (2016)",
				getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, HEADER_TITLE_BOLD)).setUnderline(-1, -8));
		phrase.add( new Chunk(MessageFormat.format("        {0}：CarrierAHU",getIntlString(I18NConstants.SOFTWARE_NAME)),
				getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, CONTENT_NOMAL)) );
		phrase.add(Chunk.NEWLINE);// 换行
		phrase.add(Chunk.NEWLINE);// 换行
		
//		phrase.add(new Chunk("                                                               Carrier Air-Conditioning and Refrigeration System (Shanghai) Co., Ltd. is",
//				getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, 7)));
//		phrase.add(Chunk.NEWLINE);// 换行
//		phrase.add(new Chunk("                                                               participating in the EUROVENT CERTIFICATION Programme for Air Handling ",
//				getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, 7)));
//		phrase.add(Chunk.NEWLINE);// 换行
//		String series = unit.getSeries();
//		if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39CQ)) {// 39CQ
//			phrase.add(new Chunk("                                                               Units. The range 39CQ unit iscertified under the number 19.05.032 and presented",
//					getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, 7)));
//		} else if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {// 39XT
//			phrase.add(new Chunk("                                                               Units. The range 39XT unit iscertified under the number 19.05.033 and presented",
//					getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, 7)));
//		}
//		phrase.add(Chunk.NEWLINE);// 换行
//		phrase.add(new Chunk("                                                               on www.eurovent-certification.com.",
//				getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, 7)));
//		phrase.add(Chunk.NEWLINE);// 换行
		
		phrase.add(new Chunk("  Carrier Air-Conditioning and Refrigeration System (Shanghai) Co., Ltd. is participating in the EUROVENT CERTIFICATION",
				getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, 7)));
		phrase.add(Chunk.NEWLINE);// 换行
		
		String series = unit.getSeries();
		if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39CQ)) {// 39CQ
			phrase.add(new Chunk("  Programme for Air Handling Units. The range 39CQ unit iscertified under the number 19.05.032 and presented",
					getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, 7)));
		} else if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39G)) {// 39G
			phrase.add(new Chunk("  Programme for Air Handling Units. The range 39G unit iscertified under the number 19.05.032 and presented",
					getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, 7)));
		} else if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {// 39XT
			phrase.add(new Chunk("  Programme for Air Handling Units. The range 39XT unit iscertified under the number 19.05.033 and presented",
					getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, 7)));
		}
		phrase.add(Chunk.NEWLINE);// 换行
		phrase.add(new Chunk("  on www.eurovent-certification.com.",
				getBaseFontChinese(UtilityConstant.SYS_PATH_CHINESEMSYHFONTPATH, 7)));
		phrase.add(Chunk.NEWLINE);// 换行
	}
	
}

package com.carrier.ahu.util.heatrecycle;

import lombok.Data;

@Data
public class HeatParam {

    private double qt;
    private double qs;

    public HeatParam(double qt, double qs) {
        this.qt = qt;
        this.qs = qs;
    }

}
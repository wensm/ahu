package com.carrier.ahu.engine.cad;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_HEATINGCOIL;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_WETFILMHUMIDIFIER;
import static com.carrier.ahu.common.intl.I18NConstants.AIR_VOLUME;
import static com.carrier.ahu.common.intl.I18NConstants.COIL_MODEL;
import static com.carrier.ahu.common.intl.I18NConstants.COIL_SECTION;
import static com.carrier.ahu.common.intl.I18NConstants.COOLING_VOLUME;
import static com.carrier.ahu.common.intl.I18NConstants.FAN_MODEL;
import static com.carrier.ahu.common.intl.I18NConstants.FAN_SECTION;
import static com.carrier.ahu.common.intl.I18NConstants.FULL_STATIC_PRESSURE;
import static com.carrier.ahu.common.intl.I18NConstants.HEATING_VOLUME;
import static com.carrier.ahu.common.intl.I18NConstants.MOTOR_POWER;
import static com.carrier.ahu.common.intl.I18NConstants.NAME;
import static com.carrier.ahu.common.intl.I18NConstants.PARAMETER;
import static com.carrier.ahu.common.intl.I18NConstants.VOLTAGE;
import static com.carrier.ahu.common.intl.I18NConstants.WATER_FLOW_VOLUME;
import static com.carrier.ahu.common.intl.I18NConstants.WATER_RESISTANCE;
import static com.carrier.ahu.constant.CommonConstant.*;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWPlate;
import com.carrier.ahu.util.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.DamperPosEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.cad.CoilBigDimension;
import com.carrier.ahu.metadata.entity.cad.CoilBigDimensionL;
import com.carrier.ahu.metadata.entity.cad.CoilBigDimensionR;
import com.carrier.ahu.metadata.entity.cad.CoilBigTuojiaDimension;
import com.carrier.ahu.metadata.entity.cad.CoilDimension;
import com.carrier.ahu.metadata.entity.cad.ColdCoilDimension;
import com.carrier.ahu.metadata.entity.cad.Damper;
import com.carrier.ahu.metadata.entity.cad.DamperDimension;
import com.carrier.ahu.metadata.entity.cad.DamperDimensionFac;
import com.carrier.ahu.metadata.entity.cad.DamperDimensionL;
import com.carrier.ahu.metadata.entity.cad.ECoilDimension;
import com.carrier.ahu.metadata.entity.cad.FanDimension;
import com.carrier.ahu.metadata.entity.cad.FanDimensionBMotor;
import com.carrier.ahu.metadata.entity.cad.FilterDrawing;
import com.carrier.ahu.metadata.entity.cad.HCoilBigDimensionNew;
import com.carrier.ahu.metadata.entity.cad.HCoilBigTuojiaDimension;
import com.carrier.ahu.metadata.entity.cad.HeatCoilDimension;
import com.carrier.ahu.metadata.entity.cad.PaneledDoor;
import com.carrier.ahu.metadata.entity.cad.SteamBigTuojiaDimension;
import com.carrier.ahu.metadata.entity.cad.SteamCoilDimension;
import com.carrier.ahu.metadata.entity.fan.FanCodeElectricSize;
import com.carrier.ahu.metadata.entity.fan.FanCodeElectricSizeBig;
import com.carrier.ahu.metadata.entity.fan.SPlugfandimension;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.po.model.PartitionObject;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IniUtils2 {
	private static final Logger logger = LoggerFactory.getLogger(IniUtils2.class);
	private static final String mxbStr = "{0}:+{1} {2}";

	private static Map<String, Object> ahuConfigMap = new HashMap<>();

	public static Map<String, Object> getAhuConfigMap() {
		return ahuConfigMap;
	}

	public static void setAhuConfigMap(Map<String, Object> ahuConfigMap) {
		IniUtils2.ahuConfigMap = ahuConfigMap;
	}

	public static void create(AhuParam params, String destPath) throws IOException {
		ahuConfigMap = configMapAhu(params);
		try (PrintWriter writer = new PrintWriter(new File(destPath), UtilityConstant.SYS_ENCODING_GBK)) {
			writeUnitInfo(params, writer);
			writeFrames(params, writer);
		}
	}

	/**
	 * CAD配置[机组属性]
	 *
	 * @param params
	 * @param writer
	 */
	private static void writeUnitInfo(AhuParam params, PrintWriter writer) {
		writeSection(UtilityConstant.METACOMMON_UNITINFO, ahuConfigMap, writer);
	}

	/**
	 * CAD配置[分段配置]
	 *
	 * @param params
	 * @param writer
	 */
	/**
	 * @param params
	 * @param writer
	 */
	private static void writeFrames(AhuParam params, PrintWriter writer) {
		Map<String, AhuPartition> framedPartitionMap = new HashMap<>();

		Map<String, AhuPartition> sortFramedPartitionMap = new LinkedHashMap<String, AhuPartition>();



		LinkedList<Map<String, Object>> writableSections = new LinkedList<>();
		LinkedList<Map<String, Object>> writableTopSections = new LinkedList<>();
		LinkedList<Map<String, Object>> writableBottomSections = new LinkedList<>();

		List<AhuPartition> bottomPartitions = params.getBottomPartitions();

		AhuLayout ahuLayout = params.getLayout();
		//当前机组数据异常需要重新进入机组界面点击保存按钮。
		if (EmptyUtil.isEmpty(ahuLayout)) {
			throw new ApiException(ErrorCode.PARTITION_VALIDATE_LAYOUT_JSON_NULL_MSG);
		}

		// 并排转向机组 安装单层机组处理三视图
		if (LayoutStyleEnum.SIDE_BY_SIDE_RETURN_1.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.SIDE_BY_SIDE_RETURN_2.style() == ahuLayout.getStyle()) {
			bottomPartitions.addAll(params.getTopPartitions());
			params.setTopPartitions(new ArrayList<>());
		}
		List<AhuPartition> topPartitions = params.getTopPartitions();

		// 立式机组,转轮,板式 顶底颠倒处理
		if (LayoutStyleEnum.VERTICAL_UNIT_1.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.VERTICAL_UNIT_2.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.DOUBLE_RETURN_1.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.DOUBLE_RETURN_2.style() == ahuLayout.getStyle()
				) {
			topPartitions = params.getBottomPartitions();
			bottomPartitions = params.getTopPartitions();
		}
		/**
		 * Frame
		 */
		//单层
		Map<String, Object> configMap = new LinkedHashMap<>();
		LinkedList<String> frameNames = new LinkedList<>();
		int frameNo = 1;
		if (params.isDoubleLayer()) {
			for (int i = 0; i < topPartitions.size(); i++, frameNo++) {
				AhuPartition ahuPartition = topPartitions.get(i);
				frameNames.add(UtilityConstant.SYS_CAD_FRAME + frameNo);
				framedPartitionMap.put(UtilityConstant.SYS_CAD_FRAME + frameNo, ahuPartition);
				LinkedList<Map<String, Object>> sections = new LinkedList<>();
				sections.addAll(ahuPartition.getSections());
				writableTopSections.addAll(sections);
			}
		}
		if (frameNames.isEmpty()) {
			configMap.put(UtilityConstant.SYS_CAD_FRAMENAME, StringUtils.EMPTY);
		} else {
			for (String frameName : frameNames) {
				sortFramedPartitionMap.put(frameName,framedPartitionMap.get(frameName));
			}
			configMap.put(UtilityConstant.SYS_CAD_FRAMENAME, frameNames.stream().collect(Collectors.joining(UtilityConstant.SYS_PUNCTUATION_PLUS)));
		}
		writeSection(UtilityConstant.SYS_CAD_BOTTOMFRAME, configMap, writer);

		//双层
		configMap.clear();
		frameNames.clear();
		if(LayoutStyleEnum.DOUBLE_RETURN_1.style() == ahuLayout.getStyle()
				||LayoutStyleEnum.DOUBLE_RETURN_2.style() == ahuLayout.getStyle()){
			for (int i = bottomPartitions.size()-1; i >=0; i--, frameNo++) {
				AhuPartition ahuPartition = bottomPartitions.get(i);
				frameNames.add(UtilityConstant.SYS_CAD_FRAME + frameNo);
				framedPartitionMap.put(UtilityConstant.SYS_CAD_FRAME + frameNo, ahuPartition);
				Collections.reverse(ahuPartition.getSections());
				writableBottomSections.addAll(ahuPartition.getSections());
			}
		}else {
		for (int i = 0; i < bottomPartitions.size(); i++, frameNo++) {
			AhuPartition ahuPartition = bottomPartitions.get(i);
			frameNames.add(UtilityConstant.SYS_CAD_FRAME + frameNo);
			framedPartitionMap.put(UtilityConstant.SYS_CAD_FRAME + frameNo, ahuPartition);
			writableBottomSections.addAll(ahuPartition.getSections());
		}
		}
		if (frameNames.isEmpty()) {
			configMap.put(UtilityConstant.SYS_CAD_FRAMENAME, StringUtils.EMPTY);
		} else {
			if (LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()
					|| LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()) {//板式、转轮 frames 反转
				Collections.reverse(frameNames);

				for (int i=0;i<frameNames.size();i++) {
					sortFramedPartitionMap.put(frameNames.get(frameNames.size()-1-i),framedPartitionMap.get(frameNames.get(i)));
				}

			}else{
				for (String frameName : frameNames) {
					sortFramedPartitionMap.put(frameName,framedPartitionMap.get(frameName));
				}
			}
			configMap.put(UtilityConstant.SYS_CAD_FRAMENAME, frameNames.stream().collect(Collectors.joining(UtilityConstant.SYS_PUNCTUATION_PLUS)));
		}
		writeSection(UtilityConstant.SYS_CAD_TOPFRAME, configMap, writer);

		/**
		 * Part
		 */
		String xilie = params.getSeries().substring(params.getSeries().length() - 4);
		int prePartLength = 1;
		for (Entry<String, AhuPartition> partitionEntry : sortFramedPartitionMap.entrySet()) {
			AhuPartition partition = partitionEntry.getValue();
			configMap.clear();
			LinkedList<Map<String, Object>> selections = partition.getSections();

			int frameLength = selections.size();

			// 热水盘管前面是冷水盘管，(frameLength --)
			SectionTypeEnum preSectionTypeEnum = null;
			Map<String, Object> preSectionMeta = null;
			SectionTypeEnum nextSectionTypeEnum = null;
			Map<String, Object> nextSectionMeta = null;

			for (int i = 0; i < selections.size(); i++) {
				Map<String, Object> sel = selections.get(i);
				Map<String, Object> nextSel = i<selections.size()-1?selections.get(i+1):null;
				String nextMetaId = (null  == nextSel?"":AhuPartition.getMetaIdOfSection(nextSel));
				nextSectionTypeEnum = "".equals(nextMetaId)?null:SectionTypeEnum.getSectionTypeFromId(nextMetaId);;

				String metaId = AhuPartition.getMetaIdOfSection(sel);
				if (null != preSectionTypeEnum && null != preSectionMeta
						&& preSectionTypeEnum == SectionTypeEnum.TYPE_COLD
						&& SectionTypeEnum.getSectionTypeFromId(metaId) == TYPE_HEATINGCOIL) {
					Map<String, Object> sectionMeta = JSON.parseObject(AhuPartition.getMetaJsonOfSection(sel), HashMap.class);

					//冷水法兰、连接方式
					boolean hasEliminator = true;
					String eliminator = String.valueOf(preSectionMeta.get(METASEXON_COOLINGCOIL_ELIMINATOR));
					if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator)) {
						hasEliminator = false;
					}
					boolean isFL = false;
					String connections = String.valueOf(preSectionMeta.get(METASEXON_COOLINGCOIL_CONNECTIONS));
					String connectionsHeat = String.valueOf(sectionMeta.get(METASEXON_HEATINGCOIL_CONNECTIONS));
					if (SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_PAIRSFLANGE.equals(connections)
							|| SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_PAIRSFLANGE.equals(connectionsHeat)) {
						isFL = true;
					}

					if(SystemCountUtil.ltBigUnit(Integer.parseInt(xilie)) && !hasEliminator && !isFL
							&& nextSectionTypeEnum != SectionTypeEnum.TYPE_WETFILMHUMIDIFIER){//冷+热+湿膜，冷+热不合并
						frameLength--;//小型号机组、挡水器、法兰,合并写入ini
					}
				}
				preSectionTypeEnum = SectionTypeEnum.getSectionTypeFromId(metaId);
				preSectionMeta = JSON.parseObject(AhuPartition.getMetaJsonOfSection(sel), HashMap.class);
			}

			// 湿膜加湿段前面是冷水盘管/热水盘管，(frameLength --)
			preSectionTypeEnum = null;
			for (Map<String, Object> sel : selections) {
				String metaId = AhuPartition.getMetaIdOfSection(sel);
				if (null != preSectionTypeEnum
						&& (preSectionTypeEnum == SectionTypeEnum.TYPE_COLD
						|| preSectionTypeEnum == TYPE_HEATINGCOIL)
						&& SectionTypeEnum.getSectionTypeFromId(metaId) == SectionTypeEnum.TYPE_WETFILMHUMIDIFIER) {
					frameLength--;
				}
				preSectionTypeEnum = SectionTypeEnum.getSectionTypeFromId(metaId);
			}
			// 高压喷雾加湿段前面是冷水盘管，(frameLength --)
			preSectionTypeEnum = null;
			for (Map<String, Object> sel : selections) {
				String metaId = AhuPartition.getMetaIdOfSection(sel);
				if (null != preSectionTypeEnum && (preSectionTypeEnum == SectionTypeEnum.TYPE_COLD)
						&& SectionTypeEnum.getSectionTypeFromId(metaId) == SectionTypeEnum.TYPE_SPRAYHUMIDIFIER) {
					frameLength--;
				}
				preSectionTypeEnum = SectionTypeEnum.getSectionTypeFromId(metaId);
			}
			configMap.put(UtilityConstant.SYS_CAD_PARTSNAME, IntStream.range(prePartLength, prePartLength + frameLength)
					.mapToObj(j -> UtilityConstant.SYS_CAD_PART + j).collect(Collectors.joining(UtilityConstant.SYS_PUNCTUATION_PLUS)));
			writeSection(partitionEntry.getKey(), configMap, writer);
			prePartLength = prePartLength + frameLength;
		}

		// 立式机组,转轮,板式，顶底颠倒
		if (LayoutStyleEnum.VERTICAL_UNIT_1.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.VERTICAL_UNIT_2.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.DOUBLE_RETURN_1.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.DOUBLE_RETURN_2.style() == ahuLayout.getStyle()) {
			writableSections.addAll(writableTopSections);

			// 转轮/板式底部都进行反转
			if(LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()
					||LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()){
				Collections.reverse(writableBottomSections);
			}

			if(LayoutStyleEnum.DOUBLE_RETURN_1.style() == ahuLayout.getStyle()
					|| LayoutStyleEnum.DOUBLE_RETURN_2.style() == ahuLayout.getStyle()) {
				retAirDirection(params,writableBottomSections);
			}
			writableSections.addAll(writableBottomSections);
		}else{// 非立式机组，顶部都进行反转
			writableSections.addAll(writableBottomSections);
			Collections.reverse(writableTopSections);
			writableSections.addAll(writableTopSections);
		}

		/**
		 * 具体Part信息
		 */
		writeParts(params, writableSections, writer);
		/**
		 * 描述信息
		 */
		writeMxb(params, writableSections, writer);
	}
	//双层转向1/2 重置顶层所有AirDirection 为R
	private static void retAirDirection(AhuParam ahuParam,LinkedList<Map<String,Object>> writableBottomSections) {
		for (Map<String, Object> writableBottomSection : writableBottomSections) {
			short position = AhuPartition.getPosOfSection(writableBottomSection);
			PartParam pp = getPartParam(position, ahuParam);
			pp.getParams().put(METASEXON_AIRDIRECTION, AirDirectionEnum.RETURNAIR.getCode());//都是用回风
		}
	}

	private static void writeParts(AhuParam ahuParam, LinkedList<Map<String, Object>> writableSelections,
								   PrintWriter writer) {
		boolean hasCombinedMixingChamber = hasCombinedMixingChamber(writableSelections);

		int i = 1;
		for (Map<String, Object> section : writableSelections) {
			short position = AhuPartition.getPosOfSection(section);
			PartParam pp = getPartParam(position, ahuParam);
			Map<String, Object> configMap = getSectionConfigMap(pp.getKey(), ahuParam, pp);
			if (EmptyUtil.isEmpty(configMap)) {
				logger.error("根据逻辑空map 不写入ini 文件， @CAD配置[段配置]时");
			} else {

				if(hasCombinedMixingChamber
						&&
                        (
                        SectionTypeEnum.TYPE_MIX.getId().equals(AhuPartition.getMetaIdOfSection(section))
                            || SectionTypeEnum.TYPE_SINGLE.getId().equals(AhuPartition.getMetaIdOfSection(section))
                        )
						&& configMap.containsKey(UtilityConstant.SYS_CAD_AIRDIRECTION)){
					configMap.put(UtilityConstant.SYS_CAD_AIRDIRECTION,"0");//混合段：新回排都是送风
				}

				writeSection(UtilityConstant.SYS_CAD_PART + i, configMap, writer);
				i++;
			}
		}
	}

	private static boolean hasCombinedMixingChamber(LinkedList<Map<String, Object>> writableSelections){
		for (Map<String, Object> writableSelection : writableSelections) {
			if(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(writableSelection.get("metaId"))){
				return true;
			}
		}
		return false;

	}

	private static PartParam getPartParam(short position, AhuParam ahuParam) {
		List<PartParam> partParams = ahuParam.getPartParams();
		for (PartParam partParam : partParams) {
			if (position == partParam.getPosition()) {
				return partParam;
			}
		}
		return null;
	}

	private static void writeMxb(AhuParam ahuParam, LinkedList<Map<String, Object>> writableSections,
								 PrintWriter writer) {
		Map<String, Object> configMap = new LinkedHashMap<>();

		if (!writableSections.isEmpty()) {
			configMap.put(UtilityConstant.SYS_CAD_COLUMNNAME, getIntlString(NAME) + UtilityConstant.SYS_PUNCTUATION_PLUS + getIntlString(PARAMETER));
			int row = 2;
            configMap.put(UtilityConstant.SYS_CAD_ROW + row++, format(mxbStr, getIntlString(VOLTAGE), MapValueUtils.getStringValue(UtilityConstant.METAHU_VOLTAGE, ahuParam.getParams())));
			for (Map<String, Object> section : writableSections) {
				short position = AhuPartition.getPosOfSection(section);
				PartParam pp = getPartParam(position, ahuParam);
				String partKey = pp.getKey();
				switch (SectionTypeEnum.getSectionTypeFromId(partKey)) {
					case TYPE_MIX: {
						continue;
					}
					case TYPE_FAN: {
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, getIntlString(FAN_SECTION));
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, format(mxbStr, getIntlString(AIR_VOLUME),
								MapValueUtils.getDoubleValue(UtilityConstant.METASEXON_FAN_AIRVOLUME, pp.getParams()), "m3/h"));
						//欧标total static pressure = ESP + ISP + System Sffect = ESP + 1.1* ISP
						//double tsp = MapValueUtils.getDoubleValue(UtilityConstant.METASEXON_FAN_EXTERNALSTATIC, pp.getParams())+MapValueUtils.getDoubleValue(UtilityConstant.METASEXON_FAN_SYSSTATIC, pp.getParams());
						//欧标total static pressure = totalStatic
						double tsp = MapValueUtils.getDoubleValue(UtilityConstant.METASEXON_FAN_TOTALSTATIC, pp.getParams());
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, format(mxbStr, getIntlString(FULL_STATIC_PRESSURE),
								Math.floor(tsp), "Pa"));
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, format(mxbStr, getIntlString(MOTOR_POWER),
								MapValueUtils.getDoubleValue(UtilityConstant.METASEXON_FAN_MOTORPOWER, pp.getParams()), "kW"));
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, format(mxbStr, getIntlString(FAN_MODEL),
								MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_FANMODEL, pp.getParams()), SYS_BLANK));
						continue;
					}
					case TYPE_COLD: {
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, getIntlString(COIL_SECTION));
						int r = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_COOLINGCOIL_ROWS, pp.getParams());
						int f = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_COOLINGCOIL_FINDENSITY, pp.getParams());
						String circuit = MapValueUtils.getStringValue(UtilityConstant.METASEXON_COOLINGCOIL_CIRCUIT, pp.getParams());
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++,
								format(mxbStr, getIntlString(COIL_MODEL), r + SYS_ALPHABET_R_UP + UtilityConstant.SYS_PUNCTUATION_SLASH + f + UtilityConstant.SYS_PUNCTUATION_SLASH + circuit, SYS_BLANK));

						Boolean summer = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_COOLINGCOIL_ENABLESUMMER, pp.getParams());
						Boolean winner = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_COOLINGCOIL_ENABLEWINTER, pp.getParams());
						boolean justWinner = false;
						if(!summer && winner){
							justWinner = true;
						}

						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, format(mxbStr, getIntlString(justWinner?HEATING_VOLUME:COOLING_VOLUME),
								MapValueUtils.getDoubleValue(
										justWinner?UtilityConstant.METASEXON_COOLINGCOIL_WRETURNHEATQ:UtilityConstant.METASEXON_COOLINGCOIL_SRETURNCOLDQ
										, pp.getParams()), "kW"));
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++,
								format(mxbStr, getIntlString(WATER_FLOW_VOLUME),
										MapValueUtils.getDoubleValue(
												justWinner?UtilityConstant.METASEXON_COOLINGCOIL_WRETURNWATERFLOW:UtilityConstant.METASEXON_COOLINGCOIL_SRETURNWATERFLOW
												, pp.getParams()),
										"m3/h"));
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, format(mxbStr, getIntlString(WATER_RESISTANCE),
								MapValueUtils.getDoubleValue(
										justWinner?UtilityConstant.METASEXON_COOLINGCOIL_WWATERRESISTANCE:UtilityConstant.METASEXON_COOLINGCOIL_SWATERRESISTANCE
										, pp.getParams()),
								"kPa"));


						continue;
					}
					case TYPE_HEATINGCOIL: {
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, getIntlString(COIL_SECTION));
						int r = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_HEATINGCOIL_ROWS, pp.getParams());
						int f = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_HEATINGCOIL_FINDENSITY, pp.getParams());
						String circuit = MapValueUtils.getStringValue(UtilityConstant.METASEXON_HEATINGCOIL_CIRCUIT, pp.getParams());
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++,
								format(mxbStr, getIntlString(COIL_MODEL), r + SYS_ALPHABET_R_UP + UtilityConstant.SYS_PUNCTUATION_SLASH + f + UtilityConstant.SYS_PUNCTUATION_SLASH + circuit, SYS_BLANK));
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, format(mxbStr, getIntlString(HEATING_VOLUME),
								MapValueUtils.getDoubleValue(UtilityConstant.METASEXON_HEATINGCOIL_WRETURNHEATQ, pp.getParams()), "kW"));
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++,
								format(mxbStr, getIntlString(WATER_FLOW_VOLUME),
										MapValueUtils.getDoubleValue(UtilityConstant.METASEXON_HEATINGCOIL_WRETURNWATERFLOW, pp.getParams()),
										"m3/h"));
						configMap.put(UtilityConstant.SYS_CAD_ROW + row++, format(mxbStr, getIntlString(WATER_RESISTANCE),
								MapValueUtils.getDoubleValue(UtilityConstant.METASEXON_HEATINGCOIL_WWATERRESISTANCE, pp.getParams()),
								"kPa"));

						continue;
					}
					default:
				}
			}
			configMap.put(UtilityConstant.SYS_CAD_RULE, "1");
			configMap.put(UtilityConstant.SYS_CAD_LENGTH, "60");
			StringBuffer allrow = new StringBuffer();
			for (int i = 2; i < row; i++) {
				allrow.append(UtilityConstant.SYS_PUNCTUATION_PLUS + UtilityConstant.SYS_CAD_ROW + i);
			}
			configMap.put(UtilityConstant.SYS_CAD_ALLROW, allrow.toString().substring(1));
		}
		writeSection(UtilityConstant.SYS_CAD_MXB, configMap, writer);
	}

	/**
	 * 生成CAD机组信息 [AHU]
	 *
	 * @param params
	 * @return
	 */
	private static final Map<String, Object> configMapAhu(AhuParam params) {
		Map<String, Object> configMap = new LinkedHashMap<String, Object>();
		String product = params.getProduct();
		AhuLayout ahuLayout = params.getLayout();
		//当前机组数据异常需要重新进入机组界面点击保存按钮。
		if (EmptyUtil.isEmpty(ahuLayout)) {
			throw new ApiException(ErrorCode.PARTITION_VALIDATE_LAYOUT_JSON_NULL_MSG);
		}
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, params.getParams());

		//三视图变形处理：暂时只处理型号相关信息，风阀等具体信息后期再处理。
		if(EmptyUtil.isNotEmpty(params.getPanelSeries()) && !params.getSeries().equals(params.getPanelSeries())) {
			serial = params.getPanelSeries();
		}

		configMap.put(UtilityConstant.SYS_CAD_PRODUCT, serial);

		String unitConfig = MapValueUtils.getStringValue(UtilityConstant.METAHU_UNITCONFIG, params.getParams());
		if (LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()) {//板式
			unitConfig = AHU_UNITCONFIG_PLATEHR;
		}else if (LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()) {//转轮
			unitConfig = AHU_UNITCONFIG_WHEELHR;
		}else if (LayoutStyleEnum.COMMON.style() == ahuLayout.getStyle()) {//卧式
			unitConfig = AHU_UNITCONFIG_HORIZONTAL;
		}else if (LayoutStyleEnum.VERTICAL_UNIT_1.style() == ahuLayout.getStyle()
				|| LayoutStyleEnum.VERTICAL_UNIT_2.style() == ahuLayout.getStyle()) {//立式
			unitConfig = AHU_UNITCONFIG_VERTICAL;
		}

		if (SystemCalculateConstants.AHU_UNITCONFIG_HORIZONTAL.equals(unitConfig)) {
			configMap.put(UtilityConstant.SYS_CAD_DIMSCALE, "1");
		} else {
			configMap.put(UtilityConstant.SYS_CAD_DIMSCALE, "1.3");
		}

		//语言版本 0，中文；1，英文
		if(LanguageEnum.Chinese == AHUContext.getLanguage()){
			configMap.put(UtilityConstant.SYS_CAD_ENGLISHTEMPLATE, 0);
		}else{
			configMap.put(UtilityConstant.SYS_CAD_ENGLISHTEMPLATE, 1);
		}
		/*
		 * 卧式-H', '立式-V', '热回收(双层)-R', '板换(双层)-T'
		 *      if FrmVarTUnit.UnitPro.UnitConfig='T' THEN            //modified  20140521
			       Stor.WriteString('UNITINFO', 'BOTTOMFRAMALIGNMODE','1')
			    ELSE
			       Stor.WriteString('UNITINFO', 'BOTTOMFRAMALIGNMODE','0');
		 */
		configMap.put(UtilityConstant.SYS_CAD_BOTTOMFRAMALIGNMODE, 0);//暂时只有卧式
		try {
			int lenth = serial.length();
			String heightStr = serial.substring(lenth - 4, lenth - 2);
			int height = Integer.parseInt(heightStr);
			configMap.put(UtilityConstant.SYS_CAD_UNITHEIGHT, height * 100);
			configMap.put(UtilityConstant.SYS_CAD_UNITHEIGHT+UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN+UtilityConstant.SYS_CAD_TOP, height * 100);

			if (SystemCountUtil.gtSmallUnitHeight(height)) {//大机组
				configMap.put(UtilityConstant.SYS_CAD_BASEHEIGHT, "200");
			} else {
				configMap.put(UtilityConstant.SYS_CAD_BASEHEIGHT, "100");
			}
		} catch (Exception e) {
			configMap.put(UtilityConstant.SYS_CAD_UNITHEIGHT, 0);
			configMap.put(UtilityConstant.SYS_CAD_UNITHEIGHT + UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN
					+ UtilityConstant.SYS_CAD_TOP, 0);
			logger.error("CAD UNITHEIGHT Error", e);
		}
		try {
			int lenth = serial.length();
			String weightStr = serial.substring(lenth - 2);
			int width = Integer.parseInt(weightStr);
			configMap.put(UtilityConstant.SYS_CAD_UNITWIDTH, width * 100);
		} catch (Exception e) {
			configMap.put(UtilityConstant.SYS_CAD_UNITWIDTH, 0);
			logger.error("CAD UNITWIDTH Error", e);
		}
		/*
		 * 卧式-H', '立式-V', '热回收(双层)-R', '板换(双层)-T'
		 *      if FrmVarTUnit.UnitPro.UnitConfig='T' THEN            //modified  20140521
			       Stor.WriteString('UNITINFO', 'BOTTOMFRAMALIGNMODE','1')
			    ELSE
			       Stor.WriteString('UNITINFO', 'BOTTOMFRAMALIGNMODE','0');
		 */
		configMap.put("UNITCONFIG", ValueFormatUtil.getShort(unitConfig).toUpperCase());
		if (SYS_ALPHABET_R_UP.equals(configMap.get("UNITCONFIG"))) {
			configMap.put("TOPFRAMEALIGNMODE", "M");
		} else if ("T".equals(configMap.get("UNITCONFIG"))) {
			configMap.put("TOPFRAMEALIGNMODE", "M");
		} else {
			configMap.put("TOPFRAMEALIGNMODE", SYS_ALPHABET_R_UP);
		}
		String pipeorientation = MapValueUtils.getStringValue(UtilityConstant.METAHU_PIPEORIENTATION, params.getParams());
		configMap.put("PIPEORIENTATION", ValueFormatUtil.getSimpleDirection(pipeorientation));

		String doororientation = MapValueUtils.getStringValue(UtilityConstant.METAHU_DOORORIENTATION, params.getParams());
		if (SystemCalculateConstants.DISCHARGE_DOORDIRECTION_LEFT.equals(doororientation)) {
			configMap.put("DOORDIRECTION", 1);
		} else if (SystemCalculateConstants.DISCHARGE_DOORDIRECTION_RIGHT.equals(doororientation)) {
			configMap.put("DOORDIRECTION", 2);
		}

		String isprerain = MapValueUtils.getStringValue(UtilityConstant.METAHU_ISPRERAIN, params.getParams());
		configMap.put("RAINTOP", ValueFormatUtil.getTorFInteger(isprerain));
		//获取分段信息
		Gson gson = new Gson();
		logger.info("UNIT ID:"+params.getUnitid()+" UNIT NAME:"+params.getName()+ " is generating CAD file." );
		List<PartitionObject> objPartitions = gson.fromJson(params.getPartition().getPartitionJson(),
				new TypeToken<List<PartitionObject>>() {
				}.getType());
		configMap.put("GROUPS", objPartitions.size());
		configMap.put("SECTIONCOUNT", params.getPartParams().size());
		configMap.put("MODULE", "100");
		String FRMTHICK = SYS_BLANK;
		if (UtilityConstant.SYS_UNIT_SERIES_39CBFI.equals(product) || UtilityConstant.SYS_UNIT_SERIES_39XT.equals(product)) {
			FRMTHICK = "75";
		} else if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
			FRMTHICK = "50";
		} else {
			FRMTHICK = "30";
		}
		configMap.put(UtilityConstant.SYS_CAD_FRMTHICK, FRMTHICK);
		String IDMDH4 = SYS_BLANK;
		if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
			IDMDH4 = "155";
		} else {
			IDMDH4 = "130";
		}
		configMap.put("IDMDH4", IDMDH4);
		String INTERVAL = SYS_BLANK;
		if ((UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(product))) {
			INTERVAL = "90";
		} else if ((UtilityConstant.SYS_UNIT_SERIES_39CBFI.equals(product))) {
			INTERVAL = "100";
		} else if ((UtilityConstant.SYS_UNIT_SERIES_39G.equals(product))) {
			INTERVAL = "50";
		} else if ((UtilityConstant.SYS_UNIT_SERIES_39XT.equals(product))) {
			INTERVAL = "104";
		} else {
			INTERVAL = "90";
		}
		configMap.put(UtilityConstant.SYS_CAD_INTERVAL, INTERVAL);
		String PANELTHICK = SYS_BLANK;
		if ((UtilityConstant.SYS_UNIT_SERIES_39G.equals(product))) {
			PANELTHICK = "25";
		} else {
			PANELTHICK = "50";
		}
		configMap.put("PANELTHICK", PANELTHICK);
		configMap.put("VOLTAGE", MapValueUtils.getStringValue(UtilityConstant.METAHU_VOLTAGE, params.getParams()));
		configMap.put("ID", params.getDrawingNo());
		configMap.put("AHUMODEL", serial);
		configMap.put("AHUMARKING", params.getName());
		configMap.put("PROJECTNAME", params.getProjectName());
		return configMap;
	}

	/**
	 * 生成CAD段信息 [混合段-001]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection001(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "1");
		StringBuffer MIXTYPE = new StringBuffer();
		boolean b5 = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_MIX_RETURNBUTTOM, partMap);
		boolean b3 = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_MIX_RETURNLEFT, partMap);
		boolean b4 = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_MIX_RETURNRIGHT, partMap);
		boolean b1 = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_MIX_RETURNTOP, partMap);
		boolean b2 = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_MIX_RETURNBACK, partMap);
		if (b1) {
			MIXTYPE.append("1");
		} else {
			MIXTYPE.append("0");
		}
		if (b2) {
			MIXTYPE.append("1");
		} else {
			MIXTYPE.append("0");
		}
		if (b3) {
			MIXTYPE.append("1");
		} else {
			MIXTYPE.append("0");
		}
		if (b4) {
			MIXTYPE.append("1");
		} else {
			MIXTYPE.append("0");
		}
		if (b5) {
			MIXTYPE.append("1");
		} else {
			MIXTYPE.append("0");
		}
		configMap.put("MIXTYPE", MIXTYPE.toString());
		String dampertype = MapValueUtils.getStringValue(UtilityConstant.METASEXON_MIX_DAMPEROUTLET, partMap);
		if (SystemCalculateConstants.MIX_DAMPEROUTLET_ED.equals(dampertype)) {
			configMap.put("DAMPERTYPE", "B");
		} else if (SystemCalculateConstants.MIX_DAMPEROUTLET_MD.equals(dampertype)) {
			configMap.put("DAMPERTYPE", "A");
		} else if (SystemCalculateConstants.MIX_DAMPEROUTLET_FD.equals(dampertype)) {
			configMap.put("DAMPERTYPE", "C");
		} else {
			configMap.put("DAMPERTYPE", SYS_BLANK);
		}
		int sectionl = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_MIX_SECTIONL, partMap);
		String doordirection = MapValueUtils.getStringValue(UtilityConstant.METASEXON_MIX_DOORDIRECTION, partMap);
		configMap.put("DOORDIRECTION", ValueFormatUtil.getSimpleDirectionInteger(doordirection));
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionl);
		String product = ahu.getProduct();
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		int iInterval = 100;
		if(serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39CBFI)!=-1){
			iInterval = IniConfigMapUtil.Interval_39CBFI;
		}else if (serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39G)!=-1){
			iInterval = IniConfigMapUtil.Interval_39G;
		}else if (serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39XT)!=-1){
			iInterval = IniConfigMapUtil.Interval_39XT;
		}else{
			iInterval = IniConfigMapUtil.Interval_Other;
		}
		if ((UtilityConstant.SYS_UNIT_SERIES_39G.equals(product))) {
			configMap.put("DOORTYPE", "2");
		} else if (("39CBF".equals(product))) {
			configMap.put("DOORTYPE", "1");
		} else if ((UtilityConstant.SYS_UNIT_SERIES_39XT.equals(product))) {
			configMap.put("DOORTYPE", "1");
		} else if ((UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(product))) {
			configMap.put("DOORTYPE", "1");
		}
		String dooro = MapValueUtils.getStringValue("meta.section.mix.DoorO", partMap);
		if (SystemCalculateConstants.MIX_DOORO_DOORNOVIEWPORT.equals(dooro)) {
			configMap.put("DOOR", "1");
			configMap.put("VIEWPORT", "0");
		} else if (SystemCalculateConstants.MIX_DOORO_DOORWITHVIEWPORT.equals(dooro)) {
			configMap.put("DOOR", "1");
			configMap.put("VIEWPORT", "1");
		} else if (SystemCalculateConstants.MIX_DOORO_NODOOR.equals(dooro)) {
			configMap.put("DOOR", "0");
			configMap.put("VIEWPORT", SYS_BLANK);
		} else {
			configMap.put("DOOR", SYS_BLANK);
			configMap.put("VIEWPORT", "0");
		}
		//TODO
		/*boolean PositivePressureDoor = MapValueUtils.getBooleanValue("meta.section.mix.PositivePressureDoor", partMap);
		setZym(configMap, PositivePressureDoor);*/

		boolean lamp = MapValueUtils.getBooleanValue("meta.section.mix.fixRepairLamp", partMap);
		configMap.put("LAMP", ValueFormatUtil.getTorFInteger(String.valueOf(lamp)));
		String doorDimension = AhuMetadata.findOne(PaneledDoor.class, serial, "1").getDoor();
		/*if (Integer.valueOf(doorDimension.substring(0, 3)) > (Integer.valueOf(sectionl) * 100
				- Integer.valueOf(String.valueOf(ahuConfigMap.get(UtilityConstant.SYS_CAD_FRMTHICK))))) {
			doorDimension = String.valueOf(Integer.valueOf(sectionl)*100-Integer.valueOf(String.valueOf(ahuConfigMap.get(UtilityConstant.SYS_CAD_FRMTHICK)))-2)+"*"+doorDimension.substring(doorDimension.length()-3,doorDimension.length());
		}*/
		configMap.put("DOORDIMENSION", doorDimension);
		Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));//暂时改为只是用段自身长度

		String mixtype = MIXTYPE.toString();
		int first1 = mixtype.indexOf("1") + 1;
		if (first1 > 0) {
			int second1 = mixtype.lastIndexOf("1") + 1;
			int lenth = serial.length();
			String heightStr = serial.substring(lenth - 4, lenth - 2);
			String weightStr = serial.substring(lenth - 2);
			int height = Integer.parseInt(heightStr);
			int width = Integer.parseInt(weightStr);
			DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class,
					serial.substring(0, serial.length() - 4), first1 + SYS_BLANK, 2 == first1?damper.getSectionL():(SYS_BLANK+sectionl));
			String metrial = MapValueUtils.getStringValue(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL, partMap);
			if (EmptyUtil.isEmpty(nsion)) {
				logger.error("查表未获取到DamperDimension @第一风阀计算时");
			} else {
				if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(metrial)) {
					double c1 = Double.parseDouble(nsion.getC1());
					double b = Double.parseDouble(nsion.getB());
					double d1 = Double.parseDouble(nsion.getD1());
					configMap.put("DIMENSION_A", c1 - 2 * 40);
					configMap.put("DIMENSION_B", 40 + b - d1);
				} else {
					configMap.put("DIMENSION_A", Double.parseDouble(nsion.getA()));
					configMap.put("DIMENSION_B", Double.parseDouble(nsion.getB()));
				}

				//第一个风阀为后出风，第二个风阀为底出风，第一个风阀需要靠上处理DIMENSION_B
				if(2 == first1 && second1 == 5){
					configMap.put("DIMENSION_B", String.valueOf(height*100+iInterval-Double.parseDouble(nsion.getA())-Double.parseDouble(nsion.getB())));
				}


				configMap.put("DIMENSION_D", IniConfigMapUtil.getDefaultDIMENSION_D(product));
				int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);

				if (first1 == 1 || first1 == 2) {
					configMap.put("DIMENSION_C", (width - 1) * 100 + DIMENSION_C_calPara - 72);
				}
				if (first1 == 3 || first1 == 4) {
					configMap.put("DIMENSION_C", (height - 1) * 100 + DIMENSION_C_calPara - 72);
				}
				if (first1 == 5) {
					int c = (width - 5) * 100 + DIMENSION_C_calPara - 72;
					configMap.put("DIMENSION_C", c);
					int DIMENSION_D_calPara = IniConfigMapUtil.getDIMENSION_D_calPara(product);
					configMap.put("DIMENSION_D", (width * 100 + DIMENSION_D_calPara - c) / 2);
				}
			}
			if (first1 < second1) {
				configMap.put("DIMENSION_A_2", SYS_BLANK);
				configMap.put("DIMENSION_B_2", SYS_BLANK);
				configMap.put("DIMENSION_C_2", SYS_BLANK);
				configMap.put("DIMENSION_D_2", SYS_BLANK);
				DamperDimension nsion2 = AhuMetadata.findOne(DamperDimension.class,
						serial.substring(0, serial.length() - 4), second1 + SYS_BLANK, 2 == second1?damper.getSectionL():(SYS_BLANK+sectionl));
				if (EmptyUtil.isEmpty(nsion2)) {
					logger.error("查表未获取到DamperDimension @第二风阀计算时");
				} else {

					if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(metrial)) {
						double c1 = Double.parseDouble(nsion2.getC1());
						double b = Double.parseDouble(nsion2.getB());
						double d1 = Double.parseDouble(nsion2.getD1());
						configMap.put("DIMENSION_A_2", c1 - 2 * 40);
						configMap.put("DIMENSION_B_2", 40 + b - d1);
					} else {
						configMap.put("DIMENSION_A_2", Double.parseDouble(nsion2.getA()));
						configMap.put("DIMENSION_B_2", Double.parseDouble(nsion2.getB()));
					}

					configMap.put("DIMENSION_D_2", IniConfigMapUtil.getDefaultDIMENSION_D(product));
					int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);

					if (second1 == 1 || second1 == 2) {
						configMap.put("DIMENSION_C_2", (width - 1) * 100 + DIMENSION_C_calPara - 72);
					}
					if (second1 == 3 || second1 == 4) {
						configMap.put("DIMENSION_C_2", (height - 1) * 100 + DIMENSION_C_calPara - 72);
					}
					if (second1 == 5) {
						int c = (width - 5) * 100 + DIMENSION_C_calPara - 72;
						configMap.put("DIMENSION_C_2", c);
						int DIMENSION_D_calPara = IniConfigMapUtil.getDIMENSION_D_calPara(product);
						configMap.put("DIMENSION_D_2", (width * 100 + DIMENSION_D_calPara - c) / 2);
					}
				}
			}
		}

		if ((UtilityConstant.SYS_UNIT_SERIES_39CBFI.equals(product))) {
			configMap.put("DIMENSION_E", 158);
		} else {
			if (SystemCalculateConstants.MIX_DAMPEROUTLET_ED.equals(dampertype)
					|| SystemCalculateConstants.MIX_DAMPEROUTLET_MD.equals(dampertype)) {
				String metrial = MapValueUtils.getStringValue(UtilityConstant.METASEXON_MIX_DAMPERMETERIAL, partMap);
				if (SystemCalculateConstants.MIX_DAMPERMETERIAL_GI.equals(metrial)) {
					if (("39CBF".equals(product))) {
						configMap.put("DIMENSION_E", 160);
					} else if ((UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(product))) {
						configMap.put("DIMENSION_E", 160);
					} else if ((UtilityConstant.SYS_UNIT_SERIES_39G.equals(product))) {
						configMap.put("DIMENSION_E", 155);
					} else if ((UtilityConstant.SYS_UNIT_SERIES_39XT.equals(product))) {
						configMap.put("DIMENSION_E", 130);
					}
				}
				if (SystemCalculateConstants.MIX_DAMPERMETERIAL_AL.equals(metrial)) {
					if (("39CBF".equals(product))) {
						configMap.put("DIMENSION_E", 105);
					} else if ((UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(product))) {
						configMap.put("DIMENSION_E", 105);
					} else if ((UtilityConstant.SYS_UNIT_SERIES_39G.equals(product))) {
						configMap.put("DIMENSION_E", 100);
					} else if ((UtilityConstant.SYS_UNIT_SERIES_39XT.equals(product))) {
						configMap.put("DIMENSION_E", 75);
					}
				}
			} else if ("FD".equals(dampertype)) {
				if (("39CBF".equals(product))) {
					configMap.put("DIMENSION_E", 80);
				} else if ((UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(product))) {
					configMap.put("DIMENSION_E", 80);
				} else if ((UtilityConstant.SYS_UNIT_SERIES_39G.equals(product))) {
					configMap.put("DIMENSION_E", 75);
				} else if ((UtilityConstant.SYS_UNIT_SERIES_39XT.equals(product))) {
					configMap.put("DIMENSION_E", 50);
				}
			} else {
				configMap.put("DIMENSION_E", SYS_BLANK);
			}
		}
		if (SystemCalculateConstants.MIX_DAMPEROUTLET_ED.equals(dampertype)
				|| SystemCalculateConstants.MIX_DAMPEROUTLET_MD.equals(dampertype)) {
			configMap.put("FILEPATH_TOP", "D:\\FZ_V.DXF");
			configMap.put("FILEPATH_FRONT", "D:\\FC.DXF");
		}
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_top");
		configMap.put("DOORDRAWING_FRONT", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_front");

		toSetPartAirDirection(partMap, configMap);

		boolean meta_section_mix_freshAirHood = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_MIX_FRESHAIRHOOD, partMap);
		configMap.put("AIRHOOD", meta_section_mix_freshAirHood?"1":"0");//新风罩
		boolean ispreRain = MapValueUtils.getBooleanValue(UtilityConstant.METAHU_ISPRERAIN, ahuMap);
		configMap.put("RAINTOP", ispreRain?"1":"0");//安装顶棚

		configMap.put("AIRHOODDRAWING_TOP", "AIRHOOD_TOP.DWG");
		configMap.put("AIRHOODDRAWING_FRONT", "AIRHOOD_FRONT.DWG");
		DamperDimensionFac dfac = AhuMetadata.findOne(DamperDimensionFac.class,
				UtilityConstant.SYS_UNIT_SERIES_39G + serial.substring(serial.length() - 4));
		if (null != dfac) {
			configMap.put("AIRHOOD_H", dfac.getH());
		}else{
			configMap.put("AIRHOOD_H", "0");
		}
		configMap.put("PARTDETAIL", "MIX");

		//TODO 暂时屏蔽风阀自定义
		//updateMixDamperSize(MIXTYPE.toString(), partMap, configMap);

		return configMap;
	}

	private static void toSetPartAirDirection(Map<String, Object> partMap, Map<String, Object> configMap) {
		int airdirection = 0;//送风
		String airDirection = MapValueUtils.getStringValue(METASEXON_AIRDIRECTION, partMap);
		if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)) {//回风机组
			airdirection = 1;
		}
		configMap.put(UtilityConstant.SYS_CAD_AIRDIRECTION, airdirection);
	}


	/** 根据风阀调整后的尺寸更新混合段段三视图配置数据  */
	private static final void updateMixDamperSize(String MIXTYPE, Map<String, Object> partMap,
												  Map<String, Object> configMap) {
		List<DamperPosEnum> damperPoses = getMixDamperPos(MIXTYPE);
		// 第一个风阀
		if (damperPoses.size() > 0) {
			DamperPosEnum damperPos = damperPoses.get(0);
			updateMixDimension("DIMENSION_A", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_A.apply(damperPos));
			updateMixDimension("DIMENSION_B", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_B.apply(damperPos));
			updateMixDimension("DIMENSION_C", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_C.apply(damperPos));
			updateMixDimension("DIMENSION_D", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_D.apply(damperPos));
		} else {
			log.warn("unknown outlet direction of mix: {}", MIXTYPE);
		}

		// 第二个风阀
		if (damperPoses.size() > 1) {
			DamperPosEnum damperPos = damperPoses.get(1);
			updateMixDimension("DIMENSION_A_2", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_A.apply(damperPos));
			updateMixDimension("DIMENSION_B_2", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_B.apply(damperPos));
			updateMixDimension("DIMENSION_C_2", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_C.apply(damperPos));
			updateMixDimension("DIMENSION_D_2", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_D.apply(damperPos));
		}
	}

	private static List<DamperPosEnum> getMixDamperPos(String MIXTYPE) {
		List<DamperPosEnum> damperPoses = Lists.newArrayList();
		if (CommonConstant.SYS_STRING_NUMBER_1.equals(String.valueOf(MIXTYPE.charAt(0)))) {
			damperPoses.add(DamperPosEnum.Top);
		}
		if (CommonConstant.SYS_STRING_NUMBER_1.equals(String.valueOf(MIXTYPE.charAt(1)))) {
			damperPoses.add(DamperPosEnum.Back);
		}
		if (CommonConstant.SYS_STRING_NUMBER_1.equals(String.valueOf(MIXTYPE.charAt(2)))) {
			damperPoses.add(DamperPosEnum.Left);
		}
		if (CommonConstant.SYS_STRING_NUMBER_1.equals(String.valueOf(MIXTYPE.charAt(3)))) {
			damperPoses.add(DamperPosEnum.Right);
		}
		if (CommonConstant.SYS_STRING_NUMBER_1.equals(String.valueOf(MIXTYPE.charAt(4)))) {
			damperPoses.add(DamperPosEnum.Bottom);
		}
		return damperPoses;
	}

	private static void updateMixDimension(String dimension, Map<String, Object> configMap,
										   Map<String, Object> partMap, String metaKey) {
		String posSize = MapValueUtils.getStringValue(
				SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, metaKey), partMap);
		if (posSize != null) {
			log.debug("update {} of mix after adjustment: {}", dimension, posSize);
			configMap.put(dimension, posSize);
		}
	}

	/**
	 * 设置正压门
	 * @param configMap
	 * @param positivePressureDoor
	 */
	private static void setZym(Map<String, Object> configMap, boolean positivePressureDoor) {
		/*如果不是正压门 DOORDRAWING_TOP=39CQ_Door_top
				DOORDRAWING_FRONT=39CQ_Door_front
		三视图的配置  如果是正压门：DOORDRAWING_TOP=XT_DOOR_TOP
		DOORDRAWING_FRONT=XT_DOOR_FRONT*/
		if(positivePressureDoor){
			configMap.put("DOORDRAWING_TOP", "XT_DOOR_TOP");
			configMap.put("DOORDRAWING_FRONT", "XT_DOOR_FRONT");
		}else{
			configMap.put("DOORDRAWING_TOP", "39CQ_Door_top");
			configMap.put("DOORDRAWING_FRONT", "39CQ_Door_front");
		}
	}

	/**
	 * 生成CAD段信息 [单层过滤段-002]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection002(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		String product = ahu.getProduct();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "2");
		String filterF = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FILTER_FITETF, partMap);
		if (SystemCalculateConstants.FILTER_FITETF_B.equals(filterF)) {
			configMap.put("DRAWOPTION", "1");
			configMap.put("FLTERTYPE", "1");
		} else if (SystemCalculateConstants.FILTER_FITETF_P.equals(filterF)) {
			configMap.put("DRAWOPTION", "2");
			configMap.put("FLTERTYPE", "2");
		} else if (SystemCalculateConstants.FILTER_FITETF_X.equals(filterF)) {
			configMap.put("DRAWOPTION", "3");
			configMap.put("FLTERTYPE", "3");
		}
		int sectionl = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_FILTER_SECTIONL, partMap);
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionl);
		String pressureGuage = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FILTER_PRESSUREGUAGE, partMap);
		if (SystemCalculateConstants.FILTER_PRESSUREGUAGE_W_O.equals(pressureGuage)) {
			configMap.put("PRESSUREGAGE", "0");
		} else {
			configMap.put("PRESSUREGAGE", "1");
		}
		int rimThickness = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_FILTER_RIMTHICKNESS, partMap);
		if (SystemCalculateConstants.FILTER_FITETF_B.equals(filterF)
				|| SystemCalculateConstants.FILTER_FITETF_P.equals(filterF)) {
			// 段长*100-50
			configMap.put("FILTERTHICKNESSX", sectionl * 100 - 50);
		}else if (SystemCalculateConstants.FILTER_FITETF_X.equals(filterF)) {//6.4.0代码逻辑采用，老系统6.4.2版本逻辑
			if (rimThickness == 2 || rimThickness == 25) {
				configMap.put("FILTERTHICKNESSX", "98");
			}
			if (rimThickness == 1 || rimThickness == 50) {
				configMap.put("FILTERTHICKNESSX", "68");
			}
		}
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);

		String filterEfficiency = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FILTER_FILTEREFFICIENCY, partMap);
		String format = SYS_BLANK;
		if (SystemCalculateConstants.FILTER_FILTEREFFICIENCY_F7.equals(filterEfficiency)
				|| SystemCalculateConstants.FILTER_FILTEREFFICIENCY_F8.equals(filterEfficiency)
				|| SystemCalculateConstants.FILTER_FILTEREFFICIENCY_F9.equals(filterEfficiency)) {
			format = "H";
		} else {
			format = MapValueUtils.getStringValue("FLTERTYPE", configMap);
		}
		FilterDrawing filterdrawing = AhuMetadata.findOne(FilterDrawing.class, serial, format);
		if (EmptyUtil.isEmpty(filterdrawing)) {
			logger.error("查表未获取到FilterDrawing @ 过滤段计算时");
		} else {
			configMap.put("FLTFRONT_DIMENSION", filterdrawing.getFrontDimension());
			configMap.put("FLTFRONT_DRAWING", filterdrawing.getFrontDrawing());
			configMap.put("FLTTOP_DRAWING", filterdrawing.getTopDrawing());
			configMap.put("FLTTOP_DIMENSION", filterdrawing.getTopDimension());
		}
		if (rimThickness == 1 || rimThickness == 50) {
			if (product.equals(UtilityConstant.SYS_UNIT_SERIES_39G) || product.equals("39CBF") || product.equals(UtilityConstant.SYS_UNIT_SERIES_39CQ)){
				configMap.put("DIMESION_FT", 123-25);
			}else
				configMap.put("DIMESION_FT", 148-50);
		}
		if (rimThickness == 2 || rimThickness == 25) {
			if (product.equals(UtilityConstant.SYS_UNIT_SERIES_39G) || product.equals("39CBF") || product.equals(UtilityConstant.SYS_UNIT_SERIES_39CQ)){
				configMap.put("DIMESION_FT", 93-25);
			}else
				configMap.put("DIMESION_FT", 118-50);
		}

		if (SystemCalculateConstants.FILTER_FITETF_B.equals(filterF)) {
			configMap.put("STARTPOINT", "0");
		}else if (SystemCalculateConstants.FILTER_FITETF_X.equals(filterF)) {
			configMap.put("STARTPOINT", "-1");
		} else {
			if (rimThickness == 1 || rimThickness == 50) {
				configMap.put("STARTPOINT", "-25");
			}

			if (rimThickness == 2 || rimThickness == 25) {
				configMap.put("STARTPOINT", "-50");
			}
		}

		if(SystemCalculateConstants.FILTER_FITETF_X.equals(filterF)){
			configMap.put("FILEPATH_TOP", "D:\\WCG11.DXF");
			configMap.put("FILEPATH_FRONT", "D:\\WCG11.DXF");
			configMap.put("FILEPATH_SIDE", "D:\\WCG12.DXF");
		}

		toSetPartAirDirection(partMap, configMap);

		if (SystemCalculateConstants.FILTER_FITETF_X.equals(filterF)) {
			configMap.put("PARTDETAIL", SYS_BLANK);
			configMap.put("DOOR", "0");
		} else {
			configMap.put("DOOR", "1");
			configMap.put("PARTDETAIL", "FILTER");
		}

		String mediaLoading = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FILTER_MEDIALOADING, partMap);
		//袋式，侧抽才有安装门
		if ((SystemCalculateConstants.FILTER_FITETF_P.equals(filterF) || SystemCalculateConstants.FILTER_FITETF_B.equals(filterF)) && SystemCalculateConstants.FILTER_FITETF_SIDELOADING.equals(mediaLoading)) {
			configMap.put("DOOR", "1");
		} else {
			configMap.put("DOOR", "0");
		}

		String doordirection = MapValueUtils.getStringValue("DOORDIRECTION", ahuConfigMap);
		configMap.put("DOORDIRECTION", doordirection);
		configMap.put("DOORTYPE", "2");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "0");
		int iInterval = 100;
		if(serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39CBFI)!=-1){
			iInterval = IniConfigMapUtil.Interval_39CBFI;
		}else if (serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39G)!=-1){
			iInterval = IniConfigMapUtil.Interval_39G;
		}else if (serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39XT)!=-1){
			iInterval = IniConfigMapUtil.Interval_39XT;
		}else{
			iInterval = IniConfigMapUtil.Interval_Other;
		}
		int lenth = serial.length();
		String heightStr = serial.substring(lenth - 4, lenth - 2);
		int height = Integer.parseInt(heightStr);

		String dimensionA ="428*"+(height*100+iInterval-72);
		configMap.put("DOORDIMENSION", dimensionA);
		configMap.put("DOORHANDLEDIMENSION", "22*158");

		//单层段定死：39G
		configMap.put("DOORDRAWING_TOP", "39G_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39G_Door_front");

		return configMap;
	}

	/**
	 * 生成CAD段信息 [综合过滤段-003]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection003(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		String product = ahu.getProduct();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "3");
		int sectionL = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_COMBINEDFILTER_SECTIONL, partMap);
		String mediaLoading = MapValueUtils.getStringValue(UtilityConstant.METASEXON_COMBINEDFILTER_METIALOADING, partMap);
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionL);
		String rimThick = MapValueUtils.getStringValue(UtilityConstant.METASEXON_COMBINEDFILTER_RIMTHICK, partMap);
		if (SystemCalculateConstants.COMBINEDFILTER_RIMTHICK_25_50.equals(rimThick)) {
			configMap.put("FILTERTHICKNESSX_P", "25");
		} else {
			configMap.put("FILTERTHICKNESSX_P", "50");
		}
		configMap.put("FILTERTHICKNESSX", sectionL * 100 - 50);
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		FilterDrawing filterdrawing = AhuMetadata.findOne(FilterDrawing.class, serial, "1");
		if (EmptyUtil.isEmpty(filterdrawing)) {
			logger.error("查表未获取到FilterDrawing @综合过滤段计算时");
		} else {
			configMap.put("FLTFRONT_DIMENSION", filterdrawing.getFrontDimension());
			configMap.put("FLTFRONT_DRAWING", filterdrawing.getFrontDrawing());
			configMap.put("FLTTOP_DRAWING", filterdrawing.getTopDrawing());
			configMap.put("FLTTOP_DIMENSION", filterdrawing.getTopDimension());

		}
		FilterDrawing filterdrawing2 = AhuMetadata.findOne(FilterDrawing.class, serial, "2");
		if (EmptyUtil.isEmpty(filterdrawing2)) {
			logger.error("查表未获取到FilterDrawing 2 @综合过滤段计算时");
		} else {
			configMap.put("FLTFRONT_DIMENSION_P", filterdrawing2.getFrontDimension());
			configMap.put("FLTFRONT_DRAWING_P", filterdrawing2.getFrontDrawing());
			configMap.put("FLTTOP_DRAWING_P", filterdrawing2.getTopDrawing());
			configMap.put("FLTTOP_DIMENSION_P", filterdrawing2.getTopDimension());

		}
		toSetPartAirDirection(partMap, configMap);

		configMap.put("PARTDETAIL", "PB FILTER");
		configMap.put("DOOR", "1");
		//过滤器抽取方式，侧抽才有安装门
		if (SystemCalculateConstants.FILTER_FITETF_SIDELOADING.equals(mediaLoading)) {
			configMap.put("DOOR", "1");
		} else {
			configMap.put("DOOR", "0");
		}
		String doordirection = MapValueUtils.getStringValue("DOORDIRECTION", ahuConfigMap);
		configMap.put("DOORDIRECTION", doordirection);
		configMap.put("DOORTYPE", "2");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "0");

		int iInterval = 100;
		if(serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39CBFI)!=-1){
			iInterval = IniConfigMapUtil.Interval_39CBFI;
		}else if (serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39G)!=-1){
			iInterval = IniConfigMapUtil.Interval_39G;
		}else if (serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39XT)!=-1){
			iInterval = IniConfigMapUtil.Interval_39XT;
		}else{
			iInterval = IniConfigMapUtil.Interval_Other;
		}
		int lenth = serial.length();
		String heightStr = serial.substring(lenth - 4, lenth - 2);
		int height = Integer.parseInt(heightStr);
		String dimensionA ="528*"+(height*100+iInterval-72);
		configMap.put("DOORDIMENSION", dimensionA);

		configMap.put("DOORHANDLEDIMENSION", "22*158");

		//综合段定死：39G
		configMap.put("DOORDRAWING_TOP", "39G_Door_top");
		configMap.put("DOORDRAWING_FRONT", "39G_Door_front");
		return configMap;
	}

	/**
	 * 生成CAD段信息 [冷水盘管段-004&直接蒸发式盘管段-018]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection004(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		String paryKey = "coolingCoil";
		if(part.getKey().contains("directExpensionCoil")){
			paryKey = "directExpensionCoil";
		}
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "4");
		String product = ahu.getProduct();
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		int len = serial.length();
		String xilie = serial.substring(len - 4);
		if (SystemCountUtil.gteBigUnit(Integer.parseInt(xilie))) {
			configMap.put("BIGTYPE", "1");
		} else {
			configMap.put("BIGTYPE", "0");
		}
		if (xilie.compareTo("3438") > -1) {
			configMap.put("DUALHEADER", "1");
		} else {
			configMap.put("DUALHEADER", "0");
		}
		configMap.put("DIMEN", "1");
		String eliminator = MapValueUtils.getStringValue(UtilityConstant.METASEXON_PREFIX+paryKey+".eliminator", partMap);
		if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH.equals(eliminator)) {
			configMap.put("BREAKWATERTYPE", "D");
		} else if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_PLASTIC.equals(eliminator)) {
			configMap.put("BREAKWATERTYPE", "P");
		} else if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_STAINLESS.equals(eliminator)) {
			configMap.put("BREAKWATERTYPE", "S");
		} else if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_GISTEEL.equals(eliminator)) {// 镀锌板为空即可
			configMap.put("BREAKWATERTYPE", SYS_BLANK);
		} else if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALUMINUM.equals(eliminator)) {// 铝合金为空即可
			configMap.put("BREAKWATERTYPE", SYS_BLANK);
		} else if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator)) {
			configMap.put("BREAKWATERTYPE", "A");
		}
		int sectionL = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_PREFIX+paryKey+".sectionL", partMap);
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionL);
		configMap.put("DSQPATH", "dsq");
		configMap.put("DSQTHICKNESS", "50");
		configMap.put("BWBHEIGHT", "10");
		configMap.put("DRAINPANHEIGHT", "50");
		int ahu17 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_FRMTHICK, ahuConfigMap);
		int ahu19 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_INTERVAL, ahuConfigMap);
		// 段长*100+(机组通用配置第19项)-2*(机组通用配置第17项)
		int drainpanwidth = sectionL * 100 + ahu19 - 2 * ahu17;
		configMap.put("DRAINPANWIDTH", drainpanwidth);
		configMap.put("HEADERLENGTH", "100");
		configMap.put("DRAINPANPATH", "shuipan");
		if (SystemCountUtil.gteBigUnit(Integer.parseInt(xilie))) {
			CoilBigTuojiaDimension nsion = AhuMetadata.findOne(CoilBigTuojiaDimension.class, serial);
			if (EmptyUtil.isEmpty(nsion)) {
				logger.error("查表未获取到CoilBigTJDimension @冷水盘管段托架计算时");
				configMap.put("TUOJIA1_L", "0");
				configMap.put("TUOJIA1_H", "0");
				configMap.put("TUOJIA2_L", "0");
				configMap.put("TUOJIA2_H", "0");
			} else {
				configMap.put("TUOJIA1_L", 471);
				configMap.put("TUOJIA1_H", nsion.getB());
				int w = Integer.parseInt(nsion.getW());
				configMap.put("TUOJIA2_L", w - 471);
				configMap.put("TUOJIA2_H", nsion.getG());
			}
			//if (xilie.compareTo("4444") > 0) {
			//	configMap.put("TUOJIA2_H", "1");
			//}
			configMap.put("TUOJIA1DRAWING", "tuojia1");
			configMap.put("TUOJIA2DRAWING", "tuojia2");
		}
		int unitwidth = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_UNITWIDTH, ahuConfigMap);
		int baseheight = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_BASEHEIGHT, ahuConfigMap);
		// 机组宽度*100+(机组通用配置第19项)- 2*(机组通用配置第17项)
		int coilwidth = unitwidth + ahu19 - 2 * ahu17;
		int rows = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_PREFIX+paryKey+".rows", partMap);
		if (SystemCountUtil.gteBigUnit(Integer.parseInt(xilie))) {
			CoilBigDimension nsioLnew = AhuMetadata.findOne(CoilBigDimensionL.class, serial);
			if (EmptyUtil.isEmpty(nsioLnew)) {
				logger.error("查表未获取到CoilBigDimensionLNew @冷水盘管段计算时");
			} else {
				configMap.put("FB", Integer.parseInt(nsioLnew.getB())-baseheight);
				configMap.put("FC", nsioLnew.getC());
				configMap.put("FG", nsioLnew.getG());
				if (rows == 4) {
					configMap.put("FD", nsioLnew.getD4r());
					configMap.put("FE", nsioLnew.getE4r());
				}
				if (rows == 6) {
					configMap.put("FD", nsioLnew.getD6r());
					configMap.put("FE", nsioLnew.getE6r());
				}
				if (rows == 8) {
					configMap.put("FD", nsioLnew.getD8r());
					configMap.put("FE", nsioLnew.getE8r());
				}
				configMap.put("COLITHICKNESS", nsioLnew.getCoilw());
				configMap.put("COILHEIGHT", nsioLnew.getCoilh());
				configMap.put("COILWIDTH", coilwidth);
				configMap.put("SDIA", nsioLnew.getDia());
			}

			CoilBigDimension nsioRnew = AhuMetadata.findOne(CoilBigDimensionR.class, serial);
			if (EmptyUtil.isEmpty(nsioRnew)) {
				logger.error("查表未获取到CoilBigDimensionRNew @冷水盘管段计算时");
			} else {
				configMap.put("FB_R", nsioRnew.getB());
				configMap.put("FC_R", nsioRnew.getC());
				configMap.put("FG_R", nsioRnew.getG());
				if (rows == 4) {
					configMap.put("FD_R", nsioRnew.getD4r());
					configMap.put("FE_R", nsioRnew.getE4r());
				}
				if (rows == 6) {
					configMap.put("FD_R", nsioRnew.getD6r());
					configMap.put("FE_R", nsioRnew.getE6r());
				}
				if (rows == 8) {
					configMap.put("FD_R", nsioRnew.getD8r());
					configMap.put("FE_R", nsioRnew.getE8r());
				}
				configMap.put("COLITHICKNESS_R", nsioRnew.getCoilw());
				configMap.put("COILHEIGHT_R", nsioRnew.getCoilh());
				configMap.put("COILWIDTH_R", coilwidth);
				configMap.put("SDIA_R", nsioRnew.getDia());
			}
		} else {
			CoilDimension nsion = AhuMetadata.findOne(CoilDimension.class, serial);
			if (EmptyUtil.isEmpty(nsion)) {
				logger.error("查表未获取到CoilBigDimensionlNew @冷水盘管段计算时");
			} else {
				String coilw = SYS_BLANK;
				String coilh = nsion.getHeight();
				if (rows == 1) {
					coilw = nsion.getR1();
				} else if (rows == 2) {
					coilw = nsion.getR2();
				} else if (rows == 3) {
					coilw = nsion.getR3();
				} else if (rows == 4) {
					coilw = nsion.getR4();
				} else if (rows == 5) {
					coilw = nsion.getR5();
				} else if (rows == 6) {
					coilw = nsion.getR6();
				} else if (rows == 7) {
					coilw = nsion.getR7();
				} else if (rows == 8) {
					coilw = nsion.getR8();
				} else if (rows == 10) {
					coilw = nsion.getR10();
				} else if (rows == 12) {
					coilw = nsion.getR12();
				}
				configMap.put("COLITHICKNESS", coilw);
				configMap.put("COILHEIGHT", coilh);
				configMap.put("COILWIDTH", coilwidth);
				if (rows > 2) {
					ColdCoilDimension ccnsion = AhuMetadata.findOne(ColdCoilDimension.class, serial);
					if (EmptyUtil.isEmpty(ccnsion)) {
						logger.error("查表未获取到ColdCoilDimensionNew @冷水盘管段计算时");
					} else {
						configMap.put("FB", Integer.parseInt(ccnsion.getB()) + 25 - baseheight);
						if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
							configMap.put("FB", Integer.parseInt(ccnsion.getB())-baseheight);
						}
						int b = Integer.parseInt(ccnsion.getB());
						configMap.put("FB", b + 25 - baseheight);
						if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
							configMap.put("FB", b - baseheight);
						}
						configMap.put("FC", ccnsion.getC());
						if (rows == 3) {
							configMap.put("FD", ccnsion.getRd3());
							configMap.put("FE", ccnsion.getRe3());
						} else if (rows == 4) {
							configMap.put("FD", ccnsion.getRd4());
							configMap.put("FE", ccnsion.getRe4());
						} else if (rows == 5) {
							configMap.put("FD", ccnsion.getRd5());
							configMap.put("FE", ccnsion.getRe5());
						} else if (rows == 6) {
							configMap.put("FD", ccnsion.getRd6());
							configMap.put("FE", ccnsion.getRe6());
						} else if (rows == 7) {
							configMap.put("FD", ccnsion.getRd7());
							configMap.put("FE", ccnsion.getRe7());
						} else if (rows == 8) {
							configMap.put("FD", ccnsion.getRd8());
							configMap.put("FE", ccnsion.getRe8());
						} else if (rows == 10) {
							configMap.put("FD", ccnsion.getRd10());
							configMap.put("FE", ccnsion.getRe10());
						} else if (rows == 12) {
							configMap.put("FD", ccnsion.getRd12());
							configMap.put("FE", ccnsion.getRe12());
						}
						configMap.put("FG", ccnsion.getG());
						configMap.put("SDIA", ccnsion.getDia());
					}
				} else {
					HeatCoilDimension hcnsion = AhuMetadata.findOne(HeatCoilDimension.class, serial);
					if (EmptyUtil.isEmpty(hcnsion)) {
						logger.error("查表未获取到ColdCoilDimensionNew @冷水盘管段计算时");
					} else {
						configMap.put("FB", Integer.parseInt(hcnsion.getB()) + 25 - baseheight);
						if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
							configMap.put("FB", Integer.parseInt(hcnsion.getB())-baseheight);
						}
						configMap.put("FC", hcnsion.getC());
						configMap.put("FD", hcnsion.getD());
						configMap.put("FE", hcnsion.getE());
						configMap.put("FG", hcnsion.getG());
						configMap.put("SDIA", hcnsion.getDia());
					}
				}
			}
		}
		configMap.put("HUIZHITUOJIA2", "0");
		configMap.put("WATERBREAKHEIGHT", "50");
		configMap.put("COILDIRECTION", MapValueUtils.getStringValue("PIPEORIENTATION", ahuConfigMap));
		configMap.put("FILEPATH_TOP", "DSQ.DXF");
		configMap.put("FILEPATH_FRONT", "DSQ.DXF");
		toSetPartAirDirection(partMap, configMap);

		configMap.put("PARTDETAIL", "CW COIL");
		String sectionKey = part.getKey();
		if (SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId().equals(sectionKey)) {
			configMap.put("DIMENSION_SM_THICKNESS", "100");
		}
		return configMap;
	}

	/**
	 * 生成CAD段信息 [热水盘管段-005]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection005(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "5");
		String product = ahu.getProduct();
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		int baseheight = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_BASEHEIGHT, ahuConfigMap);
		int len = serial.length();
		String xilie = serial.substring(len - 4);
		if (SystemCountUtil.gteBigUnit(Integer.parseInt(xilie))) {
			configMap.put("BIGTYPE", "1");
		} else {
			configMap.put("BIGTYPE", "0");
		}
		if (xilie.compareTo("3438") > -1) {
			configMap.put("DUALHEADER", "1");
		} else {
			configMap.put("DUALHEADER", "0");
		}
		// configMap.put("HUIZHITUOJIA2", "0");
		configMap.put("DIMEN", "1");
		int sectionl = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_HEATINGCOIL_SECTIONL, partMap);
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionl);
		//if (xilie.compareTo("2531") > 0) {
		//	configMap.put("DIMEN", "1");
		//}
		if (SystemCountUtil.gteBigUnit(Integer.parseInt(xilie))) {
			HCoilBigTuojiaDimension nsion = AhuMetadata.findOne(HCoilBigTuojiaDimension.class, xilie);
			if (EmptyUtil.isEmpty(nsion)) {
				logger.error("查表未获取到CoilBigTJDimension @热水盘管段计算时");
			} else {
				configMap.put("TUOJIA1_L", nsion.getW());
				configMap.put("TUOJIA1_H", nsion.getB());
			}
			configMap.put("TUOJIA1DRAWING", "HCTJ1");
		}
		configMap.put("DGORRRAINPAN", "0");
		configMap.put("DGHEIGHT", "50");
		configMap.put("TRAYLEFT", "DG_L");
		configMap.put("TRAYRIGHT", "DG_R");

		int rows = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_HEATINGCOIL_ROWS, partMap);
		if (SystemCountUtil.gteBigUnit(Integer.parseInt(xilie))) {
			HCoilBigDimensionNew nsionew = AhuMetadata.findOne(HCoilBigDimensionNew.class, xilie);
			if (EmptyUtil.isEmpty(nsionew)) {
				logger.error("查表未获取到CoilBigDimensionlNew @热水盘管段计算时");
			} else {
				configMap.put("FB", nsionew.getB());
				configMap.put("FC", nsionew.getC());
				configMap.put("FG", nsionew.getG());
				configMap.put("FD", nsionew.getD2r());
				configMap.put("FE", nsionew.getE2r());

				configMap.put("COLITHICKNESS", nsionew.getCoilw());
				configMap.put("COILHEIGHT", nsionew.getCoilh());
				configMap.put("SDIA", nsionew.getDia());
			}
		} else {
			CoilDimension nsion = AhuMetadata.findOne(CoilDimension.class, serial);;
			if (EmptyUtil.isEmpty(nsion)) {
				logger.error("查表未获取到CoilBigDimensionlNew @热水盘管段计算时");
			} else {
				String coilw = SYS_BLANK;
				String coilh = nsion.getHeight();
				if (rows == 1) {
					coilw = nsion.getR1();
				} else if (rows == 2) {
					coilw = nsion.getR2();
				} else if (rows == 3) {
					coilw = nsion.getR3();
				} else if (rows == 4) {
					coilw = nsion.getR4();
				} else if (rows == 5) {
					coilw = nsion.getR5();
				} else if (rows == 6) {
					coilw = nsion.getR6();
				} else if (rows == 7) {
					coilw = nsion.getR7();
				} else if (rows == 8) {
					coilw = nsion.getR8();
				} else if (rows == 10) {
					coilw = nsion.getR10();
				} else if (rows == 12) {
					coilw = nsion.getR12();
				}
				configMap.put("COLITHICKNESS", coilw);
				configMap.put("COILHEIGHT", coilh);
				if (rows > 2) {
					ColdCoilDimension ccnsion = AhuMetadata.findOne(ColdCoilDimension.class, serial);
					if (EmptyUtil.isEmpty(ccnsion)) {
						logger.error("查表未获取到ColdCoilDimensionNew @热水盘管段计算时");
					} else {
						int b = Integer.parseInt(ccnsion.getB());
						configMap.put("FB", b + 25 - baseheight);
						if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
							configMap.put("FB", b - baseheight);
						}
						configMap.put("FC", ccnsion.getC());
						if (rows == 3) {
							configMap.put("FD", ccnsion.getRd3());
							configMap.put("FE", ccnsion.getRe3());
						} else if (rows == 4) {
							configMap.put("FD", ccnsion.getRd4());
							configMap.put("FE", ccnsion.getRe4());
						} else if (rows == 5) {
							configMap.put("FD", ccnsion.getRd5());
							configMap.put("FE", ccnsion.getRe5());
						} else if (rows == 6) {
							configMap.put("FD", ccnsion.getRd6());
							configMap.put("FE", ccnsion.getRe6());
						} else if (rows == 7) {
							configMap.put("FD", ccnsion.getRd7());
							configMap.put("FE", ccnsion.getRe7());
						} else if (rows == 8) {
							configMap.put("FD", ccnsion.getRd8());
							configMap.put("FE", ccnsion.getRe8());
						} else if (rows == 10) {
							configMap.put("FD", ccnsion.getRd10());
							configMap.put("FE", ccnsion.getRe10());
						} else if (rows == 12) {
							configMap.put("FD", ccnsion.getRd12());
							configMap.put("FE", ccnsion.getRe12());
						}
						configMap.put("FG", ccnsion.getG());
						configMap.put("SDIA", ccnsion.getDia());
					}
				} else {
					HeatCoilDimension hcnsion = AhuMetadata.findOne(HeatCoilDimension.class, serial);
					if (EmptyUtil.isEmpty(hcnsion)) {
						logger.error("查表未获取到ColdCoilDimensionNew @热水盘管段计算时");
					} else {
						int b = Integer.parseInt(hcnsion.getB());
						configMap.put("FB", b + 25 - baseheight);
						if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
							configMap.put("FB", b - baseheight);
						}
						configMap.put("FC", hcnsion.getC());
						configMap.put("FD", hcnsion.getD());
						configMap.put("FE", hcnsion.getE());
						configMap.put("FG", hcnsion.getG());
						configMap.put("SDIA", hcnsion.getDia());
					}
				}
			}
		}
		int unitwidth = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_UNITWIDTH, ahuConfigMap);
		int ahu17 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_FRMTHICK, ahuConfigMap);
		int ahu19 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_INTERVAL, ahuConfigMap);
		// 机组宽度*100+(机组通用配置第19项)- 2*(机组通用配置第17项)
		int coilwidth = unitwidth + ahu19 - 2 * ahu17;
		configMap.put("COILWIDTH", coilwidth);
		configMap.put("COILDIRECTION", MapValueUtils.getStringValue("PIPEORIENTATION", ahuConfigMap));
		configMap.put("HEADERLENGTH", "100");
		configMap.put("AIRFLOW", ahu.getCalStyle());
		configMap.put("PARTDETAIL", "HW COIL");

		// configMap.put("DIMENSION_SM_THICKNESS", "100");
		// configMap.put(UtilityConstant.SYS_CAD_AIRDIRECTION, ahu.getCalStyle());
		// configMap.put("BREAKWATERTYPE", "D");
		// configMap.put("DSQPATH", "dsq");
		// configMap.put("BWBHEIGHT", "10");
		// configMap.put("DRAINPANHEIGHT", "50");
		// configMap.put("DRAINPANWIDTH", "630");
		// configMap.put("DRAINPANPATH", "shuipan");
		return configMap;
	}

	/**
	 * 生成CAD段信息 [蒸汽盘管段-006]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection006(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "6");
		int sectionL = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_STEAMCOIL_SECTIONL, partMap);
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionL);
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		int len = serial.length();
		String xilie = serial.substring(len - 4);
		configMap.put("BIGTYPE", "0");
		configMap.put("DUALHEADER", "0");
		if (SystemCountUtil.gteBigUnit(Integer.parseInt(xilie))) {
			configMap.put("BIGTYPE", "1");
			configMap.put("DIMEN", "1");
		}
		if (xilie.compareTo("3438") > -1) {
			configMap.put("DUALHEADER", "1");
		}else{
			configMap.put("DUALHEADER", "0");
		}
		if (SystemCountUtil.gteBigUnit(Integer.parseInt(xilie))) {
			configMap.put("BREAKWATERTYPE", "A");
			SteamBigTuojiaDimension snsion = AhuMetadata.findOne(SteamBigTuojiaDimension.class, serial);
			if (EmptyUtil.isEmpty(snsion)) {
				logger.error("查表未获取到SteamBigTuojiaDimension @蒸汽盘管段计算时");
			} else {
				configMap.put("TUOJIA1_L", snsion.getW());
				configMap.put("TUOJIA1_H", snsion.getB());
			}
			configMap.put("TUOJIA1DRAWING", "ZQTJ");
		}

		configMap.put("HUIZHITUOJIA2", "0");
		configMap.put("DGHEIGHT", "50");
		configMap.put("TRAYLEFT", "DG_L");
		configMap.put("TRAYRIGHT", "DG_R");
		SteamCoilDimension scnsion = AhuMetadata.findOne(SteamCoilDimension.class, serial);
		if (EmptyUtil.isEmpty(scnsion)) {
			logger.error("查表未获取到SteamCoilDimensionNew @蒸汽盘管段计算时");
		} else {
			configMap.put("FB", scnsion.getB());
			configMap.put("FD", scnsion.getD());
			configMap.put("FE", scnsion.getE());
			configMap.put("FC", scnsion.getC());
			configMap.put("FG", scnsion.getG());
			configMap.put("SDIA", scnsion.getDia());
			configMap.put("COILHEIGHT", scnsion.getCoilh());
			configMap.put("COLITHICKNESS", scnsion.getCoilw());

			int ahu17 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_FRMTHICK, ahuConfigMap);
			int ahu19 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_INTERVAL, ahuConfigMap);
			int unitwidth = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_UNITWIDTH, ahuConfigMap);
			// 机组宽度*100+(机组通用配置第19项)- 2*(机组通用配置第17项)
			int coilwidth = unitwidth + ahu19 - 2 * ahu17;
			configMap.put("COILWIDTH", coilwidth);

		}
		configMap.put("COILDIRECTION", SYS_ALPHABET_L_UP);
		configMap.put("HEADERLENGTH", "100");//水管的长度，突出面板的长度，俯视图会用到
		toSetPartAirDirection(partMap, configMap);
		configMap.put("PARTDETAIL", "ST COIL");
		return configMap;
	}

	/**
	 * 生成CAD段信息 [电加热盘管段-007]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection007(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "7");
		int rows = Integer.parseInt(MapValueUtils.getStringValue(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_ROWNUM, partMap).toLowerCase().replace(SYS_ALPHABET_R_UP, SYS_BLANK));
		configMap.put("ROWNUM", rows);
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		ECoilDimension nsion = AhuMetadata.findOne(ECoilDimension.class, UtilityConstant.SYS_UNIT_SERIES_39G+serial.substring(serial.length() - 4));
		if (EmptyUtil.isEmpty(nsion)) {
			logger.error("查表未获取到ECoilDimension @电加热盘管段计算时");
		} else {
			String COILHEIGHT = nsion.getH();
			String COLITHICKNESS = nsion.getW();
			String COILWIDTH = nsion.getL();
			configMap.put("COILHEIGHT", COILHEIGHT);
			configMap.put("COLITHICKNESS", COLITHICKNESS);
			configMap.put("COILWIDTH", COILWIDTH);
		}
		configMap.put("DGHEIGHT", "50");
		configMap.put("TRAYLEFT", "DG_L");
		configMap.put("TRAYRIGHT", "DG_R");
		configMap.put("FILEPATH_TOP", "HEATER_TOP");
		configMap.put("FILEPATH_FRONT", "HEATER_FRONT");
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_SECTIONL, partMap));
		configMap.put("COILDIRECTION", MapValueUtils.getStringValue("PIPEORIENTATION", ahuConfigMap));
		toSetPartAirDirection(partMap, configMap);
		configMap.put("PARTDETAIL", "EL COIL");
		return configMap;
	}

	/**
	 * 生成CAD段信息 [干蒸加湿段-008]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection008(AhuParam ahu, PartParam part) {
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "8");
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_STEAMHUMIDIFIER_SECTIONL, partMap));
		String type = MapValueUtils.getStringValue(UtilityConstant.METASEXON_STEAMHUMIDIFIER_HTYPES, partMap);
		int fH = 0;
		int fL = 0;
		int FN = 0;
		int FA = 95;
		if (SystemCalculateConstants.STEAMHUMIDIFIER_TYPE_1.equals(type)) {
			fH = 412;
			fL = 140;
			FN = 212;
		} else if ("2".equals(type)) {
			fH = 470;
			fL = 162;
			FN = 205;
		} else if ("3".equals(type)) {
			fH = 502;
			fL = 188;
			FN = 225;
		} else if ("4".equals(type)) {
			fH = 675;
			fL = 230;
			FN = 275;
		}
		configMap.put("DIMENSIONH", fH);
		configMap.put("DIMENSIONL", fL);
		configMap.put("DIMENSIONW", FN + FA);
		configMap.put("DGHEIGHT", "50");
		configMap.put("DIRECTION", SYS_ALPHABET_R_UP);
		configMap.put("FILEPATH_FRONT", "GZJS.DXF");
		configMap.put("FILEPATH_TOP", "GZJS_TOP.DXF");
		configMap.put("PARTDETAIL", "ST HUMID");

		return configMap;
	}

	/**
	 * 生成CAD段信息 [风机段-011]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection011(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "11");
		String product = ahu.getProduct();
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		int sectionl = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_FAN_SECTIONL, partMap);
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionl);
		String theDoorOrientation = MapValueUtils.getStringValue("DOORDIRECTION", ahuConfigMap);
		int iSecWidth = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_UNITWIDTH, ahuConfigMap);
		int iInterval = 100;
		if(serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39CBFI)!=-1){
			iInterval = IniConfigMapUtil.Interval_39CBFI;
		}else if (serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39G)!=-1){
			iInterval = IniConfigMapUtil.Interval_39G;
		}else if (serial.indexOf(UtilityConstant.SYS_UNIT_SERIES_39XT)!=-1){
			iInterval = IniConfigMapUtil.Interval_39XT;
		}else{
			iInterval = IniConfigMapUtil.Interval_Other;
		}

		boolean doorOnBothSide = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_FAN_DOORONBOTHSIDE, partMap);
		if (doorOnBothSide) {
			configMap.put("DOORDIRECTION", "3");
		} else {
			configMap.put("DOORDIRECTION", theDoorOrientation);
		}
		configMap.put("MOTORDIRECTION", theDoorOrientation);
		if ((UtilityConstant.SYS_UNIT_SERIES_39G.equals(product))) {
			configMap.put("DOORTYPE", "2");
		} else if (("39CBF".equals(product))) {
			configMap.put("DOORTYPE", "1");
		} else if ((UtilityConstant.SYS_UNIT_SERIES_39XT.equals(product))) {
			configMap.put("DOORTYPE", "1");
		} else if ((UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(product))) {
			configMap.put("DOORTYPE", "1");
		}
		configMap.put("DOORDIMENSION", AhuMetadata.findOne(PaneledDoor.class, serial, "11").getDoor());

		boolean fixRepairLamp = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_FAN_FIXREPAIRLAMP, partMap);
		if (fixRepairLamp) {
			configMap.put("LAMP", "1");
		} else {
			configMap.put("LAMP", "0");
		}
		String accessDoor = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_ACCESSDOOR, partMap);
		if (SystemCalculateConstants.FAN_ACCESSDOOR_DOORWITHWINDOW.equals(accessDoor)) {
			configMap.put("DOOR", "1");
			configMap.put("VIEWPORT", "1");
		} else if (SystemCalculateConstants.FAN_ACCESSDOOR_DOORWITHOUTWINDOW.equals(accessDoor)) {
			configMap.put("DOOR", "1");
			configMap.put("VIEWPORT", "0");
		}
		String outletDirection = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_OUTLETDIRECTION, partMap);
		String airDirection = MapValueUtils.getStringValue(METASEXON_AIRDIRECTION, partMap);
		String doubleReturnAirDirection = MapValueUtils.getStringValue(METASEXON_DOUBLERETURN_AIRDIRECTION, partMap);//双层机组airDirection

		int fx = 0;//送风
		if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)) {//回风机组
			fx = 1;
		}
		
		//默认送风机
		String fengxiang = SYS_ALPHABET_R_UP;;
		//带回风机组，根据风机airDirection 判断outlet方向
		AhuLayout ahuLayout = ahu.getLayout();
		//当前机组数据异常需要重新进入机组界面点击保存按钮。
		if (EmptyUtil.isEmpty(ahuLayout)) {
			throw new ApiException(ErrorCode.PARTITION_VALIDATE_LAYOUT_JSON_NULL_MSG);
		}
		if (ahuLayout.getStyle() >20) {
			//双层机组airDirection
			if (LayoutStyleEnum.DOUBLE_RETURN_1.style() == ahuLayout.getStyle()
					|| LayoutStyleEnum.DOUBLE_RETURN_2.style() == ahuLayout.getStyle()) {
				if (AirDirectionEnum.RETURNAIR.getCode().equals(doubleReturnAirDirection)) {//回风机组
					fengxiang = SYS_ALPHABET_L_UP;
				}else{
					fengxiang = SYS_ALPHABET_R_UP;
				}
			}else{
				if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)) {//回风机组
					fengxiang = SYS_ALPHABET_L_UP;
				}else{
					fengxiang = SYS_ALPHABET_R_UP;
				}
			}
		}

		String outlet = outletDirection + UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN + fengxiang;
		String motorPosition = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_MOTORPOSITION, partMap);
		/*if  varpartproperty.motorlocation='R' THEN
			str:='Side'
		 else
			str:='Back';*/
		if (SystemCalculateConstants.FAN_MOTORPOSITION_BACK.equals(motorPosition)) {
			configMap.put("OUTLET", outlet + "_H");//后置
		}
		if (SystemCalculateConstants.FAN_MOTORPOSITION_SIDE.equals(motorPosition)) {
			configMap.put("OUTLET", outlet);//侧置
		}
		String fanType = IniConfigMapUtil.getFantype(MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_FANMODEL, partMap));
		StringBuffer tuyuanpathforv = new StringBuffer();
		if (SystemCalculateConstants.FAN_OUTLETDIRECTION_THF.equals(outletDirection)) {
			tuyuanpathforv.append("THF&BHF");
		} else if (SystemCalculateConstants.FAN_OUTLETDIRECTION_BHF.equals(outletDirection)) {
			tuyuanpathforv.append("THF&BHF");
		} else if (SystemCalculateConstants.FAN_OUTLETDIRECTION_UBF.equals(outletDirection)) {
			tuyuanpathforv.append("UBF");
		} else if (SystemCalculateConstants.FAN_OUTLETDIRECTION_UBR.equals(outletDirection)) {
			tuyuanpathforv.append("UBR");
		}
		boolean standbyMotor = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_FAN_STANDBYMOTOR, partMap);
		if (standbyMotor) {
			tuyuanpathforv.append(UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN).append("Dmotor");
		} else {
			tuyuanpathforv.append(UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN).append("Motor");
		}
		if ("1".equals(theDoorOrientation)) {
			tuyuanpathforv.append("_left");
		} else{
			tuyuanpathforv.append("_Right");
		}
		tuyuanpathforv.append("_"+fengxiang);

		if (SystemCalculateConstants.FAN_MOTORPOSITION_BACK.equals(motorPosition)) {
			tuyuanpathforv.append("_h");
		}
		configMap.put("TUYUANPATHFORV", tuyuanpathforv.toString());
		int fw = iSecWidth + iInterval;

		String motorBaseNo = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_MOTORBASENO, partMap);
		FanDimension fanDimension = null;
		if (SystemCalculateConstants.FAN_MOTORPOSITION_BACK.equals(motorPosition))
			fanDimension = AhuMetadata.findOne(FanDimensionBMotor.class, serial, fanType, "1");
		else
			fanDimension = AhuMetadata.findOne(FanDimension.class, serial, fanType, "1");

		if(null != fanDimension){
			//侧置产生偏移量：
			double pp = 0;
			if (SystemCalculateConstants.FAN_MOTORPOSITION_SIDE.equals(motorPosition) && EmptyUtil.isNotEmpty(partMap.get(UtilityConstant.METASEXON_FAN_P))) {
				
			    Map<String,String> motorP = new HashMap<String, String>();
                motorP.put(METAHU_SERIAL , serial);
                motorP.put(METASEXON_FAN_SUPPLIER , String.valueOf(partMap.get(UtilityConstant.METASEXON_FAN_SUPPLIER)));
                motorP.put(METASEXON_FAN_MOTORBASENO , String.valueOf(partMap.get(UtilityConstant.METASEXON_FAN_MOTORBASENO)));
                motorP.put(METASEXON_FAN_FANMODEL , String.valueOf(partMap.get(UtilityConstant.METASEXON_FAN_FANMODEL)));
                motorP.put(METASEXON_FAN_STARTSTYLE , String.valueOf(partMap.get(UtilityConstant.METASEXON_FAN_STARTSTYLE)));
                motorP.put(METASEXON_FAN_OUTLET , String.valueOf(partMap.get(UtilityConstant.METASEXON_FAN_OUTLET)));
                motorP.put(METASEXON_FAN_TYPE , String.valueOf(partMap.get(UtilityConstant.METASEXON_FAN_TYPE)));
			    //pp = NumberUtil.convertStringToDouble(SectionContentConvertUtils.calMotorP(motorP));
				String p= SectionContentConvertUtils.calMotorP(motorP);
				if(EmptyUtil.isNotEmpty(p)){
					pp = NumberUtil.convertStringToDouble(p);
				}
				//MapValueUtils.getDoubleValue(UtilityConstant.METASEXON_FAN_P, partMap);
				
				
				//TODO 硬编码，修改可以正常侧置时，添加了侧置数据BUG。后期版本请删除 BEGIN
		        try {
		        	
		        	//如果有偏移，校验是否需要偏移
		        	if(pp>0) {
		        	String unitValue=serial;
		        	String motorValue=MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_SUPPLIER, partMap);
		        	String motorTypeValue=MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_TYPE, partMap);
		        	String motorBaseNoValue=MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_MOTORBASENO, partMap);
		        	String fanModelValue=MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_FANMODEL, partMap);
		        	String startStyleValue=MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_STARTSTYLE, partMap);
		        	String calUnit="39CQ" + SystemCountUtil.getUnit(unitValue);
		        	String fanCodeValue=BaseDataUtil.StringConversionNumber(fanModelValue);
		                String unit = SystemCountUtil.getUnit(unitValue);
		                if (SystemCountUtil.ltBigUnit(BaseDataUtil.stringConversionInteger(unit))) {
		                	FanCodeElectricSize fanCodeElectricSize = MotorPositionUtil.getSmallFanSize(fanCodeValue, calUnit);
		                    double inWidth = MotorPositionUtil.getSmallMotorSize(motorTypeValue, fanModelValue, motorValue, motorBaseNoValue, startStyleValue,
		    						fanCodeElectricSize);
		                    Integer INNERWIDTH=fanCodeElectricSize.getINNERWIDTH();
		                    
		                    if (inWidth <= INNERWIDTH) {
		                    	pp = 0;
		                    }
		                     
		                }else {
		                	
		                	FanCodeElectricSizeBig fanCodeElectricSizeBig = MotorPositionUtil.getBigFanSIze(fanCodeValue, calUnit);
		                	double P=fanCodeElectricSizeBig.getP();
		                    double inWidth = MotorPositionUtil.getBigUnitMotorSize(motorTypeValue, fanModelValue, motorValue, motorBaseNoValue, fanCodeValue, startStyleValue,
		                    		calUnit,fanCodeElectricSizeBig);
		                    
		                    if (inWidth >= 100) {
		                    	pp = 0;
		                    }
		                	
		                }
		                
		        	}
		        	
		        }catch(Exception e) {
		        	
		        }
		        
		       //TODO 硬编码，修改可以正常侧置时，添加了侧置数据BUG。后期版本请删除 BEGIN
				
			}
			//风阀距离机箱边缘距离 (theDoorOrientation:left:1,right:2)
			if ("1".equals(theDoorOrientation)) {
				configMap.put("DIMENSION_FANA", Double.parseDouble(fanDimension.getA())-pp);
			} else {
				configMap.put("DIMENSION_FANA", fw - Double.parseDouble(fanDimension.getA()) - Double.parseDouble(fanDimension.getD())+pp);
			}
			configMap.put("DIMENSION_FAND", fanDimension.getD());
			if (SystemCalculateConstants.FAN_OUTLETDIRECTION_THF.equals(outletDirection)) {
				configMap.put("DIMENSION_FANE", fanDimension.getThf());
			} else if (SystemCalculateConstants.FAN_OUTLETDIRECTION_BHF.equals(outletDirection)) {
				configMap.put("DIMENSION_FANE", fanDimension.getBhf());
			} else if (SystemCalculateConstants.FAN_OUTLETDIRECTION_UBF.equals(outletDirection)) {
				configMap.put("DIMENSION_FANE", fanDimension.getUbf());
			} else if (SystemCalculateConstants.FAN_OUTLETDIRECTION_UBR.equals(outletDirection)) {
				configMap.put("DIMENSION_FANE", fanDimension.getUbr());
			}
		}

		configMap.put("DIMENSION_FALANGETHICK", "50");
		configMap.put("DIMENSION_FALANGEEDGE", "50");
		int len = serial.length();
		String xilie = serial.substring(len - 4);
		if (SystemCountUtil.gteBigUnit(Integer.parseInt(xilie)))
			configMap.put("ABSORBERHEIGHT", "150");
		else
			configMap.put("ABSORBERHEIGHT", "40");

		configMap.put("ABSORBERNUM", "2");
		configMap.put("DUALDOOR", "0");
		if (doorOnBothSide) {
			configMap.put("DUALDOOR", "1");
		}
		configMap.put("DUALMOTOR", "0");
		if (standbyMotor) {
			configMap.put("DUALMOTOR", "1");
		}
		if (SystemCalculateConstants.FAN_OUTLETDIRECTION_THF.equals(outletDirection)) {
			configMap.put("OUTLETDISTANCE", "30");
		} else if (SystemCalculateConstants.FAN_OUTLETDIRECTION_BHF.equals(outletDirection)) {
			configMap.put("OUTLETDISTANCE", "30");
		}
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_top");
		configMap.put("DOORDRAWING_FRONT", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_front");
		configMap.put("AIRFLOW", "0");//写死
		//风机形式
		String fjxs = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_OUTLET, partMap);

		if (LayoutStyleEnum.DOUBLE_RETURN_1.style() == ahuLayout.getStyle() || LayoutStyleEnum.DOUBLE_RETURN_2.style() == ahuLayout.getStyle()) {
			if (AirDirectionEnum.RETURNAIR.getCode().equals(doubleReturnAirDirection)) {//回风机组
				configMap.put(UtilityConstant.SYS_CAD_AIRDIRECTION, "1");
			}else{
				configMap.put(UtilityConstant.SYS_CAD_AIRDIRECTION, "0");
			}
		}else{
			if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)) {//回风机组
				configMap.put(UtilityConstant.SYS_CAD_AIRDIRECTION, "1");
			}else{
				configMap.put(UtilityConstant.SYS_CAD_AIRDIRECTION, "0");
			}

			//特殊逻辑单层送风端面加入airdirection=1回风三视图属性
			String fkjjwz = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_SENDPOSITION, partMap);
            if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)) {//回风机组
                fkjjwz = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_RETURNPOSITION, partMap);
            }
			if(FAN_SENDPOSITION_FACE.equals(fkjjwz) && !FAN_OPTION_OUTLET_WWK.equals(fjxs)){//无蜗壳不用特殊处理端面加入airdirection=1
                configMap.put(UtilityConstant.SYS_CAD_AIRDIRECTION, "1");
			}
		}
		configMap.put("PARTDETAIL", "FAN");

		if(fanType.compareTo(SYS_ALPHABET_L_UP) >-1 || (fanType.equals("K") && motorBaseNo.indexOf("Y180")!=-1)){
			configMap.put("ABSORBDRAWING", "HD");
		}else{
			configMap.put("ABSORBDRAWING", "JD");
		}
//		configMap.put("ABSORBDRAWING", "HD");
		configMap.put("FANBASEDRWING", "FANBASE");
		configMap.put("FANBASEDIMENSION", "80");

		if(FAN_OPTION_OUTLET_WWK.equals(fjxs)){//无蜗壳风机
			configMap.put( "PLUGFAN","1");

			//TODO UnitModel 0卧式，1立式，2双层，3并派右，4并排左, 5双层热回收, 6单层热回收, 7板式热回收
			if (	(((LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()) || (LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle())) && (fx==1))// 1回风机
					||
					((LayoutStyleEnum.DOUBLE_RETURN_1.style() == ahuLayout.getStyle() || LayoutStyleEnum.DOUBLE_RETURN_2.style() == ahuLayout.getStyle()) && fx==1)//1回风机
			)
			{
				configMap.put( "PLUG_FRONTDRAWING", "PLUGFAN_FRONT_L");
				configMap.put( "PLUG_TOPDRAWING", "PLUGFAN_TOP_L");
			}
        	else
			{
			configMap.put( "PLUG_FRONTDRAWING", "PLUGFAN_FRONT");
			configMap.put( "PLUG_TOPDRAWING", "PLUGFAN_TOP");
			}

			SPlugfandimension splugfandimension = AhuMetadata.findOne(SPlugfandimension.class, fanType);
			if(EmptyUtil.isNotEmpty(splugfandimension)) {
				configMap.put("DIMENSION_FRONT_HEIGHT", splugfandimension.getFrontHeight());
				configMap.put("DIMENSION_FRONT_LENGTH", splugfandimension.getFrontLength());
				int topHeight = Integer.parseInt(splugfandimension.getTopHeight());
				configMap.put("DIMENSION_TOP_HEIGHT", topHeight);
				configMap.put("DIMENSION_TOP_LENGTH", splugfandimension.getFrontLength());
//				configMap.put("PLUG_FAN", splugfandimension.getPlugFanStart());
				configMap.put("PLUG_FAN", 1);  //added  20140925

				//重置上面的。。
				configMap.put("DIMENSION_FANA", (fw / 2 - topHeight / 2));
				configMap.put("DIMENSION_FAND", 500);
				configMap.put("DIMENSION_FANE", 500);
			}
		}

		//送风出口位置
		String fkjjwz = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_SENDPOSITION, partMap);
		if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)) {//回风机组
			fkjjwz = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_RETURNPOSITION, partMap);
		}

		if(!FAN_SENDPOSITION_NO.equals(fkjjwz)) {//风口接件:无
			String airFlowO = SYS_BLANK;
			if(FAN_SENDPOSITION_TOP.equals(fkjjwz))
				airFlowO ="1";//顶面
			else
				airFlowO ="2";//端面

			configMap.put( "AirFlowO", airFlowO);
			String fkjjxs = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_SUPPLYDAMPER, partMap);
			if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)
					&& (LayoutStyleEnum.DOUBLE_RETURN_1.style() != ahuLayout.getStyle() && LayoutStyleEnum.DOUBLE_RETURN_2.style() != ahuLayout.getStyle())) {//回风机组 双层转向只有送风方向
				fkjjxs = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_RETURNDAMPER, partMap);
			}
			String dampertype = "1";
			if(FAN_SUPPLYDAMPER_GSD.equals(fkjjxs)){
				configMap.put("DAMPERTYPE", "A");// 镀锌板风阀
				configMap.put( "MIXTYPE", "A");
			}
			if(FAN_SUPPLYDAMPER_AD.equals(fkjjxs)){
				configMap.put("DAMPERTYPE", "B");// 铝合金风阀
				configMap.put( "MIXTYPE", "B");
				dampertype = "2";
			}
			if(FAN_SUPPLYDAMPER_FL.equals(fkjjxs)){
				configMap.put("DAMPERTYPE", "C");// 法兰
				configMap.put( "MIXTYPE", "C");
			}
//			DamperDimensionL nsion = AhuMetadata.findOne(DamperDimensionL.class, serial, "1", dampertype);
			Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));
			DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class, serial.substring(0, serial.length() - 4),
					airFlowO, damper.getSectionL());
			/*airFlowO, "2".equals(airFlowO)?damper.getSectionL():(SYS_BLANK+sectionl));*/
			String mixType = MapValueUtils.getStringValue("AirFlowO", configMap);
			int lenth = serial.length();
			String heightStr = serial.substring(lenth - 4, lenth - 2);
			String weightStr = serial.substring(lenth - 2);
			int height = Integer.parseInt(heightStr);
			int width = Integer.parseInt(weightStr);
			String metrial = MapValueUtils.getStringValue(UtilityConstant.METASEXON_FAN_SUPPLYDAMPER, partMap);
			if (EmptyUtil.isEmpty(nsion)) {
				logger.error("查表未获取到DamperDimension @出风段计算时");
			} else {
				if (fkjjxs.startsWith(SystemCalculateConstants.FAN_SUPPLYDAMPER_AD)) {
					double c1 = Double.parseDouble(nsion.getC1());
					double b = Double.parseDouble(nsion.getB());
					double d1 = Double.parseDouble(nsion.getD1());
					configMap.put("DIMENSION_A", c1 - 2 * 40);
					configMap.put("DIMENSION_B", 40 + b - d1);
				} else {
					configMap.put("DIMENSION_A", Double.parseDouble(nsion.getA()));
					configMap.put("DIMENSION_B", Double.parseDouble(nsion.getB()));
				}

				int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);
				int first1 = Integer.parseInt(mixType);
				if (first1 == 1 || first1 == 2) {
					configMap.put("DIMENSION_C", (width - 1) * 100 + DIMENSION_C_calPara - 72);
				}
				if (first1 == 3 || first1 == 4) {
					configMap.put("DIMENSION_C", (height - 1) * 100 + DIMENSION_C_calPara - 72);
				}
				configMap.put("DIMENSION_D", IniConfigMapUtil.getDefaultDIMENSION_D(product));
				if (first1 == 5) {
					int c = (width - 5) * 100 + DIMENSION_C_calPara - 72;
					configMap.put("DIMENSION_C", c);
					int DIMENSION_D_calPara = IniConfigMapUtil.getDIMENSION_D_calPara(product);
					configMap.put("DIMENSION_D", (width * 100 + DIMENSION_D_calPara - c) / 2);
				}
			}

            /*
            '1': Vartempstr:='镀锌板';
            '2': vartempstr:='铝合金';
            */
			//镀锌板
			if(SystemCalculateConstants.FAN_SUPPLYDAMPER_GSD.equals(fkjjxs)){
				if(product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39CBFI)!=-1 || product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39XT)!=-1){
					configMap.put("DIMENSION_E", "130");
				}else if(product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39G)!=-1){
					configMap.put("DIMENSION_E", "155");
				}
			}
			//铝合金风阀
			else{
				if(product.indexOf("39C")!=-1 || product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39XT)!=-1){
					configMap.put("DIMENSION_E", "130");
				}else if(product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39G)!=-1){
					configMap.put("DIMENSION_E", "155");
				}
			}

			if (SystemCalculateConstants.FAN_SUPPLYDAMPER_GSD.equals(fkjjxs) || (SystemCalculateConstants.FAN_SUPPLYDAMPER_AD.equals(fkjjxs)) ){
				configMap.put( "FILEPATH_TOP", "D:\\FZ_V.DXF");
				configMap.put( "FILEPATH_FRONT", "D:\\FC.DXF");
			}else{
				//Stor.Writestring('PART'+INTTOSTR(parmIndex), 'FILLPATH_TOP','D:\FZ_V.DXF');
				configMap.put( "FILEPATH_FRONT", "D:\\FD.DXF");
			}
			//Stor.Writestring('PART'+INTTOSTR(parmIndex1), 'AIRDIRECTION',setpartavdirection(FrmVarTUnit.UnitPro.UnitModel,VarTpart));
			configMap.put( "FLAN_OUT", "20");
			configMap.put( "ZSTTUYUANLENPARA", "1");
			//Stor.Writestring('PART'+INTTOSTR(parmIndex1), 'AIRDIRECTION',inttostr(Airflow));
		}
		return configMap;
	}

	/**
	 * 生成CAD段信息 [新回排风段-012]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection012(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "12");
		String product = ahu.getProduct();
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);


		String aInterface = MapValueUtils.getStringValue(UtilityConstant.METASEXON_COMBINEDFILTER_AINTERFACE, partMap);


		String COMBINEDMIXINGCHAMBER_AINTERFACE_ED = "ED";
		String COMBINEDMIXINGCHAMBER_AINTERFACE_MD = "MD";
		String COMBINEDMIXINGCHAMBER_AINTERFACE_FL = "FL";

		//出风口类型：
		if(COMBINEDMIXINGCHAMBER_AINTERFACE_MD.equals(aInterface)){
			configMap.put("DAMPERTYPE", "A");// 手动风阀
		}
		if(COMBINEDMIXINGCHAMBER_AINTERFACE_ED.equals(aInterface)){
			configMap.put("DAMPERTYPE", "B");// 电动风阀
		}
		if(COMBINEDMIXINGCHAMBER_AINTERFACE_FL.equals(aInterface)){
			configMap.put("DAMPERTYPE", "C");// 法兰
		}

		configMap.put("DOORDIRECTION", MapValueUtils.getStringValue("DOORDIRECTION", ahuConfigMap));
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL,
				MapValueUtils.getIntegerValue("meta.section.combinedMixingChamber.sectionL", partMap));

		if(product.indexOf("39C")!=-1 || product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39XT)!=-1){
			configMap.put("DOORTYPE", "1");
		}
		if(product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39G)!=-1){
			configMap.put("DOORTYPE", "2");
		}

		if (product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39G)!=-1) {
			configMap.put("DIMENSION_E", "155");
		}
		if (product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39XT)!=-1) {
			configMap.put("DIMENSION_E", "130");
		}
		if (product.indexOf("39C")!=-1) {
			configMap.put("DIMENSION_E", "130");
		}
		if (product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39CBFI)!=-1) {
			configMap.put("DIMENSION_E", "158");
		}

		configMap.put("DOORDIMENSION", AhuMetadata.findOne(PaneledDoor.class, serial, "12").getDoor());

		String oSDOOR = MapValueUtils.getStringValue("meta.section.combinedMixingChamber.OSDoor", partMap);
		String iSDOOR = MapValueUtils.getStringValue("meta.section.combinedMixingChamber.ISDoor", partMap);
		boolean lamp = MapValueUtils.getBooleanValue("meta.section.combinedMixingChamber.FixRepairLamp", partMap);
		if (lamp) {
			configMap.put("OSLAMP", "1");
			configMap.put("ISLAMP", "1");
		} else {
			configMap.put("OSLAMP", "0");
			configMap.put("ISLAMP", "0");
		}
		if (SystemCalculateConstants.DISCHARGE_ODOOR_DY.equals(oSDOOR)) {
			configMap.put("OSDOOR", "1");
			configMap.put("OSVIEWPORT", "1");
		}
		if (SystemCalculateConstants.DISCHARGE_ODOOR_DN.equals(oSDOOR)) {
			configMap.put("OSDOOR", "1");
			configMap.put("OSVIEWPORT", "0");
		}
		if (SystemCalculateConstants.DISCHARGE_ODOOR_ND.equals(oSDOOR)) {
			configMap.put("OSDOOR", "0");
			configMap.put("OSVIEWPORT", "0");
		}

		if (SystemCalculateConstants.DISCHARGE_ODOOR_DY.equals(iSDOOR)) {
			configMap.put("ISDOOR", "1");
			configMap.put("ISVIEWPORT", "1");
		}
		if (SystemCalculateConstants.DISCHARGE_ODOOR_DN.equals(iSDOOR)) {
			configMap.put("ISDOOR", "1");
			configMap.put("ISVIEWPORT", "0");
		}
		if (SystemCalculateConstants.DISCHARGE_ODOOR_ND.equals(iSDOOR)) {
			configMap.put("ISDOOR", "0");
			configMap.put("ISVIEWPORT", "0");
		}

		String DamperMeterial = MapValueUtils.getStringValue("meta.section.combinedMixingChamber.DamperMeterial",
				partMap);
		String dampertype = "1";
		if (SystemCalculateConstants.COMBINEDMIXINGCHAMBER_DAMPERMETERIAL_GL.equals(DamperMeterial)) {
			dampertype = "1";
		}
		if (SystemCalculateConstants.COMBINEDMIXINGCHAMBER_DAMPERMETERIAL_AL.equals(DamperMeterial)) {
			dampertype = "2";
		}
		DamperDimensionL nsion = AhuMetadata.findOne(DamperDimensionL.class, serial, "1", dampertype);
		if (EmptyUtil.isEmpty(nsion)) {
			logger.error("查表未获取到DamperDimensionL @新回排风段计算时");
		} else {
			configMap.put("DIMENSION_A", nsion.getA());
			configMap.put("DIMENSION_B", nsion.getB());
			configMap.put("DIMENSION_D", nsion.getD());
			configMap.put("DIMENSION_B1", nsion.getB1());
			float c = Float.parseFloat(nsion.getC());
			float d = Float.parseFloat(nsion.getD());
			if ("39CBF".equals(product)) {
				configMap.put("DIMENSION_C", c + 80);
				configMap.put("DIMENSION_D", d - 20);
			}else if (UtilityConstant.SYS_UNIT_SERIES_39CQ.equals(product)) {
				configMap.put("DIMENSION_C", c + 108);
				configMap.put("DIMENSION_D", d - 20);
			}else if ("39CQEC".equals(product)) {
				configMap.put("DIMENSION_C", c + 108);
			}else if (UtilityConstant.SYS_UNIT_SERIES_39XT.equals(product)) {
				configMap.put("DIMENSION_C", c + 28);
			}else if ("39XTEC".equals(product)) {
				configMap.put("DIMENSION_C", c + 28);
			}else {
				configMap.put("DIMENSION_C", c);
			}
		}
		DamperDimensionL nsion2 = AhuMetadata.findOne(DamperDimensionL.class, serial, "2" ,"2");
		if (EmptyUtil.isEmpty(nsion2)) {
			logger.error("查表未获取到DamperDimensionL2 @新回排风段计算时");
		} else {
			configMap.put("DIMENSION_A_IN", nsion2.getA());
			configMap.put("DIMENSION_B_IN", nsion2.getB());
			configMap.put("DIMENSION_C_IN", nsion2.getC());
			configMap.put("DIMENSION_D_IN", nsion2.getD());
		}
		String AInterface = MapValueUtils.getStringValue(UtilityConstant.METASEXON_COMBINEDFILTER_AINTERFACE, partMap);
		if (SystemCalculateConstants.COMBINEDMIXINGCHAMBER_AINTERFACE_ED.equals(AInterface)
				|| SystemCalculateConstants.COMBINEDMIXINGCHAMBER_AINTERFACE_MD.equals(AInterface)) {
			configMap.put("FILEPATH_TOP", "D:\\FZ_V.DXF");
			configMap.put("FILEPATH_FRONT", "D:\\FC.DXF");
		}
		if (SystemCalculateConstants.COMBINEDMIXINGCHAMBER_AINTERFACE_FL.equals(AInterface)) {
			configMap.put("FILEPATH_TOP", "D:\\FZ_V.DXF");
			configMap.put("FILEPATH_FRONT", "D:\\FD.DXF");
		}
		configMap.put("AIRFLOW", ahu.getCalStyle());
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_top");
		configMap.put("DOORDRAWING_FRONT", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_front");
		configMap.put("PARTDETAIL", "RET & MIX");
		return configMap;
	}

	/**
	 * 生成CAD段信息 [消音段-013]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection013(AhuParam ahu, PartParam part) {
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "13");
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_ATTENUATOR_SECTIONL, partMap));
		int ahu7 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_UNITWIDTH, ahuConfigMap);
		int ahu17 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_FRMTHICK, ahuConfigMap);
		int ahu19 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_INTERVAL, ahuConfigMap);
		// 机组宽度*100+(机组通用配置第19项)-2*(机组通用配置第17项)
		int xyqwidth = ahu7 + ahu19 - 2 * ahu17;
		configMap.put("XYQWIDTH", xyqwidth);
		toSetPartAirDirection(partMap, configMap);
		configMap.put("PARTDETAIL", "ATTEN");
		return configMap;
	}

	/**
	 * 生成CAD段信息 [出风段-014]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection014(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "14");
		String OutletDirection = MapValueUtils.getStringValue(UtilityConstant.METASEXON_DISCHARGE_OUTLETDIRECTION, partMap);
		if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_T.equals(OutletDirection)) {
			configMap.put("AirFlowO", "1");
		} else if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_A.equals(OutletDirection)) {
			configMap.put("AirFlowO", "2");
		} else if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_L.equals(OutletDirection)) {
			configMap.put("AirFlowO", "3");
		} else if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_R.equals(OutletDirection)) {
			configMap.put("AirFlowO", "4");
		} else if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_F.equals(OutletDirection)) {
			configMap.put("AirFlowO", "5");
		} else {
			configMap.put("AirFlowO", "2");
		}
		String AInterface = MapValueUtils.getStringValue(UtilityConstant.METASEXON_DISCHARGE_AINTERFACE, partMap);
		if (SystemCalculateConstants.DISCHARGE_AINTERFACE_MANUALDAMPER.equals(AInterface)) {
			configMap.put("MIXTYPE", "A");
			configMap.put("DAMPERTYPE", "A");
		} else if (SystemCalculateConstants.DISCHARGE_AINTERFACE_ELECTRICDAMPER.equals(AInterface)) {
			configMap.put("MIXTYPE", "B");
			configMap.put("DAMPERTYPE", "B");
		} else if (SystemCalculateConstants.DISCHARGE_AINTERFACE_FLANGE.equals(AInterface)) {
			configMap.put("MIXTYPE", "C");
			configMap.put("DAMPERTYPE", "C");
		}
		String DoorDirection = MapValueUtils.getStringValue(UtilityConstant.METASEXON_DISCHARGE_DOORDIRECTION, partMap);
		if (SystemCalculateConstants.DISCHARGE_DOORDIRECTION_LEFT.equals(DoorDirection)) {
			configMap.put("DOORDIRECTION", "1");
		}
		if (SystemCalculateConstants.DISCHARGE_DOORDIRECTION_RIGHT.equals(DoorDirection)) {
			configMap.put("DOORDIRECTION", "2");
		}
		if (SystemCalculateConstants.DISCHARGE_DOORDIRECTION_BOTH.equals(DoorDirection)) {
			configMap.put("DOORDIRECTION", "3");
		}
		if (SystemCalculateConstants.DISCHARGE_DOORDIRECTION_FACE.equals(DoorDirection)) {
			configMap.put("DOORDIRECTION", "4");
		}
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_DISCHARGE_SECTIONL, partMap));
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		String product = ahu.getProduct();
		if (product.equals(UtilityConstant.SYS_UNIT_SERIES_39G)) {
			configMap.put("DOORTYPE", "2");
		}else if (product.equals("39CBF")) {
			configMap.put("DOORTYPE", "1");
		}else if (product.equals(UtilityConstant.SYS_UNIT_SERIES_39XT)) {
			configMap.put("DOORTYPE", "1");
		}else if (product.equals(UtilityConstant.SYS_UNIT_SERIES_39CQ)) {
			configMap.put("DOORTYPE", "1");
		}else {
			configMap.put("DOORTYPE", "1");
		}
		boolean FixRepairLamp = MapValueUtils.getBooleanValue("meta.section.discharge.FixRepairLamp", partMap);
		if (FixRepairLamp) {
			configMap.put("LAMP", "1");
		} else {
			configMap.put("LAMP", "0");
		}
		String ODoor = MapValueUtils.getStringValue("meta.section.discharge.ODoor", partMap);
		if (SystemCalculateConstants.DISCHARGE_ODOOR_DY.equals(ODoor)) {
			configMap.put("VIEWPORT", "1");
			configMap.put("DOOR", "1");
		}
		if (SystemCalculateConstants.DISCHARGE_ODOOR_DN.equals(ODoor)) {
			configMap.put("VIEWPORT", "0");
			configMap.put("DOOR", "1");
		}
		if (SystemCalculateConstants.DISCHARGE_ODOOR_ND.equals(ODoor)) {
			configMap.put("VIEWPORT", "0");
			configMap.put("DOOR", "0");
		}

		configMap.put("DOORDIMENSION", AhuMetadata.findOne(PaneledDoor.class, serial, "14").getDoor());
//		Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));//暂时改为只是用段自身长度
		String mixType = MapValueUtils.getStringValue("AirFlowO", configMap);
		int sectionl = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_DISCHARGE_SECTIONL, partMap);
		if (UtilityConstant.SYS_STRING_NUMBER_2.equals(mixType)) {
			Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));
			sectionl = NumberUtil.convertStringToDoubleInt(damper.getSectionL());
		}
		int lenth = serial.length();
		String heightStr = serial.substring(lenth - 4, lenth - 2);
		String weightStr = serial.substring(lenth - 2);
		int height = Integer.parseInt(heightStr);
		int width = Integer.parseInt(weightStr);
		DamperDimension nsion = AhuMetadata.findOne(DamperDimension.class, serial.substring(0, serial.length() - 4),
				mixType, ""+sectionl);
		String metrial = MapValueUtils.getStringValue(UtilityConstant.METASEXON_DISCHARGE_DAMPERMETERIAL, partMap);
		if (EmptyUtil.isEmpty(nsion)) {
			logger.error("查表未获取到DamperDimension @出风段计算时");
		} else {
			if (SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_ALUMINUM.equals(metrial)) {
				double c1 = Double.parseDouble(nsion.getC1());
				double b = Double.parseDouble(nsion.getB());
				double d1 = Double.parseDouble(nsion.getD1());
				configMap.put("DIMENSION_A", c1 - 2 * 40);
				configMap.put("DIMENSION_B", 40 + b - d1);
			} else {
				configMap.put("DIMENSION_A", Double.parseDouble(nsion.getA()));
				configMap.put("DIMENSION_B", Double.parseDouble(nsion.getB()));
			}

			int DIMENSION_C_calPara = IniConfigMapUtil.getDIMENSION_C_calPara(product);
			int first1 = Integer.parseInt(mixType);
			if (first1 == 1 || first1 == 2) {
				configMap.put("DIMENSION_C", (width - 1) * 100 + DIMENSION_C_calPara - 72);
			}
			if (first1 == 3 || first1 == 4) {
				configMap.put("DIMENSION_C", (height - 1) * 100 + DIMENSION_C_calPara - 72);
			}
			configMap.put("DIMENSION_D", IniConfigMapUtil.getDefaultDIMENSION_D(product));
			if (first1 == 5) {
				int c = (width - 5) * 100 + DIMENSION_C_calPara - 72;
				configMap.put("DIMENSION_C", c);
				int DIMENSION_D_calPara = IniConfigMapUtil.getDIMENSION_D_calPara(product);
				configMap.put("DIMENSION_D", (width * 100 + DIMENSION_D_calPara - c) / 2);
			}
		}
		if (SystemCalculateConstants.DISCHARGE_AINTERFACE_MANUALDAMPER.equals(AInterface)) {
			configMap.put("FILEPATH_TOP", "D:\\FZ_V.DXF");
			configMap.put("FILEPATH_FRONT", "D:\\FC.DXF");
		} else if (SystemCalculateConstants.DISCHARGE_AINTERFACE_ELECTRICDAMPER.equals(AInterface)) {
			configMap.put("FILEPATH_TOP", "D:\\FZ_V.DXF");
			configMap.put("FILEPATH_FRONT", "D:\\FC.DXF");
		} else if (SystemCalculateConstants.DISCHARGE_AINTERFACE_FLANGE.equals(AInterface)) {
			configMap.put("FILEPATH_TOP", "D:\\FZ_V.DXF");
			configMap.put("FILEPATH_FRONT", "D:\\FD.DXF");
		}

		//镀锌板
		if(SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_GL.equals(metrial)){
			if(product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39CQ)!=-1 || product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39XT)!=-1){
				configMap.put("DIMENSION_E", "130");
			}else if(product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39G)!=-1){
				configMap.put("DIMENSION_E", "155");
			}
		}
		//铝合金风阀
		else{
			if(product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39CQ)!=-1 || product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39XT)!=-1){
				configMap.put("DIMENSION_E", "130");
			}else if(product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39G)!=-1){
				configMap.put("DIMENSION_E", "155");
			}
		}

		//不带执行器添加：DIMENSION_E
		String damperExecutor = MapValueUtils.getStringValue(UtilityConstant.METASEXON_DISCHARGE_DAMPEREXECUTOR, partMap);
		if("1".equals(damperExecutor)){
			if(product.indexOf(UtilityConstant.SYS_UNIT_SERIES_39CBFI)!=-1){
				configMap.put("DIMENSION_E", "158");
			}
		}

		configMap.put("FLAN_OUT", "20");
		toSetPartAirDirection(partMap, configMap);
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", IniConfigMapUtil.getDOORDRAWING_key(product)+"_Door_top");
		configMap.put("DOORDRAWING_FRONT", IniConfigMapUtil.getDOORDRAWING_key(product)+"_Door_front");
		configMap.put("PARTDETAIL", "OUTLET");

		//TODO 暂时屏蔽风阀自定义
		//updateDischargeDamperSize(partMap, configMap);

		return configMap;
	}

	/** 根据风阀调整后的尺寸更新出风段三视图配置数据  */
	private static final void updateDischargeDamperSize(Map<String, Object> partMap, Map<String, Object> configMap) {
		String outletDirection = MapValueUtils.getStringValue(
				SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, MetaKey.KEY_OUTLETDIRECTION),
				partMap);
		DamperPosEnum damperPos = getDamperPos(outletDirection);
		if (damperPos != null) {
			updateDischargeDimension("DIMENSION_A", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_A.apply(damperPos));
			updateDischargeDimension("DIMENSION_B", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_B.apply(damperPos));
			updateDischargeDimension("DIMENSION_C", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_C.apply(damperPos));
			updateDischargeDimension("DIMENSION_D", configMap, partMap, MetaKey.KEY_DAMPER_POS_SIZE_D.apply(damperPos));
		} else {
			log.warn("unknown outlet direction of discharge: {}", outletDirection);
		}
	}

	private static DamperPosEnum getDamperPos(String dischargeOutletDirection) {
		if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_A.equals(dischargeOutletDirection)) {
			return DamperPosEnum.Back;
		} else if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_F.equals(dischargeOutletDirection)) {
			return DamperPosEnum.Bottom;
		} else if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_L.equals(dischargeOutletDirection)) {
			return DamperPosEnum.Left;
		} else if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_R.equals(dischargeOutletDirection)) {
			return DamperPosEnum.Right;
		} else if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_T.equals(dischargeOutletDirection)) {
			return DamperPosEnum.Top;
		}
		return null;
	}

	private static void updateDischargeDimension(String dimension, Map<String, Object> configMap,
												 Map<String, Object> partMap, String metaKey) {
		String posSize = MapValueUtils.getStringValue(
				SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, metaKey), partMap);
		if (posSize != null) {
			log.debug("update {} of discharge after adjustment: {}", dimension, posSize);
			configMap.put(dimension, posSize);
		}
	}

	/**
	 * 生成CAD段信息 [空段-015]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection015(AhuParam ahu, PartParam part) {
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		String product = ahu.getProduct();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "15");
		boolean DoorOnBothSide = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_ACCESS_DOORONBOTHSIDE, partMap);

		String doorType = MapValueUtils.getStringValue(UtilityConstant.METASEXON_ACCESS_ODOOR, partMap);
		String ACCESS_ODOOR_DOOR_W_O_VIEWPORT = "Door w/o viewport";
		String ACCESS_ODOOR_DOOR_W__VIEWPORT = "Door w/ viewport";
		String ACCESS_ODOOR_W_O_DOOR = "W/o door";

		if (SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(doorType)) {//开门无观察窗
			configMap.put("DOOR", "1");// 1：开门
			configMap.put("VIEWPORT", "0");//无观察窗
		}
		if (SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(doorType)) {//开门有观察窗
			configMap.put("DOOR", "1");// 1：开门
			configMap.put("VIEWPORT", "1");//有观察窗
		}
		if (SystemCalculateConstants.ACCESS_ODOOR_W_O_DOOR.equals(doorType)) {//不开门
			configMap.put("DOOR", "0");//0：不开门
			configMap.put("VIEWPORT", "0");//无观察窗
		}

		if (DoorOnBothSide) {
			configMap.put("DOORDIRECTION", "3");//3表示两侧开门
		} else {
			configMap.put("DOORDIRECTION", MapValueUtils.getStringValue("DOORDIRECTION", ahuConfigMap));// 1表示左 2表示右 4端面开门
		}
		int sectionL = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_ACCESS_SECTIONL, partMap);
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionL);

		if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
			configMap.put("DOORTYPE", "2");//门类型
		}else{
			configMap.put("DOORTYPE", "1");//门类型
		}

		boolean lamp = MapValueUtils.getBooleanValue(UtilityConstant.METASEXON_ACCESS_FIXREPAIRLAMP, partMap);
		if (lamp) {
			configMap.put("LAMP", "1");
		} else {
			configMap.put("LAMP", "0");
		}

		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahu.getParams());
		String dooris = AhuMetadata.findOne(PaneledDoor.class, serial, "15").getDoor();
		int iFrmThick = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_FRMTHICK, ahuConfigMap);

		configMap.put("DOORDIMENSION", IniConfigMapUtil.getDOORDIMENSION(dooris, sectionL, iFrmThick));

		toSetPartAirDirection(partMap, configMap);
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_top");
		configMap.put("DOORDRAWING_FRONT", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_front");
		configMap.put("PARTDETAIL", "ACCESS");

		return configMap;
	}

	/**
	 * 生成CAD段信息 [冷水+湿膜加湿段-016]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection016(AhuParam ahu, PartParam part) {
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "16");
		configMap.put("BREAKWATERTYPE", "D");
		
		//加湿段段长，有支持支持段长会为空的情况，所以这里重新设置下，如果段长为空，则默认赋值为0
		if(EmptyUtil.isEmpty(partMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_SECTIONL))){
			configMap.put(UtilityConstant.SYS_CAD_SECTIONL, 0);
		}else{
			configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_SECTIONL, partMap));
		}
		
		toSetPartAirDirection(partMap, configMap);
		configMap.put("DIMENSION_SM_THICKNESS",
				MapValueUtils.getStringValue(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_THICKNESS, partMap));

		return configMap;
	}

	/**
	 * 生成CAD段信息 [热水+湿膜加湿段-017]
	 *
	 * @param ahu
	 * @param partPre
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection017(AhuParam ahu, PartParam partPre, PartParam part) {
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> partPreMap = partPre.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "17");
		int sectionL = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_HEATINGCOIL_SECTIONL, partPreMap);
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionL);
		configMap.put("DIMENSION_SM_THICKNESS",
				MapValueUtils.getStringValue(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_THICKNESS, partMap));
		toSetPartAirDirection(partMap, configMap);
		configMap.put("BREAKWATERTYPE", "D");
		configMap.put("DSQPATH", "dsq");
		configMap.put("BWBHEIGHT", "10");
		configMap.put("DRAINPANHEIGHT", "50");
		int ahu17 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_FRMTHICK, ahuConfigMap);
		int ahu19 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_INTERVAL, ahuConfigMap);
		int drainpanwidth = sectionL * 100 + ahu19 - 2 * ahu17;
		configMap.put("DRAINPANWIDTH", drainpanwidth);// SectionL*100+(机组通用配置第19项)-2*(机组通用配置第17项)
		configMap.put("DRAINPANPATH", "shuipan");
		configMap.put("DGORRRAINPAN", "1");
		configMap.put("HEADERLENGTH", "100");
		return configMap;
	}

	/**
	 * 生成CAD段信息 [冷水+高压喷雾加湿段-018]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection018(AhuParam ahu, PartParam part) {
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "18");
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_SECTIONL, partMap));
		toSetPartAirDirection(partMap, configMap);
		configMap.put("DIMENSION_GYPW_L", "420");
		configMap.put("DIMENSION_GYPW_H", "612.5");
		configMap.put("DSQ_STARTPIONT", "963.6");
		String Eliminator = MapValueUtils.getStringValue(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_ELIMINATOR, partMap);
		// 不锈钢挡水器：S
		if (SystemCalculateConstants.SPRAYHUMIDIFIER_ELIMINATOR_SS.equals(Eliminator)) {
			configMap.put("BREAKWATERTYPE", "3");
		}
		// 镀锌板挡水器：?????
		if (SystemCalculateConstants.SPRAYHUMIDIFIER_ELIMINATOR_GS.equals(Eliminator)) {
			configMap.put("BREAKWATERTYPE", "0");
		}
		// 高压喷雾段挡水器 "铝网挡水器：D
		if (SystemCalculateConstants.SPRAYHUMIDIFIER_ELIMINATOR_AS.equals(Eliminator)) {
			configMap.put("BREAKWATERTYPE", "5");
		}
		return configMap;
	}

	/**
	 * 生成CAD段信息 [热水+高压喷雾加湿段-019]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection019(AhuParam ahu, PartParam part) {
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "19");
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_SECTIONL, partMap));
		toSetPartAirDirection(partMap, configMap);
		configMap.put("DIMENSION_GYPW_L", "420");
		configMap.put("DIMENSION_GYPW_H", "612.5");
		configMap.put("DSQ_STARTPIONT", "963.6");
		String Eliminator = MapValueUtils.getStringValue(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_ELIMINATOR, partMap);
		// 不锈钢挡水器：S
		if (SystemCalculateConstants.SPRAYHUMIDIFIER_ELIMINATOR_SS.equals(Eliminator)) {
			configMap.put("BREAKWATERTYPE", "3");
		}
		// 镀锌板挡水器：?????
		if (SystemCalculateConstants.SPRAYHUMIDIFIER_ELIMINATOR_GS.equals(Eliminator)) {
			configMap.put("BREAKWATERTYPE", "0");
		}
		// 高压喷雾段挡水器 "铝网挡水器：D
		if (SystemCalculateConstants.SPRAYHUMIDIFIER_ELIMINATOR_AS.equals(Eliminator)) {
			configMap.put("BREAKWATERTYPE", "5");
		}
		return configMap;
	}
	/**
	 * 生成CAD段信息 [冷水+热水-021]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection021(AhuParam ahu, PartParam part) {
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "21");
		toSetPartAirDirection(partMap, configMap);
		String product = ahu.getProduct();

		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		int len = serial.length();
		String xilie = serial.substring(len - 4);
		int unitwidth = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_UNITWIDTH, ahuConfigMap);
		int baseheight = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_BASEHEIGHT, ahuConfigMap);
		int ahu17 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_FRMTHICK, ahuConfigMap);
		int ahu19 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_INTERVAL, ahuConfigMap);
		// 段长*100+(机组通用配置第19项)-2*(机组通用配置第17项)
		int sectionL = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_HEATINGCOIL_SECTIONL, partMap);
		int sectionL_C = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_COOLINGCOIL_SECTIONL, partMap);
		int drainpanwidth = (sectionL_C+sectionL) * 100 + ahu19 - 2 * ahu17;//水盘宽=冷水+热水

		// 机组宽度*100+(机组通用配置第19项)- 2*(机组通用配置第17项)
		int coilwidth = unitwidth + ahu19 - 2 * ahu17;
		int rows = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_HEATINGCOIL_ROWS, partMap);
		if (SystemCountUtil.gteBigUnit(Integer.parseInt(xilie))) {
			CoilBigTuojiaDimension nsion = AhuMetadata.findOne(CoilBigTuojiaDimension.class, serial);
			if (EmptyUtil.isEmpty(nsion)) {
				logger.error("查表未获取到CoilBigTJDimension @冷水盘管段托架计算时");
				configMap.put("TUOJIA1_H", "0");
				configMap.put("TUOJIA1_L", "0");
			} else {
				configMap.put("TUOJIA1_H", nsion.getB());
				configMap.put("TUOJIA1_L", nsion.getW());
			}
			configMap.put("TUOJIADRAWING","HCTJ1");


			CoilBigDimension nsioLnew = AhuMetadata.findOne(CoilBigDimensionL.class, serial);;
			if (EmptyUtil.isEmpty(nsioLnew)) {
				logger.error("查表未获取到CoilBigDimensionLNew @冷水盘管段计算时");
			} else {
				configMap.put("COLITHICKNESS_H", nsioLnew.getCoilw());
				configMap.put("COILHEIGHT_H", nsioLnew.getCoilh());
				configMap.put("FB_H", Integer.parseInt(nsioLnew.getB())-baseheight);
				configMap.put("FC_H", nsioLnew.getC());
				configMap.put("FG_H", nsioLnew.getG());
				if (rows == 4) {
					configMap.put("FD_H", nsioLnew.getD4r());
					configMap.put("FE_H", nsioLnew.getE4r());
				}
				if (rows == 6) {
					configMap.put("FD_H", nsioLnew.getD6r());
					configMap.put("FE_H", nsioLnew.getE6r());
				}
				if (rows == 8) {
					configMap.put("FD_H", nsioLnew.getD8r());
					configMap.put("FE_H", nsioLnew.getE8r());
				}
				configMap.put("SDIA_H", nsioLnew.getDia());
			}
		} else {
			CoilDimension nsion = AhuMetadata.findOne(CoilDimension.class, serial);;
			if (EmptyUtil.isEmpty(nsion)) {
				logger.error("查表未获取到CoilBigDimensionlNew @冷水盘管段计算时");
			} else {
				String coilw = SYS_BLANK;
				String coilh = nsion.getHeight();
				if (rows == 1) {
					coilw = nsion.getR1();
				} else if (rows == 2) {
					coilw = nsion.getR2();
				} else if (rows == 3) {
					coilw = nsion.getR3();
				} else if (rows == 4) {
					coilw = nsion.getR4();
				} else if (rows == 5) {
					coilw = nsion.getR5();
				} else if (rows == 6) {
					coilw = nsion.getR6();
				} else if (rows == 7) {
					coilw = nsion.getR7();
				} else if (rows == 8) {
					coilw = nsion.getR8();
				} else if (rows == 10) {
					coilw = nsion.getR10();
				} else if (rows == 12) {
					coilw = nsion.getR12();
				}
				configMap.put("COLITHICKNESS_H", coilw);
				configMap.put("COILHEIGHT_H", coilh);

				if (rows > 2) {
					ColdCoilDimension ccnsion = AhuMetadata.findOne(ColdCoilDimension.class, serial);
					if (EmptyUtil.isEmpty(ccnsion)) {
						logger.error("查表未获取到ColdCoilDimensionNew @冷水盘管段计算时");
					} else {
						configMap.put("FB_H", Integer.parseInt(ccnsion.getB()) + 25 - baseheight);
						if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
							configMap.put("FB_H", Integer.parseInt(ccnsion.getB())-baseheight);
						}
						int b = Integer.parseInt(ccnsion.getB());
						configMap.put("FB_H", b + 25 - baseheight);
						if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
							configMap.put("FB_H", b - baseheight);
						}
						configMap.put("FC_H", ccnsion.getC());
						if (rows == 3) {
							configMap.put("FD_H", ccnsion.getRd3());
							configMap.put("FE_H", ccnsion.getRe3());
						} else if (rows == 4) {
							configMap.put("FD_H", ccnsion.getRd4());
							configMap.put("FE_H", ccnsion.getRe4());
						} else if (rows == 5) {
							configMap.put("FD_H", ccnsion.getRd5());
							configMap.put("FE_H", ccnsion.getRe5());
						} else if (rows == 6) {
							configMap.put("FD_H", ccnsion.getRd6());
							configMap.put("FE_H", ccnsion.getRe6());
						} else if (rows == 7) {
							configMap.put("FD_H", ccnsion.getRd7());
							configMap.put("FE_H", ccnsion.getRe7());
						} else if (rows == 8) {
							configMap.put("FD_H", ccnsion.getRd8());
							configMap.put("FE_H", ccnsion.getRe8());
						} else if (rows == 10) {
							configMap.put("FD_H", ccnsion.getRd10());
							configMap.put("FE_H", ccnsion.getRe10());
						} else if (rows == 12) {
							configMap.put("FD_H", ccnsion.getRd12());
							configMap.put("FE_H", ccnsion.getRe12());
						}
						configMap.put("FG_H", ccnsion.getG());
						configMap.put("SDIA_H", ccnsion.getDia());
					}
				} else {
					HeatCoilDimension hcnsion = AhuMetadata.findOne(HeatCoilDimension.class, serial);
					if (EmptyUtil.isEmpty(hcnsion)) {
						logger.error("查表未获取到ColdCoilDimensionNew @冷水盘管段计算时");
					} else {
						configMap.put("FB_H", Integer.parseInt(hcnsion.getB()) + 25 - baseheight);
						if (UtilityConstant.SYS_UNIT_SERIES_39G.equals(product)) {
							configMap.put("FB_H", Integer.parseInt(hcnsion.getB())-baseheight);
						}

						configMap.put("FC_H",hcnsion.getC());
						configMap.put("FD_H",hcnsion.getD());
						configMap.put("FE_H",hcnsion.getE());
						configMap.put("FG_H",hcnsion.getG());
						configMap.put("SDIA_H",hcnsion.getDia());
					}
				}
			}
		}

		configMap.put("COILWIDTH_H", coilwidth);
		configMap.put("DGHEIGHT","50");
		configMap.put("TRAYLEFT",SYS_BLANK);
		configMap.put("TRAYRIGHT",SYS_BLANK);

		configMap.put("DGHEIGHT",50);
		configMap.put("HEADERLENGTH_H",100);
		configMap.put("DRAINPANWIDTH", drainpanwidth);
		configMap.put("SECTIONL_H", sectionL);
		return configMap;
	}
	/**
	 * 生成CAD段信息 [高效过滤段-022]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection022(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		String product = ahu.getProduct();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "22");
		String type = MapValueUtils.getStringValue(UtilityConstant.METASEXON_HEPAFILTER_FITETF, partMap);
		String format = "5";
		configMap.put("FILTERTYPE", "1");
		if (SystemCalculateConstants.HEPAFILTER_FILTERF_P.equals(type)) {
			configMap.put("FILTERTYPE", "2");
			format = "6";
		}
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_HEPAFILTER_SECTIONL, partMap));
		configMap.put("PRESSUREGAGE", "0");
		configMap.put("FILTERTHICKNESSX", "250");
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		FilterDrawing filterdrawing = AhuMetadata.findOne(FilterDrawing.class, serial, format);;
		if (EmptyUtil.isEmpty(filterdrawing)) {
			logger.error("查表未获取到FilterDrawing @高效过滤段计算时");
			configMap.put("FLTFRONT_DRAWING", SYS_BLANK);
			configMap.put("FLTFRONT_DIMENSION", SYS_BLANK);
			configMap.put("FLTTOP_DRAWING", SYS_BLANK);
			configMap.put("FLTTOP_DIMENSION", SYS_BLANK);
		} else {
			configMap.put("FLTFRONT_DRAWING", filterdrawing.getFrontDrawing());
			configMap.put("FLTFRONT_DIMENSION", filterdrawing.getFrontDimension());
			configMap.put("FLTTOP_DRAWING", filterdrawing.getTopDrawing());
			configMap.put("FLTTOP_DIMENSION", filterdrawing.getTopDimension());
		}
		toSetPartAirDirection(partMap, configMap);
		configMap.put("STARTPOINT", "0");
		configMap.put("PARTDETAIL", "HE FILTER");
		configMap.put("DOOR", "1");
		configMap.put("DOORDIRECTION", "1");
		configMap.put("DOORTYPE", "2");
		configMap.put("LAMP", "0");
		configMap.put("VIEWPORT", "0");
		int lenth = serial.length();
		configMap.put("DOORDIMENSION", "428*"+(Integer.parseInt(serial.substring(lenth - 4, lenth - 2))*100-72));
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_top");
		configMap.put("DOORDRAWING_FRONT", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_front");
		return configMap;
	}

	/**
	 * 生成CAD段信息 [电极加湿器段-024]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection024(AhuParam ahu, PartParam part) {
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "24");
		int sectionL = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_SECTIONL, partMap);
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionL);
		configMap.put("PARTDETAIL", "ELEC HUMID");
		return configMap;
	}

	/**
	 * 生成CAD段信息 [静电过滤段-025]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection025(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "25");
		int sectionL = MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_ELECTROSTATICFILTER_SECTIONL, partMap);
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, sectionL);
		configMap.put("FILTERTHICKNESSX", sectionL * 100 - 50);
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		FilterDrawing filterdrawing = AhuMetadata.findOne(FilterDrawing.class, serial, "7");
		if (EmptyUtil.isEmpty(filterdrawing)) {
			logger.error("查表未获取到FilterDrawing @静电过滤段计算时");
		} else {
			configMap.put("FLTFRONT_DIMENSION", filterdrawing.getFrontDimension());
			configMap.put("FLTFRONT_DRAWING", filterdrawing.getFrontDrawing());
			configMap.put("FLTTOP_DRAWING", filterdrawing.getTopDrawing());
			configMap.put("FLTTOP_DIMENSION", filterdrawing.getTopDimension());
		}
		toSetPartAirDirection(partMap, configMap);
		configMap.put("STARTPOINT", "0");
		configMap.put("PARTDETAIL", "Electrostatic Filter");
		configMap.put("DOOR", "0");
		int lenth = serial.length();
		configMap.put("DOORDIMENSION", "428*"+(Integer.parseInt(serial.substring(lenth - 4, lenth - 2))*100-72));
		configMap.put("DOORHANDLEDIMENSION", "22*158");

		return configMap;
	}

	/**
	 * 生成CAD段信息 [CTR控制段-026]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection026(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		String product = ahu.getProduct();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "26");
		configMap.put("DOORDIRECTION", MapValueUtils.getStringValue("DOORDIRECTION", ahuConfigMap));
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_CTR_SECTIONL, partMap));
		configMap.put("DOORTYPE", "1");
		String accessDoor = MapValueUtils.getStringValue(UtilityConstant.METASEXON_CTR_DOORO, partMap);
		if (SystemCalculateConstants.CTR_DOORO_DOORWITHVIEWPORT.equals(accessDoor)) {
			configMap.put("DOOR", "1");
			configMap.put("VIEWPORT", "1");
		} else if (SystemCalculateConstants.CTR_DOORO_DOORNOVIEWPORT.equals(accessDoor)) {
			configMap.put("DOOR", "1");
			configMap.put("VIEWPORT", "0");
		} else if (SystemCalculateConstants.CTR_DOORO_NODOOR.equals(accessDoor)) {
			configMap.put("DOOR", "0");
			configMap.put("VIEWPORT", "0");
		}
		configMap.put("LAMP", "0");
		toSetPartAirDirection(partMap, configMap);
		String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		configMap.put("DOORDIMENSION", AhuMetadata.findOne(PaneledDoor.class, serial, "26").getDoor());
		configMap.put("DOORHANDLEDIMENSION", "22*158");
		configMap.put("DOORDRAWING_TOP", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_top");
		configMap.put("DOORDRAWING_FRONT", IniConfigMapUtil.getDOORDRAWING_key(product) +"_Door_front");
		configMap.put("PARTDETAIL", "Control");

		return configMap;
	}

	/**
	 * 生成CAD段信息 [转轮热回收段-043]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection043(AhuParam ahu, PartParam part) {
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "23");
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_WHEELHEATRECYCLE_SECTIONL, partMap));

		// 机组宽度*100+(机组通用配置第19项)- 2*(机组通用配置第17项)
		int unitheight = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_UNITHEIGHT, ahuConfigMap);
		int ahu17 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_FRMTHICK, ahuConfigMap);
		int ahu19 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_INTERVAL, ahuConfigMap);
		int hh = 2*(unitheight + ahu19) - 2 * ahu17;
		configMap.put("HEATHEIGHT", hh);//热回收高度

		String wheelDepth = MapValueUtils.getStringValue(UtilityConstant.METASEXON_WHEELHEATRECYCLE_WHEELDEPTH, partMap);
		if(wheelDepth.equals(JSON_PLATEHEATRECYCLE_WHEELDEPTH_1)){
			configMap.put("HEATTHICKNESS", JSON_PLATEHEATRECYCLE_WHEELDEPTH_1_VAl);// 转轮厚度
		}
		if(wheelDepth.equals(JSON_PLATEHEATRECYCLE_WHEELDEPTH_2)){
			configMap.put("HEATTHICKNESS", JSON_PLATEHEATRECYCLE_WHEELDEPTH_2_VAl);// 转轮厚度
		}


		configMap.put("FRAMEOPTION", "0");//框架形式 0表示正常上下层分开, 1表示上下为一个框架

		int hh1 = unitheight + ahu19 - 2 * ahu17;
		configMap.put("HEATWIDTH", hh1);// 热回收高度
		configMap.put("WHEELTYPE", "2");


		//非标（无转轮）
		/*if VarpartW.NotMount='-1' then
	            begin
	              Stor.Writestring('PART'+INTTOSTR(parmIndex1), 'DRAWING_TOP','Heat_Top');
	              Stor.Writestring('PART'+INTTOSTR(parmIndex1), 'DRAWING_FRONT','Heat_Front');
	            end
	          else
	            begin
	              Stor.Writestring('PART'+INTTOSTR(parmIndex1), 'DRAWING_TOP','HeatWheel_Top');
	              Stor.Writestring('PART'+INTTOSTR(parmIndex1), 'DRAWING_FRONT','HeatWheel_Front');
	            end;
	            */
		configMap.put("DRAWING_TOP", "HeatWheel_Top");
		configMap.put("DRAWING_FRONT", "HeatWheel_Front");

		toSetPartAirDirection(partMap, configMap);
		return configMap;
	}

	/**
	 * 生成CAD段信息 [板式热回收段-023]
	 *
	 * @param ahu
	 * @param part
	 * @return
	 */
	private static final Map<String, Object> configMapSection023(AhuParam ahu, PartParam part) {
		Map<String, Object> ahuMap = ahu.getParams();
		Map<String, Object> partMap = part.getParams();
		Map<String, Object> configMap = new LinkedHashMap<>();
		configMap.put(UtilityConstant.SYS_CAD_TYPE, "23");
		configMap.put(UtilityConstant.SYS_CAD_SECTIONL, MapValueUtils.getIntegerValue(UtilityConstant.METASEXON_PLATEHEATRECYCLE_SECTIONL, partMap));

		// 机组宽度*100+(机组通用配置第19项)- 2*(机组通用配置第17项)
		int unitheight = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_UNITHEIGHT, ahuConfigMap);
		int ahu17 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_FRMTHICK, ahuConfigMap);
		int ahu19 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_INTERVAL, ahuConfigMap);
		int hh = 2*(unitheight + ahu19) - 2 * ahu17;
		configMap.put("HEATHEIGHT", hh);//热回收高度


		String sectionSideL = MapValueUtils.getStringValue(UtilityConstant.METASEXON_PLATEHEATRECYCLE_SECTIONSIDEL, partMap);

		/*String serial = MapValueUtils.getStringValue(UtilityConstant.METAHU_SERIAL, ahuMap);
		PartWPlate partWPlate = AhuMetadata.findOne(PartWPlate.class, serial);*/
		if (null != sectionSideL) {
			configMap.put("HEATTHICKNESS", sectionSideL);// 转轮厚度
		} else {
			configMap.put("HEATTHICKNESS", "0");// 转轮厚度
		}
		configMap.put("FRAMEOPTION", "0");//框架形式 0表示正常上下层分开, 1表示上下为一个框架
		int hh1 = unitheight + ahu19 - 2 * ahu17;
		configMap.put("HEATWIDTH", hh1);//热回收高度
		configMap.put("WHEELTYPE", "1");


		//非标（无转轮）
				/*if VarpartW.NotMount='-1' then
			            begin
			              Stor.Writestring('PART'+INTTOSTR(parmIndex1), 'DRAWING_TOP','Heat_Top');
			              Stor.Writestring('PART'+INTTOSTR(parmIndex1), 'DRAWING_FRONT','Heat_Front');
			            end
			          else
			            begin
			              Stor.Writestring('PART'+INTTOSTR(parmIndex1), 'DRAWING_TOP','HeatWheel_Top');
			              Stor.Writestring('PART'+INTTOSTR(parmIndex1), 'DRAWING_FRONT','HeatWheel_Front');
			            end;
			            */
		configMap.put("DRAWING_TOP", "PlateWheel_Top");
		configMap.put("DRAWING_FRONT", "PlateWheel_Front");


		toSetPartAirDirection(partMap, configMap);
		return configMap;
	}

	private static Map<String, Object> getSectionConfigMap(String key, AhuParam ahu, PartParam part) {
		switch (SectionTypeEnum.getSectionTypeFromId(key)) {
			case TYPE_MIX:
				return configMapSection001(ahu, part);
			case TYPE_SINGLE:
				return configMapSection002(ahu, part);
			case TYPE_COMPOSITE:
				return configMapSection003(ahu, part);
			case TYPE_COLD: {
				Map<String, Object> map004 = configMapSection004(ahu, part);
				PartParam partNext = getNextPartParam(ahu, part.getPosition());
				if (EmptyUtil.isNotEmpty(partNext)) {
					switch (SectionTypeEnum.getSectionTypeFromId(partNext.getKey())) {
						case TYPE_WETFILMHUMIDIFIER: {
							Map<String, Object> map016 = configMapSection016(ahu, partNext);
							int sectionL016 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_SECTIONL, map016);
							int sectionL004 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_SECTIONL, map004);
							map004.putAll(map016);
							map004.put(UtilityConstant.SYS_CAD_SECTIONL, sectionL004 + sectionL016);
							return map004;
						}
						case TYPE_SPRAYHUMIDIFIER: {
							Map<String, Object> map018 = configMapSection018(ahu, partNext);

							int sectionL018 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_SECTIONL, map018);
							int sectionL004 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_SECTIONL, map004);
							map004.putAll(map018);
							map004.put(UtilityConstant.SYS_CAD_SECTIONL, sectionL004 + sectionL018);

							return map004;
						}
						case TYPE_HEATINGCOIL: {

							//冷水法兰、连接方式
							boolean hasEliminator = true;
							String eliminator = String.valueOf(part.getParams().get(METASEXON_COOLINGCOIL_ELIMINATOR));
							if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator)) {
								hasEliminator = false;
							}
							boolean isFL = false;
							String connections = String.valueOf(part.getParams().get(METASEXON_COOLINGCOIL_CONNECTIONS));
							String connectionsHeat = String.valueOf(partNext.getParams().get(METASEXON_HEATINGCOIL_CONNECTIONS));
							if (SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_PAIRSFLANGE.equals(connections)
									||SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_PAIRSFLANGE.equals(connectionsHeat)) {
								isFL = true;
							}

							String xilie = ahu.getSeries().substring(ahu.getSeries().length() - 4);
							//大型号机组、挡水器、法兰,不合并写入ini冷水单独写入
							if(SystemCountUtil.gteBigUnit(Integer.parseInt(xilie)) || hasEliminator || isFL){
								//冷水后面是热水，2532以后需要不合并写入ini，冷水单独写入
								return map004;
							}

							//冷水+热水+湿膜：不合并写入ini冷水单独写入
							PartParam coolingCoilPartNext = getNextPartParam(ahu, partNext.getPosition());
							if(EmptyUtil.isNotEmpty(coolingCoilPartNext) && coolingCoilPartNext.getKey().equals(TYPE_WETFILMHUMIDIFIER.getId())){
								return map004;
							}

							int sectionL004 = MapValueUtils.getIntegerValue(UtilityConstant.SYS_CAD_SECTIONL, map004);
							partNext.getParams().put(UtilityConstant.METASEXON_COOLINGCOIL_SECTIONL,sectionL004);
							Map<String, Object> map021 = configMapSection021(ahu, partNext);
							map004.putAll(map021);
							return map004;
						}
						default:
							return map004;
					}
				}
				return map004;
			}
			case TYPE_DIRECTEXPENSIONCOIL:
				return configMapSection004(ahu, part);
			case TYPE_HEATINGCOIL: {
				PartParam partPre = getPrePartParam(ahu, part.getPosition());
				PartParam partNext = getNextPartParam(ahu, part.getPosition());
				boolean partNextIsWet = false;
				if(EmptyUtil.isNotEmpty(partNext) && SectionTypeEnum.getSectionTypeFromId(partNext.getKey()) == SectionTypeEnum.TYPE_WETFILMHUMIDIFIER){
					partNextIsWet = true;
				}
				boolean partNextIsSpray = false;
				if(EmptyUtil.isNotEmpty(partNext) && SectionTypeEnum.getSectionTypeFromId(partNext.getKey()) == SectionTypeEnum.TYPE_SPRAYHUMIDIFIER){
					partNextIsSpray = true;
				}

				//冷水热水特殊处理(热水后面湿膜不能进行 冷+热 特殊处理)
				if(null != partPre && SectionTypeEnum.getSectionTypeFromId(partPre.getKey()) == SectionTypeEnum.TYPE_COLD
						&& !partNextIsWet){
					//冷水法兰、连接方式
					boolean hasEliminator = true;
					String eliminator = String.valueOf(partPre.getParams().get(METASEXON_COOLINGCOIL_ELIMINATOR));
					if (SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE.equals(eliminator)) {
						hasEliminator = false;
					}
					boolean isFL = false;
					String connections = String.valueOf(partPre.getParams().get(METASEXON_COOLINGCOIL_CONNECTIONS));
					String connectionsHeat = String.valueOf(part.getParams().get(METASEXON_HEATINGCOIL_CONNECTIONS));

					if (SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_PAIRSFLANGE.equals(connections)
							|| SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_PAIRSFLANGE.equals(connectionsHeat)) {
						isFL = true;
					}

					String xilie = ahu.getSeries().substring(ahu.getSeries().length() - 4);
					if(SystemCountUtil.gteBigUnit(Integer.parseInt(xilie)) || hasEliminator || isFL){//大型号机组、挡水器、法兰,不合并写入ini热水单独写入
						//2532以后需要不合并写入ini，热水单独写入
						return configMapSection005(ahu, part);
					}else{
						//热水盘管前面是冷水盘管，不用重复处理ini信息
						return null;
					}
				}

				Map<String, Object> map005 = configMapSection005(ahu, part);
				if(partNextIsWet){
					Map<String, Object> map017 = configMapSection017(ahu, part,partNext);
					map005.putAll(map017);
					return map005;
				}/*else if(partNextIsSpray){
					Map<String, Object> map019 = configMapSection019(ahu, partNext);
					map005.putAll(map019);
					return map005;
				}*/
				return map005;
			}
			case TYPE_STEAMCOIL:
				return configMapSection006(ahu, part);
			case TYPE_ELECTRICHEATINGCOIL:
				return configMapSection007(ahu, part);
			case TYPE_STEAMHUMIDIFIER:
				return configMapSection008(ahu, part);
			case TYPE_FAN:
				return configMapSection011(ahu, part);
			case TYPE_COMBINEDMIXINGCHAMBER:
				return configMapSection012(ahu, part);
			case TYPE_ATTENUATOR:
				return configMapSection013(ahu, part);
			case TYPE_DISCHARGE:
				return configMapSection014(ahu, part);
			case TYPE_ACCESS:
				return configMapSection015(ahu, part);
			case TYPE_HEPAFILTER:
				return configMapSection022(ahu, part);
			case TYPE_PLATEHEATRECYCLE:
				return configMapSection023(ahu, part);
			case TYPE_ELECTRODEHUMIDIFIER:
				return configMapSection024(ahu, part);
			case TYPE_ELECTROSTATICFILTER:
				return configMapSection025(ahu, part);
			case TYPE_CTR:
				return configMapSection026(ahu, part);
			case TYPE_WHEELHEATRECYCLE:
				return configMapSection043(ahu, part);
			default:
				return null;
		}
	}

	private static void writeSection(String sectionName, Map<String, Object> configMap, PrintWriter writer) {
		writer.printf("[%s]\n", sectionName);
		for (Entry<String, Object> entry : configMap.entrySet()) {
			writer.printf("%s=%s\n", entry.getKey(), entry.getValue() == null ? SYS_BLANK : entry.getValue());
		}
	}

	private static String format(String pattern, Object... arguments) {
		return MessageFormat.format(pattern, arguments);
	}

	/**
	 * 获取当前位置的前一个段
	 *
	 * @param ahu
	 * @param position
	 *            当前位置
	 * @return
	 */
	public static PartParam getPrePartParam(AhuParam ahu, Short position) {
		List<PartParam> partParamsList = ahu.getPartParams();
		for (PartParam part : partParamsList) {
			if (part.getPosition() == position - 1) {
				return part;
			}
		}
		return null;
	}

	/**
	 * 获取当前位置的下一个段
	 *
	 * @param ahu
	 * @param position
	 *            当前位置
	 * @return
	 */
	public static PartParam getNextPartParam(AhuParam ahu, Short position) {
		List<PartParam> partParamsList = ahu.getPartParams();
		for (PartParam part : partParamsList) {
			if (part.getPosition() == position + 1) {
				return part;
			}
		}
		return null;
	}
	/**
	 * 获取当前位置的下下一个段
	 *
	 * @param ahu
	 * @param position
	 *            当前位置
	 * @return
	 */
	public static PartParam getNextNextPartParam(AhuParam ahu, Short position) {
		List<PartParam> partParamsList = ahu.getPartParams();
		for (PartParam part : partParamsList) {
			if (part.getPosition() == position + 2) {
				return part;
			}
		}
		return null;
	}

}
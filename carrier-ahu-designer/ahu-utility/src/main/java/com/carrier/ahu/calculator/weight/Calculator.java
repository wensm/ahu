package com.carrier.ahu.calculator.weight;

import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;

import lombok.Data;

/**
 * 重量计算逻辑的入口，针对业务的每一条计算规则
 * 
 * @author liujianfeng
 *
 */
@Data
public abstract class Calculator {

	public static final double UNKNOWN_VALUE = -1; // 未知的值

	private String name;

	public Calculator(String name) {
		this.name = name;
	}

	public double weight(AhuParam ahu, PartParam part) {
		return 0;
	}

	public String getType() {
		return UtilityConstant.METACOMMON_TYPE_SECTION;
	}
}

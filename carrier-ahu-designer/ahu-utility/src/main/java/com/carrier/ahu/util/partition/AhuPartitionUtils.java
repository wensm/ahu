package com.carrier.ahu.util.partition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.calculator.CalculatorModel;
import com.carrier.ahu.calculator.CalculatorUtil;
import com.carrier.ahu.calculator.WeightCalculator;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.FaceTypeEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.google.common.primitives.Ints;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by Braden Zhou on 2018/09/26.
 */
@Slf4j
public final class AhuPartitionUtils {

    private static WeightCalculator weightCalculator = new WeightCalculator();
    private static CalculatorModel calculatorModel = weightCalculator.getCalModelInstance();

    private static List<CalculatorSpec> getSpec() {
        return calculatorModel.getSpec();
    }

    /**
     * Parse AhuPartition from partitionJson, and set top layer boolean value. </br>
     * Add other property if needed. </br>
     * The single layer unit, all partitions are at top layer.
     * 
     * @param unit
     * @param partition
     * @return
     */
    public static List<AhuPartition> parseAhuPartition(Unit unit, Partition partition) {
        List<AhuPartition> ahuPartitions = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
        AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
        if (null != ahuLayout && isDoubleLayer(ahuLayout)) {
            List<Integer> layerSections = getLayerSections(ahuLayout, false);
            for (AhuPartition ahuPartition : ahuPartitions) {
                for (Map<String, Object> section : ahuPartition.getSections()) {
                    short position = AhuPartition.getPosOfSection(section);
                    if (layerSections.contains(Integer.valueOf(position))) {
                        ahuPartition.setTopLayer(true);
                    } else {
                        ahuPartition.setTopLayer(false);
                    }
                    break;
                }
            }
        } else {
            ahuPartitions.forEach(ahuPartition -> {
                ahuPartition.setTopLayer(false); // all as bottom layer if only single layer
            });
        }
        // update face type
        for (AhuPartition ahuPartition : ahuPartitions) {
            List<Integer> leftFaceSections = getFaceSections(ahuLayout, true);
            List<Integer> rightFaceSections = getFaceSections(ahuLayout, false);
            boolean hasLeft = false;
            boolean hasRight = false;
            for (Map<String, Object> section : ahuPartition.getSections()) {
                short position = AhuPartition.getPosOfSection(section);
                if (leftFaceSections.contains(Integer.valueOf(position))) {
                    hasLeft = true;
                } else if (rightFaceSections.contains(Integer.valueOf(position))) {
                    hasRight = true;
                }
            }
            if (hasLeft && hasRight) {
                ahuPartition.setFaceType(FaceTypeEnum.LEFT_RIGHT);
            } else if (hasLeft) {
                ahuPartition.setFaceType(FaceTypeEnum.LEFT_ONLY);
            } else if (hasRight) {
                ahuPartition.setFaceType(FaceTypeEnum.RIGHT_ONLY);
            } else {
                ahuPartition.setFaceType(FaceTypeEnum.NONE);
            }

            //update series 机组型号
            ahuPartition.setSeries(unit.getSeries());
        }
        return ahuPartitions;
    }

    public static List<AhuPartition> getTopLayerPartitions(Unit unit, Partition partition) {
        return getLayerPartitions(unit, partition, false);
    }

    public static List<AhuPartition> getBottomLayerPartitions(Unit unit, Partition partition) {
        return getLayerPartitions(unit, partition, true);
    }

    public static List<AhuPartition> getLayerPartitions(Unit unit, Partition partition, boolean isBottom) {
        List<AhuPartition> ahuPartitions = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
        AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
        if (isDoubleLayer(ahuLayout)) {
            List<AhuPartition> layerPartitions = new ArrayList<>();
            List<Integer> layerSections = getLayerSections(ahuLayout, isBottom);
            for (AhuPartition ahuPartition : ahuPartitions) {
                for (Map<String, Object> section : ahuPartition.getSections()) {
                    short position = AhuPartition.getPosOfSection(section);
                    if (layerSections.contains(Integer.valueOf(position))) {
                        layerPartitions.add(ahuPartition);
                        break;
                    }
                }
            }
            return layerPartitions;
        }
        return isBottom ? ahuPartitions : new ArrayList<>();
    }

    private static List<Integer> getFaceSections(AhuLayout ahuLayout, boolean isLeft) {
        List<Integer> topLayerSections = new ArrayList<>();
        List<Integer> bottomLayerSections = new ArrayList<>();
        
        if(EmptyUtil.isEmpty(ahuLayout)) {
        	return new ArrayList<Integer>();
        }

        int[][] layoutData = ahuLayout.getLayoutData();
        
        if (layoutData.length > 1) { // double layer unit
            topLayerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[0]));
            topLayerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[1]));
            bottomLayerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[2]));
            bottomLayerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[3]));
        } else { // single layer unit
            topLayerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[0]));
        }

        List<Integer> faceSections = new ArrayList<>();
        if (isLeft) { // left face
            if (topLayerSections.size() > 0) {
                faceSections.add(topLayerSections.get(0));
            }
            if (bottomLayerSections.size() > 0) {
                faceSections.add(bottomLayerSections.get(0));
            }
        } else { // right face
            if (topLayerSections.size() > 0) {
                faceSections.add(topLayerSections.get(topLayerSections.size() - 1));
            }
            if (bottomLayerSections.size() > 0) {
                faceSections.add(bottomLayerSections.get(bottomLayerSections.size() - 1));
            }
        }
        return faceSections;
    }

    public static boolean isDoubleLayer(AhuLayout ahuLayout) {
        if (ahuLayout != null) {
            LayoutStyleEnum layoutStyle = LayoutStyleEnum.valueStyleOf(ahuLayout.getStyle());
            return layoutStyle != null ? layoutStyle.isDoubleLayer() : false;
        }
        return false;
    }

    public static boolean isDoubleLayer(Unit unit) {
        AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
        return isDoubleLayer(ahuLayout);
    }

    public static List<Integer> getLayerSections(AhuLayout ahuLayout, boolean isBottom) {
        List<Integer> layerSections = new ArrayList<>();
        layerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[isBottom ? 2 : 0]));
        layerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[isBottom ? 3 : 1]));
        return layerSections;
    }

    public static List<PartPO> getAirflowOrderedPartPOList(Unit unit, List<PartPO> disorderParts) {
        List<Integer> airflowSections = getAirflowSections(unit);
        if (airflowSections != null) {
            PartPO[] orderedParts = new PartPO[airflowSections.size()];
            for (PartPO part : disorderParts) {
                int position = part.getCurrentPart().getPosition().intValue();
                int indexOf = airflowSections.indexOf(position);
                if (indexOf != -1) {
                    orderedParts[indexOf] = part;
                }
            }
            return Arrays.asList(orderedParts);
        }
        return disorderParts;
    }

    /**
     * 非板式热回收，按照风向排序
     * 板式热回收报告不需要按照风向，直接从最大pos 到pos=0 进行排序。
     * @param unit
     * @param disorderParts
     * @return
     */
    public static List<PartPO> getAirflowOrderedPartPOListForReport(Unit unit, List<PartPO> disorderParts) {
        List<Integer> airflowSections = getAirflowSections(unit);

        AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
        if(EmptyUtil.isNotEmpty(ahuLayout) && LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()){
            List<PartPO> orderedParts = new ArrayList<>();
            boolean hasPassPlate = false;
            for (int i=disorderParts.size()-1;i>=0;i--) {
                if (SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(disorderParts.get(i).getCurrentPart().getSectionKey())) {
                    if(hasPassPlate){
                        orderedParts.add(disorderParts.get(i));
                    }else{
                        hasPassPlate = true;
                    }
                    continue;
                }
                orderedParts.add(disorderParts.get(i));
            }
            return orderedParts;
        }

        if (airflowSections != null) {
            PartPO[] orderedParts = new PartPO[airflowSections.size()];
            for (PartPO part : disorderParts) {
                int position = part.getCurrentPart().getPosition().intValue();
                int indexOf = airflowSections.indexOf(position);
                if (indexOf != -1) {
                    orderedParts[indexOf] = part;
                }
            }
            return Arrays.asList(orderedParts);
        }
        return disorderParts;
    }

    /**
     * 风向list不忽略热回收
     * @param unit
     * @param disorderParts
     * @return
     */
    public static List<Part> getAirflowOrderedPartListNoIgnore(Unit unit, List<Part> disorderParts) {
        if(EmptyUtil.isNotEmpty(disorderParts)){
            String partKey = disorderParts.get(0).getSectionKey();
            boolean flag = false;
            for(Part part:disorderParts){
                if(!partKey.equals(part.getSectionKey())){
                    flag = true;
                }
            }
            if(!flag){
                return disorderParts;
            }
            
        }
        
        List<Integer> airflowSections = getAirflowSectionsNoIgnore(unit);
        if (airflowSections != null) {
            Part[] orderedParts = new Part[airflowSections.size()];
            for (Part part : disorderParts) {
                int position = part.getPosition().intValue();
                int indexOf = airflowSections.indexOf(position);
                if (indexOf != -1) {
                    orderedParts[indexOf] = part;
                }
            }
            //计算盘管、风机时候，如果开启“计算价格”开关，会针对盘管风机单独调用价格dll，导致出现orderedParts里面为空的现象，需要过滤到空对象，防止价格计算出错。
            List<Part> retList = new ArrayList<>();
            for (Part orderedPart : orderedParts) {
                if(EmptyUtil.isNotEmpty(orderedPart)){
                    retList.add(orderedPart);
                }
            }
            return retList;
        }
        return disorderParts;
    }
    public static List<Part> getAirflowOrderedPartList(Unit unit, List<Part> disorderParts) {
        List<Integer> airflowSections = getAirflowSections(unit);
        if (airflowSections != null) {
            Part[] orderedParts = new Part[airflowSections.size()];
            for (Part part : disorderParts) {
                int position = part.getPosition().intValue();
                int indexOf = airflowSections.indexOf(position);
                if (indexOf != -1) {
                    orderedParts[indexOf] = part;
                }
            }
            return Arrays.asList(orderedParts);
        }
        return disorderParts;
    }

    /**
     * 风向排序后不忽略热回收
     * @param unit
     * @return
     */
    public static List<Integer> getAirflowSectionsNoIgnore(Unit unit) {
        AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
        List<Integer> airflowSections = null;
        if (isDoubleLayer(ahuLayout)) {
            if (LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()) {
                airflowSections = getPlateRecycleAirflowSections(ahuLayout,false);
            } else if (LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()) {
                airflowSections = getWheelRecycleAirflowSections(ahuLayout, false);
            } else if (LayoutStyleEnum.DOUBLE_RETURN_1.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.DOUBLE_RETURN_2.style() == ahuLayout.getStyle()) {
                airflowSections = getWheelRecycleAirflowSections(ahuLayout, false);
            } else if (LayoutStyleEnum.VERTICAL_UNIT_1.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.VERTICAL_UNIT_2.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.SIDE_BY_SIDE_RETURN_1.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.SIDE_BY_SIDE_RETURN_2.style() == ahuLayout.getStyle()) {
                airflowSections = getVerticalAirflowSections(ahuLayout);
            }
        }
        return airflowSections;
    }

    public static List<Integer> getAirflowSections(Unit unit) {
        AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
        List<Integer> airflowSections = null;
        if (isDoubleLayer(ahuLayout)) {
            if (LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()) {
                airflowSections = getPlateRecycleAirflowSections(ahuLayout,true);
            } else if (LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()) {
                airflowSections = getWheelRecycleAirflowSections(ahuLayout, true);
            } else if (LayoutStyleEnum.DOUBLE_RETURN_1.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.DOUBLE_RETURN_2.style() == ahuLayout.getStyle()) {
                airflowSections = getWheelRecycleAirflowSections(ahuLayout, false);
            } else if (LayoutStyleEnum.VERTICAL_UNIT_1.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.VERTICAL_UNIT_2.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.SIDE_BY_SIDE_RETURN_1.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.SIDE_BY_SIDE_RETURN_2.style() == ahuLayout.getStyle()) {
                airflowSections = getVerticalAirflowSections(ahuLayout);
            }
        }
        return airflowSections;
    }

    private static List<Integer> getWheelRecycleAirflowSections(AhuLayout ahuLayout, boolean ignoreTopRecycle) {
        List<Integer> airflowSections = new ArrayList<>();

        int[] topLeftLayout = ahuLayout.getLayoutData()[0];
        int[] topRightLayout = ahuLayout.getLayoutData()[1];
        int[] bottomLeftLayout = ahuLayout.getLayoutData()[2];
        int[] bottomRightLayout = ahuLayout.getLayoutData()[3];
        for (int position : bottomLeftLayout) {
            airflowSections.add(position);
        }
        for (int position : bottomRightLayout) {
            airflowSections.add(position);
        }
        for (int i = topRightLayout.length; i > 0; i--) {
            airflowSections.add(topRightLayout[i - 1]);
        }
        for (int i = topLeftLayout.length - (ignoreTopRecycle ? 1 : 0); i > 0; i--) { // ignore top recycle section
            airflowSections.add(topLeftLayout[i - 1]);
        }
        return airflowSections;
    }

    private static List<Integer> getPlateRecycleAirflowSections(AhuLayout ahuLayout, boolean ignoreBootomRecycle) {
        List<Integer> airflowSections = new ArrayList<>();

        int[] topLeftLayout = ahuLayout.getLayoutData()[0];
        int[] topRightLayout = ahuLayout.getLayoutData()[1];
        int[] bottomLeftLayout = ahuLayout.getLayoutData()[2];
        int[] bottomRightLayout = ahuLayout.getLayoutData()[3];
        for (int position : topLeftLayout) {
            airflowSections.add(position);
        }
        for (int position : bottomRightLayout) {
            airflowSections.add(position);
        }
        for (int i = topRightLayout.length; i > 0; i--) {
            airflowSections.add(topRightLayout[i - 1]);
        }
        for (int i = bottomLeftLayout.length - (ignoreBootomRecycle ? 1 : 0); i > 0; i--) { // ignore bottom recycle section
            airflowSections.add(bottomLeftLayout[i - 1]);
        }
        return airflowSections;
    }

    private static List<Integer> getVerticalAirflowSections(AhuLayout ahuLayout) {
        List<Integer> airflowSections = new ArrayList<>();

        int[] topLeftLayout = ahuLayout.getLayoutData()[0];
        int[] topRightLayout = ahuLayout.getLayoutData()[1];
        int[] bottomLeftLayout = ahuLayout.getLayoutData()[2];
        int[] bottomRightLayout = ahuLayout.getLayoutData()[3];
        for (int position : bottomLeftLayout) {
            airflowSections.add(position);
        }
        for (int position : bottomRightLayout) {
            airflowSections.add(position);
        }
        for (int position : topLeftLayout) {
            airflowSections.add(position);
        }
        for (int position : topRightLayout) {
            airflowSections.add(position);
        }
        return airflowSections;
    }

    public static double calculateAHUContainerWeight(Unit unit, Partition partition) {
        double ahuContainerWeight = 0;
        if (EmptyUtil.isNotEmpty(partition)) {
            List<AhuPartition> ahuPartitions = AhuPartitionUtils.parseAhuPartition(unit, partition);
            Iterator<AhuPartition> ahuPartitionItr = ahuPartitions.iterator();
            while (ahuPartitionItr.hasNext()) {
                AhuPartition ahuPartition = ahuPartitionItr.next();
                if (AhuPartitionUtils.hasEndFacePanel(ahuPartition, ahuPartitions)) {
                    ahuContainerWeight += calculatePartitionContainerWeight(unit, ahuPartition, ahuPartitions.size(),
                            true);
                } else {
                    ahuContainerWeight += calculatePartitionContainerWeight(unit, ahuPartition, ahuPartitions.size(),
                            false);
                }
            }
        }
        return ahuContainerWeight;
    }

    public static double calculatePartitionWeight(Unit unit, AhuPartition ahuPartition, int partitionSize,
            boolean facePanel) {
        double partitionWeight = calculatePartitionContainerWeight(unit, ahuPartition, partitionSize, facePanel);
        partitionWeight += calculatePartitionSectionWeight(ahuPartition);
        return partitionWeight;
    }

    @SuppressWarnings("unchecked")
    public static double calculatePartitionContainerWeight(Unit unit, AhuPartition ahuPartition, int partitionSize,
            boolean facePanel) {
        String unitNo = AhuUtil.getUnitNo(unit.getSeries());
        String unitSeries = AhuUtil.getUnitSeries(unit.getSeries());

        String unitJson = unit.getMetaJson();
        Map<String, Object> unitMeta = JSON.parseObject(unitJson, Map.class);

        int partitionLength = AhuUtil.getMoldSize(ahuPartition.getLength());
        int columnPartitionLength = partitionLength > UtilityConstant.SYS_INT_LARGE_UNIT_THRESHOLD
                ? UtilityConstant.SYS_INT_LARGE_UNIT_THRESHOLD
                : partitionLength;

        // calculate panel weight
        double panelFactor = getSpecWeight(unitSeries, unitNo, unitMeta, CommonConstant.SYS_WGT_PARTITION_PARAMETER,
                CommonConstant.SYS_WGT_PARTITION_PARAMETER_COFF);
        double face = 0;
        if (facePanel) {
            face = getSpecWeight(unitSeries, unitNo, unitMeta, CommonConstant.SYS_WGT_PARTITION_FACEPANEL,
                    CommonConstant.SYS_WGT_PARTITION_FACEPANEL_X);
            if (partitionSize == 1) {
                face = face * 2;
            }
        }
        double panel = getSpecWeight(unitSeries, unitNo, unitMeta, CommonConstant.SYS_WGT_PARTITION_PANELTBFE,
                String.valueOf(columnPartitionLength));
        double base = getSpecWeight(unitSeries, unitNo, unitMeta, CommonConstant.SYS_WGT_PARTITION_BASE,
                String.valueOf(columnPartitionLength));
        double ceiling = 0;
        if (hasRoof(unitMeta)) {
            ceiling = getSpecWeight(unitSeries, unitNo, unitMeta, CommonConstant.SYS_WGT_PARTITION_TOP,
                    String.valueOf(columnPartitionLength));
        }
        // calculate package weight
        double packageWeight = 0;
        List<CalculatorSpec> packageSpecs = getSectionSpecs(CommonConstant.SYS_UNIT_SPEC_SECTION_AHU);
        for (CalculatorSpec packageSpec : packageSpecs) {
            if (CalculatorUtil.isSpecEffective(packageSpec, unitMeta)) {
                Map<String, Object> specMeta = new HashMap<>();
                specMeta.put(SectionMetaUtils.getMetaAHUKey(MetaKey.KEY_SECTIONL), columnPartitionLength);
                packageWeight += weightCalculator.queryWeight(unitNo, packageSpec, specMeta,
                        calculatorModel.getSpecData(unitSeries, packageSpec), CommonConstant.SYS_UNIT_SPEC_SECTION_AHU);
            }
        }

        double secWeight = 0;
        secWeight = getSpecWeight(unitSeries, unitNo, unitMeta, CommonConstant.SYS_WGT_PARTITION_SEC,
                CommonConstant.SYS_ALPHABET_X_UP);
        secWeight = secWeight * (partitionSize - 1);

        // multiply with unit factor if partition length exceeds 35M
        double unitFactor = 1;
        if (partitionLength > UtilityConstant.SYS_INT_LARGE_UNIT_THRESHOLD) {
            unitFactor = Double.valueOf(partitionLength) / UtilityConstant.SYS_INT_LARGE_UNIT_THRESHOLD;
        }

        return face * panelFactor + base + (panel * panelFactor + packageWeight + ceiling) * unitFactor + secWeight;
    }

    public static boolean hasRoof(Map<String, Object> unitMeta) {
        if (unitMeta != null && unitMeta.containsKey(MetaKey.META_AHU_ISPRERAIN)) {
            String isprerain = unitMeta.get(MetaKey.META_AHU_ISPRERAIN).toString();
            return Boolean.valueOf(isprerain);
        }
        return false;
    }

    private static double getSpecWeight(String unitSeries, String unitNo, Map<String, Object> unitMeta, String specName,
            String specKey) {
        double weight = 0d;
        CalculatorSpec spec = getSpec(getSpec(), specName);
        if (EmptyUtil.isNotEmpty(spec)) {
            weight = weightCalculator.queryWeight(unitNo, spec, unitMeta, calculatorModel.getSpecData(unitSeries, spec),
                    specKey);
            if (weight <= 0) {
                weight = 1.0;
            }
            log.debug(String.format("calculate weight of %s: %s", specName, weight));
        } else {
            weight = 1.0;
            log.error(String.format("failed to load spec: %s ", specName));
        }
        return weight;
    }

    private static List<CalculatorSpec> getSectionSpecs(String sectionKey) {
        List<CalculatorSpec> packageSpecs = new ArrayList<>();
        Iterator<CalculatorSpec> specItr = getSpec().iterator();
        while (specItr.hasNext()) {
            CalculatorSpec spec = specItr.next();
            if (spec.getSection().equals(sectionKey)) {
                packageSpecs.add(spec);
            }
        }
        return packageSpecs;
    }

    private static CalculatorSpec getSpec(List<CalculatorSpec> list, String specName) {
        Iterator<CalculatorSpec> it = list.iterator();
        while (it.hasNext()) {
            CalculatorSpec spec = it.next();
            if (spec.getName().equals(specName)) {
                return spec;
            }
        }
        return null;
    }

    public static double calculatePartitionSectionWeight(AhuPartition ahuPartition) {
        LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
        double weight = 0d;
        boolean isRecycle = false;
        for (Map<String, Object> section : sections) {
            double sectionWeight = AhuPartition.getWeightOfSection(section);
            String sMetaId = String.valueOf(section.get("metaId"));;
            if(!EmptyUtil.isEmptyOrNull(sMetaId) && (sMetaId.equals(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId()) ||
                    sMetaId.equals(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId()))){
                if (sectionWeight > 0) {
                    weight += sectionWeight / 2;
                }
            }else{
                if (sectionWeight > 0) {
                    weight += sectionWeight;
                }
            }
            
        }
        return weight;
    }

    public static void reorderPartition(AhuLayout ahuLayout, List<AhuPartition> ahuPartitions) {
        LayoutStyleEnum layoutStyle = LayoutStyleEnum.valueStyleOf(ahuLayout.getStyle());
        if (LayoutStyleEnum.WHEEL.equals(layoutStyle)) {
            reorderPartitionForWheel(ahuLayout, ahuPartitions);
        } else if (LayoutStyleEnum.PLATE.equals(layoutStyle)) {
            reorderPartitionForPlate(ahuLayout, ahuPartitions);
        } else if (LayoutStyleEnum.VERTICAL_UNIT_1.equals(layoutStyle)
                || LayoutStyleEnum.VERTICAL_UNIT_2.equals(layoutStyle)
                || LayoutStyleEnum.DOUBLE_RETURN_1.equals(layoutStyle)
                || LayoutStyleEnum.DOUBLE_RETURN_2.equals(layoutStyle)
                || LayoutStyleEnum.SIDE_BY_SIDE_RETURN_1.equals(layoutStyle)
                || LayoutStyleEnum.SIDE_BY_SIDE_RETURN_2.equals(layoutStyle)) {
            reorderPartitionForVertical(ahuLayout, ahuPartitions);
        }
    }

    private static void reorderPartitionForWheel(AhuLayout ahuLayout, List<AhuPartition> ahuPartitions) {
        int posOfPartition = 0;
        // sort from bottom
        List<Integer> bottomSections = getBottomLayerSections(ahuLayout);
        for (AhuPartition ahuPartition : ahuPartitions) {
            LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
            int posOfSection = AhuPartition.getPosOfSection(sections.get(0));
            if (bottomSections.contains(posOfSection)) {
                ahuPartition.setPos(posOfPartition++);
            }
        }
        // sort from top
        posOfPartition = ahuPartitions.size() - 1;
        List<Integer> topSections = getTopLayerSections(ahuLayout);
        for (AhuPartition ahuPartition : ahuPartitions) {
            LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
            int posOfSection = AhuPartition.getPosOfSection(sections.get(0));
            if (topSections.contains(posOfSection)) {
                ahuPartition.setPos(posOfPartition--);
            }
        }
    }

    private static void reorderPartitionForPlate(AhuLayout ahuLayout, List<AhuPartition> ahuPartitions) {
        List<Integer> bottomSections = getBottomLayerSections(ahuLayout);
        List<AhuPartition> reversedAhuPartitions = new ArrayList<>();
        reversedAhuPartitions.addAll(ahuPartitions);
        Collections.reverse(reversedAhuPartitions);

        int posOfPartition = 0;
        // sort from bottom
        for (AhuPartition ahuPartition : reversedAhuPartitions) {
            LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
            int posOfSection = AhuPartition.getPosOfSection(sections.get(0));
            if (bottomSections.contains(posOfSection)) {
                ahuPartition.setPos(posOfPartition++);
            }
        }
        // sort from top
        posOfPartition = ahuPartitions.size() - 1;
        List<Integer> topSections = getTopLayerSections(ahuLayout);
        for (AhuPartition ahuPartition : ahuPartitions) {
            LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
            int posOfSection = AhuPartition.getPosOfSection(sections.get(0));
            if (topSections.contains(posOfSection)) {
                ahuPartition.setPos(posOfPartition--);
            }
        }
    }

    private static void reorderPartitionForVertical(AhuLayout ahuLayout, List<AhuPartition> ahuPartitions) {
        int posOfPartition = 0;
        // sort from bottom
        List<Integer> bottomSections = getBottomLayerSections(ahuLayout);
        for (AhuPartition ahuPartition : ahuPartitions) {
            LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
            int posOfSection = AhuPartition.getPosOfSection(sections.get(0));
            if (bottomSections.contains(posOfSection)) {
                ahuPartition.setPos(posOfPartition++);
            }
        }
        // sort from top
        List<Integer> topSections = getTopLayerSections(ahuLayout);
        for (AhuPartition ahuPartition : ahuPartitions) {
            LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
            int posOfSection = AhuPartition.getPosOfSection(sections.get(0));
            if (topSections.contains(posOfSection)) {
                ahuPartition.setPos(posOfPartition++);
            }
        }
    }

    public static List<Integer> getTopLayerSections(AhuLayout ahuLayout) {
        List<Integer> layerSections = new ArrayList<>();
        layerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[AhuLayout.TOP_LEFT]));
        layerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[AhuLayout.TOP_RIGHT]));
        return layerSections;
    }

    public static List<Integer> getBottomLayerSections(AhuLayout ahuLayout) {
        List<Integer> layerSections = new ArrayList<>();
        layerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[AhuLayout.BOTTOM_LEFT]));
        layerSections.addAll(Ints.asList(ahuLayout.getLayoutData()[AhuLayout.BOTTOM_RIGHT]));
        return layerSections;
    }

    /**
     * The partition has end face panel if at the start or end position in the
     * layer.
     * 
     * @param ahuPartition
     * @param ahuPartitions
     * @return
     */
    public static boolean hasEndFacePanel(AhuPartition ahuPartition, List<AhuPartition> ahuPartitions) {
        boolean isTopLayer = ahuPartition.isTopLayer();
        // filter same layer partitions
        List<AhuPartition> filteredAhuPartitions = ahuPartitions.stream().filter(p -> (p.isTopLayer() == isTopLayer))
                .collect(Collectors.toList());
        for (int i = 0; i < filteredAhuPartitions.size(); i++) {
            AhuPartition filteredAhuPartition = filteredAhuPartitions.get(0);
            if (ahuPartition.getPos() == filteredAhuPartition.getPos()) { // same partition
                return (i == 0 || i == filteredAhuPartitions.size() - 1);
            }
        }
        return false;
    }

}
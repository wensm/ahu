package com.carrier.ahu.calculator;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import cn.hutool.core.util.ObjectUtil;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.vo.SysConstants;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.carrier.ahu.calculator.panel.PanelXSLXPO;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.model.partition.PanelFace;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.util.ExcelUtils;
import com.carrier.ahu.vo.FileNamesLoadInSystem;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExcelForPanelUtils {

	public static ExcelForPanelUtils getInstance() {
		if (null == instance) {
			reload();
		}
		return instance;
	}

	public static void reload() {
		instance = new ExcelForPanelUtils();
	}

	private ExcelForPanelUtils() {
		loadBigOperateMap();
		loadNoBigOperateMap();
		loadNormalSideMap();
		loadDamperSideMap();
		loadSideFanSendFacePositionBMap();
		loadSideFanSendFacePositionSMap();
		loadSideFanSendTopPositionBMap();
		loadSideFanSendTopPositionSMap();
		loadSideFanWWKMap();
		loadSideCombinedMixingChamberFivePanelMap();
		loadBottomMap();
		loadTopMap();
	}
	public static final String FRONT_NORMALSIDE = "NormalSide";//39CQ 39G 普通端面无风阀及出风口
	public static final String FRONT_SIDEDAMPER= "SideDamper";//39CQ 39G 端面带风阀
	public static final String FRONT_SIDEFANSENDFACEPOSITIONB= "SideFanSendFacePositionB";//风机段端面出风_  后置(电机位置)
	public static final String FRONT_SIDEFANSENDFACEPOSITIONS= "SideFanSendFacePositionS";//风机段端面出风_  侧置(电机位置)
	public static final String FRONT_SIDEFANSENDTOPPOSITIONB= "SideFanSendTopPositionB";//风机段顶面出风_ 后置(电机位置)
	public static final String FRONT_SIDEFANSENDTOPPOSITIONS= "SideFanSendTopPositionS";//风机段顶面出风_ 侧置(电机位置)
	public static final String FRONT_SIDEFANWWK = "SideFanwwk";//风机段端面无蜗壳风机
	public static final String FRONT_SIDECOMBINEDMIXINGCHAMBERFIVEPANEL = "SideCombinedMixingChamberFivePanel";//新回排端面，第五个段中间面

	private Map<String, PanelXSLXPO> BigOperateMap = new HashMap<>();//操作面
	private Map<String, PanelXSLXPO> BigOperateMapByH = new HashMap<>();//操作面变形使用
	private Map<String, PanelXSLXPO> NoBigOperateMap = new HashMap<>();//非操作面
	private Map<String, PanelXSLXPO> NormalSideMap = new HashMap<>();//普通端面无风阀及出风口
	private Map<String, PanelXSLXPO> DamperSideMap = new HashMap<>();//端面带风阀
	private Map<String, PanelXSLXPO> SideFanSendFacePositionBMap = new HashMap<>();
	private Map<String, PanelXSLXPO> SideFanSendFacePositionSMap = new HashMap<>();
	private Map<String, PanelXSLXPO> SideFanSendTopPositionBMap = new HashMap<>();
	private Map<String, PanelXSLXPO> SideFanSendTopPositionSMap = new HashMap<>();
	private Map<String, PanelXSLXPO> SideFanWWKMap = new HashMap<>();
	private Map<String, PanelXSLXPO> SideCombinedMixingChamberFivePanelMap = new HashMap<>();
	private Map<String, PanelXSLXPO> BottomMap = new HashMap<>();
	private Map<String, PanelXSLXPO> TopMap = new HashMap<>();

	public Map<String, PanelXSLXPO> getBigOperateMap() {
		return BigOperateMap;
	}
	public Map<String, PanelXSLXPO> getBigOperateMapByH() {
		return BigOperateMapByH;
	}

	private List<PanelXSLXPO> BigOperatePOList;
	private List<PanelXSLXPO> BigOperatePOListByH;
	private List<PanelXSLXPO> NoBigOperatePOList;
	private List<PanelXSLXPO> NormalSidePOList;
	private List<PanelXSLXPO> DamperSidePOList;
	private List<PanelXSLXPO> SideFanSendFacePositionBPOList;
	private List<PanelXSLXPO> SideFanSendFacePositionSPOList;
	private List<PanelXSLXPO> SideFanSendTopPositionBPOList;
	private List<PanelXSLXPO> SideFanSendTopPositionSPOList;
	private List<PanelXSLXPO> SideFanWWKPOList;
	private List<PanelXSLXPO> SideCombinedMixingChamberFivePanelPOList;
	private List<PanelXSLXPO> BottomPOList;
	private List<PanelXSLXPO> TopPOList;

	public PanelXSLXPO getBigOperatePanel(String serial, String sectionTypeNo, String sectionL) {
		String key = genKey(serial, SectionTypeEnum.cutTypeNoCode(sectionTypeNo), sectionL);
		if (BigOperateMap.containsKey(key)) {
			return ObjectUtil.clone(BigOperateMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getBigOperatePanelByH(String serial,String sectionTypeNo, String sectionL) {
		String key = genKeyByH(serial, sectionTypeNo, sectionL);
		if (BigOperateMapByH.containsKey(key)) {
			return ObjectUtil.clone(BigOperateMapByH.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getNoBigOperatePanel(String serial, String sectionTypeNo, String sectionL) {
		String key = genKey(serial, SectionTypeEnum.cutTypeNoCode(sectionTypeNo), sectionL);
		if (NoBigOperateMap.containsKey(key)) {
			return ObjectUtil.clone(NoBigOperateMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getNormalSidePanel(String serial) {
		String key = serial;
		if (NormalSideMap.containsKey(key)) {
			return ObjectUtil.clone(NormalSideMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getDamperSidePanel(String serial) {
		String key = serial;
		if (DamperSideMap.containsKey(key)) {
			return ObjectUtil.clone(DamperSideMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getSideFanSendFacePositionBPanel(String serial) {
		String key = genSerial(serial);
		if (SideFanSendFacePositionBMap.containsKey(key)) {
			return ObjectUtil.clone(SideFanSendFacePositionBMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getSideFanSendFacePositionSPanel(String serial, String direction, String sectionL) {
		String key =  genSerialAndLenKey(serial,direction,sectionL);
		if (SideFanSendFacePositionSMap.containsKey(key)) {
			return ObjectUtil.clone(SideFanSendFacePositionSMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getSideFanSendTopPositionBPanel(String serial, String direction, String sectionL) {
		String key =  genSerialAndLenKeyForTopFace(serial,direction,sectionL);
		if (SideFanSendTopPositionBMap.containsKey(key)) {
			return ObjectUtil.clone(SideFanSendTopPositionBMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getSideFanSendTopPositionSPanel(String serial, String direction, String sectionL) {
		String key =  genSerialAndLenKeyForTopFace(serial,direction,sectionL);
		if (SideFanSendTopPositionSMap.containsKey(key)) {
			return ObjectUtil.clone(SideFanSendTopPositionSMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getSideFanWWKPanel(String serial) {
		String key =  AhuUtil.getUnitNo(serial);
		if (SideFanWWKMap.containsKey(key)) {
			return ObjectUtil.clone(SideFanWWKMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getSideCombinedMixingChamberFivePanelPanel(String serial) {
		String key =  AhuUtil.getUnitNo(serial);
		if (SideCombinedMixingChamberFivePanelMap.containsKey(key)) {
			return ObjectUtil.clone(SideCombinedMixingChamberFivePanelMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getBottomPanel(String serial, String sectionTypeNo, String sectionL) {
		String key = genKey(serial, SectionTypeEnum.cutTypeNoCode(sectionTypeNo), sectionL);
		if (BottomMap.containsKey(key)) {
			return ObjectUtil.clone(BottomMap.get(key));
		} else {
			return null;
		}
	}
	public PanelXSLXPO getTopPanel(String serial, String sectionTypeNo, String sectionL) {
		String key = genKey(serial, SectionTypeEnum.cutTypeNoCode(sectionTypeNo), sectionL);
		if (TopMap.containsKey(key)) {
			return ObjectUtil.clone(TopMap.get(key));
		} else {
			return null;
		}
	}

	private void loadBigOperateMap() {
		BigOperatePOList = load(FileNamesLoadInSystem.PANEL_BIGOPERATE_XLSX, UtilityConstant.SYS_PANEL_LEFT);
	}
	private void loadBigOperateMapByH() {
		BigOperatePOListByH = load(FileNamesLoadInSystem.PANEL_BIGOPERATE_XLSX, UtilityConstant.SYS_PANEL_LEFT);
	}
	private void loadNoBigOperateMap() {
		NoBigOperatePOList = load(FileNamesLoadInSystem.PANEL_NO_BIGOPERATE_XLSX, UtilityConstant.SYS_PANEL_RIGHT);
	}
	private void loadNormalSideMap() {
		NormalSidePOList = load(FileNamesLoadInSystem.PANEL_NORMALSIDE_XLSX, FRONT_NORMALSIDE);
	}
	private void loadDamperSideMap() {
		DamperSidePOList = load(FileNamesLoadInSystem.PANEL_SIDEDAMPER_XLSX, FRONT_SIDEDAMPER);
	}
	private void loadSideFanSendFacePositionBMap() {
		SideFanSendFacePositionBPOList = load(FileNamesLoadInSystem.PANEL_SIDEFANSENDFACEPOSITIONB_XLSX, FRONT_SIDEFANSENDFACEPOSITIONB);
	}
	private void loadSideFanSendFacePositionSMap() {
		SideFanSendFacePositionSPOList = load(FileNamesLoadInSystem.PANEL_SIDEFANSENDFACEPOSITIONS_XLSX, FRONT_SIDEFANSENDFACEPOSITIONS);
	}
	private void loadSideFanSendTopPositionBMap() {
		SideFanSendTopPositionBPOList = load(FileNamesLoadInSystem.PANEL_SIDEFANSENDTOPPOSITIONB_XLSX, FRONT_SIDEFANSENDTOPPOSITIONB);
	}
	private void loadSideFanSendTopPositionSMap() {
		SideFanSendTopPositionSPOList = load(FileNamesLoadInSystem.PANEL_SIDEFANSENDTOPPOSITIONS_XLSX, FRONT_SIDEFANSENDTOPPOSITIONS);
	}
	private void loadSideFanWWKMap() {
		SideFanWWKPOList = load(FileNamesLoadInSystem.PANEL_SIDEFANWWK_XLSX, FRONT_SIDEFANWWK);
	}
	private void loadSideCombinedMixingChamberFivePanelMap() {
		SideCombinedMixingChamberFivePanelPOList = load(FileNamesLoadInSystem.PANEL_SIDECOMBINEDMIXINGCHAMBERFIVEPANEL_XLSX, FRONT_SIDECOMBINEDMIXINGCHAMBERFIVEPANEL);
	}

	private void loadBottomMap() {
		BottomPOList = load(FileNamesLoadInSystem.PANEL_BOTTOM_XLSX, UtilityConstant.SYS_PANEL_BOTTOM);
	}
	private void loadTopMap() {
		TopPOList = load(FileNamesLoadInSystem.PANEL_TOP_XLSX, UtilityConstant.SYS_PANEL_TOP);
	}

	private List<PanelXSLXPO> load(String filePath, String type) {
		List<PanelXSLXPO> PanelXSLXPOList = new ArrayList<PanelXSLXPO>();
		List<List<String>> list = Collections.emptyList();
		try {
			InputStream is = new FileInputStream(SysConstants.ASSERT_DIR + filePath);
			list = ExcelUtils.read(is, ExcelUtils.isExcel2003(filePath), 0, 1);
		} catch (IOException e) {
			log.error("Failed to load excel: " + filePath, e);
			return Collections.emptyList();
		} catch (InvalidFormatException e) {
			log.error("Failed to load excel: " + filePath, e);
			return Collections.emptyList();
		}
		for (List<String> stringList : list) {
			PanelXSLXPO PanelXSLXPO = new PanelXSLXPO();
			int i = 0;
			for (String s : stringList) {
				if (i == 0) {
					PanelXSLXPO.setSerial(s);
					i++;
					continue;
				} else if (i == 1) {
					PanelXSLXPO.setSectionTypeNo(String.valueOf(NumberUtil.convertStringToDoubleInt(s)));
					i++;
					continue;
				} else if (i == 2) {
					PanelXSLXPO.setH(String.valueOf(NumberUtil.convertStringToDoubleInt(s)));
					i++;
					continue;
				} else if (i == 3) {
					PanelXSLXPO.setW(String.valueOf(NumberUtil.convertStringToDoubleInt(s)));
					i++;
					continue;
				} else if (i == 4) {
					PanelXSLXPO.setHr(s);
					i++;
					continue;
				} else if (i == 5) {
					PanelXSLXPO.setHp(s);
					i++;
					continue;
				} else if (i == 6) {
					PanelXSLXPO.setWr(s);
					i++;
					continue;
				} else if (i == 7) {
					PanelXSLXPO.setWp(s);
					i++;
					continue;
				} else if (i == 8) {
					PanelXSLXPO.setPartlen(s);
					i++;
					continue;
				} else if (i == 9) {
					PanelXSLXPO.setHw(s);
					i++;
					continue;
				}
			}

			if (type.equals(UtilityConstant.SYS_PANEL_LEFT)) {
				BigOperateMap.put(genKey(PanelXSLXPO), PanelXSLXPO);//39CQ 39G
				BigOperateMapByH.put(genKeyByH(PanelXSLXPO), PanelXSLXPO);//39CQ 39G
			} else if (type.equals(UtilityConstant.SYS_PANEL_RIGHT)) {
				NoBigOperateMap.put(genKey(PanelXSLXPO), PanelXSLXPO);//39G
			} else if (type.equals(FRONT_NORMALSIDE)) {
				NormalSideMap.put(PanelXSLXPO.getSerial(), PanelXSLXPO);//39CQ 39G
			} else if (type.equals(FRONT_SIDEDAMPER)) {
				DamperSideMap.put(PanelXSLXPO.getSerial(), PanelXSLXPO);//39CQ 39G
			} else if (type.equals(FRONT_SIDEFANSENDFACEPOSITIONB)) {
				SideFanSendFacePositionBMap.put(genSerial(PanelXSLXPO.getSerial()), PanelXSLXPO);
			} else if (type.equals(FRONT_SIDEFANSENDFACEPOSITIONS)) {
				SideFanSendFacePositionSMap.put(genSerialAndLenKey(PanelXSLXPO), PanelXSLXPO);
			} else if (type.equals(FRONT_SIDEFANSENDTOPPOSITIONB)) {
				SideFanSendTopPositionBMap.put(genSerialAndLenKey(PanelXSLXPO), PanelXSLXPO);
			} else if (type.equals(FRONT_SIDEFANSENDTOPPOSITIONS)) {
				SideFanSendTopPositionSMap.put(genSerialAndLenKey(PanelXSLXPO), PanelXSLXPO);
			} else if (type.equals(FRONT_SIDEFANWWK)) {
				SideFanWWKMap.put(AhuUtil.getUnitNo(PanelXSLXPO.getSerial()), PanelXSLXPO);
			} else if (type.equals(FRONT_SIDECOMBINEDMIXINGCHAMBERFIVEPANEL)) {
				SideCombinedMixingChamberFivePanelMap.put(AhuUtil.getUnitNo(PanelXSLXPO.getSerial()), PanelXSLXPO);
			} else if (type.equals(UtilityConstant.SYS_PANEL_BOTTOM)) {
				BottomMap.put(genKey(PanelXSLXPO), PanelXSLXPO);//39G
			} else if (type.equals(UtilityConstant.SYS_PANEL_TOP)) {
				TopMap.put(genKey(PanelXSLXPO), PanelXSLXPO);//39G
			}
			PanelXSLXPOList.add(PanelXSLXPO);
		}
		return PanelXSLXPOList;
	}
	public static String genSerialAndLenKeyForTopFace(String serial, String direction, String sectionL) {
		StringBuffer key = new StringBuffer();
		key.append(AhuUtil.getUnitNo(serial));
		int height = AhuUtil.getHeightOfAHU(serial);
		int length = NumberUtil.convertStringToDoubleInt(sectionL);
		key.append(UtilityConstant.SYS_PUNCTUATION_PLUS).append(AhuUtil.getMoldSize(length));
		if(SystemCountUtil.gtSmallUnitHeight(height)){
			key.append(UtilityConstant.SYS_PUNCTUATION_PLUS).append(UtilityConstant.SYS_STRING_LEFT.equals(direction)
					? UtilityConstant.SYS_STRING_LEFT_L
					: UtilityConstant.SYS_STRING_RIGHT_R);
		}
		return key.toString();
	}
	private String genSerial(String serial) {
		StringBuffer key = new StringBuffer();
		key.append(AhuUtil.getUnitNo(serial));
		return key.toString();
	}
	private String genSerialAndLenKey(PanelXSLXPO po) {
		StringBuffer key = new StringBuffer();
		key.append(AhuUtil.getUnitNo(po.getSerial()));

		int height = AhuUtil.getHeightOfAHU(po.getSerial());
		if(SystemCountUtil.gtSmallUnitHeight(height) && EmptyUtil.isNotEmpty(po.getPartlen())){
			key.append(UtilityConstant.SYS_PUNCTUATION_PLUS).append(po.getPartlen());
		}else{
			key.append(UtilityConstant.SYS_PUNCTUATION_PLUS).append(po.getW());
		}
		return key.toString();
	}
	public static String genSerialAndLenKey(String serial, String direction, String sectionL) {
		StringBuffer key = new StringBuffer();
		key.append(AhuUtil.getUnitNo(serial));
		int height = AhuUtil.getHeightOfAHU(serial);
		if(SystemCountUtil.gtSmallUnitHeight(height)){
			int length = NumberUtil.convertStringToDoubleInt(sectionL);
			key.append(UtilityConstant.SYS_PUNCTUATION_PLUS).append(AhuUtil.getMoldSize(length));
			key.append(UtilityConstant.SYS_PUNCTUATION_PLUS).append(UtilityConstant.SYS_STRING_LEFT.equals(direction)
																? UtilityConstant.SYS_STRING_LEFT_L
																: UtilityConstant.SYS_STRING_RIGHT_R);
		}else{
			int width = AhuUtil.getWidthOfAHU(serial);
			key.append(UtilityConstant.SYS_PUNCTUATION_PLUS).append(width);
		}
		return key.toString();
	}

	private String genKey(PanelXSLXPO po) {
		return genKey(po.getSerial(), po.getSectionTypeNo(), po.getW());
	}
	public static String genKey(String serial, String typeNo, String sectionL) {
		StringBuffer key = new StringBuffer();
		key.append(serial);
		key.append(UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN).append(SectionTypeEnum.cutTypeNoCode(typeNo));
		int length = NumberUtil.convertStringToDoubleInt(sectionL);
		key.append(UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN).append(AhuUtil.getMoldSize(length));

		return key.toString();

	}
	private String genKeyByH(PanelXSLXPO po) {
		return genKey(po.getSerial().substring(0,po.getSerial().length()-2)
				,po.getSectionTypeNo()+UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN+po.getH()
				, po.getW());
	}
	public static String genKeyByH(String serial, String typeNo, String sectionL) {
		StringBuffer key = new StringBuffer();
		key.append(serial.substring(0,serial.length()-2));
		key.append(UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN).append(typeNo);
		key.append(UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN).append(String.valueOf(AhuUtil.getHeightOfAHU(serial)));
		int length = NumberUtil.convertStringToDoubleInt(sectionL);
		key.append(UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN).append(AhuUtil.getMoldSize(length));

		return key.toString();

	}
	public List<PanelXSLXPO> getBigOperatePOList() {
		return BigOperatePOList;
	}
	public List<PanelXSLXPO> getBigOperatePOListByH() {
		return BigOperatePOListByH;
	}

	public List<PanelXSLXPO> getNormalSidePOList() {
		return NormalSidePOList;
	}
	public List<PanelXSLXPO> getDamperSidePOList() {
		return DamperSidePOList;
	}

	public List<PanelXSLXPO> getBottomPOList() {
		return BottomPOList;
	}
	public List<PanelXSLXPO> getTopPOList() {
		return TopPOList;
	}

	private static ExcelForPanelUtils instance = new ExcelForPanelUtils();
}

package com.carrier.ahu.calculator.weight.impl;

import com.carrier.ahu.ResourceConstances;
import com.carrier.ahu.calculator.weight.CalculatorForModel1;
import com.carrier.ahu.calculator.weight.WeightCalculatorConstances;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.util.EmptyUtil;

public class WElectrodeHumidifier extends CalculatorForModel1 {

	public WElectrodeHumidifier() {
		super(WeightCalculatorConstances.CAL_SECTION_ElectrodeHumidifier);
	}

	protected String getTabKey(AhuParam ahu, PartParam part) {
		return ahu.getSeries().substring(0, 4);
	}

	protected String getColumnKey(AhuParam ahu, PartParam part) {
		Object obj = part.getParams().get("meta.section.electrodeHumidifier.SHumidificationQ");
		if (EmptyUtil.isNotEmpty(obj)) {
			return String.valueOf(obj);
		}
		return UtilityConstant.SYS_STRING_NUMBER_2 + UtilityConstant.SYS_STRING_NUMBER_6;//"26"
	}

	protected String getRowKey(AhuParam ahu, PartParam part) {
		return ahu.getSeries().substring(0, 4);
	}

	public String getType() {
		return UtilityConstant.METACOMMON_TYPE_SECTION;
	}

	@Override
	protected String getResourcePath() {
		return UtilityConstant.SYS_PATH_W_SECTION_ELECTRODEHUMIDIFIER;
	}

}

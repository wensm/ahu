package com.carrier.ahu.calculator;

import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_FAN_MODEL;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_MOTOR_POWER;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_POLE;
import static com.carrier.ahu.vo.SystemCalculateConstants.DEFAULT_SCALE;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Component;

import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.metadata.entity.calc.WeightCalculatorSpec;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class WeightCalculator extends AbstractCalculator {

    private static CalculatorModel calculatorModel;

    public CalculatorModel getCalModelInstance() {
        return calculatorModel;
    }

    public void reloadCalModeInstance() {
        calculatorModel = loadPriceModel();
    }

    public double queryWeight(String smodel, CalculatorSpec spec, Map<String, Object> ahuValueMap,
            Map<String, Map<String, String>> weightMap, String sectionKey) {
        if (EmptyUtil.isEmpty(weightMap)) {
            log.error(String.format("Empty AHU serial"));
            return -1d;
        }
        Map<String, String> modelWeightMap;
        if (CalWeightTool.isFanOrBeltSpec(spec)) {
            smodel = String.valueOf(ahuValueMap.get(SectionMetaUtils.getMetaSectionKey(sectionKey, KEY_FAN_MODEL)));
        } else if (CalWeightTool.isMotorAndPoleSpec(spec)) {
            String motorPower = String
                    .valueOf(ahuValueMap.get(SectionMetaUtils.getMetaSectionKey(sectionKey, KEY_MOTOR_POWER)));
            String pole = String.valueOf(ahuValueMap.get(SectionMetaUtils.getMetaSectionKey(sectionKey, KEY_POLE)));
            smodel = NumberUtil.scale(motorPower, DEFAULT_SCALE) + UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN
                    + Double.valueOf(NumberUtils.toDouble(pole)).intValue();
        } else if (CalWeightTool.isMotorSpec(spec)) {
            String motorPower = String
                    .valueOf(ahuValueMap.get(SectionMetaUtils.getMetaSectionKey(sectionKey, KEY_MOTOR_POWER)));
            smodel = NumberUtil.scale(motorPower, DEFAULT_SCALE);
        }
        if (CalWeightTool.isSprayHumidifierSpec(spec)) {
            Object supplier = ahuValueMap.get("meta.section.sprayHumidifier.supplier");
            Object ControlM = ahuValueMap.get("meta.section.sprayHumidifier.ControlM");
            String kkkey = smodel + UtilityConstant.SYS_PUNCTUATION_DOT + supplier + UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN + ControlM;
            modelWeightMap = weightMap.get(kkkey);
        } else if (CalWeightTool.isCtrSpec(spec)) {
            String fanPower = String.valueOf(ahuValueMap.get("meta.section.fan.motorPower"));
            fanPower = NumberUtil.scale(fanPower, DEFAULT_SCALE);
            modelWeightMap = weightMap.get(fanPower);//TODO dxx 是否需要进一步确认重量。
            if (null == modelWeightMap) {// TODO 风机前面的控制段，需要在风机确定提交后，再确定。不然控制段的重量计算错误@@@
                return 0;
            }
        } else if (CalWeightTool.isPanelParamSpec(spec)) {//面板重量系数
            String exskinm = CalWeightTool.transKey(UtilityConstant.METAHU_EXSKINM,ahuValueMap);
            String exskinw = CalWeightTool.transKey(UtilityConstant.METAHU_EXSKINW,ahuValueMap);
            String inskinm = CalWeightTool.transKey(UtilityConstant.METAHU_INSKINM,ahuValueMap);
            String inskinw = CalWeightTool.transKey(UtilityConstant.METAHU_INSKINW,ahuValueMap);
            String skin = String.format("%s.%s.%s.%s", exskinm, exskinw,inskinm,inskinw);
            modelWeightMap = weightMap.get(skin);
            if (null == modelWeightMap) {
                return 0;
            }
        } else {
            modelWeightMap = weightMap.get(smodel);
        }
        if (EmptyUtil.isEmpty(modelWeightMap)) {
            log.error(String.format("Unknow AHU serial: [%s] ", smodel));
            return -1d;
        }
        String propStr = spec.getProperty();
        String weightKey = CalculatorUtil.calPropertyValues(propStr, ahuValueMap, spec, sectionKey);
        // 2.根据Key查询获得值
        if (modelWeightMap.containsKey(weightKey)) {
            try {
                double wv = Double.parseDouble(modelWeightMap.get(weightKey));
                return wv;
            } catch (Exception e) {
                return -2d;
            }
        }
        log.warn(String.format("Failed to get weight value for key [%s] @[%s]", weightKey, spec.getSimpleInfo()));
        return -1d;
    }

    @Override
    protected Map<String, Map<String, String>> resolveData(CalculatorModel model) {
        return new HashMap<>();
    }

    @Override
    protected Class<WeightCalculatorSpec> getCalculatorSpecClass() {
        return WeightCalculatorSpec.class;
    }

}

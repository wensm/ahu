package com.carrier.ahu.calculator;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.util.EmptyUtil;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 计算器引擎 [价钱计算器]
 * 
 * @author Simon
 *
 */
@Slf4j
@Data
public abstract class AbstractCalculator {
    /** 价钱文档 */
    private Map<String, Map<String, String>> calculatorMap;
    private CalculatorModel model;

    public CalculatorModel loadPriceModel() {
        if (EmptyUtil.isNotEmpty(model)) {
            return model;
        }
        model = new CalculatorModel();
        try {
            // Step 1: resolve the spec and add to model
            List<? extends CalculatorSpec> templates = AhuMetadata.findAll(this.getCalculatorSpecClass());
            model.setSpec(templates);
            // Step 2: resovle every price table
            log.info(
                    MessageFormat.format("Loading the model([{0}]) table...", this.getCalculatorSpecClass().getName()));
            setCalculatorMap(resolveData(model));
            // model.setPriceMap(priceMap);
            log.info(MessageFormat.format("The model([{0}]) table loaded", this.getCalculatorSpecClass().getName()));

        } catch (Exception e) {
            log.error(MessageFormat.format("Failed to load model([{0}]).", this.getCalculatorSpecClass().getName()), e);
        }
        return model;
    }

    protected abstract Class<? extends CalculatorSpec> getCalculatorSpecClass();

    protected abstract Map<String, Map<String, String>> resolveData(CalculatorModel model);

    public Map<String, Map<String, String>> getCalculatorMap() {
        return calculatorMap;
    }

    public void setCalculatorMap(Map<String, Map<String, String>> calculatorMap) {
        this.calculatorMap = calculatorMap;
    }

}

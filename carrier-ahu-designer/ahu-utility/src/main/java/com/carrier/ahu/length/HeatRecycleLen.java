package com.carrier.ahu.length;

import cn.hutool.core.util.NumberUtil;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.heatrecycle.HeatxPlateLength;
import com.carrier.ahu.metadata.entity.section.SectionLength;
import com.carrier.ahu.util.EmptyUtil;

import java.util.List;

/**
 * Created by liangd4 on 2018/1/15. 转轮热回收 板式热回收
 */
public class HeatRecycleLen {
    // 计算段长
    public double getLength(String serial, String sectionType, double length, String brand, String exchangeModel, String sectionSideL) {
        SectionLength sectionLength = AhuMetadata.findOne(SectionLength.class, serial);
        
        // 段长计算 1：板式 2：转轮
    	if("Z".equals(brand)) {
    	    //如果选择无转轮，则默认段长为9，最小段长为6，界面可以手动修改
            if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getCode().equals(sectionType)) { // 转轮
                return 9;
            }else{
                /*return sectionLength.getW1();*/
                //如果是无板式交换，则段长默认为该型号对应的最大段长
                List<HeatxPlateLength> heatxPlateLengthList = AhuMetadata.findList(HeatxPlateLength.class, AhuUtil.getUnitNo(serial));
                if(EmptyUtil.isNotEmpty(heatxPlateLengthList)){
                    int plateLength = 0;
                    for(HeatxPlateLength heatxPlateLength:heatxPlateLengthList){
                        if(plateLength <= heatxPlateLength.getLength()){
                            plateLength = heatxPlateLength.getLength();
                        }
                    }
                    return plateLength;
                }else{
                    return sectionLength.getW1();
                }
                
            }
//    		if(length < 4) {
//    			return 5;
//    		}
    	}
        
        if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getCode().equals(sectionType)) { // 转轮
            //return sectionLength.getW2();//废弃
            /*
            依据和百瑞/毅科这两家转轮品牌的沟通，按照转轮规格来定义段长（不再按照机组规格来定义），统一确定原则如下：
            1.      转轮规格2600及以下的，按照6M段长；
            2.      转轮规格2600以上的，按照9M段长。
            */
            if(Double.parseDouble(sectionSideL)>2600){
                return 9;
            }else{
                return 6;
            }

        } else {//板式
            /*return sectionLength.getW1();*/
            HeatxPlateLength heatxPlateLength = AhuMetadata.findOne(HeatxPlateLength.class, AhuUtil.getUnitNo(serial),exchangeModel);
            if(EmptyUtil.isNotEmpty(heatxPlateLength)){
                return heatxPlateLength.getLength();
            }else{
                return sectionLength.getW1();
            }
        }
    }
}

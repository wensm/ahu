package com.carrier.ahu.util;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 单位转化工具类
 * @author WANGD1
 *
 */
public class UnitConversionUtil {
	private static Logger logger = LoggerFactory.getLogger(UnitConversionUtil.class);
	
	/**
	 * m³/h转换成L/s
	 * @param calNum	需要转换的数值
	 * @param keep	保留小数位数
	 * @return
	 */
	public static double m3hTols(Double calNum, int keep) {
		double result = 0;
		result = new BigDecimal(calNum / 3.6).setScale(keep, BigDecimal.ROUND_HALF_UP).doubleValue();
		return result;
	}
	
	/**
	 * L/s转换成m³/h
	 * @param calNum
	 * @param keep
	 * @return
	 */
	public static double lsTom3h(Double calNum, int keep) {
		double result = 0;
		result = new BigDecimal(calNum * 3.6).setScale(keep, BigDecimal.ROUND_HALF_UP).doubleValue();
		return result;
	}
	
	public static void main(String[] args) {
		double i = -1;
		i=m3hTols(i,2);
		System.out.println(i);
	}
	
	public static int getSubCount_2(String str, String key) {
        int count = 0;
        int index = 0;
        while ((index = str.indexOf(key, index)) != -1) {
            index = index + key.length();

            count++;
        }
        return count;
    }
}
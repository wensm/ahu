package com.carrier.ahu.section.meta;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.RoleEnum;
import com.carrier.ahu.common.util.AhuUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.entity.AhuTemplate;
import com.carrier.ahu.common.entity.GroupInfo;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.templet.ExportTemplet;
import com.carrier.ahu.po.meta.Meta;
import com.carrier.ahu.po.meta.MetaParameter;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.template.TemplateFactory;

import static com.carrier.ahu.common.enums.GroupTypeEnum.*;

public class TemplateUtil {

    public static final double DEFAULT_VELOCITY = 2.65;
    public static final double DEFAULT_ECONOMY = 3.00;

	// Complete Code For AHU Group KEY, available for group editing.

	public static Map<String, Object> genAhuSectionTemplate(SectionTypeEnum section) {
		return genAhuSectionTemplate(section.getId());
	}

	public static String getDefaultAirDirection() {
		return "S";
	}

	/**
	 * AHU 默认值
	 * 
	 * @return
	 */
	public static Map<String, Object> genAhuSectionTemplate(String sectionId) {
//		Map<String, Object> valueContainer = new HashMap<String, Object>();
//		valueContainer.putAll(AhuSectionMetas.getInstance().getSectionDefaultValue(sectionId));
//		valueContainer.remove(UtilityConstant.METAHU_PRODUCT);//行业类型模板去除机组系列属性，否则确认模板后，机组系列默认为39CQ
		return AhuSectionMetas.getInstance().getSectionDefaultValue(sectionId);
//		return valueContainer;
	}

	/**
	 * 本方法返回行业模板，这些模板定义在ahu编辑界面，例如"二进制舒适型"等。 用于基于模板快速选型
	 * 
	 * @return
	 */
    public static List<AhuTemplate> getPredefinedTemplate() {
        List<AhuTemplate> ahuTemplates = new ArrayList<>();
        for (GroupTypeEnum groupType : GroupTypeEnum.values()) {
            if (groupType.isPredefined() && !needFilterByRole(groupType)) {
                ahuTemplates.add(TemplateFactory.getTemplateFactory(groupType).createTemplate());
            }
        }
        return ahuTemplates;
    }

	/**
	 * 需要屏蔽过滤掉的行业类型
	 * 销售版本需要屏蔽非并排、双层转向
	 * @param groupType
	 * @return
	 */
	private static boolean needFilterByRole(GroupTypeEnum groupType) {
		boolean needFilter = false;
		String role = AHUContext.getUserRole();
		if (RoleEnum.Sales.name().equalsIgnoreCase(role)){
			if(groupType.getId().equalsIgnoreCase(TYPE_DOUBLE_RETURN_1.getId())){
				needFilter = true;
			}else if(groupType.getId().equalsIgnoreCase(TYPE_DOUBLE_RETURN_2.getId())){
				needFilter = true;
			}else if(groupType.getId().equalsIgnoreCase(TYPE_SIDE_BY_SIDE_RETURN_1.getId())){
				needFilter = true;
			}else if(groupType.getId().equalsIgnoreCase(TYPE_SIDE_BY_SIDE_RETURN_2.getId())){
				needFilter = true;
			}
		}
		return needFilter;
	}

	public static String getTemplateLayoutString(String templateId) {
        GroupTypeEnum groupType = GroupTypeEnum.valueIdOf(templateId);
        if (EmptyUtil.isNotEmpty(groupType)) {
            TemplateFactory templateFactory = TemplateFactory.getTemplateFactory(groupType);
            return AhuLayoutUtils.getInitAhuLayoutJsonString(templateFactory.getSectionLayout(),
                    templateFactory.getPreDefinedSectionTypes().length);
        }
        return "";
    }
    
//    public static String getTemplateLayoutString(GroupInfo group) {
//        String layoutString = getTemplateLayoutString(group.getGroupType());
//        if(EmptyUtil.isEmpty(layoutString)) {
//            List<String> sectionIds = MetaCodeGen.translateGroupCodeToIds(group.getGroupCode());
//            AhuLayoutUtils.getInitAhuLayout(layoutStyle, size)
//        }
//        GroupTypeEnum groupType = GroupTypeEnum.valueIdOf(templateId);
//        if (EmptyUtil.isNotEmpty(groupType)) {
//            TemplateFactory templateFactory = TemplateFactory.getTemplateFactory(groupType);
//            return AhuLayoutUtils.getInitAhuLayoutJsonString(templateFactory.getSectionLayout(),
//                    templateFactory.getPreDefinedSectionTypes().length);
//        }
//        return "";
//    }

	private static void collectionPerformaceRelatedParaKeys(SectionTypeEnum sectionType, List<String> result) {
		Meta meta = AhuSectionMetas.getInstance().getSectionMetaWithoutDefault(sectionType);
		Iterator<MetaParameter> it = meta.getParameters().values().iterator();
		while (it.hasNext()) {
			MetaParameter para = it.next();
			if (para.isrPerformance()) {
				result.add(para.getKey());
			}
		}
	}

	public static List<String> getPerformanceExportKeyList() {
		List<String> result = new ArrayList<>();
		for (SectionTypeEnum type : AhuSectionMetas.SECTIONTYPES) {
			collectionPerformaceRelatedParaKeys(type, result);
		}
		// collectionPerformaceRelatedParaKeys(SectionTypeEnum.TYPE_SINGLE,
		// result);
		// collectionPerformaceRelatedParaKeys(SectionTypeEnum.TYPE_COMPOSITE,
		// result);
		// collectionPerformaceRelatedParaKeys(SectionTypeEnum.TYPE_MIX,
		// result);
		// collectionPerformaceRelatedParaKeys(SectionTypeEnum.TYPE_FAN,
		// result);
		// collectionPerformaceRelatedParaKeys(SectionTypeEnum.TYPE_COLD,
		// result);
		// collectionPerformaceRelatedParaKeys(SectionTypeEnum.TYPE_AHU,
		// result);
		return result;
	}
	
	/**
	 * 根据csv模板中的数据过滤要导出的字段
	 * @param exportList
	 * @return
	 */
	public static List<String> fiterByCsv(List<String> exportList){
		List<String> result = new ArrayList<>();
		List<ExportTemplet> exportTempletsList = AhuMetadata.findAll(ExportTemplet.class);
		System.out.println(exportTempletsList);
		for (ExportTemplet et : exportTempletsList) {
			for (String etl : exportList) {
				if(etl.contains(et.getExportField())) {
					result.add(etl);
				}
			}
		}
		return result;
	}

	/**
	 * 获取组编码
	 * 
	 * @param groupType
	 * @param groupPartList
	 * @return
	 */
    public static String getGroupCodeSectionKeysSorted(String groupType, List<Part> groupPartList) {
        if (EmptyUtil.isNotEmpty(groupPartList)) {
            Collections.sort(groupPartList, new Comparator<Part>() {
                public int compare(Part o1, Part o2) {
                    return o1.getPosition().compareTo(o2.getPosition());
                }
            });
            LinkedList<String> list = new LinkedList<>();
            for (Part part : groupPartList) {
                list.add(part.getSectionKey());
            }
            return MetaCodeGen.getAhuGroupCode(list);
        }
        List<Part> parts = new ArrayList<>();
        GroupTypeEnum groupTypeEnum = GroupTypeEnum.valueIdOf(groupType);
        if (groupTypeEnum != null) {
            parts = TemplateFactory.getTemplateFactory(groupTypeEnum).createTemplate().getParts();
        }
        if (!parts.isEmpty()) {
            Collections.sort(parts, new Comparator<Part>() {
                public int compare(Part o1, Part o2) {
                    return o1.getPosition().compareTo(o2.getPosition());
                }
            });
        }
        LinkedList<String> list = new LinkedList<>();
        for (Part part : parts) {
            list.add(part.getSectionKey());
        }
        return MetaCodeGen.getAhuGroupCode(list);
    }

	/**
	 * 创建全属性、优先级最低的Unit对象
	 * 
	 * @return
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings("unchecked")
	public static Unit getTemplateUnit(Unit unit) throws IllegalAccessException, InvocationTargetException {
		Unit tempUnit = new Unit();
		if (EmptyUtil.isEmpty(unit)) {
			tempUnit.setMetaJson(JSON.toJSONString(genAhuSectionTemplate(SectionTypeEnum.TYPE_AHU)));
			return tempUnit;
		} else {
			BeanUtils.copyProperties(tempUnit, unit);
//			Map<String, Object> tempMap = genAhuSectionTemplate(SectionTypeEnum.TYPE_AHU);
			Map<String, Object> copyTempMap = new HashedMap<>(genAhuSectionTemplate(SectionTypeEnum.TYPE_AHU));
			Map<String, String> map = JSON.parseObject(unit.getMetaJson(), HashMap.class);
			if (EmptyUtil.isNotEmpty(map)) {
				copyTempMap.putAll(map);
			}
			if (EmptyUtil.isEmpty(copyTempMap)) {
				copyTempMap = new HashMap<>();
			}

			if(EmptyUtil.isNotEmpty(tempUnit.getProduct()))
				copyTempMap.put(UtilityConstant.METAHU_PRODUCT,tempUnit.getProduct());//重置product
			
			tempUnit.setMetaJson(JSON.toJSONString(copyTempMap));
			return tempUnit;
		}
	}

	/**
	 * 根据已有段信息，过滤赋值段metaJson 批量<br>
	 * 要求：part.getSectionKey()不为空，否则该part会被置成null
	 * 
	 * @param parts
	 * @param conMetaJsonMap
	 * @return
	 */
	public static List<Part> getTemplateParts(List<Part> parts, Map<String, String> conMetaJsonMap) {
		if (parts.isEmpty()) {
			return parts;
		}
		if (EmptyUtil.isEmpty(conMetaJsonMap)) {
			return parts;
		}

		for (Part part : parts) {
			part = getTemplatePart(part, conMetaJsonMap);
		}

		return parts;
	}

	/**
	 * 根据已有机组信息，过滤赋值机组metaJson <br>
	 * 
	 * @param unit
	 * @param metaJsonMap
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Unit getTemplateUnitFromConstance(Unit unit, Map<String, String> metaJsonMap) {
		if (EmptyUtil.isEmpty(unit)) {
			return null;
		}
		if (EmptyUtil.isNotEmpty(metaJsonMap)) {
			return unit;
		}
		Map<String, Object> unitMetaJsonMapToSave = new HashMap<>();
		Map<String, Object> unitMetaJsonMap = new HashMap<>();
		try {
			unitMetaJsonMap = JSON.parseObject(unit.getMetaJson(), HashMap.class);
		} catch (Exception e) {

		}
		unitMetaJsonMapToSave.putAll(genAhuSectionTemplate(SectionTypeEnum.TYPE_AHU));
		unitMetaJsonMapToSave.putAll(metaJsonMap);
		unitMetaJsonMapToSave.putAll(unitMetaJsonMap);
		unit.setMetaJson(JSON.toJSONString(unitMetaJsonMapToSave));
		return unit;
	}

	/**
	 * 根据已有段信息，过滤赋值段metaJson <br>
	 * 要求：part.getSectionKey()不为空，否则返回null
	 * 
	 * @param part
	 * @param metaJsonMap
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Part getTemplatePart(Part part, Map<String, String> metaJsonMap) {
		if (EmptyUtil.isEmpty(part)) {
			return null;
		}
		if (StringUtils.isBlank(part.getSectionKey())) {
			return null;
		}
		if (EmptyUtil.isEmpty(metaJsonMap)) {
			return part;
		}
		Map<String, Object> partMetaJsonMapToSave = new HashMap<>();
		Map<String, String> partMetaJsonMap = new HashMap<>();
		try {
			partMetaJsonMap = JSON.parseObject(part.getMetaJson(), HashMap.class);
		} catch (Exception e) {

		}
		partMetaJsonMapToSave.putAll(genAhuSectionTemplate(part.getSectionKey()));
		partMetaJsonMapToSave.putAll(partMetaJsonMap);
		for (Entry<String, String> e : metaJsonMap.entrySet()) {
			if (e.getKey().startsWith(MetaCodeGen.calculateAttributePrefix(part.getSectionKey()))) {
				partMetaJsonMapToSave.put(e.getKey(), e.getValue());
			}
		}
		part.setMetaJson(JSON.toJSONString(partMetaJsonMapToSave));

		return part;
	}

	public static Map<String, String> getDefaultParameterFromMeta() {
		Map<String, String> result = new HashMap<>();

		result.put(UtilityConstant.META_DEFAULT_SINDRYBULBT, "27.0");
		result.put(UtilityConstant.META_DEFAULT_SINWETBULBT, "19.5");
		result.put(UtilityConstant.META_DEFAULT_SINRELATIVET, "50");
		result.put(UtilityConstant.META_DEFAULT_SNEWDRYBULBT, "34.0");
		result.put(UtilityConstant.META_DEFAULT_SNEWWETBULBT, "28.2");
		result.put(UtilityConstant.META_DEFAULT_SNEWRELATIVET, "65");

		result.put(UtilityConstant.META_DEFAULT_WINDRYBULBT, "27.0");
		result.put(UtilityConstant.META_DEFAULT_WINWETBULBT, "19.5");
		result.put(UtilityConstant.META_DEFAULT_WINRELATIVET, "50");
		result.put(UtilityConstant.META_DEFAULT_WNEWDRYBULBT, "34.0");
		result.put(UtilityConstant.META_DEFAULT_WNEWWETBULBT, "28.2");
		result.put(UtilityConstant.META_DEFAULT_WNEWRELATIVET, "65");

		result.put(UtilityConstant.META_DEFAULT_MAXWPD, "60");
		result.put(UtilityConstant.META_DEFAULT_AIRVOLUMEUNIT, "0");
		result.put(UtilityConstant.META_DEFAULT_HEADERMATERIAL, "Steel");
		result.put(UtilityConstant.META_DEFAULT_INTERNALSKINTHICKNESS, "Default");
		result.put(UtilityConstant.META_DEFAULT_OUTSIDESKINTHICKNESS, "Default");
		result.put(UtilityConstant.META_DEFAULT_POWER, "0");
		result.put(UtilityConstant.META_DEFAULT_MOTORSUPPLIER, "Default");

		return result;
	}

	/**
	 * 返回所有的机组和段的属性以及默认值
	 * 
	 * @return
	 */
	public static String getAllAhuProTemplateAsString() {
		return JSON.toJSONString(AhuSectionMetas.getInstance().getAllDefaultValue());
	}

    public static Map<String, Object> getAllMaterialDefaultValue(List<Part> parts) {
        Map<String, Object> allMaterialDefaultValueMap = AhuSectionMetas.getInstance().getAllMaterialDefaultValue();
        Map<String, Object> sectionMaterialDefaultValueMap = new LinkedHashMap<>();
        List<String> metaSectionKeyPrefixes = getMetaSectionKeyPrefixes(parts);
        // include ahu material property
        metaSectionKeyPrefixes.add(SectionMetaUtils.getMetaAHUKey(StringUtils.EMPTY));
        allMaterialDefaultValueMap.forEach((key, value) -> {
            if (isBelongToSection(key, metaSectionKeyPrefixes)) {
                sectionMaterialDefaultValueMap.put(key, value);
            }
        });
        return sectionMaterialDefaultValueMap;
    }

    public static String getAllMaterialDefaultValueString(List<Part> parts) {
        return JSON.toJSONString(getAllMaterialDefaultValue(parts));
    }

    private static List<String> getMetaSectionKeyPrefixes(List<Part> parts) {
        List<String> metaSectionKeyPrefixes = new ArrayList<>();
        for (Part part : parts) {
            String metaSectionPrefix = SectionMetaUtils.getMetaSectionKey(part.getSectionKey(), "");
            metaSectionKeyPrefixes.add(metaSectionPrefix);
        }
        return metaSectionKeyPrefixes;
    }

    private static boolean isBelongToSection(String key, List<String> metaSectionKeyPrefixes) {
        for (String metaSectionKeyPrefix : metaSectionKeyPrefixes) {
            if (key.startsWith(metaSectionKeyPrefix)) {
                return true;
            }
        }
        return false;
    }

    /**
	 * 返回所有的机组和段的属性以及默认值 按照公英制key返回
	 * 
	 * @return
	 */
	public static Map<String,Map<String, Map<String, Object>>> getAllAhuProTemplate() {
		Map<String, Map<String, Object>> M = AhuSectionMetas.getInstance().getAllSectionDefaultValue();
		Map<String, Map<String, Object>> B = AhuSectionMetas.getInstance().getAllSectionDefaultValueUnitTypeB();
		Map<String,Map<String, Map<String, Object>>> ret = new HashMap<>();
		ret.put("M",M);
		ret.put("B",B);
		return ret;
	}

}

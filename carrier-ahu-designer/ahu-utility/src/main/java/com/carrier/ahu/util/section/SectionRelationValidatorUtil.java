package com.carrier.ahu.util.section;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.calculation.ValidateCalcException;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.param.ValidateParam;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.util.partition.AhuPartitionGenerator;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.gson.Gson;
import java.util.Map;
import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.constant.CommonConstant.*;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

/**
 * 段关系校验:来自 确认按钮调用的 ValidateCalEngine
 */
public final class SectionRelationValidatorUtil {

    /**
     * 确认按钮校验最新段前后顺序是否符合规则
     * @throws ApiException
     * @param validateParam
     */
    public static void confirmValidate(ValidateParam validateParam) throws ApiException {
        Gson gson = new Gson();
        String prePartKey = validateParam.getPrePartKey();
        Map<String, Object> prePartJson = validateParam.getPreParams();
        String nextPartKey = validateParam.getNextPartKey();
        Map<String, Object> nextPartJson = validateParam.getNextParams();

        //1： 无蜗壳风机前必须保留进风段（带门空段、出风段、混合段、新回排段）
        if (SystemCalculateConstants.FAN_OPTION_OUTLET_WWK.equals(validateParam.getOutlet())
                && !hasAccessDoorWWK(prePartKey,prePartJson)
                ) {
            throw new ValidateCalcException(FAN_WWK_CALCULATION_FAILED);
        }
        //2： 风机(不包含无蜗壳)后面空段为均流器的空段
        if (SectionTypeEnum.getSectionTypeFromId(validateParam.getSectionId()).equals(SectionTypeEnum.TYPE_FAN)
                && !SystemCalculateConstants.FAN_OPTION_OUTLET_WWK.equals(validateParam.getOutlet())
                && (null != validateParam.getNextPartKey() && SectionTypeEnum.getSectionTypeFromId(validateParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_ACCESS))
                && !SystemCalculateConstants.ACCESS_FUNCTION_DIFFUSER.equals(validateParam.getNextParams().get(METASEXON_ACCESS_FUNCTION))) {
            throw new ValidateCalcException(FAN_ACCESS_CALCULATION_FAILED);
        }
        //2： 空段为均流器的空段,后面不能有无蜗壳
        if (SectionTypeEnum.getSectionTypeFromId(validateParam.getSectionId()).equals(SectionTypeEnum.TYPE_ACCESS)
                && SystemCalculateConstants.ACCESS_FUNCTION_DIFFUSER.equals(validateParam.getParams().get(METASEXON_ACCESS_FUNCTION))
                && (null != validateParam.getNextPartKey() && SectionTypeEnum.getSectionTypeFromId(validateParam.getNextPartKey()).equals(SectionTypeEnum.TYPE_FAN))
                && SystemCalculateConstants.FAN_OPTION_OUTLET_WWK.equals(validateParam.getNextParams().get(METASEXON_FAN_OUTLET))) {
            throw new ValidateCalcException(ACCESS_FAN_CALCULATION_FAILED);
        }


        /**
         * 3:
         * a.V型高效过滤器的检修段在前面，即6M+3M;箱型高效过滤器的检修段在后面，即3M+6M；
         * b.高效过滤段前/后有空段/混合段或者出风段时，且段长>=6M时，可省略"
         */
        if (SectionTypeEnum.getSectionTypeFromId(validateParam.getSectionId()).equals(SectionTypeEnum.TYPE_HEPAFILTER)) {
            Map<String, Object> hepaFilterJson = validateParam.getParams();

            //3M高效过滤段的前面一个段不可以是加湿段
            if(EmptyUtil.isNotEmpty(prePartKey)){
                if(isHumidifierPart(prePartKey)){
                    throw new ValidateCalcException(SECTION_HEPA_PRE_NO_HUMIDIFIER_FIX);
                }
            }
            String fitetF = String.valueOf(hepaFilterJson.get(METASEXON_HEPAFILTER_FITETF));
            if(HEPAFILTER_FILTERF_V.equals(fitetF)){
            	
            	if(EmptyUtil.isEmpty(prePartJson)) {
            		throw new ValidateCalcException(SECTION_HEPA_PRE_FIX);
            	}
                int sectionL = NumberUtil.convertStringToDoubleInt(""+prePartJson.get(METASEXON_PREFIX+prePartKey.replace("ahu.","")+METACOMMON_POSTFIX_SECTIONL));
                if(hasAccessDoorHEPAFilter(prePartKey,prePartJson) && sectionL >= 6){

                }else{
                    throw new ValidateCalcException(SECTION_HEPA_PRE_FIX);
                }
            }
            if(HEPAFILTER_FILTERF_P.equals(fitetF)){
            	if(EmptyUtil.isEmpty(nextPartJson)) {
            		throw new ValidateCalcException(SECTION_HEPA_AFTER_FIX);
            	}
            	
                int sectionL = NumberUtil.convertStringToDoubleInt(""+nextPartJson.get(METASEXON_PREFIX+nextPartKey.replace("ahu.","")+METACOMMON_POSTFIX_SECTIONL));
                if(hasAccessDoorHEPAFilter(nextPartKey,nextPartJson) && sectionL >= 6){

                }else{
                    throw new ValidateCalcException(SECTION_HEPA_AFTER_FIX);
                }
            }
        }
        /**
         * 4:
         * 静电过滤板式前面不能有单层（静电过滤，蜂窝前面可以有单层）
         */
        if (SectionTypeEnum.getSectionTypeFromId(validateParam.getSectionId()).equals(SectionTypeEnum.TYPE_ELECTROSTATICFILTER)) {
            Map<String, Object> filterJson = validateParam.getParams();

            String fitetType = String.valueOf(filterJson.get(METASEXON_ELECTROSTATICFILTER_FILTERTYPE));
            if(ELECTROSTATICFILTER_FILTERTYPE_P.equals(fitetType)
                    && EmptyUtil.isNotEmpty(prePartJson)
                    && SectionTypeEnum.TYPE_SINGLE.equals(SectionTypeEnum.getSectionTypeFromId(prePartKey))){
                    throw new ValidateCalcException(SECTION_ELECTROSTATIC_FILTER_FIX);
            }

            //改为页面validate json 提醒。
            /*if(ELECTROSTATICFILTER_FILTERTYPE_CELLULAR.equals(fitetType)
                    && EmptyUtil.isNotEmpty(prePartJson)
                    && !SectionTypeEnum.TYPE_SINGLE.equals(SectionTypeEnum.getSectionTypeFromId(prePartKey))
                    && !SectionTypeEnum.TYPE_COMPOSITE.equals(SectionTypeEnum.getSectionTypeFromId(prePartKey))){
                throw new ValidateCalcException(PARTITION_POS_VALIDATE_ELECTROSTATICFILTER1);
            }*/
        }


        /**
         * 5:段关系》可通过段：只适用于过滤段
         *
         * 1.单层过滤段：软件起判断作用，段之前必须是可通过段，非限制（TODO 需要前段一起设计），单层如果前面不是可通过需要将单层抽取方式改成：外抽、侧抽。
         *
         * 2.综合过滤段：软件起限制作用，综合过滤段之前必须是可通过段，如果不是，报错，无法继续选型
         *
         * 3.高效：软件没有限制，只是根据V型或箱型过滤器加以提示。高效的警告文字也对的，就是把后台定义的检修段，改成可通过段就行
         *
         * 4.静电：和综合过滤段一样限制
         *
         * 5.无蜗壳风机段属于向后可通过，有检修门
         *
         * 6.单层、综合过滤段 1015及以上的可以作为可通过段。
         */
        if (SectionTypeEnum.getSectionTypeFromId(validateParam.getSectionId()).equals(SectionTypeEnum.TYPE_SINGLE)
                || SectionTypeEnum.getSectionTypeFromId(validateParam.getSectionId()).equals(SectionTypeEnum.TYPE_COMPOSITE)
                || SectionTypeEnum.getSectionTypeFromId(validateParam.getSectionId()).equals(SectionTypeEnum.TYPE_ELECTROSTATICFILTER)) {
            if(EmptyUtil.isNotEmpty(prePartJson)) {
                if(canPassPart(prePartKey)
                        || SystemCalculateConstants.FAN_OPTION_OUTLET_WWK.equals(prePartJson.get(METASEXON_FAN_OUTLET))
                        || filterIsPassPart(prePartKey,validateParam.getSerial())
                        ){

                }else{
                    throw new ValidateCalcException(SECTION_FILTER_PRE_FIX);
                }
            }
        }


        /**
         * 6:选二级/三级电机时，直接启动柜电机功率范围为：0.55-7.5KW，星三角启动柜适用于电机功率大于等于11KW
         */
        Double motorPower = NumberUtil.convertStringToDouble(validateParam.getMotorPower());
        String motorType = validateParam.getType();
        String startStyle = validateParam.getStartStyle();
        if(("1".equalsIgnoreCase(motorType) || "2".equalsIgnoreCase(motorType)) && !startStyle.equalsIgnoreCase(JSON_FAN_STARTSTYLE_NO)){//选二级/三级电机,有启动柜
            if(motorPower>=0.55 && motorPower<=7.5 && !startStyle.equalsIgnoreCase(JSON_FAN_STARTSTYLE_DIRECT)){
                throw new ValidateCalcException(SECTION_FAN_MOTORTYPE_MOTORPOWER_STARTSTYLE);
            }
            if(motorPower>=11 && !startStyle.equalsIgnoreCase(JSON_FAN_STARTSTYLE_STAR)){
                throw new ValidateCalcException(SECTION_FAN_MOTORTYPE_MOTORPOWER_STARTSTYLE);
            }
        }
    }

    /**
     *  单层、综合过滤段 1015及以上的可以作为可通过段。
     * @param prePartKey
     * @param serial
     * @return
     */
    private static boolean filterIsPassPart(String prePartKey, String serial) {

        String cutSerial = serial.substring(serial.length() - 4, serial.length());
        Integer ser = Integer.parseInt(cutSerial);

        if((SectionTypeEnum.TYPE_SINGLE.getId().equalsIgnoreCase(prePartKey)
            || SectionTypeEnum.TYPE_COMPOSITE.getId().equalsIgnoreCase(prePartKey))
            && ser >= 1015){
            return true;
        }
        return false;
    }

    /**
     * 高效过滤段V箱型，前/后有空段/混合段或者出风段时，且段长>=6M时，
     * 可省略（6M+3M、3M+6M）的规则。
     *
     * @param partKey
     * @param section
     * @return
     */
    private static boolean hasAccessDoorHEPAFilter(String partKey, Map<String, Object> section) {
        String sectionId = partKey;
        if (!AhuPartitionGenerator.SECTION_WITH_DOOR_HEPAFilter.contains(sectionId)) {
            return false;
        }
        String accessDoor = null;
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(SectionTypeEnum.TYPE_MIX, section, MetaKey.KEY_DOORO);
            return SystemCalculateConstants.MIX_DOORO_DOORNOVIEWPORT.equals(accessDoor)
                    || SystemCalculateConstants.MIX_DOORO_DOORWITHVIEWPORT.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(SectionTypeEnum.TYPE_DISCHARGE, section, MetaKey.KEY_ODOOR);
            return SystemCalculateConstants.DISCHARGE_ODOOR_DN.equals(accessDoor)
                    || SystemCalculateConstants.DISCHARGE_ODOOR_DY.equals(accessDoor);
        }else if (SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(SectionTypeEnum.TYPE_ACCESS, section, MetaKey.KEY_ODOOR);
            return SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(accessDoor)
                    || SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionId)) {
            String osDoor = getSectionParameterByKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, section,
                    MetaKey.KEY_OSDOOR);
            String isDoor = getSectionParameterByKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, section,
                    MetaKey.KEY_ISDOOR);
            return SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(osDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DN.equals(osDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(isDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DN.equals(isDoor);
        }
        return false;
    }
    /**
     * 是否无蜗壳前面的检修段:带门（空段、出风段、混合段、新回排段）
     *
     * @param prePartKey
     * @param section
     * @return
     */
    private static boolean hasAccessDoorWWK(String prePartKey, Map<String, Object> section) {
        String sectionId = prePartKey;
        if (!AhuPartitionGenerator.SECTION_WITH_DOOR_WWK.contains(sectionId)) {
            return false;
        }
        String accessDoor = null;
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(SectionTypeEnum.TYPE_MIX, section, MetaKey.KEY_DOORO);
            return SystemCalculateConstants.MIX_DOORO_DOORNOVIEWPORT.equals(accessDoor)
                    || SystemCalculateConstants.MIX_DOORO_DOORWITHVIEWPORT.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(SectionTypeEnum.TYPE_DISCHARGE, section, MetaKey.KEY_ODOOR);
            return SystemCalculateConstants.DISCHARGE_ODOOR_DN.equals(accessDoor)
                    || SystemCalculateConstants.DISCHARGE_ODOOR_DY.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(SectionTypeEnum.TYPE_ACCESS, section, MetaKey.KEY_ODOOR);
            return SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(accessDoor)
                    || SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionId)) {
            String osDoor = getSectionParameterByKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, section,
                    MetaKey.KEY_OSDOOR);
            String isDoor = getSectionParameterByKey(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, section,
                    MetaKey.KEY_ISDOOR);
            return SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(osDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DN.equals(osDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(isDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DN.equals(isDoor);
        }
        return false;
    }
    /**
     * 是否通过段
     *
     * @param partKey
     * @return
     */
    private static boolean canPassPart(String partKey) {
        String sectionId = partKey;
        if (AhuPartitionGenerator.SECTION_CAN_PASS.contains(sectionId)) {
            return true;
        }
        return false;
    }

    /**
     * 是否加湿段
     *
     * @param partKey
     * @return
     */
    private static boolean isHumidifierPart(String partKey) {
        String sectionId = partKey;
        if (AhuPartitionGenerator.SECTION_HUMIDIFIER.contains(sectionId)) {
            return true;
        }
        return false;
    }

    public static String getSectionParameterByKey(SectionTypeEnum sectionType,
                                                  Map<String, Object> section, String metaKey) {
        return String.valueOf(section.get(SectionMetaUtils.getMetaSectionKey(sectionType, metaKey)));
    }

}

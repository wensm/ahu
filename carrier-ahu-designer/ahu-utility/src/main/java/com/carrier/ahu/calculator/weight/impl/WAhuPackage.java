package com.carrier.ahu.calculator.weight.impl;

import com.carrier.ahu.ResourceConstances;
import com.carrier.ahu.calculator.weight.CalculatorForModel1;
import com.carrier.ahu.calculator.weight.WeightCalculatorConstances;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;

public class WAhuPackage extends CalculatorForModel1 {

	public WAhuPackage() {
		super(UtilityConstant.METACOMMON_CAL_AHU_PACKAGE);
	}

	protected String getTabKey(AhuParam ahu, PartParam part) {
		// TODO 这儿计算key值
		return UtilityConstant.SYS_ALPHABET_C_UP + UtilityConstant.SYS_ALPHABET_K_UP + UtilityConstant.SYS_ALPHABET_D_UP
				+ UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN + UtilityConstant.SYS_ALPHABET_C_UP
				+ UtilityConstant.SYS_ALPHABET_K_UP + UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN
				+ UtilityConstant.SYS_STRING_NUMBER_3 + UtilityConstant.SYS_STRING_NUMBER_8
				+ UtilityConstant.SYS_STRING_NUMBER_0;// "CKD_CK_380"
	}

	protected String getColumnKey(AhuParam ahu, PartParam part) {
		// TODO 这儿获取分段的长度，分段的属性值，会在后期注入
		return UtilityConstant.SYS_STRING_NUMBER_1+UtilityConstant.SYS_STRING_NUMBER_0;
	}

	protected String getRowKey(AhuParam ahu, PartParam part) {
		return ahu.getSeries();
	}

	public String getType() {
		return UtilityConstant.METACOMMON_TYPE_PARTITION;
	}

	@Override
	protected String getResourcePath() {
		return UtilityConstant.SYS_PATH_W_AHU_PACKAGE;
	}

}

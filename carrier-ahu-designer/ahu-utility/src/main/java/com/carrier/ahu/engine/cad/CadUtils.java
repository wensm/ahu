package com.carrier.ahu.engine.cad;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.exception.engine.EngineException;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.util.ExecUtils;
import com.carrier.ahu.vo.SysConstants;

/**
 * Created by liaoyw on 2017/4/16.
 */
public class CadUtils {
	static Logger logger = LoggerFactory.getLogger(CadUtils.class);

//	static final String UtilityConstant.SYS_PATH_CAD_EXPORTER_PATH = SysConstants.ASSERT_DIR + "bin" + File.separator + "cad_exporter.exe";
//	static final String UtilityConstant.SYS_PATH_CAD_DEST_DIR = SysConstants.ASSERT_DIR;
//	static final String UtilityConstant.SYS_PATH_INI_NAME = "UnitConfig.ini";

	public static void exportBmp(AhuParam params, String bmpDestPath) {
		String destPath = UtilityConstant.SYS_PATH_CAD_DEST_DIR + bmpDestPath;
		File f = new File(destPath);
		File parentDir = f.getParentFile();
		if (!parentDir.exists() && !parentDir.mkdirs()) {
			throw new EngineException("Can't create cad file dir: " + destPath);
		}
		String fileName = UtilityConstant.SYS_BLANK + System.currentTimeMillis();
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName();
		}
		
		String iniPath = new File(parentDir, String.format("%s-%s", fileName, UtilityConstant.SYS_PATH_INI_NAME))
				.getAbsolutePath();

		try {
			// IniUtils.create(params, iniPath);
			// 在IniUtils2中修改对应段的信息集成，完成以后替换为IniUtils2
			IniUtils2.create(params, iniPath);
		} catch (IOException e) {
			throw new EngineException("Can not export cad bmp", e);
		}
		String iniPaths = UtilityConstant.SYS_PUNCTUATION_DQUOTATION + iniPath + UtilityConstant.SYS_PUNCTUATION_DQUOTATION;
		destPath = UtilityConstant.SYS_PUNCTUATION_DQUOTATION + destPath + UtilityConstant.SYS_PUNCTUATION_DQUOTATION;
		createBmp(iniPaths, destPath);
	}

	static void createBmp(String iniFilePath, String outputPath) {
		String cmd = String.format("%s %s %s", UtilityConstant.SYS_PATH_CAD_EXPORTER_PATH, outputPath, iniFilePath);
		ExecUtils.CommandResult r = ExecUtils.executeCmd(cmd);
		logger.info(r.toString());
		if (!r.ok) {
			throw new EngineException("Error export cad bmp: " + r);
		}
	}
}

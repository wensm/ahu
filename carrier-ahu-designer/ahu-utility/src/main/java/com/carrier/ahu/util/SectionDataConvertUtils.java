package com.carrier.ahu.util;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.FanEnginesEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.datahouse.common.FacaVelocityUtil;
import com.carrier.ahu.length.param.FilterPanelBean;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.cad.DamperDimensionL;
import com.carrier.ahu.metadata.entity.calc.SUVCLight;
import com.carrier.ahu.metadata.entity.coil.SCoilInfo;
import com.carrier.ahu.metadata.entity.coil.SCoilInfo1;
import com.carrier.ahu.metadata.entity.fan.FanCodeElectricSize;
import com.carrier.ahu.metadata.entity.fan.FanCodeElectricSizeBig;
import com.carrier.ahu.metadata.entity.fan.SKFanMotor;
import com.carrier.ahu.metadata.entity.fan.STwoSpeedMotor;
import com.carrier.ahu.metadata.entity.report.*;
import com.carrier.ahu.metadata.entity.section.SectionArea;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.report.ReportData;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.Map.Entry;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_SINGLE;
import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.constant.CommonConstant.*;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.*;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

/**
 * Created by LIANGD4 on 2018/1/3. 报告使用
 */
public class SectionDataConvertUtils {

    public static final String PRESSURE_RANGE = "0.1～0.6Mpa";
    public static final String POWER_STANDARD = "AC22V、50HZ,400w";

    // 封装控制段
    public static Map<String, String> getCtr(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_MIX_PREFIX,noChangeMap, map);// 计算风量
        String controlCabinet = UtilityConstant.METASEXON_CTR_CONTROLCABINET;// 控制柜
        String iTaHSensor = UtilityConstant.METASEXON_CTR_ITAHSENSOR;// 室内温湿度传感器
        String iCCSensor = UtilityConstant.METASEXON_CTR_ICCSENSOR;// 室内二氧化碳浓度传感器
        String aPHSensor = UtilityConstant.METASEXON_CTR_APHSENSOR;// 风管静压传感器
        String waterValveActuator = UtilityConstant.METASEXON_CTR_WATERVALVEACTUATOR;// 水阀执行器
        // 控制柜 TODO
        map.put(controlCabinet, getIntlString(ELEVEN_KW_VFC_CABINET));
        if (map.containsKey(iTaHSensor)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(iTaHSensor));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(iTaHSensor, getIntlString(INDOOR_TEMPERATURE_HUMIDITY_SENSOR));
            } else {
                map.put(iTaHSensor, UtilityConstant.SYS_BLANK);
            }
        }
        if (map.containsKey(iCCSensor)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(iCCSensor));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(iCCSensor, getIntlString(INDOOR_CO2_CONCENTRATION_SENSOR));
            } else {
                map.put(iCCSensor, UtilityConstant.SYS_BLANK);
            }
        }
        if (map.containsKey(aPHSensor)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(aPHSensor));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(aPHSensor, getIntlString(AIR_DUCT_STATIC_PRESSURE_SENSOR));
            } else {
                map.put(aPHSensor, UtilityConstant.SYS_BLANK);
            }
        }
        if (map.containsKey(waterValveActuator)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(waterValveActuator));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(waterValveActuator, getIntlString(WATER_VALVE_ACTUATOR));
            } else {
                map.put(waterValveActuator, UtilityConstant.SYS_BLANK);
            }
        }
        return map;
    }

    // 封装静电过滤过滤段
    public static Map<String, String> getElectrostaticFilter(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_ELECTROSTATICFILTER_PREFIX,noChangeMap, map);// 计算风量
        String supplier = UtilityConstant.METASEXON_ELECTROSTATICFILTER_SUPPLIER;// 供应商
        String way = UtilityConstant.METASEXON_ELECTROSTATICFILTER_WAY;// 过滤形式
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        map.put(way, getIntlString(ELECTROSTATIC_FILTRATION));
        return map;
    }


    /**
     * 封装高效过滤段
     * add by gaok2 去除"不管界面选什么，供应商的值都强制置为标准供应商的逻辑"
     * @param noChangeMap
     * @param map
     * @param language
     * @return
     */
    public static Map<String, String> getHEPAFilter(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_HEPAFILTER_FITETF,noChangeMap, map);// 计算风量
        //String supplier = UtilityConstant.METASEXON_HEPAFILTER_SUPPLIER;// 供应商
        String filterArrange = UtilityConstant.METASEXON_HEPAFILTER_FILTERARRANGE;// 过滤器布置
        /* 规格信息 */
        String para1 = UtilityConstant.METASEXON_HEPAFILTER_PARA1;
        String paraN1 = UtilityConstant.METASEXON_HEPAFILTER_PARAN1;
        String para2 = UtilityConstant.METASEXON_HEPAFILTER_PARA2;
        String paraN2 = UtilityConstant.METASEXON_HEPAFILTER_PARAN2;
        //map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        if (map.containsKey(filterArrange)) {// 封装过滤器布置
            String value = noChangeMap.get(filterArrange).toString();
            List<FilterPanelBean> filterPanelBeanList = JSONArray.parseArray(value, FilterPanelBean.class);
            if (null != filterPanelBeanList && filterPanelBeanList.size() > 0) {
                int i = 1;
                for (FilterPanelBean filterPanelBean : filterPanelBeanList) {
                    if (i == 1) {
                        if (null != filterPanelBean.getOption()) {
                            map.put(para1, filterPanelBean.getOption());
                        }
                        if (null != filterPanelBean.getCount()) {
                            map.put(paraN1, filterPanelBean.getCount());
                        }
                    } else {
                        if (null != filterPanelBean.getOption()) {
                            map.put(para2, filterPanelBean.getOption());
                        }
                        if (null != filterPanelBean.getCount()) {
                            map.put(paraN2, filterPanelBean.getCount());
                        }
                    }
                    i++;
                }
            }
        }
        return map;
    }

    // 封装消声段
    public static Map<String, String> getDischarge(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_DISCHARGE_PREFIX,noChangeMap, map);// 计算风量
        return map;
    }

    // 封装消声段
    public static Map<String, String> getAttenuator(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_ATTENUATOR_PREFIX,noChangeMap, map);// 计算风量
        String deadening = UtilityConstant.METASEXON_ATTENUATOR_DEADENING;// 消音级数
        String type = UtilityConstant.METASEXON_ATTENUATOR_TYPE;// 消音器类型
        return map;
    }

    // 封装空段
    public static Map<String, String> getAccess(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_ACCESS_PREFIX,noChangeMap, map);// 计算风量
        String functionS = UtilityConstant.METASEXON_ACCESS_FUNCTIONS;// 功能
        String functions = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_ACCESS_FUNCTION));
        map.put(functionS, getIntlString(EMPTY_SECTION));
        if (UtilityConstant.SYS_STRING_DIFFUSER.equals(functions)) {
            map.put(functionS, getIntlString(DIFFUSER));
        }
        return map;
    }

    // 封装新回排风段
    public static Map<String, String> getCombinedMixingChamber(Map<String, String> noChangeMap, Map<String, String> metaMap, LanguageEnum language) {
        metaMap = getAirVolume(CommonConstant.METASEXON_COMBINEDFILTER_PREFIX,noChangeMap, metaMap);// 计算风量
        String way = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WAY;
        String NARatio = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_NARATIO;
        String SNAVolume = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_SNAVOLUME;
        String sairvolume = UtilityConstant.METAHU_SAIRVOLUME;
        String windValveP1 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEP1;
        String windValveP2 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEP2;
        String windValveH1 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEH1;
        String windValveH2 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEH2;
        String windValveN1 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEN1;
        String windValveN2 = UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_WINDVALVEN2;
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String damperMeterial = UtilityConstant.METASEXON_COMBINEDFILTER_DAMPERMETERIAL;
        String damperMeterialValue = UtilityConstant.SYS_STRING_NUMBER_1;//镀锌板
        if (metaMap.containsKey(damperMeterial)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(damperMeterial));
            if (SystemCalculateConstants.COMBINEDMIXINGCHAMBER_DAMPERMETERIAL_AL.equals(value)) {
                damperMeterialValue = UtilityConstant.SYS_STRING_NUMBER_2;//铝合金
            }
        }

        String serialValue = UtilityConstant.SYS_BLANK;
        if (metaMap.containsKey(serial)) {
            serialValue = BaseDataUtil.constraintString(noChangeMap.get(serial));
        }

        List<DamperDimensionL> damperDimensionLList = AhuMetadata.findAll(DamperDimensionL.class);
        List<AdjustAirDoor> adjustAirDoorList = AhuMetadata.findAll(AdjustAirDoor.class);
        if (!EmptyUtil.isEmpty(damperDimensionLList)) {
            for (DamperDimensionL damperDimensionL : damperDimensionLList) {
                if (serialValue.equals(damperDimensionL.getUnit()) && damperMeterialValue.equals(damperDimensionL.getDamperType()) && UtilityConstant.SYS_STRING_NUMBER_1.equals(damperDimensionL.getMixType())) {
                    int value = BaseDataUtil.stringConversionInteger(damperDimensionL.getC())/* + 108*/;//三个系列都不加108数据已经维护最新
                    /*if(serialValue.contains(SystemCalculateConstants.AHU_PRODUCT_39G)){
                        value -= 108;//G系列暂时取消108逻辑
                    }*/

                    metaMap.put(windValveP1, damperDimensionL.getA() + UtilityConstant.SYS_ALPHABET_X + value);
                    metaMap.put(windValveN1, damperDimensionL.getA() + UtilityConstant.SYS_ALPHABET_X + value);
                } else if (serialValue.equals(damperDimensionL.getUnit()) && damperMeterialValue.equals(damperDimensionL.getDamperType()) && UtilityConstant.SYS_STRING_NUMBER_2.equals(damperDimensionL.getMixType())) {
                    metaMap.put(windValveH1, damperDimensionL.getA() + UtilityConstant.SYS_ALPHABET_X + damperDimensionL.getC());
                }
            }
        }

        if (!EmptyUtil.isEmpty(adjustAirDoorList)) {
            for (AdjustAirDoor adjustAirDoor : adjustAirDoorList) {
                if (serialValue.equals(adjustAirDoor.getFanType()) && UtilityConstant.SYS_ALPHABET_L_UP.equals(adjustAirDoor.getSection()) && UtilityConstant.SYS_STRING_NUMBER_2.equals(adjustAirDoor.getWhichFlag())) {
                    if (damperMeterialValue.equals(UtilityConstant.SYS_STRING_NUMBER_2)) {
                        metaMap.put(windValveP2, adjustAirDoor.getWringqAl());
                    } else {
                        metaMap.put(windValveP2, adjustAirDoor.getWringq());
                    }
                } else if (serialValue.equals(adjustAirDoor.getFanType()) && UtilityConstant.SYS_ALPHABET_L_UP.equals(adjustAirDoor.getSection()) && UtilityConstant.SYS_STRING_NUMBER_1.equals(adjustAirDoor.getWhichFlag())) {
                    if (damperMeterialValue.equals(UtilityConstant.SYS_STRING_NUMBER_2)) {
                        metaMap.put(windValveH2, adjustAirDoor.getWringqAl());
                    } else {
                        metaMap.put(windValveH2, adjustAirDoor.getWringq());
                    }
                } else if (serialValue.equals(adjustAirDoor.getFanType()) && UtilityConstant.SYS_ALPHABET_L_UP.equals(adjustAirDoor.getSection()) && "3".equals(adjustAirDoor.getWhichFlag())) {
                    if (damperMeterialValue.equals(UtilityConstant.SYS_STRING_NUMBER_2)) {
                        metaMap.put(windValveN2, adjustAirDoor.getWringqAl());
                    } else {
                        metaMap.put(windValveN2, adjustAirDoor.getWringq());
                    }
                }
            }
        }

        if (metaMap.containsKey(sairvolume) && metaMap.containsKey(SNAVolume)) {
            String airValue = BaseDataUtil.constraintString(noChangeMap.get(sairvolume));
            String airNewValue = BaseDataUtil.constraintString(noChangeMap.get(SNAVolume));
            double douAirValue = BaseDataUtil.stringConversionDouble(airValue);
            double douAirNewValue = BaseDataUtil.stringConversionDouble(airNewValue);
            if (douAirValue > 0 && douAirNewValue > 0) {
                double value = BaseDataUtil.decimalConvert(douAirNewValue / douAirValue, 2) * 100;
                metaMap.put(NARatio, BaseDataUtil.integerConversionString(BaseDataUtil.doubleConversionInteger(value)));
            } else {
                metaMap.put(NARatio, UtilityConstant.SYS_STRING_NUMBER_0);
            }
        }
        metaMap.put(way, getIntlString(FRESH_RETURN_AIR));

        /* add executor comment with air interface */
        String AInterfaceMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER, KEY_AINTERFACE);
        String AInterface = String.valueOf(noChangeMap.get(AInterfaceMetaKey));
        if (SystemCalculateConstants.COMBINEDMIXINGCHAMBER_AINTERFACE_ED.equals(AInterface)
                || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_AINTERFACE_FL.equals(AInterface)) {
            String Executor1MetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER, KEY_EXECUTOR1);
            String Executor1 = String.valueOf(noChangeMap.get(Executor1MetaKey));
            String executorString = getValueByUnit(Executor1, Executor1MetaKey, TYPE_COMBINEDMIXINGCHAMBER, language);
            if (StringUtils.isNotEmpty(executorString)) {
                AInterface = getValueByUnit(AInterface, AInterfaceMetaKey, TYPE_COMBINEDMIXINGCHAMBER, language);
                metaMap.put(AInterfaceMetaKey, String.format("%s(%s)", AInterface, executorString));
            }
        }

        /* add fix repair lamp with door */
        String fixRepairLampMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER,
                KEY_FIX_REPAIR_LAMP);
        String osDoorMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER, KEY_OSDOOR);
        String osDoor = String.valueOf(noChangeMap.get(osDoorMetaKey));
        String isDoorMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER, KEY_ISDOOR);
        String isDoor = String.valueOf(noChangeMap.get(isDoorMetaKey));
        String posSizeMetaKey = SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER, KEY_POS_SIZE);

        String isDoorSize = "";
        String osDoorSize = "";
        String PositivePressureDoor = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_POSITIVEPRESSUREDOOR));//正压门
        if(Boolean.parseBoolean(PositivePressureDoor)){
            PositivePressureDoor = getIntlString(POSITIVE_PRESSURE_DOOR)+SYS_PUNCTUATION_SLIGHT_PAUSE;
            String ahuDoorDirection = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METAHU_DOORORIENTATION));//开门方向
            String combinedMixingChamberSectionl = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_SECTIONL));//段长

            /*请在选型报告中增加门尺寸及方向，其中尺寸数据从表格中读取
            段长等于5M时，读取SECTIONL=5
            段长大于5M时,读取SECTIONL=6*/

            XtDoor xtDoor = AhuMetadata.findOne(XtDoor.class, noChangeMap.get(UtilityConstant.METAHU_SERIAL), String.valueOf(NumberUtil.convertStringToDoubleInt(combinedMixingChamberSectionl)>5?6:5));
            String xtDoorSize = "";
            if(EmptyUtil.isNotEmpty(xtDoor)){
                xtDoorSize = xtDoor.getXtDoorw()+"*"+xtDoor.getXtDoorh();
            }
            String xtDoorDirection = "";
            if(ahuDoorDirection.equals(AHU_DOORORIENTATION_LEFT)){
                xtDoorDirection = getIntlString(TO_RIGHT);
            }else if(ahuDoorDirection.equals(AHU_DOORORIENTATION_RIGHT)){
                xtDoorDirection = getIntlString(TO_LEFT);
            }
            PositivePressureDoor += xtDoorDirection +xtDoorSize;

            //新风侧没有正压门
            /*if(!COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(isDoor)){
                isDoorSize += PositivePressureDoor;
            }*/
            if(!COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(osDoor)){
                osDoorSize += PositivePressureDoor;
            }
            metaMap.put(posSizeMetaKey, xtDoor.getXtDoorwSize() + "*" + xtDoor.getXtDoorhSize());
        }

        isDoor = getValueByUnit(isDoor, isDoorMetaKey, TYPE_COMBINEDMIXINGCHAMBER, language);
        osDoor = getValueByUnit(osDoor, osDoorMetaKey, TYPE_COMBINEDMIXINGCHAMBER, language);

        String fixRepairLampString = getIntlString(HAS_FIX_REPAIR_LAMP);
        if (!Boolean.valueOf(String.valueOf(noChangeMap.get(fixRepairLampMetaKey)))) {
            fixRepairLampString = getIntlString(NO_FIX_REPAIR_LAMP);
        }
        metaMap.put(isDoorMetaKey, isDoor+ UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + isDoorSize + fixRepairLampString + UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
        metaMap.put(osDoorMetaKey, osDoor+ UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + osDoorSize + fixRepairLampString + UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
        return metaMap;
    }

    private static String getValueByUnit(String value, String parameterKey, SectionTypeEnum sectionType,
                                         LanguageEnum language) {
        if (StringUtils.isEmpty(value) || UtilityConstant.SYS_ASSERT_NULL.equals(value)) {
            return StringUtils.EMPTY;
        }
        return AhuSectionMetas.getInstance().getValueByUnit(value, parameterKey, sectionType.getId(), language);
    }

    // 封装干蒸加湿段
    public static Map<String, String> getSteamHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_STEAMHUMIDIFIER_PREFIX,noChangeMap, map);// 计算风量
        String way = UtilityConstant.METASEXON_STEAMHUMIDIFIER_WAY;// 加湿方式
        String supplier = UtilityConstant.METASEXON_STEAMHUMIDIFIER_SUPPLIER;// 供应商
        String tubeDiameter = UtilityConstant.METASEXON_STEAMHUMIDIFIER_TUBEDIAMETER;// 进气管径
        String type = UtilityConstant.METASEXON_STEAMHUMIDIFIER_HTYPES;// 类型
        String ControlM = UtilityConstant.METASEXON_STEAMHUMIDIFIER_CONTROLM;//控制方式
        if (map.containsKey(type)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(type));
            if ((NumberUtil.convertStringToDoubleInt(value)) == 1) {
                map.put(tubeDiameter, "DN15");
            } else if ((NumberUtil.convertStringToDoubleInt(value)) == 2) {
                map.put(tubeDiameter, "DN20");
            } else if ((NumberUtil.convertStringToDoubleInt(value)) == 3) {
                map.put(tubeDiameter, "DN25");
            } else if ((NumberUtil.convertStringToDoubleInt(value)) == 4) {
                map.put(tubeDiameter, "DN30");
            }
        }
        map.put(way, getIntlString(DRY_STEAM_AIR));
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));

        String ctlMValue = BaseDataUtil.constraintString(noChangeMap.get(ControlM));
        if (EmptyUtil.isNotEmpty(ctlMValue) && "EC".equals(ctlMValue)) {
            map.put(ControlM, getIntlString(CONTROLM_EC));
        }
        return map;
    }

    // 封装电极加湿段
    public static Map<String, String> getElectrodeHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_ELECTRODEHUMIDIFIER_PREFIX,noChangeMap, map);// 计算风量
        String way = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WAY;// 加湿方式
        String supplier = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_SUPPLIER;// 供应商
        String waterQuality = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WATERQUALIITY;// 供水水质
        String pressure = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_PRESSURE;// 进水压力
        String tubeDiameter = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_TUBEDIAMETER;// 进气管径
        String wasteTubeDiameter = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WASTETUBEDIAMETER;// 排污管径
        String power = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_POWER;// 额定功率 查询表
        String controller = UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_CONTROLLER;// 控制方式
        // TODO
        map.put(way, getIntlString(ELECTRODE_HUMIDIFICATION));
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        map.put(waterQuality, getIntlString(CLEAN_TAP_WATER));
        map.put(pressure, "0.1~0.35Mpa");
        map.put(tubeDiameter, "DN15");
        map.put(wasteTubeDiameter, ">=DN35");
        map.put(controller, getIntlString(ANALOG_VALUE) + "0-10V");
        return map;
    }

    // 封装高压喷雾段
    public static Map<String, String> getSprayHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_SPRAYHUMIDIFIER_PREFIX,noChangeMap, map);// 计算风量
        String way = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_WAY;// 加湿方式
        String waterQuality = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_WATERQUALITY;// 供水水质
        String quantity = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_QUANTITY;// 供水量
        String pressure = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_PRESSURE;// 进水压力
        String power = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_POWER;// 电源与额定功率
        String supplier = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_SUPPLIER;// 供应商
        String install = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_INSTALL;// 安装段
        String previous = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_PREVIOUS;// 当前段标识
        if (map.containsKey(previous)) {
            String value = noChangeMap.get(previous).toString();
            if (value.equals(SectionTypeEnum.TYPE_COLD.getId())) {
                map.put(install, getIntlString(COOLING_COIL_SECTION));
            } else if (value.equals(SectionTypeEnum.TYPE_HEATINGCOIL.getId())) {
                map.put(install, getIntlString(HOT_WATER_COIL_SECTION));
            }
        }
        map.put(way, getIntlString(HIGH_PRESSURE_SPRAY));
        map.put(waterQuality, getIntlString(TAP_WATER));
        map.put(quantity, getIntlString(ONE_HALF_TIMES_GREATER_THAN_SPRAY_AMOUNT));
        map.put(pressure, PRESSURE_RANGE);
        map.put(power, POWER_STANDARD);
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        return map;
    }

    //TODO 逻辑混乱 封装电加热盘管段
    public static Map<String, String> getElectricHeatingCoil(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
    	map = getAirVolume(CommonConstant.METASEXON_ELECTRICHEATINGCOIL_PREFIX,noChangeMap, map);// 计算风量
    	
    	String svelocity = UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_WVELOCITY;//迎面风速
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号

        // 计算迎面风速
        double airVolume = BaseDataUtil
                .stringConversionDouble(map.get(UtilityConstant.METAHU_AIRVOLUME));;
        double svelocityDB = 0.00;
//        String airDirection = BaseDataUtil
//                .constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
//        if (UtilityConstant.SYS_ALPHABET_R_UP.equals(airDirection)) {//根据风向获取相应的风量
//            airVolume = BaseDataUtil
//                    .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME)));
//        } else if (UtilityConstant.SYS_ALPHABET_S_UP.equals(airDirection)) {
//            airVolume = BaseDataUtil
//                    .stringConversionDouble(String.valueOf(getSupplyAirVolume(CommonConstant.METASEXON_ELECTRICHEATINGCOIL_PREFIX,noChangeMap)));
//        }
        try {
            svelocityDB = FacaVelocityUtil.getOtherCommon(airVolume, noChangeMap.get(serial),
                    SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId());
            BigDecimal shaftPowerBd = new BigDecimal(svelocityDB);// 四舍五入，保留两位小数
            shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
            map.put(svelocity, String.valueOf(shaftPowerBd));
        } catch (Exception e) {
            // 计算失败则取系统迎面风速
            map.put(svelocity, noChangeMap.get(svelocity));
            e.printStackTrace();
        }

        
        String heatMedium = UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_HEATMEDIUM;// 热媒
        map.put(heatMedium, getIntlString(ELECTRIC_HEATING));
        return map;
    }

    //TODO 逻辑混乱 封装蒸汽盘管段
    public static Map<String, String> getSteamCoil(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
    	map = getAirVolume(CommonConstant.METASEXON_STEAMCOIL_PREFIX,noChangeMap, map);// 计算风量
    	
    	String svelocity = UtilityConstant.METASEXON_STEAMCOIL_WVELOCITY;//迎面风速
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号

        // 计算迎面风速
        double airVolume = BaseDataUtil
              .stringConversionDouble(map.get(UtilityConstant.METAHU_AIRVOLUME));
        double svelocityDB = 0.00;
//        String airDirection = BaseDataUtil
//                .constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
//        if (UtilityConstant.SYS_ALPHABET_R_UP.equals(airDirection)) {//根据风向获取相应的风量
//            airVolume = BaseDataUtil
//                    .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME)));
//        } else if (UtilityConstant.SYS_ALPHABET_S_UP.equals(airDirection)) {
//            airVolume = BaseDataUtil
//                    .stringConversionDouble(String.valueOf(getSupplyAirVolume(CommonConstant.METASEXON_STEAMCOIL_PREFIX,noChangeMap)));
//        }
        
        try {
            svelocityDB = FacaVelocityUtil.getOtherCommon(airVolume, noChangeMap.get(serial),
                    SectionTypeEnum.TYPE_STEAMCOIL.getId());
            BigDecimal shaftPowerBd = new BigDecimal(svelocityDB);// 四舍五入，保留两位小数
            shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
            map.put(svelocity, String.valueOf(shaftPowerBd));
        } catch (Exception e) {
            // 计算失败则取系统迎面风速
            map.put(svelocity, noChangeMap.get(svelocity));
            e.printStackTrace();
        }

        
        String heatMedium = UtilityConstant.METASEXON_STEAMCOIL_HEATMEDIUM;// 热媒
        map.put(heatMedium, getIntlString(STEAM));
        return map;
    }

    // 封装单层过滤段
    public static Map<String, String> getFilter(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_FILTER_PREFIX,noChangeMap, map);// 计算风量
        String supplier = UtilityConstant.METASEXON_FILTER_SUPPLIER;// 供应商
        String filterArrange = UtilityConstant.METASEXON_FILTER_FILTERARRANGE;// 过滤器布置
        /* 规格信息 */
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        if (map.containsKey(filterArrange)) {// 封装过滤器布置
            String value = noChangeMap.get(filterArrange).toString();
            List<FilterPanelBean> filterPanelBeanList = JSONArray.parseArray(value, FilterPanelBean.class);
            if (null != filterPanelBeanList && filterPanelBeanList.size() > 0) {
                for (int i = 0; i < filterPanelBeanList.size(); i++) {
                    FilterPanelBean filterPanelBean = filterPanelBeanList.get(i);
                    String paraKey = SectionMetaUtils.getMetaSectionKey(TYPE_SINGLE, "para" + i);
                    String paraNKey = SectionMetaUtils.getMetaSectionKey(TYPE_SINGLE, "paraN" + i);
                    if (null != filterPanelBean.getOption()) {
                        map.put(paraKey, filterPanelBean.getOption());
                    }
                    if (null != filterPanelBean.getCount()) {
                        map.put(paraNKey, filterPanelBean.getCount());
                    }
                }
            }
        }
        
        
        //filter velocity=风量/截面积
        String airDirectionStr = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
//        String airVolume;
//        if (airDirectionStr.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {
//        	airVolume = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME));
//        } else {
//        	airVolume = BaseDataUtil.constraintString(getSupplyAirVolume(noChangeMap));
//        }
        double airVolumeDB = BaseDataUtil.stringConversionDouble(map.get(UtilityConstant.METAHU_AIRVOLUME));
//        String medialoading = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FILTER_MEDIALOADING));
        SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class, noChangeMap.get(UtilityConstant.METAHU_SERIAL));
        double area = sectionArea.getBin();
        double velocity = airVolumeDB/3600/area;
        map.put("meta.section.filter.filterVelocity", BaseDataUtil.doubleToString(velocity, 2));
        
        //yearly_energy_comsumption
        String filterEfficiency = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FILTER_FILTEREFFICIENCY));
        String fitetF = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FILTER_FITETF));
        String yearlyEnergyComsumption;
        if ("G3".equals(filterEfficiency) && "P".equals(fitetF)) {
			yearlyEnergyComsumption = "6609";
		}else if ("G3".equals(filterEfficiency) && "B".equals(fitetF)) {
			yearlyEnergyComsumption = "6685";
		}else if ("G4".equals(filterEfficiency) && "B".equals(fitetF)) {
			yearlyEnergyComsumption = "6685";
		}else if ("G4".equals(filterEfficiency) && "P".equals(fitetF)) {
			yearlyEnergyComsumption = "6609";
		}else if ("M5".equals(filterEfficiency) && "B".equals(fitetF)) {
			yearlyEnergyComsumption = "1192.68";
		}else if ("M6".equals(filterEfficiency) && "B".equals(fitetF)) {
			yearlyEnergyComsumption = "2767.87";
		}else if ("F7".equals(filterEfficiency) && "B".equals(fitetF)) {
			yearlyEnergyComsumption = "2516.31";
		}else if ("F7G".equals(filterEfficiency) && "B".equals(fitetF)) {
			yearlyEnergyComsumption = "997.41";
		}else if ("F8".equals(filterEfficiency) && "B".equals(fitetF)) {
			yearlyEnergyComsumption = "2852.98";
		}else if ("F8G".equals(filterEfficiency) && "B".equals(fitetF)) {
			yearlyEnergyComsumption = "2630.41";
		}else if ("F9".equals(filterEfficiency) && "B".equals(fitetF)) {
			yearlyEnergyComsumption = "3149.57";
		}else if ("F9G".equals(filterEfficiency) && "B".equals(fitetF)) {
			yearlyEnergyComsumption = "3637.40";
		}else {
			yearlyEnergyComsumption = "0";
		}        
        map.put("meta.section.filter.yearlyEnergyComsumption", yearlyEnergyComsumption);
                
        return map;
    }

    // 封装湿膜加湿段
    public static Map<String, String> getWetfilmHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_WETFILMHUMIDIFIER_PREFIX,noChangeMap, map);// 计算风量
        String style = UtilityConstant.METASEXON_WETFILMHUMIDIFIER_STYLE;
        String waterQuality = UtilityConstant.METASEXON_WETFILMHUMIDIFIER_WATERQUALITY;
        String install = UtilityConstant.METASEXON_WETFILMHUMIDIFIER_INSTALL;// 安装段
        String previous = UtilityConstant.METASEXON_WETFILMHUMIDIFIER_PREVIOUS;// 当前段标识
        String waterQuantity = UtilityConstant.METASEXON_WETFILMHUMIDIFIER_WATERQUANTITY;// 水量

        map.put(style, getIntlString(DIRECT_WETFILM_HUMIDIFICATION));
        map.put(waterQuality, getIntlString(TAP_WATER));
        if (map.containsKey(previous)) {
            String value = noChangeMap.get(previous).toString();
            if (value.equals(SectionTypeEnum.TYPE_COLD.getId())) {
                map.put(install, getIntlString(COOLING_COIL_SECTION));
            } else if (value.equals(SectionTypeEnum.TYPE_HEATINGCOIL.getId())) {
                map.put(install, getIntlString(HOT_WATER_COIL_SECTION));
            }
        }
        if (map.containsKey(waterQuantity)) {
            Object value = noChangeMap.get(waterQuantity);
            if (!EmptyUtil.isEmpty(value)) {
                map.put(waterQuantity, String.format("%.1f", BaseDataUtil.stringConversionDouble(BaseDataUtil.objectToString(value))));
            }
        }
        return map;
    }

    // 封装风机报告名称数据
    public static String getFanName(Map<String, String> noChangeMap, Map<String, String> map, String fanName, LanguageEnum language) {
        fanName = fanName.substring(0, fanName.indexOf(UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE) + 1) + getIntlString(SUPPLY_FAN);// 默认送风机
        String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
        if (map.containsKey(airDirection)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(airDirection));
            if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {
                fanName = fanName.substring(0, fanName.indexOf(UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE) + 1) + getIntlString(RETURN_FAN);
            }
        }
        return fanName;
    }

    private static double getLw(String serial, Map<String, String> map) {
        String engineData1 = UtilityConstant.METASEXON_FAN_ENGINEDATA1;
        String engineData2 = UtilityConstant.METASEXON_FAN_ENGINEDATA2;
        String engineData3 = UtilityConstant.METASEXON_FAN_ENGINEDATA3;
        String engineData4 = UtilityConstant.METASEXON_FAN_ENGINEDATA4;
        String engineData5 = UtilityConstant.METASEXON_FAN_ENGINEDATA5;
        String engineData6 = UtilityConstant.METASEXON_FAN_ENGINEDATA6;
        String engineData7 = UtilityConstant.METASEXON_FAN_ENGINEDATA7;
        String engineData8 = UtilityConstant.METASEXON_FAN_ENGINEDATA8;
        double vt = 0.00;
        double va = 0.00;
        double vo = 0.00;
        va = BaseDataUtil.stringConversionDouble(map.get(engineData1)) - getSForFranceValueByItem(serial,"D1");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData2)) - getSForFranceValueByItem(serial,"D2");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData3)) - getSForFranceValueByItem(serial,"D3");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData4)) - getSForFranceValueByItem(serial,"D4");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData5)) - getSForFranceValueByItem(serial,"D5");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData6)) - getSForFranceValueByItem(serial,"D6");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData7)) - getSForFranceValueByItem(serial,"D7");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData8)) - getSForFranceValueByItem(serial,"D8");
        vo = 0.1 * va;
        vt = vt + Math.pow(10, vo);
        return 10 * Math.log10(vt);
    }

    private static double getSForFranceValueByItem(String serial,String item) {
        String product = serial.substring(0, serial.length() - 4);

        SForFrance sforFrance = AhuMetadata.findOne(SForFrance.class, product,item);
        if (EmptyUtil.isNotEmpty(sforFrance)) {
            return sforFrance.getValue();
        }
        return 0;
    }

    private static double getLp(String serial, Map<String, String> map) {
        String engineData1 = UtilityConstant.METASEXON_FAN_ENGINEDATA1;
        String engineData2 = UtilityConstant.METASEXON_FAN_ENGINEDATA2;
        String engineData3 = UtilityConstant.METASEXON_FAN_ENGINEDATA3;
        String engineData4 = UtilityConstant.METASEXON_FAN_ENGINEDATA4;
        String engineData5 = UtilityConstant.METASEXON_FAN_ENGINEDATA5;
        String engineData6 = UtilityConstant.METASEXON_FAN_ENGINEDATA6;
        String engineData7 = UtilityConstant.METASEXON_FAN_ENGINEDATA7;
        String engineData8 = UtilityConstant.METASEXON_FAN_ENGINEDATA8;
        double vt = 0.00;
        double va = 0.00;
        double vo = 0.00;
        va = BaseDataUtil.stringConversionDouble(map.get(engineData1)) - getSForFranceValueByItem(serial,"In1");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData2)) - getSForFranceValueByItem(serial,"In2");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData3)) - getSForFranceValueByItem(serial,"In3");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData4)) - getSForFranceValueByItem(serial,"In4");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData5)) - getSForFranceValueByItem(serial,"In5");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData6)) - getSForFranceValueByItem(serial,"In6");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData7)) - getSForFranceValueByItem(serial,"In7");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        va = BaseDataUtil.stringConversionDouble(map.get(engineData8)) - getSForFranceValueByItem(serial,"In8");
        vo = 0.1 * BaseDataUtil.decimalConvert(va, 0);
        vt = vt + Math.pow(10, vo);
        return 10 * Math.log10(vt);
    }

    // 封装风机报告数据
    public static Map<String, String> getFan(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language, AhuParam ahuParam) {
        map = getAirVolume(CommonConstant.METASEXON_FAN_PREFIX,noChangeMap, map);// 计算风量
        String doorOnBothSide = UtilityConstant.METASEXON_FAN_DOORONBOTHSIDE;//两侧开门
        String fixRepairLamp = UtilityConstant.METASEXON_FAN_FIXREPAIRLAMP;//安装检修灯
        String para13 = UtilityConstant.METASEXON_FAN_PARA13;//耐盐雾型减震器
        String ptc = UtilityConstant.METASEXON_FAN_PTC;//ptc
        String beltGuard = UtilityConstant.METASEXON_FAN_BELTGUARD;// 皮带保护罩
        String seismicPringIsolator = UtilityConstant.METASEXON_FAN_SEISMICPRINGISOLATOR;// 防剪切减震器
        String insulation = UtilityConstant.METASEXON_FAN_INSULATION;// 绝缘等级
        String Protection = UtilityConstant.METASEXON_FAN_PROTECTION;// 防护等级
        String standbyMotor = UtilityConstant.METASEXON_FAN_STANDBYMOTOR;// 备用电机
        String motorBrand = UtilityConstant.METASEXON_FAN_MOTORBRAND;//	电机品牌
        String type = UtilityConstant.METASEXON_FAN_TYPE;// 电机类型
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String unitValue=noChangeMap.get(serial);
        String fanModel = UtilityConstant.METASEXON_FAN_FANMODEL;// 风机型号
        String motorBaseNo = UtilityConstant.METASEXON_FAN_MOTORBASENO;// 机座号
        String outlet = UtilityConstant.METASEXON_FAN_OUTLET;//风机形式
        String outletDirection = UtilityConstant.METASEXON_FAN_OUTLETDIRECTION;//出风方向
        String supplyDamper = UtilityConstant.METASEXON_FAN_SUPPLYDAMPER;//送风口型式
        String returnPosition = UtilityConstant.METASEXON_FAN_RETURNPOSITION;//送风出口位置
        String engineType = UtilityConstant.META_SECTION_FAN_ENGINETYPE;//引擎类型
        String shaftPower = UtilityConstant.METASEXON_FAN_SHAFTPOWER;//轴功率
        String inputPower = UtilityConstant.METASEXON_FAN_INPUTPOWER;//输入功率
        String theFanSupplierKey = UtilityConstant.METASEXON_FAN_FANSUPPLIER;//风机供应商

//        String fanType = fanInParam.getSerial();//机组型号
//        String supplier = fanInParam.getSupplier();//电机品牌
//        int fanHeight = SystemCountUtil.getUnitHeight(fanType);// 风机高度
//        String motorBaseNo = fanInfo.getMotorBaseNo();//机座号
//        String code = BaseDataUtil.StringConversionNumber(fanInfo.getFanModel());
//        String startStyle = fanInParam.getStartStyle();
//        String fanTypeCode = "39CQ" + SystemCountUtil.getUnit(fanType);
        
        //TODO 硬编码，修改可以正常侧置时，添加了侧置数据BUG。后期版本请删除 BEGIN
        try {
        	String startStyle=UtilityConstant.METASEXON_FAN_STARTSTYLE;
        	String sidePosition=UtilityConstant.METASEXON_FAN_P;
        	String motorsupplier=UtilityConstant.METASEXON_FAN_SUPPLIER;
        	String motorPosition=UtilityConstant.METASEXON_FAN_MOTORPOSITION;
        	
        	double sidePositionDouble=0.0;
        	if(map.containsKey(sidePosition)) {
        		String sidePString=map.get(sidePosition);
        		if(EmptyUtil.isNotEmpty(sidePString)) {
        			sidePositionDouble=BaseDataUtil.stringConversionDouble(sidePString);
        		}
        	}
        	
        	String motorPositionVal=map.get(motorPosition);

        	//如果有偏移并且是侧置时，校验是否需要偏移
        	if(sidePositionDouble>0 && motorPositionVal.equalsIgnoreCase(FAN_MOTORPOSITION_SIDE)) {
        	String motorValue=noChangeMap.get(motorsupplier);
        	String motorTypeValue=noChangeMap.get(type);
        	String motorBaseNoValue=noChangeMap.get(motorBaseNo);
        	String fanModelValue=noChangeMap.get(fanModel);
        	String startStyleValue=noChangeMap.get(startStyle);
        	String calUnit="39CQ" + SystemCountUtil.getUnit(unitValue);
        	String fanCodeValue=BaseDataUtil.StringConversionNumber(fanModelValue);
                String unit = SystemCountUtil.getUnit(unitValue);
                if (SystemCountUtil.ltBigUnit(BaseDataUtil.stringConversionInteger(unit))) {
                	FanCodeElectricSize fanCodeElectricSize = MotorPositionUtil.getSmallFanSize(fanCodeValue, calUnit);
                    double inWidth = MotorPositionUtil.getSmallMotorSize(motorTypeValue, fanModelValue, motorValue, motorBaseNoValue, startStyleValue,
    						fanCodeElectricSize);
                    Integer INNERWIDTH=fanCodeElectricSize.getINNERWIDTH();
                    
                    if (inWidth <= INNERWIDTH) {
                    	map.put(sidePosition, "0");
                    }
                     
                	
                }else {
                	
                	FanCodeElectricSizeBig fanCodeElectricSizeBig = MotorPositionUtil.getBigFanSIze(fanCodeValue, calUnit);
                	double P=fanCodeElectricSizeBig.getP();
                    double inWidth = MotorPositionUtil.getBigUnitMotorSize(motorTypeValue, fanModelValue, motorValue, motorBaseNoValue, fanCodeValue, startStyleValue,
                    		calUnit,fanCodeElectricSizeBig);
                    
                    if (inWidth >= 100) {
                    	map.put(sidePosition, "0");
                    }
                	
                }
                
        	}
        	//风机段如选择电机后置，偏移距离为0·
        	if(sidePositionDouble>0 && motorPositionVal.equalsIgnoreCase(FAN_MOTORPOSITION_BACK)){
                map.put(sidePosition, "0");
            }
        	
        }catch(Exception e) {
        	
        }
        
       //TODO 硬编码，修改可以正常侧置时，添加了侧置数据BUG。后期版本请删除 BEGIN
        

        String lw = UtilityConstant.METASEXON_FAN_LW;// A声功率级
        String lp = UtilityConstant.METASEXON_FAN_LP;// A声声压级
        //map.put(lp, BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(getLp(unitValue,noChangeMap), 1)));
        //map.put(lw, BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(getLw(unitValue,noChangeMap), 1)));
        boolean isGK = false;// 是否使用了谷科风机引擎
        if (map.containsKey(engineType)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(engineType));
            if (FanEnginesEnum.YLD.getCode().equals(value)) {
                map.put(engineType, FanEnginesEnum.YLD.getEnName());
            } else if (FanEnginesEnum.KRUGER.getCode().equals(value)) {
            	map.put(engineType, FanEnginesEnum.KRUGER.getEnName());
			}else {
                map.put(engineType, FanEnginesEnum.GK.getEnName());
                isGK = true;
            }
        }
        if (map.containsKey(doorOnBothSide)) {// 两侧开门
            String value = BaseDataUtil.constraintString(noChangeMap.get(doorOnBothSide));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(doorOnBothSide, getIntlString(YES));
            } else {
                map.put(doorOnBothSide, getIntlString(NO));
            }
        }
        if (map.containsKey(fixRepairLamp)) {// 安装检修灯
            String value = BaseDataUtil.constraintString(noChangeMap.get(fixRepairLamp));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(fixRepairLamp, getIntlString(YES));
            } else {
                map.put(fixRepairLamp, getIntlString(NO));
            }
        }
        if (map.containsKey(para13)) {// 耐盐雾型减震器
            String value = BaseDataUtil.constraintString(noChangeMap.get(para13));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(para13, getIntlString(YES));
            } else {
                map.put(para13, getIntlString(NO));
            }
        }
        if (map.containsKey(ptc)) {// ptc
            String value = BaseDataUtil.constraintString(noChangeMap.get(ptc));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE) && !AHUContext.isExportVersion()) {
                map.put(ptc, getIntlString(YES));
            } else {
                map.put(ptc, getIntlString(NO));
            }
        }
        if (map.containsKey(beltGuard)) {// 皮带保护罩
            String value = BaseDataUtil.constraintString(noChangeMap.get(beltGuard));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(beltGuard, getIntlString(YES));
            } else {
                map.put(beltGuard, getIntlString(NO));
            }
        }
        /*增加描述*/
        String fan_remark = UtilityConstant.SYS_MAP_FAN_REMARK;
        String fan_remark_one = UtilityConstant.SYS_MAP_FAN_REMARK_ONE;
        String fan_remark_two = UtilityConstant.SYS_MAP_FAN_REMARK_TWO;
        String fan_type_remark = UtilityConstant.SYS_MAP_FAN_TYPE_REMARK;
        map.put(fan_remark, getIntlString(FAN_REMARK));
        map.put(fan_remark_one, getIntlString(FAN_REMARK_ONE));
        map.put(fan_remark_two, getIntlString(FAN_REMARK_TWO));
        if (map.containsKey(type)) {
            Object value = (Object) noChangeMap.get(type);
            value = BaseDataUtil.objectToString(value);
            if (SystemCalculateConstants.FAN_TYPE_5.equals(value) || SystemCalculateConstants.FAN_TYPE_6.equals(value)) {
                map.put(fan_type_remark, getIntlString(FAN_TYPE_REMARK));
            } else {
                map.put(fan_type_remark, UtilityConstant.SYS_BLANK);
            }
        }

        String engineData1 = UtilityConstant.METASEXON_FAN_ENGINEDATA1;
        String engineData2 = UtilityConstant.METASEXON_FAN_ENGINEDATA2;
        String engineData3 = UtilityConstant.METASEXON_FAN_ENGINEDATA3;
        String engineData4 = UtilityConstant.METASEXON_FAN_ENGINEDATA4;
        String engineData5 = UtilityConstant.METASEXON_FAN_ENGINEDATA5;
        String engineData6 = UtilityConstant.METASEXON_FAN_ENGINEDATA6;
        String engineData7 = UtilityConstant.METASEXON_FAN_ENGINEDATA7;
        String engineData8 = UtilityConstant.METASEXON_FAN_ENGINEDATA8;

        /*------------------------------噪声 start---------------------*/
        // 出风口声功率级
        Double engineDataVal1 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData1));
        Double engineDataVal2 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData2));
        Double engineDataVal3 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData3));
        Double engineDataVal4 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData4));
        Double engineDataVal5 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData5));
        Double engineDataVal6 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData6));
        Double engineDataVal7 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData7));
        Double engineDataVal8 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData8));
        //默认风机品牌:谷科，噪声引擎原始结果数据+Acoff - yexinjun 2020/12/1
        if(isGK){
            engineDataVal1 += getSForFranceValueByItem(unitValue,"Ai1");
            engineDataVal2 += getSForFranceValueByItem(unitValue,"Ai2");
            engineDataVal3 += getSForFranceValueByItem(unitValue,"Ai3");
            engineDataVal4 += getSForFranceValueByItem(unitValue,"Ai4");
            engineDataVal5 += getSForFranceValueByItem(unitValue,"Ai5");
            engineDataVal6 += getSForFranceValueByItem(unitValue,"Ai6");
            engineDataVal7 += getSForFranceValueByItem(unitValue,"Ai7");
            engineDataVal8 += getSForFranceValueByItem(unitValue,"Ai8");
        }

        double lw1 = BaseDataUtil.decimalConvert(engineDataVal1 - getSForFranceValueByItem(unitValue,"D1") , 0);
        double lw2 = BaseDataUtil.decimalConvert(engineDataVal2 - getSForFranceValueByItem(unitValue,"D2") , 0);
        double lw3 = BaseDataUtil.decimalConvert(engineDataVal3 - getSForFranceValueByItem(unitValue,"D3") , 0);
        double lw4 = BaseDataUtil.decimalConvert(engineDataVal4 - getSForFranceValueByItem(unitValue,"D4") , 0);
        double lw5 = BaseDataUtil.decimalConvert(engineDataVal5 - getSForFranceValueByItem(unitValue,"D5") , 0);
        double lw6 = BaseDataUtil.decimalConvert(engineDataVal6 - getSForFranceValueByItem(unitValue,"D6") , 0);
        double lw7 = BaseDataUtil.decimalConvert(engineDataVal7 - getSForFranceValueByItem(unitValue,"D7") , 0);
        double lw8 = BaseDataUtil.decimalConvert(engineDataVal8 - getSForFranceValueByItem(unitValue,"D8") , 0);
        map.put(UtilityConstant.METASEXON_FAN_LW1, BaseDataUtil.doubleConversionIntString(lw1));
        map.put(UtilityConstant.METASEXON_FAN_LW2, BaseDataUtil.doubleConversionIntString(lw2));
        map.put(UtilityConstant.METASEXON_FAN_LW3, BaseDataUtil.doubleConversionIntString(lw3));
        map.put(UtilityConstant.METASEXON_FAN_LW4, BaseDataUtil.doubleConversionIntString(lw4));
        map.put(UtilityConstant.METASEXON_FAN_LW5, BaseDataUtil.doubleConversionIntString(lw5));
        map.put(UtilityConstant.METASEXON_FAN_LW6, BaseDataUtil.doubleConversionIntString(lw6));
        map.put(UtilityConstant.METASEXON_FAN_LW7, BaseDataUtil.doubleConversionIntString(lw7));
        map.put(UtilityConstant.METASEXON_FAN_LW8, BaseDataUtil.doubleConversionIntString(lw8));
        double doublelw =
                10*Math.log10((
                        Math.pow(10,lw1/10)+Math.pow(10,lw2/10)+
                                Math.pow(10,lw3/10)+Math.pow(10,lw4/10)+
                                Math.pow(10,lw5/10)+Math.pow(10,lw6/10)+
                                Math.pow(10,lw7/10)+Math.pow(10,lw8/10))
                );//加权八分
        map.put(lw, BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(doublelw, 1)));

        // 进风口声功率级
        double lp1 = BaseDataUtil.decimalConvert(engineDataVal1 - getSForFranceValueByItem(unitValue,"In1") , 0);
        double lp2 = BaseDataUtil.decimalConvert(engineDataVal2 - getSForFranceValueByItem(unitValue,"In2") , 0);
        double lp3 = BaseDataUtil.decimalConvert(engineDataVal3 - getSForFranceValueByItem(unitValue,"In3") , 0);
        double lp4 = BaseDataUtil.decimalConvert(engineDataVal4 - getSForFranceValueByItem(unitValue,"In4") , 0);
        double lp5 = BaseDataUtil.decimalConvert(engineDataVal5 - getSForFranceValueByItem(unitValue,"In5") , 0);
        double lp6 = BaseDataUtil.decimalConvert(engineDataVal6 - getSForFranceValueByItem(unitValue,"In6") , 0);
        double lp7 = BaseDataUtil.decimalConvert(engineDataVal7 - getSForFranceValueByItem(unitValue,"In7") , 0);
        double lp8 = BaseDataUtil.decimalConvert(engineDataVal8 - getSForFranceValueByItem(unitValue,"In8") , 0);
        map.put(UtilityConstant.METASEXON_FAN_LP1, BaseDataUtil.doubleConversionIntString(lp1));
        map.put(UtilityConstant.METASEXON_FAN_LP2, BaseDataUtil.doubleConversionIntString(lp2));
        map.put(UtilityConstant.METASEXON_FAN_LP3, BaseDataUtil.doubleConversionIntString(lp3));
        map.put(UtilityConstant.METASEXON_FAN_LP4, BaseDataUtil.doubleConversionIntString(lp4));
        map.put(UtilityConstant.METASEXON_FAN_LP5, BaseDataUtil.doubleConversionIntString(lp5));
        map.put(UtilityConstant.METASEXON_FAN_LP6, BaseDataUtil.doubleConversionIntString(lp6));
        map.put(UtilityConstant.METASEXON_FAN_LP7, BaseDataUtil.doubleConversionIntString(lp7));
        map.put(UtilityConstant.METASEXON_FAN_LP8, BaseDataUtil.doubleConversionIntString(lp8));
        double doublelp = 10*Math.log10((
                Math.pow(10,lp1/10)+Math.pow(10,lp2/10)+
                        Math.pow(10,lp3/10)+Math.pow(10,lp4/10)+
                        Math.pow(10,lp5/10)+Math.pow(10,lp6/10)+
                        Math.pow(10,lp7/10)+Math.pow(10,lp8/10))
        );//加权八分
        map.put(lp, BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(doublelp, 1)));

        //箱体的插入损失 = AB+10*LOG((1+0.04)/(1+0.04*10^(0.1*AB)))
        double AirboneCsvData1 = getSForFranceValueByItem(unitValue,"W1");
        double AirboneCsvData2 = getSForFranceValueByItem(unitValue,"W2");
        double AirboneCsvData3 = getSForFranceValueByItem(unitValue,"W3");
        double AirboneCsvData4 = getSForFranceValueByItem(unitValue,"W4");
        double AirboneCsvData5 = getSForFranceValueByItem(unitValue,"W5");
        double AirboneCsvData6 = getSForFranceValueByItem(unitValue,"W6");
        double AirboneCsvData7 = getSForFranceValueByItem(unitValue,"W7");
        double AirboneCsvData8 = getSForFranceValueByItem(unitValue,"W8");
        double XTCRSS1 = AirboneCsvData1+10*Math.log10((1+0.04)/(1+0.04*Math.pow(10,(0.1*AirboneCsvData1))));
        double XTCRSS2 = AirboneCsvData2+10*Math.log10((1+0.04)/(1+0.04*Math.pow(10,(0.1*AirboneCsvData2))));
        double XTCRSS3 = AirboneCsvData3+10*Math.log10((1+0.04)/(1+0.04*Math.pow(10,(0.1*AirboneCsvData3))));
        double XTCRSS4 = AirboneCsvData4+10*Math.log10((1+0.04)/(1+0.04*Math.pow(10,(0.1*AirboneCsvData4))));
        double XTCRSS5 = AirboneCsvData5+10*Math.log10((1+0.04)/(1+0.04*Math.pow(10,(0.1*AirboneCsvData5))));
        double XTCRSS6 = AirboneCsvData6+10*Math.log10((1+0.04)/(1+0.04*Math.pow(10,(0.1*AirboneCsvData6))));
        double XTCRSS7 = AirboneCsvData7+10*Math.log10((1+0.04)/(1+0.04*Math.pow(10,(0.1*AirboneCsvData7))));
        double XTCRSS8 = AirboneCsvData8+10*Math.log10((1+0.04)/(1+0.04*Math.pow(10,(0.1*AirboneCsvData8))));

        // 计算Airbone值 声功率级Lw(A) = 软件计算出声功率级-箱体的插入损失
        double AB1 = BaseDataUtil.decimalConvert((engineDataVal1 - XTCRSS1), 0);
        double AB2 = BaseDataUtil.decimalConvert((engineDataVal2 - XTCRSS2), 0);
        double AB3 = BaseDataUtil.decimalConvert((engineDataVal3 - XTCRSS3), 0);
        double AB4 = BaseDataUtil.decimalConvert((engineDataVal4 - XTCRSS4), 0);
        double AB5 = BaseDataUtil.decimalConvert((engineDataVal5 - XTCRSS5), 0);
        double AB6 = BaseDataUtil.decimalConvert((engineDataVal6 - XTCRSS6), 0);
        double AB7 = BaseDataUtil.decimalConvert((engineDataVal7 - XTCRSS7), 0);
        double AB8 = BaseDataUtil.decimalConvert((engineDataVal8 - XTCRSS8), 0);
        map.put(UtilityConstant.METASEXON_FAN_AB1, BaseDataUtil.doubleConversionIntString(AB1));
        map.put(UtilityConstant.METASEXON_FAN_AB2, BaseDataUtil.doubleConversionIntString(AB2));
        map.put(UtilityConstant.METASEXON_FAN_AB3, BaseDataUtil.doubleConversionIntString(AB3));
        map.put(UtilityConstant.METASEXON_FAN_AB4, BaseDataUtil.doubleConversionIntString(AB4));
        map.put(UtilityConstant.METASEXON_FAN_AB5, BaseDataUtil.doubleConversionIntString(AB5));
        map.put(UtilityConstant.METASEXON_FAN_AB6, BaseDataUtil.doubleConversionIntString(AB6));
        map.put(UtilityConstant.METASEXON_FAN_AB7, BaseDataUtil.doubleConversionIntString(AB7));
        map.put(UtilityConstant.METASEXON_FAN_AB8, BaseDataUtil.doubleConversionIntString(AB8));

        double ab = 10*Math.log10((
                Math.pow(10,AB1/10)+Math.pow(10,AB2/10)+
                        Math.pow(10,AB3/10)+Math.pow(10,AB4/10)+
                        Math.pow(10,AB5/10)+Math.pow(10,AB6/10)+
                        Math.pow(10,AB7/10)+Math.pow(10,AB8/10))
        );//加权八分
        map.put(UtilityConstant.METASEXON_FAN_AB, BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(ab, 1)));


        //计算声压级差值
        double syjcz = getlogSB(unitValue,noChangeMap,ahuParam);
        //风机噪音修正系数 5
        int FANZSXZXS = 5;
        if (BaseDataUtil.stringConversionInteger(SystemCountUtil.getUnit(unitValue))<913) {//小于0913修正系数0，不修正
            FANZSXZXS = 0;
        }
        //Airborne 声压级Lp(A) = OUTLET+风机噪音修正系数-计算声压级差值-箱体的插入损失
        String fanSeriesVal = BaseDataUtil.constraintString(noChangeMap.get(META_SECTION_FAN_SERIES));
        if (noChangeMap.get(fanModel).contains(UtilityConstant.SYS_MAP_FAN_FANMODEL_FC)) {//前弯://亿利达前倾风机原始结果加对应值，用于计算Airborne 声压级Lp(A)，其他算法不变
            String value = BaseDataUtil.constraintString(noChangeMap.get(META_SECTION_FAN_ENGINETYPE));
            if(FanEnginesEnum.YLD.getCode().equals(value) && EmptyUtil.isNotEmpty(noChangeMap.get(META_SECTION_FAN_SERIES))){
                try {
                    engineDataVal1 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData1))+isFCAppend(fanSeriesVal);
                    engineDataVal2 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData2))+isFCAppend(fanSeriesVal);
                    engineDataVal3 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData3))+isFCAppend(fanSeriesVal);
                    engineDataVal4 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData4))+isFCAppend(fanSeriesVal);
                    engineDataVal5 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData5))+isFCAppend(fanSeriesVal);
                    engineDataVal6 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData6))+isFCAppend(fanSeriesVal);
                    engineDataVal7 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData7))+isFCAppend(fanSeriesVal);
                    engineDataVal8 = BaseDataUtil.stringConversionDouble(noChangeMap.get(engineData8))+isFCAppend(fanSeriesVal);
                } catch (Exception e) {
                    e.printStackTrace();//忽略异常的风机型号
                }
            }
        }
        double AI1 = BaseDataUtil.decimalConvert((engineDataVal1 - getSForFranceValueByItem(unitValue,"Ai1")+ FANZSXZXS -syjcz - getXtcrssByGK1120(XTCRSS1,isGK,fanSeriesVal)), 0);
        double AI2 = BaseDataUtil.decimalConvert((engineDataVal2 - getSForFranceValueByItem(unitValue,"Ai2")+ FANZSXZXS -syjcz - getXtcrssByGK1120(XTCRSS2,isGK,fanSeriesVal)), 0);
        double AI3 = BaseDataUtil.decimalConvert((engineDataVal3 - getSForFranceValueByItem(unitValue,"Ai3")+ FANZSXZXS -syjcz - getXtcrssByGK1120(XTCRSS3,isGK,fanSeriesVal)), 0);
        double AI4 = BaseDataUtil.decimalConvert((engineDataVal4 - getSForFranceValueByItem(unitValue,"Ai4")+ FANZSXZXS -syjcz - getXtcrssByGK1120(XTCRSS4,isGK,fanSeriesVal)), 0);
        double AI5 = BaseDataUtil.decimalConvert((engineDataVal5 - getSForFranceValueByItem(unitValue,"Ai5")+ FANZSXZXS -syjcz - getXtcrssByGK1120(XTCRSS5,isGK,fanSeriesVal)), 0);
        double AI6 = BaseDataUtil.decimalConvert((engineDataVal6 - getSForFranceValueByItem(unitValue,"Ai6")+ FANZSXZXS -syjcz - getXtcrssByGK1120(XTCRSS6,isGK,fanSeriesVal)), 0);
        double AI7 = BaseDataUtil.decimalConvert((engineDataVal7 - getSForFranceValueByItem(unitValue,"Ai7")+ FANZSXZXS -syjcz - getXtcrssByGK1120(XTCRSS7,isGK,fanSeriesVal)), 0);
        double AI8 = BaseDataUtil.decimalConvert((engineDataVal8 - getSForFranceValueByItem(unitValue,"Ai8")+ FANZSXZXS -syjcz - getXtcrssByGK1120(XTCRSS8,isGK,fanSeriesVal)), 0);
        double ai = 10*Math.log10((
                        Math.pow(10,AI1/10)+Math.pow(10,AI2/10)+
                        Math.pow(10,AI3/10)+Math.pow(10,AI4/10)+
                        Math.pow(10,AI5/10)+Math.pow(10,AI6/10)+
                        Math.pow(10,AI7/10)+Math.pow(10,AI8/10))
        );
        //加权八分
        map.put(UtilityConstant.METASEXON_FAN_AI, BaseDataUtil.doubleConversionString(BaseDataUtil.decimalConvert(ai, 1)));


        /*------------------------------噪声 end---------------------*/


        String positionA = UtilityConstant.METASEXON_FAN_POSITIONA;// 减震器A
        String positionB = UtilityConstant.METASEXON_FAN_POSITIONB;// 减震器B
        String positionC = UtilityConstant.METASEXON_FAN_POSITIONC;// 减震器C
        String positionD = UtilityConstant.METASEXON_FAN_POSITIOND;// 减震器D
        String positionE = UtilityConstant.METASEXON_FAN_POSITIONE;// 减震器E
        String positionF = UtilityConstant.METASEXON_FAN_POSITIONF;// 减震器F
        String positionG = UtilityConstant.METASEXON_FAN_POSITIONG;// 减震器G
        String positionH = UtilityConstant.METASEXON_FAN_POSITIONH;// 减震器H
        String positionI = UtilityConstant.METASEXON_FAN_POSITIONI;// 减震器I
        String positionJ = UtilityConstant.METASEXON_FAN_POSITIONJ;// 减震器J

        String fan = UtilityConstant.SYS_BLANK;
        String machineSiteNo = UtilityConstant.SYS_BLANK;
        if (map.containsKey(fanModel)) {
            fan = noChangeMap.get(fanModel);
            if (map.containsKey(serial)) {
                Object value = noChangeMap.get(serial);
                String unit = SystemCountUtil.getUnit(String.valueOf(value));
                if (SystemCountUtil.ltBigUnit(BaseDataUtil.stringConversionInteger(unit))) {
                    if (!fan.contains(UtilityConstant.SYS_MAP_FAN_FANMODEL_SYW)) {
                        if (fan.contains(UtilityConstant.SYS_MAP_FAN_FANMODEL_FC)) {//前弯
                            fan = getSeriesByFanModel(fan) + "KTQ";
                        } else {//后弯
                            fan = getSeriesByFanModel(fan) + "KTH";
                        }
                    }
                } else {
                    fan = "KHF" + getSeriesByFanModel(fan);
                }
            }
        }

        if (map.containsKey(motorBaseNo)) {
            String valueBaseNo = noChangeMap.get(motorBaseNo);
            if (map.containsKey(serial)) {
                Object value = noChangeMap.get(serial);
                String unit = SystemCountUtil.getUnit(String.valueOf(value));
                if (SystemCountUtil.ltBigUnit(BaseDataUtil.stringConversionInteger(unit))) {
                    List<SKFanMotor> skFanMotorList = AhuMetadata.findAll(SKFanMotor.class);
                    if (!EmptyUtil.isEmpty(skFanMotorList)) {
                        for (SKFanMotor skFanMotor : skFanMotorList) {
                            if (skFanMotor.getMachineSiteNo().equals(valueBaseNo)) {
                                machineSiteNo = skFanMotor.getMachineSiteNo1();
                                break;
                            }
                        }
                    }
                } else {
                    machineSiteNo = valueBaseNo;
                }
            }
        }
        boolean fjq = false;
        if (map.containsKey(seismicPringIsolator)) {// 防剪切减震器
            String value = BaseDataUtil.constraintString(noChangeMap.get(seismicPringIsolator));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(seismicPringIsolator, getIntlString(YES));
                fjq = true;
            } else {
                map.put(seismicPringIsolator, getIntlString(NO));
            }
        }
        if (map.containsKey(serial)) {
            Object value = noChangeMap.get(serial);
            String unit = SystemCountUtil.getUnit(String.valueOf(value));
            if (SystemCountUtil.ltBigUnit(BaseDataUtil.stringConversionInteger(unit))) {
                SKAbsorber skAbsorber = AhuMetadata.findOne(SKAbsorber.class, fan, machineSiteNo);
                if (null != skAbsorber) {
                    String theA = reBuildJTT(fjq,skAbsorber.getA());
                    String theB = reBuildJTT(fjq,skAbsorber.getB());
                    String theC = reBuildJTT(fjq,skAbsorber.getC());
                    String theD = reBuildJTT(fjq,skAbsorber.getD());
                    String theE = reBuildJTT(fjq,skAbsorber.getE());
                    String theF = reBuildJTT(fjq,skAbsorber.getF());
                    String theG = reBuildJTT(fjq,skAbsorber.getG());
                    String theH = reBuildJTT(fjq,skAbsorber.getH());

                    map.put(positionA, theA);
                    map.put(positionB, theB);
                    map.put(positionC, theC);
                    map.put(positionD, theD);
                    map.put(positionE, theE);
                    map.put(positionF, theF);
                    map.put(positionG, theG);
                    map.put(positionH, theH);
                    map.put(positionI, "");
                    map.put(positionJ, "");
                    if (map.containsKey(para13)) {// 耐盐雾型减震器
                        String valuePara13 = BaseDataUtil.constraintString(noChangeMap.get(para13));
                        if (valuePara13.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                            if (!EmptyUtil.isEmpty(skAbsorber.getA())) {
                                map.put(positionA, theA + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorber.getB())) {
                                map.put(positionB, theB + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorber.getC())) {
                                map.put(positionC, theC + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorber.getD())) {
                                map.put(positionD, theD + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorber.getE())) {
                                map.put(positionE, theE + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorber.getF())) {
                                map.put(positionF, theF + SYS_STRING_S500);
                            }
                            
                            if (!EmptyUtil.isEmpty(skAbsorber.getG())) {
                                map.put(positionG, theG + SYS_STRING_S500);
                            }
                            
                            if (!EmptyUtil.isEmpty(skAbsorber.getH())) {
                                map.put(positionH, theH + SYS_STRING_S500);
                            }
                        }
                    }
                }
            } else {
                SKAbsorberBig skAbsorberBig = AhuMetadata.findOne(SKAbsorberBig.class, fan, machineSiteNo);
                if (null != skAbsorberBig) {
                    map.put(positionA, skAbsorberBig.getA());
                    map.put(positionB, skAbsorberBig.getB());
                    map.put(positionC, skAbsorberBig.getC());
                    map.put(positionD, skAbsorberBig.getD());
                    map.put(positionE, skAbsorberBig.getE());
                    map.put(positionF, skAbsorberBig.getF());
                    map.put(positionG, skAbsorberBig.getG());
                    map.put(positionH, skAbsorberBig.getH());
                    map.put(positionI, skAbsorberBig.getI());
                    map.put(positionJ, skAbsorberBig.getJ());
                    if (map.containsKey(para13)) {// 耐盐雾型减震器
                        String valuePara13 = BaseDataUtil.constraintString(noChangeMap.get(para13));
                        if (valuePara13.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getA())) {
                                map.put(positionA, skAbsorberBig.getA() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getB())) {
                                map.put(positionB, skAbsorberBig.getB() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getC())) {
                                map.put(positionC, skAbsorberBig.getC() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getD())) {
                                map.put(positionD, skAbsorberBig.getD() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getE())) {
                                map.put(positionE, skAbsorberBig.getE() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getF())) {
                                map.put(positionF, skAbsorberBig.getF() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getG())) {
                                map.put(positionG, skAbsorberBig.getG() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getH())) {
                                map.put(positionH, skAbsorberBig.getH() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getI())) {
                                map.put(positionI, skAbsorberBig.getI() + SYS_STRING_S500);
                            }
                            if (!EmptyUtil.isEmpty(skAbsorberBig.getJ())) {
                                map.put(positionJ, skAbsorberBig.getJ() + SYS_STRING_S500);
                            }
                        }
                    }
                }
            }
        }
        map.put(motorBrand, getIntlString(STANDARD_SUPPLIER_MOTOR));
        if (map.containsKey(insulation)) {
            String value = String.valueOf(noChangeMap.get(insulation));
            map.put(insulation, value.substring(0, 1));
            map.put(Protection, value.substring(value.length() - 4, value.length()));
        }
        if (map.containsKey(standbyMotor)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(standbyMotor));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(standbyMotor, getIntlString(YES));
            } else {
                map.put(standbyMotor, getIntlString(NO));
            }
        }
        if (map.containsKey(outletDirection)) {
            String str1 = UtilityConstant.SYS_BLANK;
            String str2 = UtilityConstant.SYS_BLANK;
            if (UtilityConstant.JSON_FAN_OUTLET_WWK.equals(noChangeMap.get(outlet))) {
                if (UtilityConstant.SYS_MAP_NO.equals(noChangeMap.get(returnPosition))) {
                    str1 = getIntlString(NONE);
                } else if (FAN_SENDPOSITION_TOP.equals(noChangeMap.get(returnPosition))) {
                    str1 = getIntlString(TOP_FIELD);
                } else if (FAN_SENDPOSITION_FACE.equals(noChangeMap.get(returnPosition))) {
                    str1 = getIntlString(FACE_FIELD);
                }
                if (UtilityConstant.SYS_MAP_NO.equals(noChangeMap.get(supplyDamper))) {
                    str2 = getIntlString(NONE);
                } else if (FAN_SUPPLYDAMPER_FL.equals(noChangeMap.get(supplyDamper))) {
                    str2 = getIntlString(FLANGE);
                } else if (FAN_SUPPLYDAMPER_GSD.equals(noChangeMap.get(supplyDamper))) {
                    str2 = getIntlString(GL_STEEL_DAMPER);
                } else if (FAN_SUPPLYDAMPER_AD.equals(noChangeMap.get(supplyDamper))) {
                    str2 = getIntlString(ALUMINUM_DAMPER);
                }
                map.put(outletDirection, str1 + UtilityConstant.SYS_PUNCTUATION_SLASH + str2);
            }
        }
        
        //计算轴功率
        double shaftPowerDb = new Double(0);
//        if (map.containsKey(shaftPower)) {
        	double airVolume = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_AIRVOLUME)));
        	double totalPressure = Double.parseDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_TOTALPRESSURE)));
        	double efficiency = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_EFFICIENCY)));
        	shaftPowerDb = (airVolume*totalPressure*100)/(3600*efficiency*1000);
        	BigDecimal shaftPowerBd = new BigDecimal(shaftPowerDb);//四舍五入，保留两位小数
        	shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
//        	contents[4][7] = String.valueOf(shaftPowerBd);
        	map.put(shaftPower, String.valueOf(shaftPowerBd));	
//        }
    	
//    	if (map.containsKey(inputPower)) {
    		//计算输入功率
        	double motorSafetyFacor = new Double(0);
        	double motorPower = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER)));
        	if(motorPower<0.5) {
        		motorSafetyFacor = 1.5;
        	}else if (motorPower>0.5 && motorPower<1) {
    			motorSafetyFacor = 1.4;
    		}else if (motorPower>1 && motorPower<2) {
    			motorSafetyFacor = 1.3;
    		}else if (motorPower>2 && motorPower<5) {
    			motorSafetyFacor = 1.2;
    		}else if(motorPower>5) {
    			motorSafetyFacor = 1.15;
    		}
        	double actualMotorPower = (shaftPowerDb*motorSafetyFacor)/(1);
        	System.out.println("#############################actualMotorPower=========="+actualMotorPower);
        	String motorEfficiencyStr = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTOREFF));
        	NumberFormat nf=NumberFormat.getPercentInstance();
        	Number motorEfficiencyNum;
    		try {
    			if(EmptyUtil.isNotEmpty(motorEfficiencyStr)) {
    				motorEfficiencyNum = nf.parse(motorEfficiencyStr);				
    			}else {
    				motorEfficiencyNum = 0;
    			}

    			double motorEfficiency = motorEfficiencyNum.doubleValue();
    			if(motorEfficiency==0) {
    	    		motorEfficiency = 0.94;
    	    	}
                //输入功率=轴功率*1.35
    			BigDecimal inputPowerBd = new BigDecimal(shaftPowerDb*1.35);// 四舍五入，保留两位小数
    			inputPowerBd = inputPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
//    			contents[6][7] = String.valueOf(inputPowerBd);
    			map.put(inputPower, String.valueOf(inputPowerBd));
    			if (UtilityConstant.SYS_STRING_NUMBER_4.equals(noChangeMap.get(UtilityConstant.METASEXON_FAN_TYPE))) {// 双速电机输入功率从表s_twospeedmotor表中取
    				int fanPole = BaseDataUtil
    						.stringConversionInteger(noChangeMap.get(UtilityConstant.META_SECTION_FAN_POLE));
    				double twoSpeedMotorPower = BaseDataUtil
    						.stringConversionDouble(noChangeMap.get(UtilityConstant.METASEXON_FAN_MOTORPOWER));
    				String motorType = noChangeMap.get(UtilityConstant.METASEXON_FAN_SUPPLIER);
    				List<STwoSpeedMotor> sTwoSpeedMotorList = AhuMetadata.findAll(STwoSpeedMotor.class);
    				for (STwoSpeedMotor sTwoSpeedMotor : sTwoSpeedMotorList) {
    					if (sTwoSpeedMotor.getJs() == fanPole && sTwoSpeedMotor.getGl() == twoSpeedMotorPower && sTwoSpeedMotor.getSupplier().equalsIgnoreCase(motorType)) {
//    						contents[6][7] = sTwoSpeedMotor.getInputPower();
    						map.put(inputPower, sTwoSpeedMotor.getInputPower());
    						//TODO 需要进行双电机逻辑的判断。。。。。。
                            if(sTwoSpeedMotor.getInputPower().contains("/")){
                                map.put(inputPower, sTwoSpeedMotor.getInputPower().split("/")[0]);
                            }else{
                                map.put(inputPower, sTwoSpeedMotor.getInputPower());
                            }

    						break;
    					}
    				}
    			}
    		} catch (ParseException e) {
    			e.printStackTrace();
    		}	
//		}
    	
        
        return map;
    }

    /**
     * 重置箱体插入损失
     * 标准供应商1120风机以上风机Airborne 声压级Lp(A) 不用减：箱体的插入损失
     * @param XTCRSS
     * @param isGK
     * @param fanSeriesVal
     * @return
     */
    private static double getXtcrssByGK1120(double XTCRSS,boolean isGK,String fanSeriesVal) {
        try {
            if(isGK && BaseDataUtil.stringConversionDouble(fanSeriesVal)>=1120){
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return XTCRSS;
    }

    /**
     * 亿利达前倾风机原始结果加对应值，用于计算Airborne 声压级Lp(A)
     * 亿利达前倾风机280	5	5	5	5	5	5	5	5
     * 亿利达前倾风机315	5	5	5	5	5	5	5	5
     * 亿利达前倾风机355	5	5	5	5	5	5	5	5
     * 亿利达前倾风机400	5	5	5	5	5	5	5	5
     * 亿利达前倾风机500	5	5	5	5	5	5	5	5
     * 亿利达前倾风机560	5	5	5	5	5	5	5	5
     * 亿利达前倾风机630	4	4	4	4	4	4	4	4
     * 亿利达前倾风机710	4	4	4	4	4	4	4	4
     * 亿利达前倾风机800	4	4	4	4	4	4	4	4
     * 亿利达前倾风机900	4	4	4	4	4	4	4	4
     * 亿利达前倾风机1000	4	4	4	4	4	4	4	4
     * @param fanSeriesVal
     * @return
     */
    private static int isFCAppend(String fanSeriesVal) {
        List<String> yldfcStrs = Arrays.asList(
                "280",
                "315",
                "355",
                "400",
                "500",
                "560",
                "630",
                "710",
                "800",
                "900",
                "1000");
        if(yldfcStrs.contains(fanSeriesVal)){
            if(BaseDataUtil.stringConversionDouble(fanSeriesVal) == 560){
                return 2;
            }else if(BaseDataUtil.stringConversionDouble(fanSeriesVal)<560){
                return 5;
            }
        }
        return 0;
    }

    /**
     * 计算声压级差值
     *
     *   步骤	内容	说明
     *   1	数据表增加Acoff行	39CQ/39G/39XT	ok
     *   2	数据表修正Outlet 63HZ	红色标记	ok
     *   3	计算机组总长度l1	"风机所在层：
     *        39G (段长模数*100+50)/100
     *        39CQ (段长模数*100+90)/100
     *        39XT (段长模数*100+104)/100"
     *   4	计算机组宽度l2	"39G (宽度模数*100+50)/100
     *        39CQ (宽度模数*100+90)/100
     *        39XT (宽度模数*100+104)/100"
     *   5	计算机组高度l3	"39G (高度模数*100+50)/100
     *        39CQ (高度模数*100+90)/100
     *        39XT (高度模数*100+104)/100"
     *   6	测试距离d 	固定值 1
     *   7	计算a,b,c
     *   8	计算测试面积SB
     *   9	计算声压级差值	10*logSB
     *   10	Airborne 声功率级Lw(A)	Airborne八分频各减步骤1新增Acoff，参考SHEET2第6行
     *   11	Airborne 声压级Lp(A)	步骤10的值-步骤9的值，参考SHEET2第7行
     *   12	Airborne 声压级Lp(A)加权值	参考SHEET2第7行，第10列
     *   13	报告输出Airborne 声压级	步骤12所得值
     * @param unitValue
     * @param noChangeMap
     * @param ahuParam
     * @return
     */
    private static double getlogSB(String unitValue , Map<String, String> noChangeMap , AhuParam ahuParam) {
        if(EmptyUtil.isEmpty(ahuParam)){
            return 0;
        }

        int position = NumberUtil.convertStringToDoubleInt(String.valueOf(noChangeMap.get(METAHU_ASSIS_POSITION)));
        List<AhuPartition> fanInOfPartitions = getPartitions(position,ahuParam);
        //计算机组总长度l1
        double l1 = getl1(fanInOfPartitions);

        //计算机组宽度l2
        double l2 = fanInOfPartitions.get(0).getWidth();
        l2 = reBuildL1L2L3ByS(unitValue,l2);

        //计算机组高度l3
        double l3 = fanInOfPartitions.get(0).getHeight();
        l3 = reBuildL1L2L3ByS(unitValue,l3);


        //测试距离默认1
        int d= 1;

        //计算临时计算变量a,b,c
        double a = 0.5 * l1 + d;
        double b = 0.5 * l2 + d;
        double c = l3 + d;

        //计算测试面积SB
        double sb = 4*(a*b + b*c + a*c) * ((a+b+c)/(a+b+c+2*d));

        //计算声压级差值 10*logSB
        double logsb = 10*Math.log10(sb);

        return logsb;
    }

    /**
     * 获取真实模数
     * 39G (lnum*100+50)/100
     * 39CQ (lnum*100+90)/100
     * 39XT (lnum*100+104)/100
     * @param unitValue
     * @param lnum
     * @return
     */
    private static double reBuildL1L2L3ByS(String unitValue, double lnum) {
        if(unitValue.contains(SystemCalculateConstants.AHU_PRODUCT_39G)){
            return (lnum*100+50)/1000;
        }else if(unitValue.contains(SystemCalculateConstants.AHU_PRODUCT_39CQ)){
            return (lnum*100+90)/1000;
        }else if(unitValue.contains(SystemCalculateConstants.AHU_PRODUCT_39XT)){
            return (lnum*100+104)/1000;
        }
        return lnum/10;
    }

    private static double getl1(List<AhuPartition> fanInOfPartitions) {
        double l1 = 0;
        for (AhuPartition fanInOfPartition : fanInOfPartitions) {
            l1+=(fanInOfPartition.getLength()+fanInOfPartition.getCasingWidth());
        }
        return l1/1000;
    }

    /**
     * 获取风机所在层信息
     * @param position
     * @param ahuParam
     * @return
     */
    private static List<AhuPartition> getPartitions(int position, AhuParam ahuParam) {
        if(EmptyUtil.isEmpty(ahuParam)){
            return null;
        }

        if(EmptyUtil.isNotEmpty(ahuParam.getTopPartitions())){
            for (AhuPartition ahuPartition : ahuParam.getTopPartitions()) {
                for (Map<String, Object> stringObjectMap : ahuPartition.getSections()) {
                    int pos = NumberUtil.convertStringToDoubleInt(String.valueOf(stringObjectMap.get(SYS_MAP_POS)));
                    if(position-1 == pos){
                        return ahuParam.getTopPartitions();
                    }
                }
            }
        }

        if(EmptyUtil.isNotEmpty(ahuParam.getBottomPartitions())){
            for (AhuPartition ahuPartition : ahuParam.getBottomPartitions()) {
                for (Map<String, Object> stringObjectMap : ahuPartition.getSections()) {
                    int pos = NumberUtil.convertStringToDoubleInt(String.valueOf(stringObjectMap.get(SYS_MAP_POS)));
                    if(position-1 == pos){
                        return ahuParam.getBottomPartitions();
                    }
                }
            }
        }

        //没有找到认为单层使用bootom：TODO 由于位置为0异常导致没有找到需要后期解决选型期间，assis.section.position 为空问题。
        return ahuParam.getBottomPartitions();
    }

    /**
     * 如果防剪切减震器，JD HD 减震器标识好改为JTT
     * @param fjq
     * @param absorberVal
     * @return
     */
    private static String reBuildJTT(boolean fjq, String absorberVal) {
        if(fjq && EmptyUtil.isNotEmpty(absorberVal) && absorberVal.length()>2){
            return absorberVal.replace("JD","JTT").replace("HD","JTT");
        }else{
            return absorberVal;
        }

    }

    // 追加热水盘管excel报告自定义数据
    public static Map<String, String> appendHeatingcoil(Map<String, String> noChangeMap, Map<String, String> map) {
        String headerDia = UtilityConstant.METASEXON_HEATINGCOIL_HEADERDIA;// 进出水管径
        map.put(headerDia, UtilityConstant.SYS_BLANK);
        String coilModel = UtilityConstant.METASEXON_HEATINGCOIL_COILMODEL;// 盘管型号描述
        map.put(coilModel, UtilityConstant.SYS_BLANK);

        return map;
    }

    // 封装热水盘管报告数据
    public static Map<String, String> getHeatingcoil(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        appendHeatingcoil(noChangeMap, map);
        map = getAirVolume(CommonConstant.METASEXON_HEATINGCOIL_PREFIX,noChangeMap, map);// 计算风量
        String modeNote = UtilityConstant.METASEXON_HEATINGCOIL_MODENOTE;// 模型描述
        String coilNote = UtilityConstant.METASEXON_HEATINGCOIL_COILNOTE;// 盘管线描述
        String tubeDiameter = UtilityConstant.METASEXON_HEATINGCOIL_TUBEDIAMETER;// 管径
        String interval = UtilityConstant.METASEXON_HEATINGCOIL_INTERVAL;// 管间距
        String coilFrameMaterialNote = UtilityConstant.METASEXON_HEATINGCOIL_COILFRAMEMATERIALNOTE;// 盘管支架材质
        String coilFrameMaterial = UtilityConstant.METASEXON_HEATINGCOIL_COILFRAMEMATERIAL;// 盘管框架材质
        String connections = UtilityConstant.METASEXON_HEATINGCOIL_CONNECTIONS;// 连接方式
        String rows = UtilityConstant.METASEXON_HEATINGCOIL_ROWS;// 排数
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String baffleMaterial = UtilityConstant.METASEXON_HEATINGCOIL_BAFFLEMATERIAL;// 挡风板材质
        String serialStr = UtilityConstant.SYS_BLANK;
        String headerDia = UtilityConstant.METASEXON_HEATINGCOIL_HEADERDIA;// 进出水管径
        String coilModel = UtilityConstant.METASEXON_HEATINGCOIL_COILMODEL;// 盘管型号描述
        String finDensity = UtilityConstant.METASEXON_HEATINGCOIL_FINDENSITY;// 片距
        String circuit = UtilityConstant.METASEXON_HEATINGCOIL_CIRCUIT;// 回路
        String heightLength = UtilityConstant.METASEXON_HEATINGCOIL_HEIGHTLENGTH;// 高度/长度
        String sconcentration = UtilityConstant.METASEXON_HEATINGCOIL_SCONCENTRATION;//浓度
        String scoolant = UtilityConstant.METASEXON_HEATINGCOIL_SCOOLANT;//介质
        String WreturnEnteringFluidTemperature = UtilityConstant.METASEXON_HEATINGCOIL_WRETURNENTERINGFLUIDTEMPERATURE;//进水温度（冬季计算结果）       
        String WreturnWTAScend = UtilityConstant.METASEXON_HEATINGCOIL_WRETURNWTASCEND;//水温升（夏季计算结果）
        String WreturnOutFluidTemperature = UtilityConstant.METASEXON_HEATINGCOIL_WRETURNOUTFLUIDTEMPERATURE;//出水温度
        String svelocity = UtilityConstant.METASEXON_HEATINGCOIL_WVELOCITY;//迎面风速

        // 计算迎面风速
//        if (map.containsKey(svelocity)) {
            double airVolume = BaseDataUtil.stringConversionDouble(map.get(UtilityConstant.METAHU_AIRVOLUME));
            double svelocityDB = 0.00;
            
            try {
                svelocityDB = FacaVelocityUtil.getCoil(airVolume, noChangeMap.get(serial),
                        noChangeMap.get(tubeDiameter), SectionTypeEnum.TYPE_HEATINGCOIL.getId());
                BigDecimal shaftPowerBd = new BigDecimal(svelocityDB);// 四舍五入，保留两位小数
                shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
                map.put(svelocity, String.valueOf(shaftPowerBd));
            } catch (Exception e) {
                // 计算失败则取系统迎面风速
                map.put(svelocity, noChangeMap.get(svelocity));
                e.printStackTrace();
            }
//        }

        //计算出水温度
        if (map.containsKey(WreturnEnteringFluidTemperature)) {
            double jswd = BaseDataUtil.stringConversionDouble(noChangeMap.get(WreturnEnteringFluidTemperature));
            double swj = BaseDataUtil.stringConversionDouble(noChangeMap.get(WreturnWTAScend));
            map.put(WreturnOutFluidTemperature, String.valueOf(jswd - swj));
        }


        if (map.containsKey(scoolant)) {
            if (UtilityConstant.SYS_STRING_NUMBER_1.equals(noChangeMap.get(scoolant))) {//介质 为水，浓度100%
                map.put(sconcentration, "100%");
            } else {
                map.put(sconcentration, noChangeMap.get(sconcentration) + UtilityConstant.SYS_PUNCTUATION_PERCENTAGE);
            }
        }
        String tubeDiameterValue = UtilityConstant.SYS_BLANK;
        if (map.containsKey(tubeDiameter)) {
            String value = noChangeMap.get(tubeDiameter);
            tubeDiameterValue = value;
            if (value.equals(HEATINGCOIL_TUBEDIAMETER_1_2)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_HEATINGCOIL_MODENOTE_1_2);
                map.put(interval, "31.75*27.5");
                map.put(tubeDiameter, "12.7");
            }
            if (value.equals(HEATINGCOIL_TUBEDIAMETER_3_8)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_HEATINGCOIL_MODENOTE_3_8);
                map.put(interval, "25.4*22");
                map.put(tubeDiameter, "9.52");
            }
        }
        if (map.containsKey(serial)) {
            Object value = noChangeMap.get(serial);
            String unit = SystemCountUtil.getUnit(String.valueOf(value));
            serialStr = String.valueOf(value);
            if (SystemCountUtil.gtSmallUnit(BaseDataUtil.stringConversionInteger(unit))) {
                if (map.containsKey(baffleMaterial)) {
                    String baffleMaterialStr = noChangeMap.get(baffleMaterial);
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(baffleMaterialStr,
                            baffleMaterial, SectionTypeEnum.TYPE_HEATINGCOIL.getId(), language));// 盘管支架材质
                    // 使用挡风板材质
                }
            } else {
                if (map.containsKey(coilFrameMaterial)) {
                    String coilFrameMaterialStr = noChangeMap.get(coilFrameMaterial).toString();
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(coilFrameMaterialStr,
                            coilFrameMaterial, SectionTypeEnum.TYPE_HEATINGCOIL.getId(), language));// 盘管支架材质
                    // 使用框架材质
                }

            }
        }
        // 封装管接头
        if (map.containsKey(connections)) {
            String value = noChangeMap.get(connections).toString();
            int valueStr;
            String conStr = UtilityConstant.SYS_BLANK;
            if (value.equals(SystemCalculateConstants.HEATINGCOIL_CONNECTIONS_THREAD)) {
                valueStr = 0;
                conStr = getIntlString(THREAD);
            } else {
                valueStr = 1;
                conStr = getIntlString(FLANGE);
            }
            String varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
            if (map.containsKey(rows)) {
                String valuerows = BaseDataUtil.constraintString(noChangeMap.get(rows));
                if (valuerows.equals("2") || valuerows.equals("1")) {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
                } else {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_D_UP;
                }
            }
            STakeoverSize sTakeoverSize = AhuMetadata.findOne(STakeoverSize.class, serialStr, varTakeOverSize,
                    String.valueOf(valueStr));
            if (null != sTakeoverSize) {
                map.put(connections, conStr + " " + sTakeoverSize.getSizeValue());
            } else {
                map.put(connections, UtilityConstant.SYS_BLANK);
            }
        }
        // 进出水管径
        if (map.containsKey(headerDia)) {
            String value = noChangeMap.get(connections).toString();
            int valueStr;
            if (value.equals(SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_THREAD)) {
                valueStr = 0;
            } else {
                valueStr = 1;
            }
            String varTakeOverSize = UtilityConstant.SYS_BLANK;
            if (map.containsKey(rows)) {
                String valuerows = String.valueOf(noChangeMap.get(rows));
                if (valuerows.equals("2") || valuerows.equals("1")) {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
                } else {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_D_UP;
                }
            }
            STakeoverSize sTakeoverSize = AhuMetadata.findOne(STakeoverSize.class, serialStr, varTakeOverSize,
                    String.valueOf(valueStr));
            if (null != sTakeoverSize) {
                map.put(headerDia, sTakeoverSize.getHeadSize());
            } else {
                map.put(headerDia, UtilityConstant.SYS_BLANK);
            }
        }

        //盘管型号
        if (map.containsKey(coilModel)) {
            if (map.containsKey(tubeDiameter)) {
                String value = map.get(tubeDiameter);

                String valuerows = UtilityConstant.SYS_BLANK;
                if (map.containsKey(rows)) {
                    valuerows = String.valueOf(noChangeMap.get(rows));
                }
                if ("12.7".equals(value)) {
                    map.put(coilModel, "12.7mm" + UtilityConstant.SYS_PUNCTUATION_SLASH + valuerows + UtilityConstant.SYS_ALPHABET_R_UP + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(finDensity)) + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(circuit)));
                } else {
                    map.put(coilModel, "9.52mm" + UtilityConstant.SYS_PUNCTUATION_SLASH + valuerows + UtilityConstant.SYS_ALPHABET_R_UP + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(finDensity)) + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(circuit)));
                }
            }

        }
        map.put(coilNote, "28CU");
        //冷水盘管高度长度
        StringBuffer heightLen = new StringBuffer(UtilityConstant.SYS_BLANK);
        if (tubeDiameterValue.equals(HEATINGCOIL_TUBEDIAMETER_1_2)) {
            List<SCoilInfo> coilInfos = AhuMetadata.findList(SCoilInfo.class, serialStr, UtilityConstant.SYS_STRING_NUMBER_6, String.valueOf(noChangeMap.get(circuit)));
            for (int i = 0; i < coilInfos.size(); i++) {
                int cubeHeight = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(coilInfos.get(i).getTNo() * 31.75));
                int tLen = coilInfos.get(i).getTLen();
                heightLen.append(cubeHeight + UtilityConstant.SYS_PUNCTUATION_SLASH + tLen + UtilityConstant.SYS_PUNCTUATION_COMMA);
            }
        }
        if (tubeDiameterValue.equals(HEATINGCOIL_TUBEDIAMETER_3_8)) {
            List<SCoilInfo1> coilInfos = AhuMetadata.findList(SCoilInfo1.class, serialStr, UtilityConstant.SYS_STRING_NUMBER_6, String.valueOf(noChangeMap.get(circuit)));
            for (int i = 0; i < coilInfos.size(); i++) {
                int cubeHeight = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(coilInfos.get(i).getTNo() * 25.4));
                int tLen = coilInfos.get(i).getTLen();
                heightLen.append(cubeHeight + UtilityConstant.SYS_PUNCTUATION_SLASH + tLen + UtilityConstant.SYS_PUNCTUATION_COMMA);
            }
        }
        if (heightLen.toString().length() > 0)
            map.put(heightLength, heightLen.toString().substring(0, heightLen.toString().length() - 1));
        return map;
    }

    // 封装直接蒸发式盘管报告数据
    public static Map<String, String> getDXCoil(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_DXCOIL_PREFIX,noChangeMap, map);// 计算风量
        String modeNote = UtilityConstant.METASEXON_DXCOIL_MODENOTE;// 模型描述
        String coilNote = UtilityConstant.METASEXON_DXCOIL_COILNOTE;// 盘管线描述
        String tubeDiameter = UtilityConstant.METASEXON_DXCOIL_TUBEDIAMETER;// 管径
        String interval = UtilityConstant.METASEXON_DXCOIL_INTERVAL;// 管间距
        String finTem = UtilityConstant.METASEXON_DXCOIL_FINTEM;// 最小翅片表面温度
        String cpTem = UtilityConstant.METASEXON_DXCOIL_CPTEM;// 最小铜管壁表面温度
        String circuit = UtilityConstant.METASEXON_DXCOIL_CIRCUIT;// 回路
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String coilFrameMaterialNote = UtilityConstant.METASEXON_DXCOIL_COILFRAMEMATERIALNOTE;// 盘管支架材质
        String coilFrameMaterial = UtilityConstant.METASEXON_DXCOIL_COILFRAMEMATERIAL;// 盘管框架材质
        String connections = UtilityConstant.METASEXON_DXCOIL_CONNECTIONS;// 连接方式
        String rows = UtilityConstant.METASEXON_DXCOIL_ROWS;// 排数
        String heightLength = UtilityConstant.METASEXON_DXCOIL_HEIGHTLENGTH;// 高度/长度
        String baffleMaterial = UtilityConstant.METASEXON_DXCOIL_BAFFLEMATERIAL;// 挡风板材质
        String serialStr = UtilityConstant.SYS_BLANK;
        String svelocity = UtilityConstant.METASEXON_DXCOIL_SVELOCITY;//迎面风速

        // 计算迎面风速
        if (map.containsKey(svelocity)) {
            double airVolume = BaseDataUtil.stringConversionDouble(map.get(UtilityConstant.METAHU_AIRVOLUME));
            double svelocityDB = 0.00;
//            String airDirection = BaseDataUtil
//                    .constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
//            if (UtilityConstant.SYS_ALPHABET_R_UP.equals(airDirection)) {//根据风向获取相应的风量
//                airVolume = BaseDataUtil
//                        .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME)));
//            } else if (UtilityConstant.SYS_ALPHABET_S_UP.equals(airDirection)) {
//                airVolume = BaseDataUtil
//                        .stringConversionDouble(String.valueOf(getSupplyAirVolume(noChangeMap)));
//            }
            try {
                svelocityDB = FacaVelocityUtil.getCoil(airVolume, noChangeMap.get(serial),
                        noChangeMap.get(tubeDiameter), SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId());
                BigDecimal shaftPowerBd = new BigDecimal(svelocityDB);// 四舍五入，保留两位小数
                shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
                map.put(svelocity, String.valueOf(shaftPowerBd));
            } catch (Exception e) {
                // 计算失败则取系统迎面风速
                map.put(svelocity, noChangeMap.get(svelocity));
                e.printStackTrace();
            }
        }

        if (map.containsKey(tubeDiameter)) {
            String value = noChangeMap.get(tubeDiameter);
            if (value.equals(COOLINGCOIL_TUBEDIAMETER_1_2)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_COOLINGCOIL_MODENOTE_1_2);
                map.put(interval, "31.75*27.5");
                map.put(tubeDiameter, "12.7");
            }
            if (value.equals(COOLINGCOIL_TUBEDIAMETER_3_8)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_COOLINGCOIL_MODENOTE_3_8);
                map.put(interval, "25.4*22");
                map.put(tubeDiameter, "9.52");
            }
        }
        if (map.containsKey(circuit)) {// TODO
            Object value = noChangeMap.get(circuit);
        }
        if (map.containsKey(serial)) {
            Object value = noChangeMap.get(serial);
            String unit = SystemCountUtil.getUnit(String.valueOf(value));
            serialStr = String.valueOf(value);
            if (SystemCountUtil.gtSmallUnit(BaseDataUtil.stringConversionInteger(unit))) {
                if (map.containsKey(baffleMaterial)) {
                    String baffleMaterialStr = noChangeMap.get(baffleMaterial);
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(baffleMaterialStr,
                            baffleMaterial, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId(), language));// 盘管支架材质
                    // 使用挡风板材质
                }
            } else {
                if (map.containsKey(coilFrameMaterial)) {
                    String coilFrameMaterialStr = noChangeMap.get(coilFrameMaterial).toString();
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(coilFrameMaterialStr,
                            coilFrameMaterial, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId(), language));// 盘管支架材质
                    // 使用框架材质
                }

            }
        }
        // 封装管接头
        if (map.containsKey(connections)) {
            String value = noChangeMap.get(connections).toString();
            int valueStr;
            String conStr = UtilityConstant.SYS_BLANK;
            if (value.equals(SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_THREAD)) {
                valueStr = 0;
                conStr = getIntlString(THREAD);
            } else {
                valueStr = 1;
                conStr = getIntlString(FLANGE);
            }
            String varTakeOverSize = UtilityConstant.SYS_BLANK;
            if (map.containsKey(rows)) {
                String valuerows = noChangeMap.get(rows);
                if (valuerows.equals("2") || valuerows.equals("1")) {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
                } else {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_D_UP;
                }
            }
            STakeoverSize sTakeoverSize = AhuMetadata.findOne(STakeoverSize.class, serialStr, varTakeOverSize,
                    String.valueOf(valueStr));
            if (null != sTakeoverSize) {
                map.put(connections, conStr + " " + sTakeoverSize.getSizeValue());
            } else {
                map.put(connections, UtilityConstant.SYS_BLANK);
            }
        }

        map.put(coilNote, "28CW");
        map.put(finTem, ">0.0");
        map.put(cpTem, ">0.0");
        return map;
    }

    // 追加冷水盘管excel报告自定义数据
    public static Map<String, String> appendCoolingCoil(Map<String, String> noChangeMap, Map<String, String> map) {
        String headerDia = UtilityConstant.METASEXON_COOLINGCOIL_HEADERDIA;// 进出水管径
        map.put(headerDia, UtilityConstant.SYS_BLANK);
        String coilModel = UtilityConstant.METASEXON_COOLINGCOIL_COILMODEL;// 盘管型号描述
        map.put(coilModel, UtilityConstant.SYS_BLANK);

        return map;
    }

    // 封装冷水盘管报告数据
    public static Map<String, String> getCoolingCoil(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        appendCoolingCoil(noChangeMap, map);//追加自定义key
        map = getAirVolume(CommonConstant.METASEXON_COOLINGCOIL_PREFIX,noChangeMap, map);// 计算风量
        String modeNote = UtilityConstant.METASEXON_COOLINGCOIL_MODENOTE;// 模型描述
        String coilNote = UtilityConstant.METASEXON_COOLINGCOIL_COILNOTE;// 盘管线描述
        String tubeDiameter = UtilityConstant.METASEXON_COOLINGCOIL_TUBEDIAMETER;// 管径
        String interval = UtilityConstant.METASEXON_COOLINGCOIL_INTERVAL;// 管间距
        String finTem = UtilityConstant.METASEXON_COOLINGCOIL_FINTEM;// 最小翅片表面温度
        String cpTem = UtilityConstant.METASEXON_COOLINGCOIL_CPTEM;// 最小铜管壁表面温度
        String circuit = UtilityConstant.METASEXON_COOLINGCOIL_CIRCUIT;// 回路
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String coilFrameMaterialNote = UtilityConstant.METASEXON_COOLINGCOIL_COILFRAMEMATERIALNOTE;// 盘管支架材质
        String coilFrameMaterial = UtilityConstant.METASEXON_COOLINGCOIL_COILFRAMEMATERIAL;// 盘管框架材质
        String connections = UtilityConstant.METASEXON_COOLINGCOIL_CONNECTIONS;// 连接方式
        String rows = UtilityConstant.METASEXON_COOLINGCOIL_ROWS;// 排数
        String heightLength = UtilityConstant.METASEXON_COOLINGCOIL_HEIGHTLENGTH;// 盘管高度/长度
        String baffleMaterial = UtilityConstant.METASEXON_COOLINGCOIL_BAFFLEMATERIAL;// 挡风板材质
        String headerDia = UtilityConstant.METASEXON_COOLINGCOIL_HEADERDIA;// 进出水管径
        String coilModel = UtilityConstant.METASEXON_COOLINGCOIL_COILMODEL;// 盘管型号描述
        String finDensity = UtilityConstant.METASEXON_COOLINGCOIL_FINDENSITY;// 片距
        String finType = UtilityConstant.METASEXON_COOLINGCOIL_FINTYPE;//翅片材质

        String sconcentration = UtilityConstant.METASEXON_COOLINGCOIL_SCONCENTRATION;//浓度
        String scoolant = UtilityConstant.METASEXON_COOLINGCOIL_SCOOLANT;//介质

        String SreturnEnteringFluidTemperature = UtilityConstant.METASEXON_COOLINGCOIL_SRETURNENTERINGFLUIDTEMPERATURE;//进水温度（夏季计算结果）       
        String SreturnWTAScend = UtilityConstant.METASEXON_COOLINGCOIL_SRETURNWTASCEND;//水温升（夏季计算结果）
        String SreturnOutFluidTemperature = UtilityConstant.METASEXON_COOLINGCOIL_SRETURNOUTFLUIDTEMPERATURE;//出水温度
        String svelocity = UtilityConstant.METASEXON_COOLINGCOIL_SVELOCITY;//迎面风速
        String WreturnOutFluidTemperature = UtilityConstant.METASEXON_COOLINGCOIL_WRETURNOUTFLUIDTEMPERATURE;//冬季出水温度
        String wReturnWTAScend = UtilityConstant.METASEXON_COOLINGCOIL_WRETURNWTASCEND;//水温降冬季
        String WreturnEnteringFluidTemperature = UtilityConstant.METASEXON_COOLINGCOIL_WRETURNENTERINGFLUIDTEMPERATURE;//进水温度冬季
        String valuerows = SYS_BLANK;
        if (map.containsKey(rows)) {
            valuerows = String.valueOf(noChangeMap.get(rows));
        }
        // 计算迎面风速
        if (map.containsKey(svelocity)) {
            double airVolume = BaseDataUtil.stringConversionDouble(map.get(UtilityConstant.METAHU_AIRVOLUME));
            double svelocityDB = 0.00;
//            String airDirection = BaseDataUtil
//                    .constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
//            if (UtilityConstant.SYS_ALPHABET_R_UP.equals(airDirection)) {//根据风向获取相应的风量
//                airVolume = BaseDataUtil
//                        .stringConversionDouble(String.valueOf(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME)));
//            } else if (UtilityConstant.SYS_ALPHABET_S_UP.equals(airDirection)) {
//                airVolume = BaseDataUtil
//                        .stringConversionDouble(String.valueOf(getSupplyAirVolume(noChangeMap)));
//            }
            try {
                svelocityDB = FacaVelocityUtil.getCoil(airVolume, noChangeMap.get(serial),
                        noChangeMap.get(tubeDiameter), SectionTypeEnum.TYPE_COLD.getId());
                BigDecimal shaftPowerBd = new BigDecimal(svelocityDB);// 四舍五入，保留两位小数
                shaftPowerBd = shaftPowerBd.setScale(2, BigDecimal.ROUND_HALF_UP);
                map.put(svelocity, String.valueOf(shaftPowerBd));
            } catch (Exception e) {
                // 计算失败则取系统迎面风速
                map.put(svelocity, noChangeMap.get(svelocity));
                e.printStackTrace();
            }
        }

        //计算出水温度
        if (map.containsKey(SreturnEnteringFluidTemperature)) {
            double jswd = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(SreturnEnteringFluidTemperature)));
            double sws = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(SreturnWTAScend)));
            map.put(SreturnOutFluidTemperature, String.valueOf(jswd + sws));
        }
        
        //计算冬季出水温度
        if (map.containsKey(WreturnEnteringFluidTemperature)) {
			double jswd = BaseDataUtil
					.stringConversionDouble(String.valueOf(noChangeMap.get(WreturnEnteringFluidTemperature)));
			double swj = BaseDataUtil.stringConversionDouble(String.valueOf(noChangeMap.get(wReturnWTAScend)));
			map.put(WreturnOutFluidTemperature, String.valueOf(jswd - swj));
		}

        if (map.containsKey(scoolant)) {
            if (UtilityConstant.SYS_STRING_NUMBER_1.equals(noChangeMap.get(scoolant))) {//介质 为水，浓度100%
                map.put(sconcentration, "100%");
            } else {
                map.put(sconcentration, String.valueOf(noChangeMap.get(sconcentration)) + UtilityConstant.SYS_PUNCTUATION_PERCENTAGE);
            }
        }
        String serialStr = UtilityConstant.SYS_BLANK;
        String tubeDiameterValue = UtilityConstant.SYS_BLANK;
        if (map.containsKey(tubeDiameter)) {
            tubeDiameterValue = noChangeMap.get(tubeDiameter);
            if (tubeDiameterValue.equals(COOLINGCOIL_TUBEDIAMETER_1_2)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_COOLINGCOIL_MODENOTE_1_2);

//                if (map.containsKey(rows)) {
//                    if (valuerows.equals("3") || valuerows.equals("5") || valuerows.equals("7")) {
//                        map.put(modeNote,UtilityConstant.SYS_MAP_COOLINGCOIL_MODENOTE_1_2.replace("28CW","27CW"));
//                    }
//                }

                map.put(interval, "31.75*27.5");
                map.put(tubeDiameter, UtilityConstant.JSON_COOLINGCOIL_COILMODEL_12_7);
            }
            if (tubeDiameterValue.equals(COOLINGCOIL_TUBEDIAMETER_3_8)) {
                map.put(modeNote,
                        UtilityConstant.SYS_MAP_COOLINGCOIL_MODENOTE_3_8);

//                if (map.containsKey(rows)) {
//                    if (valuerows.equals("3") || valuerows.equals("5") || valuerows.equals("7")) {
//                        map.put(modeNote,UtilityConstant.SYS_MAP_COOLINGCOIL_MODENOTE_3_8.replace("28CW","27CW"));
//                    }
//                }

                map.put(interval, "25.4*22");
                map.put(tubeDiameter, UtilityConstant.JSON_COOLINGCOIL_COILMODEL_9_52);
            }
        }
        if (map.containsKey(circuit)) {// TODO
            Object value = noChangeMap.get(circuit);
        }
        if (map.containsKey(serial)) {
            Object value = noChangeMap.get(serial);
            String unit = SystemCountUtil.getUnit(String.valueOf(value));
            serialStr = String.valueOf(value);
            if (SystemCountUtil.gtSmallUnit(BaseDataUtil.stringConversionInteger(unit))) {
                if (map.containsKey(baffleMaterial)) {
                    String baffleMaterialStr = noChangeMap.get(baffleMaterial);
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(baffleMaterialStr,
                            baffleMaterial, SectionTypeEnum.TYPE_COLD.getId(), language));// 盘管支架材质
                    // 使用挡风板材质
                }
            } else {
                if (map.containsKey(coilFrameMaterial)) {
                    String coilFrameMaterialStr = noChangeMap.get(coilFrameMaterial).toString();
                    map.put(coilFrameMaterialNote, AhuSectionMetas.getInstance().getValueByUnit(coilFrameMaterialStr,
                            coilFrameMaterial, SectionTypeEnum.TYPE_COLD.getId(), language));// 盘管支架材质
                    // 使用框架材质
                }

            }
        }
        // 封装管接头
        if (map.containsKey(connections)) {
            String value = noChangeMap.get(connections).toString();
            int valueStr;
            String conStr = UtilityConstant.SYS_BLANK;
            if (value.equals(SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_THREAD)) {
                valueStr = 0;
                conStr = getIntlString(THREAD);
            } else {
                valueStr = 1;
                conStr = getIntlString(FLANGE);
            }
            String varTakeOverSize = UtilityConstant.SYS_BLANK;
            if (map.containsKey(rows)) {
                if (valuerows.equals("2") || valuerows.equals("1")) {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
                } else {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_D_UP;
                }
            }
            STakeoverSize sTakeoverSize = AhuMetadata.findOne(STakeoverSize.class, serialStr, varTakeOverSize,
                    String.valueOf(valueStr));
            if (null != sTakeoverSize) {
                map.put(connections, conStr + " " + sTakeoverSize.getSizeValue());
            } else {
                map.put(connections, UtilityConstant.SYS_BLANK);
            }
        }
        // 进出水管径
        if (map.containsKey(headerDia)) {
            String value = noChangeMap.get(connections).toString();
            int valueStr;
            if (value.equals(SystemCalculateConstants.COOLINGCOIL_CONNECTIONS_THREAD)) {
                valueStr = 0;
            } else {
                valueStr = 1;
            }
            String varTakeOverSize = UtilityConstant.SYS_BLANK;
            if (map.containsKey(rows)) {
                if (valuerows.equals("2") || valuerows.equals("1")) {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_E_UP;
                } else {
                    varTakeOverSize = UtilityConstant.SYS_ALPHABET_D_UP;
                }
            }
            STakeoverSize sTakeoverSize = AhuMetadata.findOne(STakeoverSize.class, serialStr, varTakeOverSize,
                    String.valueOf(valueStr));
            if (null != sTakeoverSize) {
                map.put(headerDia, sTakeoverSize.getHeadSize());
            } else {
                map.put(headerDia, UtilityConstant.SYS_BLANK);
            }
        }
        //盘管型号
        if (map.containsKey(coilModel)) {
            if (map.containsKey(tubeDiameter)) {
                String value = noChangeMap.get(tubeDiameter);
                if (map.containsKey(rows)) {
                    valuerows = String.valueOf(noChangeMap.get(rows));
                }
                if (value.equals(UtilityConstant.JSON_COOLINGCOIL_COILMODEL_T4)) {
                    map.put(coilModel, "12.7mm" + UtilityConstant.SYS_PUNCTUATION_SLASH + valuerows + UtilityConstant.SYS_ALPHABET_R_UP + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(finDensity)) + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(circuit)));
                } else {
                    map.put(coilModel, "9.52mm" + UtilityConstant.SYS_PUNCTUATION_SLASH + valuerows + UtilityConstant.SYS_ALPHABET_R_UP + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(finDensity)) + UtilityConstant.SYS_PUNCTUATION_SLASH + String.valueOf(noChangeMap.get(circuit)));
                }
            }

        }
        if (map.containsKey(rows)) {
//            if (valuerows.equals("3") || valuerows.equals("5") || valuerows.equals("7")) {
//                map.put(coilNote, "27CW");
//            }else{
//                map.put(coilNote, "28CW");
//            }
        	map.put(coilNote, "28CW");
        }else{
            map.put(coilNote, "28CW");
        }
        map.put(finTem, ">0.0");
        map.put(cpTem, ">0.0");

        //冷水盘管高度长度
        StringBuffer heightLen = new StringBuffer(UtilityConstant.SYS_BLANK);
        if (tubeDiameterValue.equals(COOLINGCOIL_TUBEDIAMETER_1_2)) {
            List<SCoilInfo> coilInfos = AhuMetadata.findList(SCoilInfo.class, serialStr, UtilityConstant.SYS_STRING_NUMBER_6, String.valueOf(noChangeMap.get(circuit)));
            for (int i = 0; i < coilInfos.size(); i++) {
                int cubeHeight = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(coilInfos.get(i).getTNo() * 31.75));
                int tLen = coilInfos.get(i).getTLen();
                heightLen.append(cubeHeight + UtilityConstant.SYS_PUNCTUATION_SLASH + tLen + UtilityConstant.SYS_PUNCTUATION_COMMA);
            }
        }
        if (tubeDiameterValue.equals(COOLINGCOIL_TUBEDIAMETER_3_8)) {
            List<SCoilInfo1> coilInfos = AhuMetadata.findList(SCoilInfo1.class, serialStr, UtilityConstant.SYS_STRING_NUMBER_6, String.valueOf(noChangeMap.get(circuit)));
            for (int i = 0; i < coilInfos.size(); i++) {
                int cubeHeight = Integer.parseInt(new java.text.DecimalFormat(UtilityConstant.SYS_STRING_NUMBER_0).format(coilInfos.get(i).getTNo() * 25.4));
                int tLen = coilInfos.get(i).getTLen();
                heightLen.append(cubeHeight + UtilityConstant.SYS_PUNCTUATION_SLASH + tLen + UtilityConstant.SYS_PUNCTUATION_COMMA);
            }
        }
        if (heightLen.toString().length() > 0)
            map.put(heightLength, heightLen.toString().substring(0, heightLen.toString().length() - 1));

//        if (map.containsKey(finType)) {
//            String ft = String.valueOf(noChangeMap.get(finType));
//            String fintypev = UtilityConstant.SYS_BLANK;
//            if (UtilityConstant.JSON_COOLINGCOIL_FINTYPE_AL.equals(ft)) {
//                fintypev = "普通铝翅片";
//            } else if (UtilityConstant.JSON_COOLINGCOIL_FINTYPE_PROCOATEDAL.equals(ft)) {
//                fintypev = "亲水铝翅片";
//            } else if (UtilityConstant.JSON_COOLINGCOIL_FINTYPE_COPPER.equals(ft)) {
//                fintypev = "铜翅片";
//            }
//            map.put(finType, fintypev);
//        }

        return map;
    }

    // 追加综合过滤段奥雅纳工程格式报告，（袋式/板式）初、终阻力求和
    public static Map<String, String> appendCombinedFilter(Map<String, String> noChangeMap, Map<String, String> map) {
        String initialPD = UtilityConstant.METASEXON_COMBINEDFILTER_INITIALPD;// 初阻力
        map.put(initialPD, UtilityConstant.SYS_BLANK);
        String finalPD = UtilityConstant.METASEXON_COMBINEDFILTER_FINALPD;// 终阻力
        map.put(finalPD, UtilityConstant.SYS_BLANK);

        return map;
    }

    // 封装综合过滤段报告数据
    public static Map<String, String> getCombinedFilter(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        appendCombinedFilter(noChangeMap, map);
        map = getAirVolume(CommonConstant.METASEXON_COMBINEDFILTER_PREFIX,noChangeMap, map);// 计算风量
        String fitetF = UtilityConstant.METASEXON_COMBINEDFILTER_FITETF;// 过滤形式
        String rimThickness = UtilityConstant.METASEXON_COMBINEDFILTER_RIMTHICKNESS;// 边框厚度
        String rimThicknessP = UtilityConstant.METASEXON_COMBINEDFILTER_RIMTHICKNESSP;// 边框厚度
        String rimThicknessB = UtilityConstant.METASEXON_COMBINEDFILTER_RIMTHICKNESSB;// 边框厚度
        String filterArrange = UtilityConstant.METASEXON_COMBINEDFILTER_FILTERARRANGE;// 供应商
        String supplier = UtilityConstant.METASEXON_COMBINEDFILTER_SUPPLIER;// 过滤器布置
        /* 板式规格信息 */
        String paraP1 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAP1;
        String paraPN1 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAPN1;
        String paraP2 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAP2;
        String paraPN2 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAPN2;
        String paraP3 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAP3;
        String paraPN3 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAPN3;
        String paraP4 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAP4;
        String paraPN4 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAPN4;
        /* 袋式规格信息 */
        String paraB1 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAB1;
        String paraBN1 = UtilityConstant.METASEXON_COMBINEDFILTER_PARABN1;
        String paraB2 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAB2;
        String paraBN2 = UtilityConstant.METASEXON_COMBINEDFILTER_PARABN2;
        String paraB3 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAB3;
        String paraBN3 = UtilityConstant.METASEXON_COMBINEDFILTER_PARABN3;
        String paraB4 = UtilityConstant.METASEXON_COMBINEDFILTER_PARAB4;
        String paraBN4 = UtilityConstant.METASEXON_COMBINEDFILTER_PARABN4;

        String initialPD = UtilityConstant.METASEXON_COMBINEDFILTER_INITIALPD;// 初阻力
        String finalPD = UtilityConstant.METASEXON_COMBINEDFILTER_FINALPD;// 终阻力


        map.put(fitetF, getIntlString(PANEL_TYPE_BAG_TYPE));
        map.put(supplier, getIntlString(STANDARD_SUPPLIER));
        if (map.containsKey(rimThickness)) {
            String value = noChangeMap.get(rimThickness).toString();
            map.put(rimThicknessP, value.substring(0, 2));
            map.put(rimThicknessB, value.substring(value.length() - 2, value.length()));
        }
        if (map.containsKey(filterArrange)) {// 封装过滤器布置
            String value = noChangeMap.get(filterArrange).toString();
            List<FilterPanelBean> filterPanelBeanList = JSONArray.parseArray(value, FilterPanelBean.class);
            if (null != filterPanelBeanList && filterPanelBeanList.size() > 0) {
                int i = 1;
                for (FilterPanelBean filterPanelBean : filterPanelBeanList) {
                    if (i == 1) {
                        if (null != filterPanelBean.getPOption()) {
                            map.put(paraP1, filterPanelBean.getPOption());
                        }
                        if (null != filterPanelBean.getPCount()) {
                            map.put(paraPN1, filterPanelBean.getPCount());
                        }
                        if (null != filterPanelBean.getLOption()) {
                            map.put(paraB1, filterPanelBean.getLOption());
                        }
                        if (null != filterPanelBean.getLCount()) {
                            map.put(paraBN1, filterPanelBean.getLCount());
                        }
                    } else if (i == 2) {
                        if (null != filterPanelBean.getPOption()) {
                            map.put(paraP2, filterPanelBean.getPOption());
                        }
                        if (null != filterPanelBean.getPCount()) {
                            map.put(paraPN2, filterPanelBean.getPCount());
                        }
                        if (null != filterPanelBean.getLOption()) {
                            map.put(paraB2, filterPanelBean.getLOption());
                        }
                        if (null != filterPanelBean.getLCount()) {
                            map.put(paraBN2, filterPanelBean.getLCount());
                        }
                    } else if (i == 3) {
                        if (null != filterPanelBean.getPOption()) {
                            map.put(paraP3, filterPanelBean.getPOption());
                        }
                        if (null != filterPanelBean.getPCount()) {
                            map.put(paraPN3, filterPanelBean.getPCount());
                        }
                        if (null != filterPanelBean.getLOption()) {
                            map.put(paraB3, filterPanelBean.getLOption());
                        }
                        if (null != filterPanelBean.getLCount()) {
                            map.put(paraBN3, filterPanelBean.getLCount());
                        }
                    } else if (i == 4) {
                        if (null != filterPanelBean.getPOption()) {
                            map.put(paraP4, filterPanelBean.getPOption());
                        }
                        if (null != filterPanelBean.getPCount()) {
                            map.put(paraPN4, filterPanelBean.getPCount());
                        }
                        if (null != filterPanelBean.getLOption()) {
                            map.put(paraB4, filterPanelBean.getLOption());
                        }
                        if (null != filterPanelBean.getLCount()) {
                            map.put(paraBN4, filterPanelBean.getLCount());
                        }
                    }
                    i++;
                }
            }
        }

        //（袋式/板式）初、终阻力求和
        double initialPDB = BaseDataUtil.trans2Double(String.valueOf(noChangeMap.get(initialPD + "B")));//袋式初阻力
        double initialPDP = BaseDataUtil.trans2Double(String.valueOf(noChangeMap.get(initialPD + "P")));//板式初阻力

        double finalPDB = BaseDataUtil.trans2Double(String.valueOf(noChangeMap.get(finalPD + "B")));//袋式终阻力
        double finalPDP = BaseDataUtil.trans2Double(String.valueOf(noChangeMap.get(finalPD + "P")));//板式终阻力

        map.put(initialPD, UtilityConstant.SYS_BLANK + (initialPDB + initialPDP));
        map.put(finalPD, UtilityConstant.SYS_BLANK + (finalPDB + finalPDP));

        //袋式压差计
        Object pressureGuageB = noChangeMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEB);
        String valuePGB = BaseDataUtil.constraintString(pressureGuageB);
        //板式压差计
        Object pressureGuageP = noChangeMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEP);
        String valuePGP = BaseDataUtil.constraintString(pressureGuageP);
        String attachment = "";
        String pressureguage = "";
        String pressureguage1 = "";
        if (EmptyUtil.isNotEmpty(pressureGuageP)) {
            if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEP_PDS.equals(valuePGP)) {
                pressureguage = I18NBundle.getString("pressureDifferenceSwitch", language);
            } else if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEP_PDPG.equals(valuePGP)) {
                pressureguage = I18NBundle.getString("pointerPressureDifferentialGauge", language);
            } else if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEP_WO.equals(valuePGP)) {
                pressureguage = UtilityConstant.SYS_BLANK;
            }
            attachment += pressureguage + (UtilityConstant.SYS_BLANK.equals(pressureguage) ? UtilityConstant.SYS_BLANK
                    : (CommonConstant.SYS_PUNCTUATION_BRACKET_OPEN
                    + I18NBundle.getString("moon_intl_str_1919", language)
                    + CommonConstant.SYS_PUNCTUATION_BRACKET_CLOSE));
        }
        if (EmptyUtil.isNotEmpty(pressureGuageB)) {
            if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEB_PDS.equals(valuePGB)) {
                pressureguage1 = I18NBundle.getString("pressureDifferenceSwitch", language);
            } else if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEB_PDPG.equals(valuePGB)) {
                pressureguage1 = I18NBundle.getString("pointerPressureDifferentialGauge", language);
            } else if (UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEB_WO.equals(valuePGB)) {
                pressureguage1 = UtilityConstant.SYS_BLANK;
            }
            attachment += pressureguage1
                    + (UtilityConstant.SYS_BLANK.equals(pressureguage1) ? UtilityConstant.SYS_BLANK
                    : (CommonConstant.SYS_PUNCTUATION_BRACKET_OPEN + I18NBundle.getString("bag_type", language)
                    + CommonConstant.SYS_PUNCTUATION_BRACKET_CLOSE));
        }
        map.put(UtilityConstant.METASEXON_COMBINEDFILTER_APPENDIX, attachment);
        
        //filter velocity=风量/截面积
        String airDirectionStr = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_AIRDIRECTION));
        String airVolume=map.get(UtilityConstant.METAHU_AIRVOLUME);
//        if (airDirectionStr.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {
//        	airVolume = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METAHU_EAIRVOLUME));
//        } else {
//        	airVolume = BaseDataUtil.constraintString(getSupplyAirVolume(noChangeMap));
//        }
        double airVolumeDB = BaseDataUtil.stringConversionDouble(airVolume);
//        String medialoading = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_FILTER_MEDIALOADING));
        SectionArea sectionArea = AhuMetadata.findOne(SectionArea.class, noChangeMap.get(UtilityConstant.METAHU_SERIAL));
        double area = sectionArea.getC();//综合过滤段面积
        double velocity = airVolumeDB/3600/area;
        map.put("meta.section.combinedFilter.filterVelocity", BaseDataUtil.doubleToString(velocity, 2));
        
        //yearly_energy_comsumption
        String filterEfficiencyL = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_LMATERIALE));
        String filterEfficiencyR = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_RMATERIALE));
        String yearlyEnergyComsumption;
		double yearlyEnergyComsumptionDB = 0f;
        List<String> efficiencyList = new ArrayList<String>();
        efficiencyList.add(filterEfficiencyL);
        efficiencyList.add(filterEfficiencyR);
        int time = 0;
        for(String filterEfficiency:efficiencyList) {
			if ("G3".equals(filterEfficiency) && time == 0) {
				yearlyEnergyComsumption = "6609";
			} else if ("G3".equals(filterEfficiency) && time == 1) {
				yearlyEnergyComsumption = "6685";
			} else if ("G4".equals(filterEfficiency) && time == 1) {
				yearlyEnergyComsumption = "6685";
			} else if ("G4".equals(filterEfficiency) && time == 0) {
				yearlyEnergyComsumption = "6609";
			} else if ("M5".equals(filterEfficiency) && time == 1) {
				yearlyEnergyComsumption = "1192.68";
			} else if ("M6".equals(filterEfficiency) && time == 1) {
				yearlyEnergyComsumption = "2767.87";
			} else if ("F7".equals(filterEfficiency) && time == 1) {
				yearlyEnergyComsumption = "2516.31";
			} else if ("F7G".equals(filterEfficiency) && time == 1) {
				yearlyEnergyComsumption = "997.41";
			} else if ("F8".equals(filterEfficiency) && time == 1) {
				yearlyEnergyComsumption = "2852.98";
			} else if ("F8G".equals(filterEfficiency) && time == 1) {
				yearlyEnergyComsumption = "2630.41";
			} else if ("F9".equals(filterEfficiency) && time == 1) {
				yearlyEnergyComsumption = "3149.57";
			} else if ("F9G".equals(filterEfficiency) && time == 1) {
				yearlyEnergyComsumption = "3637.40";
			} else {
				yearlyEnergyComsumption = "0";
			}
            yearlyEnergyComsumptionDB += BaseDataUtil.stringConversionDouble(yearlyEnergyComsumption);//板式+袋式
            time++;
        }
        
        map.put("meta.section.combinedFilter.yearlyEnergyComsumption", BaseDataUtil.doubleToString(yearlyEnergyComsumptionDB, 2));
        
        return map;
    }

    /**
     * 封装奥雅纳湿膜加湿报告数据
     *
     * @param map
     * @param language
     * @return
     */
    public static Map<String, String> getARUPPFWetFilmHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        String HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_SHUMIDIFICATIONQ));
        if (EmptyUtil.isEmpty(HumidificationQ)) {
            HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_WHUMIDIFICATIONQ));
        }
        if (EmptyUtil.isNotEmpty(HumidificationQ)) {
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_METHOD, getIntlString(WET_FILM_HUMIDIFICATION_SECTION));//湿膜加湿
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_HUMIDIFICATIONQ, HumidificationQ);//加湿量
        }

        return map;
    }

    /**
     * 封装奥雅纳高压喷雾加湿段
     *
     * @param map
     * @param language
     * @return
     */
    public static Map<String, String> getARUPPFSprayHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        String HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_SHUMIDIFICATIONQ));
        if (EmptyUtil.isEmpty(HumidificationQ)) {
            HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_WHUMIDIFICATIONQ));
        }
        if (EmptyUtil.isNotEmpty(HumidificationQ)) {
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_METHOD, getIntlString(HIGH_PRESSURE_SPRAY));//高压喷雾加湿
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_HUMIDIFICATIONQ, HumidificationQ);//加湿量
        }

        return map;
    }

    /**
     * 封装奥雅纳干蒸加湿
     *
     * @param map
     * @param language
     * @return
     */
    public static Map<String, String> getARUPPFSteamHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        String HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_HUMIDIFICATIONQ));
        if (EmptyUtil.isEmpty(HumidificationQ)) {
            HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_SHUMIDIFICATIONQ));
        } else {
            HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_WHUMIDIFICATIONQ));
        }

        if (EmptyUtil.isNotEmpty(HumidificationQ)) {
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_METHOD, getIntlString(DRY_STEAM_HUMIDIFICATION_SECTION));//干蒸加湿
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_HUMIDIFICATIONQ, HumidificationQ);//加湿量
        }

        return map;
    }

    /**
     * 封装奥雅纳电极加湿
     *
     * @param map
     * @param language
     * @return
     */
    public static Map<String, String> getARUPPFElectrodeHumidifier(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        String HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_SHUMIDIFICATIONQ));
        if (EmptyUtil.isEmpty(HumidificationQ)) {
            HumidificationQ = String.valueOf(noChangeMap.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WHUMIDIFICATIONQ));
        }

        if (EmptyUtil.isNotEmpty(HumidificationQ)) {
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_METHOD, getIntlString(ELECTRODE_HUMIDIFICATION_SECTION));//干蒸加湿
            map.put(UtilityConstant.METASEXON_HUMIDIFIER_HUMIDIFICATIONQ, HumidificationQ);//加湿量
        }

        return map;
    }

    // 封装混合段报告数据
    public static Map<String, String> getMix(Map<String, String> noChangeMap, Map<String, String> map, LanguageEnum language) {
        map = getAirVolume(CommonConstant.METASEXON_MIX_PREFIX,noChangeMap, map);// 计算风量
        // 封装混合形式
        String returnBack = UtilityConstant.METASEXON_MIX_RETURNBACK;
        String returnButtom = UtilityConstant.METASEXON_MIX_RETURNBUTTOM;
        String returnLeft = UtilityConstant.METASEXON_MIX_RETURNLEFT;
        String returnRight = UtilityConstant.METASEXON_MIX_RETURNRIGHT;
        String returnTop = UtilityConstant.METASEXON_MIX_RETURNTOP;
        String fixRepairLamp = UtilityConstant.METASEXON_MIX_FIXREPAIRLAMP;
        String uvLamp = UtilityConstant.METASEXON_MIX_UVLAMP;
        String style = UtilityConstant.METASEXON_MIX_STYLE;
        String doorO = UtilityConstant.METASEXON_MIX_DOORO;// 开门情况 有检修灯的时候
        String serial = UtilityConstant.METAHU_SERIAL;// 机组型号
        String serialValue = UtilityConstant.SYS_BLANK;
        if (noChangeMap.containsKey(serial)) {
            serialValue = BaseDataUtil.constraintString(noChangeMap.get(serial));
        }
        int i = 0;
        if (map.containsKey(returnBack)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnBack));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(style, getIntlString(BACK_RETURN_AIR));
                i++;
            } else {
                map.remove(returnBack);
            }
        }
        if (map.containsKey(returnButtom)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnButtom));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(style, getIntlString(DOWN_RETURN_AIR));
                i++;
            } else {
                map.remove(returnButtom);
            }
        }
        if (map.containsKey(returnLeft)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnLeft));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(style, getIntlString(LEFT_RETURN_AIR));
                i++;
            } else {
                map.remove(returnLeft);
            }
        }
        if (map.containsKey(returnRight)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnRight));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(style, getIntlString(RIGHT_RETURN_AIR));
                i++;
            } else {
                map.remove(returnRight);
            }
        }
        if (map.containsKey(returnTop)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(returnTop));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(style, getIntlString(UPPER_RETURN_AIR));
                i++;
            } else {
                map.remove(returnTop);
            }
        }
        if (i >= 2) {
            map.put(style, getIntlString(MIX_RETURN_AIR));
        }

        if (map.containsKey(fixRepairLamp)) {// 是否安装检修灯
            String value = BaseDataUtil.constraintString(noChangeMap.get(fixRepairLamp));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
                map.put(fixRepairLamp, getIntlString(INSTALL_FIX_REPAIR_LAMP));
            } else {
                map.put(fixRepairLamp, UtilityConstant.SYS_BLANK);
            }
        }

        // 紫外线杀菌灯
        if (map.containsKey(uvLamp)) {
            String value = BaseDataUtil.constraintString(noChangeMap.get(uvLamp));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
            	String uv=noChangeMap.get(UtilityConstant.METASEXON_MIX_UVLAMPSERIAL);
            	if(!"none".equalsIgnoreCase(uv)) {
            	    //生成报告时候实时读取检修等规格，保持和页面一致，以及保证更新历史错误数据
                    SUVCLight suvclight = AhuMetadata.findOne(SUVCLight.class, serialValue);
                    String uvVal = "";
                    if (null != suvclight) {
                        uvVal = suvclight.getType() + "*" + suvclight.getQuan();
                    }
            		map.put(uvLamp, getIntlString(UV_LIGHT_CUVCA).concat(UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN).concat(uvVal.concat(UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE)));

            	}else {
                    map.put(uvLamp, getIntlString(UV_LIGHT_CUVCA));
            	}
                i++;
            }else {
            	map.put(uvLamp, "");
            }
        }
        return map;
    }

    public static Map<String, String> getAirVolume(String sectionKey,Map<String, String> noChangeMap, Map<String, String> map) {
        String airVolume = UtilityConstant.METAHU_AIRVOLUME;// 报表中通用风量标识
        String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向标识
        String eairvolume = UtilityConstant.METAHU_EAIRVOLUME;
        String sairvolume = UtilityConstant.METAHU_SAIRVOLUME;
        String velocity = UtilityConstant.METAHU_VELOCITY;
        String svelocity = UtilityConstant.METAHU_SVELOCITY;
        String evelocity = UtilityConstant.METAHU_EVELOCITY;
        if (map.containsKey(airDirection)) {
            String airDirectionStr = noChangeMap.get(airDirection).toString();
            if (airDirectionStr.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {
            	Object value = getReturnAirVolume(sectionKey,noChangeMap);
                map.put(airVolume, value.toString());
                if (map.containsKey(evelocity)) {
                    map.put(velocity, noChangeMap.get(evelocity));
                }
            } else {
                Object value = getSupplyAirVolume(sectionKey,noChangeMap);
                map.put(airVolume, value.toString());
                if (map.containsKey(svelocity)) {
                    map.put(velocity, noChangeMap.get(svelocity));
                }
            }
        }
        return map;
    }

    private static int getSeriesByFanModel(String fanModel) {
        if (fanModel.contains(UtilityConstant.SYS_MAP_FAN_FANMODEL_SYW)) {
            return BaseDataUtil.stringConversionInteger(NumberUtil.onlyNumbers(fanModel));
        }
        return BaseDataUtil.stringConversionInteger(NumberUtil.onlyNumbers(fanModel));
    }

    public static Map<String, String> getReportMap(ReportData reportData, SectionTypeEnum sectionType) {
        Map<String, String> rstMap = new HashMap<String, String>();

        for (Unit unit : reportData.getUnitList()) {
            List<PartPO> partList = new ArrayList<>();
            for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
                if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
                    partList.add(e.getValue());
                }
            }
            Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
            for (PartPO partPO : partList) {
                Part part = partPO.getCurrentPart();
                String key = part.getSectionKey();
                if (sectionType.equals(SectionTypeEnum.getSectionTypeFromId(key))) {
                    rstMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                }
            }

        }

        return rstMap;
    }
    
    
	private static Integer getSupplyAirVolume(String sectionKey,Map<String, String> allMap) {
		Object appendAirVolume=allMap.get(sectionKey.concat(UtilityConstant.METACOMMON_APPENDAIRVOLUME));
		int airVolume=BaseDataUtil
				.stringConversionInteger(String.valueOf(allMap.get(UtilityConstant.METAHU_SAIRVOLUME)));
		
		if(EmptyUtil.isEmpty(appendAirVolume)) {
			return airVolume;
		}
		
		return AirVolumeUtil.packageSAirVolumeToInt(airVolume,BaseDataUtil
				.stringConversionInteger(String.valueOf(appendAirVolume)));
		
	}
	
	private static Integer getReturnAirVolume(String sectionKey,Map<String, String> allMap) {
		Object appendAirVolume=allMap.get(sectionKey.concat(UtilityConstant.METACOMMON_APPENDAIRVOLUME));
		int airVolume=BaseDataUtil
				.stringConversionInteger(String.valueOf(allMap.get(UtilityConstant.METAHU_EAIRVOLUME)));
		
		if(EmptyUtil.isEmpty(appendAirVolume)) {
			return airVolume;
		}
		
		return AirVolumeUtil.packageSAirVolumeToInt(airVolume,BaseDataUtil
				.stringConversionInteger(String.valueOf(appendAirVolume)));
		
	}

}

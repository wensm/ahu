package com.carrier.ahu.wpd;

import static com.carrier.ahu.constant.UtilityConstant.PI;
import static com.carrier.ahu.wpd.WaterDropDetail.Coeffs.*;

import cn.hutool.core.util.NumberUtil;
import com.carrier.ahu.length.util.SystemCountUtil;



/**
 * Converted from the VB Macro in W_PD incl Header_修正_Sharon.xlsm file.
 * 
 * You may call {@link #run4ClicksInSequence} directly and will execute all four buttons in sequence and calculate the
 * result.
 * 
 * Created by Braden Zhou on 2019/07/12.
 */
public class WaterDropMacro {

    private static final double IN_WATER_TEMP = 65.02;
    private static final double OUT_WATER_TEMP = 45.13;
    // private static final double MAIN_TUBE_RISE_HEIGHT = 0.046;
    // private static final double DISTRIBUTE_TUBE_RISE_HEIGHT = 0.01;
    // private static final double COIL_TUBE_RISE_HEIGHT = 0.01;
    //
    // private static double MAIN_TUBE_DROP_COEFF = 1.0;
    // private static double DISTRIBUTE_DROP_COEFF = 1.0;

    // button1: DP (header, main, on-way)
    @SuppressWarnings("unused")
    private static double commDPHeader(WaterDropDetail detail) {
        // Main header on-way pressure drop
        double Re;
        double Roughness;
        double Dh;
        double u;
        double LenH;
        double density;
        double mu;
        double Lendis;
        double Ddis;
        int NoCir;
        double tin;
        double tout;
        double f, f1;
        double DP, DP1, DP2, DP3;

        NoCir = detail.getCoilTube().getCir_no(); // Sheet1.Cells(7, 10)

        tin = IN_WATER_TEMP; // Sheet1.Cells(9, 25)
        tout = OUT_WATER_TEMP; // Sheet1.Cells(10, 25)
        Dh = detail.getMainTube().getT_id(); // Sheet1.Cells(7, 5)
        u = detail.getMainTube().getW_vel(); // Sheet1.Cells(7, 6)
        LenH = detail.getMainTube().getT_len(); // Sheet1.Cells(7, 4)
        mu = 0.01775 / (1 + 0.0337 * tin + 0.000221 * Math.pow(tin, 2)) / 100 / 100;
        Re = u * Dh / mu / 1000;
        Roughness = MAIN_TUBE_RISE_HEIGHT; // Sheet1.Cells(7, 29)
        density = -0.085866 * Math.pow(tin, 1.4802525) + 0.1655361 * Math.pow(tin, 1.1737852) + 999.81127;

        // inlet header, horizontal section
        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow((64 / Re), 0.5);
            do {
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10));
                if ((Math.abs(f - f1) > 0.0001)) {
                    f = f - (f - f1) / 10;
                } else {
                    break;
                }
            } while (true);
            f = Math.pow(f, 2);
        }
        DP1 = 0.3 * LenH / Dh * f * Math.pow(u, 2) / 2 * density;

        // inlet header, vertical section
        DP2 = 0;
        for (int i = 1; i <= NoCir; i++) {
            Re = u * Dh / mu * (NoCir - i + 1) / NoCir / 1000;
            if (Re <= 1668) {
                f = 64 / Re;
            } else if (Re < 5000 && Re > 1668) {
                f = 0.316 / Math.pow(Re, 0.25);
            } else {
                f = Math.pow((64 / Re), 0.5);
                do {
                    f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10));
                    if (Math.abs(f - f1) > 0.0001) {
                        f = f - (f - f1) / 10;
                    } else {
                        break;
                    }
                } while (true);
                f = Math.pow(f, 2);
            }

            DP2 = 0.7 * LenH / Dh * f * Math.pow((u * (NoCir - i + 1) / NoCir), 2) / 2 / NoCir * density + DP2;
        }

        // outlet header, horizontal section
        mu = 0.01775 / (1 + 0.0337 * tout + 0.000221 * Math.pow(tout, 2)) / 100 / 100;
        density = -0.085866 * Math.pow(tout, 1.4802525) + 0.1655361 * Math.pow(tout, 1.1737852) + 999.81127;
        Re = u * Dh / mu / 1000;
        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow((64 / Re), 0.5);
            do {
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10));
                if (Math.abs(f - f1) > 0.0001) {
                    f = f - (f - f1) / 30;
                } else {
                    break;
                }
            } while (true);

            f = Math.pow(f, 2);
        }
        DP1 = 0.3 * LenH / Dh * f * Math.pow(u, 2) / 2 * density + DP1;

        // outlet header, vertical section
        DP3 = 0;
        for (int i = 1; i <= NoCir; i++) {
            Re = u * Dh / mu * (NoCir - i + 1) / NoCir / 1000;
            if (Re <= 1668) {
                f = 64 / Re;
            } else if (Re < 5000 && Re > 1668) {
                f = 0.316 / Math.pow(Re, 0.25);
            } else {
                f = Math.pow((64 / Re), 0.5);
                do {
                    f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Dh + 18.7 / (Re * f)) / Math.log(10));
                    if (Math.abs(f - f1) > 0.0001) {
                        f = f - (f - f1) / 10;
                    } else {
                        break;
                    }
                } while (true);
                f = Math.pow(f, 2);
            }
            DP3 = 0.7 * LenH / Dh * f * Math.pow((u * (NoCir - i + 1) / NoCir), 2) / 2 / NoCir * density + DP3;
        }

        DP = DP1 + DP2 + DP3;

        MAIN_TUBE_DROP_COEFF = DP; // Sheet1.Cells(8, 32)
        return DP;
    }

    // button2: DP (header, on-way)
    @SuppressWarnings("unused")
    private static double commFrictionfDis(WaterDropDetail detail) {
        // Header distribution pipe on-way pressure drop

        double Re;
        double Roughness;
        double Ddis;
        double tin;
        double tout;
        double NoCir;
        double Dh, u, Lendis, mu, density, f, f1;
        double DP, DP1, DP2, DP3;

        NoCir = detail.getCoilTube().getCir_no(); // Sheet1.Cells(7, 10)

        tin = IN_WATER_TEMP; // Sheet1.Cells(9, 25)
        tout = OUT_WATER_TEMP; // Sheet1.Cells(10, 25)
        Ddis = detail.getDistributeTube().getT_id(); // Sheet1.Cells(7, 8)
        u = detail.getDistributeTube().getW_vel();// Sheet1.Cells(7, 9)
        Lendis = detail.getDistributeTube().getT_len(); // Sheet1.Cells(7, 7)
        Roughness = DISTRIBUTE_TUBE_RISE_HEIGHT; // Sheet1.Cells(7, 34)

        // inlet distribution tube
        mu = 0.01775 / (1 + 0.0337 * tin + 0.000221 * Math.pow(tin, 2)) / 100 / 100;

        // Correlation regressed based on ASHRAE handbook data
        density = -0.085866 * Math.pow(tin, 1.4802525) + 0.1655361 * Math.pow(tin, 1.1737852) + 999.81127;

        Re = u * Ddis / mu / 1000;

        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow((64 / Re), 0.5);
            do {
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Ddis + 18.7 / (Re * f)) / Math.log(10));
                if (Math.abs(f - f1) > 0.0001) {
                    f = f - (f - f1) / 10;
                } else {
                    break;
                }
            } while (true);
            f = Math.pow(f, 2);
        }
        DP1 = f * Lendis / Ddis * Math.pow(u, 2) / 2 * density;

        // outlet distribution tube
        mu = 0.01775 / (1 + 0.0337 * tout + 0.000221 * Math.pow(tout, 2)) / 100 / 100;
        density = -0.085866 * Math.pow(tout, 1.4802525) + 0.1655361 * Math.pow(tout, 1.1737852) + 999.81127;

        Re = u * Ddis / mu / 1000;

        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow((64 / Re), 0.5);
            do {
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / Ddis + 18.7 / (Re * f)) / Math.log(10));
                if (Math.abs(f - f1) > 0.0001) {
                    f = f - (f - f1) / 10;
                } else {
                    break;
                }
            } while (true);

            f = Math.pow(f, 2);
        }
        DP2 = f * Lendis / Ddis * Math.pow(u, 2) / 2 * density;

        DP = DP1 + DP2;

        // Sheet1.Cells(7, 37)
        DISTRIBUTE_TUBE_DROP_COEFF = DP;
        return DP;
    }

    // button3: Coil (local)
    @SuppressWarnings("unused")
    private static double commCoilLocal(WaterDropDetail detail) {

        // Coil U-bend pressure drop, from CoilPC
        double Re;
        // double Roughness;
        double Dcoil;
        double dt;
        // double Len_tube;
        int Cir_No;
        double Utotal;
        double mu; // 运动粘滞系数
        double tm;
        int Row_No;
        int Tube_No;

        double u, density, f;
        double d_id = 0, d_pin = 0, d_return = 0, pitch = 0, Len_pin, Len_bend;
        double u_pin, Ratio, alpha, alpha84, Ratio_axi;
        double DP, DP1, DP2;

        dt = detail.getT_od(); // Sheet1.Cells(7, 3)
        tm = detail.getWaterTemp(); // Sheet1.Cells(7, 25)
        Cir_No = detail.getCoilTube().getCir_no(); // Sheet1.Cells(7, 10)
        Row_No = detail.getCoilTube().getRow(); // Sheet1.Cells(7, 14)
        Tube_No = detail.getCoilTube().getT_no(); // Sheet1.Cells(7, 13)

        // Correlation regressed based on ASHRAE handbook data
        density = -0.085866 * Math.pow(tm, 1.4802525) + 0.1655361 * Math.pow(tm, 1.1737852) + 999.81127;
        mu = 0.01775 / (1 + 0.0337 * tm + 0.000221 * Math.pow(tm, 2)) / 100 / 100; // 运动粘滞系数
        Utotal = detail.getW_flow(); // Sheet1.Cells(7, 17)
        if (dt == 9.52) {
            d_id = 10.058 - 2 * 0.3175; // tube data from cpc file
            d_pin = 9.43;
            d_return = 8.4;
            pitch = 25.4;
            Len_pin = PI * pitch / 2 * roundHalfEven(Tube_No / Cir_No * Row_No / 2.0);
            Len_bend = PI * pitch / 2 * roundHalfEven(Tube_No / Cir_No * Row_No / 2.0 - 1);
        } else if (dt == 12.7) {
            d_id = 13.208 - 2 * 0.4064; // tube data from cpc file
            d_pin = 12.4104;
            d_return = 11.1506;
            pitch = 31.75;
            Len_pin = PI * pitch / 2 * roundHalfEven(Tube_No / Cir_No * Row_No / 2.0);
            Len_bend = PI * pitch / 2 * roundHalfEven(Tube_No / Cir_No * Row_No / 2.0 - 1);
        }
        mu = WaterDropDetail.Coeffs.NINE_DEGREE_VISCOSITY_COEFF; // Sheet1.Cells(7, 27)
        u = Utotal / (PI * Math.pow(d_id, 2)) * 4 * 1000 * 1000 / 3600 / Cir_No; // flow velocity inside coil tube

        // pin section
        u_pin = u * Math.pow(d_id, 2) / Math.pow(d_pin, 2); // flow velocity inside hair pin
        Re = u_pin * d_pin / mu / 1000;
        Ratio = pitch / d_pin;

        alpha = 1 + 116 * Math.pow(Ratio, (-4.52));
        alpha84 = Math.pow(Ratio, 0.84) * alpha * 0.00241 * 180;
        Ratio_axi = Math.pow(d_pin, 2) / Math.pow(d_id, 2);
        f = alpha84 * Math.pow(Re, (-0.17));

        // pressure drop in hair pin
        DP1 = f * Math.pow(u_pin, 2) / 2 * density * roundHalfEven(Tube_No / Cir_No * Row_No / 2.0);

        double u_return, f_con, f_exp;
        // Return section
        u_return = u * Math.pow(d_id, 2) / Math.pow(d_return, 2); // flow velocity inside U bend
        Re = u_return * d_return / mu / 1000;
        Ratio = pitch / d_return;

        alpha = 1 + 116 * Math.pow(Ratio, (-4.52));
        alpha84 = Math.pow(Ratio, 0.84) * alpha * 0.00241 * 180;
        Ratio_axi = Math.pow(d_return, 2) / Math.pow(d_id, 2);
        f = alpha84 * Math.pow(Re, (-0.17));
        f_con = (0.1666667 * Ratio_axi - 0.6666667) * Ratio_axi + 0.5;
        f_exp = Math.pow((1 - Ratio_axi), 2);

        // pressure drop in U bend
        DP2 = (f + f_con + f_exp) * Math.pow(u_return, 2) / 2 * density
                * roundHalfEven(Tube_No / Cir_No * Row_No / 2.0 - 1);

        DP = DP1 + DP2;

        detail.getCoilCalc().setLocal(DP); // Sheet1.Cells(7, 21)

        return DP;
    }

    // butto4: DP (coil, on-way)
    @SuppressWarnings("unused")
    private static double commFrictionfCoil(WaterDropDetail detail) {
        // Coil on-way pressure drop, from ASHRAE handbook
        double Re;
        double Roughness;
        double Dcoil;
        double dt;
        double Len_tube;
        int Cir_No;
        double Utotal;
        double mu;
        double tm;
        int Row_No;
        int Tube_No;

        double u, density, f, f1;
        double d_id = 0, d_pin = 0, d_return = 0, pitch = 0, Len_pin = 0, Len_bend = 0;
        double u_pin, u_return, Ratio, alpha, alpha84, Ratio_axi;
        double DP, DP1, DP2, DP3;

        dt = detail.getT_od(); // Sheet1.Cells(7, 3)
        tm = detail.getWaterTemp(); // Sheet1.Cells(7, 25)
        Cir_No = detail.getCoilTube().getCir_no(); // Sheet1.Cells(7, 10)
        Row_No = detail.getCoilTube().getRow(); // Sheet1.Cells(7, 14)
        Tube_No = detail.getCoilTube().getT_no(); // Sheet1.Cells(7, 13)

        // Correlation regressed based on ASHRAE handbook data
        density = -0.085866 * Math.pow(tm, 1.4802525) + 0.1655361 * Math.pow(tm, 1.1737852) + 999.81127;
        Utotal = detail.getW_flow(); // Sheet1.Cells(7, 17)
        if (dt == 9.52) {
            d_id = 10.058 - 2 * 0.317; // tube data from cpc file
            d_pin = 9.43;
            d_return = 8.4;
            pitch = 25.4;
            Len_pin = PI * pitch / 2 * roundHalfEven(Tube_No / Cir_No * Row_No / 2.0);
            Len_bend = PI * pitch / 2 * roundHalfEven(Tube_No / Cir_No * Row_No / 2.0 - 1);

        } else if (dt == 12.7) {
            d_id = 13.208 - 2 * 0.4064; // tube data from cpc file
            d_pin = 12.4104;
            d_return = 11.1506;
            pitch = 31.75;
            Len_pin = PI * pitch / 2 * roundHalfEven(Tube_No / Cir_No * Row_No / 2.0);
            Len_bend = PI * pitch / 2 * roundHalfEven(Tube_No / Cir_No * Row_No / 2.0 - 1);
        }
        Len_tube = detail.getCoilTube().getT_len() * Tube_No / Cir_No * Row_No; // Sheet1.Cells(7, 11)
        mu = WaterDropDetail.Coeffs.NINE_DEGREE_VISCOSITY_COEFF; // Sheet1.Cells(7, 27)

        // straight tube section

        u = Utotal / (PI * Math.pow(d_id, 2)) * 4 * 1000 * 1000 / 3600 / Cir_No;
        Re = u * d_id / mu / 1000;
        Roughness = COIL_TUBE_RISE_HEIGHT; // Sheet1.Cells(7, 39);

        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow((64 / Re), 0.5);
            do {
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / d_id + 18.7 / (Re * f)) / Math.log(10));
                if (Math.abs(f - f1) > 0.0001) {
                    f = f - (f - f1) / 10;
                } else {
                    break;
                }
            } while (true);
            f = Math.pow(f, 2);
        }

        DP1 = f * Len_tube / d_id * Math.pow(u, 2) / 2 * density;

        // pin section

        u_pin = u * Math.pow(d_id, 2) / Math.pow(d_pin, 2);
        Re = u_pin * d_pin / mu / 1000;
        Roughness = COIL_TUBE_RISE_HEIGHT; // Sheet1.Cells(7, 39);

        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow((64 / Re), 0.5);
            do {
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / d_pin + 18.7 / (Re * f)) / Math.log(10));
                if (Math.abs(f - f1) > 0.0001) {
                    f = f - (f - f1) / 10;
                } else {
                    break;
                }
            } while (true);
            f = Math.pow(f, 2);
        }

        DP2 = f * Len_pin / d_pin * Math.pow(u_pin, 2) / 2 * density;

        // U bend section

        u_return = u * Math.pow(d_id, 2) / Math.pow(d_return, 2);
        Re = u_return * d_return / mu / 1000;
        Roughness = COIL_TUBE_RISE_HEIGHT; // Sheet1.Cells(7, 39);

        if (Re <= 1668) {
            f = 64 / Re;
        } else if (Re < 5000 && Re > 1668) {
            f = 0.316 / Math.pow(Re, 0.25);
        } else {
            f = Math.pow((64 / Re), 0.5);
            do {
                f1 = 1 / (1.74 - 2 * Math.log(2 * Roughness / d_return + 18.7 / (Re * f)) / Math.log(10));
                if (Math.abs(f - f1) > 0.0001) {
                    f = f - (f - f1) / 10;
                } else {
                    break;
                }
            } while (true);
            f = Math.pow(f, 2);
        }

        DP3 = f * Len_bend / d_return * Math.pow(u_return, 2) / 2 * density;

        DP = DP1 + DP2 + DP3;

        // Sheet1.Cells(7, 20)
        detail.getCoilCalc().setOn_way(DP);

        return DP;
    }

    /**
     * Call this method to calculate the water pressure drop data.
     * 
     * @param model 机组型号
     * @param t_od 管外径
     * @param t_id 联箱主管内径
     * @param cir_no 水循环回路数
     * @param t_len 盘管长度
     * @param t_no 一排盘管的管数
     * @param row 盘管排数
     * @param w_flow 机组水流量
     * @param waterTemp "水温
     * @return
     */
    public static WaterDropDetail run4ClicksInSequence(String model, double t_od, int t_id, int cir_no, int t_len,
            int t_no, int row, double w_flow, double waterTemp) {
        WaterDropDetail detail = new WaterDropDetail();
        detail.setArguments(model, t_od, t_id, cir_no, t_len, t_no, row, w_flow, waterTemp);
        detail.calculateFormula();
        commDPHeader(detail);
        reconcile(detail);

        commFrictionfDis(detail);
        reconcile(detail);

        commCoilLocal(detail);
        reconcile(detail);

        commFrictionfCoil(detail);
        reconcile(detail);
        return detail;
    }

    // According to "W_PD incl Header_Zheng W.xls"
    public static WaterDropDetail calculate(String model, double t_od, int t_id, int cir_no, int t_len, int t_no,
            int row, double w_flow, double waterTemp) {
        WaterDropDetail detail = new WaterDropDetail();
        detail.setArguments(model, t_od, t_id, cir_no, t_len, t_no, row, w_flow, waterTemp);
        detail.calculateFormula();
        reconcile2(detail);
        filter(model,detail);
        return detail;
    }
    
    /**
     * -在郑炜公式计算值的基础上，另加以下水阻系数：
         计算值≤1kPa， 系数为4
        计算值≤2 kPa， 系数为3
        计算值≤3 kPa， 系数为2
        计算值≤5 kPa， 系数为1.5
       计算值≤10 kPa， 系数为1.2
       计算值＞10 kPa， 风量≤20000，系数为1 OR 计算值＞10， 机组规格≤1420，系数为1
       计算值＞10 kPa， 风量＞20000，系数为0.95 OR  计算值＞10， 机组规格＞1420，系数为0.95

     * @param model
     * @param detail
     */
    private static void filter(String model,WaterDropDetail detail) {
    	int modelInt=SystemCountUtil.getUnitHeight(model);
    	double wpd=detail.getTotal();
    	if(wpd<=1) {
    		detail.setTotal(wpd*4.0);
    	}else if(wpd>1&&wpd<=2) {
    		detail.setTotal(wpd*3.0);
    	}else if(wpd>2&&wpd<=3) {
    		detail.setTotal(wpd*2.0);
    	}else if(wpd>3&&wpd<=5) {
    		detail.setTotal(wpd*1.5);
    	}else if(wpd>10&&modelInt>14) {
    		detail.setTotal(wpd*0.95);
    	}
    }

    private static void reconcile(WaterDropDetail detail) {
        double temp1 = 0, temp2 = 0;

        // Distribute: T_ID = IF(C7=9.52,C7-0.3*2,IF(C7=12.7,C7-0.35*2))
        double t_id = 0;
        if (detail.getT_od() == 9.52) {
            t_id = detail.getT_od() - 0.3 * 2;
        } else if (detail.getT_od() == 12.7) {
            t_id = detail.getT_od() - 0.35 * 2;
        }
        detail.getDistributeTube().setT_id(t_id);

        // Distribute: W_Vel = Q7*4/H7/H7/PI()/3600/J7*1000*1000
        double w_vel = detail.getW_flow() * 4 / t_id / t_id / Math.PI / 3600 / detail.getCoilTube().getCir_no() * 1000
                * 1000;
        detail.getDistributeTube().setW_vel(w_vel);

        // Coil: T_ID = IF(C7=9.52,C7-0.32,IF(C7=12.7,C7-0.2))
        t_id = 0;
        if (detail.getT_od() == 9.52) {
            t_id = detail.getT_od() - 0.32;
        } else if (detail.getT_od() == 12.7) {
            t_id = detail.getT_od() - 0.2;
        }
        detail.getCoilTube().setT_id(t_id);

        // Coil: W_Vel = Q7/(IF(C7=9.52, 10.058-2*0.3175,13.208-2*0.4064))^2*1000*1000/PI()/J7/3600*4
        if (detail.getT_od() == 9.52) {
            temp1 = 10.058 - 2 * 0.3175;
        } else {
            temp1 = 13.208 - 2 * 0.4064;
        }
        w_vel = detail.getW_flow() / Math.pow(temp1, 2) * 1000 * 1000 / Math.PI / detail.getCoilTube().getCir_no()
                / 3600 * 4;
        detail.getCoilTube().setW_vel(w_vel);

        // Coil: Cir_len = K7*M7*N7/J7+IF(C7=9.52,25.4,31.75)*(M7*N7/J7-1)*PI()/2
        if (detail.getT_od() == 9.52) {
            temp1 = 25.4;
        } else {
            temp1 = 31.75;
        }
        double cir_len = detail.getCoilTube().getT_len() * detail.getCoilTube().getT_no()
                * detail.getCoilTube().getRow() / detail.getCoilTube().getCir_no()
                + temp1 * (detail.getCoilTube().getT_no() * detail.getCoilTube().getRow()
                        / detail.getCoilTube().getCir_no() - 1) * Math.PI / 2;
        detail.getCoilTube().setCir_len(cir_len);

        // Header: on-way = AK7+AF8
        double on_way = MAIN_TUBE_DROP_COEFF + DISTRIBUTE_TUBE_DROP_COEFF;
        detail.getHeaderCalc().setOn_way(on_way);

        // Header: local = IF(C7=9.52,2.5,2)*2/2*Z7*F7*F7+IF(C7=9.52,5.5,5)/2*Z7*((I7+F7)/2)^2
        if (detail.getT_od() == 9.52) {
            temp1 = 2.5;
            temp2 = 5.5;
        } else {
            temp1 = 2;
            temp2 = 5;
        }
        double local = temp1 * 2 / 2 * detail.getDensity() * detail.getMainTube().getW_vel()
                * detail.getMainTube().getW_vel()
                + temp2 / 2 * detail.getDensity()
                        * Math.pow(((detail.getDistributeTube().getW_vel() + detail.getMainTube().getW_vel()) / 2), 2);
        detail.getHeaderCalc().setLocal(local);

        // Header: W_PD = (R7+S7)/1000
        double header_w_pd = (on_way + local) / 1000;
        detail.setHeader_w_pd(header_w_pd);

        // Coil: W_PD = (T7+U7)/1000
        double coil_w_pd = (detail.getCoilCalc().getOn_way() + detail.getCoilCalc().getLocal()) / 1000;
        detail.setCoil_w_pd(coil_w_pd);

        // Total = (V7+W7)
        double total = (header_w_pd + coil_w_pd);
        detail.setTotal(total);
    }

    private static void reconcile2(WaterDropDetail detail) {
        double temp1 = 0, temp2 = 0;

        // Distribute: T_ID = IF(C7=9.52,C7-0.3*2,IF(C7=12.7,C7-0.35*2))
        double t_id = 0;
        if (detail.getT_od() == 9.52) {
            t_id = detail.getT_od() - 0.3 * 2;
        } else if (detail.getT_od() == 12.7) {
            t_id = detail.getT_od() - 0.35 * 2;
        }
        detail.getDistributeTube().setT_id(t_id);

        // Distribute: W_Vel = Q7*4/H7/H7/PI()/3600/J7*1000*1000
        double w_vel = detail.getW_flow() * 4 / t_id / t_id / Math.PI / 3600 / detail.getCoilTube().getCir_no() * 1000
                * 1000;
        detail.getDistributeTube().setW_vel(w_vel);

        // Coil: T_ID = IF(C7=9.52,C7-0.32,IF(C7=12.7,C7-0.2))
        t_id = 0;
        if (detail.getT_od() == 9.52) {
            t_id = detail.getT_od() - 0.32;
        } else if (detail.getT_od() == 12.7) {
            t_id = detail.getT_od() - 0.2;
        }
        detail.getCoilTube().setT_id(t_id);

        // Coil: W_Vel = Q7/L7/L7*1000*1000/PI()/J7/3600*4
        w_vel = detail.getW_flow() / t_id / t_id * 1000 * 1000 / Math.PI / detail.getCoilTube().getCir_no() / 3600 * 4;
        detail.getCoilTube().setW_vel(w_vel);

        // Coil: Cir_len = K7*M7*N7/J7+IF(C7=9.52,25.4,31.75)*(M7*N7/J7-1)*PI()/2
        if (detail.getT_od() == 9.52) {
            temp1 = 25.4;
        } else {
            temp1 = 31.75;
        }
        double cir_len = detail.getCoilTube().getT_len() * detail.getCoilTube().getT_no()
                * detail.getCoilTube().getRow() / detail.getCoilTube().getCir_no()
                + temp1 * (detail.getCoilTube().getT_no() * detail.getCoilTube().getRow()
                        / detail.getCoilTube().getCir_no() - 1) * Math.PI / 2;
        detail.getCoilTube().setCir_len(cir_len);

        WaterDropDetail.Coeffs.calculateFormula(detail);

        // Header: on-way = AF7*F7*F7/2*D7*Z7/E7+AK7*I7*I7*Z7*G7/H7
        double on_way = MAIN_TUBE_DROP_COEFF * detail.getMainTube().getW_vel() * detail.getMainTube().getW_vel() / 2
                * detail.getMainTube().getT_len() * detail.getDensity() / detail.getMainTube().getT_id()
                + DISTRIBUTE_TUBE_DROP_COEFF * detail.getDistributeTube().getW_vel()
                        * detail.getDistributeTube().getW_vel() * detail.getDensity()
                        * detail.getDistributeTube().getT_len() / detail.getDistributeTube().getT_id();
        detail.getHeaderCalc().setOn_way(on_way);

        // Header: local = IF(C7=9.52,2.5,2)*2/2*Z7*F7*F7+IF(C7=9.52,5.5,5)/2*Z7*((I7+F7)/2)^2
        if (detail.getT_od() == 9.52) {
            temp1 = 2.5;
            temp2 = 5.5;
        } else {
            temp1 = 2;
            temp2 = 5;
        }
        double local = temp1 * 2 / 2 * detail.getDensity() * detail.getMainTube().getW_vel()
                * detail.getMainTube().getW_vel()
                + temp2 / 2 * detail.getDensity()
                        * Math.pow(((detail.getDistributeTube().getW_vel() + detail.getMainTube().getW_vel()) / 2), 2);
        detail.getHeaderCalc().setLocal(local);

        // Coil: on_way = AP7*O7*O7/2*P7/L7*Z7
        on_way = COIL_TUBE_DROP_COEFF * detail.getCoilTube().getW_vel() * detail.getCoilTube().getW_vel() / 2
                * detail.getCoilTube().getCir_len() / detail.getCoilTube().getT_id() * detail.getDensity();
        detail.getCoilCalc().setOn_way(on_way);

        // Coil: Local = (M7*N7/J7-1)*IF(C7=9.52,0.35,0.246)*2/2*O7*O7*Z7
        if (detail.getT_od() == 9.52) {
            temp1 = 0.35;
        } else {
            temp1 = 0.246;
        }
        local = (detail.getCoilTube().getT_no() * detail.getCoilTube().getRow() / detail.getCoilTube().getCir_no() - 1)
                * temp1 * 2 / 2 * detail.getCoilTube().getW_vel() * detail.getCoilTube().getW_vel()
                * detail.getDensity();
        detail.getCoilCalc().setLocal(local);

        // Header: W_PD = (R7+S7)/1000
        double header_w_pd = (detail.getHeaderCalc().getOn_way() + detail.getHeaderCalc().getLocal()) / 1000;
        detail.setHeader_w_pd(header_w_pd);

        // Coil: W_PD = IF(C7=9.52,(T7+U7)/1000/1.2,(T7+U7)/1000)
        double coil_w_pd = 0;
        if (detail.getT_od() == 9.52) {
            coil_w_pd = (detail.getCoilCalc().getOn_way() + detail.getCoilCalc().getLocal()) / 1000;
        } else {
            coil_w_pd = (detail.getCoilCalc().getOn_way() + detail.getCoilCalc().getLocal()) / 1000;
        }
        detail.setCoil_w_pd(coil_w_pd);

        // Total = (V7+W7)/1.2
        double total = header_w_pd + coil_w_pd;
        detail.setTotal(total);
    }

    private static int roundHalfEven(double number) {
        return NumberUtil.roundHalfEven(number, 0).intValue();
    }

}

package com.carrier.ahu.util.heatrecycle;

import java.util.List;

import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWPlate;
import com.carrier.ahu.metadata.entity.heatrecycle.PartWWheel;
import com.carrier.ahu.metadata.entity.heatrecycle.Rotorpara;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.unit.ListUtils;
import com.carrier.ahu.util.AirConditionBean;
import com.carrier.ahu.util.AirConditionUtils;
import com.carrier.ahu.util.EmptyUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Extract from RotorparaDao.
 * 
 * Created by Braden Zhou on 2018/05/25.
 */
@Slf4j
public class HeatRecycleUtils {

    private static final double K01 = 0.1;
    private static final double K02 = 0.11;
    private static final double K03 = 13;
    private static final double K04 = 15;
    private static final double K05 = 10;
    private static final double K06 = 4;
    private static final double K07 = 0.0004;
    private static final double K08 = 0.00005;
    private static final double K09 = 1.5;
    private static final double K10 = 0.00001506;

    public static AirParam calcOutAirParm(double inDryBulbT, double inWetBulbT, double indoorDryBulbT,
            double indoorWetBulbT, double wheelEfficiency) throws Exception {
        double outDryBulbT = BaseDataUtil
                .decimalConvert(inDryBulbT - wheelEfficiency * (inDryBulbT - indoorDryBulbT) / 100, 1);
        AirConditionBean inWetAcb = AirConditionUtils.FAirParmCalculate1(inDryBulbT, inWetBulbT);
        AirConditionBean indoorWetAcb = AirConditionUtils.FAirParmCalculate1(indoorDryBulbT, indoorWetBulbT);

        double lOutD = inWetAcb.getParamD() - wheelEfficiency * (inWetAcb.getParamD() - indoorWetAcb.getParamD()) / 100;
        AirConditionBean outAcb = AirConditionUtils.FAirParmCalculate2(outDryBulbT, lOutD);

        double outWetBulbT = BaseDataUtil.decimalConvert(outAcb.getParmTb(), 0);
        double outRelativeT = BaseDataUtil.decimalConvert(outAcb.getParmF(), 1);

        return new AirParam(outDryBulbT, outWetBulbT, outRelativeT);
    }

    public static AirParam calcExhaustAirParm(double inDryBulbT, double inWetBulbT, double indoorDryBulbT,
            double indoorWetBulbT, double wheelEfficiency, boolean isWinter) throws Exception {
        double exhaustDryBulbT = BaseDataUtil
                .decimalConvert(indoorDryBulbT - wheelEfficiency * (indoorDryBulbT - inDryBulbT) / 100, 1);

        AirConditionBean inWetAcb = AirConditionUtils.FAirParmCalculate1(inDryBulbT, inWetBulbT);
        AirConditionBean indoorWetAcb = AirConditionUtils.FAirParmCalculate1(indoorDryBulbT, indoorWetBulbT);

        double lExhaustD = inWetAcb.getParamD()
                - wheelEfficiency * (inWetAcb.getParamD() - indoorWetAcb.getParamD()) / 100;
        if (isWinter) {
            lExhaustD = indoorWetAcb.getParamD()
                    - wheelEfficiency * (indoorWetAcb.getParamD() - inWetAcb.getParamD()) / 100;
        }

        AirConditionBean outAcb = AirConditionUtils.FAirParmCalculate2(exhaustDryBulbT, lExhaustD);
        double exhaustWetBulbT = BaseDataUtil.decimalConvert(outAcb.getParmTb(), 0);
        double exhaustRelativeT = BaseDataUtil.decimalConvert(outAcb.getParmF(), 1);

        return new AirParam(exhaustDryBulbT, exhaustWetBulbT, exhaustRelativeT);
    }

    public static HeatParam calcHeatParam(double naVolume, double inDryBulbT, double inRelativeT, double outDryBulbT,
            double outRelativeT) throws Exception {
        naVolume /= 3600;

        AirConditionBean indoorAcb = AirConditionUtils.FAirParmCalculate3(inDryBulbT, inRelativeT);
        AirConditionBean outdoorAcb = AirConditionUtils.FAirParmCalculate3(outDryBulbT, outRelativeT);
        double qt = Math.abs(BaseDataUtil.decimalConvert(
                naVolume * 1.27 * 273 / (273 + inDryBulbT) * (indoorAcb.getParamI() - outdoorAcb.getParamI()), 2));

        double qs = Math.abs(BaseDataUtil
                .decimalConvert(naVolume * 1.27 * 273 / (273 + inDryBulbT) * (inDryBulbT - outDryBulbT), 2));

        // double ql = qt - qs;
        return new HeatParam(qt, qs);
    }

    @SuppressWarnings("unused")
    public static EfficientParam plateCal(double N03, double N04, double N05, String unitModel) {
        EfficientParam efficientParam = new EfficientParam();
        double N01;
        double N02;
        PartWPlate partWPlate = AhuMetadata.findOne(PartWPlate.class, unitModel);
        if (null != partWPlate) {
            N01 = partWPlate.getPlateWidth();
            N02 = partWPlate.getPlateLength();
        } else {
            return null;
        }
        double T10 = 1;
        double T01 = Double.valueOf((N01 - 2 * K03) / (K02 + K05) * 2).intValue();
        double T02 = Double.valueOf((N02 - 2 * K04) / (K06 + K01 + K02) / 2).intValue();
        double T03 = T01 * T02 * K05 * K06 / 2000000;
        double T04 = N03 / T03 / 3600;
        double T05 = K05 * K06 / (K05 + Math.pow(K05 * K05 / 4 + K06 * K06, 0.5)) / 500;
        double T06 = T04 * T05 / K10;
        double T07 = (K05 * K07 + Math.pow((K05 * K05 / 4 + K06 * K06), 0.5) * K08 * 2)
                / (K05 + Math.pow((K05 * K05 / 4 + K06 * K06), 0.5) * 2);
        double T08 = 0.085;
        double T09 = 0.00;
        while (T10 >= 0.00001) {
            T09 = Math.pow(-1.0 / 2.0 / Math.log(T07 / 3.71 / T05 + 2.51 / T06 / Math.pow(T08, 0.5)) * Math.log(10.0),
                    2);
            T10 = T09 - T08;
            double T11 = T08 + T10 / 2;
            T08 = T11;
        }
        double R01 = T09 / T05 * Math.pow(T04, 2) * N05 * N01 / 2000 + T04 * T04 * N05 * K09 / 2;
        double R02;
        if (N04 == 1) {
            R02 = R01;
        }
        double T13 = (N02 - K04 * 2) * 3.6 * Math.pow((N01 - K03 * 2) / 1000, 2) / N03;
        if (T13 > 0.45) {
            T13 = 0.45;
        }
        if (T13 < 0.1) {
            log.error(
                    "Plate Cal plateCal Error N03:" + N03 + " N04:" + N04 + " N05:" + N05 + " unitModel:" + unitModel);
        }
        double T14 = 12307.69231 * Math.pow(T13, 5) - 17529.13753 * Math.pow(T13, 4) + 9487.17949 * Math.pow(T13, 3)
                - 2589.51049 * Math.pow(T13, 2) + 452.87179 * T13 + 27.75;
        double T15 = -7692.30769 * Math.pow(T13, 5) + 9137.52914 * Math.pow(T13, 4) - 3959.79021 * Math.pow(T13, 3)
                + 710.48951 * Math.pow(T13, 2) + 9.11422 * T13 + 39.08333;
        double R03 = T14 / ((N04 - 1) * Math.pow((0.8 - T13), 2) + 1) / 100;
        double R04 = T15 / ((N04 - 1) * Math.pow((0.8 - T13), 2) + 1) / 100;
        efficientParam.setN01(N01);
        efficientParam.setN02(N02);
        efficientParam.setR01(BaseDataUtil.decimalConvert(R01, 1));
        efficientParam.setR03(BaseDataUtil.decimalConvert(R03 * 100, 1));
        return efficientParam;
    }

    public static double wheelCal(double F_AirVolume, int EcofreshModel) {
        double eco = EcofreshModel / 1000 / 2.0;
        double e = 3.14 * eco * eco / 2.0;
        return F_AirVolume / 3600 / e;
    }

    /**
     * 
     * @param airVolume
     * @param volumeUnit
     * @param filedName useless why??
     * @return
     */
    public static String getRotordiaHeatX(double airVolume, String volumeUnit, String filedName) {
        double rotorDiaMax = 0;
        double rotorDiaMin = 0;
        List<Rotorpara> rotorparaList = AhuMetadata.findAll(Rotorpara.class);
        ListUtils.sort(rotorparaList, true, UtilityConstant.SYS_STRING_ROTORDIA);
        // 升序排序
        for (Rotorpara Rotorpara : rotorparaList) {
        	if(EmptyUtil.isEmpty(Rotorpara.getPowerEn())||Rotorpara.getPowerEn()==0) {
        		continue;
        	}
            int rotorDia = Rotorpara.getRotorDia();
            double velface = SystemCountUtil.velfacecal(airVolume, rotorDia, volumeUnit);
            if (BaseDataUtil.decimalConvert(velface, 2) <= 3.5) {
                rotorDiaMax = BaseDataUtil.integerConversionDouble(rotorDia);
                break;
            }
        }
        ListUtils.sort(rotorparaList, false, UtilityConstant.SYS_STRING_ROTORDIA);
        // 降序排序
        for (Rotorpara Rotorpara : rotorparaList) {
        	if(EmptyUtil.isEmpty(Rotorpara.getPowerEn())||Rotorpara.getPowerEn()==0) {
        		continue;
        	}
            int rotorDia = Rotorpara.getRotorDia();
            double velface = SystemCountUtil.velfacecal(airVolume, rotorDia, volumeUnit);
            if (BaseDataUtil.decimalConvert(velface, 2) > 5.5) {
                rotorDiaMin = BaseDataUtil.integerConversionDouble(rotorDia);
                break;
            }
        }

        if (rotorDiaMin == 0) {
            rotorDiaMin = rotorDiaMax;
        }
        double velfaceMax=0;
        if(rotorDiaMax!=0) {
        velfaceMax = BaseDataUtil.decimalConvert(SystemCountUtil.velfacecal(airVolume, rotorDiaMax, volumeUnit),
                2);
        }
        double velfaceMin=0;
        if(rotorDiaMin!=0) {
        velfaceMin = BaseDataUtil.decimalConvert(SystemCountUtil.velfacecal(airVolume, rotorDiaMin, volumeUnit),
                2);
        }
        if (velfaceMax == velfaceMin) {
            return String.valueOf(BaseDataUtil.decimalConvert(rotorDiaMax, 2));
        } else if (velfaceMax-5.5 < 5.5-velfaceMin) {
            return String.valueOf(BaseDataUtil.decimalConvert(rotorDiaMin, 2));
        } else {
            return String.valueOf(BaseDataUtil.decimalConvert(rotorDiaMax, 2));
        }
    }

    public static PartWWheel getPartWWheel(String unitModel, String efficiencyType) {
        String unitNo = SystemCountUtil.getUnit(unitModel);
        return AhuMetadata.findOne(PartWWheel.class, unitNo, efficiencyType);
    }

    public static PartWWheel getPartWWheel(String unitModel) {
        String unitNo = SystemCountUtil.getUnit(unitModel);
        return AhuMetadata.findOne(PartWWheel.class, unitNo);
    }

}

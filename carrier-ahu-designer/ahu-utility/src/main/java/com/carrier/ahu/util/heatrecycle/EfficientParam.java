package com.carrier.ahu.util.heatrecycle;

import lombok.Data;

@Data
public class EfficientParam {
    private double N01;
    private double N02;
    private double N05;
    private double N06;
    private double R01;
    private double R03;
    private double R05;
    private double R06;
}
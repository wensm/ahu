package com.carrier.ahu.util.partition;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.exception.ApiException;
import com.carrier.ahu.common.exception.ErrorCode;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.common.collect.Lists;

import java.util.*;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.constant.CommonConstant.SYS_MAP_METAID;

/**
 * Validate ahu partition data before saving from manual update from screen.
 * 
 * Created by Braden Zhou on 2018/06/25.
 */
public final class AhuPartitionValidator {

    public static final int ONSITE_INSTALLATION_LENGTH_THRESHOLD = 18; // 18M
    public static final String CMC_SECTION_NOSIGLE_UNIT_NO_START = "0608";
    public static final String CMC_SECTION_NOSIGLE_UNIT_NO_END = "1015";
    public static final String TECH_SUPPORT_CAN_PASS_UNIT_NO_START = "1015";
    public static final String UNIT_HEIGHT_CHECK_NO = "2226";
    public static final String UNIT_WITH_TOP_DAMPER_HEIGHT_CHECK_NO = "2025";
    public static final String UNIT_WITH_ROOF_HEIGHT_CHECK_NO = "2025";
    public static final String UNIT_WITH_TOP_DAMPER_AND_ROOF_HEIGHT_CHECK_NO = "1825";
    public static final String UNIT_WIDTH_CHECK_NO = "1420";
    public static final String UNIT_WITH_ROOF_WIDTH_CHECK_NO = "1418";
    public static final String UNIT_WITH_ROOF_ONSITE_INSTALL_LENGTH_CHECK_NO = "1418";
    public static final int PARTITION_FOR_NORMAL_CONTAINER_LENGTH = 20;
    public static final int PARTITION_LENGTH_CKD_RANGE_FROM = 21; // 21M ~ 27M
    public static final int PARTITION_LENGTH_CKD_RANGE_TO = 27;
    public static final String UNIT_LENGTH_CKD_CHECK_NO = "1420";
    public static final int PARTITION_WITH_ROOF_ONSITE_INSTALL_LENGTH = 17; // 18M
    public static final String[][] MINIMUM_PARTITION_LENGTH_CHECK_NOS = { { "0608", "0711", "3" },
            { "0811", "0914", "4" }, { "1015", "1117", "5" }, { "1317", "1420", "6" }, { "1621", "2333", "6" } };
    public static final String NEW_UNIT_LENGTH_CHECK_NOS= "39CQ2435,39CQ2635,39CQ2735";

    public static void validate(Partition partition, Unit unit, AhuParam ahuParam) throws ApiException {
        AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
        //当前机组数据异常需要重新进入机组界面点击保存按钮。
        if (EmptyUtil.isEmpty(ahuLayout)) {
            throw new ApiException(ErrorCode.PARTITION_VALIDATE_LAYOUT_JSON_NULL_MSG);
        }
        List<AhuPartition> ahuPartitions = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();//ckd不校验最小M数

        if(!SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)) {//CKD 放开所有无关限制
            if (LayoutStyleEnum.COMMON.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.NEW_RETURN.style() == ahuLayout.getStyle()) { // 单层布局
                validate(ahuPartitions, ahuParam, ahuLayout.getStyle(), true);
            } else if (LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.DOUBLE_RETURN_1.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.DOUBLE_RETURN_2.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.VERTICAL_UNIT_1.style() == ahuLayout.getStyle()
                    || LayoutStyleEnum.VERTICAL_UNIT_2.style() == ahuLayout.getStyle()) { // 双层布局
                int indexOfSecondLayer = getIndexOfSecondLayer(ahuPartitions, ahuLayout.getLayoutData());
                List<AhuPartition> firstLayerAhuPartitions = ahuPartitions.subList(0, indexOfSecondLayer);
                List<AhuPartition> secondLayerAhuPartitions = ahuPartitions.subList(indexOfSecondLayer,
                        ahuPartitions.size());
                validate(firstLayerAhuPartitions, ahuParam, ahuLayout.getStyle(), true);
                validate(secondLayerAhuPartitions, ahuParam, ahuLayout.getStyle(), false);
                validateHeatRecycleStability(ahuLayout.getStyle(), firstLayerAhuPartitions, secondLayerAhuPartitions);
                if (LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()
                        || LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()) {
                    validateBothsideHeatRecycleStability(ahuLayout.getStyle(), firstLayerAhuPartitions, secondLayerAhuPartitions);
                }

                if (LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()) {
                    // 规则： 热回收段段沿气流方向下一个功能段是旁通（混合段或者出风段）时，不能分段。
                    if (!validateWheelHeatRecycleBypass(ahuLayout, ahuPartitions)) {
                        throw new ApiException(ErrorCode.PARTITION_VALIDATE_HEAT_RECYCLE);
                    }
                } else if (LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()) {
                    // 规则： 热回收段段沿气流方向下一个功能段是旁通（混合段或者出风段）时，不能分段。
                    if (!validatePlateHeatRecycleBypass(ahuLayout, ahuPartitions)) {
                        throw new ApiException(ErrorCode.PARTITION_VALIDATE_HEAT_RECYCLE);
                    }
                }
            }
        }
        // int totalPartitionLength = getTotalAhuPartitionLength(ahuPartitions); // 机组所有分段总长度
        int maxPartitionLength = getMaxAhuPartitionLength(ahuPartitions);

        // 高度限制
        // 1. 普通机组：2226及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
        // 2. 带顶部风阀的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
        // 3. 带顶棚的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
        // 4. 带顶部风阀+顶棚的机组：1825及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
        // 宽度、长度限制
        // 1. 1420及以上所有机组：如果机组段长≤20M，可选普通集装箱；在21~27M之间，需CKD或特种箱出厂。如果段长>27M, 只能选用CKD
        // 2. 有顶棚的机组：1418及以上，段长18M及以上，超过集装箱宽度，顶棚需现场安装。
        if (AHUContext.isExportVersion()) { // 出口版
            validateUnitHeightAgainstContainer(ahuParam);
            validateUnitWidthAgainstContainer(ahuParam, maxPartitionLength);
        }

        for (AhuPartition ahuPartition : ahuPartitions) {
            // 规则： 所有机组单独分段长度小于6M, 建议改成CKD出厂。
            // if (!validateLeaveFactoryUseCKD(ahuParam, ahuPartition.getLength())) {
            // throw new ApiException(ErrorCode.PARTITION_VALIDATE_LEN_TOTAL);
            // }
			if (AHUContext.isExportVersion()) {
				// 规则：装箱段段长大于等于18M同时机组的宽度大于等于18M，如果选择顶棚，需要提示段长超过集装箱限制，顶棚需现场安装！
				if (!validateRoofOnsiteInstallation(ahuPartition, ahuParam)) {
					throw new ApiException(ErrorCode.PARTITION_VALIDATE_LEN);
				}
			}
            /* 0608~0711 3M <br/>
             * 0811~0914 4M <br/>
             * 1015~1117 5M <br/>
             * 1317~1420 6M <br/>
             * 1621~2333 6M <br/>*/
            if (!SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery) && !validateMinimumPartitionLength(ahuPartition, ahuParam)) {
                String unitNo = AhuUtil.getUnitNo(ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString());
                int index = getIndexOfMinimumCheckNo(unitNo);
                throw new ApiException(ErrorCode.PARTITION_LENGTH_NO_LESS_THAN,
                        MINIMUM_PARTITION_LENGTH_CHECK_NOS[index][0] + "~"
                                + MINIMUM_PARTITION_LENGTH_CHECK_NOS[index][1],
                        MINIMUM_PARTITION_LENGTH_CHECK_NOS[index][2]);
            }
            //39CQ 2435、2635、2735这三个机组的分段段长需要限制在3米以内（即≤28M）
            if (!validateNewUnitPartitionLength(ahuPartition, ahuParam)) {
                throw new ApiException(ErrorCode.NEW_UNIT_PARTITION_VALIDATE_LEN);
            }
            //39CQ 2435、2635、2735这三个机组的分段段长需要限制在1米以上（即>=10M）
            if (!validateNewUnitPartitionLengthMin(ahuPartition, ahuParam)) {
                throw new ApiException(ErrorCode.NEW_UNIT_PARTITION_VALIDATE_LEN_MIN);
            }
            if(!validateUnitPartitionLength(ahuPartition, ahuParam)) {
            	throw new ApiException(ErrorCode.UNIT_PARTITION_VALIDATE_LEN);
            }
        }
    }

    private static void validate(List<AhuPartition> ahuPartitions, AhuParam ahuParam, int layoutStyle, boolean isTop)
            throws ApiException {
        Map<String, Object> preLastSection = null;
        String preLastSectionId = "";
        boolean switchAirFlow = false; // 默认气流方向从左到右, 不用切换
        boolean foundHeatRecycle = false;
        if (LayoutStyleEnum.WHEEL.style() == layoutStyle && isTop) { // 转轮热回收上层气流方向从右至左
            switchAirFlow = true;
        } else if (LayoutStyleEnum.PLATE.style() == layoutStyle && !isTop) { // 板式热回收下层初始方向从右至左
            switchAirFlow = true;
        }

        for (AhuPartition ahuPartition : ahuPartitions) {
            Map<String, Object> nextFirstSection = ahuPartition.getSections().get(0);
            String nextFirstSectionId = AhuPartition.getMetaIdOfSection(nextFirstSection); // 当前分段块里面的第一个段
            
            // 规则：检查不可拆分段
            if (!validateUnsplittableSection(preLastSectionId, nextFirstSectionId)) {
                throwCannotBeDismantledException(preLastSectionId, nextFirstSectionId);
            }

            // 规则：所有机组【空段】选择了门选项且段长小于5M，不能独立作为分成一段出厂，也就是空段的前面或者后面都必须有其他功能段！
            if (!validateAccessSectionWithODoor(ahuPartition, ahuParam)) {
                throw new ApiException(ErrorCode.PARTITION_VALIDATE_ACCESS_LENGH);
            }

            // 规则： 装箱段不能只有一个【控制段】
            if (!validateSingleControlSection(ahuPartition)) {
                throw new ApiException(ErrorCode.PARTITION_VALIDATE_CTR_EXIST);
            }

            // 规则： 带顶棚选项且有顶部风阀（【混合段】【出风段】【风机段】），机组的宽度大于等于18M, 整机出厂时，英文版时
            // 需要提示,出厂方式自动默认为开顶箱或者CKD。
            if (AHUContext.isExportVersion()) { // 出口版
            	
            	String isprerain = ahuParam.getParams().get(MetaKey.META_AHU_ISPRERAIN).toString();
                String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
                String unitNo = AhuUtil.getUnitNo(ahuParam.getSeries());
                int length=ahuPartition.getLength();
            	
            	if (hasFaceDamper(ahuPartition)==1) {
                    // 机组长度方向的一端面上有水平进出风风阀：机组段长≤19M，可选普通集装箱；在20~26M之间，需CKD或特种箱出厂。如果段长>26M, 只能选用CKD
                    if (UNIT_LENGTH_CKD_CHECK_NO.compareTo(unitNo) < 1
                            && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                    || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))
                            ) {
                    	
                    	if((2000<length&&length<=2600)) {
                    		throw new ApiException(ErrorCode.PARTITION_LENGTH_LIMITED_BY_FACEDAMPER_NUM1);
                    	}
                        
                    } 
                    
                    if(UNIT_LENGTH_CKD_CHECK_NO.compareTo(unitNo) < 1
                        && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery))
                        ) {
                	
                	if(length>2600) {
                		throw new ApiException(ErrorCode.PARTITION_LENGTH_LIMITED_BY_FACEDAMPER_NUM1);
                	}
                    
                }
            }else if (hasFaceDamper(ahuPartition)>1) {
                    // 机组长度方向的两端面都有水平进出风风阀：机组段长≤17M，可选普通集装箱；在18~24M之间，需CKD或特种箱出厂。如果段长>24M, 只能选用CKD。
            	if (UNIT_LENGTH_CKD_CHECK_NO.compareTo(unitNo) < 1
                        && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))
                        ) {
                	
                	if((1800<length&&length<=2400)) {
                		throw new ApiException(ErrorCode.PARTITION_LENGTH_LIMITED_BY_FACEDAMPER_NUM2);
                	}
                    
                }
            	
            	if(UNIT_LENGTH_CKD_CHECK_NO.compareTo(unitNo) < 1
                    && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery))
                    ) {
            	
            	if(length>2400) {
            		throw new ApiException(ErrorCode.PARTITION_LENGTH_LIMITED_BY_FACEDAMPER_NUM2);
            	}
                
            }
                } 
            	
                if (!validateRoofWithTopOutlet(ahuPartition, ahuParam)) {
                    throw new ApiException(ErrorCode.EXPORT_UNIT_WITH_TOP_DAMPER_NEED_CKD);
                }
            }

            // 规则：0608~1015机组的新回排段不能单独分段。
            if (!validateSingleCombinedMixingChamberSection(ahuPartition, ahuParam)) {
                throw new ApiException(ErrorCode.PARTITION_VALIDATE_COMBINEDMIXINGCHAMBER_SPLIT);
            }

            // 规则：所有机组小于5M段长的带检修门功能段（混合段，出风段，风机段，空段，二次回风段）不能单独分段。
            if (!validateSingleSectionWithAccessDoor(ahuPartition, ahuParam)) {
                throw new ApiException(ErrorCode.PARTITION_VALIDATE_DOOR);
            }

            // 规则: 由于现场段连接操作困难,两个都不带检修门功能段之间禁止分段!
            if (!SectionTypeEnum.TYPE_COMPOSITE.getId().equals(preLastSectionId)//综合过滤段后面允许分段，不需要其他的多余规则
                    && !validateAccessDoorOfAdjacentSection(ahuPartitions, ahuPartition, ahuParam)) {
                throwCannotBeDismantledException(preLastSectionId, nextFirstSectionId);
            }

            // 规则: 由于现场段连接操作困难,过滤段前或者侧抽过滤段前后禁止分段!
            // 需要考虑气流方向
            Map<String, Object> preFlowSection = preLastSection;
            Map<String, Object> nextFlowSection = nextFirstSection;
            if (switchAirFlow) {
                preFlowSection = nextFirstSection;
                nextFlowSection = preLastSection;
            }
            //39G/CQ/XT机组都去掉39XT这条分段限制
            if (!validateSplittedFilterSection(preFlowSection, nextFlowSection, ahuParam)) {
                throwCannotBeDismantledException(preLastSectionId, nextFirstSectionId);
            }

            // 规则: 控制段需要与风机段作为一段出厂，不能单独分段！
            if (!validateSplittedCtrAndFanSection(preLastSectionId, nextFirstSectionId)) {
                throwCannotBeDismantledException(preLastSectionId, nextFirstSectionId);
            }

            preLastSection = ahuPartition.getSections().get(ahuPartition.getSections().size() - 1);
            preLastSectionId = AhuPartition.getMetaIdOfSection(preLastSection); // 上个分段块里面的最后一个段
            if (LayoutStyleEnum.PLATE.style() == layoutStyle && !foundHeatRecycle) { // 从板式热回段下一个开始切换气流方向
                if (isHeatRecycleInPartition(ahuPartition)) {
                    switchAirFlow = !switchAirFlow;
                }
            }
        }
    }

    public static boolean isDeliveryCKD(AhuParam ahuParam) {
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
        return SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery);
    }

    private static void throwCannotBeDismantledException(String preLastSectionId, String nextFirstSectionId) {
        String preLastSectionName = getIntlString(SectionTypeEnum.getSectionTypeFromId(preLastSectionId).getCnName());
        String nextFirstSectionName = getIntlString(
                SectionTypeEnum.getSectionTypeFromId(nextFirstSectionId).getCnName());
        throw new ApiException(ErrorCode.SECTION_CANNOT_BE_DISMANTLED, preLastSectionName, nextFirstSectionName);
    }

    private static boolean isHeatRecycleInPartition(AhuPartition ahuPartition) {
        LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
        for (Map<String, Object> section : sections) {
            String sectionId = AhuPartition.getMetaIdOfSection(section);
            if (SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(sectionId)
                    || SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(sectionId)) { // 找到热回收段
                return true;
            }
        }
        return false;
    }

    /**
     * 校验热回收机组上下层段长稳定性。
     */
    private static void validateHeatRecycleStability(int layoutStyle,
            List<AhuPartition> firstLayerAhuPartitions, List<AhuPartition> secondLayerAhuPartitions) {
        int tempLength = 0;
        int topLeftLength = 0;
        int topRightLength = 0;
        int bottomLeftLength = 0;
        int bottomRightLength = 0;
        AhuPartition topHeatRecyclePartition = null;
        AhuPartition bottomHeatRecyclePartition = null;
        // calculate top layer
        for (AhuPartition partition : firstLayerAhuPartitions) {
            List<Map<String, Object>> sections = partition.getSections();
            for (Map<String, Object> section : sections) {
                String sectionId = AhuPartition.getMetaIdOfSection(section);
                if (getHeatReacycleSectionId(layoutStyle).equals(sectionId)) {
                    topHeatRecyclePartition = partition;
                    topLeftLength = tempLength;
                    tempLength = 0;
                    continue;
                }
                int sectionLength = AhuPartition.getSectionLOfSection(section);
                tempLength += sectionLength;
            }
        }
        topRightLength = tempLength;
        tempLength = 0;

        // calculate bottom layer
        for (AhuPartition partition : secondLayerAhuPartitions) {
            List<Map<String, Object>> sections = partition.getSections();
            for (Map<String, Object> section : sections) {
                String sectionId = AhuPartition.getMetaIdOfSection(section);
                if (getHeatReacycleSectionId(layoutStyle).equals(sectionId)) {
                    bottomHeatRecyclePartition = partition;
                    bottomLeftLength = tempLength;
                    tempLength = 0;
                    continue;
                }
                tempLength += AhuPartition.getSectionLOfSection(section);
            }
        }
        bottomRightLength = tempLength;

        if (topLeftLength+topRightLength > bottomLeftLength+bottomRightLength) {
            throw new ApiException(ErrorCode.PARTITION_VALIDATE_HEAT_RECYCLE_STABILITY);
        }

        // 校验热回收上下分段稳定性
//        validateHeatRecycleStability(topHeatRecyclePartition, bottomHeatRecyclePartition, layoutStyle);
    }
    
    private static void validateBothsideHeatRecycleStability(int layoutStyle,
            List<AhuPartition> firstLayerAhuPartitions, List<AhuPartition> secondLayerAhuPartitions) {
        int tempLength = 0;
        int topLeftLength = 0;
        int topRightLength = 0;
        int bottomLeftLength = 0;
        int bottomRightLength = 0;
        AhuPartition topHeatRecyclePartition = null;
        AhuPartition bottomHeatRecyclePartition = null;
        // calculate top layer
        for (AhuPartition partition : firstLayerAhuPartitions) {
            List<Map<String, Object>> sections = partition.getSections();
            for (Map<String, Object> section : sections) {
                String sectionId = AhuPartition.getMetaIdOfSection(section);
                if (getHeatReacycleSectionId(layoutStyle).equals(sectionId)) {
                    topHeatRecyclePartition = partition;
                    topLeftLength = tempLength;
                    tempLength = 0;
                    continue;
                }
                int sectionLength = AhuPartition.getSectionLOfSection(section);
                tempLength += sectionLength;
            }
        }
        topRightLength = tempLength;
        tempLength = 0;

        // calculate bottom layer
        for (AhuPartition partition : secondLayerAhuPartitions) {
            List<Map<String, Object>> sections = partition.getSections();
            for (Map<String, Object> section : sections) {
                String sectionId = AhuPartition.getMetaIdOfSection(section);
                if (getHeatReacycleSectionId(layoutStyle).equals(sectionId)) {
                    bottomHeatRecyclePartition = partition;
                    bottomLeftLength = tempLength;
                    tempLength = 0;
                    continue;
                }
                tempLength += AhuPartition.getSectionLOfSection(section);
            }
        }
        bottomRightLength = tempLength;

        if (topLeftLength>bottomLeftLength || topRightLength>bottomRightLength) {
            throw new ApiException(ErrorCode.PARTITION_VALIDATE_HEAT_RECYCLE_STABILITY);
        }

        // 校验热回收上下分段稳定性
//        validateHeatRecycleStability(topHeatRecyclePartition, bottomHeatRecyclePartition, layoutStyle);
    }

    private static String getHeatReacycleSectionId(int layoutStyle) {
        if (LayoutStyleEnum.WHEEL.style() == layoutStyle) {
            return SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId();
        } else if (LayoutStyleEnum.PLATE.style() == layoutStyle) {
            return SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId();
        }
        return "";
    }

    private static void validateHeatRecycleStability(AhuPartition topPartition, AhuPartition bottomPartition,
            int layoutStyle) {
        int tempLength = 0;
        int topLeftLength = 0;
        int topRightLength = 0;
        int bottomLeftLength = 0;
        int bottomRightLength = 0;

        // calculate top layer
        List<Map<String, Object>> sections = topPartition.getSections();
        for (Map<String, Object> section : sections) {
            String sectionId = AhuPartition.getMetaIdOfSection(section);
            if (getHeatReacycleSectionId(layoutStyle).equals(sectionId)) {
                topLeftLength = tempLength;
                tempLength = 0;
                continue;
            }
            int sectionLength = AhuPartition.getSectionLOfSection(section);
            tempLength += sectionLength;
        }
        topRightLength = tempLength;
        tempLength = 0;

        // calculate bottom layer
        sections = bottomPartition.getSections();
        for (Map<String, Object> section : sections) {
            String sectionId = AhuPartition.getMetaIdOfSection(section);
            if (getHeatReacycleSectionId(layoutStyle).equals(sectionId)) {
                bottomLeftLength = tempLength;
                tempLength = 0;
                continue;
            }
            tempLength += AhuPartition.getSectionLOfSection(section);
        }
        bottomRightLength = tempLength;

        if (topLeftLength > bottomLeftLength || topRightLength > bottomRightLength) {
            throw new ApiException(ErrorCode.PARTITION_VALIDATE_HEAT_RECYCLE_STABILITY);
        }
    }

    /**
     * 校验返回false, 如果都不带检修门。 </br>
     * 
     * 规则: 由于现场段连接操作困难,两个都不带检修门功能段之间禁止分段!
     * 
     * @param preLastSection
     * @param nextFirstSection
     * @param ahuParam
     * @return
     */
    public static boolean validateAccessDoorOfAdjacentSection(Map<String, Object> preLastSection,
            Map<String, Object> nextFirstSection, AhuParam ahuParam) {
        if (preLastSection != null && nextFirstSection != null) {
            // 返回false，如果都没有检修门
            return hasAccessDoor(preLastSection, ahuParam) || hasAccessDoor(nextFirstSection, ahuParam);
        }
        return true;
    }

    /**
     * 校验返回false, 如果两侧都无法通过。 </br>
     * 
     * 规则: 由于现场段连接操作困难, 前后分段需要有一个段带门并可以通过到分段处进行连接!
     * 
     * @param ahuParatitions
     * @param currentPartition
     * @param ahuParam
     * @return
     */
    public static boolean validateAccessDoorOfAdjacentSection(List<AhuPartition> ahuParatitions,
            AhuPartition ahuPartition, AhuParam ahuParam) {
        return canPass(ahuParatitions, ahuPartition, ahuParam, true)
                || canPass(ahuParatitions, ahuPartition, ahuParam, false);
    }

    private static boolean canPass(List<AhuPartition> ahuPartitions, AhuPartition ahuPartition, AhuParam ahuParam,
            boolean forward) {
        int idx = ahuPartitions.indexOf(ahuPartition);
        if (idx == 0) {
            return true;
        }

        List<Map<String, Object>> sections = Lists.newLinkedList();
        if (forward) {
            for (int i = 0; i < idx; i++) {
                AhuPartition prePartition = ahuPartitions.get(i);
                sections.addAll(prePartition.getSections());
            }
        } else {
            for (int i = idx; i < ahuPartitions.size(); i++) {
                AhuPartition prePartition = ahuPartitions.get(i);
                sections.addAll(prePartition.getSections());
            }
        }
        return canPass(sections, ahuParam, forward);
    }

    private static boolean canPass(List<Map<String, Object>> sections, AhuParam ahuParam, boolean forward) {
        List<Map<String, Object>> orderedSections = Lists.newLinkedList();
        orderedSections.addAll(sections);
        if (forward) {
            Collections.reverse(orderedSections);
        }

        boolean isDoubleLayer=ahuParam.isDoubleLayer();
        for (int i = 0; i < orderedSections.size(); i++) {
            Map<String, Object> section = orderedSections.get(i);

            if (hasAccessDoor(section, ahuParam)) {
                String sectionId = AhuPartition.getMetaIdOfSection(section);
                // 无蜗壳风机向后可通过，向前不可通过； 离心风机（前倾、后倾、机翼型）向前可通过，向后不可通过；
                // 其它段有门即可通过
                if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionId)) {
                	String outletKey = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_FAN, section,
                            "meta.section.fan.outlet");
                	
                if (!"wwk".equals(outletKey)) {
                	String direction = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_FAN, section,
                            "meta.section.airDirection");
					if (isDoubleLayer) {
						if (forward && "R".equals(direction)) {
							return true;
						} else if (!forward && "S".equals(direction)) {
							return true;
						} else {
							return false;
						}
					}else{
					    if (!forward) {
							return true;
						} else {
							return false;
						}
					}
                }
                if (SectionTypeEnum.TYPE_WWKFAN.getId().equals(sectionId)) {
                	String direction = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_FAN, section,
                            "meta.section.airDirection");
					if (isDoubleLayer) {
						if (forward && "S".equals(direction)) {
							return true;
						} else if (!forward && "R".equals(direction)) {
							return true;
						} else {
							return false;
						}
					} else {
						if (forward) {
							return true;
						} else {
							return false;
						}
					}

                }
                }
                return true;
            }

            if (canPass(section, ahuParam)) {
                continue;
            } else {
                return false;
            }
        }
        return false;
    }

    private static boolean canPass(Map<String, Object> section, AhuParam ahuParam) {
        String sectionId = AhuPartition.getMetaIdOfSection(section);
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionId)
                || SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionId)
                || SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionId)
                || SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionId)
                || SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionId)
                || SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(sectionId)
                || SectionTypeEnum.TYPE_CTR.getId().equals(sectionId)) {
            return true;
        } else if (SectionTypeEnum.TYPE_SINGLE.getId().equals(sectionId)
                || SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionId)
                || SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionId)) {
            // 工厂认为不可通过； 如果销售支持统一意见，可以在专家版中定义1015及以上规格可通过拆除过滤器安装框后通过。
            String role = AHUContext.getUserRole();
            String unitNo = AhuUtil.getUnitNo(ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString());
            if (/*(RoleEnum.Engineer.name().equalsIgnoreCase(role) || RoleEnum.Factory.name().equalsIgnoreCase(role))//统一处理为：1015及以上机组，过滤段为可通过段
                    &&*/ TECH_SUPPORT_CAN_PASS_UNIT_NO_START.compareTo(unitNo) < 1) {
                return true;
            }
        }
        return false;
    }

    private static boolean hasAccessDoor(Map<String, Object> section, AhuParam ahuParam) {
        String sectionId = AhuPartition.getMetaIdOfSection(section);
        if (!AhuPartitionGenerator.SECTION_WITH_DOOR.contains(sectionId)) {
            return false;
        }

        String accessDoor = null;
        if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_MIX, section, MetaKey.KEY_DOORO);
            return SystemCalculateConstants.MIX_DOORO_DOORNOVIEWPORT.equals(accessDoor)
                    || SystemCalculateConstants.MIX_DOORO_DOORWITHVIEWPORT.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_DISCHARGE, section, MetaKey.KEY_ODOOR);
            return SystemCalculateConstants.DISCHARGE_ODOOR_DN.equals(accessDoor)
                    || SystemCalculateConstants.DISCHARGE_ODOOR_DY.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_FAN, section, MetaKey.KEY_ACCESSDOOR);
            return true;
        }else if (SectionTypeEnum.TYPE_WWKFAN.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_WWKFAN, section, MetaKey.KEY_ACCESSDOOR);
            return true;
        } else if (SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionId)) {
            accessDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_ACCESS, section, MetaKey.KEY_ODOOR);
            return SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(accessDoor)
                    || SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(accessDoor);
        } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(sectionId)) {
            String osDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, section,
                    MetaKey.KEY_OSDOOR);
            String isDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, section,
                    MetaKey.KEY_ISDOOR);
            return SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(osDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DN.equals(osDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(isDoor)
                    || SystemCalculateConstants.COMBINEDMIXINGCHAMBER_OSDOOR_DN.equals(isDoor);
        }
        // else if (SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionId)
        // || SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(sectionId)
        // || SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(sectionId)
        // || SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionId)) {
        // return true;
        // }
        return false;
    }

    private static int getIndexOfSecondLayer(List<AhuPartition> ahuPartitions, int[][] layoutData) {
        int indexOfSecondLayer = 0;
        if (layoutData.length == 4) { // two layers
            for (int i = 0; i < ahuPartitions.size(); i++) {
                AhuPartition ahuPartition = ahuPartitions.get(i);
                LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
                int position = AhuPartition.getPosOfSection(sections.get(0));
                if (Arrays.binarySearch(layoutData[2], position) != -1) { // in second layer
                    return i;
                }
            }
        }
        return indexOfSecondLayer;
    }

    @SuppressWarnings("unused")
    private static int getTotalAhuPartitionLength(List<AhuPartition> ahuPartitions) {
        int totalLength = 0;
        for (AhuPartition ahuPartition : ahuPartitions) {
            totalLength += ahuPartition.getLength();
        }
        return totalLength;
    }

    private static int getMaxAhuPartitionLength(List<AhuPartition> ahuPartitions) {
        int maxLength = 0;
        for (AhuPartition ahuPartition : ahuPartitions) {
            int temLength = ahuPartition.getLength();
            if (maxLength < temLength) {
                maxLength = temLength;
            }
        }
        return maxLength;
    }

    /**
     * 根据layout数据划分开上下层分段。
     * 
     * @param ahuPartitions
     * @param layoutData
     * @return
     */
    public static List<List<AhuPartition>> getLayeredAhuPartitions(List<AhuPartition> ahuPartitions,
            int[][] layoutData) {
        List<List<AhuPartition>> layeredAhuPartitions = new ArrayList<>();
        if (layoutData.length == 4) { // two layers
            List<AhuPartition> firstLayerAhuPartition = new ArrayList<>();
            List<AhuPartition> secondLayerAhuPartition = new ArrayList<>();
            for (AhuPartition ahuPartition : ahuPartitions) {
                LinkedList<Map<String, Object>> sections = ahuPartition.getSections();
                int position = AhuPartition.getPosOfSection(sections.get(0));
                // in first layer
                if (Arrays.binarySearch(layoutData[0], position) != -1
                        || Arrays.binarySearch(layoutData[1], position) != -1) {
                    firstLayerAhuPartition.add(ahuPartition);
                } else { // in second layer
                    secondLayerAhuPartition.add(ahuPartition);
                }
            }
            layeredAhuPartitions.add(firstLayerAhuPartition);
            layeredAhuPartitions.add(secondLayerAhuPartition);
        } else { // one layer
            layeredAhuPartitions.add(ahuPartitions);
        }

        return layeredAhuPartitions;
    }

    /**
     * 校验失败返回false，如果分段长度大于最大段长，并且包含可拆分的段。
     * 
     * @param partition
     * @return
     */
    public static boolean validatePartitionLength(AhuPartition partition) {
        int maxLength = AhuPartitionGenerator.getMaxPartitionLength();
        if (partition.getLength() > maxLength && hasSplittableSection(partition.getSections())) {
            return false;
        }
        return true;
    }

    /**
     * 检查是否有可拆分的段。
     * 
     * @param sections
     * @return
     */
    private static boolean hasSplittableSection(List<Map<String, Object>> sections) {
        for (Map<String, Object> section : sections) {
            String sectionId = AhuPartition.getMetaIdOfSection(section);
            if (EmptyUtil.isNotEmpty(sectionId) && !AhuPartitionGenerator.UNSPLITABLE_SECTION.contains(sectionId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 返回false，如果前后两个段都不能被拆开。
     * 
     * @param preLastSectionId
     * @param nextFirstSectionId
     * @return
     */
    public static boolean validateUnsplittableSection(String preLastSectionId, String nextFirstSectionId) {
        return !(AhuPartitionGenerator.UNSPLITABLE_SECTION.contains(nextFirstSectionId)
                && AhuPartitionGenerator.UNSPLITABLE_SECTION.contains(preLastSectionId));
    }

    /**
     * 校验返回true, 如果, </br>
     * 规则：所有机组【空段】选择了门选项且段长小于5M，不能独立作为分成一段出厂，也就是空段的前面或者后面都必须有其他功能段！
     * 
     * @param partition
     * @param ahuParam
     * @return
     */
    public static boolean validateAccessSectionWithODoor(AhuPartition partition, AhuParam ahuParam) {
        Map<String, Object> section = partition.getSections().get(0);
        String oDoor = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_ACCESS, section, MetaKey.KEY_ODOOR);
        return !((partition.getSections().size() == 1)
                && AhuPartition.getSectionLOfSection(section) < AhuPartitionGenerator.getMinAccessLength(ahuParam.getProduct())
                && SectionTypeEnum.TYPE_ACCESS.getId().equals(AhuPartition.getMetaIdOfSection(section))
                && (SystemCalculateConstants.ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(oDoor)
                        || SystemCalculateConstants.ACCESS_ODOOR_DOOR_W_O_VIEWPORT.equals(oDoor)));
    }

    public static String getSectionParameterByKey(AhuParam ahuParam, SectionTypeEnum sectionType,
            Map<String, Object> section, String metaKey) {
        return getSectionParameterByKey1(ahuParam, sectionType.getId(), AhuPartition.getPosOfSection(section),
                SectionMetaUtils.getMetaSectionKey(sectionType, metaKey));
    }

    public static String getSectionParameterByKey1(AhuParam ahuParam, String sectionKey, short sectionPos,
            String key) {
        for (PartParam partParam : ahuParam.getPartParams()) {
            if (partParam.getKey().equals(sectionKey) // 段type相同
                    && partParam.getPosition() == sectionPos) {// 位置相等
                return String.valueOf(partParam.getParams().get(key));
            }
        }
        return null;
    }

    /**
     * 校验返回false, 如果分段只有一个控制段。
     * 
     * @param partition
     * @return
     */
    public static boolean validateSingleControlSection(AhuPartition partition) {
        Map<String, Object> section = partition.getSections().get(0);
        return !(partition.getSections().size() == 1
                && SectionTypeEnum.TYPE_CTR.getId().equals(AhuPartition.getMetaIdOfSection(section)));
    }

    /**
     * 校验返回false, 如果, </br>
     * 
     * 规则： 带顶棚选项且有顶部风阀（【混合段】【出风段】【风机段】），机组的宽度大于等于18M, 整机出厂时，英文版时需要提示,出厂方式自动默认为开顶箱或者CKD。
     * 
     * @param partition
     * @param ahuParam
     * @return
     */
    public static boolean validateRoofWithTopOutlet(AhuPartition partition, AhuParam ahuParam) {
        String isprerain = ahuParam.getParams().get(MetaKey.META_AHU_ISPRERAIN).toString();
        String unitModel = ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString();
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
        int unitWidth = AhuUtil.getWidthOfAHU(unitModel);
        if (Boolean.valueOf(isprerain) && unitWidth >= ONSITE_INSTALLATION_LENGTH_THRESHOLD) {
            for (Map<String, Object> section : partition.getSections()) {
                if (SectionTypeEnum.TYPE_MIX.getId().equals(AhuPartition.getMetaIdOfSection(section))) {
                    String returnTop = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_MIX, section,
                            MetaKey.KEY_RETURNTOP);
                    if (Boolean.valueOf(returnTop)) {
                        return (SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery));
                    }
                } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(AhuPartition.getMetaIdOfSection(section))) {
                    String outletDirection = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_DISCHARGE, section,
                            MetaKey.KEY_OUTLETDIRECTION);
                    if (SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_T.equals(outletDirection)) {
                        return (SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery));
                    }
                } else if (SectionTypeEnum.TYPE_FAN.getId().equals(AhuPartition.getMetaIdOfSection(section))) {
                    String outletDirection = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_FAN, section,
                            MetaKey.KEY_oUTLETDIRECTION);
                    if (SystemCalculateConstants.FAN_OUTLETDIRECTION_UBF.equals(outletDirection)
                            || SystemCalculateConstants.FAN_OUTLETDIRECTION_UBR.equals(outletDirection)) {
                        return (SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery));
                    }
                }
            }
        }
        return true;
    }

    /**
     * 校验返回false, 如果, </br>
     * 规则 ：装箱段段长大于等于18M同时机组的宽度大于等于18M，如果选择顶棚，需要提示段长超过集装箱限制，顶棚需现场安装！
     * 
     * @param partition
     * @param ahuParam
     * @return
     */
    public static boolean validateRoofOnsiteInstallation(AhuPartition partition, AhuParam ahuParam) {
        if(!ahuParam.getParams().containsKey(MetaKey.META_AHU_ISPRERAIN)) {
        	return true;
        }
    	String isprerain = ahuParam.getParams().get(MetaKey.META_AHU_ISPRERAIN).toString();
        String unitModel = ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString();
        int unitWidth = AhuUtil.getWidthOfAHU(unitModel);
        return !(Boolean.valueOf(isprerain) && unitWidth >= ONSITE_INSTALLATION_LENGTH_THRESHOLD
                && partition.getLength() >= AhuUtil.getRealSize(ONSITE_INSTALLATION_LENGTH_THRESHOLD));
    }

    /**
     * 校验返回false, 如果,
     * 
     * </br>
     * 规则：0608~1015机组的新回排段不能单独分段。
     * 
     * @param partition
     * @return
     */
    public static boolean validateSingleCombinedMixingChamberSection(AhuPartition partition, AhuParam ahuParam) {
        Map<String, Object> section = partition.getSections().get(0);
        String unitNo = AhuUtil.getUnitNo(ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString());
        return !((partition.getSections().size() == 1) && CMC_SECTION_NOSIGLE_UNIT_NO_START.compareTo(unitNo) < 1
                && CMC_SECTION_NOSIGLE_UNIT_NO_END.compareTo(unitNo) > -1
                && SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(AhuPartition.getMetaIdOfSection(section)));
    }

    /**
     * 校验返回false, 如果, </br>
     * 
     * 规则8：所有机组小于5M段长的带检修门功能段（混合段，出风段，风机段，空段，二次回风段）不能单独分段。
     * 
     * @param partition
     * @param ahuParam
     * @return
     */
    public static boolean validateSingleSectionWithAccessDoor(AhuPartition partition, AhuParam ahuParam) {
        if (partition.getSections().size() == 1) { // 独立分段
            Map<String, Object> section = partition.getSections().get(0);
            int sectionLength = AhuPartition.getSectionLOfSection(section);
            if (sectionLength < AhuPartitionGenerator.getMinPartitionWithDoorLength()) { // 小于 5M
                return !hasAccessDoor(section, ahuParam);
            }
        }
        return true;
    }

    /**
     * 校验返回false, 如果前后分段末尾是单层过滤段。 </br>
     * 
     * 规则: 由于现场段连接操作困难,过滤段前或者侧抽过滤段前后禁止分段! --修改为单层过滤器和综合过滤器前不能分段
     * 
     * @param preLastSection
     * @param nextFirstSection
     * @param ahuParam
     * @return
     */
    public static boolean validateSplittedFilterSection(Map<String, Object> preLastSection,
            Map<String, Object> nextFirstSection, AhuParam ahuParam) {
        String preLastSectionId = null;
        String nextFirstSectionId = null;
        String fitetF = null;
        if (preLastSection != null) {
            preLastSectionId = AhuPartition.getMetaIdOfSection(preLastSection);
            if (SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(preLastSectionId)) {
                fitetF = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_HEPAFILTER, preLastSection,
                        MetaKey.KEY_FITETF);
                // 高效过滤段，1. 如果选择箱型，可以在看作有门，但是门在箱体后部，可以分到一个分段的末尾
                if (!SystemCalculateConstants.HEPAFILTER_FILTERF_P.equals(fitetF)) {
                    return false;
                }
            }
        }
        if (nextFirstSection != null) {
            nextFirstSectionId = AhuPartition.getMetaIdOfSection(nextFirstSection);
            if (SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(nextFirstSectionId)) {
                /*fitetF = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_HEPAFILTER, nextFirstSection,
                        MetaKey.KEY_FITETF);*/
                // 高效过滤段，2.如果选择V形，门可以看作是在段的前面，可以被分到一个分段的开头
                /*if (!SystemCalculateConstants.HEPAFILTER_FILTERF_V.equals(fitetF)) {*/
                    return false;//最新逻辑20200720：高效前面不能分段。
                /*}*/
            }
        }
		if (preLastSection != null && nextFirstSection != null) {
			String mediaLoading = null;
			String filterF=null;
			if (SectionTypeEnum.TYPE_SINGLE.getId().equals(preLastSectionId)) { // 侧抽后不允许分段
				mediaLoading = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_SINGLE, preLastSection,
						MetaKey.KEY_MEDIALOADING);
				filterF = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_SINGLE, preLastSection,
						MetaKey.KEY_FITETF);
				return (!SystemCalculateConstants.FILTER_MEDIALOADING_SIDELOADING.equals(mediaLoading))&&(!SystemCalculateConstants.FILTER_FITETF_P.equals(filterF));

			}else if(SectionTypeEnum.TYPE_SINGLE.getId().equals(nextFirstSectionId)) {
				mediaLoading = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_SINGLE, nextFirstSection,
						MetaKey.KEY_MEDIALOADING);
				filterF = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_SINGLE, preLastSection,
						MetaKey.KEY_FITETF);
				return (!SystemCalculateConstants.FILTER_MEDIALOADING_SIDELOADING.equals(mediaLoading))&&(!SystemCalculateConstants.FILTER_MEDIALOADING_FRONTLOADING.equals(mediaLoading))&&(!SystemCalculateConstants.FILTER_FITETF_P.equals(filterF));
			}else if (SectionTypeEnum.TYPE_COMPOSITE.getId().equals(preLastSectionId)) { // 侧抽后不允许分段
				mediaLoading = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_COMPOSITE, preLastSection,
						MetaKey.KEY_MEDIALOADING);
				return !SystemCalculateConstants.COMBINEDFILTER_MEDIALOADING_SIDELOADING.equals(mediaLoading);

			}else if(SectionTypeEnum.TYPE_COMPOSITE.getId().equals(nextFirstSectionId)) {
				mediaLoading = getSectionParameterByKey(ahuParam, SectionTypeEnum.TYPE_COMPOSITE, nextFirstSection,
						MetaKey.KEY_MEDIALOADING);
				return !SystemCalculateConstants.COMBINEDFILTER_MEDIALOADING_SIDELOADING.equals(mediaLoading)&&(!SystemCalculateConstants.FILTER_MEDIALOADING_FRONTLOADING.equals(mediaLoading));
			}

		}
        return true;
    }

    /**
     * 返回false，如果, </br>
     * 
     * 规则: 控制段需要与风机段作为一段出厂，不能单独分段！
     * 
     * @param preLastSectionId
     * @param nextFirstSectionId
     * @return
     */
    public static boolean validateSplittedCtrAndFanSection(String preLastSectionId, String nextFirstSectionId) {
        return !((SectionTypeEnum.TYPE_CTR.getId().equals(preLastSectionId)
                && SectionTypeEnum.TYPE_FAN.getId().equals(nextFirstSectionId))
                || (SectionTypeEnum.TYPE_CTR.getId().equals(nextFirstSectionId)
                        && SectionTypeEnum.TYPE_FAN.getId().equals(preLastSectionId)));
    }

    /**
     * 校验返回false, 如果, </br>
     * 
     * 规则：所有机组单独分段长度小于6M, 建议改成CKD出厂。
     * 
     * @param ahuParam
     * @param partitionLength
     * @return
     */
    public static boolean validateLeaveFactoryUseCKD(AhuParam ahuParam, int partitionLength) {
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
        return SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                || !(partitionLength < AhuPartitionGenerator.getMinPartitionLength());
    }

    /**
     * 校验返回false, 如果, </br>
     * 
     * 规则： 热回收段段沿气流方向下一个功能段是旁通（混合段或者出风段）时，不能分段。
     * 
     * @param ahuLayout
     * @param ahuPartitions
     * @return
     */
    public static boolean validatePlateHeatRecycleBypass(AhuLayout ahuLayout, List<AhuPartition> ahuPartitions) {
        if (LayoutStyleEnum.PLATE.style() == ahuLayout.getStyle()) {
            int[] bottomLeftLayout = ahuLayout.getLayoutData()[2];
            int positionOfBottomRecyle = -1;
            if (bottomLeftLayout.length > 0) { // 回收段在最后一个
                positionOfBottomRecyle = bottomLeftLayout[bottomLeftLayout.length - 1];
            }

            if (positionOfBottomRecyle != -1) {
                Map<String, Object> lastSectionOfPrePartition = null;
                for (int p = 0; p < ahuPartitions.size(); p++) {
                    AhuPartition ahuPartition = ahuPartitions.get(p);
                    for (int s = 0; s < ahuPartition.getSections().size(); s++) {
                        Map<String, Object> section = ahuPartition.getSections().get(s);
                        int position = AhuPartition.getPosOfSection(section);
                        if (position == positionOfBottomRecyle) { // 找到了回收段
                            // 回收段是分段中第一个段
                            if (s == 0) {
                                if (lastSectionOfPrePartition != null) {
                                    int lastSectionPosition = AhuPartition.getPosOfSection(lastSectionOfPrePartition);
                                    // 而且上个分段的最后一个段也是在底层左侧的布局里面
                                    if (Arrays.binarySearch(bottomLeftLayout, lastSectionPosition) != -1) {
                                        // 同时上个分段的最后一个段是旁通(混合段或者出风段)
                                        if ((SectionTypeEnum.TYPE_MIX.getId()
                                                .equals(AhuPartition.getMetaIdOfSection(lastSectionOfPrePartition))
                                                || SectionTypeEnum.TYPE_DISCHARGE.getId().equals(
                                                        AhuPartition.getMetaIdOfSection(lastSectionOfPrePartition)))) {
                                            return false; // 不允许分段
                                        }
                                    }
                                }
                            }
                            if (s == ahuPartition.getSections().size() - 1) {// 回收段是分段中最后一个段
                                if (p < ahuPartitions.size() - 1) {// 获取下一个分段, 再获取分段中的第一个段
                                    AhuPartition nextPartition = ahuPartitions.get(p + 1);
                                    Map<String, Object> firstSectionOfNextPartiton = nextPartition.getSections().get(0);
                                    // 下个分段的第一个段是旁通(混合段或者出风段)
                                    if ((SectionTypeEnum.TYPE_MIX.getId()
                                            .equals(AhuPartition.getMetaIdOfSection(firstSectionOfNextPartiton))
                                            || SectionTypeEnum.TYPE_DISCHARGE.getId().equals(
                                                    AhuPartition.getMetaIdOfSection(firstSectionOfNextPartiton)))) {
                                        return false; // 不允许分段
                                    }
                                } else { // 沒有后续分段
                                    return true;
                                }
                            }
                            // 回收段没有独立分开
                            return true;
                        } else if (s == ahuPartition.getSections().size() - 1) { // 记录本分段最后一个段
                            lastSectionOfPrePartition = section;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * 转轮热回收段下一个段是旁通(混合段或者出风段), 不允许分段
     * 
     * @param preSection
     * @param nextSection
     * @return
     */
    public static boolean validateWheelHeatRecycleBypass(Map<String, Object> preSection,
            Map<String, Object> nextSection) {
        if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(AhuPartition.getMetaIdOfSection(preSection))
                && (SectionTypeEnum.TYPE_MIX.getId().equals(AhuPartition.getMetaIdOfSection(nextSection))
                        || SectionTypeEnum.TYPE_DISCHARGE.getId()
                                .equals(AhuPartition.getMetaIdOfSection(nextSection)))) {
            return false;
        }
        return true;
    }

    /**
     * 板式热回收段下一个段是旁通(混合段或者出风段), 不允许分段
     * 
     * @param preSection
     * @param nextSection
     * @return
     */
    public static boolean validatePlateHeatRecycleBypass(Map<String, Object> preSection,
            Map<String, Object> nextSection) {
        if (SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(AhuPartition.getMetaIdOfSection(preSection))
                && (SectionTypeEnum.TYPE_MIX.getId().equals(AhuPartition.getMetaIdOfSection(nextSection))
                        || SectionTypeEnum.TYPE_DISCHARGE.getId()
                                .equals(AhuPartition.getMetaIdOfSection(nextSection)))) {
            return false;
        }
        return true;
    }

    /**
     * 校验返回false, 如果, </br>
     * 
     * 规则： 热回收段段沿气流方向下一个功能段是旁通（混合段或者出风段）时，不能分段。
     * 规则调整：上层、下层都按照选型界面右面一个功能段是旁通（混合段或者出风段）时，不能分段。
     *
     * @param ahuLayout
     * @param ahuPartitions
     * @return
     */
    public static boolean validateWheelHeatRecycleBypass(AhuLayout ahuLayout, List<AhuPartition> ahuPartitions) {
        // 转轮回收型
        if (LayoutStyleEnum.WHEEL.style() == ahuLayout.getStyle()) {
            int[] topLeftLayout = ahuLayout.getLayoutData()[0];
            int[] bottomLeftLayout = ahuLayout.getLayoutData()[2];
            int positionOfTopRecyle = -1;
            int positionOfBottomRecyle = -1;
            if (topLeftLayout.length > 0) { // 回收段在最后一个
                positionOfTopRecyle = topLeftLayout[topLeftLayout.length - 1];
            }
            if (bottomLeftLayout.length > 0) { // 回收段在最后一个
                positionOfBottomRecyle = bottomLeftLayout[bottomLeftLayout.length - 1];
            }

            /*if (positionOfTopRecyle != -1) { // 检查上层回收段
                Map<String, Object> lastSectionOfPrePartition = null;
                for (int p = 0; p < ahuPartitions.size(); p++) {
                    AhuPartition ahuPartition = ahuPartitions.get(p);
                    for (int s = 0; s < ahuPartition.getSections().size(); s++) {
                        Map<String, Object> section = ahuPartition.getSections().get(s);
                        int position = AhuPartition.getPosOfSection(section);
                        if (position == positionOfTopRecyle) { // 找到了回收段
                            if (s == 0) {// 回收段是分段中第一个段
                                if (lastSectionOfPrePartition != null) { // 上层气流方向从右到左, 反向检查分段
                                    if ((SectionTypeEnum.TYPE_MIX.getId()
                                            .equals(AhuPartition.getMetaIdOfSection(lastSectionOfPrePartition))
                                            || SectionTypeEnum.TYPE_DISCHARGE.getId().equals(
                                                    AhuPartition.getMetaIdOfSection(lastSectionOfPrePartition)))) {// 上个分段的最后一个段是旁通(混合段或者出风段)
                                        return false; // 不允许分段
                                    }
                                }
                            }
                        } else if (s == ahuPartition.getSections().size() - 1) { // 记录本分段最后一个段
                            lastSectionOfPrePartition = section;
                        }
                    }
                }
            }*/

            if (positionOfBottomRecyle != -1 || positionOfTopRecyle != -1) {
                for (int p = 0; p < ahuPartitions.size(); p++) {
                    AhuPartition ahuPartition = ahuPartitions.get(p);
                    for (int s = 0; s < ahuPartition.getSections().size(); s++) {
                        Map<String, Object> section = ahuPartition.getSections().get(s);
                        int position = AhuPartition.getPosOfSection(section);
                        if (position == positionOfBottomRecyle || position == positionOfTopRecyle) { // 找到了回收段
                            if (s == ahuPartition.getSections().size() - 1) {// 回收段是分段中最后一个段, 下层气流方向从左到右, 正向检查分段
                                if (p < ahuPartitions.size() - 1) {// 获取下一个分段, 再获取分段中的第一个段
                                    AhuPartition nextPartition = ahuPartitions.get(p + 1);
                                    Map<String, Object> firstSectionOfNextPartiton = nextPartition.getSections().get(0);
                                    if ((SectionTypeEnum.TYPE_MIX.getId()
                                            .equals(AhuPartition.getMetaIdOfSection(firstSectionOfNextPartiton))
                                            || SectionTypeEnum.TYPE_DISCHARGE.getId().equals(
                                                    AhuPartition.getMetaIdOfSection(firstSectionOfNextPartiton)))) {// 下个分段的第一个段是旁通(混合段或者出风段)
                                        return false; // 不允许分段
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * 校验返回false，如果, </br>
     * 1. 普通机组：2226及以上机组，超过集装箱高度，需CKD或开顶箱出厂。 </br>
     * 2. 带顶部风阀的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。</br>
     * 3. 带顶棚的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。</br>
     * 4. 带顶部风阀+顶棚的机组：1825及以上机组，超过集装箱高度，需CKD或开顶箱出厂。</br>
     * 
     * @param ahuParam
     * @return
     */
    public static void validateUnitHeightAgainstContainer(AhuParam ahuParam) {
        String isprerain = ahuParam.getParams().get(MetaKey.META_AHU_ISPRERAIN).toString();
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
        String unitNo = AhuUtil.getUnitNo(ahuParam.getSeries());
        if (Boolean.valueOf(isprerain)) { // has roof
            if (hasTopDamper(ahuParam)) {
                // 带顶部风阀+顶棚的机组：1825及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
                if (UNIT_WITH_TOP_DAMPER_AND_ROOF_HEIGHT_CHECK_NO.compareTo(unitNo) < 1
                        && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))) {
                    throw new ApiException(ErrorCode.UNIT_WITH_TOP_DAMPER_AND_ROOF_EXCEED_HEIGHT_NEED_CKD);
                }
            } else {
                // 带顶棚的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
                if (UNIT_WITH_ROOF_HEIGHT_CHECK_NO.compareTo(unitNo) < 1
                        && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))) {
                    throw new ApiException(ErrorCode.UNIT_WITH_ROOF_EXCEED_HEIGHT_NEED_CKD);
                }
            }
        } else {
            if (hasTopDamper(ahuParam)) {
                // 带顶部风阀的机组：2025及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
                if (UNIT_WITH_TOP_DAMPER_HEIGHT_CHECK_NO.compareTo(unitNo) < 1
                        && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))) {
                    throw new ApiException(ErrorCode.UNIT_WITH_TOP_DAMPER_EXCEED_HEIGHT_NEED_CKD);
                }
            } else {
                // 普通机组：2226及以上机组，超过集装箱高度，需CKD或开顶箱出厂。
                if (UNIT_HEIGHT_CHECK_NO.compareTo(unitNo) < 1
                        && !(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                                || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))) {
                    throw new ApiException(ErrorCode.NORMAL_UNIT_EXCEED_HEIGHT_NEED_CKD);
                }
            }
        }
    }

    private static boolean hasTopDamper(AhuParam ahuParam) {
        List<PartParam> partParams = ahuParam.getPartParams();
        for (PartParam partParam : partParams) {
            if (SectionTypeEnum.TYPE_MIX.getId().equals(partParam.getKey())) {
                String returnTop = partParam.getParams()
                        .get(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, MetaKey.KEY_RETURNTOP))
                        .toString();
                return Boolean.valueOf(returnTop);
            } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(partParam.getKey())) {
                String outletDirection = partParam.getParams().get(
                        SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, MetaKey.KEY_OUTLETDIRECTION))
                        .toString();
                return SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_T.equals(outletDirection);
            } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId().equals(partParam.getKey())) {
                return true;
            }else if (SectionTypeEnum.TYPE_FAN.getId().equals(partParam.getKey())) {
                String outletDirectionReturn = String.valueOf(partParam.getParams()
                        .get(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_RETURNPOSITION))
                        .toString());
                String outletDirectionSend = String.valueOf(partParam.getParams()
                        .get(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_SENDPOSITION)));
                return SystemCalculateConstants.FAN_SENDPOSITION_TOP.equals(outletDirectionReturn)
                        || SystemCalculateConstants.FAN_SENDPOSITION_TOP.equals(outletDirectionSend);
            }
        }
        return false;
    }
    
    
    private static int hasFaceDamper(AhuPartition ahuParam) {
        int num=0;
        for (int s = 0; s < ahuParam.getSections().size(); s++) {
            Map<String, Object> section = ahuParam.getSections().get(s);
            String secionStr=ahuParam.getMetaJsonOfSection(section);
            Map<String,String> unitMap=JSON.parseObject(secionStr,HashMap.class);
            String sectionId=AhuPartition.getMetaIdOfSection(section);
            if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionId)) {
                String returnBack = String.valueOf(unitMap
                        .get(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_MIX, MetaKey.KEY_RETURN_BACK)));
               if("true".equals(returnBack)) {
            	   num++;
               }
            } else if (SectionTypeEnum.TYPE_DISCHARGE.getId().equals(sectionId)) {
                String outletDirection = String.valueOf(unitMap.get(
                        SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_DISCHARGE, MetaKey.KEY_oUTLETDIRECTION)));
                if(SystemCalculateConstants.DISCHARGE_OUTLETDIRECTION_A.equals(outletDirection)) {
                	num++;
                }
            }else if (SectionTypeEnum.TYPE_FAN.getId().equals(sectionId)) {
                String outletDirectionReturn = String.valueOf(unitMap.get(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_RETURNPOSITION)));
                String outletDirectionSend = String.valueOf(unitMap.get(SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_SENDPOSITION)));
                if(SystemCalculateConstants.FAN_SENDPOSITION_FACE.equals(outletDirectionReturn)
                        || SystemCalculateConstants.FAN_SENDPOSITION_FACE.equals(outletDirectionSend)) {
                	num++;
                }
            }
        }
        return num;
    }

    /**
     * 1. 1420及以上所有机组：如果机组段长≤20M，可选普通集装箱；在21~27M之间，需CKD或特种箱出厂。如果段长>27M, 只能选用CKD</br>
     * 2. 有顶棚的机组：1418及以上，段长18M及以上，超过集装箱宽度，顶棚需现场安装。</br>
     * 
     * @param ahuParam
     * @param totalPartitionLength
     */
    public static void validateUnitWidthAgainstContainer(AhuParam ahuParam, int totalPartitionLength) {
        String isprerain = ahuParam.getParams().get(MetaKey.META_AHU_ISPRERAIN).toString();
        String delivery = ahuParam.getParams().get(MetaKey.META_AHU_DELIVERY).toString();
        String unitNo = AhuUtil.getUnitNo(ahuParam.getSeries());
        if (UNIT_LENGTH_CKD_CHECK_NO.compareTo(unitNo) < 1) { // 1420及以上所有机组
            if (totalPartitionLength <= AhuUtil.getRealSize(PARTITION_FOR_NORMAL_CONTAINER_LENGTH)) {
                // 机组段长≤20M，可选普通集装箱
                // 可以选择任何一个出厂方式
            } else if (totalPartitionLength >= AhuUtil.getRealSize(PARTITION_LENGTH_CKD_RANGE_FROM)
                    && totalPartitionLength <= AhuUtil.getRealSize(PARTITION_LENGTH_CKD_RANGE_TO)) {
                // 段长在21~27M之间，需CKD或特种箱出厂
                if (!(SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)
                        || SystemCalculateConstants.AHU_DELIVERY_SECTIONOTFR.equals(delivery))) {
                    throw new ApiException(ErrorCode.PARTITION_LENGHT_BETWEEN_21M_TO_27M_NEED_CKD_OR_SPECIAL_CONTAINER);
                }
            } else if (totalPartitionLength > AhuUtil.getRealSize(PARTITION_LENGTH_CKD_RANGE_TO)) {
                // 段长>27M, 只能选用CKD
                if (!SystemCalculateConstants.AHU_DELIVERY_CKD.equals(delivery)) {
                    throw new ApiException(ErrorCode.PARTITION_LENGHT_GREATER_THAN_27M_NEED_CKD);
                }
            }
        }

        // 有顶棚的机组：1418及以上，段长18M及以上，超过集装箱宽度，顶棚需现场安装。
        if (Boolean.valueOf(isprerain) && UNIT_WITH_ROOF_ONSITE_INSTALL_LENGTH_CHECK_NO.compareTo(unitNo) < 1
                && totalPartitionLength >= AhuUtil.getRealSize(PARTITION_WITH_ROOF_ONSITE_INSTALL_LENGTH)) {
            throw new ApiException(ErrorCode.PARTITION_LENGTH_GREATER_THAN_18M_NEED_TO_INSTALL_ONSITE);
        }
    }

    /**
     * 0608~0711 3M <br/>
     * 0811~0914 4M <br/>
     * 1015~1117 5M <br/>
     * 1317~1420 6M <br/>
     * 1621~2333 6M <br/>
     * 
     * @param ahuPartition
     * @param ahuParam
     * @return
     */
    public static boolean validateMinimumPartitionLength(AhuPartition ahuPartition, AhuParam ahuParam) {
        String unitNo = AhuUtil.getUnitNo(ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString());
        int index = getIndexOfMinimumCheckNo(unitNo);
        if (index != -1) {
            return (ahuPartition.getLength() >= AhuUtil
                    .getRealSize(Integer.valueOf(MINIMUM_PARTITION_LENGTH_CHECK_NOS[index][2])));
        }
        return true;
    }
    /**
     * 如果是39CQ 2435、2635、2735这三个机组的分段段长需要限制在1米以上（即>=10M）
     * @param ahuPartition
     * @param ahuParam
     * @return
     */
    public static boolean validateNewUnitPartitionLengthMin(AhuPartition ahuPartition, AhuParam ahuParam) {
        String unitNo = ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString();
        if(NEW_UNIT_LENGTH_CHECK_NOS.contains(unitNo)) {
        	return ahuPartition.getLength() >= 1000;
        }
        return true;
    }
    /**
     * 如果是39CQ 2435、2635、2735这三个机组的分段段长需要限制在3米以内（即≤30M），
     * 一个分段只有一个功能段(且风机段)不用限制大小
     * @param ahuPartition
     * @param ahuParam
     * @return
     */
    public static boolean validateNewUnitPartitionLength(AhuPartition ahuPartition, AhuParam ahuParam) {

        // 规则: 装箱段不能只有一个【风机段】
        if(ahuPartition.getSections().size()==1 &&
                (ahuPartition.getSections().get(0).get(SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_FAN.getId())
                 ||ahuPartition.getSections().get(0).get(SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_WWKFAN.getId()))) {
            return true;
        }

        String unitNo = ahuParam.getParams().get(MetaKey.META_AHU_SERIAL).toString();
        if(NEW_UNIT_LENGTH_CHECK_NOS.contains(unitNo)) {
            return ahuPartition.getLength() <= 3000;
        }

        return true;
    }
    /**
     * 除39CQ新增三个型号外其他机组：默认分段时，段长不超过3米；手动分段时，在大于3米的时候分段线变为红色提醒用户过长，但是仍然可以操作，最长限制在4米以内（即≤38M）
     * 一个分段只有一个功能段(且风机段)不用限制大小
     * @param ahuPartition
     * @param ahuParam
     * @return
     */
    public static boolean validateUnitPartitionLength(AhuPartition ahuPartition, AhuParam ahuParam) {
        // 规则: 装箱段不能只有一个【风机段】
        if(ahuPartition.getSections().size()==1 &&
                (ahuPartition.getSections().get(0).get(SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_FAN.getId())
                        ||ahuPartition.getSections().get(0).get(SYS_MAP_METAID).equals(SectionTypeEnum.TYPE_WWKFAN.getId()))) {
            return true;
        }
        return ahuPartition.getLength() <= 3800;
    }

    public static int getIndexOfMinimumCheckNo(String unitNo) {
        for (int i = 0; i < MINIMUM_PARTITION_LENGTH_CHECK_NOS.length; i++) {
            String[] checkNoCase = MINIMUM_PARTITION_LENGTH_CHECK_NOS[i];
            if (checkNoCase[0].compareTo(unitNo) < 1 && checkNoCase[1].compareTo(unitNo) > -1) {
                return i;
            }
        }
        return -1;
    }
}

package com.carrier.ahu.util.meta;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.lang3.math.NumberUtils;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.DamperPosEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.metadata.SectionMetadata;
import com.carrier.ahu.metadata.section.MetaValue;
import com.carrier.ahu.po.meta.RulesVO;
import com.carrier.ahu.po.meta.SerialVO;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Handle section meta values, validations and serial data.
 * 
 * Created by Braden Zhou on 2018/06/19.
 */
public final class SectionMetaUtils {

    public static final String META_SECTION_PREFIX = "meta.section.";
    public static final String INFO_SECTION_PREFIX = "info.section.";
    public static final String NS_SECTION_PREFIX = "ns.section.";
    public static final String META_AHU_PREFIX = "meta.ahu.";
    public static final String META_AHU_HYPHEN_PREFIX = "meta_ahu_";
    public static final String AHU_PREFIX = "ahu.";
    public static final String NS_AHU_PREFIX = "ns.ahu.";
    public static final String NS_AHU_HYPHEN_PREFIX = "ns_ahu_";

    public static final class MetaKey {

        public static final String META_AHU_ISPRERAIN = "meta.ahu.ISPRERAIN";
        public static final String META_AHU_SERIAL = "meta.ahu.serial";
        public static final String META_AHU_DELIVERY = "meta.ahu.delivery";

        private static final String DOT = ".";
        public static final String KEY_ODOOR = "ODoor";
        public static final String KEY_DOORO = "DoorO";
        public static final String KEY_ACCESSDOOR = "accessDoor";
        public static final String KEY_OSDOOR = "OSDoor";
        public static final String KEY_ISDOOR = "ISDoor";
        public static final String KEY_POS_SIZE = "PosSize";
        public static final String KEY_RETURNTOP = "returnTop";
        public static final String KEY_OUTLETDIRECTION = "OutletDirection";
        public static final String KEY_oUTLETDIRECTION = "outletDirection";
        public static final String KEY_RETURNPOSITION = "returnPosition";
        public static final String KEY_SENDPOSITION = "sendPosition";
        public static final String KEY_MEDIALOADING = "mediaLoading";
        public static final String KEY_WEIGHT = "Weight";
        public static final String KEY_SECTIONL = "sectionL";
        public static final String KEY_fIX_REPAIR_LAMP = "fixRepairLamp";
        public static final String KEY_FIX_REPAIR_LAMP = "FixRepairLamp";
        public static final String KEY_UV_LAMP = "uvLamp";
        public static final String KEY_FRESH_AIR_HOOD = "freshAirHood";
        public static final String KEY_DOOR_DIRECTION = "DoorDirection";
        public static final String KEY_DOOR_ON_BOTH_SIDE = "DoorOnBothSide";
        public static final String KEY_dOOR_ON_BOTH_SIDE = "doorOnBothSide";
        public static final String KEY_PRESSURE_GUAGE = "pressureGuage";
        public static final String KEY_PRESSURE_GUAGE_P = "pressureGuageP";
        public static final String KEY_PRESSURE_GUAGE_B = "pressureGuageB";
        public static final String KEY_SUPPLIER = "Supplier";
        public static final String KEY_sUPPLIER = "supplier";
        public static final String KEY_NSCHANGESUPPLIER = "nsChangeSupplier";
        public static final String KEY_NSSUPPLIERPRICE = "nsSupplierPrice";
        //add by gaok2 begin
        public static final String KEY_THICKNESS = "thickness";
        public static final String KEY_HQ = "hq";
        public static final String KEY_FANMODEL = "fanModel";
        public static final String KEY_NS_FANMODEL = "nsFanModel";
        public static final String KEY_NS_FANSUPPLIER = "nsFanSupplier";
        public static final String KEY_NS_MOTORPOWER = "nsMotorPower";
        public static final String KEY_NS_MOTORPOLE = "nsMmotorPole";
        public static final String KEY_MOTORBASENO = "motorBaseNo";
        public static final String KEY_NS_PRESSUREGUAGE = "nsPressureGuage";
        public static final String KEY_NS_PRESSUREGUAGEMODEL = "nsPressureGuageModel";
        public static final String KEY_NS_PRESSUREGUAGECOUNT = "nsPressureGuageCount";
        public static final String KEY_NS_DRAINPANMATERIAL = "nsDrainpanMaterial";
        public static final String KEY_NS_DRAINPANTYPE = "nsDrainpanType";
        //add by gaok2 end
        public static final String KEY_NSCHANGEFAN = "nsChangeFan";
        public static final String KEY_NSFANPRICE = "nsFanPrice";
        public static final String KEY_NSDRAINPAN = "nsDrainpan";
        public static final String KEY_NSDRAINPANPRICE = "nsDrainpanPrice";
        public static final String KEY_NSPRESSUREGUAGEPRICE = "nsPressureGuagePrice";
        public static final String KEY_NSCHANGEMOTOR = "nsChangeMotor";
        public static final String KEY_NSMOTORPRICE = "nsMotorPrice";
        public static final String KEY_NSPRESSUREGUAGE = "nsPressureGuage";
        public static final String KEY_NSCHANGEELIMINATOR = "nsChangeEliminator";
        public static final String KEY_NSSPRINGTRANSFORM = "nsSpringtransform";
        public static final String KEY_NSSPRINGTRANSFORMPRICE = "nsSpringtransformPrice";
        public static final String KEY_PRICE = "Price";
        public static final String KEY_MEMO = "MEMO";
        public static final String KEY_DEFORMATIONPRICE = "deformationPrice";
        public static final String KEY_NS_ENABLE = "enable";
        public static final String KEY_NS_DEFORMATION = "deformation";// 是否变形
        public static final String KEY_FAN_MODEL = "fanModel";
        public static final String KEY_MOTOR_POWER = "motorPower";
        public static final String KEY_POLE = "pole";
        public static final String KEY_WATER_QUANTITY = "WaterQuantity";
        public static final String KEY_RUNNING_WEIGHT = "runningWeight";
        public static final String KEY_AINTERFACE = "AInterface";
        public static final String KEY_EXECUTOR1 = "Executor1";
        public static final String KEY_PRODUCT_PACKING_LIST_NAME = "productPackingListName";
        public static final String KEY_SERIAL = "serial";
        public static final String KEY_RETURN_BACK = "returnBack";
        public static final String KEY_RETURN_BOTTOM = "returnButtom";
        public static final String KEY_RETURN_LEFT = "returnLeft";
        public static final String KEY_RETURN_RIGHT = "returnRight";
        public static final String KEY_RETURN_TOP = "returnTop";
        public static final String KEY_OUTLET = "outlet";
        public static final String KEY_TYPE = "type";
        public static final String KEY_EXTRA_STATIC = "extraStatic";
        public static final String KEY_INSULATION = "insulation";
        public static final String KEY_PROTECTION = "Protection";
        public static final String KEY_FITETF = "fitetF";
        public static final String KEY_ROWS = "rows";
        public static final String KEY_HUMIDIFICATION = "Humidification";
        public static final String KEY_ACTUAL_HUMIDIFICATIONQ = "actualHumidificationQ";
        public static final String KEY_HEAT = "Heat";
        public static final String KEY_SHUMIDIFICATIONQ = "SHumidificationQ";
        public static final String KEY_WHUMIDIFICATIONQ = "WHumidificationQ";
        public static final String KEY_SHEATQ = "SHeatQ";
        public static final String KEY_WHEATQ = "WHeatQ";
        public static final String KEY_ENABLE_WINTER = "EnableWinter";
        public static final String KEY_ENABLE_SUMMER = "EnableSummer";
        public static final String KEY_ENGINE_TYPE = "engineType";
        public static final String KEY_FAN_FRAME = "fanFrame";
        public static final String KEY_NS_CHANNEL_STEEL_BASE = "nsChannelSteelBase";
        public static final String KEY_NS_CHANNEL_STEEL_BASE_MODEL = "nsChannelSteelBaseModel";
        public static final String KEY_NS_CHANNEL_STEEL_BASE_PRICE = "nsChannelSteelBasePrice";
        public static final String KEY_BOX_PRICE = "boxPrice";
        public static final String KEY_DEFORMATION_PRICE = "deformationPrice";
        public static final String KEY_NS_HEIGHT = "nsheight";
        public static final String KEY_NS_WIDTH = "nswidth";
        public static final String KEY_NS_DEFORMATIONSERIAL = "deformationSerial";
        public static final String KEY_WIDTH = "width";
        public static final String KEY_HEIGHT = "height";
        public static final String KEY_PRODUCT = "product";
        public static final String KEY_POSITIVE_PRESSURE_DOOR = "PositivePressureDoor";
        public static final String KEY_dAMPER_METERIAL = "damperMeterial";
        public static final String KEY_DAMPER_METERIAL = "DamperMeterial";
        public static final String KEY_DAMPER_OUTLET = "damperOutlet";
        public static final String KEY_DAMPER_ELIMINATOR = "eliminator";

        public static final Function<DamperPosEnum, String> KEY_DAMPER_MODE_A = (damperPos) -> String
                .format("damper%sModeA", damperPos.name());
        public static final Function<DamperPosEnum, String> KEY_DAMPER_MODE_B = (damperPos) -> String
                .format("damper%sModeB", damperPos.name());
        public static final Function<DamperPosEnum, String> KEY_DAMPER_MODE_C = (damperPos) -> String
                .format("damper%sModeC", damperPos.name());
        public static final Function<DamperPosEnum, String> KEY_DAMPER_MODE_D = (damperPos) -> String
                .format("damper%sModeD", damperPos.name());
        public static final Function<DamperPosEnum, String> KEY_DAMPER_POS_SIZE_A = (damperPos) -> String
                .format("damper%sPosSizeA", damperPos.name());
        public static final Function<DamperPosEnum, String> KEY_DAMPER_POS_SIZE_B = (damperPos) -> String
                .format("damper%sPosSizeB", damperPos.name());
        public static final Function<DamperPosEnum, String> KEY_DAMPER_POS_SIZE_C = (damperPos) -> String
                .format("damper%sPosSizeC", damperPos.name());
        public static final Function<DamperPosEnum, String> KEY_DAMPER_POS_SIZE_D = (damperPos) -> String
                .format("damper%sPosSizeD", damperPos.name());
        public static final Function<DamperPosEnum, String> KEY_DAMPER_SIZE_X = (damperPos) -> String
                .format("damper%sSizeX", damperPos.name());
        public static final Function<DamperPosEnum, String> KEY_DAMPER_SIZE_Y = (damperPos) -> String
                .format("damper%sSizeY", damperPos.name());
        public static final Function<DamperPosEnum, String> KEY_DAMPER_TYPE = (damperPos) -> String
                .format("damper%sType", damperPos.name());

        public static final String KEY_NARATIO = "NARatio";
    }

    public static String getMetaSectionKey(SectionTypeEnum sectionType, String metaKey) {
    	if(metaKey.contains(META_SECTION_PREFIX)) {
    		return metaKey;
    		
    	}
        return META_SECTION_PREFIX + getSectionName(sectionType) + MetaKey.DOT + metaKey;
    }

    /**
     * For user customize. Used for customized info key in code rules.
     * 
     * @param sectionType
     * @param metaKey
     * @return
     */
    public static String getInfoSectionKey(SectionTypeEnum sectionType, String metaKey) {
        return INFO_SECTION_PREFIX + getSectionName(sectionType) + MetaKey.DOT + metaKey;
    }

    public static String getMetaSectionKey(String sectionId, String metaKey) {
        return META_SECTION_PREFIX + getSectionName(sectionId) + MetaKey.DOT + metaKey;
    }

    public static String getInfoSectionKey(String sectionId, String metaKey) {
        return INFO_SECTION_PREFIX + getSectionName(sectionId) + MetaKey.DOT + metaKey;
    }

    public static String getNSSectionKey(String sectionId, String metaKey) {
        return NS_SECTION_PREFIX + getSectionName(sectionId) + MetaKey.DOT + metaKey;
    }

    public static String getMetaAHUKey(String metaKey) {
        return META_AHU_PREFIX + metaKey;
    }
    public static String getMetaAHUHyphenKey(String metaKey) {
        return META_AHU_HYPHEN_PREFIX + metaKey;
    }

    public static String getNSAHUKey(String metaKey) {
        return NS_AHU_PREFIX + metaKey;
    }
    public static String getNSAHUHyphenKey(String metaKey) {
        return NS_AHU_HYPHEN_PREFIX + metaKey;
    }

    private static String getSectionName(SectionTypeEnum sectionType) {
        return sectionType.getId().replaceAll(AHU_PREFIX, "");
    }

    private static String getSectionName(String sectionId) {
        return sectionId.replaceAll(AHU_PREFIX, "");
    }

    /**
     * Get default value which set up in meta values json file.
     * 
     * @param key
     * @param unitSeries '39CQ' use factory meta values if not set.
     * @return
     */
    public static String getDefaultValue(String key, String unitSeries) {
        String factory = AHUContext.getFactoryName();
        Map<String, MetaValue> sectionMetaValues = SectionMetadata.getMetaValue(factory, unitSeries);
        if (sectionMetaValues.containsKey(key)) {
            return sectionMetaValues.get(key).getDefaultValue();
        }
        return "";
    }

    public static Map<String, MetaValue> getSectionMetaValues(String unitSeries) {
        String factory = AHUContext.getFactoryName();
        return SectionMetadata.getMetaValue(factory, unitSeries);
    }

    public static Map<String, List<RulesVO>> getSectionMetaValidations(String unitSeries) {
        String factory = AHUContext.getFactoryName();
        return SectionMetadata.getMetaValidation(factory, unitSeries);
    }

    /**
     * Use 39CQ as default.
     * 
     * @param unitSeries
     * @return
     */
    public static Map<String, List<SerialVO>> getSectionMetaSerial(String unitSeries) {
        String factory = AHUContext.getFactoryName();
        if (EmptyUtil.isEmpty(unitSeries)) {
            unitSeries = "39CQ"; // TODO remove it later.
        }
        return SectionMetadata.getMetaSerial(factory, unitSeries);
    }

    /**
     * Get meta value which set up in meta values json file.
     * 
     * @param key
     * @param unitSeries '39CQ' use factory meta values if not set.
     * @return
     */
    public static MetaValue getSectionMetaValue(String key, String unitSeries) {
        String factory = AHUContext.getFactoryName();
        Map<String, MetaValue> sectionMetaValues = SectionMetadata.getMetaValue(factory, unitSeries);
        return sectionMetaValues.get(key);
    }

    public static String getMetaSectionKeyPrefix(String sectionkey) {
        if (EmptyUtil.isEmpty(sectionkey)) {
            return null;
        }
        if (sectionkey.startsWith(AHU_PREFIX)) {
            return sectionkey.replace(AHU_PREFIX, META_SECTION_PREFIX);
        }
        return sectionkey;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> getSectionMeta(Part section) {
        return JSON.parseObject(section.getMetaJson(), Map.class);
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> getUnitMeta(Unit unit) {
        return JSON.parseObject(unit.getMetaJson(), Map.class);
    }

    public static String getSectionMetaValueString(SectionTypeEnum sectionType, String metaKey,
            Map<String, Object> sectionMeta) {
        return getSectionMetaValueString(getMetaSectionKey(sectionType, metaKey), sectionMeta);
    }

    public static String getSectionMetaValueString(String metaKey, Map<String, Object> sectionMeta) {
        return String.valueOf(sectionMeta.get(metaKey));
    }

    public static int getSectionMetaValueInt(SectionTypeEnum sectionType, String metaKey,
            Map<String, Object> sectionMeta) {
        return NumberUtils.toInt(getSectionMetaValueString(sectionType, metaKey, sectionMeta));
    }

    public static double getSectionMetaValueDouble(SectionTypeEnum sectionType, String metaKey,
            Map<String, Object> sectionMeta) {
        return NumberUtils.toDouble(getSectionMetaValueString(sectionType, metaKey, sectionMeta));
    }

    public static double getSectionMetaValueDouble(String metaKey, Map<String, Object> sectionMeta) {
        return NumberUtils.toDouble(getSectionMetaValueString(metaKey, sectionMeta));
    }

}

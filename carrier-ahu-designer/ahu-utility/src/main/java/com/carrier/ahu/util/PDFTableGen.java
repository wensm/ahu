package com.carrier.ahu.util;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.enums.SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER;
import static com.carrier.ahu.common.intl.I18NConstants.AHRI_WARNNING;
import static com.carrier.ahu.common.intl.I18NConstants.COOLINGCOIL_COPPER_WARNNING;
import static com.carrier.ahu.common.intl.I18NConstants.META_COOLINGCOIL_FINTYPE_WARNING;
import static com.carrier.ahu.report.common.ReportConstants.*;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_POS_SIZE;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Map;

import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.report.common.ReportConstants;
import com.carrier.ahu.report.content.ReportContent;
import com.carrier.ahu.report.pdf.PDFTableGenerator;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PDFTableGen {

	/**
	 * 添加柏诚报告段
	 * @param document
	 * @param key
	 * @param pos
	 * @param order
	 * @param noChangeMap
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void addSectionBoCheng(Document document, String key, String pos, int order, Map<String, String> noChangeMap, Map<String, String> allMap,
										 LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit)
            throws DocumentException, IOException {
		switch (SectionTypeEnum.getSectionTypeFromId(key)) {
			case TYPE_FAN: {
				allMap = SectionDataConvertUtils.getFan(noChangeMap,allMap, language, null);
				PDF4ReportUtils.addPartName(document, genBaiChengName(pos, order, SectionTypeEnum.TYPE_FAN, language));
				String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_FAN), project, unit, allMap, language,
						unitType);
				contents = SectionContentConvertUtils.getBCfan(contents, allMap, language);
				document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_FAN));
				return;
			}
            case TYPE_COLD: {
                allMap = SectionDataConvertUtils.getCoolingCoil(noChangeMap,allMap, language);
                PDF4ReportUtils.addPartName(document, genBaiChengName(pos, order, SectionTypeEnum.TYPE_COLD, language));
                String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_COOLINGCOIL), project, unit, allMap,
                        language, unitType);
                document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_COOLINGCOIL));

                return;
            }
			case TYPE_HEATINGCOIL: {
				allMap = SectionDataConvertUtils.getHeatingcoil(noChangeMap,allMap, language);
				PDF4ReportUtils.addPartName(document, genBaiChengName(pos, order, SectionTypeEnum.TYPE_HEATINGCOIL, language));
				String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_HEATINGCOIL), project, unit, allMap, language,
						unitType);
				contents = SectionContentConvertUtils.getBCheatingcoil(contents, allMap, language);
				document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_HEATINGCOIL));
				return;
			}
			case TYPE_SINGLE: {
				allMap = SectionDataConvertUtils.getFilter(noChangeMap,allMap, language);
				PDF4ReportUtils.addPartName(document, genBaiChengName(pos,order,SectionTypeEnum.TYPE_SINGLE, language));
				String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_FILTER), project, unit, allMap,
						language, unitType);
				// 封装过滤段特殊格式数据
				contents = SectionContentConvertUtils.getBCFilter(contents, allMap, SectionTypeEnum.TYPE_SINGLE);
				document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_FILTER));

				return;
			}
			case TYPE_COMPOSITE: {
				allMap = SectionDataConvertUtils.getCombinedFilter(noChangeMap,allMap, language);
				PDF4ReportUtils.addPartName(document, genBaiChengName(pos, order,SectionTypeEnum.TYPE_COMPOSITE, language));
				String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_FILTER), project, unit, allMap,
						language, unitType);
                // 封装过滤段特殊格式数据
                contents = SectionContentConvertUtils.getBCFilter(contents, allMap, SectionTypeEnum.TYPE_COMPOSITE);
				document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_FILTER));

				return;
			}
			case TYPE_HEPAFILTER: {
				allMap = SectionDataConvertUtils.getHEPAFilter(noChangeMap,allMap, language);
                PDF4ReportUtils.addPartName(document, genBaiChengName(pos, order,SectionTypeEnum.TYPE_HEPAFILTER, language));
				String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_FILTER), project, unit, allMap, language,
						unitType);
                // 封装过滤段特殊格式数据
                contents = SectionContentConvertUtils.getBCFilter(contents, allMap, SectionTypeEnum.TYPE_HEPAFILTER);
				document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_FILTER));

				return;
			}
			case TYPE_ELECTROSTATICFILTER: {
				allMap = SectionDataConvertUtils.getElectrostaticFilter(noChangeMap,allMap, language);
                PDF4ReportUtils.addPartName(document, genBaiChengName(pos, order,SectionTypeEnum.TYPE_ELECTROSTATICFILTER, language));
				String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_FILTER), project, unit, allMap, language,
						unitType);
                // 封装过滤段特殊格式数据
                contents = SectionContentConvertUtils.getBCFilter(contents, allMap, SectionTypeEnum.TYPE_ELECTROSTATICFILTER);
				document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_FILTER));

				return;
			}
			case TYPE_WETFILMHUMIDIFIER: {
				allMap = SectionDataConvertUtils.getWetfilmHumidifier(noChangeMap,allMap, language);
				PDF4ReportUtils.addPartName(document, genBaiChengName(pos, order, SectionTypeEnum.TYPE_WETFILMHUMIDIFIER, language));
				String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_WET), project, unit, allMap, language,
						unitType);
				// 封装过滤段特殊格式数据
				contents = SectionContentConvertUtils.getBCWet(contents, allMap, SectionTypeEnum.TYPE_WETFILMHUMIDIFIER);
				document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_WET));
				return;
			}
			case TYPE_STEAMHUMIDIFIER: {
				allMap = SectionDataConvertUtils.getSteamHumidifier(noChangeMap,allMap, language);
				PDF4ReportUtils.addPartName(document, genBaiChengName(pos, order, SectionTypeEnum.TYPE_STEAMHUMIDIFIER, language));
				String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_WET), project, unit, allMap, language,
						unitType);
				// 封装过滤段特殊格式数据
				contents = SectionContentConvertUtils.getBCWet(contents, allMap, SectionTypeEnum.TYPE_STEAMHUMIDIFIER);
				document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_WET));
				return;
			}
			case TYPE_SPRAYHUMIDIFIER: {
				allMap = SectionDataConvertUtils.getSprayHumidifier(noChangeMap,allMap, language);
				PDF4ReportUtils.addPartName(document, genBaiChengName(pos, order, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER, language));
				String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_WET), project, unit, allMap, language,
						unitType);
				// 封装过滤段特殊格式数据
				contents = SectionContentConvertUtils.getBCWet(contents, allMap, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER);
				document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_WET));
				return;
			}
			case TYPE_ELECTRODEHUMIDIFIER: {
				allMap = SectionDataConvertUtils.getElectrodeHumidifier(noChangeMap,allMap, language);
				PDF4ReportUtils.addPartName(document, genBaiChengName(pos, order, SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER, language));
				String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_T_BOCHENG_WET), project, unit, allMap,
						language, unitType);
				// 封装过滤段特殊格式数据
				contents = SectionContentConvertUtils.getBCWet(contents, allMap, SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER);
				document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_T_BOCHENG_WET));
				return;
			}
			default:
				log.warn(MessageFormat.format("技术说明报告-AHU段信息未知，报告忽略声称对应段信息 - unitNo：{0},partTypeCode:{1}", unit.getUnitNo(),
						key));
		}
	}
	@SuppressWarnings("unused")
    public static void addSection(Document document, String key, String pos, int order, Map<String, String> noChangeMap, Map<String, String> allMap,
								  LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, AhuParam ahuParam)
            throws DocumentException, IOException {
		switch (SectionTypeEnum.getSectionTypeFromId(key)) {
		case TYPE_MIX: {
			allMap = SectionDataConvertUtils.getMix(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos,order, SectionTypeEnum.TYPE_MIX, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_MIX), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getMix(contents,noChangeMap, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_MIX));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_MIX_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_MIX_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents2, ReportConstants.REPORT_TECH_MIX_SUMMER));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_MIX_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_MIX_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents3, ReportConstants.REPORT_TECH_MIX_SUMMER));
			}

			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_MIX);

			return;
		}
		case TYPE_SINGLE: {
			allMap = SectionDataConvertUtils.getFilter(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_SINGLE, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FILTER), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getSingle(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_FILTER));

			//添加单层过滤段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_FILTER_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				// 添加非标选项
				addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_SINGLE);
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_FILTER), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_SINGLE.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getSingleNS(nsAHUcontents, unit, language, allMap);
				if(EmptyUtil.isNotEmpty(nsAHUcontents))
				document.add(PDFTableGenerator.generate(nsAHUcontents, ReportConstants.REPORT_NS_FILTER));
			}
			
			return;
		}
		case TYPE_COMPOSITE: {
			allMap = SectionDataConvertUtils.getCombinedFilter(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_COMPOSITE, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COMBINEDFILTER), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getComposite(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_COMBINEDFILTER));
			
			//袋式压差计
			Object pressureGuageB = allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEB);
			String valuePGB = BaseDataUtil.constraintString(pressureGuageB);
			//板式压差计
			Object pressureGuageP = allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEP);
			String valuePGP = BaseDataUtil.constraintString(pressureGuageP);
			if (!UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEB_WO.equals(valuePGB)
					|| !UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEP_WO.equals(valuePGP)) {// 压差计展示
				String[][] contentAppendix = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COMBINEDFILTER_APPENDIX), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentAppendix, ReportConstants.REPORT_TECH_COMBINEDFILTER_APPENDIX));
			}
			
			//添加综合过滤段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_COMBINEDFILTER_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				// 添加非标选项
				addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_COMPOSITE);
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_COMPOSITE), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_COMPOSITE.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getCompositeNS(nsAHUcontents, unit, language, allMap);
				if(EmptyUtil.isNotEmpty(nsAHUcontents))
				document.add(PDFTableGenerator.generate(nsAHUcontents, ReportConstants.REPORT_NS_COMPOSITE));
			}
			return;
		}
		case TYPE_COLD: {
			allMap = SectionDataConvertUtils.getCoolingCoil(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_COLD, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COOLINGCOIL), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getCoolingCoil(contents, allMap, language, unitType);// 定制转换
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_COOLINGCOIL));

			Object enableSummer = allMap.get(UtilityConstant.METASEXON_COOLINGCOIL_ENABLESUMMER);
			String enableSummerValue = BaseDataUtil.constraintString(enableSummer);
			if (enableSummerValue.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsSummer = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COOLINGCOIL_SUMMER), project, unit, allMap,
						language, unitType);
				contentsSummer = SectionContentConvertUtils.getCoolingCoilSummer(contentsSummer, allMap, language, unitType);// 定制转换
				document.add(PDFTableGenerator.generate(contentsSummer, ReportConstants.REPORT_TECH_COOLINGCOIL));
			}


			Object enableWinter = allMap.get(UtilityConstant.METASEXON_COOLINGCOIL_ENABLEWINTER);
			String value = BaseDataUtil.constraintString(enableWinter);
			if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COOLINGCOIL_WINTER), project, unit,
							allMap, language, unitType);
				contents1 = SectionContentConvertUtils.getCoolingCoilWinner(contents1, allMap, language, unitType);
				document.add(PDFTableGenerator.generate(contents1, ReportConstants.REPORT_TECH_COOLINGCOIL));
			}

			// 添加AHRI标识
			Object ahri = allMap.get(UtilityConstant.METASEXON_COOLINGCOIL_AHRI);
			String ahriV = BaseDataUtil.constraintString(ahri);
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(ahriV)) {
				String[][] contentsAhri1 = ValueFormatUtil.transReport(
						ReportContent.getReportContent(REPORT_TECH_FAN_AHRI), project, unit, allMap, language,
						unitType);
				document.add(PDFTableGenerator.generate(contentsAhri1, ReportConstants.REPORT_TECH_FAN_AHRI));
			} else {
				Paragraph paragraph = PDF4ReportUtils.getChineseParagraph(getIntlString(AHRI_WARNNING),
						CONTENT_NOMAL);
				paragraph.setAlignment(Element.ALIGN_LEFT);
				document.add(paragraph);
			}
			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_COLD);
			if ("铜翅片".equals(String.valueOf(allMap.get(UtilityConstant.METASEXON_COOLINGCOIL_FINTYPE)))) {
				Paragraph paragraph = PDF4ReportUtils.getChineseParagraph(getIntlString(COOLINGCOIL_COPPER_WARNNING), CONTENT_NOMAL);
				paragraph.setAlignment(Element.ALIGN_LEFT);
				document.add(paragraph);
			}
			
			//铜管铜片的盘管是非标盘管，需要在价格单和技术报告中需提醒
			String isCopper = BaseDataUtil.constraintString(allMap.get(UtilityConstant.METASEXON_COOLINGCOIL_FINTYPE));
			if (UtilityConstant.JSON_COOLINGCOIL_FINTYPE_COPPER.equals(isCopper)) {
				Paragraph paragraph = PDF4ReportUtils.getChineseParagraph(getIntlString(META_COOLINGCOIL_FINTYPE_WARNING),
						CONTENT_NOMAL);
				paragraph.setAlignment(Element.ALIGN_LEFT);
				document.add(paragraph);
			}
			return;
		}
		case TYPE_DIRECTEXPENSIONCOIL: {
			allMap = SectionDataConvertUtils.getDXCoil(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_DIRECTEXPENSIONCOIL), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getDirectexpensioncoil(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_COOLINGCOIL));
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_DXCOIL_ENABLEWINTER);
			String value = BaseDataUtil.constraintString(enableWinter);
			if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_DIRECTEXPENSIONCOIL_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents1, ReportConstants.REPORT_TECH_COOLINGCOIL));
			}
			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit,
					SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL);
			return;
		}
		case TYPE_HEATINGCOIL: {
			allMap = SectionDataConvertUtils.getHeatingcoil(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_HEATINGCOIL, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEATINGCOIL), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getHeatingCoil(contents, allMap, language, unitType);// 定制转换
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_HEATINGCOIL));
			//添加AHRI标识
			Object wAhri = allMap.get(UtilityConstant.METASEXON_HEATINGCOIL_AHRI);
			String wAhriV = BaseDataUtil.constraintString(wAhri);
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(wAhriV)) {
				String[][] contentsAhri = ValueFormatUtil.transReport(
						ReportContent.getReportContent(REPORT_TECH_FAN_AHRI), project, unit, allMap, language,
						unitType);
				document.add(PDFTableGenerator.generate(contentsAhri, ReportConstants.REPORT_TECH_FAN_AHRI));
			} else {
				Paragraph paragraph = PDF4ReportUtils.getChineseParagraph(getIntlString(AHRI_WARNNING), CONTENT_NOMAL);
				paragraph.setAlignment(Element.ALIGN_LEFT);
				document.add(paragraph);
			}
			
			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_HEATINGCOIL);
			//铜管铜片的盘管是非标盘管，需要在价格单和技术报告中需提醒
			String isCopper = BaseDataUtil.constraintString(allMap.get(UtilityConstant.METASEXON_HEATINGCOIL_FINTYPE));
			if (UtilityConstant.JSON_COOLINGCOIL_FINTYPE_COPPER.equals(isCopper)) {
				Paragraph paragraph = PDF4ReportUtils.getChineseParagraph(getIntlString(META_COOLINGCOIL_FINTYPE_WARNING),
						CONTENT_NOMAL);
				paragraph.setAlignment(Element.ALIGN_LEFT);
				document.add(paragraph);
			}
			return;
		}
		case TYPE_STEAMCOIL: {
			allMap = SectionDataConvertUtils.getSteamCoil(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_STEAMCOIL, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMCOIL), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getTechSte(contents, allMap, language);
			
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_STEAMCOIL));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_STEAMCOIL_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsS = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMCOIL_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentsS, ReportConstants.REPORT_TECH_STEAMCOIL));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_STEAMCOIL_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsW = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMCOIL_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentsW, ReportConstants.REPORT_TECH_STEAMCOIL));
			}

			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_STEAMCOIL);
			return;
		}
		case TYPE_ELECTRICHEATINGCOIL: {
			allMap = SectionDataConvertUtils.getElectricHeatingCoil(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRICHEATINGCOIL), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getElectricheatingcoil(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_ELECTRICHEATINGCOIL));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsS = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRICHEATINGCOIL_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentsS, ReportConstants.REPORT_TECH_ELECTRICHEATINGCOIL));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsW = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRICHEATINGCOIL_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentsW, ReportConstants.REPORT_TECH_ELECTRICHEATINGCOIL));
			}

			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit,
					SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL);
			return;
		}
		case TYPE_STEAMHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getSteamHumidifier(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_STEAMHUMIDIFIER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMHUMIDIFIER), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getSteamhumidifier(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_STEAMHUMIDIFIER));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents2, ReportConstants.REPORT_TECH_STEAMHUMIDIFIER));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_STEAMHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents3, ReportConstants.REPORT_TECH_STEAMHUMIDIFIER));
			}

			//添加干蒸加湿段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_STEAMHUMIDIFIER_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				// 添加非标选项
				addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_STEAMHUMIDIFIER);
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(ReportConstants.REPORT_NS_STEAMHUMIDIFIER), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getSteamhumidifierNS(nsAHUcontents, unit, language, allMap);
				if(EmptyUtil.isNotEmpty(nsAHUcontents))
				document.add(PDFTableGenerator.generate(nsAHUcontents, ReportConstants.REPORT_NS_STEAMHUMIDIFIER));
			}
			
			return;
		}
		case TYPE_WETFILMHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getWetfilmHumidifier(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_WETFILMHUMIDIFIER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_WETFILMHUMIDIFIER), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getWetFilmHumidifier(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_WETFILMHUMIDIFIER));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_WETFILMHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents2, ReportConstants.REPORT_TECH_WETFILMHUMIDIFIER));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_WETFILMHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents3, ReportConstants.REPORT_TECH_WETFILMHUMIDIFIER));
			}

			//添加湿膜加湿段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_WETFILMHUMIDIFIER_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				// 添加非标选项
				addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_WETFILMHUMIDIFIER);
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(ReportConstants.REPORT_NS_WETFILMHUMIDIFIER), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getWethumidifierNS(nsAHUcontents, unit, language, allMap);
				if(EmptyUtil.isNotEmpty(nsAHUcontents))
				document.add(PDFTableGenerator.generate(nsAHUcontents, ReportConstants.REPORT_NS_WETFILMHUMIDIFIER));
			}
			return;
		}
		case TYPE_SPRAYHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getSprayHumidifier(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_SPRAYHUMIDIFIER), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getSprayhumidifier(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_SPRAYHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents2, ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_SPRAYHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents3, ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER));
			}
			//增加提示
			String[][] contentAppendix = ValueFormatUtil.transReport(ReportContent.getReportContent(ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER_APPENDIX), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contentAppendix, ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER_APPENDIX));
			//添加高压喷雾段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_SPRAYHUMIDIFIER_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				// 添加非标选项
				addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER);
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(ReportConstants.REPORT_NS_SPRAYHUMIDIFIER), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getSprayhumidifierNS(nsAHUcontents, unit, language, allMap);
				if(EmptyUtil.isNotEmpty(nsAHUcontents))
				document.add(PDFTableGenerator.generate(nsAHUcontents, ReportConstants.REPORT_NS_SPRAYHUMIDIFIER));
			}
			return;
		}
		case TYPE_FAN: {
			allMap = SectionDataConvertUtils.getFan(noChangeMap,allMap, language,ahuParam);
			/* 当类型为风机：根据风向 送风、回风重新封装报告名称 */
			String fanName = genName(pos, order, SectionTypeEnum.TYPE_FAN, language);
			fanName = SectionDataConvertUtils.getFanName(noChangeMap,allMap, fanName, language);
			PDF4ReportUtils.addPartName(document, fanName);
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN), project, unit, allMap, language,
					unitType);
			// Eurovent update begin
			if (SystemCountUtil.isEuroUnit(unit)) {
				contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_EURO), project, unit,
						allMap, language, unitType);
				contents = SectionContentConvertUtils.getFanEuro(contents,noChangeMap, allMap, language);
			}else {
			// 封装风机段特殊字段
			contents = SectionContentConvertUtils.getFan(contents,noChangeMap, allMap, language);
			}
			// Eurovent update end
			// 封装风机段特殊字段
//			contents = SectionContentConvertUtils.getFan(contents,noChangeMap, allMap, language);

			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_FAN));
			String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_SOUND), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents2, ReportConstants.REPORT_TECH_FAN_SOUND));
			String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_ABSORBER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents3, ReportConstants.REPORT_TECH_FAN_ABSORBER));
			String[][] contents5 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_REMARK), project, unit, allMap,
					language, unitType);
			contents5 = SectionContentConvertUtils.getFanRemark(contents5,unit, language, noChangeMap);
			document.add(PDFTableGenerator.generate(contents5, ReportConstants.REPORT_TECH_FAN_REMARK));
			
			//添加风机过滤段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_FAN_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_FAN);
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_FAN), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_HEPAFILTER.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getFanNS(nsAHUcontents, unit, language, allMap);
				if(EmptyUtil.isNotEmpty(nsAHUcontents))
				document.add(PDFTableGenerator.generate(nsAHUcontents, ReportConstants.REPORT_NS_FAN));
			}
			
//			//添加AHRI标识
//			String ahriPath = SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR;			
//			ahriPath += "AHRI_LOGO.png";			
//
//			Image imgAhri = Image.getInstance(ahriPath);
//			imgAhri.scalePercent(20);
//
//			float x = 36;
//			float y = (PageSize.A4.getHeight() - imgAhri.getScaledHeight()) - 150;
//			imgAhri.setAbsolutePosition(x, y);
//
////			Paragraph info = new Paragraph();
////			info.add(new Chunk(UtilityConstant.SYS_BLANK));
////			info.setSpacingAfter(90f);// 设置段落下空白，放置图片
////			document.add(info);
//			document.add(imgAhri);
			
			return;
		}
		case TYPE_COMBINEDMIXINGCHAMBER: {
			allMap = SectionDataConvertUtils.getCombinedMixingChamber(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COMBINEDMIXINGCHAMBER), project, unit, allMap, language,
					unitType);

			if("true".equals(String.valueOf(allMap.get("osNoDoor")))){
				contents[8][5] = "--";
			}else{
				String PositivePressureDoor = BaseDataUtil.constraintString(noChangeMap.get(UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_POSITIVEPRESSUREDOOR));//正压门
				if (Boolean.parseBoolean(PositivePressureDoor)) {
					contents[8][5] = allMap.get(SectionMetaUtils.getMetaSectionKey(TYPE_COMBINEDMIXINGCHAMBER, KEY_POS_SIZE));
				}
			}
			if("true".equals(String.valueOf(allMap.get("isNoDoor")))){
				contents[12][5] = "--";
			}

			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_COMBINEDMIXINGCHAMBER));
			String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COMBINEDMIXINGCHAMBER_SUMMER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents1, ReportConstants.REPORT_TECH_COMBINEDMIXINGCHAMBER_SUMMER));
			
			Object enableWinter = allMap.get(UtilityConstant.META_SECTION_COMBINEDMIXINGCHAMBER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
			String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_COMBINEDMIXINGCHAMBER_WINTER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents2, ReportConstants.REPORT_TECH_COMBINEDMIXINGCHAMBER_WINTER));
			}
			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit,
					SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER);

			return;
		}
		case TYPE_ATTENUATOR: {
			allMap = SectionDataConvertUtils.getAttenuator(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_ATTENUATOR, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ATTENUATOR), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getAttenuator(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_ATTENUATOR));

			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_ATTENUATOR);
			return;
		}
		case TYPE_DISCHARGE: {
			allMap = SectionDataConvertUtils.getDischarge(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_DISCHARGE, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_DISCHARGE), project, unit, allMap, language,
					unitType);
			// 封装出风段特殊字段
			contents = SectionContentConvertUtils.getDischarge(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_DISCHARGE));

			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_DISCHARGE);

			return;
		}
		case TYPE_ACCESS: {
			allMap = SectionDataConvertUtils.getAccess(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_ACCESS, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ACCESS), project, unit, allMap, language,
					unitType);
			// 封装空段特殊字段
			contents = SectionContentConvertUtils.getAccess(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_ACCESS));

			// 添加非标选项
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_ACCESS_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_ACCESS);
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_ACCESS), project, unit, allMap, language,
						unitType);
				nsAHUcontents = SectionContentConvertUtils.getAccessNS(nsAHUcontents, unit, language, allMap);

				if(EmptyUtil.isNotEmpty(nsAHUcontents))
					document.add(PDFTableGenerator.generate(nsAHUcontents, ReportConstants.REPORT_NS_ACCESS));
			}
			return;
		}
		case TYPE_HEPAFILTER: {
			allMap = SectionDataConvertUtils.getHEPAFilter(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_HEPAFILTER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEPAFILTER), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getHepafilter(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_HEPAFILTER));

			//添加高效过滤段非标
			String enable = String.valueOf(allMap.get(UtilityConstant.METANS_HEPAFILTER_ENABLE));//是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				// 添加非标选项
				addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_HEPAFILTER);
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_HEPAFILTER), project, unit, allMap, language,
						unitType);
//				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_HEPAFILTER.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getHepaNS(nsAHUcontents, unit, language, allMap);
				if(EmptyUtil.isNotEmpty(nsAHUcontents))
				document.add(PDFTableGenerator.generate(nsAHUcontents, ReportConstants.REPORT_NS_HEPAFILTER));
			}

			return;
		}
		case TYPE_ELECTRODEHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getElectrodeHumidifier(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRODEHUMIDIFIER), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getTechElec1(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsS = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRODEHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				contentsS = SectionContentConvertUtils.getTechElecSummerOrWinner(contentsS, allMap, language);
				document.add(PDFTableGenerator.generate(contentsS, ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsW = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTRODEHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				contentsW = SectionContentConvertUtils.getTechElecSummerOrWinner(contentsW, allMap, language);
				document.add(PDFTableGenerator.generate(contentsW, ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER));
			}
			//增加提示
			String[][] contentAppendix = ValueFormatUtil.transReport(ReportContent.getReportContent(ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER_APPENDIX), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contentAppendix, ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER_APPENDIX));
			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit,
					SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER);
			return;
		}
		case TYPE_ELECTROSTATICFILTER: {
			allMap = SectionDataConvertUtils.getElectrostaticFilter(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_ELECTROSTATICFILTER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_ELECTROSTATICFILTER), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getElectrostaticfilter(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_ELECTROSTATICFILTER));

			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit,
					SectionTypeEnum.TYPE_ELECTROSTATICFILTER);
			return;
		}
		case TYPE_CTR: {
			allMap = SectionDataConvertUtils.getCtr(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_CTR, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_CTR), project, unit, allMap, language,
					unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_CTR));

			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit, SectionTypeEnum.TYPE_CTR);
			return;
		}
		case TYPE_WHEELHEATRECYCLE: {
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_WHEELHEATRECYCLE, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEATRECYCLE), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getWheelHeatRecycle(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_HEATRECYCLE));
			String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_HEATRECYCLE_DELIVERY), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents1, ReportConstants.REPORT_TECH_HEATRECYCLE_DELIVERY));

			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit,
					SectionTypeEnum.TYPE_WHEELHEATRECYCLE);
			return;
		}
		case TYPE_PLATEHEATRECYCLE: {
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_PLATEHEATRECYCLE, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_PLATEHEATRECYCLE), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getPlateHeatRecycle(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, REPORT_TECH_PLATEHEATRECYCLE));
			String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_PLATEHEATRECYCLE_DELIVERY), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents1, ReportConstants.REPORT_TECH_HEATRECYCLE_DELIVERY));
			
			// 添加非标选项
			addCommonF(document, allMap, language, unitType, project, unit,
					SectionTypeEnum.TYPE_PLATEHEATRECYCLE);
			return;
		}
		default:
			log.warn(MessageFormat.format("技术说明报告-AHU段信息未知，报告忽略声称对应段信息 - unitNo：{0},partTypeCode:{1}", unit.getUnitNo(),
					key));
		}
	}

	@SuppressWarnings("unused")
    public static void addSection4Saler(Document document, String key, String pos, int order, Map<String, String> noChangeMap, Map<String, String> allMap,
										LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, AhuParam ahuParam)
            throws DocumentException, IOException {
		switch (SectionTypeEnum.getSectionTypeFromId(key)) {
		case TYPE_MIX: {
			allMap = SectionDataConvertUtils.getMix(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_MIX, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_MIX), project, unit, allMap, language,
					unitType);
			contents = SectionContentConvertUtils.getMix4Sale(contents,noChangeMap, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_MIX));
			return;
		}
		case TYPE_SINGLE: {
			allMap = SectionDataConvertUtils.getFilter(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_SINGLE, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_FILTER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_FILTER));
			return;
		}
		case TYPE_COMPOSITE: {
			allMap = SectionDataConvertUtils.getCombinedFilter(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_COMPOSITE, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_COMBINEDFILTER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_COMBINEDFILTER));
			return;
		}
		case TYPE_COLD: {
			allMap = SectionDataConvertUtils.getCoolingCoil(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_COLD, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_COOLINGCOIL), project, unit, allMap,
					language, unitType);
//			contents = SectionContentConvertUtils.getCoolingCoil4Sale(contents, allMap, language, unitType);// 定制转换
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_COOLINGCOIL));

			Object enableWinter = allMap.get(UtilityConstant.METASEXON_COOLINGCOIL_ENABLEWINTER);
			String value = BaseDataUtil.constraintString(enableWinter);
			if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_COOLINGCOIL_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents1, ReportConstants.REPORT_TECH_COOLINGCOIL));
			}
			return;
		}
		case TYPE_DIRECTEXPENSIONCOIL: {
			log.warn("直接蒸发式盘管段销售版报告无数据规范，使用工程版代替");
			allMap = SectionDataConvertUtils.getDXCoil(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_DIRECTEXPENSIONCOIL), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_COOLINGCOIL));
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_DXCOIL_ENABLEWINTER);
			String value = BaseDataUtil.constraintString(enableWinter);
			if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_DIRECTEXPENSIONCOIL_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents1, ReportConstants.REPORT_TECH_COOLINGCOIL));
			}
			return;
		}
		case TYPE_HEATINGCOIL: {
			allMap = SectionDataConvertUtils.getHeatingcoil(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_HEATINGCOIL, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_HEATINGCOIL), project, unit, allMap, language,
					unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_HEATINGCOIL));
			return;
		}
		case TYPE_STEAMCOIL: {
			allMap = SectionDataConvertUtils.getSteamCoil(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_STEAMCOIL, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_STEAMCOIL), project, unit, allMap, language,
					unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_STEAMCOIL));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_STEAMCOIL_ENABLESUMMER);
			if (true) {
				String[][] contentsS = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_STEAMCOIL_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentsS, ReportConstants.REPORT_TECH_STEAMCOIL));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_STEAMCOIL_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsW = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_STEAMCOIL_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentsW, ReportConstants.REPORT_TECH_STEAMCOIL));
			}
			return;
		}
		case TYPE_ELECTRICHEATINGCOIL: {
			allMap = SectionDataConvertUtils.getElectricHeatingCoil(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_ELECTRICHEATINGCOIL), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_ELECTRICHEATINGCOIL));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_ENABLESUMMER);
			if (true) {
				String[][] contentsS = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_ELECTRICHEATINGCOIL_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentsS, ReportConstants.REPORT_TECH_ELECTRICHEATINGCOIL));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_ELECTRICHEATINGCOIL_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsW = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_ELECTRICHEATINGCOIL_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentsW, ReportConstants.REPORT_TECH_ELECTRICHEATINGCOIL));
			}

			return;
		}
		case TYPE_STEAMHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getSteamHumidifier(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_STEAMHUMIDIFIER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_STEAMHUMIDIFIER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_STEAMHUMIDIFIER));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_STEAMHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents2, ReportConstants.REPORT_TECH_STEAMHUMIDIFIER));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_STEAMHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_STEAMHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents3, ReportConstants.REPORT_TECH_STEAMHUMIDIFIER));
			}

			return;
		}
		case TYPE_WETFILMHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getWetfilmHumidifier(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_WETFILMHUMIDIFIER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_WETFILMHUMIDIFIER), project, unit, allMap, language,
					unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_WETFILMHUMIDIFIER));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_WETFILMHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents2, ReportConstants.REPORT_TECH_WETFILMHUMIDIFIER));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_WETFILMHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents3, ReportConstants.REPORT_TECH_WETFILMHUMIDIFIER));
			}
			return;
		}
		case TYPE_SPRAYHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getSprayHumidifier(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_SPRAYHUMIDIFIER), project, unit, allMap, language,
					unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_SPRAYHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents2, ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contents3 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_SPRAYHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contents3, ReportConstants.REPORT_TECH_SPRAYHUMIDIFIER));
			}
			return;
		}
		case TYPE_FAN: {
			allMap = SectionDataConvertUtils.getFan(noChangeMap,allMap, language, ahuParam);
			/* 当类型为风机：根据风向 送风、回风重新封装报告名称 */
			String fanName = genName(pos, order, SectionTypeEnum.TYPE_FAN, language);
			fanName = SectionDataConvertUtils.getFanName(noChangeMap,allMap, fanName, language);
			PDF4ReportUtils.addPartName(document, fanName);
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_FAN), project, unit, allMap, language,
					unitType);
			// 封装风机段特殊字段
			contents = SectionContentConvertUtils.getFan4Sale(contents, noChangeMap, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_FAN));

			return;
		}
		case TYPE_COMBINEDMIXINGCHAMBER: {
			allMap = SectionDataConvertUtils.getCombinedMixingChamber(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_COMBINEDMIXINGCHAMBER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_COMBINEDMIXINGCHAMBER));
			String[][] contents1 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_COMBINEDMIXINGCHAMBER_SUMMER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents1, ReportConstants.REPORT_TECH_COMBINEDMIXINGCHAMBER_SUMMER));
			String[][] contents2 = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_COMBINEDMIXINGCHAMBER_WINTER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents2, ReportConstants.REPORT_TECH_COMBINEDMIXINGCHAMBER_WINTER));

			return;
		}
		case TYPE_ATTENUATOR: {
			allMap = SectionDataConvertUtils.getAttenuator(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_ATTENUATOR, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_ATTENUATOR), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_ATTENUATOR));
			return;
		}
		case TYPE_DISCHARGE: {
			allMap = SectionDataConvertUtils.getDischarge(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_DISCHARGE, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_DISCHARGE), project, unit, allMap, language,
					unitType);
			// 封装出风段特殊字段
//			contents = SectionContentConvertUtils.getDischarge(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_DISCHARGE));
			return;
		}
		case TYPE_ACCESS: {
			allMap = SectionDataConvertUtils.getAccess(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_ACCESS, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_ACCESS), project, unit, allMap,
					language, unitType);
			// 封装空段特殊字段
//			contents = SectionContentConvertUtils.getAccess(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_ACCESS));
			return;
		}
		case TYPE_HEPAFILTER: {
			allMap = SectionDataConvertUtils.getHEPAFilter(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_HEPAFILTER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_HEPAFILTER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_HEPAFILTER));
			return;
		}
		case TYPE_ELECTRODEHUMIDIFIER: {
			allMap = SectionDataConvertUtils.getElectrodeHumidifier(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_ELECTRODEHUMIDIFIER), project, unit, allMap,
					language, unitType);
			contents = SectionContentConvertUtils.getTechElec14Sale(contents, allMap, language);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER));
			Object enableSummer = allMap.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_ENABLESUMMER);
			String valueS = BaseDataUtil.constraintString(enableSummer);
			if (valueS.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsS = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_ELECTRODEHUMIDIFIER_SUMMER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentsS, ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER));
			}
			Object enableWinter = allMap.get(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_ENABLEWINTER);
			String valueW = BaseDataUtil.constraintString(enableWinter);
			if (valueW.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
				String[][] contentsW = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_ELECTRODEHUMIDIFIER_WINTER), project, unit, allMap,
						language, unitType);
				document.add(PDFTableGenerator.generate(contentsW, ReportConstants.REPORT_TECH_ELECTRODEHUMIDIFIER));
			}

			return;
		}
		case TYPE_ELECTROSTATICFILTER: {
			allMap = SectionDataConvertUtils.getElectrostaticFilter(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_ELECTROSTATICFILTER, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_ELECTROSTATICFILTER), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_ELECTROSTATICFILTER));
			return;
		}
		case TYPE_CTR: {
			log.warn("控制段销售版报告无数据规范，使用工程版代替");
			allMap = SectionDataConvertUtils.getCtr(noChangeMap,allMap, language);
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_CTR, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_CTR), project, unit, allMap, language,
					unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_CTR));
			return;
		}
		case TYPE_WHEELHEATRECYCLE: {
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_WHEELHEATRECYCLE, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_WHEELHEATRECYCLE), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_HEATRECYCLE));
			return;
		}
		case TYPE_PLATEHEATRECYCLE: {
			PDF4ReportUtils.addPartName(document, genName(pos, order, SectionTypeEnum.TYPE_PLATEHEATRECYCLE, language));
			String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_SALES_PLATEHEATRECYCLE), project, unit, allMap,
					language, unitType);
			document.add(PDFTableGenerator.generate(contents, ReportConstants.REPORT_TECH_HEATRECYCLE));
			return;
		}
		default:
			log.warn(MessageFormat.format("技术说明报告-AHU段信息未知，报告忽略声称对应段信息 - unitNo：{0},partTypeCode:{1}", unit.getUnitNo(),
					key));
		}
	}

	public static void addCommonF(Document document, Map<String, String> allMap,
			LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
			throws IOException, DocumentException {
		String[][] techCommonFcontents = ReportContent.getReportContent(REPORT_TECH_NONSTANDARD);
		techCommonFcontents = SectionContentConvertUtils.getCommonF(techCommonFcontents, allMap, sectionType);
		if (null != techCommonFcontents) {
			techCommonFcontents = ValueFormatUtil.transReport(techCommonFcontents, project, unit, allMap, language,
					unitType);
			document.add(PDFTableGenerator.generate(techCommonFcontents, ReportConstants.REPORT_TECH_NONSTANDARD));// 生成非标选项pdftable
		}
	}

	private static String genName(String pos, int order, SectionTypeEnum type, LanguageEnum language) {
		StringBuffer s = new StringBuffer(pos+ "_" + order);
		s.append(UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
		s.append(getIntlString(type.getCnName()));
		return s.toString();
	}
	
	private static String genBaiChengName(String pos, int order, SectionTypeEnum type, LanguageEnum language) {
		StringBuffer s = new StringBuffer();
		s.append(getIntlString(type.getCnName()));
		return s.toString();
	}
	private static String genName(String pos, int order, String partName) {
		StringBuffer s = new StringBuffer(pos+ "_" + order);
		s.append(UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE);
		s.append(partName);
		return s.toString();
	}

}

package com.carrier.ahu.util.ahu;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.util.EmptyUtil;
import com.google.gson.Gson;

/**
 * Extract from CalculatorTool class.
 * 
 * Created by Braden Zhou on 2018/07/06.
 */
public class AhuParamUtils {

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static AhuParam getAhuParam(Unit unit, List<Part> parts, String partKey) {
        AhuParam ahuParam = new AhuParam();
        if (EmptyUtil.isNotEmpty(unit)) {
            ahuParam.setCost(unit.getCost());
            ahuParam.setCustomerName(unit.getCustomerName());
            ahuParam.setDrawing(unit.getDrawing());
            ahuParam.setDrawingNo(unit.getDrawingNo());
            ahuParam.setGroupId(unit.getGroupId());
            ahuParam.setLb(unit.getLb());
            ahuParam.setModifiedno(unit.getModifiedno());
            ahuParam.setMount(unit.getMount());
            ahuParam.setName(unit.getName());
            ahuParam.setNsprice(unit.getNsprice());
            ahuParam.setPaneltype(unit.getPaneltype());
            ahuParam.setPid(unit.getPid());
            ahuParam.setPrice(unit.getPrice());
            ahuParam.setProduct(unit.getProduct());
            ahuParam.setSeries(unit.getSeries());
            ahuParam.setUnitid(unit.getUnitid());
            ahuParam.setUnitNo(unit.getUnitNo());
            ahuParam.setWeight(unit.getWeight());
            AhuLayout ahuLayout = AhuLayoutUtils.parse(unit.getLayoutJson());
            if (AhuPartitionUtils.isDoubleLayer(ahuLayout)) {
                ahuParam.setDoubleLayer(true);
            }
            ahuParam.setLayout(ahuLayout);
            ahuParam.setSectionrelations(unit.getSectionrelations());
            if (StringUtils.isNotBlank(unit.getMetaJson())) {
                Gson gson = new Gson();
                Map<String, Object> map = (Map<String, Object>) gson.fromJson(unit.getMetaJson(), Map.class);
                ahuParam.setParams(map);
            }
        }

        List<PartParam> partParams = new ArrayList<PartParam>();
        for (Part section : parts) {
            if (StringUtils.isBlank(partKey)) {
                PartParam partParam = new PartParam();
                partParam.setAvdirection(section.getAvdirection());
                partParam.setCost(section.getCost());
                partParam.setGroupi(section.getGroupi());
                partParam.setKey(section.getSectionKey());
                partParam.setOrders(section.getOrders());
                partParam.setPartid(section.getPartid());
                partParam.setPic(section.getPic());
                partParam.setPid(unit.getPid());
                partParam.setPosition(section.getPosition());
                partParam.setSetup(section.getSetup());
                partParam.setTypes(
                        EmptyUtil.isEmpty(section.getTypes()) ? MetaCodeGen.getSectionType(section.getSectionKey())
                                : section.getTypes());
                partParam.setUnitid(unit.getUnitid());
                Map<String, Object> map = (Map) JSONObject.parseObject(section.getMetaJson());
                partParam.setParams(map);
                partParams.add(partParam);
            } else {
                if (partKey.equals(section.getSectionKey())) {
                    PartParam partParam = new PartParam();
                    partParam.setAvdirection(section.getAvdirection());
                    partParam.setCost(section.getCost());
                    partParam.setGroupi(section.getGroupi());
                    partParam.setKey(section.getSectionKey());
                    partParam.setOrders(section.getOrders());
                    partParam.setPartid(section.getPartid());
                    partParam.setPic(section.getPic());
                    partParam.setPid(unit.getPid());
                    partParam.setPosition(section.getPosition());
                    partParam.setSetup(section.getSetup());
                    partParam.setTypes(
                            EmptyUtil.isEmpty(section.getTypes()) ? MetaCodeGen.getSectionType(section.getSectionKey())
                                    : section.getTypes());
                    partParam.setUnitid(unit.getUnitid());
                    Map<String, Object> map = (Map) JSONObject.parseObject(section.getMetaJson());
                    partParam.setParams(map);
                    partParams.add(partParam);
                }
            }

        }
        ahuParam.setPartParams(partParams);
        return ahuParam;
    }

}

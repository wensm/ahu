package com.carrier.ahu.util;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.intl.I18NConstants.BACK_RETURN_AIR;
import static com.carrier.ahu.common.intl.I18NConstants.BOTTOM_RETURN_AIR;
import static com.carrier.ahu.common.intl.I18NConstants.CONTRACT_NO_COLON;
import static com.carrier.ahu.common.intl.I18NConstants.COOLINGCOIL_COPPER_WARNNING;
import static com.carrier.ahu.common.intl.I18NConstants.COOL_COIL_PART;
import static com.carrier.ahu.common.intl.I18NConstants.DRY_STEAM_HUMIDIFICATION_SECTION;
import static com.carrier.ahu.common.intl.I18NConstants.ELECTRODE_HUMIDIFICATION_SECTION;
import static com.carrier.ahu.common.intl.I18NConstants.FAN_RFAN;
import static com.carrier.ahu.common.intl.I18NConstants.FAN_SFAN;
import static com.carrier.ahu.common.intl.I18NConstants.FUNCTIONAL_SECTION_RESISTANCE_LOSS;
import static com.carrier.ahu.common.intl.I18NConstants.HIGH_PRESSURE_SPRAY_HUMIDIFICATION_SECTION;
import static com.carrier.ahu.common.intl.I18NConstants.HOTCOIL_PART;
import static com.carrier.ahu.common.intl.I18NConstants.LEFT_SIDE_RETURN_AIR;
import static com.carrier.ahu.common.intl.I18NConstants.MIX_SECTION;
import static com.carrier.ahu.common.intl.I18NConstants.NON_STANDARD_QUOTATION_NUMBER_COLON;
import static com.carrier.ahu.common.intl.I18NConstants.PROJECT_ADDRESS_COLON;
import static com.carrier.ahu.common.intl.I18NConstants.PROJECT_CODE_COLON;
import static com.carrier.ahu.common.intl.I18NConstants.PROJECT_NAME_COLON;
import static com.carrier.ahu.common.intl.I18NConstants.RIGHT_SIDE_RETURN_AIR;
import static com.carrier.ahu.common.intl.I18NConstants.SALES_COLON;
import static com.carrier.ahu.common.intl.I18NConstants.SOFTWARE_VERSION_COLON;
import static com.carrier.ahu.common.intl.I18NConstants.TOP_RETURN_AIR_COLON;
import static com.carrier.ahu.common.intl.I18NConstants.UNIT1;
import static com.carrier.ahu.common.intl.I18NConstants.UNIT_DIMENSION_L_W_H;
import static com.carrier.ahu.common.intl.I18NConstants.UNIT_INFORMATION;
import static com.carrier.ahu.common.intl.I18NConstants.WET_FILM_HUMIDIFICATION_SECTION;
import static com.carrier.ahu.constant.CommonConstant.JSON_COOLINGCOIL_ELIMINATOR_PLASTIC;
import static com.carrier.ahu.constant.CommonConstant.JSON_FAN_FANSUPPLIER_A;
import static com.carrier.ahu.constant.CommonConstant.JSON_FAN_STARTSTYLE_VFD;
import static com.carrier.ahu.constant.CommonConstant.JSON_PLATEHEATRECYCLE_WHEELDEPTH_1;
import static com.carrier.ahu.constant.CommonConstant.JSON_PLATEHEATRECYCLE_WHEELDEPTH_1_VAl;
import static com.carrier.ahu.constant.CommonConstant.JSON_PLATEHEATRECYCLE_WHEELDEPTH_2;
import static com.carrier.ahu.constant.CommonConstant.JSON_PLATEHEATRECYCLE_WHEELDEPTH_2_VAl;
import static com.carrier.ahu.constant.CommonConstant.JSON_WHEELHEATRECYCLE_BRAND_A;
import static com.carrier.ahu.constant.CommonConstant.JSON_WHEELHEATRECYCLE_BRAND_C;
import static com.carrier.ahu.constant.CommonConstant.JSON_WHEELHEATRECYCLE_MODEL_1;
import static com.carrier.ahu.constant.CommonConstant.JSON_WHEELHEATRECYCLE_MODEL_2;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_FAN_POWER;
import static com.carrier.ahu.constant.CommonConstant.SYS_ASSERT_TRUE;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_DOC_PARALIST_AHU;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_DOC_PARALIST_COOLINGCOIL;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_DOC_PARALIST_ELECTRODEHUMIDIFIER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_DOC_PARALIST_FAN;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_DOC_PARALIST_HEATINGCOIL;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_DOC_PARALIST_MIX;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_DOC_PARALIST_OTHER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_DOC_PARALIST_SPRAYHUMIDIFIER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_DOC_PARALIST_STEAMHUMIDIFIER;
import static com.carrier.ahu.report.common.ReportConstants.REPORT_DOC_PARALIST_WETFILMHUMIDIFIER;
import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_DELIVERY_CKD;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.entity.heatrecycle.HeatxPlate;
import com.carrier.ahu.model.calunit.AhuParam;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.model.partition.PanelCalculationObj;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.report.DeliveryInfo;
import com.carrier.ahu.model.calunit.UnitUtil;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.po.model.PartitionObject;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.report.Report;
import com.carrier.ahu.report.ReportData;
import com.carrier.ahu.report.ReportExcelCRMList;
import com.carrier.ahu.report.ReportExcelDelivery;
import com.carrier.ahu.report.ReportExcelProject;
import com.carrier.ahu.report.ReportExcelProject.ProjAhu;
import com.carrier.ahu.report.SAPFunction;
import com.carrier.ahu.report.content.ReportContent;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lowagie.text.DocumentException;



// http://blog.csdn.net/sinat_29581293/article/details/52122191
@Component
public class Excel4ReportUtils {
	protected static Logger logger = LoggerFactory.getLogger(Excel4ReportUtils.class);

private static String PatchVersion;
	
	@Value("${ahu.patch.version}")
	private void setPatchVersion(String pv) {
		PatchVersion = pv;
	}	
	
	private final static String ContentsErrorMsg = "Generate Sheet Contents Error.Reason :>> [{0}]";
//	/** 模版位置 [参数列表] */
//	public static final String TEMPLATE_PATH_PARA = "asserts/template/template.excel.paramlist.xlsx";
//	/** 模版位置 [参数列表] 英文版*/
//	public static final String TEMPLATE_PATH_PARA_EN = "asserts/template/template.excel.paramlist.en.xlsx";
//	/** 模版位置 [项目清单] */
//	public static final String TEMPLATE_PATH_PROJ = "asserts/template/template.excel.prolist.xlsx";
//	/** 模版位置 [项目清单] 英文版 */
//	public static final String TEMPLATE_PATH_PROJ_EN = "asserts/template/template.excel.prolist.en.xlsx";
//	/** 模版位置 [长期供货] */
//	public static final String TEMPLATE_PATH_DELI = "asserts/template/template.excel.delivery.xlsx";
//	/** 模版位置 [长期供货] 英文版*/
//	public static final String TEMPLATE_PATH_DELI_EN = "asserts/template/template.excel.delivery.en.xlsx";
//	/** 模版位置 [CRM清单] */
//	public static final String TEMPLATE_PATH_CRMLIST = "asserts/template/template.excel.crmlist.xlsx";
//	/** 模版位置 [CRM清单] 英文版*/
//	public static final String TEMPLATE_PATH_CRMLIST_EN = "asserts/template/template.excel.crmlist.en.xlsx";
//
//	/** SAP Price Code */
//    public static final String TEMPLATE_PATH_SAP_PRICE_CODE = "asserts/template/template.sap.price.code.xlsx";

	/**
	 * 创建-[项目清单]-报表内容
	 * 
	 * @param report
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void generateExcelProlist(File file, ReportExcelProject report, ReportData reportData) throws IOException {
		FileInputStream fileInputStream = new FileInputStream(file.getPath());
		XSSFWorkbook wb = new XSSFWorkbook(fileInputStream);
		XSSFSheet sheet = wb.getSheetAt(0);
		formatSheet4Project(sheet, report, reportData);
		FileOutputStream os = new FileOutputStream(file.getPath());
		wb.write(os);
		os.close();
		wb.close();
		fileInputStream.close();
	}

	private static void formatSheet4Project(XSSFSheet sheet, ReportExcelProject report, ReportData reportData) {
		Project project = report.getProject();
		List<ProjAhu> projAhus = report.getProjAhus();
		
		//处理版本号
		String versionColon = "";
		if (AHUContext.isExportVersion()) {// 判断是否出口版
			versionColon = AHUContext.getAhuVersion() + UtilityConstant.SYS_BLANK_SPACE + UtilityConstant.SYS_VERSION_EXPORT
					+ UtilityConstant.SYS_BLANK_SPACE + PatchVersion;
		} else {
			versionColon = AHUContext.getAhuVersion() + UtilityConstant.SYS_BLANK_SPACE + PatchVersion;
		}
		
		sheet.getRow(2).getCell(2).setCellValue(getIntlString(PROJECT_CODE_COLON) + project.getNo());
		sheet.getRow(2).getCell(4).setCellValue(getIntlString(SOFTWARE_VERSION_COLON) + versionColon);
		sheet.getRow(3).getCell(2).setCellValue(getIntlString(CONTRACT_NO_COLON) + project.getContract());
		sheet.getRow(3).getCell(4)
				.setCellValue(getIntlString(NON_STANDARD_QUOTATION_NUMBER_COLON)
						+ (EmptyUtil.isNotEmpty(project.getEnquiryNo()) ? (project.getEnquiryNo())
								: (UtilityConstant.SYS_BLANK)));
		sheet.getRow(4).getCell(2).setCellValue(getIntlString(PROJECT_NAME_COLON) + project.getName());
		sheet.getRow(4).getCell(4).setCellValue(getIntlString(SALES_COLON) + project.getSaler());
		sheet.getRow(5).getCell(2).setCellValue(getIntlString(PROJECT_ADDRESS_COLON) + project.getAddress());

		double totalPrice = 0;
		double totalWeight = 0;

		int i = 9;
		for (ProjAhu ahu : projAhus) {
			XSSFRow row = sheet.createRow(i++);
			row.createCell(0).setCellValue(ahu.getUnitId());
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(ahu.getDeformation())){//项目清单中，如果机组型号是非标，那么机组型号显示非标后的型号。
				row.createCell(1).setCellValue(ahu.getSerial().substring(0,ahu.getSerial().length()-4) + ahu.getDeformationSerial());
			}else {
				row.createCell(1).setCellValue(ahu.getSerial());
			}
			row.createCell(2).setCellValue(ahu.getAhuName());
			row.createCell(3).setCellValue(ahu.getAmount());
			row.createCell(4).setCellValue(ahu.getPackingPrice());
			row.createCell(5).setCellValue(ahu.getStdPrice());
			row.createCell(6).setCellValue(ahu.getNStdpackingPrice());//为负数也打印(ahu.getNStdpackingPrice()>0?ahu.getNStdpackingPrice():0)
			row.createCell(7).setCellValue(ahu.getFactoryForm().equals(AHU_DELIVERY_CKD)?Math.round(ahu.getStdPrice()*0.06):0);
			row.createCell(8).setCellValue(Math.round(ahu.getTotalPrice()));
			row.createCell(9).setCellValue(ahu.getWeight());
			row.createCell(10).setCellValue(ahu.getFactoryForm());
			totalPrice += Math.round(ahu.getTotalPrice());
			totalWeight += ahu.getWeight();
		}
		XSSFRow row = sheet.createRow(++i);
		row.createCell(7).setCellValue(UtilityConstant.SYS_STRING_TOTAL);
		row.createCell(8).setCellValue(totalPrice);
		row.createCell(9).setCellValue(totalWeight);
		
		//冷水盘管-翅片选择铜翅片 项目清单中需要输出说明：*选择铜翅片，只限于计算性能，不包含价格，具体报价请联系当地技术支持！
		Map<String,PartPO> partMap = reportData.getPartMap();
		Set<String> keySet = partMap.keySet();
		PartPO coolingCoil = null;
		List<PartPO> coolingCoilList = new ArrayList<PartPO>();
		for(String key : keySet) {
			if(key.contains(UtilityConstant.METASEXON_COOLINGCOIL)) {
				coolingCoil = partMap.get(key);
				coolingCoilList.add(coolingCoil);
			}
		}
		if(coolingCoilList.size() > 0) {
			for(PartPO pp : coolingCoilList) {
				String metaJson = pp.getCurrentPart().getMetaJson();
				Map<String, Object> metaMap = (Map<String, Object>)JSON.parse(metaJson);
				String finType = (String)metaMap.get(UtilityConstant.METASEXON_COOLINGCOIL_FINTYPE);
				if(finType!=null && UtilityConstant.JSON_COOLINGCOIL_FINTYPE_COPPER.equals(finType)) {
					row = sheet.createRow(i+2);
					row.createCell(0).setCellValue(getIntlString(COOLINGCOIL_COPPER_WARNNING));
					break;
				}
			}
		}
		
	}

	/**
	 * 创建-[参数汇总]-报表内容
	 * @param file
	 * @param report
	 * @param reportData
	 * @param unitType
	 * @throws IOException
	 */
	public static void generateExcelParalist(File file, Report report, ReportData reportData, UnitSystemEnum unitType)
			throws IOException {
		InputStream fileInputStream = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fileInputStream);
		XSSFSheet sheet = wb.getSheetAt(0);
		Project project = reportData.getProject();
		List<LinkedList<String>> paramValuesList = new ArrayList<>();
	
		
		
		
		// 获取所有机组混合段、冷水盘管、热水盘管、风机 最大个数；
		int maxCountMix = 0;
		int maxCountCoolingCoil = 0;
		int maxCountHeatingCoil = 0;
		int maxCountFan = 0;
		int maxCountSteamHumidifier = 0;
		int maxCountWetFilmHumidifier = 0;
		int maxCountSprayHumidifier = 0;
		int maxCountElectrodeHumidifier = 0;
		for (Unit unit : reportData.getUnitList()) {
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}
			int tempMaxCountMix = 0;
			int tempMaxCountCoolingCoil = 0;
			int tempMaxCountHeatingCoil = 0;
			int tempMaxCountFan = 0;
			int tempMaxSteamHumidifier = 0;
			int tempMaxWetFilmHumidifier = 0;
			int tempMaxSprayHumidifier = 0;
			int tempMaxElectrodeHumidifier = 0;
			for (PartPO partPO : partList) {
				Part part = partPO.getCurrentPart();
				String key = part.getSectionKey();
				switch (SectionTypeEnum.getSectionTypeFromId(key)) {
					case TYPE_MIX: {
						tempMaxCountMix++;
						break;
					}
					case TYPE_COLD: {
						tempMaxCountCoolingCoil ++;
						break;
					}
					case TYPE_HEATINGCOIL: {
						tempMaxCountHeatingCoil ++;
						break;
					}
					case TYPE_FAN: {
						tempMaxCountFan ++;
						break;
					}
					case TYPE_STEAMHUMIDIFIER: {
						tempMaxSteamHumidifier ++;
						break;
					}
					case TYPE_WETFILMHUMIDIFIER: {
						tempMaxWetFilmHumidifier ++;
						break;
					}
					case TYPE_SPRAYHUMIDIFIER: {
						tempMaxSprayHumidifier ++;
						break;
					}
					case TYPE_ELECTRODEHUMIDIFIER: {
						tempMaxElectrodeHumidifier ++;
						break;
					}
					default:
						break;
				}
			}
			
			if(tempMaxCountMix > maxCountMix){
				maxCountMix = tempMaxCountMix;
			}
			if(tempMaxCountCoolingCoil > maxCountCoolingCoil){
				maxCountCoolingCoil = tempMaxCountCoolingCoil;
			}
			if(tempMaxCountHeatingCoil > maxCountHeatingCoil){
				maxCountHeatingCoil = tempMaxCountHeatingCoil;
			}
			if(tempMaxCountFan > maxCountFan){
				maxCountFan = tempMaxCountFan;
			}
			if(tempMaxSteamHumidifier > maxCountSteamHumidifier){
				maxCountSteamHumidifier = tempMaxSteamHumidifier;
			}
			if(tempMaxWetFilmHumidifier > maxCountWetFilmHumidifier){
				maxCountWetFilmHumidifier = tempMaxWetFilmHumidifier;
			}
			if(tempMaxSprayHumidifier > maxCountSprayHumidifier){
				maxCountSprayHumidifier = tempMaxSprayHumidifier;
			}
			if(tempMaxElectrodeHumidifier > maxCountElectrodeHumidifier){
				maxCountElectrodeHumidifier = tempMaxElectrodeHumidifier;
			}
		}
		int maxPartition = 1;//最大分段个数
		Gson gson = new Gson();
		for (Unit unit : reportData.getUnitList()) {
			Partition partion = reportData.getPartitionMap().get(unit.getUnitid());
			if(partion==null) {
				continue;
			}
			//获取分段信息
			List<PartitionObject> objPartitions = gson.fromJson(partion.getPartitionJson(),
					new TypeToken<List<PartitionObject>>() {
					}.getType());
			int tempMaxPartitions = objPartitions.size();
			if(tempMaxPartitions > maxPartition){
				maxPartition = tempMaxPartitions;
			}
		}
		//标题行索引
		int writeIndex = 6;
				
		// 添加列head名称
		LanguageEnum language = AHUContext.getLanguage();
		String[][] ahuContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_AHU), language, unitType);
		String[][] mixContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_MIX), language, unitType);
		String[][] coolingCoilContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_COOLINGCOIL), language, unitType);
		String[][] heatingCoilContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_HEATINGCOIL), language, unitType);
		String[][] fanContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_FAN), language, unitType);
		String[][] otherContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_OTHER), language, unitType);
		Map<String, String> mixMap = SectionDataConvertUtils.getReportMap(reportData,SectionTypeEnum.TYPE_MIX);
		//mixContents = getMixContents(mixContents, mixMap);
		String[][] steamHumidifierContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_STEAMHUMIDIFIER), language, unitType);
		String[][] wetFilmHumidifierContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_WETFILMHUMIDIFIER), language, unitType);
		String[][] sprayHumidifierContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_SPRAYHUMIDIFIER), language, unitType);
		String[][] electrodeHumidifierContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_ELECTRODEHUMIDIFIER), language, unitType);
		writeSheetTitle4Paralist(wb,sheet,
				maxCountMix,maxCountCoolingCoil,maxCountHeatingCoil,maxCountFan,maxCountSteamHumidifier,maxCountWetFilmHumidifier,maxCountSprayHumidifier,maxCountElectrodeHumidifier,
				ahuContents,mixContents,coolingCoilContents,heatingCoilContents,fanContents,otherContents,steamHumidifierContents,wetFilmHumidifierContents,sprayHumidifierContents,electrodeHumidifierContents,
				writeIndex,maxPartition);

		Map<String, Partition> partitionMap = reportData.getPartitionMap();

		// 添加列body数据
		for (Unit unit : reportData.getUnitList()) {
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}
			Partition partition = null;
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}
			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);

			String groupCode = unit.getGroupCode();
			if (null == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
				logger.warn(MessageFormat.format("技术说明报告-AHU组编码信息不合法，报告忽略声称对应段信息 - unitNo：{0},GroupCode:{1}", new Object[] { unit.getUnitNo(), unit.getGroupCode() }));
			} else {
				LinkedList<String> paramValues = new LinkedList<>();
				//机组信息
				Map<String, String> unitMap = allMap.get(UnitConverter.UNIT);
				String[][] unitContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_DOC_PARALIST_AHU), project, unit, unitMap, language,unitType);
				for (int i = 0; i < unitContents.length; i++) {
					paramValues.add(unitContents[i][2]);
				}
				
				//混合段
				int hasCountMix = 0;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					if(SectionTypeEnum.TYPE_MIX.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> allMap2 = allMap.get(UnitConverter.genUnitKey(unit, part));
						// contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_DOC_PARALIST_MIX), project, unit, allMap2, language,unitType);
                        mixContents = ValueFormatUtil.transReportUnitType(ReportContent.getReportContent(REPORT_DOC_PARALIST_MIX), language, unitType);
                        String[][] contents = SectionContentConvertUtils.getParaMix(mixContents, allMap2, language);
						for (int i = 0; i < contents.length; i++) {
							paramValues.add(contents[i][2]);
						}
						hasCountMix ++ ;
					}
				}
				for(int i=0;i<maxCountMix-hasCountMix;i++){
					String[][] contents = ReportContent.getReportContent(REPORT_DOC_PARALIST_MIX);
					for (int j = 0; j < contents.length; j++) {
						paramValues.add(UtilityConstant.SYS_BLANK);
					}
				}
				
				//冷水盘管
				int hasCountCoolingCoil = 0;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					if(SectionTypeEnum.TYPE_COLD.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> noChangeMap = allMap.get(UnitConverter.genUnitKey(unit, part));
						Map<String, String> allMap2=allMap.get(UnitConverter.genUnitKey(unit, part));
	                    Map<String, String> cloneMap=UnitUtil.clone(noChangeMap);
						allMap2 = SectionDataConvertUtils.getCoolingCoil(allMap2,cloneMap,language);
						String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_DOC_PARALIST_COOLINGCOIL), project, unit, allMap2, language,unitType);
						String whetherWinter = String.valueOf(allMap2.get(UtilityConstant.METASEXON_COOLINGCOIL_ENABLEWINTER));
						if ("true".equals(whetherWinter)) {
							for (int i = 0; i < contents.length; i++) {
								paramValues.add(contents[i][2]);
							}							
						}else {//如果冬季不生效，将不显示冬季制热参数
							for (int i = 0; i < contents.length; i++) {
								if (i < contents.length - 9) {
									paramValues.add(contents[i][2]);
								} else {
									paramValues.add(UtilityConstant.SYS_BLANK);
								}
							}
						}

						hasCountCoolingCoil ++ ;
					}
				}
				for(int i=0;i<maxCountCoolingCoil-hasCountCoolingCoil;i++){
					String[][] contents = ReportContent.getReportContent(REPORT_DOC_PARALIST_COOLINGCOIL);
					for (int j = 0; j < contents.length; j++) {
						paramValues.add(UtilityConstant.SYS_BLANK);
					}
				}
				//热水盘管
				int hasCountHeatCoil = 0;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					if(SectionTypeEnum.TYPE_HEATINGCOIL.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> noChangeMap = allMap.get(UnitConverter.genUnitKey(unit, part));
						Map<String, String> allMap2=allMap.get(UnitConverter.genUnitKey(unit, part));
						Map<String, String> cloneMap=UnitUtil.clone(noChangeMap);

						allMap2 = SectionDataConvertUtils.getHeatingcoil(allMap2,cloneMap,language);
						String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_DOC_PARALIST_HEATINGCOIL), project, unit, allMap2, language,unitType);
						for (int i = 0; i < contents.length; i++) {
							paramValues.add(contents[i][2]);
						}
						hasCountHeatCoil ++;
					}
				}
				for(int i=0;i<maxCountHeatingCoil-hasCountHeatCoil;i++){
					String[][] contents = ReportContent.getReportContent(REPORT_DOC_PARALIST_HEATINGCOIL);
					for (int j = 0; j < contents.length; j++) {
						paramValues.add(UtilityConstant.SYS_BLANK);
					}
				}

				AhuParam ahuParam = ValueFormatUtil.transDBData2AhuParam(project, unit, partList, partition);

				//风机电机
				int hasCountFan = 0;
				int Sindex = 0;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					if(SectionTypeEnum.TYPE_FAN.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> allMap2 = allMap.get(UnitConverter.genUnitKey(unit, part));
						Map<String, String> cloneMap=UnitUtil.clone(allMap2);
						allMap2 = SectionDataConvertUtils.getFan(allMap2,cloneMap,language, ahuParam); //风机》 A声压级噪音dB(A)
						String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_DOC_PARALIST_FAN), project, unit, allMap2, language,unitType);
						contents = SectionContentConvertUtils.getFan4Paralist(contents, allMap2, language);
						//填充送风机和回风机数据
						
						String airDirection = UtilityConstant.METASEXON_AIRDIRECTION;// 风向
				        if (allMap2.containsKey(airDirection)) {
				            String value = BaseDataUtil.constraintString(allMap2.get(airDirection));
				            //送风机数据在回风机数据左边
				            if (value.equals(UtilityConstant.SYS_ALPHABET_R_UP)) {//回风
				            	Sindex = paramValues.size();//记录回风数据在linkedlist开始的位置，方便送风数据的插入
				            	for (int i = 0; i < contents.length; i++) {
									paramValues.add(contents[i][2]);
								}
				            }else if (value.equals(UtilityConstant.SYS_ALPHABET_S_UP)) {//送风
								if (Sindex == 0) {//没有添加过回风机数据的情况
									for (int i = 0; i < contents.length; i++) {
										paramValues.add(contents[i][2]);
									}
								}else if (Sindex > 0) {//之前添加过回风机数据情况
									for (int i = 0; i < contents.length; i++) {
										paramValues.add(Sindex, contents[i][2]);//插入到回风数据的前面
										Sindex++;
									}
								}
				            	
							}
				        }

						
						hasCountFan ++;
					}
				}
				for(int i=0;i<maxCountFan-hasCountFan;i++){
					String[][] contents = ReportContent.getReportContent(REPORT_DOC_PARALIST_FAN);
					for (int j = 0; j < contents.length; j++) {
						paramValues.add(UtilityConstant.SYS_BLANK);
					}
				}
				
				//干蒸加湿段
				int hasCountSteamHumidifier = 0;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					if(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> noChangeMap = allMap.get(UnitConverter.genUnitKey(unit, part));
						Map<String, String> allMap2=allMap.get(UnitConverter.genUnitKey(unit, part));
//						allMap2 = SectionDataConvertUtils.getHeatingcoil(noChangeMap,allMap2, language);
						String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_DOC_PARALIST_STEAMHUMIDIFIER), project, unit, allMap2, language,unitType);
						for (int i = 0; i < contents.length; i++) {
							paramValues.add(contents[i][2]);
						}
						hasCountSteamHumidifier ++;
					}
				}
				for(int i=0;i<maxCountSteamHumidifier-hasCountSteamHumidifier;i++){
					String[][] contents = ReportContent.getReportContent(REPORT_DOC_PARALIST_STEAMHUMIDIFIER);
					for (int j = 0; j < contents.length; j++) {
						paramValues.add(UtilityConstant.SYS_BLANK);
					}
				}
				
				//湿膜加湿段
				int hasCountWetFilmHumidifier = 0;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					if(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> noChangeMap = allMap.get(UnitConverter.genUnitKey(unit, part));
						Map<String, String> allMap2=allMap.get(UnitConverter.genUnitKey(unit, part));
//						allMap2 = SectionDataConvertUtils.getHeatingcoil(noChangeMap,allMap2, language);
						String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_DOC_PARALIST_WETFILMHUMIDIFIER), project, unit, allMap2, language,unitType);
						for (int i = 0; i < contents.length; i++) {
							paramValues.add(contents[i][2]);
						}
						hasCountWetFilmHumidifier ++;
					}
				}
				for(int i=0;i<maxCountWetFilmHumidifier-hasCountWetFilmHumidifier;i++){
					String[][] contents = ReportContent.getReportContent(REPORT_DOC_PARALIST_WETFILMHUMIDIFIER);
					for (int j = 0; j < contents.length; j++) {
						paramValues.add(UtilityConstant.SYS_BLANK);
					}
				}
				
				//高压喷雾加湿段
				int hasCountSprayHumidifier = 0;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					if(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> noChangeMap = allMap.get(UnitConverter.genUnitKey(unit, part));
						Map<String, String> allMap2=allMap.get(UnitConverter.genUnitKey(unit, part));
//						allMap2 = SectionDataConvertUtils.getHeatingcoil(noChangeMap,allMap2, language);
						String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_DOC_PARALIST_SPRAYHUMIDIFIER), project, unit, allMap2, language,unitType);
						for (int i = 0; i < contents.length; i++) {
							paramValues.add(contents[i][2]);
						}
						hasCountSprayHumidifier ++;
					}
				}
				for(int i=0;i<maxCountSprayHumidifier-hasCountSprayHumidifier;i++){
					String[][] contents = ReportContent.getReportContent(REPORT_DOC_PARALIST_SPRAYHUMIDIFIER);
					for (int j = 0; j < contents.length; j++) {
						paramValues.add(UtilityConstant.SYS_BLANK);
					}
				}
				
				//电极加湿段
				int hasCountElectrodeHumidifier = 0;
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					if(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> noChangeMap = allMap.get(UnitConverter.genUnitKey(unit, part));
						Map<String, String> allMap2=allMap.get(UnitConverter.genUnitKey(unit, part));
//						allMap2 = SectionDataConvertUtils.getHeatingcoil(noChangeMap,allMap2, language);
						String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_DOC_PARALIST_ELECTRODEHUMIDIFIER), project, unit, allMap2, language,unitType);
						for (int i = 0; i < contents.length; i++) {
							paramValues.add(contents[i][2]);
						}
						hasCountElectrodeHumidifier ++;
					}
				}
				for(int i=0;i<maxCountElectrodeHumidifier-hasCountElectrodeHumidifier;i++){
					String[][] contents = ReportContent.getReportContent(REPORT_DOC_PARALIST_ELECTRODEHUMIDIFIER);
					for (int j = 0; j < contents.length; j++) {
						paramValues.add(UtilityConstant.SYS_BLANK);
					}
				}
				
				//others：
				int sysTotalStatic = 0;		//总系统压降Pa (风机段的系统压降(Pa)meta_section_fan_sysStatic 的总和)
				int sysATTDISResistance = 0;//消声出风段Pa (【消音段阻力meta.section.attenuator.Resistance】  +  【出风段阻力meta.section.discharge.Resistance】)
				int coilresi = 0;			//表冷挡水段Pa (【冷水盘管段挡水器阻力meta_section_coolingCoil_driftEliminatorResistance】  +  【夏季空气阻力meta_section_coolingCoil_SAirResistance】)
				int filterresi = 0;			//进风过滤段Pa (【混合段的阻力】   + 【单层过滤段平均阻力】  + 【综合过滤段.板式.平均阻力】  +  【综合过滤段.袋式.平均阻力】)
				for (PartPO partPO : partList) {
					Part part = partPO.getCurrentPart();
					String key = part.getSectionKey();
					//总系统压降Pa
					if(SectionTypeEnum.TYPE_FAN.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> allMap2 = allMap.get(UnitConverter.genUnitKey(unit, part));
						sysTotalStatic += BaseDataUtil.trans2Double(String.valueOf(allMap2.get(UtilityConstant.METASEXON_FAN_SYSSTATIC)));

					}
					//消声出风段Pa
					if(SectionTypeEnum.TYPE_ATTENUATOR.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> allMap2 = allMap.get(UnitConverter.genUnitKey(unit, part));
						sysATTDISResistance += BaseDataUtil.trans2Double(String.valueOf(allMap2.get(UtilityConstant.METASEXON_ATTENUATOR_RESISTANCE)));

					}
					if(SectionTypeEnum.TYPE_DISCHARGE.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> allMap2 = allMap.get(UnitConverter.genUnitKey(unit, part));
						sysATTDISResistance += BaseDataUtil.trans2Double(String.valueOf(allMap2.get(UtilityConstant.METASEXON_DISCHARGE_RESISTANCE)));

					}
					//表冷挡水段Pa
					if(SectionTypeEnum.TYPE_COLD.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> noChangeMap=ValueFormatUtil.getAllUnitMetaJsonData(unit, partList).get(UnitConverter.genUnitKey(unit, part));
	                    Map<String, String> allMap2=UnitUtil.clone(noChangeMap);
						allMap2 = SectionDataConvertUtils.getCoolingCoil(noChangeMap,allMap2, language);
						coilresi += (BaseDataUtil.trans2Double(String.valueOf(allMap2.get(UtilityConstant.METASEXON_COOLINGCOIL_DRIFTELIMINATORRESISTANCE)))
								+BaseDataUtil.trans2Double(String.valueOf(allMap2.get(UtilityConstant.METASEXON_COOLINGCOIL_SAIRRESISTANCE))));
					}
					
					//进风过滤段Pa
					if(SectionTypeEnum.TYPE_MIX.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> allMap2 = allMap.get(UnitConverter.genUnitKey(unit, part));
						filterresi += BaseDataUtil.trans2Double(String.valueOf(allMap2.get(UtilityConstant.METASEXON_MIX_RESISTANCE)));

					}
					if(SectionTypeEnum.TYPE_SINGLE.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> allMap2 = allMap.get(UnitConverter.genUnitKey(unit, part));
						filterresi += BaseDataUtil.trans2Double(String.valueOf(allMap2.get(UtilityConstant.METASEXON_FILTER_DEVERAGEPD)));

					}
					if(SectionTypeEnum.TYPE_COMPOSITE.equals(SectionTypeEnum.getSectionTypeFromId(key))){
						Map<String, String> allMap2 = allMap.get(UnitConverter.genUnitKey(unit, part));
						filterresi += BaseDataUtil.trans2Double(String.valueOf(allMap2.get(UtilityConstant.METASEXON_COMBINEDFILTER_EVERAGEPDP)));//板式
						filterresi += BaseDataUtil.trans2Double(String.valueOf(allMap2.get(UtilityConstant.METASEXON_COMBINEDFILTER_EVERAGEPDB)));//袋式
					}
				}
				Map<String, String> theAllMap = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);
				theAllMap.put(UtilityConstant.METASEXON_OTHERS_SYSSTATIC, UtilityConstant.SYS_BLANK+sysTotalStatic);
				theAllMap.put(UtilityConstant.METASEXON_OTHERS_RESISTANCE, UtilityConstant.SYS_BLANK+sysATTDISResistance);
				theAllMap.put(UtilityConstant.METASEXON_OTHERS_COILRESI, UtilityConstant.SYS_BLANK+coilresi);
				theAllMap.put(UtilityConstant.METASEXON_OTHERS_FILTERRESI, UtilityConstant.SYS_BLANK+filterresi);
				
				//机箱分段信息
				Partition partion = reportData.getPartitionMap().get(unit.getUnitid());
				if(partion==null) {
					logger.warn("报表-创建-[参数汇总]-报表内容-异常, UnitId: " + unit.getUnitid()+":"+unit.getName()+":"+unit.getDrawingNo());
					continue;
				}
				//获取分段信息明细
//				List<PartitionObject> objPartitions = gson.fromJson(partion.getPartitionJson(),
//						new TypeToken<List<PartitionObject>>() {
//						}.getType());
				List<AhuPartition> objPartitions = AhuPartitionUtils.parseAhuPartition(unit, partion);
				int tempMaxPartitions = objPartitions.size();
				String serial = unit.getSeries();
				String xilie = serial.substring(serial.length() - 4);
				for (int i = 0; i <  tempMaxPartitions; i++) {
					AhuPartition po = objPartitions.get(i);
					if(null != po){
						int length = po.getLength();
						int width = po.getWidth();
						int height = po.getHeight();
						int casingWidth = po.getCasingWidth();
						//长宽高
						int c = length+casingWidth;
						int k = width*100 + casingWidth;
						//TODO 高度特殊处理根据机组形式(立式卧式等等)不同老代码如下
						/*if FrmvarTunit.UnitPro.unitmodel=0 then
		                        begin
		                              if rightstr(varfanstype,4)>='2532' then
		                                  VarGroupH:=strtoint(midstr(VarFansType,4 + iAuxiH,2)) * 100 + 200 + iAuxiLen
		                              else
		                                  VarGroupH:=strtoint(midstr(VarFansType,4 + iAuxiH,2)) * 100 + 100 + iAuxiLen;
		                        end
		                        else
		                        begin
		                              if varTpart.Position=0 then
		                                  VarGroupH:=strtoint(midstr(VarFansType,4 + iAuxiH,2)) * 100 + iAuxiLen
		                              else
		                                  begin
		                                      if rightstr(varfanstype,4)>='2532' then
		                                      VarGroupH:=strtoint(midstr(VarFansType,4 + iAuxiH,2)) * 100 + 200 + iAuxiLen
		                                      else
		                                      VarGroupH:=strtoint(midstr(VarFansType,4 + iAuxiH,2)) * 100 + 100 + iAuxiLen; //@modified by zhengyu
		                                  end;
		                 end;*/
						int g = 0;
//						if (SystemCountUtil.gteBigUnit(xilie)) {
//							g = height*100 +(po.isTopLayer()?0:200)+ casingWidth;
//						}else{
//							g = height*100 +(po.isTopLayer()?0:100)+ casingWidth;	
//						}
						g = BaseDataUtil.stringConversionInteger((SectionContentConvertUtils.wrapHeight(allMap.get(UnitConverter.UNIT), partion, unit, po)));
						
						paramValues.add(c + UtilityConstant.SYS_PUNCTUATION_STAR + k + UtilityConstant.SYS_PUNCTUATION_STAR + g);
					}else{
						paramValues.add(UtilityConstant.SYS_BLANK);
					}
				}
				for(int i=0;i<maxPartition-tempMaxPartitions;i++){
						paramValues.add(UtilityConstant.SYS_BLANK);//补充空cell
				}
				
				//orthers
				String[][] othersContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_DOC_PARALIST_OTHER), project, unit, theAllMap, language,unitType);
				for (int i = 0; i < othersContents.length; i++) {
					paramValues.add(othersContents[i][2]);
				}
				
				paramValuesList.add(paramValues);
			}
		}
		XSSFCellStyle wrapStyle = getWrapStyle(wb,UtilityConstant.SYS_FONT_ARIAL,9,false,false);// 设置内容列字体和cell样式
		formatSheet4Paralist(sheet, paramValuesList,writeIndex,wrapStyle);
		
		
		FileOutputStream os = new FileOutputStream(file);
		wb.write(os);
		os.close();
		wb.close();
		fileInputStream.close();
	}

	/**
	 * 写入sheet 标题列
	 * @param wb
	 * @param sheet
	 * @param maxCountMix
	 * @param maxCountCoolingCoil
	 * @param maxCountHeatingCoil
	 * @param maxCountFan
	 * @param maxCountSteamHumidifier
	 * @param maxCountWetFilmHumidifier
	 * @param maxCountSprayHumidifier
	 * @param maxCountElectrodeHumidifier
	 * @param ahuContents
	 * @param mixContents
	 * @param coolingCoilContents
	 * @param heatingCoilContents
	 * @param fanContents
	 * @param otherContents
	 * @param steamHumidifierContents
	 * @param wetFilmHumidifierContents
	 * @param sprayHumidifierContents
	 * @param electrodeHumidifierContents
	 * @param titleIndex
	 * @param maxPartition
	 * @throws IOException
	 */
	private static void writeSheetTitle4Paralist(XSSFWorkbook wb, XSSFSheet sheet, int maxCountMix,
			int maxCountCoolingCoil, int maxCountHeatingCoil, int maxCountFan, int maxCountSteamHumidifier,
			int maxCountWetFilmHumidifier, int maxCountSprayHumidifier, int maxCountElectrodeHumidifier,
			String[][] ahuContents, String[][] mixContents, String[][] coolingCoilContents,
			String[][] heatingCoilContents, String[][] fanContents, String[][] otherContents,
			String[][] steamHumidifierContents, String[][] wetFilmHumidifierContents,
			String[][] sprayHumidifierContents, String[][] electrodeHumidifierContents, int titleIndex,
			int maxPartition) throws IOException {
		
		
		checkSheetContents(ahuContents);
		checkSheetContents(mixContents);
		checkSheetContents(coolingCoilContents);
		checkSheetContents(heatingCoilContents);
		checkSheetContents(fanContents);
		checkSheetContents(otherContents);
		checkSheetContents(steamHumidifierContents);
		checkSheetContents(wetFilmHumidifierContents);
		checkSheetContents(sprayHumidifierContents);
		checkSheetContents(electrodeHumidifierContents);
		
		XSSFCellStyle wrapStyle = getWrapStyle(wb,UtilityConstant.SYS_FONT_SONGTI,10,true,true);// 设置字体
		
		XSSFRow rowTitle0 = sheet.createRow(titleIndex-1);
		rowTitle0.setHeightInPoints(40);                  // 设置行的高度   
		XSSFRow rowTitle = sheet.createRow(titleIndex);
		rowTitle.setHeightInPoints(60);                  // 设置行的高度   
		
		
		
		//机组基本信息
		int ahuCols = ahuContents.length;
		sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex-1, 0,ahuCols-1));
		setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex-1, 0,ahuCols-1), sheet, wb);   //合并的单元格重新加边框
		XSSFCell ahuCell = rowTitle0.createCell(0);
		ahuCell.setCellValue(getIntlString(UNIT_INFORMATION));
		ahuCell.setCellStyle(wrapStyle);
		
		int stepJ = 0;//当前写入的列index
		for (int j = 0; j < ahuCols; j++) {
			
			XSSFCell titleCell = rowTitle.createCell(j);
			String unit = ahuContents[j][1];
			//获取单位，加在名称后面用括号包起来
			titleCell.setCellValue(ahuContents[j][0]+((null!=unit && unit.length()>0)?UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+unit+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE:UtilityConstant.SYS_BLANK));
            titleCell.setCellStyle(wrapStyle);
            stepJ ++;
		}
		
		//混合段
		for (int i = 0; i < maxCountMix; i++) {
			int mixCols = mixContents.length;
			
			sheet.addMergedRegion(new CellRangeAddress(titleIndex - 1, titleIndex - 1, stepJ, stepJ + mixCols - 1));
			setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+mixCols-1), sheet, wb);   //合并的单元格重新加边框
			
			XSSFCell mixCell = rowTitle0.createCell(stepJ);
			mixCell.setCellValue(getIntlString(MIX_SECTION));
			mixCell.setCellStyle(wrapStyle);
			
			for (int j = 0; j < mixCols; j++) {
				XSSFCell titleCell = rowTitle.createCell(stepJ);
				String unit = mixContents[j][1];
				//获取单位，加在名称后面用括号包起来
				titleCell.setCellValue(
								mixContents[j][0] + ((null != unit && unit.length() > 0)
										? UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + unit
												+ UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE
										: UtilityConstant.SYS_BLANK));
				titleCell.setCellStyle(wrapStyle);
				stepJ++;
			}
		}
		
		//冷水盘管
		for (int i = 0; i < maxCountCoolingCoil; i++) {
			int ccclen = coolingCoilContents.length;
			sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+ccclen-1));
			setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+ccclen-1), sheet, wb);   //合并的单元格重新加边框
			XSSFCell cccCell = rowTitle0.createCell(stepJ);
			cccCell.setCellValue(getIntlString(COOL_COIL_PART));
			cccCell.setCellStyle(wrapStyle);
			
			for (int j = 0; j < ccclen; j++) {
				
				XSSFCell titleCell = rowTitle.createCell(stepJ);
				String unit = coolingCoilContents[j][1];
				titleCell.setCellValue(coolingCoilContents[j][0]+((null!=unit && unit.length()>0)?UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+unit+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE:UtilityConstant.SYS_BLANK));
	            titleCell.setCellStyle(wrapStyle);
	            stepJ ++;
			}
		}
		
		//热水盘管
		for (int i = 0; i < maxCountHeatingCoil; i++) {
			int hcclen = heatingCoilContents.length;
			sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+hcclen-1));
			setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+hcclen-1), sheet, wb);   //合并的单元格重新加边框
			XSSFCell cccCell = rowTitle0.createCell(stepJ);
			cccCell.setCellValue(getIntlString(HOTCOIL_PART));
			cccCell.setCellStyle(wrapStyle);
			for (int j = 0; j < hcclen; j++) {
				
				XSSFCell titleCell = rowTitle.createCell(stepJ);
				String unit = heatingCoilContents[j][1];
				titleCell.setCellValue(heatingCoilContents[j][0]+((null!=unit && unit.length()>0)?UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+unit+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE:UtilityConstant.SYS_BLANK));
	            titleCell.setCellStyle(wrapStyle);
	            stepJ ++;
			}
		}

		//风机
		for (int i = 0; i < maxCountFan; i++) {
			int fanlen = fanContents.length;
			sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+fanlen-1));
			setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+fanlen-1), sheet, wb);   //合并的单元格重新加边框
			XSSFCell fanCell = rowTitle0.createCell(stepJ);
			if (i==0) {
				fanCell.setCellValue(getIntlString(FAN_SFAN));				
			}else if (i==1) {
				fanCell.setCellValue(getIntlString(FAN_RFAN));
			}
			fanCell.setCellStyle(wrapStyle);
			for (int j = 0; j < fanlen; j++) {
			
				
				XSSFCell titleCell = rowTitle.createCell(stepJ);
				String unit = fanContents[j][1];
				titleCell.setCellValue(fanContents[j][0]+((null!=unit && unit.length()>0)?UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+unit+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE:UtilityConstant.SYS_BLANK));
	            titleCell.setCellStyle(wrapStyle);
	            stepJ ++;
			}
		}
		
		//其他信息
		
		//干蒸加湿段
		for (int i = 0; i < maxCountSteamHumidifier; i++) {
			int sthlen = steamHumidifierContents.length;
//			sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+sthlen-1));
//			setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+sthlen-1), sheet, wb);   //合并的单元格重新加边框
			XSSFCell sthCell = rowTitle0.createCell(stepJ);
			sthCell.setCellValue(getIntlString(DRY_STEAM_HUMIDIFICATION_SECTION));
			sthCell.setCellStyle(wrapStyle);
			
			for (int j = 0; j < sthlen; j++) {
				
				XSSFCell titleCell = rowTitle.createCell(stepJ);
				String unit = steamHumidifierContents[j][1];
				titleCell.setCellValue(steamHumidifierContents[j][0]+((null!=unit && unit.length()>0)?UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+unit+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE:UtilityConstant.SYS_BLANK));
	            titleCell.setCellStyle(wrapStyle);
	            stepJ ++;
			}
		}
		
		//湿膜加湿段
		for (int i = 0; i < maxCountWetFilmHumidifier; i++) {
			int wtflen = wetFilmHumidifierContents.length;
//			sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+wtflen-1));
//			setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+wtflen-1), sheet, wb);   //合并的单元格重新加边框
			XSSFCell sthCell = rowTitle0.createCell(stepJ);
			sthCell.setCellValue(getIntlString(WET_FILM_HUMIDIFICATION_SECTION));
			sthCell.setCellStyle(wrapStyle);
			
			for (int j = 0; j < wtflen; j++) {
				
				XSSFCell titleCell = rowTitle.createCell(stepJ);
				String unit = wetFilmHumidifierContents[j][1];
				titleCell.setCellValue(wetFilmHumidifierContents[j][0]+((null!=unit && unit.length()>0)?UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+unit+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE:UtilityConstant.SYS_BLANK));
	            titleCell.setCellStyle(wrapStyle);
	            stepJ ++;
			}
		}
		
		//高压喷雾加湿段
		for (int i = 0; i < maxCountSprayHumidifier; i++) {
			int spylen = sprayHumidifierContents.length;
//			sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+spylen-1));
//			setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+spylen-1), sheet, wb);   //合并的单元格重新加边框
			XSSFCell sthCell = rowTitle0.createCell(stepJ);
			sthCell.setCellValue(getIntlString(HIGH_PRESSURE_SPRAY_HUMIDIFICATION_SECTION));
			sthCell.setCellStyle(wrapStyle);
			
			for (int j = 0; j < spylen; j++) {
				
				XSSFCell titleCell = rowTitle.createCell(stepJ);
				String unit = sprayHumidifierContents[j][1];
				titleCell.setCellValue(sprayHumidifierContents[j][0]+((null!=unit && unit.length()>0)?UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+unit+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE:UtilityConstant.SYS_BLANK));
	            titleCell.setCellStyle(wrapStyle);
	            stepJ ++;
			}
		}
		
		//电极加湿段
		for (int i = 0; i < maxCountElectrodeHumidifier; i++) {
			int elclen = electrodeHumidifierContents.length;
//			sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+elclen-1));
//			setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ,stepJ+elclen-1), sheet, wb);   //合并的单元格重新加边框
			XSSFCell sthCell = rowTitle0.createCell(stepJ);
			sthCell.setCellValue(getIntlString(ELECTRODE_HUMIDIFICATION_SECTION));
			sthCell.setCellStyle(wrapStyle);
			
			for (int j = 0; j < elclen; j++) {
				
				XSSFCell titleCell = rowTitle.createCell(stepJ);
				String unit = electrodeHumidifierContents[j][1];
				titleCell.setCellValue(electrodeHumidifierContents[j][0]+((null!=unit && unit.length()>0)?UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+unit+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE:UtilityConstant.SYS_BLANK));
	            titleCell.setCellStyle(wrapStyle);
	            stepJ ++;
			}
		}
		
		//机箱
		XSSFCell jxCell0 = rowTitle0.createCell(stepJ);
		jxCell0.setCellStyle(wrapStyle);
		jxCell0.setCellValue(getIntlString(UNIT_DIMENSION_L_W_H));
		for (int i = 0 ;i<maxPartition;i++) {
			XSSFCell jxCell = rowTitle.createCell(stepJ);//机箱cell
			jxCell.setCellStyle(wrapStyle);
			jxCell.setCellValue(getIntlString(UNIT1)+(i+1));
			stepJ ++;
		}
		//merge 机箱
		if(maxPartition>1) {
			sheet.addMergedRegion(new CellRangeAddress(titleIndex - 1, titleIndex - 1, stepJ - maxPartition, stepJ - 1));
			setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex - 1, titleIndex - 1, stepJ - maxPartition, stepJ - 1), sheet, wb);   //合并的单元格重新加边框
		}
		int otherlen = otherContents.length;
		for (int j = 0; j < otherlen; j++) {
			
			XSSFCell rectangleCell = rowTitle0.createCell(stepJ+j);
			rectangleCell.setCellStyle(wrapStyle);
			if(j==0 || j==4){
				String unit = otherContents[j][1];
				rectangleCell.setCellValue(otherContents[j][0]+((null!=unit && unit.length()>0)?UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN+unit+UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE:UtilityConstant.SYS_BLANK));
			}
			if(j==1){
				rectangleCell.setCellValue(getIntlString(FUNCTIONAL_SECTION_RESISTANCE_LOSS));
			}
		}
		
		
		//merge 机组静止重量
		sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex, stepJ,stepJ));
		setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex, stepJ,stepJ), sheet, wb);   //合并的单元格重新加边框
		//merge 功能段阻力损耗		
		sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ+1,stepJ+3));
		setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex-1, stepJ+1,stepJ+3), sheet, wb);   //合并的单元格重新加边框
		//merge 机组总压损(Pa)
		sheet.addMergedRegion(new CellRangeAddress(titleIndex-1, titleIndex, stepJ+4,stepJ+4));
		setBorderStyle(HSSFCellStyle.BORDER_THIN, new CellRangeAddress(titleIndex-1, titleIndex, stepJ+4,stepJ+4), sheet, wb);   //合并的单元格重新加边框

		
		for (int j = 0; j < otherlen; j++) {
			
			XSSFCell titleCell = rowTitle.createCell(stepJ);
			String unit = otherContents[j][1];
			titleCell
					.setCellValue(
							otherContents[j][0] + ((null != unit && unit.length() > 0)
									? UtilityConstant.SYS_PUNCTUATION_BRACKET_OPEN + unit
											+ UtilityConstant.SYS_PUNCTUATION_BRACKET_CLOSE
									: UtilityConstant.SYS_BLANK));
            titleCell.setCellStyle(wrapStyle);
            stepJ ++;
		}
		
		String ref = (Columns.getCorrespondingLabel(1)+(titleIndex+1))+":"+(Columns.getCorrespondingLabel(stepJ)+(titleIndex+1));
		CellRangeAddress range = CellRangeAddress.valueOf(ref);
		sheet.setAutoFilter(range );//设置过滤下拉框
	}
	/**
	 * 获取字体
	 * @param wb
	 * @return
	 */
	private static XSSFCellStyle getWrapStyle(XSSFWorkbook wb,String fontName,int fontSize,boolean isBold,boolean hasBorder) {
		// 创建字体对象     
		XSSFFont ztFont = wb.createFont();     
		ztFont.setFontName(fontName);
		ztFont.setFontHeightInPoints((short)fontSize);    // 将字体大小设置为?px     
		ztFont.setBold(isBold); //字体加粗 
		// 创建单元格样式对象     
		XSSFCellStyle wrapStyle = (XSSFCellStyle)wb.createCellStyle();     
		wrapStyle.setWrapText(true);
		wrapStyle.setAlignment(HorizontalAlignment.CENTER);   
		wrapStyle.setVerticalAlignment(VerticalAlignment.CENTER);   
		
		if(hasBorder){
			wrapStyle.setBorderBottom(BorderStyle.THIN);     
			wrapStyle.setBorderTop(BorderStyle.THIN);     
			wrapStyle.setBorderLeft(BorderStyle.THIN);     
			wrapStyle.setBorderRight(BorderStyle.THIN);    
		}
		wrapStyle.setFont(ztFont);
		return wrapStyle;
	}
	/**
	 * 合并后的单元格重新渲染  border
	 * @param border
	 * @param region
	 * @param sheet
	 * @param wb
	 */
	public static void setBorderStyle(short border, CellRangeAddress region, XSSFSheet sheet, XSSFWorkbook wb){
        RegionUtil.setBorderBottom(border, region, sheet, wb);		//下边框
        RegionUtil.setBorderLeft(border, region, sheet, wb);     //左边框
        RegionUtil.setBorderRight(border, region, sheet, wb);    //右边框
        RegionUtil.setBorderTop(border, region, sheet, wb);      //上边框
    }
	/**
	 * 检查绘制表格的数据
	 * 
	 * @param content
	 * @throws IOException
	 */
	public static void checkSheetContents(String[][] content) throws IOException {
		if (EmptyUtil.isEmpty(content)) {
			throw new IOException(MessageFormat.format(ContentsErrorMsg, new Object[] { "content is Null" }));
		}
		int rows = content.length;
		if (rows == 0) {
			throw new IOException(MessageFormat.format(ContentsErrorMsg, new Object[] { "Rows is 0" }));
		}
		int cols = content[0].length;
		if (cols == 0) {
			throw new IOException(MessageFormat.format(ContentsErrorMsg, new Object[] { "Column counts is 0" }));
		}
		for (int i = 0; i < rows; i++) {
			if (content[i].length != cols) {
				throw new IOException(MessageFormat.format(ContentsErrorMsg,
						new Object[] { "Column counts is different at row " + (i + 1) }));
			}
		}
	}
	/**
	 * 写入内容cell
	 * @param sheet
	 * @param paramValuesList
	 * @param writeIndex
	 * @param style
	 */
	private static void formatSheet4Paralist(XSSFSheet sheet, List<LinkedList<String>> paramValuesList,int writeIndex,XSSFCellStyle style) {
		for (LinkedList<String> valueList : paramValuesList) {
			writeIndex++;
			XSSFRow row = sheet.createRow(writeIndex);
			int j = 0;
			for (String value : valueList) {
				XSSFCell tempCell = row.createCell(j++);
				tempCell.setCellValue(value);
				tempCell.setCellStyle(style);
			}
		}
	}

	/**
	 * 创建-[长交货期明细]-报表内容
	 * 
	 * @param file
	 * @param deliveryList
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void generateExcelDelivery(File file, List<ReportExcelDelivery> deliveryList) throws IOException {
		FileInputStream fileInputStream = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fileInputStream);
		XSSFSheet sheet = wb.getSheetAt(0);
		formatSheet4Delivery(sheet, deliveryList);
		FileOutputStream os = new FileOutputStream(file);
		wb.write(os);
		os.close();
		wb.close();
		fileInputStream.close();
	}
	/**
	 * 创建-[CRM 清单]-报表内容
	 *
	 * @param file
	 * @param CRMList
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void generateExcelCRMList(File file, List<ReportExcelCRMList> CRMList) throws IOException {
		FileInputStream fileInputStream = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fileInputStream);
		XSSFSheet sheet = wb.getSheetAt(0);
		formatSheet4CRMList(sheet, CRMList);
		FileOutputStream os = new FileOutputStream(file);
		wb.write(os);
		os.close();
		wb.close();
		fileInputStream.close();
	}

    public static void generateExcelSAPPriceCode(File file, List<SAPFunction> sapFunctions,
            List<PanelCalculationObj> casingList) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);

		Workbook wb = null;
		/* 判断文件的类型，是2003还是2007 */
		boolean isExcel2003 = true;
		if (ExcelUtils.isExcel2007(file.getPath())) {
			isExcel2003 = false;
		}
		try {
			if (isExcel2003) {
				wb = ExcelUtils.openExcelByPOIFSFileSystem(fileInputStream);
			} else {
				wb = ExcelUtils.openExcelByFactory(fileInputStream);
			}
		} catch (EncryptedDocumentException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} finally {
			if (!EmptyUtil.isEmpty(fileInputStream)) {
				fileInputStream.close();
			}
		}

		Sheet sheet = wb.getSheetAt(0);
        formatSheet4SAPFunction(sheet, sapFunctions);

        sheet = wb.getSheetAt(2);
        formatSheet4CasingList(sheet, casingList);

        FileOutputStream os = new FileOutputStream(file);
        wb.write(os);
        os.close();
        wb.close();
        fileInputStream.close();
    }

    private static void formatSheet4SAPFunction(Sheet sheet, List<SAPFunction> sapFunctions) {
        int i = 2;
        for (SAPFunction sapFunction : sapFunctions) {
			Row row = sheet.createRow(i++);
            row.createCell(0).setCellValue(sapFunction.getFuncName());
            row.createCell(1).setCellValue(sapFunction.getFuncCode());
            row.createCell(2).setCellValue(sapFunction.getFuncList());
            row.createCell(3).setCellValue(sapFunction.getInstSection());
            row.createCell(4).setCellValue(sapFunction.getSectionPosition());
            row.createCell(5).setCellValue(sapFunction.getSectionWeight());
        }
    }

    private static void formatSheet4CasingList(Sheet sheet, List<PanelCalculationObj> casingList) {
        int i = 1;
        for (PanelCalculationObj casingObj : casingList) {
			Row row = sheet.createRow(i++);
            row.createCell(0).setCellValue(casingObj.getPartName());
            row.createCell(1).setCellValue(casingObj.getPartLM());
            row.createCell(2).setCellValue(casingObj.getPartWM());
            row.createCell(3).setCellValue(casingObj.getQuantity());
            row.createCell(4).setCellValue(EmptyUtil.toString(casingObj.getMemo(), StringUtils.EMPTY));
            row.createCell(5).setCellValue(casingObj.getPro());
            row.createCell(6).setCellValue(casingObj.getInstSection());
            row.createCell(7).setCellValue(casingObj.getInstPostion());
        }
    }

	private static void formatSheet4Delivery(Sheet sheet, List<ReportExcelDelivery> deliveryList) {
		int i = 2;
		for (ReportExcelDelivery dly : deliveryList) {
			Row row = sheet.createRow(i++);
			row.createCell(0).setCellValue(dly.getUnitId());
			row.createCell(1).setCellValue(dly.getSerial());
			row.createCell(2).setCellValue(dly.getAhuName());

			for (int j = 0; j < dly.getDelivery().length; j++) {
				if (j==0) {
					row.createCell(3).setCellValue(dly.getDelivery()[j]);
					row.createCell(4).setCellValue(dly.getDays()[j]);
				}else {
					Row rowDetail = sheet.createRow(i++);
					rowDetail.createCell(3).setCellValue(dly.getDelivery()[j]);
					rowDetail.createCell(4).setCellValue(dly.getDays()[j]);					
				}
			}
		}
	}
	private static void formatSheet4CRMList(XSSFSheet sheet, List<ReportExcelCRMList> CRMList) {
		int i = 1;
		CellStyle style14 = sheet.getColumnStyle(14);//获取绿色建筑模板样式
		for (ReportExcelCRMList crm : CRMList) {
			XSSFRow row = sheet.createRow(i++);
			row.createCell(0).setCellValue(crm.getDrawingNo());
			row.createCell(1).setCellValue(crm.getUnitType());
			row.createCell(2).setCellValue(crm.getSerial());
			row.createCell(3).setCellValue(crm.getMount());
			row.createCell(4).setCellValue(crm.getFacePrice());
			row.createCell(5).setCellValue(UtilityConstant.SYS_BLANK);
			row.createCell(6).setCellValue(crm.getProductionPlace());
			row.createCell(7).setCellValue(crm.getContract());
			row.createCell(8).setCellValue(crm.getSelectionTimes());

			row.createCell(10).setCellValue(crm.getContractSerial());
			row.createCell(11).setCellValue(crm.getFactoryForm().equals(AHU_DELIVERY_CKD)?Math.round(crm.getStdPrice()*0.06):0);
			row.createCell(12).setCellValue(crm.getFactoryForm());
			row.createCell(13).setCellValue(crm.getIsHealthBuilding());

			XSSFCell hb = row.createCell(14);//绿色建筑
			hb.setCellStyle(style14);//保留模板样式防止文本过长
			hb.setCellValue(crm.getHealthBuilding());
		}
	}

	@SuppressWarnings("deprecation")
	protected static void formatSheetDemo(XSSFWorkbook wb, XSSFSheet sheet) {
		// 设置excel每列宽度
		sheet.setColumnWidth(0, 4000);
		sheet.setColumnWidth(1, 3500);
		// 创建字体样式
		XSSFFont font = wb.createFont();
		font.setFontName(UtilityConstant.SYS_FONT_VERDANA);
		font.setFontHeight((short) 300);
		font.setColor(IndexedColors.BLUE.index);

		// 创建单元格样式
		XSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.index);
		style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

		// 设置边框
		style.setBottomBorderColor(IndexedColors.RED.index);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);

		style.setFont(font);// 设置字体

		// 创建Excel的sheet的一行
		XSSFRow row = sheet.createRow(0);
		row.setHeight((short) 500);// 设定行的高度
		// 创建一个Excel的单元格
		XSSFCell cell = row.createCell(0);

		// 合并单元格(startRow，endRow，startColumn，endColumn)
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 2));

		// 给Excel的单元格设置样式和赋值
		cell.setCellStyle(style);
		cell.setCellValue("hello world");

		// 设置单元格内容格式
		XSSFCellStyle style1 = wb.createCellStyle();
		XSSFDataFormat fmt = wb.createDataFormat();
		style1.setDataFormat(fmt.getFormat(UtilityConstant.SYS_FORMAT_h_mm_ss));

		style1.setWrapText(true);// 自动换行

		row = sheet.createRow(1);

		// 设置单元格的样式格式

		cell = row.createCell(0);
		cell.setCellStyle(style1);
		cell.setCellValue(new Date());

		// 创建超链接
		// HSSFHyperlink link = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
		// link.setAddress("http://www.baidu.com");
		// cell = row.createCell(1);
		// cell.setCellValue("百度");
		// cell.setHyperlink(link);// 设定单元格的链接
	}
	/**
	 * 生成延长交货周期选项信息
	 * @param unit
	 * @param partList
	 * @return
	 */
	public static ReportExcelDelivery generateReportExcelDelivery(Unit unit, List<Part> partList) {
		ReportExcelDelivery reprotDelivery = new ReportExcelDelivery();
		reprotDelivery.setUnitId(unit.getUnitNo());
		reprotDelivery.setSerial(unit.getSeries());
		reprotDelivery.setAhuName(unit.getName());
		List<String> deliverys = new ArrayList<String>();
		List<String> days = new ArrayList<String>();
		String xilie = unit.getSeries().substring(unit.getSeries().length() - 4);
		DeliveryInfo dinfo = null;
		boolean wheelPlateHandled = false;//是否已经处理了热回收
		for (int i = 0; i < partList.size(); i++) {
			String sectionKey = partList.get(i).getSectionKey();
			String preSectionKey = i>0?partList.get(i-1).getSectionKey():"";
			String nextSectionKey = i<partList.size()-1?partList.get(i+1).getSectionKey():"";

			Map<String, String> allMap = new HashMap<>();
			Map<String, String> preAllMap = null;
			Map<String, String> nextAllMap = null;

			Part tempPart = partList.get(i);
			allMap.putAll(JSON.parseObject(tempPart.getMetaJson(), HashMap.class));
			if(EmptyUtil.isNotEmpty(preSectionKey)){
				preAllMap = new HashMap<>();
				Part preTempPart = partList.get(i);
				preAllMap.putAll(JSON.parseObject(preTempPart.getMetaJson(), HashMap.class));
			}
			if(EmptyUtil.isNotEmpty(nextSectionKey)){
				nextAllMap = new HashMap<>();
				Part nextTempPart = partList.get(i);
				nextAllMap.putAll(JSON.parseObject(nextTempPart.getMetaJson(), HashMap.class));
			}

			if(SectionTypeEnum.TYPE_FAN.getId().equals(sectionKey)){//风机

				//风机供应商
				String theFanSupplierKey = UtilityConstant.METASEXON_FAN_FANSUPPLIER;
				String theFanSupplier = allMap.get(theFanSupplierKey);
				//电机品牌
				String fanSupplierKey = UtilityConstant.METASEXON_FAN_SUPPLIER;
				String fanSupplier = allMap.get(fanSupplierKey);
				String fanSupplierCN = MetaSelectUtil.getTextForSelect(fanSupplierKey, fanSupplier, AHUContext.getLanguage());
				//电机类型
				String fanTypeKey = UtilityConstant.METASEXON_FAN_TYPE;
				String fanType = allMap.get(fanTypeKey);
				String fanTypeCN = MetaSelectUtil.getTextForSelect(fanTypeKey, fanType, AHUContext.getLanguage());

				//电机功率
				String fanMotorPowerKey = UtilityConstant.METASEXON_FAN_MOTORPOWER;;
				String fanMotorPower = allMap.get(fanMotorPowerKey);

				//电源
				/*Frm_K	VOLTAGE0	380V-3Ph-50Hz	380V-3Ph-50Hz	0
				Frm_K	VOLTAGE1	380V-3Ph-60Hz	380V-3Ph-60Hz	1
				Frm_K	VOLTAGE2	230V-3Ph-50Hz	230V-3Ph-50Hz	2
				Frm_K	VOLTAGE3	230V-3Ph-60Hz	230V-3Ph-60Hz	3
				Frm_K	VOLTAGE4	400V-3Ph-50Hz	400V-3Ph-50Hz	4
				Frm_K	VOLTAGE5	400V-3Ph-60Hz	400V-3Ph-60Hz	5
				Frm_K	VOLTAGE6	415V-3Ph-50Hz	415V-3Ph-50Hz	6
				Frm_K	VOLTAGE7	415V-3Ph-60Hz	415V-3Ph-60Hz	7
				Frm_K	VOLTAGE8	460V-3Ph-50Hz	460V-3Ph-50Hz	8
				Frm_K	VOLTAGE9	460V-3Ph-60Hz	460V-3Ph-60Hz	9
				*/
				//启动方式
				String startStyleKey = UtilityConstant.METASEXON_FAN_STARTSTYLE;
				String startStyle = allMap.get(startStyleKey);

				String power = "0";//默认
				String powerCN = MetaSelectUtil.getTextForSelect(METASEXON_FAN_POWER, power, AHUContext.getLanguage());
				switch (fanSupplier) {
					case UtilityConstant.JSON_FAN_SUPPLIER_ABB:
						fanSupplier = UtilityConstant.SYS_ALPHABET_A_UP;//ABB
						break;
					case UtilityConstant.JSON_FAN_SUPPLIER_DONGYUAN:
						fanSupplier = UtilityConstant.SYS_ALPHABET_C_UP;//TECO电机
						break;
					case UtilityConstant.JSON_FAN_SUPPLIER_DEFAULT:
						fanSupplier = UtilityConstant.SYS_ALPHABET_B_UP;//标准供应商
						break;
					case UtilityConstant.JSON_FAN_SUPPLIER_SIMENS:
						fanSupplier = UtilityConstant.SYS_ALPHABET_S_UP;//西门子电机
						break;
					default:
						break;
				}
				switch (fanType) {
					case UtilityConstant.JSON_FAN_TYPE_NUM_2:
						fanType = UtilityConstant.SYS_ALPHABET_A_UP;//三级能效电机
						break;
					case UtilityConstant.JSON_FAN_TYPE_NUM_3:
						fanType = UtilityConstant.SYS_ALPHABET_B_UP;//防爆电机
						break;
					case UtilityConstant.JSON_FAN_TYPE_NUM_4:
						fanType = UtilityConstant.SYS_ALPHABET_C_UP;//双速电机
						break;
					case UtilityConstant.JSON_FAN_TYPE_NUM_5:
						fanType = UtilityConstant.SYS_ALPHABET_D_UP;//变频电机(IC416)
						break;
					default:
						break;
				}
				String option=fanSupplier + fanType + power;
				String optionCn=fanSupplierCN + fanTypeCN + powerCN;
				dinfo = AhuMetadata.findOne(DeliveryInfo.class, "MOTOR", option);
				if(EmptyUtil.isNotEmpty(dinfo) && Integer.parseInt(dinfo.getDelivery())>=22){
					deliverys.add(optionCn);
					days.add(dinfo.getDelivery());
				}

				//默认风机品牌 & 机组型号大于2532
				if(theFanSupplier.equals(JSON_FAN_FANSUPPLIER_A) && SystemCountUtil.gteBigUnit(Integer.parseInt(xilie))){
					dinfo = AhuMetadata.findOne(DeliveryInfo.class, "FAN", "A_BIG");
					if(EmptyUtil.isNotEmpty(dinfo)){
						deliverys.add("大型风机");
						days.add(dinfo.getDelivery());
					}
				}

				//风机非标
                String nsEnableKey = UtilityConstant.NS_SECTION_FAN_ENABLE;
                String nsChangeFanKey = UtilityConstant.NS_SECTION_FAN_NSCHANGEFAN;
                String nsEnable = String.valueOf(allMap.get(nsEnableKey));
                String nsChangeFan = String.valueOf(allMap.get(nsChangeFanKey));
				if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsEnable)
						&& UtilityConstant.SYS_ASSERT_TRUE.equals(nsChangeFan)){
					String nsFanModelKey = UtilityConstant.NS_SECTION_FAN_NSFANMODEL;
					String nsFanModel = String.valueOf(allMap.get(nsFanModelKey));
					dinfo = AhuMetadata.findOne(DeliveryInfo.class, "FAN", nsFanModel);
					if(EmptyUtil.isNotEmpty(dinfo)){
						deliverys.add("Kruger风机");
						days.add(dinfo.getDelivery());
					}
                }

				/*星-三角启动-用户安装	Star delta startup-field install	4
				VFD变频启动柜-用户安装	Variable-frequency-field install*	5
				直接启动-用户安装		Directly startup-field install		2
				无启动柜				No Startup							0

				"no","无启动柜",
				"vfd","VFD变频启动柜用户安装",
				"star","星-三角启动-用户安装",
				"direct","直接启动-用户安装",
				"enbedded","嵌入式VFD变频启动柜"
				*/
				//变频启动柜
				if(startStyle.equals(JSON_FAN_STARTSTYLE_VFD)){
					/*if Varpartproperty_K.Power>='M' then
						Cell.FormulaR1C1 :=floattostr(GetPartDeliverytime('STARTUP5', '30KW_H'))
        			else
						Cell.FormulaR1C1 :=floattostr(GetPartDeliverytime('STARTUP5', '30KW_L'));*/
					int theMotorPower = NumberUtil.convertStringToDoubleInt(fanMotorPower);
					if(theMotorPower >= 30){
						dinfo = AhuMetadata.findOne(DeliveryInfo.class, "STARTUP5", "30KW_H");
					}else{
						dinfo = AhuMetadata.findOne(DeliveryInfo.class, "STARTUP5", "30KW_L");
					}
					if(EmptyUtil.isNotEmpty(dinfo)){
						deliverys.add("变频启动柜");
						days.add(dinfo.getDelivery());
					}

				}
			}else if(SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionKey)
					|| SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getId().equals(sectionKey)){
				//高效过滤 静电过滤
				dinfo = AhuMetadata.findOne(DeliveryInfo.class, "FILTER",
						SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionKey)?"V":"Y");
				if(EmptyUtil.isNotEmpty(dinfo)){
					deliverys.add(getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionKey).getCnName()));
					days.add(dinfo.getDelivery());
				}
			}else if(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(sectionKey)
					&& !SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(preSectionKey)){
				//湿膜加湿
				dinfo = AhuMetadata.findOne(DeliveryInfo.class, "HUMIDIFY","I");
				if(EmptyUtil.isNotEmpty(dinfo)){
					deliverys.add(getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionKey).getCnName()));
					days.add(dinfo.getDelivery());
				}
			}else if(SectionTypeEnum.TYPE_COLD.getId().equals(sectionKey)
						|| SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(sectionKey)){
				/*
					17: 热水+湿膜：湿膜加湿、防冻开关
					19: 热水+高压喷雾：塑料挡水器、高压喷雾加湿、防冻开关
					21: 冷水+热水盘管 :防冻开关
					18：冷水＋高压喷雾 :塑料挡水器、高压喷雾加湿
					4:  冷水盘管 :
					5:  热水盘管：防冻开关
				*/

				boolean hasPlasticEliminator = false;
				//防冻开关
				boolean hasFrostProtectionOption = false;
				String frostProtectionOptionKey = UtilityConstant.METASEXON_HEATINGCOIL_FROSTPROTECTIONOPTION;
				String frostProtectionOption = String.valueOf(allMap.get(frostProtectionOptionKey));

				if(SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(sectionKey)
						&& SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(nextSectionKey)){
					if(frostProtectionOption.equals(SYS_ASSERT_TRUE)){//防冻开关
						hasFrostProtectionOption = true;
					}
					dinfo = AhuMetadata.findOne(DeliveryInfo.class, "HUMIDIFY","I");
					if(EmptyUtil.isNotEmpty(dinfo)){//湿膜加湿
						deliverys.add(getIntlString(SectionTypeEnum.getSectionTypeFromId(nextSectionKey).getCnName()));
						days.add(dinfo.getDelivery());
					}
				}else if(SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(sectionKey)
						&& SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(nextSectionKey)){
					dinfo = AhuMetadata.findOne(DeliveryInfo.class, "HUMIDIFY","J");
					if(EmptyUtil.isNotEmpty(dinfo)){//高压喷雾加湿
						deliverys.add(getIntlString(SectionTypeEnum.getSectionTypeFromId(nextSectionKey).getCnName()));
						days.add(dinfo.getDelivery());
					}
					//塑料挡水器
					hasPlasticEliminator = true;
				}else if(SectionTypeEnum.TYPE_COLD.getId().equals(sectionKey)
						&& SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(nextSectionKey)){
					frostProtectionOption = String.valueOf(nextAllMap.get(frostProtectionOptionKey));
					if(frostProtectionOption.equals(SYS_ASSERT_TRUE)){//防冻开关
						hasFrostProtectionOption = true;
					}

				}else if(SectionTypeEnum.TYPE_COLD.getId().equals(sectionKey)
						&& SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(nextSectionKey)){
					if(frostProtectionOption.equals(SYS_ASSERT_TRUE)){//防冻开关
						hasFrostProtectionOption = true;
					}
					dinfo = AhuMetadata.findOne(DeliveryInfo.class, "HUMIDIFY","J");
					if(EmptyUtil.isNotEmpty(dinfo)){//高压喷雾加湿
						deliverys.add(getIntlString(SectionTypeEnum.getSectionTypeFromId(nextSectionKey).getCnName()));
						days.add(dinfo.getDelivery());
					}
					//塑料挡水器
					hasPlasticEliminator = true;
				}else if(SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(sectionKey)){
					if(frostProtectionOption.equals(SYS_ASSERT_TRUE)){//防冻开关
						hasFrostProtectionOption = true;
					}
				}

				//塑料挡水器
				if(hasPlasticEliminator){
					String plasticEliminatorKey = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_ELIMINATOR;
					String plasticEliminator = nextAllMap.get(plasticEliminatorKey);
					if(plasticEliminator.equals(JSON_COOLINGCOIL_ELIMINATOR_PLASTIC)){
						dinfo = AhuMetadata.findOne(DeliveryInfo.class, "COIL","BW1");
						if(EmptyUtil.isNotEmpty(dinfo)){
							deliverys.add("塑料挡水器");
							days.add(dinfo.getDelivery());
						}
					}
				}

				if(hasFrostProtectionOption){
					dinfo = AhuMetadata.findOne(DeliveryInfo.class, "FROST","A");
					if(EmptyUtil.isNotEmpty(dinfo)){
						deliverys.add("防冻开关");
						days.add(dinfo.getDelivery());
					}
				}
			}else if(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(sectionKey)
					|| SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(sectionKey)){

				if(wheelPlateHandled){
					continue;//由于双层会出现两次热回收，需要提过循环防止两次打印热回收内容。
				}
				String modelKey = UtilityConstant.METASEXON_COOLINGCOIL_ELIMINATOR;
				String model = allMap.get(modelKey);//热回收型号
				String tempCn = "";
				String tempCode = "";
				if(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId().equals(sectionKey)){
					tempCode = "SG";
					if(model.equals(JSON_WHEELHEATRECYCLE_MODEL_1) || model.equals(JSON_WHEELHEATRECYCLE_MODEL_2)){
						tempCode = "MS";
					}
					String wheelDepthKey = UtilityConstant.METASEXON_WHEELHEATRECYCLE_WHEELDEPTH;
					String wheelDepth = allMap.get(wheelDepthKey);
					if(wheelDepth.equals(JSON_PLATEHEATRECYCLE_WHEELDEPTH_1)){
						tempCode+= JSON_PLATEHEATRECYCLE_WHEELDEPTH_1_VAl;
					}
					if(wheelDepth.equals(JSON_PLATEHEATRECYCLE_WHEELDEPTH_2)){
						tempCode+= JSON_PLATEHEATRECYCLE_WHEELDEPTH_2_VAl;
					}
					String brandKey = UtilityConstant.METASEXON_WHEELHEATRECYCLE_BRAND;
					String brand = allMap.get(brandKey);
					tempCn = "百瑞热转轮";
					if(brand.equals(JSON_WHEELHEATRECYCLE_BRAND_A)){//标准供应商热转轮
						tempCode = JSON_WHEELHEATRECYCLE_BRAND_A;
						tempCn = "标准供应商热转轮";
					}
					if(brand.equals(JSON_WHEELHEATRECYCLE_BRAND_C)){//毅科热转轮
						tempCode = JSON_WHEELHEATRECYCLE_BRAND_C;
						tempCn = "毅科热转轮";
					}

				}
				if(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(sectionKey)){
					tempCode = "A";
					tempCn = "板式热回收";
				}

				dinfo = AhuMetadata.findOne(DeliveryInfo.class, "HEATRECOVERY",tempCode);
				if(EmptyUtil.isNotEmpty(dinfo)){
					deliverys.add(tempCn);
					days.add(dinfo.getDelivery());
				}

				//板片需要进口，货备库存，下单前需要提前30个工作日书面告知备货，备货交货周期40天，不备货海运90天，空运费运费另计。
				if(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId().equals(sectionKey)){
					List<HeatxPlate> heatxPlates = AhuMetadata.findList(HeatxPlate.class,
							AhuUtil.getUnitNo(unit.getSeries()));
					String corrugatedChannel = allMap.get(UtilityConstant.METASEXON_PLATEHEATRECYCLE_CORRUGATED_CHANNEL);

					for (HeatxPlate heatxPlate : heatxPlates) {
						if(heatxPlate.getCorrugatedChannel().equals(corrugatedChannel) && "1".equals(heatxPlate.getIsLt())){
							deliverys.add("进口板式热回收型号");
							days.add("90");
						}
					}
				}
				wheelPlateHandled = true;//已经处理了热回收
			}
		}
		boolean hasYCJ = isHasYCJ(partList);
		if(hasYCJ){
			dinfo = AhuMetadata.findOne(DeliveryInfo.class, "PRESSUREGAGE","A");
			if(EmptyUtil.isNotEmpty(dinfo)){
				deliverys.add("压差计/压差开关");
				days.add(dinfo.getDelivery());
			}
		}
		boolean hasUVLamp = isHasUVLamp(partList);
		if(hasUVLamp){
			dinfo = AhuMetadata.findOne(DeliveryInfo.class, "LAMP","A");
			if(EmptyUtil.isNotEmpty(dinfo)){
				deliverys.add("紫外线杀菌灯");
				days.add(dinfo.getDelivery());
			}
		}
		String[] theDeliverys = new String[deliverys.size()];;
		deliverys.toArray(theDeliverys);
		String[] theDays  = new String[days.size()];
		days.toArray(theDays);
		reprotDelivery.setDelivery(theDeliverys);
		reprotDelivery.setDays(theDays);
		
		return reprotDelivery;
	}
	/**
	 * 杀菌灯
	 * @param partList
	 * @return
	 */
	private static boolean isHasUVLamp(List<Part> partList) {
		boolean hasUVLamp = false;
		for (int i = 0; i < partList.size(); i++) {
			String sectionKey = partList.get(i).getSectionKey();
			if (SectionTypeEnum.TYPE_MIX.getId().equals(sectionKey)
					|| SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionKey)) {
				Map<String, String> allMap = new HashMap<>();
				Part tempPart = partList.get(i);
				allMap.putAll(JSON.parseObject(tempPart.getMetaJson(), HashMap.class));
				String lampKey =UtilityConstant.METASEXON_MIX_UVLAMP;
				//混合段、空段
				if (SectionTypeEnum.TYPE_ACCESS.getId().equals(sectionKey)) {
					lampKey =UtilityConstant.METASEXON_ACCESS_UVLAMP;
				}

				Object lamp = allMap.get(lampKey);
				String valueLamp = BaseDataUtil.constraintString(lamp);
				if (UtilityConstant.SYS_ASSERT_TRUE.equals(valueLamp)) {
					hasUVLamp = true;
					break;
				}
			}
		}
		return hasUVLamp;
	}

	/**
	 * 压差计
	 * @param partList
	 * @return
	 */
	private static boolean isHasYCJ(List<Part> partList) {
		boolean hasYCJ = false;
		for (int i = 0; i < partList.size(); i++) {
			String sectionKey = partList.get(i).getSectionKey();
			Map<String, String> allMap = new HashMap<>();
			Part tempPart = partList.get(i);
			allMap.putAll(JSON.parseObject(tempPart.getMetaJson(), HashMap.class));
			//过滤段、综合过滤段、高效过滤段
			if(SectionTypeEnum.TYPE_SINGLE.getId().equals(sectionKey)){
				Object pressureGuage = allMap.get(UtilityConstant.METASEXON_FILTER_PRESSUREGUAGE);
				String valuePG = BaseDataUtil.constraintString(pressureGuage);
				if (!UtilityConstant.JSON_FILTER_PRESSUREGUAGE_WO.equals(valuePG)) {// 压差计
					hasYCJ = true;
					break;
				}
			}else if(SectionTypeEnum.TYPE_COMPOSITE.getId().equals(sectionKey)){
				// 袋式压差计
				Object pressureGuageB = allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEB);
				String valuePGB = BaseDataUtil.constraintString(pressureGuageB);
				// 板式压差计
				Object pressureGuageP = allMap.get(UtilityConstant.METASEXON_COMBINEDFILTER_PRESSUREGUAGEP);
				String valuePGP = BaseDataUtil.constraintString(pressureGuageP);
				if (!UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEB_WO.equals(valuePGB)
						|| !UtilityConstant.JSON_COMBINEDFILTER_PRESSUREGUAGEP_WO.equals(valuePGP)) {// 压差计
					hasYCJ = true;
					break;
				}
			}else if(SectionTypeEnum.TYPE_HEPAFILTER.getId().equals(sectionKey)){
				Object pressureGuage = allMap.get(UtilityConstant.METASEXON_HEPAFILTER_PRESSUREGUAGE);
				String valuePG = BaseDataUtil.constraintString(pressureGuage);
				if (!UtilityConstant.JSON_HEPAFILTER_PRESSUREGUAGE_WO.equals(valuePG)) {// 压差计
					hasYCJ = true;
					break;
				}
			}
		}
		return hasYCJ;
	}

	private static String[][] getMixContents(String[][] mixContents, Map<String, String> mixMap){
		String returnTop = UtilityConstant.METASEXON_MIX_RETURNTOP;//1
        String returnBack = UtilityConstant.METASEXON_MIX_RETURNBACK;//2
        String returnLeft = UtilityConstant.METASEXON_MIX_RETURNLEFT;//3
        String returnRight = UtilityConstant.METASEXON_MIX_RETURNRIGHT;//4
        String returnButtom = UtilityConstant.METASEXON_MIX_RETURNBUTTOM;//5
		int returnTopWF = 1;
        int returnBackWF = 2;
        int returnLeftWF = 3;
        int returnRightWF = 4;
        int returnButtomWF = 5;
        
        int count = 0;
        if (mixMap.containsKey(returnTop)) {
            String value = BaseDataUtil.constraintString(mixMap.get(returnTop));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
            	if (count == 0) {
					mixContents[0][0] = getIntlString(TOP_RETURN_AIR_COLON);
				}else if (count > 0 ) {
					mixContents[1][0] = getIntlString(TOP_RETURN_AIR_COLON);
				}
				count++;
			}
        }
        if (mixMap.containsKey(returnBack)) {
        	String value = BaseDataUtil.constraintString(mixMap.get(returnBack));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
            	if (count == 0) {
					mixContents[0][0] = getIntlString(BACK_RETURN_AIR);
					
				}else if (count > 0 ) {
					mixContents[1][0] = getIntlString(BACK_RETURN_AIR);
				}
                count++;
			}
		}
        if (mixMap.containsKey(returnLeft)) {
        	String value = BaseDataUtil.constraintString(mixMap.get(returnLeft));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
            	if (count == 0) {
					mixContents[0][0] = getIntlString(LEFT_SIDE_RETURN_AIR); 
					
				}else if (count > 0 ) {
					mixContents[1][0] = getIntlString(LEFT_SIDE_RETURN_AIR);
				}
                count++;
			}
		}
        if (mixMap.containsKey(returnRight)) {
        	String value = BaseDataUtil.constraintString(mixMap.get(returnRight));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
            	if (count == 0) {
					mixContents[0][0] = getIntlString(RIGHT_SIDE_RETURN_AIR); 
				}else if (count > 0 ) {
					mixContents[1][0] = getIntlString(RIGHT_SIDE_RETURN_AIR);
				}
                count++;
			}
		}
        if (mixMap.containsKey(returnButtom)) {
        	String value = BaseDataUtil.constraintString(mixMap.get(returnButtom));
            if (value.equals(UtilityConstant.SYS_ASSERT_TRUE)) {
            	if (count == 0) {
					mixContents[0][0] = getIntlString(BOTTOM_RETURN_AIR); 
				}else if (count > 0 ) {
					mixContents[1][0] = getIntlString(BOTTOM_RETURN_AIR);
				}
                count++;
			}
		}
		if(count >= 2){
            return mixContents;
        }else{
            String[][] sContents = new String[1][3];
            sContents[0] = mixContents[0];
            return sContents;
        }
		
	}
}

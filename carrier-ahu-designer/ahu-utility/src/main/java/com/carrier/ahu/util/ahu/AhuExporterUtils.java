package com.carrier.ahu.util.ahu;

import static com.carrier.ahu.vo.SysConstants.SLASH_LINUX;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.vo.SysConstants;

/**
 * Created by Braden Zhou on 2019/01/03.
 */
public class AhuExporterUtils {

    public static String getThreeViewPath(String projectId, String unitId, String userName) {
        return String.format(SysConstants.CAD_DIR + SLASH_LINUX + "%s" + SLASH_LINUX + "%s" + SLASH_LINUX + "%s.bmp",
                projectId, unitId, userName);
    }

    /**
     * Three view file path: /asserts/output/cad/pid/unitid/username.bmp
     * 
     * @param projectId
     * @param unitId
     * @param userName
     * @return
     */
    public static File getThreeViewAssetFile(String projectId, String unitId, String userName) {
        return new File(UtilityConstant.SYS_PATH_CAD_DEST_DIR + getThreeViewPath(projectId, unitId, userName));
    }

    private static String getTempThreeViewFilePath(String path, String fileName) {
        return path + String.format("%s.bmp", fileName);
    }

    public static File getTempThreeViewFile(String path, String fileName) {
        return new File(getTempThreeViewFilePath(path, fileName));
    }

    public static void cleanImportFilePath() {
        File importDir = new File(SysConstants.DIR_IMPORT);
        if (importDir.exists()) {
            File[] files = importDir.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    FileUtils.deleteQuietly(file);
                }
            }
        }
    }

    public static File findProjectFile(String importFilePath) {
        File importDir = new File(importFilePath);
        if (importDir.exists() && importDir.isDirectory()) {
            File[] unzippedFiles = importDir.listFiles();
            for (File unzippedFile : unzippedFiles) {
                // find .prj file
                if (unzippedFile.getName().endsWith(SysConstants.PRJ_EXTENSION)) {
                    return unzippedFile;
                }
            }
        }
        return null;
    }

    public static File findThreeViewFile(String importFilePath, String unitId) {
        String threeViewFileName = getTempThreeViewFilePath(UtilityConstant.SYS_BLANK, unitId);

        File importDir = new File(importFilePath);
        if (importDir.exists() && importDir.isDirectory()) {
            File[] unzippedFiles = importDir.listFiles();
            for (File unzippedFile : unzippedFiles) {
                // find three view file
                if (unzippedFile.getName().equals(threeViewFileName)) {
                    return unzippedFile;
                }
            }
        }
        return null;
    }

    /**
     * Find unit three view file from upload files and copy to assets folder.
     * 
     * @param importFilePath
     * @param projectId
     * @param unitId
     * @param userName
     * @throws IOException
     */
    public static void importThreeViewFile(String importFilePath, String projectId, String unitId, String userName)
            throws IOException {
        File threeViewFile = findThreeViewFile(importFilePath, unitId);
        if (threeViewFile != null) {
            File threeViewAssetFile = getThreeViewAssetFile(projectId, unitId, userName);
            File threeViewAssetDir = threeViewAssetFile.getParentFile();
            if (!threeViewAssetDir.exists()) {
                threeViewAssetDir.mkdirs();
            }
            FileUtils.copyFile(threeViewFile, threeViewAssetFile);
        }
    }

    /**
     * 导入风机曲线、三视图图片和配置文件
     * 
     * @param destPath
     * @param zipInStream
     * @throws IOException
     */
    public static void importImgFile(String destPath, InputStream zipInStream) throws IOException {
        ZipInputStream zipIn = null;
        FileOutputStream out = null;
        byte[] buffer = new byte[1024];
        try {
            zipIn = new ZipInputStream(zipInStream);
            ZipEntry entry = zipIn.getNextEntry();
            while (entry != null) {
                if (!entry.isDirectory()) { // ignore directory
                    File file = null;
                    if (entry.getName().endsWith(UtilityConstant.SYS_NAME_GUKE_BMP)) {
                        file = new File(UtilityConstant.SYS_PATH_BMP_GK + entry.getName());
                    } else if (entry.getName().endsWith(UtilityConstant.SYS_NAME_YLD_BMP)) {
                        file = new File(UtilityConstant.SYS_PATH_BMP_YLD + entry.getName());
                    } else if (entry.getName().contains(UtilityConstant.SYS_NAME_KRUGER_CHART)) {
                        file = new File(UtilityConstant.SYS_PATH_BMP_KRUGER + entry.getName());
                    } else if (entry.getName().contains(UtilityConstant.SYS_NAME_CAD_FLAG)) {
                        file = new File(UtilityConstant.SYS_PATH_BMP_CAD + entry.getName());
                    } else {
                        file = new File(destPath + entry.getName());
                    }
                    File dir = file.getParentFile();
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    file.createNewFile();
                    out = new FileOutputStream(file);
                    int len = 0;
                    while ((len = zipIn.read(buffer)) > 0) {
                        out.write(buffer, 0, len);
                    }
                    out.close();
                }
                entry = zipIn.getNextEntry();
            }
        } finally {
            IOUtils.closeQuietly(zipIn);
            IOUtils.closeQuietly(out);
        }
    }

    public static void importImgFile(InputStream zipInStream, Map<String, String> updateIds) throws IOException {
        ZipInputStream zipIn = null;
        FileOutputStream out = null;
        byte[] buffer = new byte[1024];
        try {
            zipIn = new ZipInputStream(zipInStream);
            ZipEntry entry = zipIn.getNextEntry();
            while (entry != null) {
                if (entry.isDirectory()) { // ignore directory
                    entry = zipIn.getNextEntry();
                    continue;
                }

                try {
                    // 替换更新后的项目和机组id
                    String fileName = entry.getName();
                    for (Map.Entry<String, String> updateId : updateIds.entrySet()) {
                        fileName = fileName.replace(updateId.getKey(), updateId.getValue());
                    }

                    File file = null;
                    if (fileName.endsWith(UtilityConstant.SYS_NAME_GUKE_BMP)) {
                        file = new File(UtilityConstant.SYS_PATH_BMP_GK + fileName);
                    } else if (fileName.endsWith(UtilityConstant.SYS_NAME_YLD_BMP)) {
                        file = new File(UtilityConstant.SYS_PATH_BMP_YLD + fileName);
                    } else if (fileName.contains(UtilityConstant.SYS_NAME_KRUGER_CHART)) {
                        file = new File(UtilityConstant.SYS_PATH_BMP_KRUGER + fileName);
                    } else if (fileName.contains(UtilityConstant.SYS_NAME_CAD_FLAG)) {
                        file = new File(UtilityConstant.SYS_PATH_BMP_CAD + fileName);
                    }
                    if (file != null) {
                        File dir = file.getParentFile();
                        if (!dir.exists()) {
                            dir.mkdirs();
                        }
                        file.createNewFile();
                        out = new FileOutputStream(file);
                        int len = 0;
                        while ((len = zipIn.read(buffer)) > 0) {
                            out.write(buffer, 0, len);
                        }
                    }
                } finally {
                    IOUtils.closeQuietly(out);
                }
                entry = zipIn.getNextEntry();
            }
        } finally {
            IOUtils.closeQuietly(zipIn);
        }
    }

    public static void unzipProjectFile(String destPath, InputStream zipInStream) throws IOException {
        ZipInputStream zipIn = null;
        FileOutputStream out = null;
        byte[] buffer = new byte[1024];
        try {
            zipIn = new ZipInputStream(zipInStream);
            ZipEntry entry = zipIn.getNextEntry();
            while (entry != null) {
                if (!entry.isDirectory()) { // ignore directory
                    try {
                        File file = null;
                        if (entry.getName().endsWith(UtilityConstant.SYS_PRJ_EXTENSION)) {
                            file = new File(destPath + entry.getName());

                            File dir = file.getParentFile();
                            if (!dir.exists()) {
                                dir.mkdirs();
                            }
                            file.createNewFile();
                            out = new FileOutputStream(file);
                            int len = 0;
                            while ((len = zipIn.read(buffer)) > 0) {
                                out.write(buffer, 0, len);
                            }
                        }
                    } finally {
                        IOUtils.closeQuietly(out);
                    }
                }
                entry = zipIn.getNextEntry();
            }
        } finally {
            IOUtils.closeQuietly(zipIn);
        }
    }

}

package com.carrier.ahu.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.report.SPaneldXtDoor;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.po.model.Section;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.ahu.AhuLayoutUtils;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.SysConstants;
import com.google.gson.Gson;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;

import static com.carrier.ahu.constant.CommonConstant.SYS_BLANK;
import static com.carrier.ahu.constant.CommonConstant.SYS_NOT_NUM;
import static com.carrier.ahu.constant.CommonConstant.SYS_PUNCTUATION_HYPHEN;
import static com.carrier.ahu.constant.UtilityConstant.COMMON_KEY_DOOR_DIMENSION;

public class ValueFormatUtil {
	protected static Logger logger = LoggerFactory.getLogger(ValueFormatUtil.class);

	public static void main(String[] args) {
		String ss = "meta.section.electrodeHumidifier.WInWetBulbT/meta.section.electrodeHumidifier.WInRelativeT";
		if (ss.contains(UtilityConstant.SYS_PUNCTUATION_SLASH)) {
			StringBuffer valueJoin = new StringBuffer();
			String[] s = ss.split(UtilityConstant.SYS_PUNCTUATION_SLASH);
			for (String k : s) {
				valueJoin.append(k).append(UtilityConstant.SYS_PUNCTUATION_SLASH);
			}
			System.out.println(valueJoin.substring(0, valueJoin.length() - 1));
		}
		System.out.println("safas".charAt(0));

		System.out.println(getSectionKeyByParaKey("meta.ahu.electrodeHumidifier.WInWetBulbT"));
	}

	/**
	 * 检查机组编号<br>
	 * 范围：001 - 999<br>
	 * 长度：3<br>
	 * 
	 * @param unitNO
	 * @return
	 */
	public static boolean isUnitNoLegal(String unitNO) {
		if (StringUtils.isBlank(unitNO)) {
			return false;
		}
		if (unitNO.length() != 3) {
			return false;
		}
		if (unitNO.equals("000")) {
			return false;
		}
		try {
			int no = Integer.parseInt(unitNO);
			if (no < 1) {
				return false;
			}
			if (no > 999) {
				return false;
			}
			return true;
		} catch (NumberFormatException e) {
			logger.error(MessageFormat.format("检查机组编号异常[unitNO:{0}]", unitNO), e);
		}
		return false;
	}

	/**
	 * 检查机组图纸编号<br>
	 * 范围：1 - 999<br>
	 * 长度：1< length <3 <br>
	 * <br>
	 * 
	 * @param drawingNO
	 * @return
	 */
	public static boolean isDrawingNoLegal(String drawingNO) {
		if (StringUtils.isBlank(drawingNO)) {
			return false;
		}
		if (drawingNO.length() > 3) {
			return false;
		}
		try {
			Integer.parseInt(drawingNO);
			return true;
		} catch (NumberFormatException e) {
			logger.error(MessageFormat.format("检查机组图纸编号异常[drawingNO:{0}]", drawingNO), e);
		}
		return false;
	}

	/**
	 * 格式化机组编号
	 * 
	 * @param unitNO
	 * @return
	 */
	public static String formatUnitNO(String unitNO) {
		try {
			return transNumberic2String(Integer.parseInt(unitNO), 3);
		} catch (NumberFormatException e) {
			logger.error(MessageFormat.format("格式化机组编号异常[NO:{0}]", unitNO), e);
			return null;
		}
	}

	/**
	 * 获取可用机组编号
	 *
	 * @param unitNO
	 * @return
	 */
	public static String getUsefulUnitNO(List<String> unitNOList, int inputNO) {
		if (EmptyUtil.isEmpty(unitNOList)) {
			return "001";
		}
		if (inputNO > 0 && inputNO < 1000) {
			String cs = transNumberic2String(inputNO, 3);
			if (!unitNOList.contains(cs)) {
				return cs;
			}
		}
		for (int i = 1; i < 1000; i++) {
			String cs = transNumberic2String(i, 3);
			if (!unitNOList.contains(cs)) {
				return cs;
			}
		}
		return "001";
	}
	
	/**
	 * 模板导入时，获取可用机组编号
	 * @param unitNOList
	 * @param inputNO
	 * @return
	 */
	public static String getUsefulUnitNOWhenImport(List<String> unitNOList, int inputNO) {
		if (EmptyUtil.isEmpty(unitNOList) && inputNO == 1) {
			return "001";
		}else {
			unitNOList = new ArrayList<String>();
		}
		if (inputNO > 0 && inputNO < 1000) {
			String cs = transNumberic2String(inputNO, 3);
			if (!unitNOList.contains(cs)) {
				return cs;
			}
		}
		for (int i = 1; i < 1000; i++) {
			String cs = transNumberic2String(i, 3);
			if (!unitNOList.contains(cs)) {
				return cs;
			}
		}
		return "001";
	}

	/**
	 * 将整形数字转化为制定长度的字符串
	 * 
	 * @param number
	 *            整数
	 * @param strLength
	 *            指定长度
	 * @return
	 */
	public static String transNumberic2String(int number, int strLength) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < strLength; i++) {
			sb.append("0");
		}
		sb.append(String.valueOf(number));
		String s = sb.toString();
		return s.substring(s.length() - strLength);
	}

    public static String transDouble2String(double d) {
        String s = SYS_BLANK + d;
        int index = s.indexOf(UtilityConstant.SYS_PUNCTUATION_DOT);
        if (index < 0) {
            return s;
        } else {
            s = s + "00";
            s = s.substring(0, index + 3);
            return s;
        }
    }

    public static String transObject2String(Object obj) {
        if (obj instanceof Double) {
            return SYS_BLANK + (double) obj;
        }
        return String.valueOf(obj);
    }

	/**
	 * 格式化metaJson数据值为String类型
	 * 
	 * @param metaJsonValue
	 * @return
	 */
	public static String formatMetaJsonValue2String(Object metaJsonValue) {
		if (EmptyUtil.isEmpty(metaJsonValue)) {
			return SYS_BLANK;
		}
		return String.valueOf(metaJsonValue);
	}

	/**
	 * 渲染报告数据
	 * 
	 * @param contents
	 *            报告模版
	 * @param project
	 *            报告内项目数据
	 * @param unit
	 *            报告内AHU数据
	 * @param allMap
	 *            报告内属性数据
	 * @return
	 * @throws IOException
	 */
	public static String[][] transReport(String[][] contents, Project project, Unit unit, Map<String, String> allMap,
			LanguageEnum language, UnitSystemEnum unitType) throws IOException {
	    PDFUtils.checkTableContents(contents);
		String[][] returnContents = new String[contents.length][contents[0].length];
		for (int i = 0; i < contents.length; i++) {
			for (int j = 0; j < contents[i].length; j++) {
				String key = contents[i][j];
				if (EmptyUtil.isEmpty(key)) {
					returnContents[i][j] = key;
					continue;
				}
				// 将Key用多语言过滤
				key = AHUContext.getIntlString(key);System.out.println("=============key1============="+ key);

				// 针对带有 / 的 key ，进行属性拆分解析
				if (key.contains(UtilityConstant.SYS_PUNCTUATION_SLASH)) {
					StringBuffer unitJoin = new StringBuffer();
					StringBuffer valueJoin = new StringBuffer();
					String[] s = key.split(UtilityConstant.SYS_PUNCTUATION_SLASH);
					for (String k : s) {
						String value = getValue(k, project, unit, allMap);
						value = rebuildNsPriceForNA(k,value);
						if (key.startsWith(UtilityConstant.METASEXON_META_PREFIX)) {
							String[] vu = AhuSectionMetas.getInstance().getValueByUnit(value, k, unitType);
							String v = vu[0];
							String u = vu[1];
							if (EmptyUtil.isNotEmpty(u)) {
								unitJoin.append(UtilityConstant.SYS_PUNCTUATION_SLASH).append(u);
							}
							valueJoin.append(UtilityConstant.SYS_PUNCTUATION_SLASH).append(
									AhuSectionMetas.getInstance().getValueByUnit(v, k,
									getSectionKeyByParaKey(k), language));
						} else {
						    value = AHUContext.getIntlString(value); // handle i18n keys
							valueJoin.append(UtilityConstant.SYS_PUNCTUATION_SLASH).append(value);
						}
					}
					if (key.startsWith(UtilityConstant.METASEXON_META_PREFIX)) {
						if (EmptyUtil.isNotEmpty(unitJoin.toString())) {
							returnContents[i][j - 1] = unitJoin.substring(1);
						}
					}
					returnContents[i][j] = valueJoin.substring(1);
				} else {
					if (key.startsWith(UtilityConstant.METASEXON_META_PREFIX)) {
                        String suffix = getSuffixFromKey(key); // for the meta as label
                        if (!StringUtils.isEmpty(suffix)) {
                            key = key.replaceAll(suffix, StringUtils.EMPTY);
                        }

						String value = getValue(key, project, unit, allMap);
						String[] vu = AhuSectionMetas.getInstance().getValueByUnit(value, key, unitType);
						String v = vu[0];
						String u = vu[1];
						if (EmptyUtil.isNotEmpty(u) && !CharUtil.isChinese(returnContents[i][j - 1]) && !returnContents[i][j - 1].contains(":")) {//替换值前面cell 的单位：中文不替换、带冒号的英文不替换，原因为防止替换属性名
						    // comment this not to change the label
							returnContents[i][j - 1] = u;
							//报告Pa KG 单位的1.3key 统一格式化为整数
							if(u.equals("Pa") || u.equals("KG")){
								v = String.valueOf(BaseDataUtil.doubleConversionInteger(BaseDataUtil.trans2Double(v)));
							}
						}
						returnContents[i][j] = AhuSectionMetas.getInstance().getValueByUnit(v, key,
								getSectionKeyByParaKey(key), language) + suffix;
						returnContents[i][j] = rebuildNsPriceForNA(key,returnContents[i][j]);
						continue;
					}else if(key.startsWith(UtilityConstant.METANS_NS_PREFIX)){
						String value = getValue(key, project, unit, allMap);
						String[] vu = AhuSectionMetas.getInstance().getValueByUnit(value, key, unitType);
						String v = vu[0];
						returnContents[i][j] = AhuSectionMetas.getInstance().getValueByUnit(v, key,
								getSectionKeyByParaKey(key), language);
						returnContents[i][j] = rebuildNsPriceForNA(key,returnContents[i][j]);
						continue;
					}
					returnContents[i][j] = getValue(key, project, unit, allMap);System.out.println("=============key2============="+ key);
					returnContents[i][j] = rebuildNsPriceForNA(key,returnContents[i][j]);

				}
			}
		}
		return returnContents;
	}

	/**
	 * 1 机组复制后，非标价格目前清空，针对非标价格为空的，在打印非标清单是显示N/A
	 * 2 机组非标价格输入默认为空，如直接保存，非标价格为空时，显示N/A
	 * @param key
	 * @param val
	 * @return
	 */
	private static String rebuildNsPriceForNA(String key, String val) {
		if ((key.startsWith("ns")||key.startsWith("NS")) && (key.endsWith("Price")||key.endsWith("price"))
				&& EmptyUtil.isEmpty(val)) { // is material property
			return SYS_NOT_NUM;
		}
		return val;
	}

	private static String getSuffixFromKey(String key) {
        if (key.endsWith(SysConstants.COLON.trim())) {
            return SysConstants.COLON.trim();
        }
        return StringUtils.EMPTY;
    }

    /**
	 * 渲染报告数据
	 * 
	 * @param contents
	 *            报告模版
	 * @param project
	 *            报告内项目数据
	 * @param unit
	 *            报告内AHU数据
	 * @param allMap
	 *            报告内属性数据
	 * @return
	 * @throws IOException
	 */
	public static String transMetaValue(String metaKey,String metaValue,Project project, Unit unit,LanguageEnum language, UnitSystemEnum unitType) throws IOException {
			String retVal = metaValue;
			String key = metaKey;
			if (EmptyUtil.isEmpty(key)) {
				return retVal;
			}
			// 将Key用多语言过滤
			key =  AHUContext.getIntlString(key);
			// 针对带有 / 的 key ，进行属性拆分解析
			if (key.contains(UtilityConstant.SYS_PUNCTUATION_SLASH)) {
				StringBuffer unitJoin = new StringBuffer();
				StringBuffer valueJoin = new StringBuffer();
				String[] s = key.split(UtilityConstant.SYS_PUNCTUATION_SLASH);
				for (String k : s) {
					String value = metaValue;
					if (key.startsWith(UtilityConstant.METASEXON_META_PREFIX)) {
						String[] vu = AhuSectionMetas.getInstance().getValueByUnit(value, key, unitType);
						String v = vu[0];
						String u = vu[1];
						if (EmptyUtil.isNotEmpty(u)) {
							unitJoin.append(UtilityConstant.SYS_PUNCTUATION_SLASH).append(u);
						}
						valueJoin.append(UtilityConstant.SYS_PUNCTUATION_SLASH).append(v);
					} else {
						valueJoin.append(UtilityConstant.SYS_PUNCTUATION_SLASH).append(value);
					}
				}
				retVal = valueJoin.substring(1);
			} else {
				if (key.startsWith(UtilityConstant.METASEXON_META_PREFIX)) {
					String value = retVal;
					String[] vu = AhuSectionMetas.getInstance().getValueByUnit(value, key, unitType);
					String v = vu[0];
					retVal = AhuSectionMetas.getInstance().getValueByUnit(v, key,getSectionKeyByParaKey(key), language);
				}
			}
			return retVal;
	}
	/**
	 * 渲染报告数据 公制（M）或者英制(B) 单位符号
	 * @param contents
	 * @param project
	 * @param unit
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @return
	 * @throws IOException
	 */
	public static String[][] transReportUnitType(String[][] contents,LanguageEnum language, UnitSystemEnum unitType) throws IOException {
	    PDFUtils.checkTableContents(contents);
		String[][] returnContents = new String[contents.length][contents[0].length];
		for (int i = 0; i < contents.length; i++) {
			for (int j = 0; j < contents[i].length; j++) {
				String key = contents[i][j];
				if (EmptyUtil.isEmpty(key)) {
					returnContents[i][j] = key;
					continue;
				}
				// 将Key用多语言过滤
				key =  AHUContext.getIntlString(key);
				if (key.startsWith(UtilityConstant.METASEXON_META_PREFIX)) {
					String value =  key;
					String[] vu = AhuSectionMetas.getInstance().getValueByUnit(value, key, unitType);
					String u = vu[1];
					if (EmptyUtil.isNotEmpty(u)) {//替换值前面cell 的单位
						returnContents[i][j - 1] = u;
					}
				}
				returnContents[i][j] = key;
			}
		}
		return returnContents;
	}

	/**
	 * 识别不同类型的报告数据
	 * 
	 * @param key
	 * @param project
	 * @param unit
	 * @param allMap
	 * @return
	 */
	private static String getValue(String key, Project project, Unit unit, Map<String, String> allMap) {
		if (key.startsWith("AHU.")) {
			String keyValue = getUnitTableParam(key, unit);
			return keyValue;
		} else if (key.startsWith("PROJECT.")) {
			String keyValue = getProjectTableParam(key, project);
			return keyValue;
        } else if (key.startsWith(UtilityConstant.METASEXON_META_PREFIX)
                || key.startsWith(UtilityConstant.METANS_NS_PREFIX)
                || key.startsWith(UtilityConstant.METANS_INFO_PREFIX)) {
            Object keyValue = allMap.get(key);
            return ValueFormatUtil.formatMetaJsonValue2String(keyValue);
        }  else if (key.startsWith(UtilityConstant.COMMON_KEY_PREFIX)) {//所有段通用属性
            return getCommonValue(key,unit);
        } else if (key.equals("SystemVersion")) {
			return AHUContext.getAhuVersion();
		} else {
			if (EmptyUtil.isNotEmpty(key)) {
				if (!CharUtil.isChinese(key)) {
					logger.warn("整合报表数据-工程格式-时，key命名不合规范，Key：" + key);
				}
			}
			return key;
		}
	}

	/**
	 * 获取所有功能段通用key对应的值
	 * @param key
	 * @param unit
	 * @return
	 */
	public static String getCommonValue(String key, Unit unit) {
		String retStr = SYS_PUNCTUATION_HYPHEN;
		//开门尺寸
		if(key.startsWith(COMMON_KEY_DOOR_DIMENSION)){
			try {
				/*boolgroup:=true;
				for i:=0 to OrderParts.Count - 1 do
					begin
				VarTpart := tPart(OrderParts.Objects[i]);
				If (VarTpart.ID<>lpart.ID) and (VarTpart.group=lpart.group)  then
						begin
				boolgroup:=true;
				break;
				end
				else
							boolgroup:=false;
							end;
							if (not boolgroup) and ((lpart.PartProperty.sectionL<=6) or ((lpart.types=12) and (lpart.PartProperty.sectionL<=12))) then
							XTDoordimension:=inttostr(DoorH)+'*'+inttostr(DoorW-100)
			  else
							XTDoordimension:=inttostr(DoorH)+'*'+inttostr(DoorW);*/
				//TODO boolgroup 合并段的情况开门尺寸需要特殊处理宽度-100
				String partTypeNum = SectionTypeEnum.getSectionTypeFromId(key.split("_")[1]).getCodeNum();
				SPaneldXtDoor sPaneldXtDoor = AhuMetadata.findOne(SPaneldXtDoor.class, unit.getSeries(),String.valueOf(Integer.parseInt(partTypeNum)));
				retStr = sPaneldXtDoor.getDoorW()+"*"+sPaneldXtDoor.getDoorH();
			} catch (Exception e) {
			}
		}
		return retStr;
	}
	/**
	 * 根据键名称获取项目的表属性值
	 * 
	 * @param key
	 * @param unit
	 * @return
	 */
	public static String getProjectTableParam(String key, Project project) {
		String tableParam = key.substring(key.indexOf('.') + 1);
		if ("pid".equalsIgnoreCase(tableParam)) {
			return null == project.getPid() ? SYS_BLANK : project.getPid();
		} else if ("no".equalsIgnoreCase(tableParam)) {
			return null == project.getNo() ? SYS_BLANK : project.getNo();
		} else if ("name".equalsIgnoreCase(tableParam)) {
			return null == project.getName() ? SYS_BLANK : project.getName();
		} else if ("address".equalsIgnoreCase(tableParam)) {
			return null == project.getAddress() ? SYS_BLANK : project.getAddress();
		} else if ("contract".equalsIgnoreCase(tableParam)) {
			return null == project.getContract() ? SYS_BLANK : project.getContract();
		} else if ("saler".equalsIgnoreCase(tableParam)) {
			return null == project.getSaler() ? SYS_BLANK : project.getSaler();
		} else if ("enquiryNo".equalsIgnoreCase(tableParam)) {
			return null == project.getEnquiryNo() ? SYS_BLANK : project.getEnquiryNo();
		} else if ("projectDate".equalsIgnoreCase(tableParam)) {
			return null == project.getProjectDate() ? SYS_BLANK : project.getProjectDate();
		} else if ("priceBase".equalsIgnoreCase(tableParam)) {
			return null == project.getPriceBase() ? SYS_BLANK : project.getPriceBase();
		} else if ("customerName".equalsIgnoreCase(tableParam)) {
			return null == project.getCustomerName() ? SYS_BLANK : project.getCustomerName();
		} else {
			logger.warn("获取项目的表属性值-属性名未知：" + tableParam);
			return SYS_BLANK;
		}
	}

	/**
	 * 根据键名称获取机组的表属性值
	 * 
	 * @param key
	 * @param unit
	 * @return
	 */
	public static String getUnitTableParam(String key, Unit unit) {
		String tableParam = key.substring(key.indexOf('.') + 1);
		if ("unitid".equalsIgnoreCase(tableParam)) {
			return null == unit.getUnitid() ? SYS_BLANK : unit.getUnitid();
		} else if ("unitNo".equalsIgnoreCase(tableParam)) {
			return null == unit.getUnitNo() ? SYS_BLANK : unit.getUnitNo();
		} else if ("drawingNo".equalsIgnoreCase(tableParam)) {
			return null == unit.getDrawingNo() ? SYS_BLANK : unit.getDrawingNo();
		} else if ("series".equalsIgnoreCase(tableParam)) {
			return null == unit.getSeries() ? SYS_BLANK : unit.getSeries();
		} else if ("product".equalsIgnoreCase(tableParam)) {
			return null == unit.getProduct() ? SYS_BLANK : unit.getProduct();
		} else if ("mount".equalsIgnoreCase(tableParam)) {
			return null == unit.getMount() ? SYS_BLANK : String.valueOf(unit.getMount());
		} else if ("name".equalsIgnoreCase(tableParam)) {
			return null == unit.getName() ? SYS_BLANK : unit.getName();
		} else if ("customerName".equalsIgnoreCase(tableParam)) {
			return null == unit.getCustomerName() ? SYS_BLANK : unit.getCustomerName();
		} else if ("groupCode".equalsIgnoreCase(tableParam)) {
			return null == unit.getGroupCode() ? SYS_BLANK : unit.getGroupCode();
		} else if ("weight".equalsIgnoreCase(tableParam)) {
			return null == unit.getWeight() ? SYS_BLANK : SYS_BLANK+BaseDataUtil.decimalConvert(unit.getWeight(), 1);
		} else {
			logger.warn("获取机组的表属性值-属性名未知：" + tableParam);
			return SYS_BLANK;
		}
	}

	/**
	 * 获得AHU以及对应的段的所有metadata
	 * 
	 * @param unit
	 * @param partList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Map<String, String>> getAllUnitMetaJsonData(Unit unit, List<PartPO> partList) {
		Map<String, String> unitMap = new HashMap<>();
		Map<String, Map<String, String>> allMetaMap = new HashMap<String, Map<String, String>>();
		try {
			String ahuMetaJson = unit.getMetaJson();
			unitMap.putAll(JSON.parseObject(ahuMetaJson, HashMap.class));
			allMetaMap.put(UnitConverter.UNIT, unitMap);
		} catch (Exception e) {
			logger.warn(MessageFormat.format("转化AHU的metaJson时异常", new Object[] {}), e);
		}
		for (PartPO partPo : partList) {
			Part part = partPo.getCurrentPart();
			if (part.getUnitid().startsWith("A")) {
				try {
					Map<String, String> sectionMap = new HashMap<>();
					String sectionMetaJson = part.getMetaJson();
					sectionMap.putAll(unitMap);//优先加载机组属性
					sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));//段的属性覆盖机组属性 例如：风向
					sectionMap.put(
							partPo.getCurrentPart().getSectionKey().concat(UtilityConstant.SYS_PUNCTUATION_DOT).concat(SysConstants.PART_PREVIOUS),
							String.valueOf(partPo.getPrePart().getSectionKey()));
					allMetaMap.put(UnitConverter.genUnitKey(unit, part), sectionMap);
				} catch (Exception e) {
					logger.warn(MessageFormat.format("转化段metaJson时异常, unitid:{0}-->partKey:{1}",
							new Object[] { unit.getUnitid(), part.getSectionKey() }), e);
					continue;
				}
			} else {
				logger.warn(MessageFormat.format("转化段metaJson时，unitid不匹配>>unitid:{0}--partUnitid:{1}",
						new Object[] { unit.getUnitid(), part.getUnitid() }));
				continue;
			}
		}
		return allMetaMap;
	}
	/**
	 * 获得AHU以及对应的段的所有metadata放到一个map:注意重复段回覆盖，不建议使用
	 * @param unit
	 * @param partList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, String> getAllUnitMetaJsonData2OneMap(Unit unit, List<PartPO> partList) {
		Map<String, String> allMap = new HashMap<>();
		try {
			String ahuMetaJson = unit.getMetaJson();
			allMap.putAll(JSON.parseObject(ahuMetaJson, HashMap.class));
		} catch (Exception e) {
			logger.warn(MessageFormat.format("转化AHU的metaJson时异常", new Object[] {}), e);
		}
		for (PartPO partPo : partList) {
			Part part = partPo.getCurrentPart();
			if (part.getUnitid().startsWith("A")) {
				try {
					Map<String, String> sectionMap = new HashMap<>();
					String sectionMetaJson = part.getMetaJson();
					sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));//段的属性覆盖机组属性 例如：风向
					sectionMap.put(
							partPo.getCurrentPart().getSectionKey().concat(UtilityConstant.SYS_PUNCTUATION_DOT).concat(SysConstants.PART_PREVIOUS),
							String.valueOf(partPo.getPrePart().getSectionKey()));

					//重构json防止风机里面出现盘管数据产生铭牌冷热量错误问题。
					sectionMap = rebuildPartSectionMap(part,sectionMap);

					allMap.putAll(sectionMap);
				} catch (Exception e) {
					logger.warn(MessageFormat.format("转化段metaJson时异常, unitid:{0}-->partKey:{1}",
							new Object[] { unit.getUnitid(), part.getSectionKey() }), e);
					continue;
				}
			} else {
				logger.warn(MessageFormat.format("转化段metaJson时，unitid不匹配>>unitid:{0}--partUnitid:{1}",
						new Object[] { unit.getUnitid(), part.getUnitid() }));
				continue;
			}
		}
		return allMap;
	}

	private static Map<String, String> rebuildPartSectionMap(Part part, Map<String, String> sectionMap) {
		String sectionKey = part.getSectionKey();
		if(SectionTypeEnum.TYPE_FAN.getId().equals(sectionKey)){
			String coolCoilsectionKey = SectionTypeEnum.TYPE_COLD.getId().replace(CommonConstant.METAHU_AHU_NAME, CommonConstant.METASEXON_SECTION);
			Map<String, String> retSectionMap = new HashedMap<>();
			Iterator<String> ite = sectionMap.keySet().iterator();
			while (ite.hasNext()){
				String key = ite.next();
				String val = String.valueOf(sectionMap.get(key));
				if(key.contains(coolCoilsectionKey)){
					//风机段忽略冷水内容
				}else{
					retSectionMap.put(key,val);
				}
			}
			return retSectionMap;
		}
		return sectionMap;
	}

	/**
	 * 转化数据库数据为运算参数bean
	 * 
	 *
	 * @param project
	 * @param unit
	 * @param parts
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static AhuParam transDBData2AhuParam(Project project, Unit unit, List<PartPO> parts, Partition partition) {
		AhuParam ahuParam = new AhuParam();
		if (EmptyUtil.isNotEmpty(unit)) {
			ahuParam.setCost(unit.getCost());
			ahuParam.setCustomerName(unit.getCustomerName());
			ahuParam.setDrawing(unit.getDrawing());
			ahuParam.setDrawingNo(unit.getDrawingNo());
			ahuParam.setGroupId(unit.getGroupId());
			ahuParam.setLb(unit.getLb());
			ahuParam.setModifiedno(unit.getModifiedno());
			ahuParam.setMount(unit.getMount());
			ahuParam.setName(unit.getName());
			ahuParam.setNsprice(unit.getNsprice());
			ahuParam.setPaneltype(unit.getPaneltype());
			ahuParam.setPid(unit.getPid());
			ahuParam.setPrice(unit.getPrice());
			ahuParam.setProduct(unit.getProduct());
			ahuParam.setSeries(unit.getSeries());
			ahuParam.setPanelSeries(unit.getPanelSeries());
			ahuParam.setUnitid(unit.getUnitid());
			ahuParam.setUnitNo(unit.getUnitNo());
			ahuParam.setWeight(unit.getWeight());
			ahuParam.setLayout(AhuLayoutUtils.parse(unit.getLayoutJson()));
			if (StringUtils.isNotBlank(unit.getMetaJson())) {
				Gson gson = new Gson();
				Map<String, Object> map = (Map<String, Object>) gson.fromJson(unit.getMetaJson(), Map.class);
				ahuParam.setParams(map);
			}
			//项目名称
			ahuParam.setProjectName(project.getName());
		}

		if (EmptyUtil.isNotEmpty(partition)) {
			ahuParam.setPartition(partition);
			ahuParam.setDoubleLayer(AhuPartitionUtils.isDoubleLayer(unit));
			ahuParam.setTopPartitions(AhuPartitionUtils.getTopLayerPartitions(unit, partition));
			ahuParam.setBottomPartitions(AhuPartitionUtils.getBottomLayerPartitions(unit, partition));
		}

		List<PartParam> partParams = new ArrayList<PartParam>();
		for (PartPO partPO : parts) {
			Part section = partPO.getCurrentPart();
			PartParam partParam = new PartParam();
			partParam.setAvdirection(section.getAvdirection());
			partParam.setCost(section.getCost());
			partParam.setGroupi(section.getGroupi());
			partParam.setKey(section.getSectionKey());
			partParam.setOrders(section.getOrders());
			partParam.setPartid(section.getPartid());
			partParam.setPic(section.getPic());
			partParam.setPid(unit.getPid());
			partParam.setPosition(section.getPosition());
			partParam.setSetup(section.getSetup());
			partParam.setTypes(section.getTypes() == null ? MetaCodeGen.getSectionType(section.getSectionKey())
					: section.getTypes());
			partParam.setUnitid(unit.getUnitid());
			Map<String, Object> map = (Map) JSONObject.parseObject(section.getMetaJson());
			partParam.setParams(map);
			partParams.add(partParam);
		}
		ahuParam.setPartParams(partParams);
		return ahuParam;
	}

	/**
	 * 获取方向简要标识
	 * 
	 * @param direction
	 * @return
	 */
	public static String getSimpleDirection(String direction) {
		if (StringUtils.isBlank(direction)) {
			return SYS_BLANK;
		}
		if (UtilityConstant.SYS_STRING_LEFT.equalsIgnoreCase(direction)) {
			return "L";
		}
		if (UtilityConstant.SYS_STRING_BILATERAL.equalsIgnoreCase(direction)) {
			return "L";
		}
		if (UtilityConstant.SYS_STRING_RIGHT.equalsIgnoreCase(direction)) {
			return "R";
		}
		if (UtilityConstant.SYS_ASSERT_TOP.equalsIgnoreCase(direction)) {
			return "T";
		}
		if (UtilityConstant.SYS_ASSERT_BOTTOM.equalsIgnoreCase(direction)) {
			return "B";
		}
		if (UtilityConstant.SYS_STRING_BOTH.equalsIgnoreCase(direction)) {
			return "B";
		}
		if (UtilityConstant.SYS_STRING_FRONT.equalsIgnoreCase(direction)) {
			return "F";
		}
		return SYS_BLANK;
	}

	/**
	 * 获取方向简要标识
	 * 
	 * @param direction
	 * @return
	 */
	public static int getSimpleDirectionInteger(String direction) {
		if (StringUtils.isBlank(direction)) {
			return -1;
		}
		if (UtilityConstant.SYS_STRING_LEFT.equalsIgnoreCase(direction)) {
			return 1;
		}
		if (UtilityConstant.SYS_STRING_RIGHT.equalsIgnoreCase(direction)) {
			return 2;
		}
		if (UtilityConstant.SYS_STRING_BOTH.equalsIgnoreCase(direction)) {
			return 3;
		}
		if (UtilityConstant.SYS_STRING_FRONT.equalsIgnoreCase(direction)) {
			return 4;
		}
		return -1;
	}

	/**
	 * 获取字符串首字母大写，特殊情况返回特殊值
	 * 
	 * @param fullStr
	 * @return
	 */
	public static String getShort(String fullStr) {
		if (StringUtils.isBlank(fullStr)) {
			return SYS_BLANK;
		}
		if ("wheelHR".equals(fullStr)) {
			return "R";
		}
		if ("plateHR".equals(fullStr)) {
			return "T";
		}
		return String.valueOf(fullStr.charAt(0));
	}

	/**
	 * 根据描述真假获取对应数值
	 * 
	 * <pre>
	 * ValueFormatUtil.getTorFInteger(UtilityConstant.SYS_ASSERT_TRUE)         = 1
	 * ValueFormatUtil.getTorFInteger(UtilityConstant.SYS_ASSERT_TRUE)        = 1
	 * ValueFormatUtil.getTorFInteger("T")             = 1
	 * ValueFormatUtil.getTorFInteger("f")              = 0
	 * ValueFormatUtil.getTorFInteger(UtilityConstant.SYS_ASSERT_FALSE)     = 0
	 * ValueFormatUtil.getTorFInteger(UtilityConstant.SYS_ASSERT_FALSE)        = 0
	 * ValueFormatUtil.getTorFInteger(UtilityConstant.SYS_BLANK)                = -1
	 * ValueFormatUtil.getTorFInteger("dfs")                = -1
	 * </pre>
	 * 
	 * @param bool
	 * @return
	 */
	public static int getTorFInteger(String bool) {
		if (UtilityConstant.SYS_ASSERT_TRUE.equalsIgnoreCase(bool)) {
			return 1;
		} else if ("t".equalsIgnoreCase(bool)) {
			return 1;
		} else if ("f".equalsIgnoreCase(bool)) {
			return 0;
		} else if (UtilityConstant.SYS_ASSERT_FALSE.equalsIgnoreCase(bool)) {
			return 0;
		}
		return -1;
	}

	public static String getSectionKeyByParaKey(String paraKey) {
		if (paraKey.startsWith(UtilityConstant.METAHU_PREFIX) || paraKey.startsWith(UtilityConstant.METAHU_NS_PREFIX)) {
			return UtilityConstant.SYS_UNIT_SPEC_SECTION_AHU;
		}
		if (paraKey.startsWith(UtilityConstant.METASEXON_PREFIX)) {
			int lindex = paraKey.lastIndexOf(UtilityConstant.SYS_PUNCTUATION_DOT);
			paraKey = paraKey.substring(0, lindex);
			return paraKey.replace("meta.section", UtilityConstant.SYS_UNIT_SPEC_SECTION_AHU);
		}
		if (paraKey.startsWith(UtilityConstant.METANS_NS_SECTION)) {
			int lindex = paraKey.lastIndexOf(UtilityConstant.SYS_PUNCTUATION_DOT);
			paraKey = paraKey.substring(0, lindex);
			return paraKey.replace("ns.section", UtilityConstant.SYS_UNIT_SPEC_SECTION_AHU);
		}
		return paraKey;
	}

}

package com.carrier.ahu.wpd;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Braden Zhou on 2019/07/12.
 */
@Data
@EqualsAndHashCode(callSuper = false)
class CoilTubeDetail extends TubeDetail {

    private int cir_no;
    private int t_no;
    private int row;
    private double cir_len;

    public CoilTubeDetail() {
        super();
        this.cir_no = 0;
        this.t_no = 0;
        this.row = 0;
        this.cir_len = 0;
    }

}

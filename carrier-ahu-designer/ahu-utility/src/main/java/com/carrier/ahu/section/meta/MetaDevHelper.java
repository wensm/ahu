package com.carrier.ahu.section.meta;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

import com.carrier.ahu.po.meta.SectionMeta;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.po.meta.Meta;
import com.carrier.ahu.po.meta.MetaParameter;

import lombok.extern.slf4j.Slf4j;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

/**
 * Helper类，用来转换前期的metadata（ahu + 5sections）到excel格式，这样之后的元数据统一由excel定义。
 * 参考ahu.section.parameters1.1.xls 这个类属于一次性使用，如需需要，默认要关闭assis属性和default属性的创建。
 *
 * @author JL
 * @see MetaCodeGen#appendDefaultMeta(Meta, String)
 * @see MetaCodeGen#getAssistantParameter()
 */
@Slf4j
public class MetaDevHelper {
	private static Comparator<String> stringComparator = new Comparator<String>() {

		@Override
		public int compare(String o1, String o2) {
			return o1.compareToIgnoreCase(o2);
		}
	};

	public static final void print5MetadataAsCsv() {
//		SectionTypeEnum[] sectionTypes = new SectionTypeEnum[] { SectionTypeEnum.TYPE_FAN, SectionTypeEnum.TYPE_MIX,
//				SectionTypeEnum.TYPE_COMPOSITE, SectionTypeEnum.TYPE_AHU, SectionTypeEnum.TYPE_COLD,
//				SectionTypeEnum.TYPE_SINGLE };
//		for (int i = 0; i < sectionTypes.length; i++) {
//			SectionTypeEnum st = sectionTypes[i];
//			if (st.equals(SectionTypeEnum.TYPE_AHU)) {
//
//				print(st, AhuUtilMeta.getAhuMeta());
//			}
//			if (st.equals(SectionTypeEnum.TYPE_SINGLE)) {
//				print(st, SimpleFilterSectionMeta.getFilterSectionMeta());
//			}
//			if (st.equals(SectionTypeEnum.TYPE_MIX)) {
//				print(st, MixSectionMeta.getMixSectionMeta());
//			}
//			if (st.equals(SectionTypeEnum.TYPE_FAN)) {
//				print(st, FanSectionMeta.getFanSectionMeta());
//			}
//			if (st.equals(SectionTypeEnum.TYPE_COLD)) {
//				print(st, CoolingCoilFilterSectionMeta.getCoolingCoilSectionMeta());
//			}
//			if (st.equals(SectionTypeEnum.TYPE_COMPOSITE)) {
//				print(st, CombinedFilterSectionMeta.getCombinedFilterSectionMeta());
//			}
//		}
	}

	protected static void print(SectionTypeEnum st, Meta ahuMeta) {

		Map<String, MetaParameter> paraMap = ahuMeta.getParameters();
		List<String> keyList = new ArrayList<>();
		Iterator<String> it = paraMap.keySet().iterator();
		while (it.hasNext()) {
			keyList.add(it.next());
		}
		Collections.sort(keyList, stringComparator);
		it = keyList.iterator();
		StringBuffer buffer = new StringBuffer();
		buffer.append("key,name,groiup,rMaterial,rDirection,rPerformance,editable,global,valueSource,valueType,memo");
		while (it.hasNext()) {
			MetaParameter para = paraMap.get(it.next());
			String key = para.getKey();
			if (key.startsWith(AhuSectionMetas.KEY_ASSIS)) {
				continue;
			}
			buffer.append(UtilityConstant.SYS_PUNCTUATION_NEWLINE);
			buffer.append(para.getKey());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.getName());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.getGroup());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.isrMaterial());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.isrDirection());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.isrPerformance());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.isEditable());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.isUrEnable());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.isGlobal());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.getValueSource());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.getValueType());
			buffer.append(UtilityConstant.SYS_PUNCTUATION_COMMA);
			buffer.append(para.getMemo());
		}
		String content = buffer.toString();
		System.out.println(content);
		System.out.println("");
		System.out.println(st.getName() + "   " + st.getId());
		File file = new File("d:\\" + st.getName() + UtilityConstant.SYS_CSV_EXTENSION);
		try {
			FileUtils.writeStringToFile(file, content, Charset.forName(UtilityConstant.SYS_ENCODING_UTF8_UP));
		} catch (IOException e) {
			log.error("Error on writing file", e);
		}

	}

	public static void main1(String[] args) {
		print5MetadataAsCsv();
	}

	public static void main(String[] args) {
		// generateParameterTemplateCode();
		// generateImportTemplate();
		// generateCaseTemplate();
		generateIntlPropertiesFileForMeta();
	}

	@SuppressWarnings("unused")
	private static void generateCaseTemplate() {
		String template = "case 'ahu.steamCoil':\n" + "        if (standard) {\n"
				+ "          section = <SteamCoilContainer {...this.props} />\n" + "        } else {\n"
				+ "          section = <SteamCoilNSContainer {...this.props} />\n" + "        }\n" + "        break;";

		AhuSectionMetas asm = AhuSectionMetas.getInstance();
		List<SectionMeta> list = asm.getAhuAndSectionMetas();
		Iterator<SectionMeta> it = list.iterator();
		while (it.hasNext()) {
			SectionMeta meta = it.next();
			String key = meta.getMetaId();
			int index = key.lastIndexOf(UtilityConstant.SYS_PUNCTUATION_DOT);
			if (index < 0) {
				continue;
			}
			key = key.substring(index + 1);
			String str = template.replace(UtilityConstant.SYS_STRING_STEAMCOIL_1, key);
			key = key.substring(0, 1).toUpperCase() + key.substring(1);
			System.out.println(str.replace(UtilityConstant.SYS_STRING_STEAMCOIL, key));
		}
	}

	@SuppressWarnings("unused")
	private static void generateImportTemplate() {
		String template = "import SteamCoilContainer from '../section/steamCoil/SteamCoilContainer'\n"
				+ "import SteamCoilNSContainer from '../section/steamCoil/SteamCoilNSContainer'";
		AhuSectionMetas asm = AhuSectionMetas.getInstance();
		List<SectionMeta> list = asm.getAhuAndSectionMetas();
		Iterator<SectionMeta> it = list.iterator();
		while (it.hasNext()) {
			SectionMeta meta = it.next();
			String key = meta.getMetaId();
			int index = key.lastIndexOf(UtilityConstant.SYS_PUNCTUATION_DOT);
			if (index < 0) {
				continue;
			}
			key = key.substring(index + 1);
			String str = template.replace(UtilityConstant.SYS_STRING_STEAMCOIL_1, key);
			key = key.substring(0, 1).toUpperCase() + key.substring(1);
			System.out.println(str.replace(UtilityConstant.SYS_STRING_STEAMCOIL, key));
		}
	}

	@SuppressWarnings("unused")
	private static void generateParameterTemplateCode() {
		AhuSectionMetas asm = AhuSectionMetas.getInstance();
		List<SectionMeta> list = asm.getAhuAndSectionMetas();
		Iterator<SectionMeta> it = list.iterator();
		while (it.hasNext()) {
			SectionMeta meta = it.next();
			System.out.println(UtilityConstant.SYS_PUNCTUATION_NEWLINE + meta.getMetaId() + UtilityConstant.SYS_PUNCTUATION_NEWLINE);
			printSectionMeta(meta);
		}
	}

	private static void printSectionMeta(SectionMeta meta) {
		String template = "                    <div className=\"col-lg-3\">\n"
				+ "                        <Field id=\"KEY_SS\"/>\n" + "                    </div>";
		Map<String, MetaParameter> map = meta.getParameters();
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			if (key.startsWith(AhuSectionMetas.KEY_ASSIS) || key.startsWith(AhuSectionMetas.KEY_NS)
					|| key.endsWith(UtilityConstant.SYS_MAP_COMPLETED)) {
				continue;
			}
			System.out.println(template.replace("KEY_SS", key));
		}
	}

	public static void generateIntlPropertiesFileForMeta() {
		String path = "/tmp/ahuTemp/metaIntlJson";
		String cnPath = "/Users/liujianfeng/carrier/bakForWin/carrier/oversion/AHU_Designer6.4.0/Language/Chinese.xml";
		String enPath = "/Users/liujianfeng/carrier/bakForWin/carrier/oversion/AHU_Designer6.4.0/Language/English.xml";
		String prefix = "moon_intl_str_";

		Map<String, String> cnMap = new HashMap<>();
		Map<String, String> enMap = new HashMap<>();
		// Resolve Language xml File, and inject it into Json Map
		resolveXml(cnPath, cnMap);
		resolveXml(enPath, enMap);
		// Reduce on the two maps, and merge them with one single key
		Map<String, String> cnMap1 = new LinkedHashMap<>();
		Map<String, String> enMap1 = new LinkedHashMap<>();
		Map<String, String> cn2KeyMap = new LinkedHashMap<>();

		Iterator<String> it = cnMap.keySet().iterator();
		int i = 0;
		while (it.hasNext()) {
			String key = it.next();
			String cnValue = cnMap.get(key).trim();
			if (cn2KeyMap.containsKey(cnValue)) {
				continue;
			}
			String newKey = prefix + String.valueOf(1000 + i++);
			cnMap1.put(newKey, cnValue);
			if (enMap.containsKey(key)) {
				enMap1.put(newKey, enMap.get(key).trim());
			} else {
				enMap1.put(newKey, cnValue);
				System.out.println(String.format("%s not found", key));
			}
			cn2KeyMap.put(cnValue, newKey);
		}

		File file = new File(path);
		file.mkdirs();

		try {
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
			FileUtils.write(new File(path + "/cnProp.json"), gson.toJson(cnMap1));
			FileUtils.write(new File(path + "/enProp.json"), gson.toJson(enMap1));
			FileUtils.write(new File(path + "/cn2KeyProp.json"), gson.toJson(cn2KeyMap));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void resolveXml(String path, Map<String, String> map) {
		try {
			File inputFile = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName(UtilityConstant.SYS_MAP_ITEM);
			System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				// System.out.println("\nCurrent Element :" +
				// nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String itemName = eElement.getParentNode().getNodeName() + UtilityConstant.SYS_PUNCTUATION_DOT + eElement.getAttribute("ItemName");
					// System.out.println("Item Name : "
					// + eElement.getParentNode().getNodeName() + UtilityConstant.SYS_PUNCTUATION_DOT +
					// eElement.getAttribute("ItemName"));
					// System.out.println("Value : "
					// + eElement
					// .getAttribute(UtilityConstant.SYS_MAP_VALUE));
					map.put(itemName, eElement.getAttribute(UtilityConstant.SYS_MAP_VALUE));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

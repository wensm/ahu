package com.carrier.ahu.calculator.weight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

/**
 * 重量数据模型1 行：机组 列：key 根据机组和key，找到对应的值
 * 
 * @author liujianfeng
 *
 */
@Data
public class WeightModel1 {
	private String name;
	List<String> keyList = new ArrayList<>();
	Map<String, List<Double>> rowMap = new HashMap<>();
}

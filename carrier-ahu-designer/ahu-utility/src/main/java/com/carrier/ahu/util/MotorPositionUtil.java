package com.carrier.ahu.util;

import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_STARTSTYLE_ENBEDDED;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_SUPPLIER_ABB;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_SUPPLIER_DEFAULT;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_SUPPLIER_DONGYUAN;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_SUPPLIER_GE;
import static com.carrier.ahu.vo.SystemCalculateConstants.FAN_SUPPLIER_SIMENS;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.fan.FanCodeElectricSize;
import com.carrier.ahu.metadata.entity.fan.FanCodeElectricSizeBig;
import com.carrier.ahu.metadata.entity.fan.FanEmElectricSize;
import com.carrier.ahu.metadata.entity.fan.FanEmElectricSizeBig;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

public class MotorPositionUtil {
	
	private static Logger LOGGER = LoggerFactory.getLogger(MotorPositionUtil.class);
	
	
	public static double getSmallMotorSize(String motorType, String fanInfo, String supplier, String motorBaseNo,
				String startStyle, FanCodeElectricSize fanCodeElectricSize) {
			FanEmElectricSize fanEmElectricSize = null;
			List<FanEmElectricSize> fanEmElectricSizeList = AhuMetadata.findAll(FanEmElectricSize.class);
			if (null != fanEmElectricSizeList && fanEmElectricSizeList.size() > 0) {
			    for (FanEmElectricSize emElectricSize : fanEmElectricSizeList) {
			        if (motorBaseNo.startsWith(emElectricSize.getMOTORBASENO())) {
			            fanEmElectricSize = emElectricSize;
			            break;
			        }
			    }
			}
	//                if (null != fanCodeElectricSize && null != fanEmElectricSize) {
	//                    Integer INNERWIDTH = fanCodeElectricSize.getINNERWIDTH();
			    Integer A = fanCodeElectricSize.getA();
	//                    Integer P = fanCodeElectricSize.getP();
			    Integer AP = fanCodeElectricSize.getAP();
			    Integer M = fanCodeElectricSize.getM();
			    Integer N = fanCodeElectricSize.getN();
			    Integer D = 0;
			    Integer W = fanCodeElectricSize.getW();
			    Integer BPABB = fanEmElectricSize.getBPABB();
			    Integer BPWOLONG = fanEmElectricSize.getBPWOLONG();
			    Double BPDONGYUAN = fanEmElectricSize.getBPDONGYUAN();
			    Integer BPGE = fanEmElectricSize.getBPGE();
			    Integer BPSIMENS = fanEmElectricSize.getBPSIMENS();
			    Integer PTABB = fanEmElectricSize.getPTABB();
			    Integer PTWOLONG = fanEmElectricSize.getPTWOLONG();
			    Double PTDONGYUAN = fanEmElectricSize.getPTDONGYUAN();
			    Integer PTGE = fanEmElectricSize.getPTGE();
			    Integer PTGE3 = fanEmElectricSize.getPTGE3();
			    Integer PTSIMENS3 = fanEmElectricSize.getPTSIMENS3();
			    Integer PTSIMENS2 = fanEmElectricSize.getPTSIMENS2();
			    Double SSDONGYUAN = fanEmElectricSize.getSSDONGYUAN();
			    Integer SSWOLONG = fanEmElectricSize.getSSWOLONG();
			    Integer FBWOLONG = fanEmElectricSize.getFBWOLONG();
			    Integer C = fanEmElectricSize.getC();
			    double L = 0;
			    if (startStyle.equals(FAN_STARTSTYLE_ENBEDDED)) {//安装启动柜 //嵌入式变频启动柜
			        D = fanCodeElectricSize.getD();
			    }
			    if (SystemCalculateConstants.FAN_TYPE_5.equals(motorType) || SystemCalculateConstants.FAN_TYPE_6.equals(motorType)) { //变频电机
			        if (FAN_SUPPLIER_DEFAULT.equals(supplier)) {
			            L = BPWOLONG;
			        } else if (FAN_SUPPLIER_DONGYUAN.equals(supplier)) {
			            L = BaseDataUtil.doubleConversionInteger(BPDONGYUAN);
			        } else if (FAN_SUPPLIER_ABB.equals(supplier)) {
			            L = BPABB;
			        } else if (FAN_SUPPLIER_GE.equals(supplier)) {
			            L = BPGE;
			        } else if (FAN_SUPPLIER_SIMENS.equals(supplier)) {
			            L = BPSIMENS;
			        } else {
			            LOGGER.error("fan supplier is null " + fanInfo);
			        }
			    } else if (SystemCalculateConstants.FAN_TYPE_3.equals(motorType)) {//防爆电机
			        if (FAN_SUPPLIER_DEFAULT.equals(supplier)) {
			            L = FBWOLONG;
			        } else if (FAN_SUPPLIER_DONGYUAN.equals(supplier)) {
			            LOGGER.error("fan supplier is FBDONG YUAN " + fanInfo);
			        } else if (FAN_SUPPLIER_ABB.equals(supplier)) {
			            LOGGER.error("fan supplier is FBABB " + fanInfo);
			        } else if (FAN_SUPPLIER_GE.equals(supplier)) {
			            LOGGER.error("fan supplier is FBGE " + fanInfo);
			        } else {
			            LOGGER.error("fan supplier is FBSIMENS " + fanInfo);
			        }
			    } else if (SystemCalculateConstants.FAN_TYPE_4.equals(motorType)) {//双速电机
			        if (FAN_SUPPLIER_DEFAULT.equals(supplier)) {
			            L = SSWOLONG;
			        } else if (FAN_SUPPLIER_DONGYUAN.equals(supplier)) {
			            L = SSDONGYUAN;
			        } else if (FAN_SUPPLIER_ABB.equals(supplier)) {
			            LOGGER.error("fan supplier is SSABB " + fanInfo);
			        } else if (FAN_SUPPLIER_GE.equals(supplier)) {
			            LOGGER.error("fan supplier is SSGE " + fanInfo);
			        } else {
			            LOGGER.error("fan supplier is SSSIMENS " + fanInfo);
			        }
			    } else {//普通电机、二级三级电机
			        if (FAN_SUPPLIER_DEFAULT.equals(supplier)) {
			            L = PTWOLONG;
			        } else if (FAN_SUPPLIER_DONGYUAN.equals(supplier)) {
			            L = PTDONGYUAN;
			        } else if (FAN_SUPPLIER_ABB.equals(supplier)) {
			            L = PTABB;
			        } else if (FAN_SUPPLIER_GE.equals(supplier)) {
			            if (SystemCalculateConstants.FAN_TYPE_2.equals(motorType)) {//三级能效电机
			                L = PTGE3;
			            } else {
			                L = PTGE;
			            }
			        } else if (FAN_SUPPLIER_SIMENS.equals(supplier)) {
			            if (SystemCalculateConstants.FAN_TYPE_2.equals(motorType)) {//三级能效电机
			                L = PTSIMENS3;
			            } else {//二级能效电机
			                L = PTSIMENS2;
			            }
			        } else {
			            LOGGER.error("fan supplier is PTNULL " + JSON.toJSONString(fanInfo));
			        }
			    }
			    double inWidth = A + N + (M - N) / 2 - C + L + 50 + D;
			return inWidth;
		}
	
		public static FanCodeElectricSize getSmallFanSize(String code, String fanTypeCode) {
			FanCodeElectricSize fanCodeElectricSize = AhuMetadata.findOne(FanCodeElectricSize.class, fanTypeCode, code);
			return fanCodeElectricSize;
		}
	
		public static double getBigUnitMotorSize(String motorType, String fanInfo, String supplier,
				String motorBaseNo, String code, String startStyle, String fanTypeCode,FanCodeElectricSizeBig fanCodeElectricSizeBig) {
			/*大机组风机电机尺寸*/
			FanEmElectricSizeBig fanEmElectricSizeBig = null;
			List<FanEmElectricSizeBig> fanEmElectricSizeBigList = AhuMetadata.findAll(FanEmElectricSizeBig.class);
			if (null != fanEmElectricSizeBigList && fanEmElectricSizeBigList.size() > 0) {
			    for (FanEmElectricSizeBig electricSizeBig : fanEmElectricSizeBigList) {
			        if (motorBaseNo.startsWith(electricSizeBig.getMOTORBASENO())) {
			            fanEmElectricSizeBig = electricSizeBig;
			            break;
			        }
			    }
			}
			Double SIDEDISTANCESIZE = fanCodeElectricSizeBig.getSIDEDISTANCESIZE();
	//                    Double P = fanCodeElectricSizeBig.getP();
			Integer BOTTOMLENGTH = fanCodeElectricSizeBig.getBOTTOMLENGTH();
			Integer FANSIZE = fanCodeElectricSizeBig.getFANSIZE();
			Integer BPABB = fanEmElectricSizeBig.getBPABB();
			Integer BPWOLONG = fanEmElectricSizeBig.getBPWOLONG();
			Double BPDONGYUAN = fanEmElectricSizeBig.getBPDONGYUAN();
			Integer BPGE = fanEmElectricSizeBig.getBPGE();
			Integer BPSIMENS = fanEmElectricSizeBig.getBPSIMENS();
			Integer PTABB = fanEmElectricSizeBig.getPTABB();
			Integer PTWOLONG = fanEmElectricSizeBig.getPTWOLONG();
			Integer PTDONGYUAN = fanEmElectricSizeBig.getPTDONGYUAN();
			Integer PTGE = fanEmElectricSizeBig.getPTGE();
			Integer PTGE2 = fanEmElectricSizeBig.getPTGE2();
			Integer PTSIMENS3 = fanEmElectricSizeBig.getPTSIMENS3();
			Integer PTSIMENS2 = fanEmElectricSizeBig.getPTSIMENS2();
			Double SSDONGYUAN = fanEmElectricSizeBig.getSSDONGYUAN();
			Integer SSWOLONG = fanEmElectricSizeBig.getSSWOLONG();
			Integer FBWOLONG = fanEmElectricSizeBig.getFBWOLONG();
			Integer C = fanEmElectricSizeBig.getC();
			Integer D = 0;
			Integer W = fanCodeElectricSizeBig.getW();
			if (startStyle.equals(FAN_STARTSTYLE_ENBEDDED)) {//安装启动柜 //嵌入式变频启动柜
			    D = fanCodeElectricSizeBig.getD();
			}
			double L = 0;
			if (SystemCalculateConstants.FAN_TYPE_5.equals(motorType) || SystemCalculateConstants.FAN_TYPE_6.equals(motorType)) { //变频电机
			    if (FAN_SUPPLIER_DEFAULT.equals(supplier)) {
			        L = BPWOLONG;
			    } else if (FAN_SUPPLIER_DONGYUAN.equals(supplier)) {
			        L = BaseDataUtil.doubleConversionInteger(BPDONGYUAN);
			    } else if (FAN_SUPPLIER_ABB.equals(supplier)) {
			        L = BPABB;
			    } else if (FAN_SUPPLIER_GE.equals(supplier)) {
			        L = BPGE;
			    } else if (FAN_SUPPLIER_SIMENS.equals(supplier)) {
			        L = BPSIMENS;
			    } else {
			        LOGGER.error("fan supplier is simens big" + fanInfo);
			    }
			} else if (SystemCalculateConstants.FAN_TYPE_3.equals(motorType)) {//防爆电机
			    if (FAN_SUPPLIER_DEFAULT.equals(supplier)) {
			        L = FBWOLONG;
			    } else if (FAN_SUPPLIER_DONGYUAN.equals(supplier)) {
			        LOGGER.error("fan supplier is big FBDONG YUAN " + fanInfo);
			    } else if (FAN_SUPPLIER_ABB.equals(supplier)) {
			        LOGGER.error("fan supplier is big FBABB " + fanInfo);
			    } else if (FAN_SUPPLIER_GE.equals(supplier)) {
			        LOGGER.error("fan supplier is big FBGE " + fanInfo);
			    } else {
			        LOGGER.error("fan supplier is big FBSIMENS " + fanInfo);
			    }
			} else if (SystemCalculateConstants.FAN_TYPE_4.equals(motorType)) {//双速电机
			    if (FAN_SUPPLIER_DEFAULT.equals(supplier)) {
			        L = SSWOLONG;
			    } else if (FAN_SUPPLIER_DONGYUAN.equals(supplier)) {
			        L = SSDONGYUAN;
			    } else if (FAN_SUPPLIER_ABB.equals(supplier)) {
			        LOGGER.error("fan supplier is big SSABB " + fanInfo);
			    } else if (FAN_SUPPLIER_GE.equals(supplier)) {
			        LOGGER.error("fan supplier is big SSGE " + fanInfo);
			    } else {
			        LOGGER.error("fan supplier is big SSSIMENS " + fanInfo);
			    }
			} else {//普通电机、二级三级电机
			    if (FAN_SUPPLIER_DEFAULT.equals(supplier)) {
			        L = PTWOLONG;
			    } else if (FAN_SUPPLIER_DONGYUAN.equals(supplier)) {
			        L = PTDONGYUAN;
			    } else if (FAN_SUPPLIER_ABB.equals(supplier)) {
			        L = PTABB;
			    } else if (FAN_SUPPLIER_GE.equals(supplier)) {
			        if (SystemCalculateConstants.FAN_TYPE_1.equals(motorType)) {//二级能效电机
			            L = PTGE2;
			        } else {
			            L = PTGE;
			        }
			    } else if (FAN_SUPPLIER_SIMENS.equals(supplier)) {
			        if (SystemCalculateConstants.FAN_TYPE_2.equals(motorType)) {//三级能效电机
			            L = PTSIMENS3;
			        } else {//二级能效电机
			            L = PTSIMENS2;
			        }
			    } else {
			        LOGGER.error("fan supplier is big PTSIMENS " + fanInfo);
			    }
			}
			double inWidth = BOTTOMLENGTH - FANSIZE - SIDEDISTANCESIZE - L - D;
			return inWidth;
		}
	
		public static FanCodeElectricSizeBig getBigFanSIze(String code, String fanTypeCode) {
			FanCodeElectricSizeBig fanCodeElectricSizeBig = AhuMetadata.findOne(FanCodeElectricSizeBig.class, fanTypeCode, code);
			return fanCodeElectricSizeBig;
		}
}


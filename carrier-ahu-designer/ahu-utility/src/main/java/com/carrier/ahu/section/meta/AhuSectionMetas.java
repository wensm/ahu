package com.carrier.ahu.section.meta;

import static com.carrier.ahu.vo.SystemCalculateConstants.AHU_PRODUCT_39CQ;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cn.hutool.core.util.ObjectUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;

import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.AhuPropSetting;
import com.carrier.ahu.metadata.entity.AhuPropUnit;
import com.carrier.ahu.metadata.section.ValueOption;
import com.carrier.ahu.po.meta.MetaParameter;
import com.carrier.ahu.po.meta.SectionMeta;
import com.carrier.ahu.po.meta.ValueType;
import com.carrier.ahu.po.meta.layout.Group;
import com.carrier.ahu.po.meta.layout.MetaLayout;
import com.carrier.ahu.section.meta.MetaLayoutGen.MetaParameterWithUnit;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.ExcelUtils;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey;
import com.carrier.ahu.vo.FileNamesLoadInSystem;

import lombok.extern.slf4j.Slf4j;

/**
 * Described all the sections in project phase 2 All the data comes from file
 * /sections/ahu.section.parameter.xls:
 *
 * @author JL
 */
@Slf4j
public class AhuSectionMetas {

    private Map<String, Map<String, Object>> sectionDefaultValueMap = new HashMap<>();//公制
    private Map<String, Map<String, Object>> sectionDefaultValueMapUnitTypeB = new HashMap<>();//英制
    private Map<String, Object> allDefaultValueMap = new HashMap<>();
    private Map<String, Object> allMaterialDefaultValueMap = new LinkedHashMap<>();

    private static class AhuSectionMetaHelper {
        private static final AhuSectionMetas INSTANCE = new AhuSectionMetas();
    }

    public static AhuSectionMetas getInstance() {
        return AhuSectionMetaHelper.INSTANCE;
    }

    private AhuSectionMetas() {
        loadUnits();
        loadSections();
        setAllMetaList();
    }

	/** 机组属性参数配置 */
	private Map<String, AhuPropUnit> ahuPropUnitMap;
	/** 机组设定配置 */
	private Map<String, Map<String, String>> unitSettingMap;
	/** 有代码中默认属性的段属性列表 */
	private List<SectionMeta> sectionMetaListWithDefault;
	/** 没有代码中默认属性的段属性列表 */
	private List<SectionMeta> sectionMetaListWithoutDefault;
	/** 系统所有属性列表 */
	private List<SectionMeta> allMetaList;
	/** 系统所有属性列表 */
	private Map<String, MetaParameter> allMetaMap;
	/** 段布局设定 */
	private Map<String, MetaLayout> metaLayoutMap;
	/** 存放各个系列段布局*/
	private Map<String, Map<String, MetaLayout>> metaLayoutMaps = new HashMap<>();
	/** 没有代码中默认属性的段属性Map */
	private Map<String, SectionMeta> allSectionMetaMap = new HashMap<>();
	/**  */
	private Map<String, List<MetaParameter>> plateMetaWithDetail = new HashMap<String, List<MetaParameter>>();

	/** 系统所有材料相关属性名称列表 */
	private List<String> allMaterialMetaKeyList;
	/** 系统所有材料相关属性列表 */
	private List<MetaParameter> allMaterialMetaList;

	/** 段分类数组 */
    public final static SectionTypeEnum[] SECTIONTYPES = new SectionTypeEnum[] { SectionTypeEnum.TYPE_AHU,
            SectionTypeEnum.TYPE_MIX, SectionTypeEnum.TYPE_SINGLE, SectionTypeEnum.TYPE_COMPOSITE,
            SectionTypeEnum.TYPE_FAN, SectionTypeEnum.TYPE_COLD, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL,
            SectionTypeEnum.TYPE_HEATINGCOIL, SectionTypeEnum.TYPE_STEAMCOIL, SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL,
            SectionTypeEnum.TYPE_WETFILMHUMIDIFIER, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER,
            SectionTypeEnum.TYPE_STEAMHUMIDIFIER, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER,
            SectionTypeEnum.TYPE_ACCESS, SectionTypeEnum.TYPE_ATTENUATOR, SectionTypeEnum.TYPE_DISCHARGE,
            SectionTypeEnum.TYPE_HEPAFILTER, SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER,
            SectionTypeEnum.TYPE_ELECTROSTATICFILTER, SectionTypeEnum.TYPE_CTR, SectionTypeEnum.TYPE_WHEELHEATRECYCLE,
            SectionTypeEnum.TYPE_PLATEHEATRECYCLE, SectionTypeEnum.META_DEFAULT_PARAMETER };

    /** 阻力计算数组 不包含热回收段 */
    public final static SectionTypeEnum[] SECTIONTYPES_RESISTANCE = new SectionTypeEnum[] { SectionTypeEnum.TYPE_AHU,
            SectionTypeEnum.TYPE_MIX, SectionTypeEnum.TYPE_SINGLE, SectionTypeEnum.TYPE_COMPOSITE,
            SectionTypeEnum.TYPE_FAN, SectionTypeEnum.TYPE_COLD, SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL,
            SectionTypeEnum.TYPE_HEATINGCOIL, SectionTypeEnum.TYPE_STEAMCOIL, SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL,
            SectionTypeEnum.TYPE_WETFILMHUMIDIFIER, SectionTypeEnum.TYPE_SPRAYHUMIDIFIER,
            SectionTypeEnum.TYPE_STEAMHUMIDIFIER, SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER,
            SectionTypeEnum.TYPE_ACCESS, SectionTypeEnum.TYPE_ATTENUATOR, SectionTypeEnum.TYPE_DISCHARGE,
            SectionTypeEnum.TYPE_HEPAFILTER, SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER,
            SectionTypeEnum.TYPE_ELECTROSTATICFILTER, SectionTypeEnum.TYPE_CTR,
            SectionTypeEnum.META_DEFAULT_PARAMETER };

	public final static String[] COMMON_SECTION_META_SHEETNAMES = {UtilityConstant.METACOMMON_COMMON,UtilityConstant.METACOMMON_NONSTANDARD};//两个公用sheetname【公用的meta属性、非标】
	public static final String KEY_ASSIS = "assis";
	public static final String KEY_NS = UtilityConstant.METACOMMON_NS;
	public static final String KEY_META = UtilityConstant.METACOMMON_META;

	/**
	 * 从《ahu.meta.unit1.0.xlsx》加载机组相关的配置<br>
	 */
    private void loadUnits() {
        List<AhuPropUnit> ahuPropUnits = AhuMetadata.findAll(AhuPropUnit.class);
        Map<String, AhuPropUnit> ahuPropUnitMap = new HashMap<>();
        Map<String, String> unitBMap = new HashMap<>();
        Map<String, String> unitMMap = new HashMap<>();
        for (AhuPropUnit ahuPropUnit : ahuPropUnits) {
            ahuPropUnitMap.put(ahuPropUnit.getKey(), ahuPropUnit);
        }

        List<AhuPropSetting> ahuPropSettings = AhuMetadata.findAll(AhuPropSetting.class);
        for (AhuPropSetting ahuPropSetting : ahuPropSettings) {
            unitBMap.put(ahuPropSetting.getKey(), ahuPropSetting.getB());
            unitMMap.put(ahuPropSetting.getKey(), ahuPropSetting.getM());
        }
        this.ahuPropUnitMap = ahuPropUnitMap;
        this.unitSettingMap = new HashMap<>();
        this.unitSettingMap.put(UtilityConstant.SYS_ALPHABET_B_UP, unitBMap);
        this.unitSettingMap.put(UtilityConstant.SYS_ALPHABET_M_UP, unitMMap);
    }

	/**
	 * 从《ahu.section.parameters1.3.xls》加载段属性相关的配置<br>
	 */
	private void loadSections() {
		List<SectionMeta> sectionMetaListWithDefault = new ArrayList<SectionMeta>();
		List<SectionMeta> sectionMetaListWithoutDefault = new ArrayList<SectionMeta>();

		List<String> allMaterialMetaKeyList = new ArrayList<>();
		List<MetaParameter> allMaterialMetaList = new ArrayList<>();

		Map<String, MetaLayout> metaLayoutMap = new HashMap<>();
		InputStream is = null;
		List<List<String>> list = Collections.emptyList();
		
		//公共meta
		List<List<List<String>>> listCommon = new ArrayList<List<List<String>>>();
		
		try {
			is = AhuSectionMetas.class.getClassLoader().getResourceAsStream(FileNamesLoadInSystem.SECTIONS_XLSX_NAME);
			Workbook book = ExcelUtils.read(is, ExcelUtils.isExcel2003(FileNamesLoadInSystem.SECTIONS_XLSX_NAME));
			
			
			for(String common:COMMON_SECTION_META_SHEETNAMES){
				list = ExcelUtils.readSheet(book, common, 1);
				if(null !=list){
					listCommon.add(list);
				}
			}
			for (int i = 0; i < SECTIONTYPES.length; i++) {
				SectionTypeEnum sectionTypeEnum = SECTIONTYPES[i];
				String sheetName = sectionTypeEnum.getId();
				SectionMeta metaWithDefault = createSectionMeta(sectionTypeEnum);
				SectionMeta metaWithoutDefault = createSectionMetaWithoutDefault(sectionTypeEnum);

				list = ExcelUtils.readSheet(book, sheetName, 1);

				//添加默认meta属性到每个段
				for(List<List<String>> commonMeta:listCommon){
					resolveSheet(metaWithDefault, commonMeta, allMaterialMetaKeyList, allMaterialMetaList,true);	
				}
				
				resolveSheet(metaWithDefault, list, allMaterialMetaKeyList, allMaterialMetaList,false);
				resolveSheet(metaWithoutDefault, list, allMaterialMetaKeyList, allMaterialMetaList,false);
				sectionMetaListWithDefault.add(metaWithDefault);
				sectionMetaListWithoutDefault.add(metaWithoutDefault);

				// Prepare layout
				MetaLayout layout = getSectionMetaLayout(metaWithDefault);
				// Inject layout properties 841656514564
				// TODO：在添加第三方语言的时候，恢复多语言从独立多语言文件获取的逻辑，
				// MetaLayoutGen.mergeLayoutProperty(layout, meta,
				// metacn2KeyMap);
				MetaLayoutGen.mergeLayoutProperty(layout, metaWithDefault, Collections.emptyMap(),AHU_PRODUCT_39CQ);
				metaLayoutMap.put(metaWithDefault.getMetaId(), layout);
				allSectionMetaMap.put(metaWithDefault.getMetaId(),metaWithDefault);
			}
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			log.error("Failed to load excel: " + FileNamesLoadInSystem.SECTIONS_XLSX_NAME, e);
			return;
		} finally {
			try {
				if (EmptyUtil.isNotEmpty(is)) {
					is.close();
				}
			} catch (IOException e) {
				log.error("Failed to close " + FileNamesLoadInSystem.SECTIONS_XLSX_NAME, e);
			}
		}
		this.sectionMetaListWithDefault = sectionMetaListWithDefault;
		this.sectionMetaListWithoutDefault = sectionMetaListWithoutDefault;
		this.metaLayoutMap = metaLayoutMap;
		this.plateMetaWithDetail = getPlatMetaWithDetail(metaLayoutMap);
		this.allMaterialMetaKeyList = allMaterialMetaKeyList;
		this.allMaterialMetaList = allMaterialMetaList;

		log.error("开始打印1.3excel 加载结果>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		log.error("allMaterialMetaKeyList.size:"+allMaterialMetaKeyList.size());
		log.error("allMaterialMetaList.size:"+allMaterialMetaList.size());
		for (Map.Entry<String, MetaLayout> entry : metaLayoutMap.entrySet()) {
			String partKey = entry.getKey();
			MetaLayout partDefaultMap = entry.getValue();

			List<MetaParameter> mps = partDefaultMap.getGroups().get(0).getParas();
			for (int mlm = 0; mlm < mps.size(); mlm++) {
				log.error(partKey+":"+mps.get(mlm).getKey());
			}
		}
		log.error("结束打印1.3excel 加载结果>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}

	private Map<String, List<MetaParameter>> getPlatMetaWithDetail(Map<String, MetaLayout> metaLayoutMap2) {
		Map<String, List<MetaParameter>> plateMetaWithDetail = new HashMap<String, List<MetaParameter>>();
		Iterator<String> it = metaLayoutMap.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			MetaLayout layout = metaLayoutMap.get(key);
			List<MetaParameter> list = layout.getGroups().get(0).getParas();
			plateMetaWithDetail.put(key, list);
		}
		return plateMetaWithDetail;
	}

	/**
	 * 返回标准的key，map格式的元数据，以方便读取
	 * 
	 * @return
	 */
	public Map<String, List<MetaParameter>> getPlatMetaWithDetail() {
		return this.plateMetaWithDetail;
	}

	private SectionMeta createSectionMeta(SectionTypeEnum sectionTypeEnum) {
		SectionMeta meta = new SectionMeta();
		meta.setMetaId(sectionTypeEnum.getId());
		meta.setName(sectionTypeEnum.getName());
		meta.setcName(sectionTypeEnum.getCnName());
		meta.setVersion(sectionTypeEnum.getVersion());
//		MetaCodeGen.appendDefaultMeta(meta);
		return meta;
	}

	private SectionMeta createSectionMetaWithoutDefault(SectionTypeEnum sectionTypeEnum) {
		SectionMeta meta = new SectionMeta();
		meta.setMetaId(sectionTypeEnum.getId());
		meta.setName(sectionTypeEnum.getName());
		meta.setcName(sectionTypeEnum.getCnName());
		meta.setVersion(sectionTypeEnum.getVersion());
		return meta;
	}
	/**
	 * 构造meta集合
	 * @param meta
	 * @param list
	 * @param allMaterialMetaKeyList
	 * @param allMaterialMetaList
	 * @param commonMeta 是否公用meta
	 */
	private void resolveSheet(SectionMeta meta, List<List<String>> list, List<String> allMaterialMetaKeyList,
			List<MetaParameter> allMaterialMetaList, boolean commonMeta) {
		Iterator<List<String>> it = list.iterator();
		while (it.hasNext()) {
			List<String> subList = it.next();
			MetaParameter para = resolveParameter(meta, subList,commonMeta);
			meta.appendParameter(para);
			if (para.isUrEnable()) {
				meta.appendParameter(cloneParaWithUr(para));
			}
			if (para.isrMaterial()) {
				allMaterialMetaKeyList.add(para.getKey());
				allMaterialMetaList.add(para);
			}
		}
	}

	private MetaParameter cloneParaWithUr(MetaParameter para) {
		String key = para.getKey();
		if (key.startsWith(KEY_META + UtilityConstant.SYS_PUNCTUATION_DOT)) {
			key = KEY_NS + UtilityConstant.SYS_PUNCTUATION_DOT + key.substring(5);
		}
		return new MetaParameter(key, para.getName(), para.getGroup(), para.isrMaterial(), para.isrDirection(),
				para.isrPerformance(), para.isEditable(), para.isUrEnable(), para.isGlobal(), para.getValueSource(),
				para.getValueType(), para.getMemo(), para.getEmemo());
	}
	/**
	 * 构造meta MetaParameter对象
	 * @param meta
	 * @param list
	 * @param commonMeta 是否公用meta
	 * @return
	 */
	private MetaParameter resolveParameter(SectionMeta meta, List<String> list, boolean commonMeta) {
		String prefix = MetaCodeGen.calculateAttributePrefix(meta.getMetaId());
		MetaParameter para = new MetaParameter();
		String key = list.get(0).trim();
		
		String valueSource = list.get(9);
		
		if(!commonMeta)//普通meta  处理key valueSource
		{
			if (!key.startsWith(KEY_META + UtilityConstant.SYS_PUNCTUATION_DOT)) 
			{
				key = prefix + UtilityConstant.SYS_PUNCTUATION_DOT + key;
			}
			
			if (!valueSource.startsWith(KEY_META + UtilityConstant.SYS_PUNCTUATION_DOT)) 
			{
				if (StringUtils.isEmpty(valueSource)) {
					valueSource = key;
				} else if (ahuPropUnitMap.containsKey(valueSource)) {
					// It is a Unit
				} else {
					// Get the default value source
					valueSource = prefix + UtilityConstant.SYS_PUNCTUATION_DOT + valueSource;
				}
			}
		}
		else//公用meta sheetName【meta.common，meta.nonStandard】只处理key 2018 4/18
		{
			
			if(!key.startsWith(KEY_NS + UtilityConstant.SYS_PUNCTUATION_DOT))//meta 【***】【assis.section.***】 【meta.section.***】 ,处理【***】添加段prefix
			{
				if (!key.contains(UtilityConstant.SYS_PUNCTUATION_DOT)) {
					key = prefix + UtilityConstant.SYS_PUNCTUATION_DOT + key;
				}
				if (!StringUtils.isEmpty(valueSource) && !valueSource.contains(UtilityConstant.SYS_PUNCTUATION_DOT)) {
					if (ahuPropUnitMap.containsKey(valueSource)) {
						// It is a Unit
					} else {
						// Get the default value source
						valueSource = prefix + UtilityConstant.SYS_PUNCTUATION_DOT + valueSource;
					}
				}
				if(StringUtils.isEmpty(valueSource)){
					valueSource = key;
				}
			}
			else{//非标sheet 里面的key 特殊处理【ns.***】 
				key = key.replace(KEY_NS + UtilityConstant.SYS_PUNCTUATION_DOT, UtilityConstant.SYS_BLANK);
				key = prefix + UtilityConstant.SYS_PUNCTUATION_DOT + key;
				key = key.replace(UtilityConstant.METACOMMON_META, UtilityConstant.METACOMMON_NS);
			}
		}
		para = new MetaParameter(key, list.get(1), list.get(2), Boolean.parseBoolean(list.get(3)),
				Boolean.parseBoolean(list.get(4)), Boolean.parseBoolean(list.get(5)), Boolean.parseBoolean(list.get(6)),
				Boolean.parseBoolean(list.get(7)), Boolean.parseBoolean(list.get(8)), valueSource,
				getValueType(list.get(10)), list.get(11),
				(list.size() >= 13 && StringUtils.isNotBlank(list.get(12))) ? list.get(12) : list.get(11));
		if (list.size() > 13 && !StringUtils.isEmpty(list.get(13))) {
			String s = list.get(13);
			//出口默认值
			if(AHUContext.isExportVersion() &&
				list.size() > 14 && !StringUtils.isEmpty(list.get(14))){
				if(EmptyUtil.isNotEmpty(list.get(14))){
					s = list.get(14);
				}
			}

            try {
                Object defaultValue = s;
                if (para.getValueType() == ValueType.BooleanV) {
                    defaultValue = Boolean.valueOf(s.toLowerCase());
                } else if (para.getValueType() == ValueType.IntV) {
                    defaultValue = NumberUtil.convertStringToDoubleInt(s.toLowerCase());
                } else if (para.getValueType()==ValueType.DoubleV) {
                    defaultValue = Double.valueOf(s.toLowerCase());
                }else if (para.getValueType()==ValueType.Double2V) {
                    defaultValue = Double.valueOf(s.toLowerCase());
                }
                appendTemplate(meta.getMetaId(), key, defaultValue, para.getValueSource());
                if (isMaterialRelated(list)) { // cache material related parameter
                    allMaterialDefaultValueMap.put(key, defaultValue);
                }
			} catch (Exception e) {
				log.warn("Error in metadata excel ", e);
				appendTemplate(meta.getMetaId(), key, s,para.getValueSource());
			}
		} else {
			if (isMaterialRelated(list)) { // cache material related parameter
				allMaterialDefaultValueMap.put(key, "");
			}
			appendTemplate(meta.getMetaId(), key, UtilityConstant.SYS_BLANK,para.getValueSource());
		}
		return para;
	}

    private void appendTemplate(String metaId, String key, Object value,String valueSource) {
		appendDefaultValue(metaId, key, value,valueSource);
	}

    private void appendDefaultValue(String metaId, String key, Object value,String valueSource) {
        //公制
		Map<String, Object> sectionValueMap = sectionDefaultValueMap.get(metaId);
        if (EmptyUtil.isEmpty(sectionValueMap)) {
			sectionValueMap = new HashMap<>();
			sectionDefaultValueMap.put(metaId, sectionValueMap);
		}
		sectionValueMap.put(key, value);

        //英制
		Map<String, Object> sectionValueMapB = sectionDefaultValueMapUnitTypeB.get(metaId);
        if (EmptyUtil.isEmpty(sectionValueMapB)) {
			sectionValueMapB = new HashMap<>();
			sectionDefaultValueMapUnitTypeB.put(metaId, sectionValueMapB);
        }
		sectionValueMapB.put(key, getValueByValueSource(String.valueOf(value), key,UnitSystemEnum.B, valueSource)[0]);

        allDefaultValueMap.put(key, value);
    }

    private boolean isMaterialRelated(List<String> list) {
        return Boolean.valueOf(list.get(3));
    }

    public Map<String, Object> getAllMaterialDefaultValue() {
        return Collections.unmodifiableMap(this.allMaterialDefaultValueMap);
    }

    public Map<String, Object> getAllDefaultValue() {
        return Collections.unmodifiableMap(this.allDefaultValueMap);
    }

	/**
	 * 获取公制所有段默认值
	 * @return
	 */
    public Map<String, Map<String, Object>> getAllSectionDefaultValue() {
        return Collections.unmodifiableMap(this.sectionDefaultValueMap);
    }

	/**
	 * 获取英制所有段默认值
	 * @return
	 */
	public Map<String, Map<String, Object>> getAllSectionDefaultValueUnitTypeB() {
        return Collections.unmodifiableMap(this.sectionDefaultValueMapUnitTypeB);
    }

	/**
	 * 获取公制具体段默认值
	 * @param sectionName
	 * @return
	 */
    public Map<String, Object> getSectionDefaultValue(String sectionName) {
        Map<String, Object> sectionDefaultValueMap = this.sectionDefaultValueMap.get(sectionName);
        return sectionDefaultValueMap != null ? Collections.unmodifiableMap(sectionDefaultValueMap) : null;
    }

	/**
	 * 获取英制具体段默认值
	 * @param sectionName
	 * @return
	 */
	public Map<String, Object> getSectionDefaultValueUnitTypeB(String sectionName) {
        Map<String, Object> sectionDefaultValueMapUnitTypeB = this.sectionDefaultValueMapUnitTypeB.get(sectionName);
        return sectionDefaultValueMapUnitTypeB != null ? Collections.unmodifiableMap(sectionDefaultValueMapUnitTypeB) : null;
    }

	private ValueType getValueType(String valueType) {
		if (EmptyUtil.isEmpty(valueType)) {
			valueType = UtilityConstant.SYS_BLANK;
		}
		valueType = valueType.trim();
		if (UtilityConstant.SYS_STRING_BOOLEANV.equalsIgnoreCase(valueType)) {
			return ValueType.BooleanV;
		} else if (UtilityConstant.SYS_STRING_STRINGV.equalsIgnoreCase(valueType)) {
			return ValueType.StringV;
		} else if (UtilityConstant.SYS_STRING_INTV.equalsIgnoreCase(valueType)) {
			return ValueType.IntV;
		} else if (UtilityConstant.SYS_STRING_DOUBLEV.equalsIgnoreCase(valueType)) {
			return ValueType.DoubleV;
		} else if (UtilityConstant.SYS_STRING_DOUBLE2V.equalsIgnoreCase(valueType)) {
			return ValueType.Double2V;
		}else if (UtilityConstant.SYS_STRING_LONGSTRINGV.equalsIgnoreCase(valueType)) {
			return ValueType.LongStringV;
		}else {
			return ValueType.StringV;
		}
	}

	/**
	 * Generate the section's MetaLayout with details
	 *
	 * @return
	 */
	public Map<String, MetaLayout> getSectionMetaLayoutWithDetail() {
		Map<String, MetaLayout> result = new HashMap<>();
		result.putAll(metaLayoutMap);
		result.remove(SectionTypeEnum.TYPE_AHU.getId());
		return result;
	}

	/**
	 * Generate the section's MetaLayout with details
	 *
	 * @return
	 */
	public MetaLayout getAhuMetaLayoutWithDetail() {
		return metaLayoutMap.get(SectionTypeEnum.TYPE_AHU.getId());
	}

	/**
	 * Generate the section's MetaLayout with details by unitSeries
	 *
	 * @return
	 * @param unitSeries 系列：39CQ...
	 */
	public Map<String, MetaLayout> getAhuAndSectionMetaLayoutWithDetail(String unitSeries) {
		try {
			String factory = AHUContext.getFactoryName();
			if (EmptyUtil.isEmpty(unitSeries)) {
				unitSeries = UtilityConstant.SYS_UNIT_SERIES_39CQ; // TODO remove it later.
			}
			if (EmptyUtil.isNotEmpty(factory)) {
				//返回39CQ 系列layout
				if(unitSeries.equals(AHU_PRODUCT_39CQ)){
					return metaLayoutMap;
				}else{
					//返回39XT 39G 系列layout
					if (metaLayoutMaps.containsKey(factory+unitSeries)) {
                        return metaLayoutMaps.get(factory+unitSeries);
                    }
					Map<String, MetaLayout> metaLayoutMapNew = getMetaLayoutMapNew(unitSeries);
					metaLayoutMaps.put(factory+unitSeries,metaLayoutMapNew);
					return metaLayoutMapNew;
				}
			}
		} catch (Exception e) {
			log.error("Failed to load Series layout");
		}
		return metaLayoutMap;
	}

	/**
	 * 根据39CQ系列layout 获取 mege后的 XT G系列layout
	 * @param unitSeries
	 * @return
	 */
	private Map<String, MetaLayout> getMetaLayoutMapNew(String unitSeries) {
		Map<String, MetaLayout> metaLayoutMapNew = new HashMap<>();
		for (int i = 0; i < SECTIONTYPES.length; i++) {
			SectionTypeEnum sectionTypeEnum = SECTIONTYPES[i];
			SectionMeta metaWithDefault = allSectionMetaMap.get(sectionTypeEnum.getId());
			//获取39CQ 段layout
			MetaLayout layout = metaLayoutMap.get(metaWithDefault.getMetaId());

			MetaLayout layoutTemp = ObjectUtil.clone(layout);//深拷贝，39CQ layout(MetaLayout 里面引入的所有对象需要实现序列化)
			//mege XT G 系列layout
			MetaLayoutGen.mergeLayoutProperty(layoutTemp, metaWithDefault, Collections.emptyMap(),unitSeries);
			metaLayoutMapNew.put(metaWithDefault.getMetaId(), layoutTemp);
		}
		return metaLayoutMapNew;
	}

	/**
	 * Generate the section's MetaLayout
	 *
	 * @return
	 */
	private MetaLayout getSectionMetaLayout(SectionMeta meta) {
		MetaLayout layout = new MetaLayout(meta.getMetaId());
		layout.setVersion(meta.getVersion());
		List<Group> groups = new ArrayList<>();
		Group group1 = new Group(UtilityConstant.SYS_STRING_DEFAULT_1, UtilityConstant.SYS_STRING_DEFAULT_1);
		List<String> keys = new ArrayList<>();
		Iterator<String> it = meta.getParameters().keySet().iterator();
		while (it.hasNext()) {
			keys.add(it.next());
		}
		group1.setKeys(keys);
		groups.add(group1);
		layout.setGroups(groups);
		return layout;
	}

	/**
	 * 仅仅返回段的元数据信息
	 *
	 * @return
	 */
	public List<SectionMeta> getSectionMetas() {
		List<SectionMeta> result = new ArrayList<>();
		Iterator<SectionMeta> it = sectionMetaListWithDefault.iterator();
		while (it.hasNext()) {
			SectionMeta meta = it.next();
			if (meta.getMetaId().equals(SectionTypeEnum.TYPE_AHU.getId())) {
				continue;
			}
			result.add(meta);
		}
		return result;
	}

	/**
	 * 仅仅返回段的元数据信息
	 *
	 * @return
	 */
	public SectionMeta getDefaultParameterMeta() {
		Iterator<SectionMeta> it = sectionMetaListWithDefault.iterator();
		while (it.hasNext()) {
			SectionMeta meta = it.next();
			if (meta.getMetaId().equals(SectionTypeEnum.META_DEFAULT_PARAMETER.getId())) {
				return meta;
			}
		}
		return null;
	}

	/**
	 * 仅仅返回段的元数据信息[AHU]
	 *
	 * @return
	 */
	public SectionMeta getAhuMeta() {
		Iterator<SectionMeta> it = sectionMetaListWithDefault.iterator();
		while (it.hasNext()) {
			SectionMeta meta = it.next();
			if (meta.getMetaId().equals(SectionTypeEnum.TYPE_AHU.getId())) {
				return meta;
			}
		}
		return null;
	}

	/**
	 * 返回单位数据的Map信息
	 *
	 * @return
	 */
	public Map<String, AhuPropUnit> getAhuPropUnitMeta() {
		return this.ahuPropUnitMap;
	}

	public Map<String, Map<String, String>> getUnitSetting() {
		return this.unitSettingMap;
	}

	/**
	 * Return exact SectionMata
	 *
	 * @param type
	 *            the section type
	 * @return
	 * @see SectionTypeEnum
	 */
	public SectionMeta getSectionMeta(SectionTypeEnum type) {
		Iterator<SectionMeta> it = sectionMetaListWithDefault.iterator();
		while (it.hasNext()) {
			SectionMeta meta = it.next();
			if (meta.getMetaId().equals(type.getId())) {
				return meta;
			}
		}
		return null;
	}

	/**
	 * Return exact SectionMataWithoutDefault
	 *
	 * @param type
	 *            the section type
	 * @return
	 * @see SectionTypeEnum
	 */
	public SectionMeta getSectionMetaWithoutDefault(SectionTypeEnum type) {
		Iterator<SectionMeta> it = sectionMetaListWithoutDefault.iterator();
		while (it.hasNext()) {
			SectionMeta meta = it.next();
			if (meta.getMetaId().equals(type.getId())) {
				return meta;
			}
		}
		return null;
	}

	/**
	 * 返回段和机组的元数据信息
	 *
	 * @return
	 */
	public List<SectionMeta> getAhuAndSectionMetas() {
		return sectionMetaListWithDefault;
	}

	/**
	 * 返回所有属性的集合
	 * 
	 * @return
	 */
	public List<SectionMeta> getAllMetaList() {
		return allMetaList;
	}

	/**
	 * 将所有属性整合到allMetaList
	 */
	private void setAllMetaList() {
		List<SectionMeta> all = new ArrayList<>();
		all.addAll(sectionMetaListWithDefault);
		all.add(getAhuMeta());
		this.allMetaList = all;
		Map<String, MetaParameter> map = new HashMap<>();
		for (SectionMeta meta : allMetaList) {
			map.putAll(meta.getParameters());
		}
		this.allMetaMap = map;
	}

	/**
	 * 返回所有属性的Map
	 * 
	 * @return
	 */
	public Map<String, MetaParameter> getAllMetaMap() {
		return this.allMetaMap;
	}

	/**
	 * 
	 * @param value
	 *            公制的值
	 * @param parameterKey
	 *            属性的key
	 * @param unitType
	 *            公制（M）或者英制(B)
	 * @return
	 */
	public String[] getValueByUnit(String value, String parameterKey, UnitSystemEnum unitType) {
		if (!unitSettingMap.containsKey(unitType.getId()) || !allMetaMap.containsKey(parameterKey)) {
			return new String[] { value, UtilityConstant.SYS_BLANK };
		}
		MetaParameter para = allMetaMap.get(parameterKey);
		String valueSource = para.getValueSource();
		if (!ahuPropUnitMap.containsKey(valueSource)) {
			return new String[] { value, UtilityConstant.SYS_BLANK };
		}
		AhuPropUnit unit = ahuPropUnitMap.get(valueSource);
		Map<String, String> unitMap = unitSettingMap.get(unitType.getId());
		if (!unitMap.containsKey(valueSource)) {
			return new String[] { value, UtilityConstant.SYS_BLANK };
		}
		String unitItemType = unitMap.get(valueSource);
		if (UnitSystemEnum.M.getId().equals(unitItemType)) {
			return new String[] { value, unit.getMlabel() };
		} else if (UnitSystemEnum.B.getId().equals(unitItemType)) {
			try {
				double doub = Double.valueOf(value);
				double result = doub;
				if (unit.getKey().equals(UtilityConstant.SYS_STRING_TEMPRETURE)) {
					result = 32 + doub * 1.8;
					result = NumberUtil.scale(result, 2);
				} else {
					result = doub * unit.getTob();
					result = NumberUtil.scale(result, 4);
				}
				return new String[] { String.valueOf(result), unit.getBlabel() };
			} catch (Exception e) {
				log.warn(String.format("Failed to convert value to double %s on %s.", value, parameterKey));
				return new String[] { value, unit.getMlabel() };
			}

		} else {
			log.warn("Wrong unit setting: " + unitItemType);
			return new String[] { value, UtilityConstant.SYS_BLANK };
		}

	}
	
	/**
	 * 
	 * @param value
	 *            公制的值
	 * @param parameterKey
	 *            属性的key
	 * @param unitType
	 *            公制（M）或者英制(B)
	 * @return
	 */
	public String getStringValueByUnit(String value, String parameterKey) {
		
	    UnitSystemEnum unitType = AHUContext.getUnitSystem();
		 
		if (!unitSettingMap.containsKey(unitType.getId()) || !allMetaMap.containsKey(parameterKey)) {
			return value;
		}
		MetaParameter para = allMetaMap.get(parameterKey);
		String valueSource = para.getValueSource();
		if (!ahuPropUnitMap.containsKey(valueSource)) {
			return value;
		}
		AhuPropUnit unit = ahuPropUnitMap.get(valueSource);
		
		if (UnitSystemEnum.M.getId().equals(unitType.getId())) {
			return value;
		} else if (UnitSystemEnum.B.getId().equals(unitType.getId())) {
			try {
				double doub = Double.valueOf(value);
				double result = doub;
				if (unit.getKey().equals(UtilityConstant.SYS_STRING_TEMPRETURE)) {
					result = 32 + doub * 1.8;
					result = NumberUtil.scale(result, 2);
				} else {
					result = doub * unit.getTob();
					result = NumberUtil.scale(result, 4);
				}
				return  String.valueOf(result);
			} catch (Exception e) {
				log.warn(String.format("Failed to convert value to double %s on %s.", value, parameterKey));
				return value;
			}

		} else {
			return value;
		}

	}
	/**
	 * 
	 * @param value
	 *            公制的值
	 * @param parameterKey
	 *            属性的key
	 * @param unitType
	 *            公制（M）或者英制(B)
	 * @return
	 */
	public String[] getValueByValueSource(String value, String parameterKey,UnitSystemEnum unitType, String valueSource) {
		if (!ahuPropUnitMap.containsKey(valueSource)) {
			return new String[] { value, UtilityConstant.SYS_BLANK };
		}
		AhuPropUnit unit = ahuPropUnitMap.get(valueSource);
		Map<String, String> unitMap = unitSettingMap.get(unitType.getId());
		if (!unitMap.containsKey(valueSource)) {
			return new String[] { value, UtilityConstant.SYS_BLANK };
		}
		String unitItemType = unitMap.get(valueSource);
		if (UnitSystemEnum.M.getId().equals(unitItemType)) {
			return new String[] { value, unit.getMlabel() };
		} else if (UnitSystemEnum.B.getId().equals(unitItemType)) {
			try {
				double doub = Double.valueOf(value);
				double result = doub;
				if (unit.getKey().equals(UtilityConstant.SYS_STRING_TEMPRETURE)) {
					result = 32 + doub * 1.8;
					result = NumberUtil.scale(result, 2);
				} else {
					result = doub * unit.getTob();
					result = NumberUtil.scale(result, 4);
				}
				return new String[] { String.valueOf(result), unit.getBlabel() };
			} catch (Exception e) {
				log.warn(String.format("Failed to convert value to double %s on %s.", value, parameterKey));
				return new String[] { value, unit.getMlabel() };
			}

		} else {
			log.warn("Wrong unit setting: " + unitItemType);
			return new String[] { value, UtilityConstant.SYS_BLANK };
		}

	}
	/**
	 * 获取下拉框类型的值显示
	 * 
	 * @param value
	 *            公制的值
	 * @param parameterKey
	 *            属性的key
	 * @param sectionKey
	 *            段的key
	 * @param locale
	 *            语言
	 * @return
	 */
	public String getValueByUnit(String value, String parameterKey, String sectionKey, LanguageEnum locale) {
		List<MetaParameter> pList = plateMetaWithDetail.get(sectionKey);
		if (EmptyUtil.isEmpty(pList)) {
			log.warn(String.format("Failed to find the key : %s", parameterKey));
			return value;
		}
		for (MetaParameter para : pList) {
			if (para.getKey().equals(parameterKey)) {
				// if ((para instanceof MetaParameterWithUnit)) {
				// return value;
				// }

				MetaParameterWithUnit parau = (MetaParameterWithUnit) para;
				ValueOption option = parau.getOption();
				if (EmptyUtil.isEmpty(option)) {
					return value;
				}
				int index = ArrayUtils.indexOf(option.getOptions(), value);

				if (index == -1) {
					log.warn(String.format("Failed to find the value %s @ key : %s", value, parameterKey));
					return value;
				}
                String engLabel = parau.getOption().getLabels()[index];
                // if label is default, use value instead
                if (UtilityConstant.SYS_STRING_DEFAULT.equalsIgnoreCase(engLabel)) {
                    // update default fan supplier to 'WOLONG' in report 
                    String fanSupplier = SectionMetaUtils.getMetaSectionKey(SectionTypeEnum.TYPE_FAN, MetaKey.KEY_sUPPLIER);
                    if(fanSupplier.equals(parameterKey)) {
                        return AHUContext.getIntlString(UtilityConstant.JSON_FAN_SUPPLIER_WOLONG_KEY);
                    }
                    return value;
                } else if (locale == LanguageEnum.Chinese) {
                    return parau.getOption().getClabels()[index];
                } else {
                    return engLabel;
                }
			} else {
				continue;
			}
		}
		log.warn(String.format("Failed to find the key : %s", parameterKey));
		return value;
	}

	public boolean isMaterialMetaKey(String key) {
		return allMaterialMetaKeyList.contains(key);
	}

	public List<MetaParameter> getAllMaterialMetaList() {
		return allMaterialMetaList;
	}

}

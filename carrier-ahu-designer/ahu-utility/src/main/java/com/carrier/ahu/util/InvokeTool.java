package com.carrier.ahu.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.util.ReflectionUtils;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.po.meta.MetaParameter;
import com.carrier.ahu.po.meta.SectionMeta;
import com.carrier.ahu.section.meta.AhuSectionMetas;


import lombok.extern.slf4j.Slf4j;

import static com.carrier.ahu.constant.CommonConstant.METASEXON_AIRDIRECTION;

/**
 * 利用反射机制完成Map向Bean的转换
 * 
 * @author jaaka
 *
 */
@Slf4j
public class InvokeTool<E> {
	protected static Logger logger = LoggerFactory.getLogger(InvokeTool.class);
	protected final static String ErrMsg = "{0} when Reflect @ genCoilInParamFromAhuParam, Name:{1}, ParamType:{2}";

	/**
	 * 将Bean转化成metaJson
	 * 
	 * @param bean
	 * @param season
	 * @param sectionKey
	 * @return
	 */
	public Map<String, Object> reInvoke(E bean, String season, String sectionKey) {
		Map<String, Object> result = new HashMap<>();
		// 获取当前段的所有属性列表
		SectionMeta meta = AhuSectionMetas.getInstance()
				.getSectionMeta(SectionTypeEnum.getSectionTypeFromId(sectionKey));
		Map<String, MetaParameter> parameters = meta.getParameters();
		// 拼接属性名称，在全属性列表中查找，未找到在拼接季节，然后在找，还未找到，抛出
		String metaKeyPrefix = sectionKey.replace(UtilityConstant.METAHU_AHU_NAME, UtilityConstant.METASEXON_PREFIX);
		String[] metaKeys = ReflectionUtils.getFiledName(bean);
		for (String key : metaKeys) {
			String metaKey = metaKeyPrefix + UtilityConstant.SYS_PUNCTUATION_DOT + key;
			if (parameters.containsKey(metaKey)) {
				Object value = ReflectionUtils.getFieldValueByName(key, bean);
				result.put(metaKey, value);
			} else {
				metaKey = metaKeyPrefix + UtilityConstant.SYS_PUNCTUATION_DOT + season.toUpperCase() + key;
				if (parameters.containsKey(metaKey)) {
					Object value = ReflectionUtils.getFieldValueByName(key, bean);
					result.put(metaKey, value);
				} else {
					logger.warn(MessageFormat.format("Bean 反序列成 metaJson 未找到属性名,Meta:{0},BeanField:{1},season:{2}",
							sectionKey, key, season));
				}
			}
		}

		return result;
	}

	/**
	 * 盘管段转换
	 * 
	 * @param ahuParam
	 * @param pparam
	 * @return
	 */
	public E genInParamFromAhuParam(AhuParam ahuParam, PartParam pparam, String season, E para) {
		Map<String, Object> amap = ahuParam.getParams();
		Map<String, Object> pmap = pparam.getParams();
		String partKey=pparam.getKey();
		String part=null;
		try{
			part=partKey.split("\\.")[1];
		}catch(Exception e) {
			
		}
		Field[] fields = para.getClass().getDeclaredFields();
		Map<String, Field> coilInParamFieldMap = new HashMap<>();
		for (Field field : fields) {
			coilInParamFieldMap.put(field.getName(), field);
		}
		Class<?> clazz = para.getClass();
		for (Entry<String, Object> entry : amap.entrySet()) {
			setParam(para, entry, coilInParamFieldMap, clazz, season);
			// setParam(para, entry, coilInParamFieldMap);
		}
		for (Entry<String, Object> entry : pmap.entrySet()) {
			
			if(StringUtils.isNotEmpty(part)&&entry.getKey().contains(part)||part==null||METASEXON_AIRDIRECTION.equals(entry.getKey())) {
			setParam(para, entry, coilInParamFieldMap, clazz, season);
			}
			// setParam(para, entry, coilInParamFieldMap);
		}
		return para;
	}

	/**
	 * 
	 * @param para
	 * @param entry
	 * @param coilInParamFieldMap
	 * @param clazz
	 * @param season
	 *            w（enableWinter），s（enableSummer）
	 */
	public static void setParam(Object para, Entry<String, Object> entry, Map<String, Field> coilInParamFieldMap,
			Class<?> clazz, String season) {
		try {
			String sprefix = "";// 季节的前缀
			if (StringUtils.isNotBlank(season)) {
				sprefix = season.toUpperCase();
			}

			String mKey = entry.getKey();
			Object mValue = entry.getValue();
			int start = mKey.lastIndexOf(UtilityConstant.SYS_PUNCTUATION_DOT) + 1;
			String ekey = mKey.substring(start);
			if (StringUtils.isEmpty(ekey)) {
				logger.error("Emprty key : " + mKey);
				return;
			}
			if (coilInParamFieldMap.containsKey(ekey.substring(1)) && sprefix.equals(ekey.substring(0, 1))) {
				// 或者Wkey or Skey是否存在，其中W，S是冬夏季变量名的缩写
				String setterName = UtilityConstant.SYS_STRING_SET + toUpperCase4Index(ekey.substring(1));
				Field feild = coilInParamFieldMap.get(ekey.substring(1));
				setter(para, setterName, feild, clazz, mValue);
			} else if (coilInParamFieldMap.containsKey(ekey)) {
				// 检查对应的key是否存在
				String setterName = UtilityConstant.SYS_STRING_SET + toUpperCase4Index(ekey);
				Field feild = coilInParamFieldMap.get(ekey);
				setter(para, setterName, feild, clazz, mValue);
			}  else if (coilInParamFieldMap.containsKey(mKey.replace(UtilityConstant.SYS_PUNCTUATION_DOT,UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN))) {
				// 检查对应的fullkey是否存在
				String tempMKey = mKey.replace(UtilityConstant.SYS_PUNCTUATION_DOT,
						UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN);
				String setterName = UtilityConstant.SYS_STRING_SET + toUpperCase4Index(tempMKey);
				Field feild = coilInParamFieldMap.get(tempMKey);
				setter(para, setterName, feild, clazz, mValue);
			} else {
	//			logger.debug(MessageFormat.format("{0} does not contain field from map key: [{1}]", clazz.getSimpleName(),
	//					entry.getKey()));
			}
		} catch (Exception e) {

		}
	}

	private static void setter(Object para, String setterName, Field feild, Class<?> clazz, Object mValue) {
		String feildTypeName = feild.getType().getName();
		if (UtilityConstant.SYS_STRING_JAVA_LANG_STRING.equals(feildTypeName)) {
			try {
				Method method = clazz.getMethod(setterName, String.class);
				try {
					method.invoke(para, mValue.toString());
				} catch (IllegalAccessException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALACCESSEXCEPTION, setterName, feildTypeName));
				} catch (IllegalArgumentException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALARGUMENTEXCEPTION, setterName, feildTypeName));
				} catch (InvocationTargetException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_INVOCATIONTARGETEXCEPTION, setterName, feildTypeName));
				}
			} catch (NoSuchMethodException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NOSUCHMETHODEXCEPTION, setterName, feildTypeName));
			} catch (SecurityException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_SECURITYEXCEPTION, setterName, feildTypeName));
			}
		} else if (UtilityConstant.SYS_STRING_JAVA_LANG_DOUBLE.equals(feildTypeName)) {
			try {
				Method method = clazz.getMethod(setterName, Double.class);
				Double d = Double.parseDouble(mValue.toString());
				try {
					method.invoke(para, d);
				} catch (IllegalAccessException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALACCESSEXCEPTION, setterName, feildTypeName));
				} catch (IllegalArgumentException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALARGUMENTEXCEPTION, setterName, feildTypeName));
				} catch (InvocationTargetException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_INVOCATIONTARGETEXCEPTION, setterName, feildTypeName));
				}
			} catch (NoSuchMethodException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NOSUCHMETHODEXCEPTION, setterName, feildTypeName));
			} catch (SecurityException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_SECURITYEXCEPTION, setterName, feildTypeName));
			} catch (NumberFormatException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NUMBERFORMATEXCEPTION, setterName, feildTypeName));
			}
		} else if (UtilityConstant.SYS_STRING_JAVA_DOUBLE.equals(feildTypeName)) {
			try {
				Method method = clazz.getMethod(setterName, double.class);
				double d = Double.parseDouble(mValue.toString());
				try {
					method.invoke(para, d);
				} catch (IllegalAccessException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALACCESSEXCEPTION, setterName, feildTypeName));
				} catch (IllegalArgumentException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALARGUMENTEXCEPTION, setterName, feildTypeName));
				} catch (InvocationTargetException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_INVOCATIONTARGETEXCEPTION, setterName, feildTypeName));
				}
			} catch (NoSuchMethodException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NOSUCHMETHODEXCEPTION, setterName, feildTypeName));
			} catch (SecurityException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_SECURITYEXCEPTION, setterName, feildTypeName));
			} catch (NumberFormatException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NUMBERFORMATEXCEPTION, setterName, feildTypeName));
			}

		} else if (UtilityConstant.SYS_STRING_JAVA_LANG_INTEGER.equals(feildTypeName)) {
			try {
				Method method = clazz.getMethod(setterName, Integer.class);
				Double d = Double.parseDouble(mValue.toString());
				try {
					method.invoke(para, d.intValue());
				} catch (IllegalAccessException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALACCESSEXCEPTION, setterName, feildTypeName));
				} catch (IllegalArgumentException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALARGUMENTEXCEPTION, setterName, feildTypeName));
				} catch (InvocationTargetException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_INVOCATIONTARGETEXCEPTION, setterName, feildTypeName));
				}
			} catch (NoSuchMethodException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NOSUCHMETHODEXCEPTION, setterName, feildTypeName));
			} catch (SecurityException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_SECURITYEXCEPTION, setterName, feildTypeName));
			} catch (NumberFormatException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NUMBERFORMATEXCEPTION, setterName, feildTypeName));
			}
		} else if (UtilityConstant.SYS_STRING_JAVA_INT.equals(feildTypeName)) {
			try {
				Method method = clazz.getMethod(setterName, int.class);
				Double d = Double.parseDouble(mValue.toString());
				try {
					method.invoke(para, d.intValue());
				} catch (IllegalAccessException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALACCESSEXCEPTION, setterName, feildTypeName));
				} catch (IllegalArgumentException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALARGUMENTEXCEPTION, setterName, feildTypeName));
				} catch (InvocationTargetException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_INVOCATIONTARGETEXCEPTION, setterName, feildTypeName));
				}
			} catch (NoSuchMethodException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NOSUCHMETHODEXCEPTION, setterName, feildTypeName));
			} catch (SecurityException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_SECURITYEXCEPTION, setterName, feildTypeName));
			} catch (NumberFormatException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NUMBERFORMATEXCEPTION, setterName, feildTypeName));
			}
		} else if (UtilityConstant.SYS_STRING_JAVA_LANG_BOOLEAN.equals(feildTypeName)) {
			try {
				Method method = clazz.getMethod(setterName, Boolean.class);
				try {
					method.invoke(para, Boolean.parseBoolean(mValue.toString()));
				} catch (IllegalAccessException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALACCESSEXCEPTION, setterName, feildTypeName));
				} catch (IllegalArgumentException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALARGUMENTEXCEPTION, setterName, feildTypeName));
				} catch (InvocationTargetException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_INVOCATIONTARGETEXCEPTION, setterName, feildTypeName));
				}
			} catch (NoSuchMethodException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NOSUCHMETHODEXCEPTION, setterName, feildTypeName));
			} catch (SecurityException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_SECURITYEXCEPTION, setterName, feildTypeName));
			}
		} else if (UtilityConstant.SYS_STRING_JAVA_BOOLEAN.equals(feildTypeName)) {
			try {
				Method method = clazz.getMethod(setterName, boolean.class);
				try {
					method.invoke(para, Boolean.parseBoolean(mValue.toString()));
				} catch (IllegalAccessException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALACCESSEXCEPTION, setterName, feildTypeName));
				} catch (IllegalArgumentException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_ILLEGALARGUMENTEXCEPTION, setterName, feildTypeName));
				} catch (InvocationTargetException e) {
					logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_INVOCATIONTARGETEXCEPTION, setterName, feildTypeName));
				}
			} catch (NoSuchMethodException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_NOSUCHMETHODEXCEPTION, setterName, feildTypeName));
			} catch (SecurityException e) {
				logger.error(MessageFormat.format(ErrMsg, UtilityConstant.SYS_EXCEPTION_SECURITYEXCEPTION, setterName, feildTypeName));
			}
		}
	}

	/**
	 * 首字母大写
	 * 
	 * @param string
	 * @return
	 */
	private static String toUpperCase4Index(String string) {
		char[] methodName = string.toCharArray();
		methodName[0] = toUpperCase(methodName[0]);
		return String.valueOf(methodName);
	}

	/**
	 * 字符转成大写
	 * 
	 * @param chars
	 * @return
	 */
	private static char toUpperCase(char chars) {
		if (97 <= chars && chars <= 122) {
			chars ^= 32;
		}
		return chars;
	}

}

package com.carrier.ahu.util;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.metadata.section.MetaValue;
import com.carrier.ahu.metadata.section.ValueOption;
import com.carrier.ahu.util.meta.SectionMetaUtils;

/**
 * @ClassName: MetaSelectUtil 
 * @Description: (通过下拉框key 以及下拉框option，获取中英文显示值) 
 * @author dongxiangxiang <dong.xiangxiang@carries.utc.com>   
 * @date 2018年3月5日 上午9:51:11 
 *
 */
public class MetaSelectUtil {
	private static Logger logger = LoggerFactory.getLogger(MetaSelectUtil.class);

	/**
	 * 获取下拉框中文/英文 显示值
	 * @param metaKey
	 * @param option
	 * @param language
	 * @return
	 */
	public static String getTextForSelect(String metaKey,String option,LanguageEnum language){
	    try {
	        // TODO should pass unit series
			Map<String, MetaValue> metaValues = SectionMetaUtils.getSectionMetaValues(null);
			
			ValueOption optionObj = metaValues.get(metaKey).getOption();
			String[] labels = optionObj.getClabels();
			if(LanguageEnum.English.equals(language)){
				labels = optionObj.getLabels();
			}
			String[] options = optionObj.getOptions();
			for (int i = 0; i < options.length; i++) {
				if(option.equals(options[i])){
					return labels[i];
				}
			}
		} catch (Exception e) {
			logger.error("获取下拉框中文/英文 显示值 error：",e);
		}
		return "";
	}
}

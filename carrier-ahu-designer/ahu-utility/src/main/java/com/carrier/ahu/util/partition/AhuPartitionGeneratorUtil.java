package com.carrier.ahu.util.partition;

import static com.carrier.ahu.common.model.partition.AhuPartition.S_MKEY_METAID;
import static com.carrier.ahu.common.model.partition.AhuPartition.S_MKEY_METAJSON;
import static com.carrier.ahu.constant.CommonConstant.*;
import static com.carrier.ahu.util.partition.AhuPartitionGenerator.PREFER_JIAQIANG_VERTIAL_LINE_VAL;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;

import java.io.File;
import java.text.MessageFormat;
import java.util.*;


import cn.hutool.core.util.ObjectUtil;
import com.carrier.ahu.constant.CommonConstant;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.NumberUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.calculator.ExcelForPanelUtils;
import com.carrier.ahu.calculator.panel.PanelXSLXPO;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.model.partition.FrameLine;
import com.carrier.ahu.common.model.partition.PanelFace;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.cad.Damper;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.FileNamesLoadInSystem;
import com.carrier.ahu.vo.SysConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import cn.hutool.core.io.file.FileReader;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AhuPartitionGeneratorUtil {
    /**
     * 前后端面切割
     * @param face
     * @param serial
     * @param isChange
     * @param partition
     * @param partionIndex
     * @param partitions
     * @param panelFace
     */
    public static void cutFrontAndBack(String face, String serial, boolean isChange, AhuPartition partition, int partionIndex, List<AhuPartition> partitions, PanelFace panelFace) {
        //端面段的属性
        Map<String, Object> ahuParaMap = partition.getAhuParameters();
        Map<String, String> sectionMap = new HashMap<>();
        Map<String, Object> partMap = partition.getSections().get(0);
        if(face.equals(SYS_PANEL_BACK)){
            partMap = partition.getSections().get(partition.getSections().size() - 1);
        }
        String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
        sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));
        String metaId = String.valueOf(partMap.get(S_MKEY_METAID));
        SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);

        //风机
        boolean isFan = false;
        if(SectionTypeEnum.TYPE_FAN == sectionType){
            isFan = true;
        }
        //无蜗壳风机
        boolean isWWKSection = false;
        if(JSON_FAN_OUTLET_WWK.equals(sectionMap.get(METASEXON_FAN_OUTLET))){
            isWWKSection = true;
        }


        /**
         * 1第一个分段的前端面、最后一个分段的后端面、
         * 2无蜗壳风机左面采用数据切割 ；无蜗壳风机右面不采用数据库切割
         * 3普通风机后端面采用数据库切割
         **/
        if(
                (partition.getFaceType().isHasLeft() && face.equals(SYS_PANEL_FRONT))
                        || (partition.getFaceType().isHasRight() && face.equals(SYS_PANEL_BACK) && !isWWKSection)
                        || (!isWWKSection && isFan && face.equals(SYS_PANEL_BACK)
                        || (isWWKSection))
                ){

            //构造查询exel的参数
            LinkedList<Map<String, Object>> coupleSections = partition.getCoupleSections();
            Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSections.get(0));
            if (face.equals(SYS_PANEL_BACK)) {
                sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSections.get(coupleSections.size() - 1));
            }
            String typeNo = MapValueUtils.getStringValue(AhuPartition.C_MKEY_CODE, coupleSections.get(0));
            if (face.equals(SYS_PANEL_BACK)) {
                typeNo = MapValueUtils.getStringValue(AhuPartition.C_MKEY_CODE, coupleSections.get(coupleSections.size() - 1));
            }
            while (typeNo.startsWith("0")) {
                typeNo = typeNo.substring(1);
            }

            //查询excel切割数据
            PanelXSLXPO po = null;
            if (null == sectionType) {
                System.out.println("在解析分段数据时候出错了");
                sectionType = SectionTypeEnum.getSectionTypeFromId(metaId.split(SYS_PUNCTUATION_LOW_HYPHEN)[0]);
                System.out.println("在解析分段数据时候出错了"+sectionType);
            }
            if (SectionTypeEnum.TYPE_FAN == sectionType) {// 风机段端面excel切割
                po = getFanPanelXSLXPO(serial, ahuParaMap, sectionMap, sectionL,face);

                //回风机、进口位置“端面”，左端面切割使用 混合段风阀切割方式
                String airDirection = sectionMap.get(METASEXON_AIRDIRECTION);
                if (!isWWKSection && AirDirectionEnum.RETURNAIR.getCode().equals(airDirection) && face.equals(SYS_PANEL_FRONT)) {//回风机组
                    String fkjjwz = sectionMap.get(METASEXON_FAN_RETURNPOSITION);
                    if (!FAN_SENDPOSITION_NO.equals(fkjjwz) && FAN_SENDPOSITION_FACE.equals(fkjjwz)){
                        po = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial);
                    }
                }
            } else if ((SectionTypeEnum.TYPE_MIX == sectionType && SYS_ASSERT_TRUE.equals(String.valueOf(sectionMap.get(METASEXON_MIX_RETURNBACK))))
                    ||(face.equals(SYS_PANEL_BACK) && SectionTypeEnum.TYPE_DISCHARGE == sectionType && JSON_MIX_OUTTYPE_A.equals(String.valueOf(sectionMap.get(METASEXON_DISCHARGE_OUTLETDIRECTION))))) {
                // 端面带风阀(混合段》后回风) 出风段后出风
                po = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial);

                //获取变形Po
                if(isChange && (serial.contains(AHU_PRODUCT_39XT) || serial.contains(AHU_PRODUCT_39G))){
                    po = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial.replace(AHU_PRODUCT_39XT,AHU_PRODUCT_39CQ).replace(AHU_PRODUCT_39G,AHU_PRODUCT_39CQ));
                    if (null != po) {
                        po.setSerial(serial);
                    }
                }
            } else {// 普通端面无风阀及出风口

                //最左端面，最右端面单层外抽，特殊处理为无面板
                boolean isFiterXSection = false;
                if(SectionTypeEnum.TYPE_SINGLE == sectionType
                    &&JSON_FILTER_FILTERF_X.equals(sectionMap.get(METASEXON_FILTER_FITETF))){
                    isFiterXSection = true;
                }
                if(isFiterXSection &&
                        ((partition.getFaceType().isHasLeft() && face.equals(SYS_PANEL_FRONT))
                        || (partition.getFaceType().isHasRight() && face.equals(SYS_PANEL_BACK)))
                        ){
                    Integer[] horizontals, verticals, horizontalTypes, verticalTypes;
                    horizontals = new Integer[] { panelFace.getPanelWidth() };
                    verticals = new Integer[] { panelFace.getPanelLength() };
                    horizontalTypes = new Integer[] { SYS_PANEL_TYPE_WU };//无面板
                    verticalTypes = new Integer[] { SYS_PANEL_TYPE_WU};//无面板
                    // 组装叶子面板
                    slicePanelByRule(panelFace, horizontals, horizontalTypes, verticals, verticalTypes);
                    // 封装叶子面板为树状结构
                    slicePanelByBitreeRule(panelFace,serial);
                    return;
                }else{
                    po = ExcelForPanelUtils.getInstance().getNormalSidePanel(serial);
                }


            }

            if (null == po) {
                log.warn(MessageFormat.format("面板{0}端面初始化-数据库逻辑-查询数据库元数据失败异常 serial:[{01}],typeNo:[{2}],sectionL:[{3}]", face,serial, typeNo, String.valueOf(sectionL)));
            }
            //数据库切割
            doProcessPanel(po, panelFace, serial);
        }else{
            /** 分段夹层面板不切割 **/

            Integer[] horizontals, verticals, horizontalTypes, verticalTypes;
            horizontals = new Integer[] { panelFace.getPanelWidth() };
            verticals = new Integer[] { panelFace.getPanelLength() };

            horizontalTypes = new Integer[] { SYS_PANEL_TYPE_WU };//无面板
            verticalTypes = new Integer[] { SYS_PANEL_TYPE_WU};//无面板

            // 组装叶子面板
            slicePanelByRule(panelFace, horizontals, horizontalTypes, verticals, verticalTypes);

            // 封装叶子面板为树状结构
            slicePanelByBitreeRule(panelFace,serial);
        }
    }
    /**
     * XT系列 前后端面切割
     * @param face
     * @param serial
     * @param isChange
     * @param partition
     * @param partionIndex
     * @param partitions
     * @param panelFace
     */
    public static void cutFrontAndBackForXT(String face, String serial, boolean isChange, AhuPartition partition, int partionIndex, List<AhuPartition> partitions, PanelFace panelFace) {
        //端面段的属性
        Map<String, String> sectionMap = new HashMap<>();
        Map<String, Object> partMap = partition.getSections().get(0);
        if(face.equals(SYS_PANEL_BACK)){
            partMap = partition.getSections().get(partition.getSections().size() - 1);
        }
        String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
        sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));
        String metaId = String.valueOf(partMap.get(S_MKEY_METAID));
        SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);

        /**
         * 1第一个分段的前端面、最后一个分段的后端面
         **/
        if((partition.getFaceType().isHasLeft() && face.equals(SYS_PANEL_FRONT))
                || (partition.getFaceType().isHasRight() && face.equals(SYS_PANEL_BACK))
                ) {

            //构造查询exel的参数
            LinkedList<Map<String, Object>> coupleSections = partition.getCoupleSections();
            Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSections.get(0));
            if (face.equals(SYS_PANEL_BACK)) {
                sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSections.get(coupleSections.size() - 1));
            }
            String typeNo = MapValueUtils.getStringValue(AhuPartition.C_MKEY_CODE, coupleSections.get(0));
            if (face.equals(SYS_PANEL_BACK)) {
                typeNo = MapValueUtils.getStringValue(AhuPartition.C_MKEY_CODE, coupleSections.get(coupleSections.size() - 1));
            }
            while (typeNo.startsWith("0")) {
                typeNo = typeNo.substring(1);
            }

            //查询excel切割数据
            PanelXSLXPO po = null;
            if (null == sectionType) {
                System.out.println("在解析分段数据时候出错了");
                sectionType = SectionTypeEnum.getSectionTypeFromId(metaId.split(SYS_PUNCTUATION_LOW_HYPHEN)[0]);
                System.out.println("在解析分段数据时候出错了"+sectionType);
            }
            if ((SectionTypeEnum.TYPE_MIX == sectionType && SYS_ASSERT_TRUE.equals(String.valueOf(sectionMap.get(METASEXON_MIX_RETURNBACK))))
                    ||(SectionTypeEnum.TYPE_DISCHARGE == sectionType && JSON_MIX_OUTTYPE_A.equals(String.valueOf(sectionMap.get(METASEXON_DISCHARGE_OUTLETDIRECTION))))) {
                // 端面带风阀(混合段》后回风) 出风段后出风
                po = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial);
                //获取变形Po
                if(isChange && (serial.contains(AHU_PRODUCT_39XT) || serial.contains(AHU_PRODUCT_39G))){
                    po = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial.replace(AHU_PRODUCT_39XT,AHU_PRODUCT_39CQ).replace(AHU_PRODUCT_39G,AHU_PRODUCT_39CQ));
                    if (null == po) {
                        po.setSerial(serial);
                    }
                }
            } else {// 普通端面无风阀及出风口,9M逻辑切割
                cut39XTCommonSizePanel(face, serial, partition, panelFace);
                return;
            }

            if (null == po) {
                log.warn(MessageFormat.format("面板{0}端面初始化-数据库逻辑-查询数据库元数据失败异常 serial:[{01}],typeNo:[{2}],sectionL:[{3}]", face,serial, typeNo, String.valueOf(sectionL)));
            }
            //数据库切割
            doProcessPanel(po, panelFace, serial);
        }else{
            /** 分段夹层面板不切割 **/
            Integer[] horizontals, verticals, horizontalTypes, verticalTypes;
            horizontals = new Integer[] { panelFace.getPanelWidth() };
            verticals = new Integer[] { panelFace.getPanelLength() };

            horizontalTypes = new Integer[] { SYS_PANEL_TYPE_WU };//无面板
            verticalTypes = new Integer[] { SYS_PANEL_TYPE_WU};//无面板

            // 组装叶子面板
            slicePanelByRule(panelFace, horizontals, horizontalTypes, verticals, verticalTypes);


            // 封装叶子面板为树状结构
            slicePanelByBitreeRule(panelFace,serial);
        }
    }
    /**
     * 获取风机中间面、右端面 excel切割数据并转换为po
     * @param serial
     * @param ahuParaMap
     * @param sectionMap
     * @param sectionL
     * @param face 所属面
     * @return
     */
    private static PanelXSLXPO getFanPanelXSLXPO(String serial, Map<String, Object> ahuParaMap, Map<String, String> sectionMap, Integer sectionL,String face) {
        PanelXSLXPO po;
        String direction = String.valueOf(ahuParaMap.get(AhuPartitionGenerator.pipeOrientation));//接管方向
        String outletDirection = METASEXON_FAN_OUTLETDIRECTION;//出风方向:顶部THF/BHF 端面（水平）UBF/UBR
        String fanMotorPosition = METASEXON_FAN_MOTORPOSITION;//电机位置:侧置side 后置back
        String fanOutlet = METASEXON_FAN_OUTLET;//风机形式
        outletDirection = sectionMap.get(outletDirection);
        fanMotorPosition = sectionMap.get(fanMotorPosition);
        fanOutlet = sectionMap.get(fanOutlet);
        //无蜗壳风机
        if(JSON_FAN_OUTLET_WWK.equals(fanOutlet)){
            po = ExcelForPanelUtils.getInstance().getSideFanWWKPanel(serial);

            //送风出口位置
            String airDirection = sectionMap.get(METASEXON_AIRDIRECTION);
            String fkjjwz = sectionMap.get(METASEXON_FAN_SENDPOSITION);
            if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)) {//回风机组
                fkjjwz = sectionMap.get(METASEXON_FAN_RETURNPOSITION);
            }
            if(face.equals(SYS_PANEL_BACK)) {
                if (!FAN_SENDPOSITION_NO.equals(fkjjwz) && FAN_SENDPOSITION_FACE.equals(fkjjwz)) {//端面出风&无蜗壳右端面采用标准混合段后出风方式切割。
                    po = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial);
                }else{
                    po = ExcelForPanelUtils.getInstance().getNormalSidePanel(serial);// 普通端面无风阀及出风口
                }
            }
        }else {//普通风机

            /**顶部出风:1：端面采用普通excel切割 2: 不需要中间面**/
            if (FAN_OUTLETDIRECTION_UBF.equals(outletDirection) || FAN_OUTLETDIRECTION_UBR.equals(outletDirection)) {
                if(face.equals(SYS_PANEL_FRONT) || face.equals(SYS_PANEL_BACK)) {
                    po = ExcelForPanelUtils.getInstance().getNormalSidePanel(serial);// 普通端面无风阀及出风口
                }else if(NumberUtil.convertStringToDoubleInt(face)>=6){
                    po = null;
                }else{
                    po = null;
                }
            }else {
                /** 端面出风（中间面、右端面）**/
                //吴千群需要回退成动态侧置、后置不能写死
                if ("side".equals(fanMotorPosition)) {//侧置
                    po = ExcelForPanelUtils.getInstance().getSideFanSendFacePositionSPanel(serial, direction, String.valueOf(sectionL));
                    if(EmptyUtil.isEmpty(po)){
                        //新建切割对象
                        po =createPanelXSLXPO(serial);//变形的中见面找不到暂时用整张普通面板
                    }
                } else {//后置
                    po = ExcelForPanelUtils.getInstance().getSideFanSendFacePositionBPanel(serial);
                    if(EmptyUtil.isEmpty(po)){
                        //新建切割对象
                        po =createPanelXSLXPO(serial);//变形的中见面找不到暂时用整张普通面板
                    }
                }
                //po = ExcelForPanelUtils.getInstance().getSideFanSendFacePositionSPanel(serial, direction, String.valueOf(sectionL));
                if(face.equals(SYS_PANEL_FRONT)){
                    po = ExcelForPanelUtils.getInstance().getNormalSidePanel(serial);// 普通端面无风阀及出风口
                }
            }
        }
        return po;
    }

    private static PanelXSLXPO createPanelXSLXPO(String serial) {
        PanelXSLXPO po = new PanelXSLXPO();
        int[] heightWidth= AhuUtil.getHeightAndWidthOfAHU(serial);
        po.setH(SYS_BLANK+heightWidth[0]);
        po.setW(SYS_BLANK+heightWidth[1]);
        po.setWr(String.valueOf(heightWidth[1]));
        po.setWp(String.valueOf(SYS_PANEL_TYPE_PUTONG));
        po.setHr(SYS_BLANK+heightWidth[0]);
        po.setHp(SYS_BLANK+SYS_PANEL_TYPE_WU);
        return po;
    }

    /**
     * 获取风机顶面excel切割数据并转换为po
     * @param serial
     * @param direction 接管方向
     * @param sectionL
     * @param fanMotorPosition
     * @return
     */
    private static PanelXSLXPO getFanTopPanelXSLXPO(String serial, String direction, Integer sectionL, String fanMotorPosition) {
        //顶部出风(顶面)
        PanelXSLXPO po = null;
        if ("side".equals(fanMotorPosition)) {//侧置
            po = ExcelForPanelUtils.getInstance().getSideFanSendTopPositionSPanel(serial, direction, String.valueOf(sectionL));
        } else {//后置
            po = ExcelForPanelUtils.getInstance().getSideFanSendTopPositionBPanel(serial, direction, String.valueOf(sectionL));
        }
        return po;
    }
    /**
     * 端面中间面切割
     * @param serial
     * @param isChange
     * @param partition
     * @param partitions
     * @param panels
     */
    public static void cutMiddleView(String serial, boolean isChange, AhuPartition partition, List<AhuPartition> partitions, Map<String, PanelFace> panels) {
        LinkedList<Map<String, Object>> coupleSections = partition.getCoupleSections();
        Map<String, Object> ahuParaMap = partition.getAhuParameters();
            /*
            端面中间面
            二次回风段 ahu.combinedMixingChamber
            风机段 ahu.fan
            混合段 ahu.mix
            */
        int paneId = 6;//端面中间面
        int serialCode = 0;
        int sectionsStepIndex = 0;//（由于coupleSections 有段合并情况）需要记录partition.getSections() 的index 保证和 coupleSections index 同步
        for (Map<String, Object> coupleSection : coupleSections) {

            //查询excel切割数据
            PanelXSLXPO po = null;
            String metaId = MapValueUtils.getStringValue(AhuPartition.C_MKEY_METAID, coupleSection);
            Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);
            if (null == sectionType) {
                System.out.println("在解析分段数据时候出错了");
                sectionType = SectionTypeEnum.getSectionTypeFromId(metaId.split(SYS_PUNCTUATION_LOW_HYPHEN)[0]);
                System.out.println("在解析分段数据时候出错了"+sectionType);
            }

            if(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER != sectionType
                    && SectionTypeEnum.TYPE_FAN != sectionType
                    && (serialCode == 0 || serialCode==coupleSections.size()-1)){//非新回排段、风机，并且是两端的端面跳过
                serialCode++;
                sectionsStepIndex++;
                continue;
            }

            if (metaId.contains(SYS_PUNCTUATION_LOW_HYPHEN)) {
                sectionsStepIndex++;
            }
            Map<String, Object> partMap = partition.getSections().get(sectionsStepIndex);
            Map<String, String> sectionMap = new HashMap<>();
            String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
            sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));
            String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));

            //混合段，后回风开启,添加端面中间面
            if (SectionTypeEnum.TYPE_MIX == sectionType) {
                if (metaId.equals(metaId1) && SYS_ASSERT_TRUE.equals(String.valueOf(sectionMap.get(METASEXON_MIX_RETURNBACK)))) {
                    po = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial);
                    //获取变形Po
                    if(isChange && (serial.contains(AHU_PRODUCT_39XT) || serial.contains(AHU_PRODUCT_39G))){
                        po = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial.replace(AHU_PRODUCT_39XT,AHU_PRODUCT_39CQ).replace(AHU_PRODUCT_39G,AHU_PRODUCT_39CQ));
                        if (null == po) {
                            po.setSerial(serial);
                        }
                    }
                }
            }else if (SectionTypeEnum.TYPE_DISCHARGE == sectionType) {//出风段，后回风开启,添加端面中间面
                if (metaId.equals(metaId1)
                        && JSON_MIX_OUTTYPE_A.equals(String.valueOf(sectionMap.get(METASEXON_DISCHARGE_OUTLETDIRECTION)))) {
                    po = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial);
                    //获取变形Po
                    if(isChange && (serial.contains(AHU_PRODUCT_39XT) || serial.contains(AHU_PRODUCT_39G))){
                        po = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial.replace(AHU_PRODUCT_39XT,AHU_PRODUCT_39CQ).replace(AHU_PRODUCT_39G,AHU_PRODUCT_39CQ));
                        if (null == po) {
                            po.setSerial(serial);
                        }
                    }
                }
            }else if(SectionTypeEnum.TYPE_FAN == sectionType) {//风机段excel中间面切割
                boolean hasFanMiddlePanel = false;

                if(coupleSections.size()>1) {//风机单独分段没有中间面
                    //无蜗壳在分段边上才有有中间面
                    if(serialCode == 0 || serialCode==coupleSections.size()-1){
                        if(JSON_FAN_OUTLET_WWK.equals(sectionMap.get(METASEXON_FAN_OUTLET))){
                            hasFanMiddlePanel = true;
                        }

                        if(!JSON_FAN_OUTLET_WWK.equals(sectionMap.get(METASEXON_FAN_OUTLET))
                                && serialCode == 0){//普通风机出风方向右：也有中间面 TODO
                            hasFanMiddlePanel = true;
                        }
                    }else{
                        //分段中间所有风机都有中间面
                        hasFanMiddlePanel = true;
                    }
                }

                if(hasFanMiddlePanel){
                    if(serial.contains(AHU_PRODUCT_39XT)){
                        po = cut39XTCommonSizePanelPO(""+paneId, serial, partition, createPanelFace(SYS_BLANK+paneId, partition));
                    }else{
                        po = getFanPanelXSLXPO(serial, ahuParaMap, sectionMap, sectionL,""+paneId);
                    }
                }
            }else if(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER == sectionType){
                po = ExcelForPanelUtils.getInstance().getSideCombinedMixingChamberFivePanelPanel(serial);
            }

            /* 通用中间面切割、新回排中间面切割 */
            if (null != po || SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER == sectionType) {
                PanelFace panelFace = null;

                PanelFace panelFaceFull = null;

                //生成新回排 前面的空面板 (单独分段不生成)
                if(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER == sectionType && coupleSections.size()>1){
                    String cMixPanelId = SYS_BLANK;

                    if(coupleSections.size()>1 && serialCode == 0){//新回排 端面切割重写到到id:5的左端面
                        cMixPanelId = SYS_PANEL_FRONT;
                    } else if(coupleSections.size()>1 && serialCode==coupleSections.size()-1){//新回排 端面切割重写到到id:6的右端面
                        cMixPanelId = SYS_PANEL_BACK;
                    } else{
                        cMixPanelId = SYS_BLANK+paneId;//中间位置的新回排，需要使用大于5的动态paneId
                        paneId++;
                    }

                    panelFace = createPanelFace(SYS_BLANK+cMixPanelId, partition);
                    panelFaceFull = generateFullMiddleView(panelFace,po,SYS_PANEL_TYPE_WU,serial);
                    panels.put(SYS_BLANK+cMixPanelId,panelFaceFull);
                }

                if(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER == sectionType){
                    panelFace = createPanelFace(SYS_BLANK + paneId, partition);
                    doProcessPanel(po, panelFace,serial);
                    if(serial.contains(SYS_UNIT_SERIES_39XT)){
                        panels.put(SYS_BLANK + paneId,panelFace);
                    }else{
                        //JSON 预先切割模式
                        File jsonForPanel = new File(SysConstants.ASSERT_DIR +
                                MessageFormat.format(FileNamesLoadInSystem.PANEL_CQ_G_MIDDLE_PANEL_XLSX, serial.substring(serial.length() - 4)));
                        if (jsonForPanel.exists()) {
                            try {
                                FileReader fileReader = new FileReader(jsonForPanel);
                                String result = fileReader.readString();
                                result = result.replace(SYS_PANEL_JSONSPLIT_ManualID,SYS_BLANK + paneId);

                                Gson gson = new Gson();
                                panelFace = gson.fromJson(result, new TypeToken<PanelFace>() {}.getType());
                            } catch (Exception e) {
                                log.error("read error :"+jsonForPanel.getPath(),e);
                            }

                        }
                        panels.put(SYS_BLANK + paneId,panelFace);
                    }
                    paneId++;
                }else {//通用中间面切割
                    panelFace = createPanelFace(SYS_BLANK+paneId, partition);
                    doProcessPanel(po, panelFace, serial);
                    panels.put(SYS_BLANK + paneId, panelFace);
                    paneId++;
                }

                //生成新回排 后面的空面板 (单独分段不生成)
                if(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER == sectionType && coupleSections.size()>1){
                    panelFace = createPanelFace(SYS_BLANK+paneId, partition);
                    panelFaceFull = generateFullMiddleView(panelFace,po,SYS_PANEL_TYPE_WU, serial);
                    panels.put(SYS_BLANK+paneId,panelFaceFull);
                    paneId++;
                }

            }
            serialCode++;
            sectionsStepIndex++;

        }

    }

    /**
     * 处理切割细节
     * @param po
     * @param panelFace
     * @param serial
     */
    private static void doProcessPanel(PanelXSLXPO po, PanelFace panelFace, String serial) {
        genPanelFaceFromPO(panelFace, po);
        // 封装叶子面板为树状结构
        slicePanelByBitreeRule(panelFace, serial);
    }

    /**
     * 生成整张面板的切割结果
     * @param panelFace
     * @param po
     * @param pType
     * @param serial
     * @return
     */
    private static PanelFace generateFullMiddleView(PanelFace panelFace, PanelXSLXPO po, int pType, String serial) {
        try {
            PanelFace panelFaceTemp = new PanelFace();
            BeanUtils.copyProperties(panelFaceTemp,panelFace);
            PanelXSLXPO poTemp = new PanelXSLXPO();
            if(EmptyUtil.isNotEmpty(po)) {
                BeanUtils.copyProperties(poTemp, po);
            }else{
                poTemp.setSerial(serial);
                poTemp.setH(SYS_BLANK+panelFace.getPanelWidth());
                poTemp.setW(SYS_BLANK+panelFace.getPanelLength());
            }
            poTemp.setWr(SYS_BLANK+panelFace.getPanelLength());
            poTemp.setWp(SYS_BLANK+pType);
            poTemp.setHr(SYS_BLANK+panelFace.getPanelWidth());
            poTemp.setHp(SYS_BLANK+pType);

            doProcessPanel(poTemp, panelFaceTemp, serial);
            return panelFaceTemp;
        } catch (Exception e) {
            log.warn(MessageFormat.format("面板初始化-中间面正整张面板 panelFace:[{0}]", String.valueOf(panelFace)));
            return null;
        }
    }

    /**
     * 生成整张面板的切割PO
     *
     * @param sectionL
     * @param panelFace
     * @param pType
     * @return
     */
    private static PanelXSLXPO generateFullPanel(Integer sectionL, PanelFace panelFace, int pType, String sectionDoorOpendAndViewport) {
        try {
            PanelXSLXPO poTemp = new PanelXSLXPO();
            poTemp.setH(SYS_BLANK+panelFace.getPanelWidth());
            poTemp.setW(SYS_BLANK+sectionL);
            poTemp.setWr(SYS_BLANK+sectionL);
            poTemp.setWp(SYS_BLANK+pType);
            poTemp.setHr(SYS_BLANK+panelFace.getPanelWidth());
            poTemp.setHp(SYS_BLANK+pType);
            poTemp.setSectionDoorOpendAndViewport(sectionDoorOpendAndViewport);
            return poTemp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 操作面，调用数据库逻辑
     * @param serial
     * @param isChange 是否变形
     * @param partition
     * @param panelFace
     * @param isPipeOrientation
     * @param doororientationStr
     */
    public static void cutBigOperatePanel(String face, String serial, boolean isChange, AhuPartition partition, PanelFace panelFace, Boolean isPipeOrientation, String doororientationStr) {
        LinkedList<Map<String, Object>> coupleSections = partition.getCoupleSections();
        int size = coupleSections.size();
        Map<String, Object> ahuParaMap = partition.getAhuParameters();
        Integer[] vs = new Integer[size];
        Integer[] vTypes = new Integer[size];
        String[] vSectionTypes = new String[size];
        Integer[] hs = new Integer[] { panelFace.getPanelWidth() };
        Integer[] hTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };

        Map<String, PanelXSLXPO> poMap = new HashMap<>();
        int serialCode = 0;
        int sectionsStepIndex = 0;//（由于coupleSections 有段合并情况）需要记录partition.getSections() 的index 保证和 coupleSections index 同步
        for (Map<String, Object> coupleSection : coupleSections) {
            Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
            vs[serialCode] = AhuUtil.getMoldSize(sectionL);

            String metaId = MapValueUtils.getStringValue(AhuPartition.C_MKEY_METAID, coupleSection);
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);
            if (null == sectionType) {
                System.out.println("在解析分段数据时候出错了");
                sectionType = SectionTypeEnum.getSectionTypeFromId(metaId.split(SYS_PUNCTUATION_LOW_HYPHEN)[0]);
                System.out.println("在解析分段数据时候出错了"+sectionType);
            }
            if(metaId.contains(SYS_PUNCTUATION_LOW_HYPHEN)){
                sectionsStepIndex++;
            }
            vSectionTypes[serialCode] = metaId;
            Map<String, Object> partMap = partition.getSections().get(sectionsStepIndex);
            Map<String, String> sectionMap = new HashMap<>();
            String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
            sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));

            //开门观察窗Has
            String sectionDoorOpendAndViewport = CommonConstant.SYS_BLANK;
            sectionDoorOpendAndViewport = getDoorOpenAndViewport(sectionType, sectionMap);

            //混合段，左回风开启,操作面为无面板，右回风开启,非操作面为无面板
            if (SectionTypeEnum.TYPE_MIX == sectionType){
                String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));
                if(metaId.equals(metaId1)
                        &&
                        ((SYS_ASSERT_TRUE.equals(String.valueOf(sectionMap.get(METASEXON_MIX_RETURNLEFT))) && face.equals(SYS_PANEL_LEFT)) ||
                          (SYS_ASSERT_TRUE.equals(String.valueOf(sectionMap.get(METASEXON_MIX_RETURNRIGHT))) && face.equals(SYS_PANEL_RIGHT))
                        )
                        ){
                    vTypes[serialCode] = SYS_PANEL_TYPE_WU;
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, null);//不再进行数据库逻辑分割
                    sectionsStepIndex++;
                    serialCode++;
                    continue;
                }
            }
            //出风段，左回风开启,操作面为无面板
            if (SectionTypeEnum.TYPE_DISCHARGE == sectionType){
                String outletDirection = String.valueOf(sectionMap.get(METASEXON_DISCHARGE_OUTLETDIRECTION));
                String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));
                if(metaId.equals(metaId1) &&
                        ((face.equals(SYS_PANEL_LEFT) && JSON_MIX_OUTTYPE_L.equals(outletDirection)) ||
                                (face.equals(SYS_PANEL_RIGHT) && JSON_MIX_OUTTYPE_R.equals(outletDirection))
                        )
                        ){
                    vTypes[serialCode] = SYS_PANEL_TYPE_WU;
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, null);//不再进行数据库逻辑分割
                    sectionsStepIndex++;
                    serialCode++;
                    continue;
                }
            }
            vTypes[serialCode] = SYS_PANEL_TYPE_PUTONG;
            String typeNo = MapValueUtils.getStringValue(AhuPartition.C_MKEY_CODE, coupleSection);
            while (typeNo.startsWith("0")) {
                typeNo = typeNo.substring(1);
            }

            PanelXSLXPO po = ExcelForPanelUtils.getInstance().getBigOperatePanel(serial, typeNo,
                    String.valueOf(sectionL));
            //获取变形Po
            if(isChange){
                po = ExcelForPanelUtils.getInstance().getBigOperatePanelByH(serial,typeNo,String.valueOf(sectionL));
            }

            //接管方向和当前面一致 39G盘管段为外面板、39CQ/39G使用操作面切割excel/不一致使用非操作面excel
            if((face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT))
                    && (SectionTypeEnum.TYPE_COLD == sectionType
                    || SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL == sectionType
                    || SectionTypeEnum.TYPE_HEATINGCOIL == sectionType
                    || SectionTypeEnum.TYPE_STEAMCOIL == sectionType
                    || SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL == sectionType)
                    ){
                if(!isPipeOrientation)
                    po = ExcelForPanelUtils.getInstance().getNoBigOperatePanel(serial, typeNo,
                            String.valueOf(sectionL));
                if(isPipeOrientation && serial.contains(SYS_UNIT_SERIES_39G)) {//
                    po = getPanelTypeOfPo(SYS_PANEL_TYPE_WAI, po);
                }
            }else if((face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT))
                    &&  (SectionTypeEnum.TYPE_STEAMHUMIDIFIER == sectionType
                            || SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER == sectionType
                        )){
                if(isPipeOrientation) {//接管方向和当前面一致干蒸电极加湿段为加湿面板
                    po = getPanelTypeOfPo(SYS_PANEL_TYPE_JIASHI, po);
                }
            }

            //混合段里面的开门方向和面相反，方法体操作面切割改为普通面切割
            if(SectionTypeEnum.TYPE_MIX == sectionType){
                String directionMixDoor = String.valueOf(sectionMap.get(METASEXON_MIX_DOORDIRECTION));
                if((directionMixDoor.equals(MIX_DOORDIRECTION_LEFT) && face.equals(SYS_PANEL_RIGHT) )
                        || (directionMixDoor.equals(MIX_DOORDIRECTION_RIGHT) && face.equals(SYS_PANEL_LEFT)))
                    po = ExcelForPanelUtils.getInstance().getNoBigOperatePanel(serial, typeNo,
                            String.valueOf(sectionL));
            }


            if (null == po) {
                log.warn(MessageFormat.format("面板初始化-数据库逻辑-查询数据库元数据失败异常 serial:[{0}],typeNo:[{1}],sectionL:[{2}]",
                        serial, typeNo, String.valueOf(sectionL)));
            }else{
                po.setSectionMetaId(metaId);
                po.setSectionDoorOpendAndViewport(sectionDoorOpendAndViewport);
            }

            //TODO 暂时去掉“机组铭牌修改：请修改39G机组门面板属性里的“铭牌门”为普通门。” 吴千群认为需要通过 页面修改为名牌们不用代码处理。
            //送风机铭牌门（39G），回风机普通门
            /*String airDirection = sectionMap.get(METASEXON_AIRDIRECTION);
            if (SectionTypeEnum.TYPE_FAN == sectionType
                    && serial.contains(SYS_UNIT_SERIES_39G)
                    && AirDirectionEnum.SUPPLYAIR.getCode().equals(airDirection)) {//送风机组
                getPanelTypeOfPo(SYS_PANEL_TYPE_MEN,SYS_PANEL_TYPE_MEN_MING,po);
            }*/

            poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);
            serialCode++;
            sectionsStepIndex++;
        }
        slicePanelByVerticalFirstRule(panelFace, hs, hTypes, null, null, vs, vTypes, vSectionTypes);
        slicePanelByExcelPo(panelFace, poMap);
        // 封装叶子面板为树状结构
        slicePanelByBitreeRule(panelFace, serial);
    }
    public static String getDoorOpenAndViewport(SectionTypeEnum sectionType, Map<String, String> sectionMap) {
        String sectionDoorOpendAndViewport;
        if (SectionTypeEnum.TYPE_MIX == sectionType
                && !MIX_DOORO_NODOOR.equals(sectionMap.get(METASEXON_MIX_DOORO))) {//混合段开门
            sectionDoorOpendAndViewport = "true";

            if(MIX_DOORO_DOORWITHVIEWPORT.equals(sectionMap.get(METASEXON_MIX_DOORO))){//有窗
                sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"true";
            }else{
                sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"false";
            }

        } else if (SectionTypeEnum.TYPE_FAN == sectionType) {//风机段肯定开门
            sectionDoorOpendAndViewport = "true";

            if(FAN_ACCESSDOOR_DOORWITHWINDOW.equals(sectionMap.get(METASEXON_FAN_ACCESSDOOR))){//有窗
                sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"true";
            }else{
                sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"false";
            }
        } else if (SectionTypeEnum.TYPE_ACCESS == sectionType
                && !ACCESS_ODOOR_W_O_DOOR.equals(sectionMap.get(METASEXON_ACCESS_ODOOR))) {//空段开门
            sectionDoorOpendAndViewport = "true";

            if(ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(sectionMap.get(METASEXON_ACCESS_ODOOR))){//有窗
                sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"true";
            }else{
                sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"false";
            }
        } else if (SectionTypeEnum.TYPE_DISCHARGE == sectionType
                && !DISCHARGE_ODOOR_ND.equals(sectionMap.get(METASEXON_DISCHARGE_DISCHARGEODOOR))) {//出风段开门
            sectionDoorOpendAndViewport = "true";

            if(DISCHARGE_ODOOR_DY.equals(sectionMap.get(METASEXON_DISCHARGE_DISCHARGEODOOR))){//有窗
                sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"true";
            }else{
                sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"false";
            }
        } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER == sectionType
                && (!COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_ISDOOR)) //新回排段新风侧开门
                || !COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_OSDOOR))//新回排段回风侧开门
        )
                ) {
            //新回排：回风侧（左）、新风侧（右）
            String OSDOOR_DOOR = "false";
            String OViewPort = SYS_PUNCTUATION_SEMICOLON+"false";
            String ISDOOR_DOOR = "false";
            String IViewPort = SYS_PUNCTUATION_SEMICOLON+"false";
            if(!COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_OSDOOR))){
                OSDOOR_DOOR = "true";

                if(COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_OSDOOR))){//有窗
                    OViewPort = SYS_PUNCTUATION_SEMICOLON+"true";
                }
            }
            if(!COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_ISDOOR))){
                ISDOOR_DOOR = "true";

                if(COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_ISDOOR))){//有窗
                    IViewPort= SYS_PUNCTUATION_SEMICOLON+"true";
                }
            }
            sectionDoorOpendAndViewport = OSDOOR_DOOR + OViewPort
                    +SYS_PUNCTUATION_COMMA
                    +ISDOOR_DOOR + IViewPort;
        } else {
            sectionDoorOpendAndViewport = "true" + SYS_PUNCTUATION_SEMICOLON + "false";//页面没有开门属性的开门默认使用excel 的情况
        }
        return sectionDoorOpendAndViewport;
    }

    /**
     * XT系列操作面、非操作面切割
     * @param face
     * @param serial
     * @param partition
     * @param panelFace
     * @param doororientationStr
     */
    public static void cutXTBigNoBigOperatePanel(String face, String serial, AhuPartition partition, PanelFace panelFace, String doororientationStr) {
        LinkedList<Map<String, Object>> coupleSections = partition.getCoupleSections();
        int size = coupleSections.size();
        Map<String, Object> ahuParaMap = partition.getAhuParameters();
        Integer[] vs = new Integer[size];
        Integer[] vTypes = new Integer[size];
        String[] vSectionTypes = new String[size];
        Integer[] hs = new Integer[] { panelFace.getPanelWidth() };
        Integer[] hTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };

        Map<String, PanelXSLXPO> poMap = new HashMap<>();
        int serialCode = 0;
        int sectionsStepIndex = 0;//（由于coupleSections 有段合并情况）需要记录partition.getSections() 的index 保证和 coupleSections index 同步
        for (Map<String, Object> coupleSection : coupleSections) {
            Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
            sectionL = AhuUtil.getMoldSize(sectionL);
            vs[serialCode] = sectionL;

            String metaId = MapValueUtils.getStringValue(AhuPartition.C_MKEY_METAID, coupleSection);
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);
            if (null == sectionType) {
                System.out.println("在解析分段数据时候出错了");
                sectionType = SectionTypeEnum.getSectionTypeFromId(metaId.split(SYS_PUNCTUATION_LOW_HYPHEN)[0]);
                System.out.println("在解析分段数据时候出错了"+sectionType);
            }
            if(metaId.contains(SYS_PUNCTUATION_LOW_HYPHEN)){
                sectionsStepIndex++;
            }
            vSectionTypes[serialCode] = metaId;

            //混合段、风机段、新回排风段、空段、出风段 开门，需要门面板
            Map<String, Object> partMap = partition.getSections().get(sectionsStepIndex);
            Map<String, String> sectionMap = new HashMap<>();
            String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
            sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));

            String sectionDoorOpendAndViewport = CommonConstant.SYS_BLANK;
            String directionMixDoor = String.valueOf(sectionMap.get(METASEXON_MIX_DOORDIRECTION));
            String directionDischargeDoor = String.valueOf(sectionMap.get(METASEXON_DISCHARGE_DOORDIRECTION));
            String doorOnBothSide = BaseDataUtil.constraintString(sectionMap.get(UtilityConstant.METASEXON_FAN_DOORONBOTHSIDE));//两侧开门
            String doorOnBothSideAccess = BaseDataUtil.constraintString(sectionMap.get(UtilityConstant.METASEXON_ACCESS_DOORONBOTHSIDE));//两侧开门
            if (SectionTypeEnum.TYPE_MIX == sectionType
                    && !MIX_DOORO_NODOOR.equals(sectionMap.get(METASEXON_MIX_DOORO))
                    && (
                    ((directionMixDoor.equals(MIX_DOORDIRECTION_LEFT) && face.equals(SYS_PANEL_LEFT) )
                            || (directionMixDoor.equals(MIX_DOORDIRECTION_RIGHT) && face.equals(SYS_PANEL_RIGHT)))
                            ||
                            (MIX_DOORDIRECTION_BOTH.equals(directionMixDoor)) && (face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT))
            )
                    ) {//混合段里面的开门方向和所在面相同(或混合段左右开门)，需要门面板
                vTypes[serialCode] = SYS_PANEL_TYPE_MEN;
                sectionDoorOpendAndViewport = "true";

                if(MIX_DOORO_DOORWITHVIEWPORT.equals(sectionMap.get(METASEXON_MIX_DOORO))){//有窗
                    sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"true";
                }else{
                    sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"false";
                }
                //添加切割对象
                poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode,
                        generateFullPanel(sectionL,panelFace,SYS_PANEL_TYPE_MEN,sectionDoorOpendAndViewport));
            } else if (SectionTypeEnum.TYPE_FAN == sectionType
                    && (    (doororientationStr.equals(AHU_DOORORIENTATION_LEFT) && face.equals(SYS_PANEL_LEFT) )
                    || (doororientationStr.equals(AHU_DOORORIENTATION_RIGHT) && face.equals(SYS_PANEL_RIGHT))
                    || ("true".equals(doorOnBothSide) && (face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT)))
            )
                    ) {//风机段里面的开门方向(既是AHU开门方向)和所在面相同(或者风机左右双侧开门)，需要门面板
                vTypes[serialCode] = SYS_PANEL_TYPE_MEN;
                sectionDoorOpendAndViewport = "true";

                if(FAN_ACCESSDOOR_DOORWITHWINDOW.equals(sectionMap.get(METASEXON_FAN_ACCESSDOOR))){//有窗
                    sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"true";
                }else{
                    sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"false";
                }
                //添加切割对象
                poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode,
                        generateFullPanel(sectionL, panelFace,SYS_PANEL_TYPE_MEN,sectionDoorOpendAndViewport));
            } else if (SectionTypeEnum.TYPE_ACCESS == sectionType
                    && !ACCESS_ODOOR_W_O_DOOR.equals(sectionMap.get(METASEXON_ACCESS_ODOOR))
                    && (    (doororientationStr.equals(AHU_DOORORIENTATION_LEFT) && face.equals(SYS_PANEL_LEFT) )
                    || (doororientationStr.equals(AHU_DOORORIENTATION_RIGHT) && face.equals(SYS_PANEL_RIGHT))
                    || ("true".equals(doorOnBothSideAccess) && (face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT)))
            )
                    ) {//空段里面的开门方向(既是AHU开门方向)和所在面相同(或者空段左右双侧开门)，需要门面板
                vTypes[serialCode] = SYS_PANEL_TYPE_MEN;
                sectionDoorOpendAndViewport = "true";

                if(ACCESS_ODOOR_DOOR_W__VIEWPORT.equals(sectionMap.get(METASEXON_ACCESS_ODOOR))){//有窗
                    sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"true";
                }else{
                    sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"false";
                }
                //添加切割对象
                poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode,
                        generateFullPanel(sectionL, panelFace,SYS_PANEL_TYPE_MEN,sectionDoorOpendAndViewport));
            } else if (SectionTypeEnum.TYPE_DISCHARGE == sectionType
                    && !DISCHARGE_ODOOR_ND.equals(sectionMap.get(METASEXON_DISCHARGE_DISCHARGEODOOR))
                    && (
                    (((directionDischargeDoor.equals(DISCHARGE_DOORDIRECTION_LEFT) && face.equals(SYS_PANEL_LEFT) )
                            || (directionDischargeDoor.equals(DISCHARGE_DOORDIRECTION_RIGHT) && face.equals(SYS_PANEL_RIGHT)))
                            ||
                            (DISCHARGE_DOORDIRECTION_BOTH.equals(directionDischargeDoor)) && (face.equals(SYS_PANEL_LEFT)
                                    || face.equals(SYS_PANEL_RIGHT)))
            )
                    ) {//出风段里面的开门方向和所在面相同(或出风段左右开门)，出风段开门
                vTypes[serialCode] = SYS_PANEL_TYPE_MEN;
                sectionDoorOpendAndViewport = "true";

                if(DISCHARGE_ODOOR_DY.equals(sectionMap.get(METASEXON_DISCHARGE_DISCHARGEODOOR))){//有窗
                    sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"true";
                }else{
                    sectionDoorOpendAndViewport += SYS_PUNCTUATION_SEMICOLON+"false";
                }
                //添加切割对象
                poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode,
                        generateFullPanel(sectionL, panelFace,SYS_PANEL_TYPE_MEN,sectionDoorOpendAndViewport));
            } else if (SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER == sectionType
                    && (!COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_ISDOOR)) //新回排段新风侧开门
                    || !COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_OSDOOR))//新回排段回风侧开门
            )
                    && ((doororientationStr.equals(AHU_DOORORIENTATION_LEFT) && face.equals(SYS_PANEL_LEFT) )
                    || (doororientationStr.equals(AHU_DOORORIENTATION_RIGHT) && face.equals(SYS_PANEL_RIGHT))
            )
                    ) {
                //新回排：回风侧（左）、新风侧（右）均等切割为两个面板；
                vTypes[serialCode] = SYS_PANEL_TYPE_MEN;
                //新建切割对象
                PanelXSLXPO po = new PanelXSLXPO();
                po.setH(SYS_BLANK+panelFace.getPanelWidth());
                po.setW(SYS_BLANK+panelFace.getPanelLength());
                Integer[] theVerticals = calculate2Pre(sectionL);
                po.setWr(array2SplitStr(theVerticals));
                StringBuffer wp = new StringBuffer(SYS_BLANK);
                for (int i = 0; i < theVerticals.length; i++) {

                    if(i==0 && !COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_OSDOOR))){
                        wp.append(SYS_PANEL_TYPE_MEN);
                    }else if(i==1 && !COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_ISDOOR))){
                        wp.append(SYS_PANEL_TYPE_MEN);
                    }else{
                        wp.append(SYS_PANEL_TYPE_PUTONG);
                    }
                    if(i<theVerticals.length-1)
                        wp.append(SYS_PUNCTUATION_PLUS);
                }
                po.setWp(wp.toString());
                po.setHr(String.valueOf(panelFace.getPanelWidth()));
                po.setHp(String.valueOf(SYS_PANEL_TYPE_PUTONG));

                //新回排：回风侧（左）、新风侧（右）
                String OSDOOR_DOOR = "false";
                String OViewPort = SYS_PUNCTUATION_SEMICOLON+"false";
                String ISDOOR_DOOR = "false";
                String IViewPort = SYS_PUNCTUATION_SEMICOLON+"false";
                if(!COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_OSDOOR))){
                    OSDOOR_DOOR = "true";

                    if(COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_OSDOOR))){//有窗
                        OViewPort = SYS_PUNCTUATION_SEMICOLON+"true";
                    }
                }
                if(!COMBINEDMIXINGCHAMBER_OSDOOR_ND.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_ISDOOR))){
                    ISDOOR_DOOR = "true";

                    if(COMBINEDMIXINGCHAMBER_OSDOOR_DY.equals(sectionMap.get(METASEXON_COMBINEDMIXINGCHAMBER_ISDOOR))){//有窗
                        IViewPort= SYS_PUNCTUATION_SEMICOLON+"true";
                    }
                }
                sectionDoorOpendAndViewport = OSDOOR_DOOR + OViewPort
                        +SYS_PUNCTUATION_COMMA
                        +ISDOOR_DOOR + IViewPort;

                //添加切割对象
                po.setSectionDoorOpendAndViewport(sectionDoorOpendAndViewport);
                poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);

            } else {
                vTypes[serialCode] = SYS_PANEL_TYPE_PUTONG;

                //新建切割对象
                PanelXSLXPO po = new PanelXSLXPO();
                po.setH(SYS_BLANK+panelFace.getPanelWidth());
                po.setW(SYS_BLANK+panelFace.getPanelLength());
                Integer[] theVerticals = calculatePanelSliceUnitsNoPre(sectionL, AhuPartitionGenerator.XT_PREFER_PANEL_LENGTH);
                po.setWr(array2SplitStr(theVerticals));
                Integer[] vPartTypes = new Integer[vs.length];
                for (int i = 0; i < vPartTypes.length; i++) {
                    vPartTypes[i] = SYS_PANEL_TYPE_PUTONG;
                }
                po.setWp(array2SplitStr(vPartTypes));
                po.setHr(String.valueOf(panelFace.getPanelWidth()));
                po.setHp(String.valueOf(SYS_PANEL_TYPE_PUTONG));
                sectionDoorOpendAndViewport = "true" + SYS_PUNCTUATION_SEMICOLON + "false";//页面没有开门属性的开门默认使用excel
                //添加切割对象
                po.setSectionDoorOpendAndViewport(sectionDoorOpendAndViewport);
                poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);
            }
            serialCode++;
            sectionsStepIndex++;
        }

        slicePanelByVerticalFirstRule(panelFace, hs, hTypes, null, null, vs, vTypes,vSectionTypes);
        slicePanelByExcelPo(panelFace, poMap);
        // 封装叶子面板为树状结构
        slicePanelByBitreeRule(panelFace, serial);
    }
    /**
     * 39XT顶、底面板切割
     * @param face
     * @param serial
     * @param partition
     * @param panelFace
     */
    public static void cut39XTTopBottomPanel(String face, String serial, AhuPartition partition, PanelFace panelFace) {
        LinkedList<Map<String, Object>> coupleSections = partition.getCoupleSections();

        /**
         * 1：对 (混合段) 及 (其他段面板merge后长度) 整合
         *
         * 如果混合段，顶、底出风进行特殊处理（面板处理为空面板）
         * 格式：
         * stepByMix            : 长度
         * stepByMix + ahu.mix  : 长度
         **/
        LinkedHashMap<String,Integer> mergedPanelLen = new LinkedHashMap<String,Integer>();
        Map<String,Map<String,String>> mergedPanelLenParams = new HashMap<>();
        int stepBySpecial = 1;
        int sectionsStepIndex = 0;
        boolean hasSpecialReturnTopRight = false;
        for (Map<String, Object> coupleSection : coupleSections) {
            String metaId = MapValueUtils.getStringValue(AhuPartition.C_MKEY_METAID, coupleSection);
            if(metaId.contains(SYS_PUNCTUATION_LOW_HYPHEN)){
                sectionsStepIndex++;
            }

            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);
            Map<String, Object> partMap = partition.getSections().get(sectionsStepIndex);
            Map<String, String> sectionMap = new HashMap<>();
            String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
            sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));

            boolean isSpecial = false;
            //1: 混合段、出风段顶部、底出风不进行分割;
            if(SectionTypeEnum.TYPE_MIX == sectionType){
                String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));

                String top = String.valueOf(sectionMap.get(METASEXON_MIX_RETURNTOP));//顶部出风
                String bootom = String.valueOf(sectionMap.get(METASEXON_MIX_RETURNBUTTOM));//底出风
                if(metaId.equals(metaId1) &&
                        ((SYS_ASSERT_TRUE.equals(top) && face.equals(SYS_PANEL_TOP))
                                || (SYS_ASSERT_TRUE.equals(bootom) && face.equals(SYS_PANEL_BOTTOM)))){
                    isSpecial = true;
                }
            }
            if(SectionTypeEnum.TYPE_DISCHARGE == sectionType){
                String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));

                String outletDirection = String.valueOf(sectionMap.get(METASEXON_DISCHARGE_OUTLETDIRECTION));//顶、底出风
                if(metaId.equals(metaId1) &&
                        ((JSON_MIX_OUTTYPE_T.equals(outletDirection) && face.equals(SYS_PANEL_TOP))
                                || (JSON_MIX_OUTTYPE_F.equals(outletDirection) && face.equals(SYS_PANEL_BOTTOM)))){
                    isSpecial = true;
                }
            }
            //3: 顶部新回排段特殊处理 整个无面板
            if(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER == sectionType && face.equals(SYS_PANEL_TOP)){
                isSpecial = true;
            }
            //4: 转轮热回收段，上层底面板下层顶面板：为无面板
            if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE == sectionType &&
                    (
                            (partition.isTopLayer()  && face.equals(SYS_PANEL_BOTTOM))
                                    ||
                                    (!partition.isTopLayer() && face.equals(SYS_PANEL_TOP))
                    )
                    ){
                isSpecial = true;
            }
            if(isSpecial){
                Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
                mergedPanelLen.put(sectionType.getId()+stepBySpecial,sectionL);
                sectionsStepIndex++;
                stepBySpecial++;
                hasSpecialReturnTopRight = true;
                continue;
            }

            //5: 被特殊切割段 隔开的其他分段按照一个面板进行切割。
            Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
            int len = AhuUtil.getMoldSize(sectionL);
            if(len>0) {
                if (mergedPanelLen.containsKey(String.valueOf(stepBySpecial))) {
                    mergedPanelLen.put(SYS_BLANK + stepBySpecial, mergedPanelLen.get(SYS_BLANK + stepBySpecial) + len);
                } else {
                    mergedPanelLen.put(SYS_BLANK + stepBySpecial, len);
                }
            }
            sectionsStepIndex++;
        }

        /**
         * 2：混合段包含顶面、非操作面出风进行特殊处理
         **/

        if(hasSpecialReturnTopRight) {
            List<Integer> vsList = new ArrayList<Integer>();
            List<Integer> vsTypesList = new ArrayList<Integer>();

            Integer[] vs , vTypes;
            Integer[] hs = new Integer[] { panelFace.getPanelWidth() };
            Integer[] hTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };
            Map<String, PanelXSLXPO> poMap = new HashMap<>();

            Iterator iter = mergedPanelLen.entrySet().iterator();
            int serialCode = 0;
            while (iter.hasNext()) {

                Map.Entry entry = (Map.Entry) iter.next();
                String key = String.valueOf(entry.getKey());
                Integer val = NumberUtil.convertStringToDoubleInt(entry.getValue().toString());

                Integer sectionL = val;
                int len = AhuUtil.getMoldSize(sectionL);
                vsList.add(len);
                if(key.contains(SectionTypeEnum.TYPE_MIX.getId()) || key.contains(SectionTypeEnum.TYPE_DISCHARGE.getId())){
                    vsTypesList.add(SYS_PANEL_TYPE_WU);
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, null);//不再进行二次切割
                    serialCode++;
                }else if(key.contains(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId())
                        || key.contains(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId())){
                    //顶部新回排段特殊处理 整个无面板
                    //转轮热回收段，上层底面板下层顶面板：为无面板

                    vsTypesList.add(SYS_PANEL_TYPE_WU);// 无面板
                    //新建切割对象
                    PanelXSLXPO po = new PanelXSLXPO();
                    po.setH(SYS_BLANK+panelFace.getPanelWidth());
                    po.setW(SYS_BLANK+panelFace.getPanelLength());
                    po.setWr(String.valueOf(len));
                    po.setWp(String.valueOf(SYS_PANEL_TYPE_WU));
                    po.setHr(SYS_BLANK+panelFace.getPanelWidth());
                    po.setHp(SYS_BLANK+SYS_PANEL_TYPE_WU);
                    //添加切割对象
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);
                    serialCode++;
                }else{
                    vsTypesList.add(SYS_PANEL_TYPE_PUTONG);
                    //新建切割对象
                    PanelXSLXPO po = new PanelXSLXPO();
                    po.setH(SYS_BLANK+panelFace.getPanelWidth());
                    po.setW(SYS_BLANK+panelFace.getPanelLength());
                    Integer[] theVerticals = calculatePanelSliceUnitsNoPre(len, AhuPartitionGenerator.XT_PREFER_PANEL_LENGTH);
                    po.setWr(array2SplitStr(theVerticals));
                    StringBuffer wp = new StringBuffer(SYS_BLANK);
                    for (int i = 0; i < theVerticals.length; i++) {
                        wp.append(SYS_PANEL_TYPE_PUTONG);// 面板类型普通面板
                        if(i<theVerticals.length-1)
                            wp.append(SYS_PUNCTUATION_PLUS);
                    }
                    po.setWp(wp.toString());
                    po.setHr(String.valueOf(panelFace.getPanelWidth()));
                    po.setHp(String.valueOf(SYS_PANEL_TYPE_PUTONG));
                    //添加切割对象
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);
                    serialCode++;
                }
            }

            vs = (Integer[]) vsList.toArray(new Integer[0]);
            vTypes = (Integer[]) vsTypesList.toArray(new Integer[0]);

            slicePanelByVerticalFirstRule(panelFace, hs, hTypes, null, null, vs, vTypes, null);
            slicePanelByExcelPo(panelFace, poMap);
            // 封装叶子面板为树状结构
            slicePanelByBitreeRule(panelFace, serial);
        }else {
            /**
             * 3： 没有特殊逻辑： 顶面、底面，整个面板采用逻辑切割
             */
            cut39XTCommonSizePanel(face, serial, partition, panelFace);
        }
    }

    /**
     * 39XT面板通用逻辑切割
     * @param face
     * @param serial
     * @param partition
     * @param panelFace
     */
    public static void cut39XTCommonSizePanel(String face, String serial, AhuPartition partition, PanelFace panelFace) {

        Integer[] hs = new Integer[] { panelFace.getPanelWidth() };
        Integer[] hTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };

        int len = AhuUtil.getMoldSize(partition.getLength());
        if (face.equals(UtilityConstant.SYS_PANEL_FRONT) || face.equals(UtilityConstant.SYS_PANEL_BACK)) {
            len = panelFace.getPanelLength();
        }

        Integer[] vs = calculatePanelSliceUnitsNoPre(len, AhuPartitionGenerator.XT_PREFER_PANEL_LENGTH);
        Integer[] vTypes = new Integer[vs.length];
        for (int i = 0; i < vTypes.length; i++) {
            vTypes[i] = SYS_PANEL_TYPE_PUTONG;
        }
        slicePanelByVerticalFirstRule(panelFace, hs, hTypes, null, null, vs, vTypes, null);
        // 封装叶子面板为树状结构
        slicePanelByBitreeRule(panelFace, serial);
    }

    /**
     * 39XT面板通用逻辑切割生成excelPO
     * @param face
     * @param serial
     * @param partition
     * @param panelFace
     * @return
     */
    public static PanelXSLXPO cut39XTCommonSizePanelPO(String face, String serial, AhuPartition partition, PanelFace panelFace) {

        int len = AhuUtil.getMoldSize(partition.getLength());
        if (face.equals(UtilityConstant.SYS_PANEL_FRONT)
                || face.equals(UtilityConstant.SYS_PANEL_BACK)
                || NumberUtil.convertStringToDoubleInt(face)>=6 ) {
            len = panelFace.getPanelLength();//端面中间面使用面板宽度
        }

        //新建切割对象
        PanelXSLXPO po = new PanelXSLXPO();
        po.setH(SYS_BLANK+panelFace.getPanelWidth());
        po.setW(SYS_BLANK+panelFace.getPanelLength());
        Integer[] theVerticals = calculatePanelSliceUnitsNoPre(len, AhuPartitionGenerator.XT_PREFER_PANEL_LENGTH);
        po.setWr(array2SplitStr(theVerticals));
        StringBuffer wp = new StringBuffer(SYS_BLANK);
        for (int i = 0; i < theVerticals.length; i++) {
            wp.append(SYS_PANEL_TYPE_PUTONG);// 面板类型普通面板
            if(i<theVerticals.length-1)
                wp.append(SYS_PUNCTUATION_PLUS);
        }
        po.setWp(wp.toString());
        po.setHr(String.valueOf(panelFace.getPanelWidth()));
        po.setHp(String.valueOf(SYS_PANEL_TYPE_PUTONG));

        return po;

    }
    /**
     * 39G面板（底面板、顶面板、右面板）调用数据库逻辑
     * @param doororientationStr
     * @param face
     * @param serial
     * @param isChange
     * @param partition
     * @param panelFace
     * @param isRigthPipe
     */
    public static void cut39GDBPanel(String doororientationStr, String face, String serial, boolean isChange, AhuPartition partition, PanelFace panelFace, Boolean isRigthPipe) {
        LinkedList<Map<String, Object>> coupleSections = partition.getCoupleSections();
        int size = coupleSections.size();

        String direction = isRigthPipe?SYS_STRING_RIGHT:SYS_STRING_LEFT;

        Integer[] vs = new Integer[size];
        Integer[] vTypes = new Integer[size];
        Integer[] hs = new Integer[] { panelFace.getPanelWidth() };
        Integer[] hTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };

        Map<String, PanelXSLXPO> poMap = new HashMap<>();
        int serialCode = 0;
        int sectionsStepIndex = 0;//（由于coupleSections 有段合并情况）需要记录partition.getSections() 的index 保证和 coupleSections index 同步
        for (Map<String, Object> coupleSection : coupleSections) {
            Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
            int len = AhuUtil.getMoldSize(sectionL);
            if(0 == len){
                sectionsStepIndex++;
//                    serialCode++;//防止空段影响map 和 poMap key 对应关系
                vs = Arrays.copyOf(vs, vs.length-1);
                vTypes = Arrays.copyOf(vTypes, vTypes.length-1);
                continue;
            }

            vs[serialCode] = AhuUtil.getMoldSize(sectionL);

            String metaId = MapValueUtils.getStringValue(AhuPartition.C_MKEY_METAID, coupleSection);
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);
            if (null == sectionType) {
                System.out.println("在解析分段数据时候出错了");
                sectionType = SectionTypeEnum.getSectionTypeFromId(metaId.split(SYS_PUNCTUATION_LOW_HYPHEN)[0]);
                System.out.println("在解析分段数据时候出错了"+sectionType);
            }
            if(metaId.contains(SYS_PUNCTUATION_LOW_HYPHEN)){
                sectionsStepIndex++;
            }

            Map<String, Object> partMap = partition.getSections().get(sectionsStepIndex);
            Map<String, String> sectionMap = new HashMap<>();
            String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
            sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));
            //1混合段，出风段顶底右出风开启,面为无面板
            if (SectionTypeEnum.TYPE_MIX == sectionType || SectionTypeEnum.TYPE_DISCHARGE == sectionType){

                String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));

                String returnDirection = SYS_BLANK;
                if(face.equals(SYS_PANEL_BOTTOM)){
                    returnDirection = METASEXON_MIX_RETURNBUTTOM;
                }else if(face.equals(SYS_PANEL_TOP)){
                    returnDirection = METASEXON_MIX_RETURNTOP;
                }else if(face.equals(SYS_PANEL_RIGHT)){
                    returnDirection = METASEXON_MIX_RETURNRIGHT;
                }else if(face.equals(SYS_PANEL_LEFT)){
                    returnDirection = METASEXON_MIX_RETURNLEFT;
                }

                //出风段出风方向
                String outletDirection = String.valueOf(sectionMap.get(METASEXON_DISCHARGE_OUTLETDIRECTION));

                if (metaId.equals(metaId1)
                        &&
                        (   SYS_ASSERT_TRUE.equals(String.valueOf(sectionMap.get(returnDirection))) //混合段
                                ||
                                ((JSON_MIX_OUTTYPE_T.equals(outletDirection) && face.equals(SYS_PANEL_TOP))//出风段
                                        || (JSON_MIX_OUTTYPE_R.equals(outletDirection) && face.equals(SYS_PANEL_RIGHT))
                                        || (JSON_MIX_OUTTYPE_F.equals(outletDirection) && face.equals(SYS_PANEL_BOTTOM)))
                        )
                        ) {
                    vTypes[serialCode] = SYS_PANEL_TYPE_WU;
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, null);//不再进行数据库逻辑分割
                    sectionsStepIndex++;
                    serialCode++;
                    continue;
                }
            }

            //2:风机：顶部出风，顶部面板特殊excel切割
            if (face.equals(SYS_PANEL_TOP) && SectionTypeEnum.TYPE_FAN == sectionType) {
                vTypes[serialCode] = SYS_PANEL_TYPE_PUTONG;
                String outletDirection = sectionMap.get(METASEXON_FAN_OUTLETDIRECTION);//出风方向:顶部THF/BHF 端面（水平）UBF/UBR
                String fanMotorPosition = sectionMap.get(METASEXON_FAN_MOTORPOSITION);//电机位置:侧置side 后置back
                //顶部出风(顶面)
                if (FAN_OUTLETDIRECTION_UBF.equals(outletDirection) || FAN_OUTLETDIRECTION_UBR.equals(outletDirection)) {

                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode,
                            getFanTopPanelXSLXPO(serial,direction,AhuUtil.getMoldSize(sectionL), fanMotorPosition));//风机excel切割
                    sectionsStepIndex++;
                    serialCode++;
                    continue;
                }
            }

            //3转轮热回收段，上层底面板下层顶面板：为无面板
            if (SectionTypeEnum.TYPE_WHEELHEATRECYCLE == sectionType &&
                    (
                            (partition.isTopLayer()  && face.equals(SYS_PANEL_BOTTOM))
                                    ||
                                    (!partition.isTopLayer() && face.equals(SYS_PANEL_TOP))
                    )
                    ){

                String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));
                if (metaId.equals(metaId1)){
                    vTypes[serialCode] = SYS_PANEL_TYPE_WU;
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, null);//不再进行数据库逻辑分割
                    sectionsStepIndex++;
                    serialCode++;
                    continue;
                }
            }

            //4(非操作面、操作面)接管方向和当前面一致：盘管段为外面板,不一致为普通面板 TODO excel需要调整
            int coilWaiPanel = -1;
            if((face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT))
                    && (SectionTypeEnum.TYPE_COLD == sectionType
                    || SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL == sectionType
                    || SectionTypeEnum.TYPE_HEATINGCOIL == sectionType
                    || SectionTypeEnum.TYPE_STEAMCOIL == sectionType
                    || SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL == sectionType)
                    ){
                if (isRigthPipe){
                    coilWaiPanel = SYS_PANEL_TYPE_WAI;
                }else{
                    coilWaiPanel = SYS_PANEL_TYPE_PUTONG;
                }
            }else if((face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT))
                    && (SectionTypeEnum.TYPE_STEAMHUMIDIFIER == sectionType
                    || SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER == sectionType)
                    ){
                if (isRigthPipe){
                    coilWaiPanel = SYS_PANEL_TYPE_JIASHI;//接管方向和当前面一致干蒸电极加湿段为加湿面板
                }else{
                    coilWaiPanel = SYS_PANEL_TYPE_PUTONG;
                }
            }



            vTypes[serialCode] = SYS_PANEL_TYPE_PUTONG;

            String typeNo = MapValueUtils.getStringValue(AhuPartition.C_MKEY_CODE, coupleSection);
            while (typeNo.startsWith("0")) {
                typeNo = typeNo.substring(1);
            }

            PanelXSLXPO po = null;
            if(face.equals(SYS_PANEL_BOTTOM)){
                po = ExcelForPanelUtils.getInstance().getBottomPanel(serial, typeNo,
                        String.valueOf(sectionL));
            }else if(face.equals(SYS_PANEL_TOP)){
                po = ExcelForPanelUtils.getInstance().getTopPanel(serial, typeNo,
                        String.valueOf(sectionL));
            }else if(face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT)){
                po = ExcelForPanelUtils.getInstance().getNoBigOperatePanel(serial, typeNo,
                        String.valueOf(sectionL));
            }

            if (null == po) {
                po = new PanelXSLXPO();
                create2511Po(panelFace, sectionType, AhuUtil.getMoldSize(sectionL), po,serial);
                log.warn(MessageFormat.format("面板初始化-数据库逻辑-查询数据库元数据失败异常 serial:[{0}],typeNo:[{1}],sectionL:[{2}]",
                        serial, typeNo, String.valueOf(sectionL)));
            }
            //5:接管方向和当前面一致 39G盘管段为外面板、使用操作面切割excel
            if(coilWaiPanel >-1) {
                /**接管方向导致盘管段左右切割互换**/
                if (coilWaiPanel == SYS_PANEL_TYPE_WAI || coilWaiPanel == SYS_PANEL_TYPE_JIASHI) {
                    po = ExcelForPanelUtils.getInstance().getBigOperatePanel(serial, typeNo,
                            String.valueOf(sectionL));
                }
                po = getPanelTypeOfPo(coilWaiPanel, po);
            }

            //6: 混合段里面的开门方向和所在面相同(或混合段左右开门)，方法体非操作面切割改为操作面切割
            if(SectionTypeEnum.TYPE_MIX == sectionType){
                String directionMixDoor = String.valueOf(sectionMap.get(METASEXON_MIX_DOORDIRECTION));
                if(((directionMixDoor.equals(MIX_DOORDIRECTION_LEFT) && face.equals(SYS_PANEL_LEFT) )
                        || (directionMixDoor.equals(MIX_DOORDIRECTION_RIGHT) && face.equals(SYS_PANEL_RIGHT)))
                        || (MIX_DOORDIRECTION_BOTH.equals(directionMixDoor)) && (face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT))){
                    //新建切割对象
                    po = ExcelForPanelUtils.getInstance().getBigOperatePanel(serial, typeNo,
                            String.valueOf(sectionL));
                    //获取变形Po
                    if(isChange){
                        po = ExcelForPanelUtils.getInstance().getBigOperatePanelByH(serial,typeNo,String.valueOf(sectionL));
                    }

                }
            }
            //7: 风机段里面的开门方向(既是AHU开门方向)和所在面相同(或者风机左右双侧开门)，方法体非操作面切割改为操作面切割
            if(SectionTypeEnum.TYPE_FAN == sectionType){
                String doorOnBothSide = BaseDataUtil.constraintString(sectionMap.get(UtilityConstant.METASEXON_FAN_DOORONBOTHSIDE));//两侧开门
                if((doororientationStr.equals(AHU_DOORORIENTATION_LEFT) && face.equals(SYS_PANEL_LEFT) )
                        || (doororientationStr.equals(AHU_DOORORIENTATION_RIGHT) && face.equals(SYS_PANEL_RIGHT))
                        || ("true".equals(doorOnBothSide) && (face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT)))){
                    po = ExcelForPanelUtils.getInstance().getBigOperatePanel(serial, typeNo,
                            String.valueOf(sectionL));
                    //获取变形Po
                    if(isChange){
                        po = ExcelForPanelUtils.getInstance().getBigOperatePanelByH(serial,typeNo,String.valueOf(sectionL));
                    }
                }
            }

            poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);
            serialCode++;
            sectionsStepIndex++;
        }
        slicePanelByVerticalFirstRule(panelFace, hs, hTypes, null, null, vs, vTypes, null);
        slicePanelByExcelPo(panelFace, poMap);
        // 封装叶子面板为树状结构
        slicePanelByBitreeRule(panelFace, serial);
    }
    /**
     * 底面特殊逻辑
     * 1:盘管段，加湿段底面板为独立普通面板
     * 2:风机段底面板是特殊面板为加强面板，加强面板原则上也不能大于25X11
     * 3:除此之外的面板全部为普通面板，按照39CQ面板布置基本原则
     * @param partition
     * @param panelFace
     * @param serial
     */
    public static void cutBottomPanel(AhuPartition partition, PanelFace panelFace, String serial) {
        LinkedList<Map<String, Object>> coupleSections = partition.getCoupleSections();
        // int size = coupleSections.size();

        List<Integer> vsList = new ArrayList<Integer>();
        List<Integer> vsTypesList = new ArrayList<Integer>();
        Integer[] vs, vTypes;
        Integer[] hs = new Integer[] { panelFace.getPanelWidth() };
        Integer[] hTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };

        Map<String, PanelXSLXPO> poMap = new HashMap<>();
        int serialCode = 0;
        int sectionsStepIndex = 0;
        for (Map<String, Object> coupleSection : coupleSections) {

            String metaId = MapValueUtils.getStringValue(AhuPartition.C_MKEY_METAID, coupleSection);
            if(metaId.contains(SYS_PUNCTUATION_LOW_HYPHEN)){
                sectionsStepIndex++;
            }
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);
            if(SectionTypeEnum.TYPE_MIX == sectionType
                    || SectionTypeEnum.TYPE_DISCHARGE == sectionType){
                Map<String, Object> partMap = partition.getSections().get(sectionsStepIndex);
                Map<String, String> sectionMap = new HashMap<>();
                String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
                sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));
                String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));

                //出风段底部出风
                String outletDirection = String.valueOf(sectionMap.get(METASEXON_DISCHARGE_OUTLETDIRECTION));
                if (metaId.equals(metaId1)
                        && (
                        SYS_ASSERT_TRUE.equals(String.valueOf(sectionMap.get(METASEXON_MIX_RETURNBUTTOM)))
                                || JSON_MIX_OUTTYPE_F.equals(outletDirection)
                )
                        ) {
                    Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
                    vsList.add(AhuUtil.getMoldSize(sectionL));
                    vsTypesList.add(SYS_PANEL_TYPE_WU);
                    sectionsStepIndex++;
                    serialCode++;
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, null);//不再进行二次切割
                    continue;
                }
            }

            if ((SectionTypeEnum.TYPE_COLD == sectionType
                    || SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL == sectionType
                    || SectionTypeEnum.TYPE_HEATINGCOIL == sectionType
                    || SectionTypeEnum.TYPE_STEAMCOIL == sectionType
                    || (SectionTypeEnum.TYPE_WHEELHEATRECYCLE == sectionType && partition.isTopLayer()))
                    && SystemCountUtil.getUnitWidth(serial)<25 //大于25的要使用：25*11逻辑不能整个为普通面板
                    && SystemCountUtil.getUnitHeight(serial)<25 //大于25的要使用：25*11逻辑不能整个为普通面板
                    ) {
                Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
                vsList.add(AhuUtil.getMoldSize(sectionL));
                vsTypesList.add(SectionTypeEnum.TYPE_WHEELHEATRECYCLE == sectionType?SYS_PANEL_TYPE_WU:SYS_PANEL_TYPE_PUTONG);
                poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, null);//不再进行二次切割
            } else {

                Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
                int len = AhuUtil.getMoldSize(sectionL);
                if(0 == len){
                    sectionsStepIndex++;
//                    serialCode++;//防止空段影响map 和 poMap key 对应关系
                    continue;
                }
                vsList.add(len);
                vsTypesList.add(SYS_PANEL_TYPE_PUTONG);



                //新建切割对象
                PanelXSLXPO po = new PanelXSLXPO();
                create2511Po(panelFace, sectionType, len, po,serial);
                //添加切割对象
                poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);

            }
            sectionsStepIndex++;
            serialCode++;
        }
        vs = (Integer[]) vsList.toArray(new Integer[0]);
        vTypes = (Integer[]) vsTypesList.toArray(new Integer[0]);

        slicePanelByVerticalFirstRule(panelFace, hs, hTypes, null, null, vs, vTypes, null);
        slicePanelByExcelPo(panelFace, poMap);
        // 封装叶子面板为树状结构
        slicePanelByBitreeRule(panelFace, serial);
    }

    private static void create2511Po(PanelFace panelFace, SectionTypeEnum sectionType, int len, PanelXSLXPO po,String serial) {
        po.setH(SYS_BLANK+panelFace.getPanelWidth());
        po.setW(SYS_BLANK+panelFace.getPanelLength());
        Integer[] theVerticals = calculatePanelSliceUnits(len, AhuPartitionGenerator.PREFER_PANEL_LENGTH);
        po.setWr(array2SplitStr(theVerticals));
        StringBuffer wp = new StringBuffer(SYS_BLANK);
        for (int i = 0; i < theVerticals.length; i++) {
            if (SectionTypeEnum.TYPE_FAN == sectionType) {
                wp.append(SYS_PANEL_TYPE_JIAQIANG);// 风机加强面板
            } else {
                wp.append(SYS_PANEL_TYPE_PUTONG);// 普通面板
            }
            if(i<theVerticals.length-1)
                wp.append(SYS_PUNCTUATION_PLUS);
        }
        po.setWp(wp.toString());
        Integer[] theHorizontals = calculatePanelSliceUnits(panelFace.getPanelWidth(), getPreferPanelWidth(panelFace.getId().substring(0,1),serial));
        po.setHr(array2SplitStr(theHorizontals));
        StringBuffer hp = new StringBuffer(SYS_BLANK);
        for (int i = 0; i < theHorizontals.length; i++) {
            if (SectionTypeEnum.TYPE_FAN == sectionType) {
                hp.append(SYS_PANEL_TYPE_JIAQIANG);// 风机加强面板
            } else {
                hp.append(SYS_PANEL_TYPE_PUTONG);// 普通面板
            }
            if(i<theHorizontals.length-1)
                hp.append(SYS_PUNCTUATION_PLUS);
        }
        po.setHp(hp.toString());
    }

    /**
     * excel切割
     * @param panelFace
     * @param poMap
     */
    private static void slicePanelByExcelPo(PanelFace panelFace, Map<String, PanelXSLXPO> poMap) {
        if (panelFace.getSubPanels() != null) {
            for (int i = 0; i < panelFace.getSubPanels().length; i++) {
                genPanelFaceFromPO(panelFace.getSubPanels()[i], poMap.get(panelFace.getSubPanels()[i].getId()));
            }
        }else{//只有一个分段面板既是段
            for (int i = 0; i < poMap.keySet().size(); i++) {
                if(null != poMap.get(panelFace.getId()+"_"+i)) {//防止偶发某些段为空导致poMap 失效，例如：单层过滤段长度0+混合段，第一个poMap为空
                    genPanelFaceFromPO(panelFace, poMap.get(panelFace.getId() + "_"+i));
                    break;
                }
            }
        }
    }

    /**
     * 非操作面、顶面，切割
     * @param face
     * @param isChange
     * @param partition
     * @param panelFace
     * @param serial
     * @param isPipeOrientation
     * @param doororientationStr
     */
    public static void cutNoBigOperateOrTopPanel(String face, boolean isChange, AhuPartition partition, PanelFace panelFace, String serial, Boolean isPipeOrientation, String doororientationStr) {
        LinkedList<Map<String, Object>> coupleSections = partition.getCoupleSections();

        String direction = isPipeOrientation?SYS_STRING_LEFT:SYS_STRING_RIGHT;

        /**
         * 1：对 (混合段) 及 (其他段面板merge后长度) 整合
         *
         * 如果混合段，顶、右出风进行特殊处理（面板处理为空面板）
         * 格式：
         * stepByMix            : 长度
         * stepByMix + ahu.mix  : 长度
         **/
        LinkedHashMap<String,Integer> mergedPanelLen = new LinkedHashMap<String,Integer>();
        Map<String,Map<String,String>> mergedPanelLenParams = new HashMap<>();
        int stepBySpecial = 1;
        int sectionsStepIndex = 0;
        boolean hasSpecialReturnTopRight = false;
        for (Map<String, Object> coupleSection : coupleSections) {
            String metaId = MapValueUtils.getStringValue(AhuPartition.C_MKEY_METAID, coupleSection);
            if(metaId.contains(SYS_PUNCTUATION_LOW_HYPHEN)){
                sectionsStepIndex++;
            }

            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);
            Map<String, Object> partMap = partition.getSections().get(sectionsStepIndex);
            Map<String, String> sectionMap = new HashMap<>();
            String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
            sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));

            boolean isSpecial = false;
            //1: 混合段、出风段顶部、右出风不进行分割;
            if(SectionTypeEnum.TYPE_MIX == sectionType){
                String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));

                String top = String.valueOf(sectionMap.get(METASEXON_MIX_RETURNTOP));//顶部出风
                String right = String.valueOf(sectionMap.get(METASEXON_MIX_RETURNRIGHT));//右出风
                String left = String.valueOf(sectionMap.get(METASEXON_MIX_RETURNLEFT));//右出风
                if(metaId.equals(metaId1) &&
                        ((SYS_ASSERT_TRUE.equals(top) && face.equals(SYS_PANEL_TOP))
                                || (SYS_ASSERT_TRUE.equals(right) && face.equals(SYS_PANEL_RIGHT))
                                || (SYS_ASSERT_TRUE.equals(left) && face.equals(SYS_PANEL_LEFT))
                        )){
                    isSpecial = true;
                }
            }
            if(SectionTypeEnum.TYPE_DISCHARGE == sectionType){
                String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));

                String outletDirection = String.valueOf(sectionMap.get(METASEXON_DISCHARGE_OUTLETDIRECTION));//顶部出风
                if(metaId.equals(metaId1) &&
                        ((JSON_MIX_OUTTYPE_T.equals(outletDirection) && face.equals(SYS_PANEL_TOP))
                                || (JSON_MIX_OUTTYPE_R.equals(outletDirection) && face.equals(SYS_PANEL_RIGHT)))){
                    isSpecial = true;
                }
            }
            //2(操作面)接管方向和当前面一致：39CQ盘管段为普通切割
            if (isPipeOrientation &&
                    (face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT))
                    &&
                    (SectionTypeEnum.TYPE_COLD == sectionType
                            || SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL == sectionType
                            || SectionTypeEnum.TYPE_HEATINGCOIL == sectionType
                            || SectionTypeEnum.TYPE_STEAMCOIL == sectionType
                            || SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL == sectionType
                            || SectionTypeEnum.TYPE_STEAMHUMIDIFIER == sectionType
                            || SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER == sectionType
                    )
                    ){
                String metaId1 = String.valueOf(partMap.get(S_MKEY_METAID));
                if(metaId.equals(metaId1)){
                    isSpecial = true;
                }
            }
            //3: 顶部新回排段特殊处理 按照长度11进行逻辑竖切
            if(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER == sectionType && face.equals(SYS_PANEL_TOP)){
                isSpecial = true;

            }
            //4:无蜗壳风机：顶部风阀，顶面板切割右边区域使用无面板；
            //5:普通风机：顶部出风，顶部面板特殊excel切割
            if (face.equals(SYS_PANEL_TOP) && SectionTypeEnum.TYPE_FAN == sectionType) {
                HashMap<String, String> FanParamsMap = new HashMap<String, String>();

                String fanOutlet = sectionMap.get(METASEXON_FAN_OUTLET);//风机形式
                String fanMotorPosition = sectionMap.get(METASEXON_FAN_MOTORPOSITION);//电机位置:侧置side 后置back
                //无蜗壳风机
                if(JSON_FAN_OUTLET_WWK.equals(fanOutlet)) {
                    //送风出口位置
                    String airDirection = sectionMap.get(METASEXON_AIRDIRECTION);
                    String fkjjwz = sectionMap.get(METASEXON_FAN_SENDPOSITION);
                    if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)) {//回风机组
                        fkjjwz = sectionMap.get(METASEXON_FAN_RETURNPOSITION);
                    }
                    if (!FAN_SENDPOSITION_NO.equals(fkjjwz) && FAN_SENDPOSITION_TOP.equals(fkjjwz)) {
                        isSpecial = true;
                    }
                }else{
                    String outletDirection = sectionMap.get(METASEXON_FAN_OUTLETDIRECTION);//出风方向:顶部THF/BHF 端面（水平）UBF/UBR
                    //顶部出风(顶面)
                    if (FAN_OUTLETDIRECTION_UBF.equals(outletDirection) || FAN_OUTLETDIRECTION_UBR.equals(outletDirection)) {
                        isSpecial = true;
                    }
                }
                FanParamsMap.put("fanOutlet",fanOutlet);
                FanParamsMap.put("fanMotorPosition",fanMotorPosition);
                mergedPanelLenParams.put(sectionType.getId()+stepBySpecial,FanParamsMap);
            }
            //6: 底层转轮顶部面板特殊处理：无面板
            if(SectionTypeEnum.TYPE_WHEELHEATRECYCLE == sectionType
                    && face.equals(SYS_PANEL_TOP)
                    && !partition.isTopLayer()){
                isSpecial = true;
            }

            //7: 混合段里面的开门方向和所在面相同(或混合段左右开门)，方法体非操作面切割改为操作面切割
            if(SectionTypeEnum.TYPE_MIX == sectionType){
                String directionMixDoor = String.valueOf(sectionMap.get(METASEXON_MIX_DOORDIRECTION));
                if(((directionMixDoor.equals(MIX_DOORDIRECTION_LEFT) && face.equals(SYS_PANEL_LEFT) )
                        || (directionMixDoor.equals(MIX_DOORDIRECTION_RIGHT) && face.equals(SYS_PANEL_RIGHT)))
                        || (MIX_DOORDIRECTION_BOTH.equals(directionMixDoor)) && (face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT))){
                    isSpecial = true;

                    HashMap<String, String> mixParamsMap = new HashMap<String, String>();
                    mixParamsMap.put("isAHUPartSameDirection","true");
                    mergedPanelLenParams.put(sectionType.getId()+stepBySpecial,mixParamsMap);
                }
            }
            //8: 风机段里面的开门方向(既是AHU开门方向)和所在面相同(或者风机左右双侧开门)，方法体非操作面切割改为操作面切割
            if(SectionTypeEnum.TYPE_FAN == sectionType){
                String doorOnBothSide = BaseDataUtil.constraintString(sectionMap.get(UtilityConstant.METASEXON_FAN_DOORONBOTHSIDE));//两侧开门
                if((doororientationStr.equals(AHU_DOORORIENTATION_LEFT) && face.equals(SYS_PANEL_LEFT) )
                        || (doororientationStr.equals(AHU_DOORORIENTATION_RIGHT) && face.equals(SYS_PANEL_RIGHT))
                        || ("true".equals(doorOnBothSide) && (face.equals(SYS_PANEL_LEFT) || face.equals(SYS_PANEL_RIGHT)))){
                    isSpecial = true;

                    HashMap<String, String> mixParamsMap = new HashMap<String, String>();
                    mixParamsMap.put("isAHUPartSameDirection","true");
                    mergedPanelLenParams.put(sectionType.getId()+stepBySpecial,mixParamsMap);
                }
            }


            if(isSpecial){
                Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
                mergedPanelLen.put(sectionType.getId()+stepBySpecial,sectionL);
                sectionsStepIndex++;
                stepBySpecial++;
                hasSpecialReturnTopRight = true;
                continue;
            }

            //5: 被特殊切割段 隔开的其他分段按照一个面板进行切割。
            Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, coupleSection);
            int len = AhuUtil.getMoldSize(sectionL);
            if(len>0) {
                if (mergedPanelLen.containsKey(String.valueOf(stepBySpecial))) {
                    mergedPanelLen.put(SYS_BLANK + stepBySpecial, mergedPanelLen.get(SYS_BLANK + stepBySpecial) + len);
                } else {
                    mergedPanelLen.put(SYS_BLANK + stepBySpecial, len);
                }
            }
            sectionsStepIndex++;
        }

        /**
         * 2：混合段包含顶面、非操作面出风进行特殊处理
         **/

        if(hasSpecialReturnTopRight) {
            List<Integer> vsList = new ArrayList<Integer>();
            List<Integer> vsTypesList = new ArrayList<Integer>();

            Integer[] vs , vTypes;
            Integer[] hs = new Integer[] { panelFace.getPanelWidth() };
            Integer[] hTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };
            Map<String, PanelXSLXPO> poMap = new HashMap<>();

            Iterator iter = mergedPanelLen.entrySet().iterator();
            int serialCode = 0;
            while (iter.hasNext()) {

                Map.Entry entry = (Map.Entry) iter.next();
                String key = String.valueOf(entry.getKey());
                Integer val = NumberUtil.convertStringToDoubleInt(entry.getValue().toString());

                Integer sectionL = val;
                int len = AhuUtil.getMoldSize(sectionL);
                vsList.add(len);
                if(key.contains(SectionTypeEnum.TYPE_MIX.getId()) || key.contains(SectionTypeEnum.TYPE_DISCHARGE.getId())){
                    String isAHUPartSameDirection = getMapVal(mergedPanelLenParams,key,"isAHUPartSameDirection");
                    if("true".equals(isAHUPartSameDirection)){//7: 混合段里面的开门方向和AHU开门方向相同，方法体非操作面切割改为操作面切割
                        //新建切割对象
                        PanelXSLXPO po = new PanelXSLXPO();
                        String typeNo = SectionTypeEnum.TYPE_MIX.getCodeNum();
                        while (typeNo.startsWith("0")) {
                            typeNo = typeNo.substring(1);
                        }
                        po = ExcelForPanelUtils.getInstance().getBigOperatePanel(serial, typeNo,
                                String.valueOf(sectionL));
                        //获取变形Po
                        if(isChange){
                            po = ExcelForPanelUtils.getInstance().getBigOperatePanelByH(serial,typeNo,String.valueOf(sectionL));
                        }
                        //添加切割对象
                        poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);
                        vsTypesList.add(SYS_PANEL_TYPE_PUTONG);
                    }else{
                        vsTypesList.add(SYS_PANEL_TYPE_WU);
                        poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, null);//不再进行二次切割
                    }
                    serialCode++;
                }else if(key.contains(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId())){//顶部新回排段特殊处理 按照长度11进行逻辑竖切
                    vsTypesList.add(SYS_PANEL_TYPE_WU);// 无面板

                    //新建切割对象
                    PanelXSLXPO po = new PanelXSLXPO();
                    //新回排大机型顶部切割，使用TOP.xls 切割
                    if(SystemCountUtil.gtSmallUnit(Integer.parseInt(serial.substring(serial.length()-4)))){
                        String typeNo = SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getCodeNum();
                        while (typeNo.startsWith("0")) {
                            typeNo = typeNo.substring(1);
                        }
                        po = ExcelForPanelUtils.getInstance().getTopPanel(serial, typeNo,
                                String.valueOf(sectionL));
                    }else{
                        po.setH(SYS_BLANK+panelFace.getPanelWidth());
                        po.setW(SYS_BLANK+panelFace.getPanelLength());
                        Integer[] bootomVerticals = calculatePanelSliceUnits(len, AhuPartitionGenerator.PREFER_PANEL_LENGTH);
                        po.setWr(array2SplitStr(bootomVerticals));
                        StringBuffer wp = new StringBuffer(SYS_BLANK);
                        for (int i = 0; i < bootomVerticals.length; i++) {
                            wp.append(SYS_PANEL_TYPE_WU);// 无面板
                            if(i<bootomVerticals.length-1)
                                wp.append(SYS_PUNCTUATION_PLUS);
                        }
                        po.setWp(wp.toString());
                        po.setHr(SYS_BLANK+panelFace.getPanelWidth());
                        po.setHp(SYS_BLANK+SYS_PANEL_TYPE_WU);
                    }
                    //添加切割对象
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);
                    serialCode++;
                }else if(key.contains(SectionTypeEnum.TYPE_FAN.getId())){//无蜗壳风机：顶部风阀，顶面板切割右边区域使用无面板；

                    //新建切割对象
                    PanelXSLXPO po = new PanelXSLXPO();

                    String fanOutlet = getMapVal(mergedPanelLenParams,key,"fanOutlet");
                    String fanMotorPosition = getMapVal(mergedPanelLenParams,key,"fanMotorPosition");
                    String isAHUPartSameDirection = getMapVal(mergedPanelLenParams,key,"isAHUPartSameDirection");
                    if("true".equals(isAHUPartSameDirection)){//风机段里面的开门方向为左右双侧开门，方法体非操作面切割改为操作面切割
                        //新建切割对象
                        String typeNo = SectionTypeEnum.TYPE_FAN.getCodeNum();
                        while (typeNo.startsWith("0")) {
                            typeNo = typeNo.substring(1);
                        }
                        po = ExcelForPanelUtils.getInstance().getBigOperatePanel(serial, typeNo,
                                String.valueOf(sectionL));
                        //获取变形Po
                        if(isChange){
                            po = ExcelForPanelUtils.getInstance().getBigOperatePanelByH(serial,typeNo,String.valueOf(sectionL));
                        }
                    }else if(JSON_FAN_OUTLET_WWK.equals(fanOutlet)){//无蜗壳风机
                        po.setH(SYS_BLANK+panelFace.getPanelWidth());
                        po.setW(SYS_BLANK+panelFace.getPanelLength());

                        /*Damper damper = AhuMetadata.findOne(Damper.class, serial.substring(serial.length() - 4));//混合段标准段长
                        int wuPanelLen = NumberUtil.parseInt(damper.getSectionL());*/

                        PanelXSLXPO sideXls = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial);
                        if(isChange && (serial.contains(AHU_PRODUCT_39XT) || serial.contains(AHU_PRODUCT_39G))){
                            sideXls = ExcelForPanelUtils.getInstance().getDamperSidePanel(serial.replace(AHU_PRODUCT_39XT,AHU_PRODUCT_39CQ).replace(AHU_PRODUCT_39G,AHU_PRODUCT_39CQ));
                            if (null == po) {
                                sideXls.setSerial(serial);
                            }
                        }
                        int wuPanelLen = AhuUtil.getHeightOfAHU(serial) - cn.hutool.core.util.NumberUtil.parseInt(sideXls.getH());//无面板M数

                        po.setWr(array2SplitStr(new Integer[]{len-wuPanelLen,wuPanelLen}));
                        po.setWp(SYS_PANEL_TYPE_PUTONG + SYS_PUNCTUATION_PLUS + SYS_PANEL_TYPE_WU);// 普通面板 + 无面板
                        po.setHr(SYS_BLANK+panelFace.getPanelWidth());
                        po.setHp(SYS_BLANK+SYS_PANEL_TYPE_WU);
                    }else{
                        //普通风机顶面excel切割
                        po = getFanTopPanelXSLXPO(serial,direction,len,fanMotorPosition);
                    }
                    vsTypesList.add(SYS_PANEL_TYPE_PUTONG);// 普通面板
                    //添加切割对象
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);
                    serialCode++;
                }else if(key.contains(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId())){
                    //底层转轮顶部面板特殊处理：无面板
                    vsTypesList.add(SYS_PANEL_TYPE_WU);
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, null);//不再进行二次切割
                    serialCode++;
                }else{
                    vsTypesList.add(SYS_PANEL_TYPE_PUTONG);
                    //新建切割对象
                    PanelXSLXPO po = new PanelXSLXPO();
                    po.setH(SYS_BLANK+panelFace.getPanelWidth());
                    po.setW(SYS_BLANK+panelFace.getPanelLength());
                    Integer[] bootomVerticals = calculatePanelSliceUnits(len, AhuPartitionGenerator.PREFER_PANEL_LENGTH);
                    po.setWr(array2SplitStr(bootomVerticals));
                    StringBuffer wp = new StringBuffer(SYS_BLANK);
                    for (int i = 0; i < bootomVerticals.length; i++) {
                        wp.append(getPanelType(serial, key,isPipeOrientation));// 面板类型（普通面板、外面板）
                        if(i<bootomVerticals.length-1)
                            wp.append(SYS_PUNCTUATION_PLUS);
                    }
                    po.setWp(wp.toString());
                    Integer[] theHorizontals = calculatePanelSliceUnits(panelFace.getPanelWidth(), getPreferPanelWidth(face,serial));
                    po.setHr(array2SplitStr(theHorizontals));
                    StringBuffer hp = new StringBuffer(SYS_BLANK);
                    for (int i = 0; i < theHorizontals.length; i++) {

                        hp.append(getPanelType(serial,key,isPipeOrientation));// 面板类型（普通面板、外面板）

                        if(i<theHorizontals.length-1)
                            hp.append(SYS_PUNCTUATION_PLUS);
                    }
                    po.setHp(hp.toString());
                    //添加切割对象
                    poMap.put(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + serialCode, po);
                    serialCode++;
                }
            }

            vs = (Integer[]) vsList.toArray(new Integer[0]);
            vTypes = (Integer[]) vsTypesList.toArray(new Integer[0]);

            slicePanelByVerticalFirstRule(panelFace, hs, hTypes, null, null, vs, vTypes, null);
            slicePanelByExcelPo(panelFace, poMap);
            // 封装叶子面板为树状结构
            slicePanelByBitreeRule(panelFace, serial);
        }else {
            /**
             * 3： 没有特殊逻辑：非操作面、顶面，整个面板采用逻辑切割
             */
            Integer[] horizontals, verticals, horizontalTypes, verticalTypes;
            horizontals = calculatePanelSliceUnits(panelFace.getPanelWidth(), getPreferPanelWidth(face,serial));
            verticals = calculatePanelSliceUnits(panelFace.getPanelLength(), AhuPartitionGenerator.PREFER_PANEL_LENGTH);
            horizontalTypes = new Integer[horizontals.length];
            for (int i = 0; i < horizontals.length; i++) {
                horizontalTypes[i] = 0;
            }
            verticalTypes = new Integer[verticals.length];
            for (int i = 0; i < verticals.length; i++) {
                verticalTypes[i] = 0;
            }
            // 组装叶子面板
            slicePanelByRule(panelFace, horizontals, horizontalTypes, verticals, verticalTypes);
            // 封装叶子面板为树状结构
            slicePanelByBitreeRule(panelFace, serial);
        }
    }

    /**
     * 获取map 值
     * @param mergedPanelLenParams
     * @param key1
     * @param key2
     * @return
     */
    private static String getMapVal(Map<String,Map<String,String>> mergedPanelLenParams,String key1,String key2){
        String retVal = SYS_BLANK;
        if(EmptyUtil.isNotEmpty(mergedPanelLenParams)
                && EmptyUtil.isNotEmpty(mergedPanelLenParams.get(key1))
                && EmptyUtil.isNotEmpty(mergedPanelLenParams.get(key1).get(key2))){
            retVal = mergedPanelLenParams.get(key1).get(key2);
        }
        return retVal;
    }
    /**
     * 39CQ 顶底采用25M 切割
     * @param face
     * @param serial
     * @return
     */
    private static int getPreferPanelWidth(String face,String serial){
        if(serial.contains(SYS_UNIT_SERIES_39CQ) &&
                (face.equals(UtilityConstant.SYS_PANEL_TOP) || face.equals(UtilityConstant.SYS_PANEL_BOTTOM))){
            return AhuPartitionGenerator.PREFER_PANEL_WIDTH1;
        }else {
            return AhuPartitionGenerator.PREFER_PANEL_WIDTH;
        }
    }
    /**
     * (非操作面)接管方向和当前面一致：盘管段为外面版
     * @param serial
     * @param key
     * @param isPipeOrientation
     * @return
     */
    private static String getPanelType(String serial, String key, Boolean isPipeOrientation) {
        if (isPipeOrientation &&
                (  key.contains(SectionTypeEnum.TYPE_COLD.getId())
                        || key.contains(SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId())
                        || key.contains(SectionTypeEnum.TYPE_HEATINGCOIL.getId())
                        || key.contains(SectionTypeEnum.TYPE_STEAMCOIL.getId())
                        || key.contains(SectionTypeEnum.TYPE_STEAMCOIL.getId()))) {
            if (serial.contains(SYS_UNIT_SERIES_39CQ)){
                return String.valueOf(SYS_PANEL_TYPE_PUTONG);//39CQ无外面板，外面板当作普通面板
            }else{
                return String.valueOf(SYS_PANEL_TYPE_WAI);//外面板
            }
        }else if (isPipeOrientation &&
                (  key.contains(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId())
                        || key.contains(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId())
                )) {//接管方向和当前面一致干蒸电极加湿段为加湿面板
            return String.valueOf(SYS_PANEL_TYPE_JIASHI);//加湿
        }else{
            return String.valueOf(SYS_PANEL_TYPE_PUTONG);
        }
    }

    /**
     * 替换xlsPo 面板类型
     * @param pType
     * @param po
     */
    private static PanelXSLXPO getPanelTypeOfPo(int pType,PanelXSLXPO po) {
        if (null == po) {
            log.warn("面板初始化-数据库逻辑-查询数据库元数据失败异常 getPanelTypeOfPo");
            return null;
        }
        PanelXSLXPO clonePo = ObjectUtil.clone(po);//深拷贝

        clonePo.setHp(clonePo.getHp().replaceAll("\\d",String.valueOf(pType)));
        clonePo.setWp(clonePo.getWp().replaceAll("\\d",String.valueOf(pType)));

        return clonePo;
    }

    /**
     * 替换xlsPo 面板类型
     * @param oldPType
     * @param newPType
     * @param po
     */
    private static void getPanelTypeOfPo(int oldPType,int newPType,PanelXSLXPO po) {
        if (null == po) {
            log.warn("面板初始化-数据库逻辑-查询数据库元数据失败异常 getPanelTypeOfPo");
            return;
        }
        po.setHp(po.getHp().replaceAll(String.valueOf(oldPType),String.valueOf(newPType)));
        po.setWp(po.getWp().replaceAll(String.valueOf(oldPType),String.valueOf(newPType)));
    }

    /**
     * 本方法用于计算整个面板的切分单位，规则如下：<br>
     * 规则 10 10 <br>
     * 面板 45 50 <br>
     * 切: 45%10 50%10 <br>
     * 得到: 4*10+5 5*10 <br>
     * 本方法输入为：（45， 10） or (50， 10) <br>
     * <br>
     * 注：请分两次调用计算width，length的单位数组 <br>
     *
     * <br>
     * TODO but (45, 10), will get (10, 10, 10, 7, 8), comment needs to be updated.
     *
     * 10M 面板平均9M 为单位
     * 切割结果：5M+5M
     * @param total
     * @param preferUnit
     * @return
     */
    public static Integer[] calculatePanelSliceUnits(int total, int preferUnit) {
        if (total <= preferUnit) {
            return new Integer[] { total };
        }
        List<Integer> result = new ArrayList<>();
        int size = total / preferUnit;
        int left = total % preferUnit;
        for (int i = 0; i < size - 1; i++) {
            result.add(preferUnit);
        }
        if (left == 0) {
            result.add(preferUnit);
        } else {
            int tempT = left + preferUnit;
            int tempU = tempT / 2;
            result.add(tempU);
            result.add(tempT - tempU);
        }
        return result.toArray(new Integer[0]);
    }

    /**
     * calculatePanelSliceUnits 无均等重构
     *
     * 10M 面板平均9M 为单位
     * 切割结果：9M+1M
     * @param total
     * @param preferUnit
     * @return
     */
    public static Integer[] calculatePanelSliceUnitsNoPre(int total, int preferUnit) {
        if (total <= preferUnit) {
            return new Integer[] { total };
        }
        List<Integer> result = new ArrayList<Integer>();
        int size = total / preferUnit;
        int left = total % preferUnit;
        for (int i = 0; i < size; i++) {
            result.add(preferUnit);
        }
        if (left != 0) {
            result.add(left );
        }
        return result.toArray(new Integer[0]);
    }

    /**
     * 二等分
     * @param total
     * @return
     */
    public static Integer[] calculate2Pre(int total) {
        List<Integer> result = new ArrayList<Integer>();
        if(total<=1){
            result.add(1);
        }else {
            int preferUnit = total / 2;
            result.add(preferUnit);
            result.add(total - preferUnit);
        }
        return result.toArray(new Integer[0]);
    }
    /**
     * 初始化面板分割的方法 1 - [先纵切，再横切] <br>
     * 按照列和行的平铺方式，整个的切割只分两类，先平铺列，再在列里面平铺行<br>
     * 可以表示数据结构，但是前端暂时不支持，同时不支持前端复杂的切割方式<br>
     * @param panelFace 待切割面板数据
     * @param horizontals 横切数据
     * @param horizontalTypes 横切面板类型
     * @param hRepeatSlice 横向独立切割数据
     * @param hTypeRepeatSlice 横向独立切割面板类型
     * @param verticals 纵切数据
     * @param verticalTypes 纵切面板类型
     * @param vSectionTypes 纵切后段类型
     */
    public static void slicePanelByVerticalFirstRule(PanelFace panelFace,
                                                     Integer[] horizontals, Integer[] horizontalTypes, List<Integer[]> hRepeatSlice, List<Integer[]> hTypeRepeatSlice,
                                                     Integer[] verticals, Integer[] verticalTypes, String[] vSectionTypes) {
        /* 如果横/纵数组都为空，不需要切割 */
        if (ArrayUtils.isEmpty(horizontals) && ArrayUtils.isEmpty(verticals)) {
            panelFace.setDirection(SYS_PANEL_DIRECTION_NONE);
            panelFace.setPanelType(String.valueOf(SYS_PANEL_TYPE_PUTONG));
            return;
        }
        int plength = panelFace.getPanelLength();
        int pwidth = panelFace.getPanelWidth();
        /* 初始化横with父面板长 */
        if (ArrayUtils.isEmpty(horizontals)) {
            horizontals = new Integer[] { pwidth };
            horizontalTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };
        }
        /* 初始化纵向with父面板宽 */
        if (ArrayUtils.isEmpty(verticals)) {
            verticals = new Integer[] { plength };
            verticalTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };
        }

        // printPODataArray(horizontals, horizontalTypes, verticals, verticalTypes);
        PanelFace pvface = null;
        List<PanelFace> subFaceList = new ArrayList<>();
        List<PanelFace> AllSubHFaceList = new ArrayList<>();
        List<PanelFace> faceList = new ArrayList<>();//没有二次切割的面板进行门has处理
        for (int i = 0; i < verticals.length; i++) {
            int theLength = verticals[i];
            if(0 == theLength){//段长为0的段不参与切割
                continue;
            }
            String pfvType = String.valueOf(verticalTypes[i]);
            // 如果宽度和父面板相等，不需要做切割，直接把父面板设置成当前面板
            if (verticals[i] == plength) {
                pvface = panelFace;
                panelFace.setPanelType(pfvType);
                panelFace.setDirection(SYS_PANEL_DIRECTION_HORIZONTAL);
                faceList.add(pvface);
            } else {
                // 创建一个新的纵向子面板，并加入到父面板的孩子
                panelFace.setDirection(SYS_PANEL_DIRECTION_VERTICAL);
                pvface = createPanelFace(theLength, panelFace.getPanelWidth());
                pvface.setId(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + i);
                pvface.setPanelType(pfvType);

                if(ArrayUtils.isNotEmpty(vSectionTypes)) {
                    setSectionMetaId(i, pvface, vSectionTypes);
                }else{
                    pvface.setSectionMetaId(panelFace.getSectionMetaId());
                }
                pvface.setSectionDoorOpendAndViewport(panelFace.getSectionDoorOpendAndViewport());
                subFaceList.add(pvface);
            }


            // 做横向切割
            List<PanelFace> subHFaceList = new ArrayList<>();
            //如果需要横向独立切割，重置切割参数
            if(EmptyUtil.isNotEmpty(hRepeatSlice) && EmptyUtil.isNotEmpty(hTypeRepeatSlice)){
                horizontals = hRepeatSlice.get(i);
                horizontalTypes = hTypeRepeatSlice.get(i);
            }
            PanelFace phface = null;
            for (int j = 0; j < horizontals.length; j++) {
                int theWidth = horizontals[j];
                String pfhType = String.valueOf(horizontalTypes[j]);
                if (horizontals[0] == pwidth) {
                    phface = pvface;
                    phface.setDirection(SYS_PANEL_DIRECTION_NONE);
                    if (null == phface.getPanelType() || String.valueOf(SYS_PANEL_TYPE_PUTONG).equals(pfvType)) {
                        //phface.setPanelType(pfhType);
                    }
                    faceList.add(phface);
                } else {
                    // 创建一个新的横向子面板，并加入到父面板的孩子
                    pvface.setDirection(SYS_PANEL_DIRECTION_HORIZONTAL);
                    phface = createPanelFace(pvface.getPanelLength(), theWidth);
                    phface.setId(pvface.getId() + SYS_PUNCTUATION_LOW_HYPHEN + j);
                    phface.setSectionMetaId(pvface.getSectionMetaId());
                    phface.setSectionDoorOpendAndViewport(pvface.getSectionDoorOpendAndViewport());
                    if (null == phface.getPanelType()){
                        //默认面板类型为普通
                        phface.setPanelType(String.valueOf(SYS_PANEL_TYPE_PUTONG));
                        //1.（竖切=横切）且类型为非普通面板，才处理当前孩子面板类型为非普通
                        if(!String.valueOf(SYS_PANEL_TYPE_PUTONG).equals(pfvType)
                                && pfhType.equals(pfvType) && theLength>=3){//长大于等于3
                            phface.setPanelType(pfhType);
                        }
                        //2.没有纵切：（混合段:面切割参数为0：按照面板高度进行（空面板+面板）处理）
                        if(theLength == plength){
                            phface.setPanelType(pfhType);
                        }
                    }
                    phface.setSectionMetaId(panelFace.getSectionMetaId());
                    subHFaceList.add(phface);
                }
            }
            AllSubHFaceList.addAll(subHFaceList);
            if (!subHFaceList.isEmpty()) {
                PanelFace[] subs = new PanelFace[subHFaceList.size()];
                subs = subHFaceList.toArray(subs);
                pvface.setSubPanels(subs);
            }

        }
        if(panelFace.getId().startsWith(SYS_PANEL_LEFT) || panelFace.getId().startsWith(SYS_PANEL_RIGHT) ) {
            toSetSectionDoorOpendAndViewport(AllSubHFaceList);
            toSetSectionDoorOpendAndViewport(subFaceList);
            toSetSectionDoorOpendAndViewport(faceList);
        }
        if (!subFaceList.isEmpty()) {
            PanelFace[] subs = new PanelFace[subFaceList.size()];
            subs = subFaceList.toArray(subs);
            panelFace.setSubPanels(subs);
        }

    }

    /**
     * 风机的出风面板，增加 风机型号+出风方向+接管方向
     */

    private static void setFanParameter(PanelFace pface) {
        String sectionMetaId = pface.getSectionMetaId();
        if(String.valueOf(SYS_PANEL_TYPE_CHUFENG).equals(pface.getPanelType())
                &&EmptyUtil.isNotEmpty(sectionMetaId)
                && ((sectionMetaId.contains(SectionTypeEnum.TYPE_FAN.getId()))||((sectionMetaId.contains(SectionTypeEnum.TYPE_WWKFAN.getId()))))){
            pface.setMemo("TEST TODO");
        }

    }

    /**
     * 操作面范围内；如果段没开门：修改excel加载出来的门面板为普通面板；修改开窗面板memo为Has
     * @param pfaces
     */
    private static void toSetSectionDoorOpendAndViewport(List<PanelFace> pfaces) {
        boolean hasSetCombinedmixingchamberLeftDoor = false;
        for (int i = 0; i < pfaces.size(); i++) {
            PanelFace pface = pfaces.get(i);
            String sectionMetaId = pface.getSectionMetaId();
            String sectionDoorOpendAndViewport = pface.getSectionDoorOpendAndViewport();

            if ((String.valueOf(SYS_PANEL_TYPE_MEN).equals(pface.getPanelType())
                    || String.valueOf(SYS_PANEL_TYPE_ZENGYA).equals(pface.getPanelType())
                    || String.valueOf(SYS_PANEL_TYPE_CHAIXIE).equals(pface.getPanelType())
                    || String.valueOf(SYS_PANEL_TYPE_MEN_MING).equals(pface.getPanelType()))//门面板
                    && EmptyUtil.isNotEmpty(sectionMetaId)
                    && EmptyUtil.isNotEmpty(sectionDoorOpendAndViewport)) {

                boolean isPuTongPanel = false;
                boolean hasViewPort = false;

                //新回排两个门面板有可能不会同时开(面板里面：回风侧（左）、新风侧（右）)
                if (sectionMetaId.contains(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId())) {
                    if (!hasSetCombinedmixingchamberLeftDoor) {
                        if (!Boolean.parseBoolean(sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_COMMA)[0].split(SYS_PUNCTUATION_SEMICOLON)[0])) {//处理第一个门
                            isPuTongPanel = true;
                        }
                        if (Boolean.parseBoolean(sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_COMMA)[0].split(SYS_PUNCTUATION_SEMICOLON)[1])) {//处理第一个门，开窗
                            hasViewPort = true;
                        }
                    }
                    if (sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_COMMA).length > 1 && hasSetCombinedmixingchamberLeftDoor) {
                        if (!Boolean.parseBoolean(sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_COMMA)[1].split(SYS_PUNCTUATION_SEMICOLON)[0])) {//处理第二个门
                            isPuTongPanel = true;
                        }
                        if (Boolean.parseBoolean(sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_COMMA)[1].split(SYS_PUNCTUATION_SEMICOLON)[1])) {//处理第二个门，开窗
                            hasViewPort = true;
                        }
                    }
                    hasSetCombinedmixingchamberLeftDoor = true;
                } else {
                    //普通功能段开门只有一个门
                    if (!Boolean.parseBoolean(sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_SEMICOLON)[0])) {//
                        isPuTongPanel = true;
                    }

                    if (Boolean.parseBoolean(sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_SEMICOLON)[1])) {//处理开窗
                        hasViewPort = true;
                    }
                }
                if (isPuTongPanel)
                    pface.setPanelType(String.valueOf(SYS_PANEL_TYPE_PUTONG));
                if (hasViewPort)
                    pface.setMemo(String.valueOf(SYS_PANEL_DESC_HASVIEWPORT));
            }
        }
    }

    /**
     * 设置段类型到面板
     * @param i
     * @param pvface
     * @param vSectionTypes
     */
    private static void setSectionMetaId(int i, PanelFace pvface, String[] vSectionTypes) {
        try {
            pvface.setSectionMetaId(vSectionTypes[i]);
        } catch (Exception e) {
        }
    }

    /**
     * 初始化面板分割的方法2: 先横向使用二分法切割，再纵向使用二分法切割，可以切割出任意大小的矩形
     * 本方法以slicePanelByRule为基础，在基础上做两两合并
     *
     * @param panelFace
     * @param serial
     */
    public static void slicePanelByBitreeRule(PanelFace panelFace, String serial) {
        int direction = panelFace.getDirection();
        PanelFace[] subPanels = panelFace.getSubPanels();
        String defaultParentIdPrefix = panelFace.getId();
        if (subPanels != null && subPanels.length > 1) { // array不会出现只有一个元素的情况
            int pos = subPanels.length / 2;
            PanelFace[] horizontalsLeft = ArrayUtils.subarray(subPanels, 0, pos);
            PanelFace[] horizontalsRight = ArrayUtils.subarray(subPanels, pos, subPanels.length);
            PanelFace left = mergePanels(direction, horizontalsLeft, defaultParentIdPrefix + "_0");
            PanelFace right = mergePanels(direction, horizontalsRight, defaultParentIdPrefix + "_1");

//            if(serial.contains(SYS_UNIT_SERIES_39CQ))//只有CQ做面板连接器
//                makePanelConnector(direction, panelFace, left, right);

            slicePanelByBitreeRule(left, serial);
            slicePanelByBitreeRule(right, serial);
            panelFace.setSubPanels(new PanelFace[] { left, right });
        }
//        else{
//            if(serial.contains(SYS_UNIT_SERIES_39CQ)) {//只有CQ做面板连接器
//
//                if(EmptyUtil.isNotEmpty(panelFace.getPanelType()) && panelFace.getPanelType().equals(String.valueOf(SYS_PANEL_TYPE_JIAQIANG))){
//                    panelFace.setPanelConnector(buildF(panelFace.getPanelConnector(), SYS_PANEL_PANELCONNECTOR_F));
//                }else{
//                    if (panelFace.getId().length() == 1) {
//                        panelFace.setPanelConnector(SYS_PANEL_PANELCONNECTOR_00);//普通整张面板为：00
//                    }
//                }
//
//            }
//        }
        // 封装树状结构下的框条信息
        fillFrameLine(panelFace,serial);
    }

    /**
     * Create a PanelFace instance by converting mm size to mold size.
     *
     * @param length
     * @param width
     * @return
     */
    public static PanelFace createPanelFace(int length, int width) {
        return new PanelFace(AhuUtil.getMoldSize(length), AhuUtil.getMoldSize(width));
    }

    /**
     * 把面板切割为一个二维数组，横向个数 * 纵向个数，默认从纵向开始切（二维数组的第一层为纵向的父面板）
     *
     * @param panelFace
     * @param horizontals
     * @param horizontalTypes
     * @param verticals
     * @param verticalTypes
     */
    public static void slicePanelByRule(PanelFace panelFace, Integer[] horizontals, Integer[] horizontalTypes,
                                        Integer[] verticals, Integer[] verticalTypes) {
        slicePanelByRule(panelFace, horizontals, horizontalTypes, verticals, verticalTypes, 0);
    }

    private static PanelFace mergePanels(int direction, PanelFace[] subPanels, String nodeId) {
        if (subPanels.length == 1) {
            subPanels[0].setId(nodeId);
            return subPanels[0];
        }
        PanelFace panel = new PanelFace();
        if (direction == SYS_PANEL_DIRECTION_HORIZONTAL) {
            int width = 0;
            for (int i = 0; i < subPanels.length; i++) {
                PanelFace panelFace = subPanels[i];
                width = width + panelFace.getPanelWidth();
            }
            panel.setDirection(direction);
            panel.setPanelLength(subPanels[0].getPanelLength());
            panel.setPanelWidth(width);
        } else if (direction == SYS_PANEL_DIRECTION_VERTICAL) {
            int length = 0;
            for (int i = 0; i < subPanels.length; i++) {
                PanelFace panelFace = subPanels[i];
                length = length + panelFace.getPanelLength();
            }
            panel.setDirection(direction);
            panel.setPanelLength(length);
            panel.setPanelWidth(subPanels[0].getPanelWidth());
        }
        panel.setSubPanels(subPanels);
        panel.setId(nodeId);
        return panel;
    }

    /**
     * 构造面板连接器
     * 左边继承，右边变A
     * @param direction
     * @param panelFace
     * @param left
     * @param right
     */
//    private static void makePanelConnector(int direction, PanelFace panelFace, PanelFace left, PanelFace right) {
//        String jiaqiangLeftF = "";
//        String jiaqiangRightF = "";
//        if(EmptyUtil.isNotEmpty(left.getPanelType()) && left.getPanelType().equals(String.valueOf(SYS_PANEL_TYPE_JIAQIANG))){
//            jiaqiangLeftF = SYS_PANEL_PANELCONNECTOR_F;
//        }
//        if(EmptyUtil.isNotEmpty(right.getPanelType()) && right.getPanelType().equals(String.valueOf(SYS_PANEL_TYPE_JIAQIANG))){
//            jiaqiangRightF = SYS_PANEL_PANELCONNECTOR_F;
//        }
//
//        if (SYS_PANEL_DIRECTION_VERTICAL == direction) {
//            if (panelFace.getId().length() == 1) {
//                panelFace.setPanelConnector(SYS_PANEL_PANELCONNECTOR_00);
//                left.setPanelConnector(buildF(SYS_PANEL_PANELCONNECTOR_00,jiaqiangLeftF));
//                right.setPanelConnector(buildF(SYS_PANEL_PANELCONNECTOR_0A,jiaqiangRightF));
//            } else {
//                String pc = panelFace.getPanelConnector();
//                if (EmptyUtil.isEmpty(pc)) {
//                    log.warn("面板初始化-构造面板连接器异常1"
//                            + "\npanelFace：" + panelFace.toString()
//                            + "\nleft：" + left.toString()
//                            + "\nright：" + right.toString());
//                    return;
//                }
//                left.setPanelConnector(buildF(pc,jiaqiangLeftF));
//                right.setPanelConnector(buildF(pc.substring(0, 1) + SYS_PANEL_PANELCONNECTOR_A,jiaqiangRightF));
//            }
//        }
//        if (SYS_PANEL_DIRECTION_HORIZONTAL == direction) {
//            if (panelFace.getId().length() == 1) {
//                panelFace.setPanelConnector(SYS_PANEL_PANELCONNECTOR_00);
//                left.setPanelConnector(buildF(SYS_PANEL_PANELCONNECTOR_00,jiaqiangLeftF));
//                right.setPanelConnector(buildF(SYS_PANEL_PANELCONNECTOR_A0,jiaqiangRightF));
//            } else {
//                String pc = panelFace.getPanelConnector();
//                if (EmptyUtil.isEmpty(pc)) {
//                    log.warn("面板初始化-构造面板连接器异常2\npanelFace：" + panelFace.toString() + "\nleft：" + left.toString()
//                            + "\nright：" + right.toString());
//                    return;
//                }
//                left.setPanelConnector(buildF(pc,jiaqiangLeftF));
//                right.setPanelConnector(buildF(SYS_PANEL_PANELCONNECTOR_A + pc.substring(1),jiaqiangRightF));
//            }
//        }
//    }

    /**
     * 追加
     * @param s
     * @param jiaqiangF
     * @return
     */
//    private static String buildF(String s, String jiaqiangF) {
//        if(s.length() == 2){
//            return s+jiaqiangF;
//        }else if(s.length()==3){
//            return s.substring(0,2)+jiaqiangF;
//        }else{
//            return SYS_PANEL_PANELCONNECTOR_00+jiaqiangF;
//        }
//
//    }

    /**
     * 填充面板的切割信息和框条信息<br>
     * 规则如下：<br>
     *
     * @param panelFace
     * @param serial
     */
    public static void fillFrameLine(PanelFace panelFace, String serial) {
        if (null != panelFace) {
            int direction = panelFace.getDirection();
            if (direction == SYS_PANEL_DIRECTION_VERTICAL) {
                panelFace.setCutDirection(SYS_PANEL_CUT_DIRECTION_VERTICAL);
                FrameLine frameLine = new FrameLine(
                        getFrameLineType(panelFace,serial,SYS_PANEL_DIRECTION_VERTICAL),
                        panelFace.getPanelWidth());
                frameLine.setLineConnector(getLineConnector(direction,panelFace.getPanelConnector()));
                panelFace.setFrameLine(frameLine);
            } else if (direction == SYS_PANEL_DIRECTION_HORIZONTAL) {
                panelFace.setCutDirection(SYS_PANEL_CUT_DIRECTION_HORIZONTAL);
                FrameLine frameLine = new FrameLine(
                        getFrameLineType(panelFace,serial,SYS_PANEL_DIRECTION_HORIZONTAL)
                        , panelFace.getPanelLength());
                frameLine.setLineConnector(getLineConnector(direction,panelFace.getPanelConnector()));
                panelFace.setFrameLine(frameLine);
            } else {
                panelFace.setCutDirection(SYS_PANEL_CUT_DIRECTION_NONE);
                panelFace.setFrameLine(null);
            }
            PanelFace[] subPanels = panelFace.getSubPanels();
            if (null != subPanels) {
                for (PanelFace pf : subPanels) {
                    fillFrameLine(pf, serial);
                }
            }else{
                panelFace.setFrameLine(null);//没有子面板不需要中框
            }
        }
    }

    /**
     * 获取中框类型
     * 1：39CQ 39G 除底面板，当中框大于20M 为加强中框
     * 2：39XT 所有型号操作面盘管段，两边的为竖中框为 ：内角钢+装饰条
     * @param panelFace
     * @param serial
     * @param sysPanelDirection 切割方向
     * @return
     */
    private static String getFrameLineType(PanelFace panelFace, String serial, int sysPanelDirection) {
        if((serial.contains(SYS_UNIT_SERIES_39CQ) || serial.contains(SYS_UNIT_SERIES_39G))
                && !panelFace.getId().startsWith(SYS_PANEL_BOTTOM)) {

            //横切Length
            if(SYS_PANEL_DIRECTION_HORIZONTAL == sysPanelDirection){
                return panelFace.getPanelLength() >= PREFER_JIAQIANG_VERTIAL_LINE_VAL ? FrameLine.TYPE_JIAQIANG : FrameLine.TYPE_PUTONG;
            }else{//纵切width
                return panelFace.getPanelWidth() >= PREFER_JIAQIANG_VERTIAL_LINE_VAL ? FrameLine.TYPE_JIAQIANG : FrameLine.TYPE_PUTONG;
            }
        }

        if(serial.contains(SYS_UNIT_SERIES_39XT)){

            try {
                PanelFace[] subPanels = panelFace.getSubPanels();

                if (subPanels != null && subPanels.length > 1) {

                    String leftSectionMetaId = subPanels[0].getSectionMetaId();
                    String rightSectionMetaId = subPanels[1].getSectionMetaId();
                    if(EmptyUtil.isEmpty(leftSectionMetaId) && null != subPanels[0].getSubPanels() && subPanels[0].getSubPanels().length>1){
                        leftSectionMetaId = subPanels[0].getSubPanels()[0].getSectionMetaId();
                    }
                    if(EmptyUtil.isEmpty(rightSectionMetaId) && null != subPanels[1].getSubPanels() && subPanels[1].getSubPanels().length>1){
                        rightSectionMetaId = subPanels[1].getSubPanels()[0].getSectionMetaId();
                    }

                    String sectionMetaId = leftSectionMetaId+rightSectionMetaId;
                    if(sectionMetaId.contains(SectionTypeEnum.TYPE_COLD.getId())
                            || sectionMetaId.contains(SectionTypeEnum.TYPE_DIRECTEXPENSIONCOIL.getId())
                            || sectionMetaId.contains(SectionTypeEnum.TYPE_HEATINGCOIL.getId())
                            || sectionMetaId.contains(SectionTypeEnum.TYPE_STEAMCOIL.getId())
                            || sectionMetaId.contains(SectionTypeEnum.TYPE_STEAMCOIL.getId())){
                        return FrameLine.TYPE_JIAQIANG;
                    }
                }
            } catch (Exception e) {
            }

        }
        return FrameLine.TYPE_PUTONG;
    }

    private static String getLineConnector(int direction, String panelConnector) {
        if(panelConnector.length()>=2) {
            if (direction == SYS_PANEL_DIRECTION_VERTICAL) {
                return String.valueOf(panelConnector.toCharArray()[0]);
            } else if (direction == SYS_PANEL_DIRECTION_HORIZONTAL) {
                return String.valueOf(panelConnector.toCharArray()[1]);
            }
        }
        return "";
    }

    /**
     *
     * @param panelFace
     * @param horizontals
     * @param horizontalTypes
     * @param verticals
     * @param verticalTypes
     * @param cutStyle 0:竖切 1：横切
     */
    public static void slicePanelByRule(PanelFace panelFace, Integer[] horizontals, Integer[] horizontalTypes,
                                        Integer[] verticals, Integer[] verticalTypes, int cutStyle) {
        if (cutStyle == 0) {
            slicePanelByVerticalFirstRule(panelFace, horizontals, horizontalTypes, null, null, verticals, verticalTypes, null);
        } else if (cutStyle == 1) {
            slicePanelByHorizonFirstRule(panelFace, horizontals, horizontalTypes, null, null, verticals, verticalTypes);
        } else {
            log.error("Unknown parameter cutStyle: " + cutStyle);
        }
    }
    /**
     * 初始化面板分割的方法 1 - [先横切，再纵切] <br>
     * 按照行和列的平铺方式，整个的切割只分两层，先平铺行，再在行里面平铺列<br>
     * 可以表示数据结构，但是前端暂时不支持，同时不支持前端复杂的切割方式<br>
     *  @param panelFace 待切割面板数据
     * @param horizontals 横切数据
     * @param horizontalTypes 横切面板类型
     * @param vRepeatSlice 纵向向独立切割数据
     * @param vTypeRepeatSlice 纵向独立切割面板类型
     * @param verticals 纵切数据
     * @param verticalTypes 纵切面板类型
     */
    public static void slicePanelByHorizonFirstRule(PanelFace panelFace,
                                                    Integer[] horizontals, Integer[] horizontalTypes,
                                                    List<Integer[]> vRepeatSlice, List<Integer[]> vTypeRepeatSlice, Integer[] verticals, Integer[] verticalTypes) {
        /* 如果横/纵数组都为空，不需要切割 */
        if (ArrayUtils.isEmpty(horizontals) && ArrayUtils.isEmpty(verticals)) {
            panelFace.setDirection(SYS_PANEL_DIRECTION_NONE);
            panelFace.setPanelType(String.valueOf(SYS_PANEL_TYPE_PUTONG));
            return;
        }
        int plength = panelFace.getPanelLength();
        int pwidth = panelFace.getPanelWidth();
        /* 初始化横with父面板长 */
        if (ArrayUtils.isEmpty(horizontals)) {
            horizontals = new Integer[] { pwidth };
            horizontalTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };
        }
        /* 初始化纵向with父面板宽 */
        if (ArrayUtils.isEmpty(verticals)) {
            verticals = new Integer[] { plength };
            verticalTypes = new Integer[] { SYS_PANEL_TYPE_PUTONG };
        }

        PanelFace phface = null;
        List<PanelFace> subFaceList = new ArrayList<>();
        for (int i = 0; i < horizontals.length; i++) {
            int theHeight = horizontals[i];
            String pfhType = String.valueOf(horizontalTypes[i]);
            // 如果宽度和父面板相等，不需要做切割，直接把父面板设置成当前面板
            if (horizontals[i] == pwidth) {
                phface = panelFace;
                panelFace.setDirection(SYS_PANEL_DIRECTION_VERTICAL);
                phface.setPanelType(pfhType);
            } else {
                // 创建一个新的横向子面板，并加入到父面板的孩子
                panelFace.setDirection(SYS_PANEL_DIRECTION_HORIZONTAL);
                phface = createPanelFace(panelFace.getPanelLength(), theHeight);
                phface.setId(panelFace.getId() + SYS_PUNCTUATION_LOW_HYPHEN + i);
                phface.setPanelType(pfhType);
                subFaceList.add(phface);
            }
            // 做纵向切割
            List<PanelFace> subVFaceList = new ArrayList<>();
            //如果需要横向独立切割，重置切割参数
            if(EmptyUtil.isNotEmpty(vRepeatSlice) && EmptyUtil.isNotEmpty(vTypeRepeatSlice)){
                verticals = vRepeatSlice.get(i);
                verticalTypes = vTypeRepeatSlice.get(i);
            }
            PanelFace pvface = null;
            for (int j = 0; j < verticals.length; j++) {
                int theWidth = verticals[j];
                String pfvType = String.valueOf(verticalTypes[j]);
                if (verticals[0] == plength) {
                    pvface = phface;
                    pvface.setDirection(SYS_PANEL_DIRECTION_NONE);
                    if (null == phface.getPanelType() || String.valueOf(SYS_PANEL_TYPE_PUTONG).equals(pfhType)) {
                        //phface.setPanelType(pfvType);
                    }
                } else {
                    // 创建一个新的横向子面板，并加入到父面板的孩子
                    phface.setDirection(SYS_PANEL_DIRECTION_VERTICAL);
                    pvface = createPanelFace(theWidth, theHeight);
                    pvface.setId(phface.getId() + SYS_PUNCTUATION_LOW_HYPHEN + j);
                    if (null == pvface.getPanelType()) {
                        //默认面板类型为普通
                        pvface.setPanelType(String.valueOf(SYS_PANEL_TYPE_PUTONG));
                        //1.（竖切=横切）且类型为非普通面板，才处理当前孩子面板类型为非普通
                        if (!String.valueOf(SYS_PANEL_TYPE_PUTONG).equals(pfvType)
                                && pfhType.equals(pfvType) && theWidth >= 3) {
                            pvface.setPanelType(pfhType);
                        }
                        //2.没有横切：不用特殊处理
                        if (theWidth == plength) {
                            pvface.setPanelType(pfhType);
                        }
                    }
                    pvface.setSectionMetaId(panelFace.getSectionMetaId());
                    subVFaceList.add(pvface);
                }
            }
            if (!subVFaceList.isEmpty()) {
                PanelFace[] subs = new PanelFace[subVFaceList.size()];
                subs = subVFaceList.toArray(subs);
                phface.setSubPanels(subs);
            }

        }
        if (!subFaceList.isEmpty()) {
            PanelFace[] subs = new PanelFace[subFaceList.size()];
            subs = subFaceList.toArray(subs);
            panelFace.setSubPanels(subs);
        }

    }
    public static void genPanelFaceFromPO(PanelFace panelFace, PanelXSLXPO po) {
        if (null == po) {
            log.warn("面板初始化-数据库逻辑-元数据为空，分割取消");
            return;
        }
        Integer[] horizontals, verticals, horizontalTypes, verticalTypes;
        String horizontalsStr = po.getHr().split(SYS_PUNCTUATION_COMMA)[0];
        String horizontalsTypeStr = po.getHp().split(SYS_PUNCTUATION_COMMA)[0];

        //横向独立切割（比例、切割后类型）集合
        List<Integer[]> hRepeatSlice = buildRepeatSliceList(po.getHr());
        List<Integer[]> hTypeRepeatSlice = buildRepeatSliceList(po.getHp());

        String verticalsStr = po.getWr().split(SYS_PUNCTUATION_COMMA)[0];
        String verticalsTypeStr = po.getWp().split(SYS_PUNCTUATION_COMMA)[0];

        //纵向独立切割（比例、切割后类型）集合
        List<Integer[]> vRepeatSlice = buildRepeatSliceList(po.getWr());
        List<Integer[]> vTypeRepeatSlice = buildRepeatSliceList(po.getWp());

        String[] hArray = horizontalsStr.replace(SYS_PUNCTUATION_PLUS, SYS_PUNCTUATION_COMMA).split(SYS_PUNCTUATION_COMMA);
        String[] htArray = horizontalsTypeStr.replace(SYS_PUNCTUATION_PLUS, SYS_PUNCTUATION_COMMA).split(SYS_PUNCTUATION_COMMA);
        String[] vArray = verticalsStr.replace(SYS_PUNCTUATION_PLUS, SYS_PUNCTUATION_COMMA).split(SYS_PUNCTUATION_COMMA);
        String[] vtArray = verticalsTypeStr.replace(SYS_PUNCTUATION_PLUS, SYS_PUNCTUATION_COMMA).split(SYS_PUNCTUATION_COMMA);
        horizontals = new Integer[hArray.length];
        verticals = new Integer[vArray.length];
        horizontalTypes = new Integer[htArray.length];
        verticalTypes = new Integer[vtArray.length];

        if (vArray.length != vtArray.length) {
            log.warn("面板初始化-数据库逻辑-查询数据库元纵切数据异常\n" + po.toString());
            return;
        }
        if (hArray.length != htArray.length) {
            log.warn("面板初始化-数据库逻辑-查询数据库元横切数据异常\n" + po.toString());
        }

        for (int i = 0; i < hArray.length; i++) {
            horizontals[i] = NumberUtil.convertStringToDoubleInt(hArray[i]);
            horizontalTypes[i] = NumberUtil.convertStringToDoubleInt(htArray[i]);
        }
        for (int i = 0; i < vArray.length; i++) {
            verticals[i] = NumberUtil.convertStringToDoubleInt(vArray[i]);
            verticalTypes[i] = NumberUtil.convertStringToDoubleInt(vtArray[i]);
        }

        /**后台计算参数复制**/
        if(StringUtils.isNotEmpty(po.getSectionTypeNo())&&SectionTypeEnum.getSectionTypeByCodeNum(po.getSectionTypeNo())!=null){
            panelFace.setSectionMetaId(SectionTypeEnum.getSectionTypeByCodeNum(po.getSectionTypeNo()).getId());
        }
        panelFace.setSectionDoorOpendAndViewport(po.getSectionDoorOpendAndViewport());
        setFanParameter(panelFace);

        int plength = panelFace.getPanelLength();
        int pwidth = panelFace.getPanelWidth();
        /*
            面切割参数Hr 为0：按照面板高度进行（空面板+面板）处理
            面板切割：混合段在端面，根据是否选择回风，进行特殊逻辑切割
                (根据excel H 面板高度，在面板上面留有空面板例如：
                        FanType PartType H W Hr Hp Wr Wp
                        39G1317 1        7 17 0 0 17 0
                        切割结果为：上面6(13-7 : pwidth-po.getH())的空面板，下面7(po.getH())的普通面板
                )
        */
        //新建切割对象
        PanelXSLXPO damperPo = null;
        if (panelFace.getId().equals(SYS_PANEL_FRONT) || panelFace.getId().equals(SYS_PANEL_BACK) || isMiddleEndView(panelFace.getId())) {
            /* 初始化横with父面板长 */
            if ("0".equals(horizontalsStr)) {
                horizontals = new Integer[]{pwidth-NumberUtil.convertStringToDoubleInt(po.getH()),NumberUtil.convertStringToDoubleInt(po.getH())};
                horizontalTypes = new Integer[]{SYS_PANEL_TYPE_WU,SYS_PANEL_TYPE_PUTONG};

                //初始化切割后空面板下面的普通面板excelPO 切割对象
                damperPo = new PanelXSLXPO();
                damperPo.setH(SYS_BLANK+po.getH());
                damperPo.setW(SYS_BLANK+plength);
                damperPo.setWr(array2SplitStr(verticals));
                damperPo.setWp(array2SplitStr(verticalTypes));
                damperPo.setHr(SYS_BLANK+po.getH());
                damperPo.setHp(SYS_BLANK+SYS_PANEL_TYPE_PUTONG);
                damperPo.setSectionMetaId(SectionTypeEnum.getSectionTypeByCodeNum(po.getSectionTypeNo()).getId());

                //重置切割后空面板下面的普通面板,为整张普通面板
                verticals = new Integer[]{plength};
                verticalTypes = new Integer[]{SYS_PANEL_TYPE_PUTONG};

                //部分小机型整个面板为空面板 SideDamper.xlsx 0608 0609
                if ("0".equals(po.getH())) {
                    panelFace.setPanelType(String.valueOf(SYS_PANEL_TYPE_WU));
                    horizontals = new Integer[]{pwidth};
                    horizontalTypes = new Integer[]{SYS_PANEL_TYPE_WU};
                    verticals = new Integer[]{plength};
                    verticalTypes = new Integer[]{SYS_PANEL_TYPE_WU};
                }
            }
        }
        // printPODataArray(horizontals, horizontalTypes, verticals, verticalTypes);
        // 组装叶子面板
        if(EmptyUtil.isNotEmpty(vRepeatSlice) && EmptyUtil.isNotEmpty(vTypeRepeatSlice)){//纵向需要基于横向结果，进行每个结果独立规则切割
            slicePanelByHorizonFirstRule(panelFace, horizontals, horizontalTypes,vRepeatSlice,vTypeRepeatSlice, verticals, verticalTypes);
        }else{
            slicePanelByVerticalFirstRule(panelFace, horizontals, horizontalTypes, hRepeatSlice,hTypeRepeatSlice,verticals, verticalTypes, null);
        }
        // 封装叶子面板为树状结构
        // shakePanelByBitreeRule(panelFace);

        //excelPO方式切割后空面板下面的普通面板
        if(EmptyUtil.isNotEmpty(damperPo) && EmptyUtil.isNotEmpty(panelFace.getSubPanels()))
            genPanelFaceFromPO(panelFace.getSubPanels()[1], damperPo);
    }

    private static List<Integer[]> buildRepeatSliceList(String hrOrHP) {

        List<Integer[]> repeatSliceList = null;
        if(hrOrHP.split(SYS_PUNCTUATION_COMMA).length>1){
            repeatSliceList = new ArrayList<>();
            for (String rOrp : hrOrHP.split(SYS_PUNCTUATION_COMMA)) {
                String[] rOroArray = rOrp.replace(SYS_PUNCTUATION_PLUS, SYS_PUNCTUATION_COMMA).split(SYS_PUNCTUATION_COMMA);
                Integer[] horizontalsOrTypes = new Integer[rOroArray.length];
                for (int i = 0; i < rOroArray.length; i++) {
                    horizontalsOrTypes[i] = NumberUtil.convertStringToDoubleInt(rOroArray[i]);
                }
                repeatSliceList.add(horizontalsOrTypes);
            }
        }
        return repeatSliceList;
    }

    /**
     * 判断id是否为端面中间面
     * @param panelId
     * @return
     */
    public static boolean isMiddleEndView(String panelId){
        panelId = panelId.split(SYS_PUNCTUATION_LOW_HYPHEN)[0];
        if(NumberUtil.convertStringToDoubleInt(panelId)>5){
            return true;
        }
        return false;
    }

    /**
     * 面板切割数据格式化
     * 数组进行字符串拼接形式转换，SYS_PUNCTUATION_PLUS加号隔开
     * @param SafetyMeasure
     * @return
     */
    public static String array2SplitStr(Integer[] SafetyMeasure){
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<SafetyMeasure.length;i++){
            sb.append(SafetyMeasure[i]);
            if(i<SafetyMeasure.length-1)
                sb.append(SYS_PUNCTUATION_PLUS);
        }
        return sb.toString();
    }
    public static PanelFace createPanelFace(String face, AhuPartition partition) {
        PanelFace panelFace = null;
        if (face.equals(SYS_PANEL_TOP) || face.equals(SYS_PANEL_BOTTOM)) {
            // 顶面底面，使用长和宽
            panelFace = createPanelFace(partition.getLength(), partition.getWidth());
        } else if (face.equals(SYS_PANEL_FRONT) || face.equals(SYS_PANEL_BACK) || isMiddleEndView(face)) {
            // 端面，使用宽和高
            panelFace = createPanelFace(partition.getWidth(), partition.getHeight());
        } else {
            // 非操作面和操作面，使用长和高
            panelFace = createPanelFace(partition.getLength(), partition.getHeight());
        }
        panelFace.setId(face);
        return panelFace;
    }

    /**
     * 面板合并
     * @param panel1
     * @param panel2
     * @param panelFace
     * @param product
     * @return
     */
    public static String mergePanel(String panel1, String panel2, PanelFace panelFace, String product) {

        LinkedHashMap<String, LinkedHashMap<String, PanelFace>> listPanelFulls = new LinkedHashMap<String, LinkedHashMap<String, PanelFace>>();
        LinkedHashMap<String, PanelFace> allPanels = new LinkedHashMap<String, PanelFace>();
        PanelFaceUtil.convertTreeToMap(null, panelFace, listPanelFulls, allPanels, 0);

        PanelFaceUtil.conbinePanel(panel1,panel2,listPanelFulls,allPanels);
        try {
            PanelFaceUtil.convertTreeToMap(null, panelFace, listPanelFulls, allPanels, 0);
            PanelFaceUtil.resetConnectorType(product,listPanelFulls,allPanels,panelFace.getDirection());
        } catch (Exception e) {
            log.error("mergePanel error :",e);
        }
        Gson gson = new Gson();
        String retMergedPanelJson = gson.toJson(panelFace);
        return retMergedPanelJson;
    }
}
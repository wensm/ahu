package com.carrier.ahu.util.partition;


import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.carrier.ahu.calculator.panel.PanelXSLXPO;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.model.partition.*;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.report.SPaneldXtDoor;
import com.carrier.ahu.metadata.entity.report.XtDoor;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.param.PositivePressureDoorParam;
import com.carrier.ahu.po.AhuLayout;
import com.carrier.ahu.positivepressuredoor.PositivePressureDoorUtil;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.NumberUtil;
import com.carrier.ahu.util.ahu.AhuParamUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.Map.Entry;

import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.common.model.partition.AhuPartition.S_MKEY_METAID;
import static com.carrier.ahu.common.model.partition.AhuPartition.S_MKEY_METAJSON;
import static com.carrier.ahu.common.model.partition.FrameLine.*;
import static com.carrier.ahu.constant.CommonConstant.*;
import static com.carrier.ahu.length.util.SystemCountUtil.gteBigUnit;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_POSITIVE_PRESSURE_DOOR;
import static com.carrier.ahu.util.meta.SectionMetaUtils.getMetaSectionKey;
import static com.carrier.ahu.vo.SystemCalculateConstants.*;
import static java.util.Locale.SIMPLIFIED_CHINESE;

/**
 *
 * It is a tool class to generate partition definition for an ahu </br>
 *
 * Created by liujianfeng on 2017/9/1.
 */
@Slf4j
public class AhuPartitionGenerator {

    private static Gson gson = new Gson();

    public final static int PARTITION_MAX_LENGTH = 30;
    public final static int PARTITION_MIN_LENGTH = 6;
    public final static int ACCESS_MIN_LENGTH_CQ_G_WITHDOOR = 4;
    public final static int ACCESS_MIN_LENGTH = 5;
    public final static int PARTITION_WITHDOOR_MIN_LENGTH = 5;
    public final static Map<String, Integer> AHU_PRODUCT_CASING_WIDTH = new HashMap<String, Integer>() {
        {
            put(AHU_PRODUCT_39G, 50);
            put(AHU_PRODUCT_39CQ, 90);
            put(AHU_PRODUCT_39XT, 104);
        }
    };
    public final static int PREFER_PANEL_WIDTH = 24;
    public final static int PREFER_PANEL_WIDTH1 = 25;//CQ 顶面、底面使用25以上宽度进行逻辑分割
    public final static int PREFER_PANEL_LENGTH = 11;

    /*1：39CQ 39G 除底面板，当中框大于等于20M 为加强中框*/
    public final static int PREFER_JIAQIANG_VERTIAL_LINE_VAL = 20;

    /* XT是计算每个面的总长度，然后除以标准面板宽度900后往上取整所得
       XT面板只有两种规则，宽度方面或者高度方面，比如XT0608，只有两种规则,一种是高度方面6M*900,宽度方向8M*900 */
    public final static int XT_PREFER_PANEL_LENGTH = 9;//XT系列纵切

    public final static int PARTITION_LENGTH_ADD_UP = 500;

    public final static int UP_LEFT = 0;
    public final static int UP_RIGHT = 1;
    public final static int BOTTOM_LEFT = 2;
    public final static int BOTTOM_RIGHT = 3;

    public final static String SAVEFROMPANEL = "panel";
    public final static String SAVEFROMPANELPARTION= "partition";
    public final static String xt_Split_height= "height";
    public final static String xt_Split_width = "width";
    public final static String HB_39 = "39HB";

    /* ahu参与面板切割的参数 */
    public static final String inWindDirection = "meta.ahu.inWindDirection";
    public static final String doorRienTation = "meta.ahu.doororientation";
    public static final String baseType = "meta.ahu.baseType";
    public static final String doororientation = "meta.ahu.doororientation";//检修门方向
    public static final String pipeOrientation = "meta.ahu.pipeorientation";//接管方向
    public static List<String> TRANSFER_AHUPARAMETER_KEYS = Arrays.asList(
            inWindDirection,doorRienTation,baseType,doororientation,pipeOrientation);

    public final static Set<String> UNSPLITABLE_SECTION = new HashSet<String>() {
        private static final long serialVersionUID = 1L;
        {
            add(SectionTypeEnum.TYPE_COLD.getId());
            add(SectionTypeEnum.TYPE_STEAMCOIL.getId());
            add(SectionTypeEnum.TYPE_HEATINGCOIL.getId());
            add(SectionTypeEnum.TYPE_ELECTRICHEATINGCOIL.getId());
            add(SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId());
//            add(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId());
            add(SectionTypeEnum.TYPE_HEATRECYCLE.getId());
            add(SectionTypeEnum.TYPE_CTR.getId());
            add(SectionTypeEnum.TYPE_ELECTROSTATICFILTER.getId());
            add(SectionTypeEnum.TYPE_ATTENUATOR.getId());
        }
    };
    public final static Set<String> SECTION_WITH_DOOR = new HashSet<String>() {
        private static final long serialVersionUID = 1L;
        {
            add(SectionTypeEnum.TYPE_MIX.getId());
            add(SectionTypeEnum.TYPE_DISCHARGE.getId());
            add(SectionTypeEnum.TYPE_FAN.getId());
            add(SectionTypeEnum.TYPE_ACCESS.getId());
            add(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
//            add(SectionTypeEnum.TYPE_COMPOSITE.getId());
//            add(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId());
//            add(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId());
//            add(SectionTypeEnum.TYPE_HEPAFILTER.getId());
        }
    };
    public final static Set<String> SECTION_CAN_PASS = new HashSet<String>() {
        private static final long serialVersionUID = 1L;
        {
            add(SectionTypeEnum.TYPE_MIX.getId());
            add(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId());
            add(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
            add(SectionTypeEnum.TYPE_DISCHARGE.getId());
            add(SectionTypeEnum.TYPE_ACCESS.getId());
            add(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId());
            add(SectionTypeEnum.TYPE_CTR.getId());
        }
    };
    public final static Set<String> SECTION_HUMIDIFIER = new HashSet<String>() {
        private static final long serialVersionUID = 1L;
        {
            add(SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId());
            add(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId());
            add(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId());
            add(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId());
        }
    };
    //无蜗壳使用
    public final static Set<String> SECTION_WITH_DOOR_WWK = new HashSet<String>() {
        private static final long serialVersionUID = 1L;
        {
            add(SectionTypeEnum.TYPE_MIX.getId());
            add(SectionTypeEnum.TYPE_DISCHARGE.getId());
            add(SectionTypeEnum.TYPE_ACCESS.getId());
            add(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
        }
    };
    //高效过滤段V箱型，前/后有空段/混合段或者出风段时，且段长>=6M时， 可省略（6M+3M、3M+6M）的规则。
    public final static Set<String> SECTION_WITH_DOOR_HEPAFilter = new HashSet<String>() {
        private static final long serialVersionUID = 1L;
        {
            add(SectionTypeEnum.TYPE_MIX.getId());
            add(SectionTypeEnum.TYPE_DISCHARGE.getId());
            add(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId());
            add(SectionTypeEnum.TYPE_ACCESS.getId());
        }
    };
    public static final class Meta {
        public static String JOINING_KEYS = "joiningKeys";
        public static String MAX_LENGTH = "maxLength";
        public static String MIN_LENGTH = "minLength";
    }

    /* 面板中框,计算相关参数 */
    private static final String calPanelSplitKey = "_";
    private static final String calPanel = "PANEL";
    private static final String calPanelFrameLine = "FLINE";
    /** 水盘出水孔第二孔的位置 M数》字母转换**/
    public final static Map<String, String> KAIKONG = new LinkedHashMap<String, String>() {
        {
            int j=1;
            for(int i = 65; i <= 90; i++){
                put(""+j, String.valueOf((char)i));
                j++;
            }
        }
    };
    /**
     * 解析当前的布局信息，如果布局信息为空，就默认直接生成一个style：00的布局
     *
     * @param unit
     * @param partList
     * @return
     */
    public static AhuLayout getLayout(Unit unit, List<Part> partList) {
        String lstr = unit.getLayoutJson();
        if (StringUtils.isEmpty(lstr)) {
            AhuLayout layout = new AhuLayout();
            layout.setStyle(00);
            int[][] data = new int[1][partList.size()];
            Iterator<Part> it = partList.iterator();
            int index = 1;
            while (it.hasNext()) {
                it.next();
                data[0][index - 1] = index;
                index++;
            }
            layout.setLayoutData(data);
            return layout;
        } else {
            return gson.fromJson(lstr, AhuLayout.class);
        }

    }

    public static Partition generatePartition(String ahuId, Unit unit, List<Part> partList) {
        // generate the partition on the fly
        Partition partition = new Partition();
        partition.setPartitionJson(generateUnitStringWithPartitions(unit, partList));
        partition.setUnitid(ahuId);
        partition.setPid(unit.getPid());
        return partition;
    }

    private static String generateUnitStringWithPartitions(Unit unit, List<Part> partList) {
        AhuParam ahuParam = AhuParamUtils.getAhuParam(unit, partList, "");
        AhuLayout layout = getLayout(unit, partList);
        List<AhuPartition> partitions = new ArrayList<AhuPartition>();
        int[] dimension = AhuUtil.getHeightAndWidthOfAHU(unit);
        List<int[]> layoutDatas = getMergedLayoutData(layout);
        int partitionPos = 0;
        AhuPartition prePartition = null;
        for (int lpos = 0; lpos < layoutDatas.size(); lpos++) {
            int[] layoutData = layoutDatas.get(lpos);
            List<List<Map<String, Object>>> allMergedSections = getMergedPartSections(ahuParam, partList, layoutData);

            List<AhuPartition> layoutPartitions = new ArrayList<AhuPartition>();
            AhuPartition partition = createAhuPartition(unit.getProduct(),dimension, partitionPos++);
            // 在分段前确定需要分成的段的数量，找到一个平均长度
            double avgPartitionLength = getAvgPartitionLength(layoutData, partList);

            for (int sindex = 0; sindex < allMergedSections.size(); sindex++) {
                List<Map<String, Object>> mergedSections = allMergedSections.get(sindex);
                int sectionL = 0;
                for (Map<String, Object> section : mergedSections) {
                    sectionL += AhuPartition.getSectionLOfSection(section);
                }
                // 板式热回收段下一个段是旁通(混合段或者出风段), 不允许分段
                if (LayoutStyleEnum.PLATE.style() == layout.getStyle() && lpos == BOTTOM_RIGHT && sindex == 0) {
                    String nextSectionId = AhuPartition.getMetaIdOfSection(mergedSections.get(0));
                    if (SectionTypeEnum.TYPE_MIX.getId().equals(nextSectionId)
                            || SectionTypeEnum.TYPE_DISCHARGE.getId().equals(nextSectionId)) {
                        prePartition.getSections().addAll(mergedSections);
                        prePartition.setLength(prePartition.getLength() + sectionL);
                        continue; // 继续计算下一个分段
                    }
                }
                // 根据平均段长的数据，等分 分段
                if (partition.getLength() + sectionL >= avgPartitionLength) {
                    // 新回排段不能单独分段
                    if (!partition.getSections().isEmpty()
                            && AhuPartitionValidator.validateSingleCombinedMixingChamberSection(partition, ahuParam)) {
                        // 创建一个新分段
                        if (isAirflowReversed(layout, lpos)) {
                            Collections.reverse(partition.getSections());
                            layoutPartitions.add(0, partition);
                        } else {
                            layoutPartitions.add(partition);
                        }
                        partition = createAhuPartition(unit.getProduct(), dimension, partitionPos++);
                    }
                }
                // 增加段到已分好的段
                partition.getSections().addAll(mergedSections);
                partition.setLength(partition.getLength() + sectionL);
            }
            if (isAirflowReversed(layout, lpos)) {
                Collections.reverse(partition.getSections());
                layoutPartitions.add(0, partition);
            } else {
                layoutPartitions.add(partition);
            }
            // 板式热回收段，保留下层的热回收段分段
            if (LayoutStyleEnum.PLATE.style() == layout.getStyle() && lpos == BOTTOM_LEFT) {
                prePartition = layoutPartitions.get(layoutPartitions.size() - 1);
            }
            appendTransferAhuparameters(layoutPartitions,ahuParam,layout);
            partitions.addAll(layoutPartitions);
        }

        // 重新排序分段，在前台更正后可以去掉这个部分
        partitionPos = 0;
        int sectionPos = 0;
        for (AhuPartition newOrderPartition : partitions) {
            newOrderPartition.setPos(partitionPos++);
            for (Map<String, Object> section : newOrderPartition.getSections()) {
                section.put(AhuPartition.S_MKEY_POS, sectionPos++);
            }
        }

        // reorder partition position for double layer AHUs
        AhuPartitionUtils.reorderPartition(layout, partitions);

        // Create panel
        //populatePartitionPanel(unit.getSeries(), partitions); //分段不进行面板初始化

        return gson.toJson(partitions);
    }

    /**
     * 添加ahu 需要传导的参数到分段类
     * @param partitions
     * @param ahuParam
     * @param layout
     */
    private static void appendTransferAhuparameters(List<AhuPartition> partitions, AhuParam ahuParam, AhuLayout layout) {
        Map<String, Object> transferAhuparameters = getAhuParameters(ahuParam,layout);
        for (AhuPartition partition : partitions) {
            partition.setAhuParameters(transferAhuparameters);
        }
    }

    private static Map<String, Object> getAhuParameters(AhuParam ahuParam, AhuLayout layout) {
        Map<String, Object> transferAhuparameters = new HashMap<>();
        for (int i = 0; i < TRANSFER_AHUPARAMETER_KEYS.size(); i++) {
            transferAhuparameters.put(TRANSFER_AHUPARAMETER_KEYS.get(i),ahuParam.getParams().get(TRANSFER_AHUPARAMETER_KEYS.get(i)));
        }
        transferAhuparameters.put(METASEXON_LAYOUT_STYLE,layout.getStyle());
        return transferAhuparameters;
    }

    public static String clearPanels(List<AhuPartition> partitions, String series, AhuLayout layout){
        for (int i = 1; i <= partitions.size(); i++) {
            AhuPartition partition = partitions.get(i-1);
            //重置宽高
            int[] dimension = AhuUtil.getHeightAndWidthOfAHU(series);
            partition.setHeight(dimension[0]);
            partition.setWidth(dimension[1]);

            //panel
            partition.setPanels(null);
            //底座
            partition.setBase("");

            //保存分段更新ahuparameters layoutStyle
            Map<String, Object> ahuParameters = partition.getAhuParameters();
            if(EmptyUtil.isNotEmpty(partition.getAhuParameters()) && EmptyUtil.isNotEmpty(layout)){
                ahuParameters.put(METASEXON_LAYOUT_STYLE,layout.getStyle());
                partition.setAhuParameters(ahuParameters);
            }

        }
        return gson.toJson(partitions);
    }
    private static List<List<Map<String, Object>>> getMergedPartSections(AhuParam ahuParam, List<Part> parts,
                                                                         int[] layoutData) {
        List<List<Map<String, Object>>> allMergedSections = new ArrayList<>();
        List<Map<String, Object>> mergedSections = new ArrayList<>();
        Map<String, Object> preSection = null;
        for (int i = 0; i < layoutData.length; i++) {
            Map<String, Object> nextSection = createPartitionSection(parts.get(layoutData[i] - 1));
            if (isSplitableSection(preSection, nextSection, ahuParam)) { // 可拆分的段不合并
                mergedSections = new ArrayList<>();
                mergedSections.add(nextSection);
                allMergedSections.add(mergedSections);
            } else { // 合并不可拆分的段
                mergedSections = allMergedSections.get(allMergedSections.size() - 1);
                mergedSections.add(nextSection);
            }
            preSection = nextSection;
        }
        return allMergedSections;
    }

    private static double getAvgPartitionLength(int[] layoutData, List<Part> partList) {
        // 在分段前确定需要分成的段的数量，找到一个平均长度
        double ahuLength = 0;
        for (int j = 0; j < layoutData.length; j++) {
            int pos = layoutData[j];
            Part part = partList.get(pos - 1);
            ahuLength = ahuLength + AhuUtil.getRealSectionLength(part);
        }
        double avgLength = ahuLength / Math.ceil(ahuLength / PARTITION_MAX_LENGTH / 100);
        int maxPartitionLength = getMaxPartitionLength();
        if ((maxPartitionLength - avgLength) > PARTITION_LENGTH_ADD_UP) {
            return avgLength + PARTITION_LENGTH_ADD_UP;
        }
        return maxPartitionLength;
    }

    private static Map<String, Object> createPartitionSection(Part part) {
        int sectionL = AhuUtil.getRealSectionLength(part);
        double weight = AhuUtil.getWeight(part);
        return AhuPartition.createPartitionSection(part, part.getPosition() - 1, sectionL, weight);
    }

    /**
     * 根据Layout重新分组段，再进行分段。</br>
     * 转轮热回收：上层气流从右至左，下层气流从左至右</br>
     * 板式热回收：上层左侧气流从左至右，上层右侧气流从右至左，下层左侧气流从右至左，下层右侧气流从左至右</br>
     * 其它：单层气流从左至右</br>
     *
     * @param layout
     * @return
     */
    private static List<int[]> getMergedLayoutData(AhuLayout layout) {
        List<int[]> splitLayouts = new ArrayList<>();
        int[][] layoutData = layout.getLayoutData();
        if (LayoutStyleEnum.WHEEL.style() == layout.getStyle()) { // 合并上层，合并下层
            int[] upLayout = ArrayUtils.addAll(layoutData[UP_LEFT], layoutData[UP_RIGHT]);
            ArrayUtils.reverse(upLayout); // 气流相反
            splitLayouts.add(upLayout);
            splitLayouts.add(ArrayUtils.addAll(layoutData[BOTTOM_LEFT], layoutData[BOTTOM_RIGHT]));
        } else if (LayoutStyleEnum.PLATE.style() == layout.getStyle()) {
            splitLayouts.add(layoutData[UP_LEFT]);
            int[] upRightLayout = layoutData[UP_RIGHT];
            ArrayUtils.reverse(upRightLayout); // 气流相反
            splitLayouts.add(upRightLayout);
            int[] bottomLeftLayout = layoutData[BOTTOM_LEFT];
            ArrayUtils.reverse(bottomLeftLayout); // 气流相反
            splitLayouts.add(bottomLeftLayout);
            splitLayouts.add(layoutData[BOTTOM_RIGHT]);
        } else if (LayoutStyleEnum.VERTICAL_UNIT_1.style() == layout.getStyle()
                || LayoutStyleEnum.VERTICAL_UNIT_2.style() == layout.getStyle()
                || LayoutStyleEnum.DOUBLE_RETURN_1.style() == layout.getStyle()
                || LayoutStyleEnum.DOUBLE_RETURN_2.style() == layout.getStyle()
                || LayoutStyleEnum.SIDE_BY_SIDE_RETURN_1.style() == layout.getStyle()
                || LayoutStyleEnum.SIDE_BY_SIDE_RETURN_2.style() == layout.getStyle()) {
            splitLayouts.add(layoutData[UP_LEFT]);
            splitLayouts.add(layoutData[BOTTOM_LEFT]);
        } else { // 单层
            splitLayouts.add(layoutData[0]);
        }
        return splitLayouts;
    }

    private static boolean isAirflowReversed(AhuLayout layout, int pos) {
        if ((LayoutStyleEnum.WHEEL.style() == layout.getStyle() && pos == UP_LEFT)
                || (LayoutStyleEnum.PLATE.style() == layout.getStyle() && (pos == UP_RIGHT || pos == BOTTOM_LEFT))) {
            return true;
        }
        return false;
    }

    private static boolean isSplitableSection(Map<String, Object> preSection, Map<String, Object> nextSection,
                                              AhuParam ahuParam) {
        if (preSection != null && nextSection != null) {
            String preSectionId = AhuPartition.getMetaIdOfSection(preSection);
            String nextSectionId = AhuPartition.getMetaIdOfSection(nextSection);
            return (!UNSPLITABLE_SECTION.contains(preSectionId) || !UNSPLITABLE_SECTION.contains(nextSectionId))
                    && AhuPartitionValidator.validateSplittedCtrAndFanSection(preSectionId, nextSectionId)
                    && AhuPartitionValidator.validateAccessDoorOfAdjacentSection(preSection, nextSection, ahuParam)
                    && AhuPartitionValidator.validateSplittedFilterSection(preSection, nextSection, ahuParam)
                    && AhuPartitionValidator.validateWheelHeatRecycleBypass(preSection, nextSection)
                    && AhuPartitionValidator.validatePlateHeatRecycleBypass(preSection, nextSection);
        }
        return true;
    }

    /**
     * 新建一个全新的Partition
     * @param product
     * @param dimension
     * @param pos
     * @return
     */
    private static AhuPartition createAhuPartition(String product, int[] dimension, int pos) {
        AhuPartition partition = new AhuPartition();
        partition.setHeight(dimension[0]);
        partition.setWidth(dimension[1]);
        partition.setSections(new LinkedList<>());
        partition.setLength(0);
        partition.setPos(pos);
        partition.setCasingWidth(AHU_PRODUCT_CASING_WIDTH.get(product));
        return partition;
    }

    /**
     * 填充面板AB
     * @param panelJson
     * @return
     */
    public static String initab(String panelJson) throws Exception{
        Gson gson = new Gson();
        Map<String,PanelFace> thePanels = gson.fromJson(panelJson, new TypeToken<Map<String,PanelFace>>() {}.getType());

        for(Entry<String, PanelFace> panel:thePanels.entrySet()) {
            try {
                toSetConnectorType(SYS_UNIT_SERIES_39CQ, panel.getValue());
            }catch(Exception e) {
                log.error("initab>Reset Panel Error:" + e.getMessage());
                log.debug(e.getMessage(), e);
            }
        }
        String retABPanelsJson = gson.toJson(thePanels);
        return retABPanelsJson;
    }

    /**
     * 面板合并
     * @param panel1
     * @param panel2
     * @param panelJson
     * @param product
     * @return
     * @throws Exception
     */
    public static String mergePanel(String panel1, String panel2, String panelJson, String product) throws Exception{
        Gson gson = new Gson();
        PanelFace panelFace = gson.fromJson(panelJson, new TypeToken<PanelFace>() {}.getType());
        String mergedPanelJson = AhuPartitionGeneratorUtil.mergePanel(panel1,panel2,panelFace,product);
        return mergedPanelJson;
    }

    /**
     * XT面板箱体零部件清单统计
     * @param serial
     * @param partition
     */
    public static void initXTSummary(String serial,Unit unit, Partition partition) {
        //需要后台统计的类型；TODO：后续变动需要加进来
        List<String> summaryType = Arrays.asList(SYS_PANEL_TYPE_MEN_CATEGORY, SYS_PANEL_TYPE_PUTONG_CATEGORY
                ,SYS_PANEL_TYPE_JIAQIANG_CATEGORY,SYS_PANEL_TYPE_JIASHI_CATEGORY);
        Gson gson = new Gson();
        List<PanelCalculationObj> casingList = AhuPartitionGenerator.getCasinglist(serial, unit, partition, false, true);
        List<XTSummary> xtSummarys = new ArrayList<XTSummary>();
        List<XTSummary> xtPanelSummarys = new ArrayList<XTSummary>();
        List<XTSummary> xtDoorSummarys = new ArrayList<XTSummary>();
        int doorCount = 0;
        for (PanelCalculationObj casingObj : casingList) {
            String category = casingObj.getCategory();
            if(summaryType.contains(category)
                    || category.equals(TYPE_JIAQIANG_CATEGORY)
                    || category.equals(TYPE_NJG_CATEGORY)
                    || category.equals(TYPE_NJGP_CATEGORY)) {
                XTSummary xtSummary = new XTSummary();
                xtSummary.setPartName(EmptyUtil.toString(casingObj.getPartName(), StringUtils.EMPTY));
                xtSummary.setPartWM(EmptyUtil.toString(casingObj.getPartWM(), String.valueOf(0)));
                xtSummary.setPartLM(EmptyUtil.toString(casingObj.getPartLM(), String.valueOf(0)));
                xtSummary.setQuantity(EmptyUtil.toString(casingObj.getQuantity(), String.valueOf(0)));

                if(SYS_PANEL_TYPE_MEN_CATEGORY.equals(category)){
                    xtDoorSummarys.add(xtSummary);
                    doorCount++;
                }else{
                    xtPanelSummarys.add(xtSummary);
                }
            }
        }
        //普通面板
        xtSummarys.addAll(xtPanelSummarys);
        if(doorCount > 0) {
            XTSummary xtSummaryDoorCount = new XTSummary();
            xtSummaryDoorCount.setPartName(AHUContext.getIntlString(DOOR_PANEL_PARTS, SIMPLIFIED_CHINESE));
            xtSummaryDoorCount.setPartWM(SYS_BLANK);
            xtSummaryDoorCount.setPartLM(SYS_BLANK);
            xtSummaryDoorCount.setQuantity(SYS_BLANK + doorCount);

            //门部件
            xtSummarys.add(xtSummaryDoorCount);
            xtSummarys.addAll(xtDoorSummarys);
        }

        partition.setSummaryJson(gson.toJson(xtSummarys));
    }
    /**
     * 填充面板
     * @param serial
     * @param isChange
     * @param partitions
     */
    public static void populatePartitionPanel(String serial, boolean isChange, List<AhuPartition> partitions) {
        for (int i = 1; i <= partitions.size(); i++) {
            AhuPartition partition = partitions.get(i-1);
            Map<String, PanelFace> panels = new HashMap<>();
            panels.put(UtilityConstant.SYS_PANEL_FRONT, createPanelFace(UtilityConstant.SYS_PANEL_FRONT, serial,isChange, partition,i,partitions));
            panels.put(UtilityConstant.SYS_PANEL_BACK, createPanelFace(UtilityConstant.SYS_PANEL_BACK, serial, isChange, partition,i,partitions));
            panels.put(UtilityConstant.SYS_PANEL_TOP, createPanelFace(UtilityConstant.SYS_PANEL_TOP, serial, false, partition,i,partitions));
            panels.put(UtilityConstant.SYS_PANEL_BOTTOM, createPanelFace(UtilityConstant.SYS_PANEL_BOTTOM, serial, false, partition,i,partitions));
            panels.put(UtilityConstant.SYS_PANEL_LEFT, createPanelFace(UtilityConstant.SYS_PANEL_LEFT, serial, isChange, partition, i, partitions));
            panels.put(UtilityConstant.SYS_PANEL_RIGHT, createPanelFace(UtilityConstant.SYS_PANEL_RIGHT, serial, isChange, partition, i, partitions));
            //端面中间面
            AhuPartitionGeneratorUtil.cutMiddleView(serial,isChange, partition, partitions,panels);
            partition.setPanels(panels);

            Queue<String> fans=new ArrayDeque<String>();

            try {
                fans=PanelFaceUtil.getFanProperty(serial,partition);
            }catch(Exception e) {
                log.error("Reset Panel Error:"+e.getMessage());
                log.debug(e.getMessage(),e);

            }
            for(Entry<String, PanelFace> panel:panels.entrySet()) {
                try {
                    LinkedHashMap<String, LinkedHashMap<String, PanelFace>> listPanelFulls = toSetConnectorType(serial, panel.getValue());
                    PanelFaceUtil.setFanParameter(serial, listPanelFulls, fans);
                }catch(Exception e) {
                    log.error("Reset Panel Error:"+e.getMessage());
                    log.debug(e.getMessage(),e);

                }
            }

            //底座(顶层不需要底座)
            if(!partition.isTopLayer())
                partition.setBase(generateBase(serial, partition));
        }
    }

    /**
     * 重置Connector AB值
     * @param serial
     * @param value
     * @return
     */
    private static LinkedHashMap<String, LinkedHashMap<String, PanelFace>> toSetConnectorType(String serial, PanelFace value) {
        LinkedHashMap<String, LinkedHashMap<String, PanelFace>> listPanelFulls = new LinkedHashMap<String, LinkedHashMap<String, PanelFace>>();
        LinkedHashMap<String, PanelFace> allPanels = new LinkedHashMap<String, PanelFace>();
        PanelFaceUtil.convertTreeToMap(null, value, listPanelFulls, allPanels, 0);
        PanelFaceUtil.resetConnectorType(serial, listPanelFulls, allPanels, value.getDirection());
        return listPanelFulls;
    }
    /**
     * 获取系列名，基座号专用
     * 39CQ1215: 39CQ<br>
     * 39G1215: 39G<br>
     * G1215: G<br>
     * @param unitModel
     * @param isBig
     * @return
     */
    public static String getBaseUnitSeries(String unitModel, boolean isBig) {
        String unitSeries = unitModel;
        if (unitModel.length() > UtilityConstant.SYS_INT_UNIT_NO_SIZE) {
            unitSeries = unitModel.substring(0, unitModel.length() - UtilityConstant.SYS_INT_UNIT_NO_SIZE);
        }

        if(isBig && unitModel.contains(SYS_UNIT_SERIES_39CQ) ){
            unitSeries = "39CQT";
        }
        if(isBig && unitModel.contains(SYS_UNIT_SERIES_39XT) ){
            unitSeries = "39XBT";
        }
        return unitSeries;
    }
    /**
     * 生成底座编号
     * @param serial
     * @param partition
     * @return
     */
    private static String generateBase(String serial, AhuPartition partition) {
        Map<String, Object> ahuParaMap = partition.getAhuParameters();

        StringBuffer base = new StringBuffer("");

        /** 1: 机组底座铲车孔型式(孔型式(1-槽铁铲车孔外翻、2-槽铁铲车孔内翻) **/
        /*int ccxs = 2532;//型号大于等于2532 采用内翻
        if(NumberUtil.convertStringToDoubleInt(AhuUtil.getUnitNo(serial)) >= ccxs){
            base.append("2");
        }else{
            base.append("1");
        }*/
        base.append("0");//18/12/06 机组底座零件号第一位0

        /** 2: 材料涂装方式(0-电镀锌、1-热镀锌、2-喷涂) **/
        if(AHU_BASETYPE_POWDERCOATEDCR.equals(ahuParaMap.get(baseType))){
            base.append("2");
        }else if(AHU_BASETYPE_HDGALVANIZEDSTEEL.equals(ahuParaMap.get(baseType))){//热镀锌
            base.append("1");
        }else{
            base.append("0");
        }

        /** 3: 机组系列代号 **/
        boolean isBig = false;
        if(gteBigUnit(Integer.parseInt(AhuUtil.getUnitNo(serial)))){
            isBig = true;
        }
        base.append(getBaseUnitSeries(serial,isBig));

        /** 4: 机组底座水盘出水孔(0－无孔、1－1孔、2－2孔) **/
        String[] retSP = generateShuiPan(partition);
        base.append(retSP[0]);

        /** 5: 分段宽度M、分段长度M **/
        base.append(String.format("%02d",partition.getWidth()));
        int sectionL = partition.getLength();
        if (sectionL > 100) {
            sectionL = sectionL / 100;
        }
        base.append(String.format("%03d",sectionL));

        /** 6: 水盘编号 **/
        base.append(retSP[1]);

        return base.toString();
    }
    /**
     * 生成底座编号,价格ini文件使用
     * @param serial
     * @param partition
     * @return
     */
    public static String generateBaseForPrice(AhuPartition partition) {
        StringBuffer base = new StringBuffer("");

        /** 1: 机组系列代号 **/
        base.append("39HB");

        /** 2: 机组底座水盘出水孔(0－无孔、1－1孔、2－2孔) **/
        String[] retSP = generateShuiPan(partition);
        base.append(retSP[0]);

        /** 3: 分段宽度M、分段长度M **/
        base.append(String.format("%02d",partition.getWidth()));
        int sectionL = partition.getLength();
        if (sectionL > 100) {
            sectionL = sectionL / 100;
        }
        base.append(String.format("%03d",sectionL));

        /** 4: 水盘编号 **/
        base.append("R");
        return base.toString();
    }

    private static int calIHoleCount(AhuPartition partition) {
        int iHoleCount = 0;
        LinkedList<Map<String, Object>> sections = partition.getSections();
        for (int i = 0; i < sections.size(); i++) {
            Map<String, Object> partMap = sections.get(i);
            Map<String, String> sectionMap = new HashMap<>();
            String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
            sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));
            String metaId = String.valueOf(partMap.get(S_MKEY_METAID));
            String sectionLKey = MetaCodeGen.calculateAttributePrefix(metaId)+ ".sectionL";
            int sectionL = 0;
            if(EmptyUtil.isNotEmpty(String.valueOf(sectionMap.get(sectionLKey)))){
                sectionL = NumberUtil.convertStringToDoubleInt(String.valueOf(sectionMap.get(sectionLKey)));
            }
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);
            String typeNo = sectionType.getCodeNum();
            while (typeNo.startsWith("0")) {
                typeNo = typeNo.substring(1);
            }

            /*   4,16,17,10,19,21: iHoleCount:=iHoleCount+1;
                20:         if lPart.PartProperty.SectionL<=9 then iHoleCount:=iHoleCount+1 else iHoleCount:=iHoleCount+2;
                18:      iHoleCount:=iHoleCount+2;
           */
            if(typeNo.equals("4") || typeNo.equals("16")
                    || typeNo.equals("17") || typeNo.equals("10")
                    || typeNo.equals("19") || typeNo.equals("21")){
                iHoleCount++;
            }else if(typeNo.equals("20")){
                if(sectionL<=9){
                    iHoleCount++;
                }else{
                    iHoleCount+=2;
                }
            }else if(typeNo.equals("18")){
                iHoleCount+=2;
            }
        }
        return iHoleCount;
    }

    /**
     *
     * 水盘开孔个数
     * 1：如果有一个水盘，那就肯定有一孔
     * 2：两个就对应两个
     * 3：没有就是零
     *
     * 开孔基本原则  
     * 1：两个及以上个数水盘显示从左到右还是从右到左：根据最边上的开孔距离端面哪个近就从那个开始作为第一个。
     * 2：水盘开孔都是在功能段左面往右3M 处，大型机组在功能段右面往左3M 处
     * 3：第一个水盘的长度是距离端面的M 数，第二个水盘的长度是距离第一个水盘的距离，第三个水盘的长度是距离第二个水盘的距离，以此类推。。。
     *
     * @param partition
     * @return
     */
    private static String[] generateShuiPan(AhuPartition partition) {
        int partitionSectionL = partition.getLength();
        if (partitionSectionL > 100) {
            partitionSectionL = partitionSectionL / 100;
        }

        String[] ret = new String[2];
        LinkedList<Map<String, Object>> sections = partition.getSections();
        List<Integer> finalLen = new ArrayList<>();//每个孔距离当前分段左端面或右端面最近的距离
        List<String> leftRightLen = new ArrayList<>();//每个孔距离左端面和右端面的距离
        int len = 0;
        for (int i = 0; i < sections.size(); i++) {
            Map<String, Object> partMap = sections.get(i);
            Map<String, String> sectionMap = new HashMap<>();
            String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
            sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));


            String metaId = String.valueOf(partMap.get(S_MKEY_METAID));
            String sectionLKey = MetaCodeGen.calculateAttributePrefix(metaId)+ ".sectionL";
            int sectionL = 0;
            if(EmptyUtil.isNotEmpty(String.valueOf(sectionMap.get(sectionLKey)))){
                sectionL = NumberUtil.convertStringToDoubleInt(String.valueOf(sectionMap.get(sectionLKey)));
            }

            boolean heatingCoilHasSP = false;
            if(SectionTypeEnum.TYPE_HEATINGCOIL.getId().equals(metaId) && i<sections.size()-1){
                Map<String, Object> nextPartMap = sections.get(i+1);
                String nextMetaId = String.valueOf(nextPartMap.get(S_MKEY_METAID));
                if(SectionTypeEnum.TYPE_WETFILMHUMIDIFIER.getId().equals(nextMetaId)){
                    heatingCoilHasSP = true;
                }
            }


            //冷水盘管、干蒸汽、高压喷雾、高压、（热水+湿膜） 有水盘
            if(SectionTypeEnum.TYPE_COLD.getId().equals(metaId)
                    || (SectionTypeEnum.TYPE_STEAMHUMIDIFIER.getId().equals(metaId) && !SPRAYHUMIDIFIER_MATERIAL_WITHOUT.equals(sectionMap.get(METASEXON_STEAMHUMIDIFIER_MATERIAL)))//干蒸选择非：无水盘
                    || SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER.getId().equals(metaId)
                    || SectionTypeEnum.TYPE_SPRAYHUMIDIFIER.getId().equals(metaId)
                    || heatingCoilHasSP//热水+湿膜
                    ){
                //判定孔的位置，就是从左往右，或者从右往左，哪个距离近用那个
                int leftLen = len+3;//从左进风方向开孔在所在段3M 处
                if(EmptyUtil.isNotEmpty(partition.getSeries()) && SystemCountUtil.gteBigUnit(SystemCountUtil.getUnitNoInt(partition.getSeries()))){
                    leftLen = len+(sectionL-3);//大型机组从右进风方向开孔在所在段3M 处
                }
                int rigthLen = partitionSectionL-(leftLen);
                if(leftLen > rigthLen){
                    finalLen.add(rigthLen);
                }else{
                    finalLen.add(leftLen);
                }
                leftRightLen.add(leftLen+","+rigthLen);
            }
            len+=sectionL;
        }
        ret[0] = ""+finalLen.size();
        if(finalLen.size() == 0){
            ret[1] = "";
        }else {
            ret[1] = buildSPCode(finalLen,leftRightLen);
        }
        return ret;
    }

    /**
     * 是否：孔在底座号里面的先后顺序从左往右
     * @param finalLen
     * @return
     */
    private static boolean isSPLeftDirection(List<Integer> finalLen) {
        boolean isLeftDirection = true;
        //第一个孔距离大于最后一个孔距离：从右到左输出孔距离。
        if(finalLen.size()>1
                && finalLen.get(0)>finalLen.get(finalLen.size()-1)){
            isLeftDirection = false;
        }
        return isLeftDirection;
    }

    /**
     * 生成底座编号水盘字母序列
     * @param finalLen
     * @param leftRightLen
     * @return
     */
    private static String buildSPCode(List<Integer> finalLen, List<String> leftRightLen) {
        StringBuilder retCode = new StringBuilder("");
        if(finalLen.size()==1){
            //只有一个孔
            retCode.append(generateShuiPan(finalLen.get(0)));
        }else{
            if(isSPLeftDirection(finalLen)){
                for (int i=0;i<leftRightLen.size();i++) {
                    int theFinalLen = 0;
                    if(i == 0){
                        theFinalLen = Integer.parseInt(leftRightLen.get(i).split(",")[0]);
                    }else{
                        theFinalLen = Integer.parseInt(leftRightLen.get(i).split(",")[0]) - Integer.parseInt(leftRightLen.get(i-1).split(",")[0]);
                    }
                    retCode.append(generateShuiPan(theFinalLen));
                }

            }else{//第一个孔距离大于最后一个孔距离：从右到左输出孔距离。
                for (int i = leftRightLen.size() - 1; i >= 0; i--){
                    int theFinalLen = 0;
                    if(i == leftRightLen.size() - 1){
                        theFinalLen = Integer.parseInt(leftRightLen.get(i).split(",")[1]);
                    }else{
                        theFinalLen = Integer.parseInt(leftRightLen.get(i).split(",")[1]) - Integer.parseInt(leftRightLen.get(i+1).split(",")[1]);
                    }
                    retCode.append(generateShuiPan(theFinalLen));
                }
            }
        }
        return retCode.toString();
    }

    private static String generateShuiPan(int kaikongArg) {
        String kaikong = String.valueOf(kaikongArg);
        return null!=KAIKONG.get(kaikong)?KAIKONG.get(kaikong):kaikong;
    }
    /**
     * 面板布置初始化<br>
     * @param face
     * @param serial
     * @param isChange
     * @param partition
     * @param partionIndex 从1开始
     * @param partitions
     * @return
     */
    private static PanelFace createPanelFace(String face, String serial, boolean isChange, AhuPartition partition, int partionIndex, List<AhuPartition> partitions) {
        PanelFace panelFace = AhuPartitionGeneratorUtil.createPanelFace(face, partition);

        String doororientationStr = String.valueOf(partition.getAhuParameters().get(doororientation));
        String pipeOrientationStr = String.valueOf(partition.getAhuParameters().get(pipeOrientation));
        Boolean isLeftPipe = SYS_STRING_LEFT.equals(pipeOrientationStr);//接管方向：左
        Boolean isRigthPipe = SYS_STRING_RIGHT.equals(pipeOrientationStr);//接管方向：右
        if(SYS_STRING_BILATERAL.equals(pipeOrientationStr)){
            isLeftPipe = true;
            isRigthPipe = true;
        }

        if (face.equals(UtilityConstant.SYS_PANEL_FRONT) || face.equals(UtilityConstant.SYS_PANEL_BACK)) {// 前后端面
            if(serial.contains(AHU_PRODUCT_39XT)) {
                AhuPartitionGeneratorUtil.cutFrontAndBackForXT(face, serial,isChange, partition, partionIndex, partitions, panelFace);
            }else {
                AhuPartitionGeneratorUtil.cutFrontAndBack(face, serial,isChange, partition, partionIndex, partitions, panelFace);
            }
        } else if (face.equals(UtilityConstant.SYS_PANEL_LEFT)) {//左面切割
            if(serial.contains(AHU_PRODUCT_39XT)) {
                /**检修门导致左右面切割互换**/
                if (SYS_STRING_LEFT.equals(doororientationStr)) {//检修门方向左,左面板属于操作面
                    AhuPartitionGeneratorUtil.cutXTBigNoBigOperatePanel(face, serial, partition, panelFace, doororientationStr);
                }else{
                    AhuPartitionGeneratorUtil.cut39XTTopBottomPanel(face,serial, partition, panelFace);
                }
            }else if(serial.contains(AHU_PRODUCT_39G)){
                /**检修门导致左右面切割互换**/
                if (SYS_STRING_LEFT.equals(doororientationStr)) {//检修门方向左,左面板属于操作面
                    AhuPartitionGeneratorUtil.cutBigOperatePanel(face,serial,isChange, partition, panelFace, isLeftPipe, doororientationStr);
                } else {
                    AhuPartitionGeneratorUtil.cut39GDBPanel(doororientationStr, face, serial, isChange, partition, panelFace, isLeftPipe);
                }
            }else if(serial.contains(AHU_PRODUCT_39CQ)){
                /**检修门导致左右面切割互换**/
                if (SYS_STRING_LEFT.equals(doororientationStr)) {//检修门方向左,左面板属于操作面
                    AhuPartitionGeneratorUtil.cutBigOperatePanel(face,serial,isChange, partition, panelFace, isLeftPipe,doororientationStr);
                } else {
                    AhuPartitionGeneratorUtil.cutNoBigOperateOrTopPanel(face,isChange, partition, panelFace, serial, isLeftPipe,doororientationStr);
                }
            }
        } else if (face.equals(UtilityConstant.SYS_PANEL_BOTTOM)) {//底面切割

            if(serial.contains(AHU_PRODUCT_39CQ)){
                AhuPartitionGeneratorUtil.cutBottomPanel(partition, panelFace,serial);
            }else if(serial.contains(AHU_PRODUCT_39G)){
                AhuPartitionGeneratorUtil.cut39GDBPanel(doororientationStr, face,serial, isChange, partition, panelFace, isRigthPipe);
            }else if(serial.contains(AHU_PRODUCT_39XT)){
                AhuPartitionGeneratorUtil.cut39XTTopBottomPanel(face,serial, partition, panelFace);
            }

        } else {//顶面、右面 切割

            if(serial.contains(AHU_PRODUCT_39CQ)){

                if(face.equals(UtilityConstant.SYS_PANEL_TOP)){
                    AhuPartitionGeneratorUtil.cutNoBigOperateOrTopPanel(face, isChange, partition, panelFace,serial,isRigthPipe, doororientationStr);
                }else if(face.equals(UtilityConstant.SYS_PANEL_RIGHT)) {
                    /**检修门导致左右面切割互换**/
                    if (SYS_STRING_RIGHT.equals(doororientationStr)) {//检修门方向右,右面板属于操作面
                        AhuPartitionGeneratorUtil.cutBigOperatePanel(face,serial, isChange, partition, panelFace,isRigthPipe, doororientationStr);
                    } else {
                        AhuPartitionGeneratorUtil.cutNoBigOperateOrTopPanel(face, isChange, partition, panelFace, serial,isRigthPipe, doororientationStr);
                    }
                }

            }else if(serial.contains(AHU_PRODUCT_39G)){
                /**检修门导致左右面切割互换**/
                if(face.equals(UtilityConstant.SYS_PANEL_RIGHT) && SYS_STRING_RIGHT.equals(doororientationStr)) {//检修门方向右,右面板属于操作面
                    AhuPartitionGeneratorUtil.cutBigOperatePanel(face,serial, isChange, partition, panelFace,isRigthPipe, doororientationStr);
                }else{
                    AhuPartitionGeneratorUtil.cut39GDBPanel(doororientationStr,face,serial,isChange, partition, panelFace,isRigthPipe);
                }
            }else if(serial.contains(AHU_PRODUCT_39XT)){
                if(face.equals(UtilityConstant.SYS_PANEL_TOP)) {
                    AhuPartitionGeneratorUtil.cut39XTTopBottomPanel(face,serial, partition, panelFace);
                }else  if(face.equals(UtilityConstant.SYS_PANEL_RIGHT)) {
                    /**检修门导致左右面切割互换**/
                    if(face.equals(UtilityConstant.SYS_PANEL_RIGHT) && SYS_STRING_RIGHT.equals(doororientationStr)) {//检修门方向右,右面板属于操作面
                        AhuPartitionGeneratorUtil.cutXTBigNoBigOperatePanel(face, serial, partition, panelFace, doororientationStr);
                    }else{
                        AhuPartitionGeneratorUtil.cut39XTTopBottomPanel(face,serial, partition, panelFace);
                    }
                }
            }

        }

        return panelFace;

    }
    public static void genPanelFaceFromPO4JsonTool(PanelFace panelFace, PanelXSLXPO po) {
        AhuPartitionGeneratorUtil.genPanelFaceFromPO(panelFace, po);
        // 封装叶子面板为树状结构
        AhuPartitionGeneratorUtil.slicePanelByBitreeRule(panelFace, "");
    }



    // @formatter:off
    /*
    @SuppressWarnings("unused")
    private static void slicePanel(PanelFace panelFace, int dep, int maxDep) {
        if (dep == maxDep) {
            panelFace.setDirection(PanelFace.DIRECTION_NONE);
            return;
        }

        int length = panelFace.getPanelLength();
        int width = panelFace.getPanelWidth();
        if (dep % 2 == 0) {
            panelFace.setDirection(PanelFace.DIRECTION_HORIZONTAL);
            int newWidth = width / 2;
            PanelFace panelFace1 = createPanelFace(length, newWidth);
            PanelFace panelFace2 = createPanelFace(length, width - newWidth);
            PanelFace[] subs = new PanelFace[] { panelFace1, panelFace2 };
            panelFace.setSubPanels(subs);
        } else {
            panelFace.setDirection(PanelFace.DIRECTION_VERTICAL);
            int newLength = length / 2;
            PanelFace panelFace1 = createPanelFace(newLength, width);
            PanelFace panelFace2 = createPanelFace(length - newLength, width);
            PanelFace[] subs = new PanelFace[] { panelFace1, panelFace2 };
            panelFace.setSubPanels(subs);
        }
        panelFace.getSubPanels()[0].setId(panelFace.getId() + "_0");
        panelFace.getSubPanels()[1].setId(panelFace.getId() + "_1");
        slicePanel(panelFace.getSubPanels()[0], dep + 1, maxDep);
        slicePanel(panelFace.getSubPanels()[1], dep + 1, maxDep);
    }
    */
    // @formatter:on





    /**
     * 统计分段面板中框面板等数据
     *
     * @param series
     * @param unit
     * @param partition
     * @param reCalPanel 是否重新计算面板数据(报告不用重新计算；价格需要使用默认面板切割计算；面板分割为空需要计算)
     * @param useChangeSeries 是否使用变形型号
     * @return
     */
    public static List<PanelCalculationObj> getCasinglist(String series, Unit unit, Partition partition, boolean reCalPanel, boolean useChangeSeries){
        boolean isChange = false;
        if(useChangeSeries && EmptyUtil.isNotEmpty(unit.getPanelSeries()) && !series.equals(unit.getPanelSeries())){
            series = unit.getPanelSeries();
            isChange = true;
        }
//        List<AhuPartition> ahuPartitionList = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
        List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit,partition);

        if(reCalPanel || EmptyUtil.isEmpty(ahuPartitionList.get(0).getPanels())) {
            //        populatePartitionPanel(AHU_PRODUCT_39G + AhuUtil.getUnitNo(series), ahuPartitionList);//39CQ 39G 都采用39G 型号进行计算面板价格
            populatePartitionPanel(series, isChange, ahuPartitionList);//series ： 39CQ 39G
        }
        List<PanelCalculationObj> retCal = new ArrayList<PanelCalculationObj>();


        for (AhuPartition ap : ahuPartitionList) {
            List<PanelCalculationObj> retFramePanelCalTemp = new ArrayList<PanelCalculationObj>();
            List<PanelCalculationObj> retPanelFaceOutsideFrameCalTemp = new ArrayList<PanelCalculationObj>();

            int pos = ap.getPos()+1;

            //重置价格底座
            if(reCalPanel) {
                ap.setBase(generateBaseForPrice(ap));
            }
            if(ap.isTopLayer()){
                ap.setBase("");//(顶层不需要底座)
            }
            calPanelFaceOutsideFrame(retPanelFaceOutsideFrameCalTemp,pos,ap);//框条 Y 接角 底座

            Map<String, PanelFace> panels = ap.getPanels();
            //六面，中框、面板
            PanelFace pfFTOP = panels.get(UtilityConstant.SYS_PANEL_TOP);
            calPanelFaceObjProcess(series,retFramePanelCalTemp,pfFTOP,UtilityConstant.SYS_PANEL_TOP,pos);
            PanelFace pfFBOTTOM = panels.get(UtilityConstant.SYS_PANEL_BOTTOM);
            calPanelFaceObjProcess(series, retFramePanelCalTemp,pfFBOTTOM,UtilityConstant.SYS_PANEL_BOTTOM,pos);
            PanelFace pfFRIGHT = panels.get(UtilityConstant.SYS_PANEL_RIGHT);
            calPanelFaceObjProcess(series, retFramePanelCalTemp,pfFRIGHT,UtilityConstant.SYS_PANEL_RIGHT,pos);
            PanelFace pfFLEFT = panels.get(UtilityConstant.SYS_PANEL_LEFT);
            calPanelFaceObjProcess(series, retFramePanelCalTemp,pfFLEFT,UtilityConstant.SYS_PANEL_LEFT,pos);
            PanelFace pfFRONT = panels.get(UtilityConstant.SYS_PANEL_FRONT);
            calPanelFaceObjProcess(series, retFramePanelCalTemp,pfFRONT,UtilityConstant.SYS_PANEL_FRONT,pos);
            PanelFace pfFBACK = panels.get(UtilityConstant.SYS_PANEL_BACK);
            calPanelFaceObjProcess(series, retFramePanelCalTemp,pfFBACK,UtilityConstant.SYS_PANEL_BACK,pos);
            //中间面，中框、面板
            for (Map.Entry<String, PanelFace> stringPanelFaceEntry : panels.entrySet()) {
                String key = stringPanelFaceEntry.getKey();
                if(AhuPartitionGeneratorUtil.isMiddleEndView(key)){
                    PanelFace pfMiddleEndView = stringPanelFaceEntry.getValue();
                    calPanelFaceObjProcess(series, retFramePanelCalTemp,pfMiddleEndView,key,pos);

                }
            }
            //XT 特殊960规则统计
            if(series.contains(AHU_PRODUCT_39XT)) {
                reCalXTPanel(series,retFramePanelCalTemp);
            }

            retCal.addAll(retPanelFaceOutsideFrameCalTemp);
            retCal.addAll(retFramePanelCalTemp);
        }

        /*for (int i = 0; i < retCal.size(); i++) {
            if(i==0) {
                System.out.printf("%-20s", "计算key");
                System.out.printf("%-20s", "名称");
                System.out.printf("%-20s", "长度");
                System.out.printf("%-20s", "宽度");
                System.out.printf("%-20s", "数量");
                System.out.printf("%-20s", "Memo");
                System.out.printf("%-20s", "面");
                System.out.printf("%-20s", "分段");
                System.out.println("");
            }
                PanelCalculationObj panelCalculationObj = retCal.get(i);
                System.out.printf("%-20s",panelCalculationObj.getCategory());
                System.out.printf("%-20s",panelCalculationObj.getPartName());
                System.out.printf("%-20s",panelCalculationObj.getPartLM());
                System.out.printf("%-20s",panelCalculationObj.getPartWM());
                System.out.printf("%-20s",panelCalculationObj.getQuantity());
                System.out.printf("%-20s",panelCalculationObj.getMemo());
                System.out.printf("%-20s",panelCalculationObj.getInstPostion());
                System.out.printf("%-20s",panelCalculationObj.getInstSection());
                System.out.println("");
        }*/
        return retCal;
    }

    /**
     * 统计XT正压门
     *
     * 检修门在左：XT正压门 H列用：4
     * 检修门在右：XT正压门 H列用：3
     *
     * @param series
     * @param unit
     * @param partition
     * @return
     */
    public static List<PanelCalculationObj> getXTCasinglist(String series, Unit unit, Partition partition){
        List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit,partition);
        List<PanelCalculationObj> retCal = new ArrayList<PanelCalculationObj>();

        for (AhuPartition ap : ahuPartitionList) {
            List<PanelCalculationObj> retFramePanelCalTemp = new ArrayList<PanelCalculationObj>();

            int pos = ap.getPos()+1;
            String doororientationStr = String.valueOf(ap.getAhuParameters().get(doororientation));
            int position;
            if (SYS_STRING_LEFT.equals(doororientationStr)) {//检修门方向左,左面板属于操作面
                position = Integer.parseInt(SYS_PANEL_LEFT)+1;//sap从1开始，需要加1
            }else{
                position = Integer.parseInt(SYS_PANEL_RIGHT)+1;//sap从1开始，需要加1
            }

            LinkedList<Map<String, Object>> apSections = ap.getSections();
            for (Map<String, Object> partMap : apSections) {

                Integer sectionL = MapValueUtils.getIntegerValue(AhuPartition.C_MKEY_SECTIONL, partMap);
                String metaId = MapValueUtils.getStringValue(AhuPartition.S_MKEY_METAID, partMap);
                SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(metaId);
                Map<String, String> sectionMap = new HashMap<>();
                String sectionMetaJson = String.valueOf(partMap.get(S_MKEY_METAJSON));
                sectionMap.putAll(JSON.parseObject(sectionMetaJson, HashMap.class));
                /*
                    mix
                    fan
                    discharge
                    combinedmixingchamber
                    access
                */
                if (SectionTypeEnum.TYPE_MIX == sectionType
                        || SectionTypeEnum.TYPE_FAN == sectionType
                        || SectionTypeEnum.TYPE_WWKFAN == sectionType
                        || SectionTypeEnum.TYPE_DISCHARGE == sectionType
                        || SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER == sectionType
                        || SectionTypeEnum.TYPE_ACCESS == sectionType){
                    String positivePressureDoor = String.valueOf(sectionMap.get(getMetaSectionKey(sectionType.getId(), KEY_POSITIVE_PRESSURE_DOOR)));
                    if("true".equalsIgnoreCase(positivePressureDoor)){

                        PanelCalculationObj pco = new PanelCalculationObj();
                        pco.setCategory(SYS_PANEL_TYPE_ZENGYA_CATEGORY);
                        pco.setPartName("XT正压门");

                        PositivePressureDoorUtil positivePressureDoorUtil = new PositivePressureDoorUtil();
                        PositivePressureDoorParam positivePressureDoorParam = positivePressureDoorUtil.getPositivePressureDoorSituation(ap.getSeries(),sectionMap);
                        if(EmptyUtil.isNotEmpty(positivePressureDoorParam)){
                            //设置检修门尺寸
                            //String doorSize = positivePressureDoorParam.getDoorW()+"*"+ positivePressureDoorParam.getDoorH();
                            pco.setPartLM(positivePressureDoorParam.getDoor().split("\\*")[0]);
                            pco.setPartWM(positivePressureDoorParam.getDoor().split("\\*")[1]);
//                            pco.setPartLM(String.valueOf(positivePressureDoorParam.getDoorW()>100?positivePressureDoorParam.getDoorW()/100:positivePressureDoorParam.getDoorW()));
//                            pco.setPartWM(String.valueOf(positivePressureDoorParam.getDoorH()>100?positivePressureDoorParam.getDoorH()/100:positivePressureDoorParam.getDoorH()));
                        }else{   //mix fan discharge access
                            /*请在选型报告中增加门尺寸及方向，其中尺寸数据从表格中读取
                            段长等于5M时，读取SECTIONL=5
                            段长大于5M时,读取SECTIONL=6*/
                            XtDoor xtDoor = AhuMetadata.findOne(XtDoor.class, ap.getSeries(), String.valueOf(sectionL>5?6:5));
                            //String xtDoorSize  = xtDoor.getXtDoorw()+"*"+xtDoor.getXtDoorh();
                            pco.setPartLM(String.valueOf(xtDoor.getXtDoorw()));
                            pco.setPartWM(String.valueOf(xtDoor.getXtDoorh()));
                        }
                        pco.setPro("00");
                        pco.setQuantity("1");
                        pco.setInstSection(""+pos);
                        pco.setInstPostion(""+position);

                        //开门观察窗Has
                        String sectionDoorOpendAndViewport = AhuPartitionGeneratorUtil.getDoorOpenAndViewport(sectionType, sectionMap);
                        boolean hasViewPort = false;

                        //新回排两个门面板有可能不会同时开(面板里面：回风侧（左）、新风侧（右）)
                        if (metaId.contains(SectionTypeEnum.TYPE_COMBINEDMIXINGCHAMBER.getId())) {
                            if (Boolean.parseBoolean(sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_COMMA)[0].split(SYS_PUNCTUATION_SEMICOLON)[1])) {//处理第一个门，开窗
                                hasViewPort = true;
                            }
                            if (sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_COMMA).length > 1) {
                                if (Boolean.parseBoolean(sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_COMMA)[1].split(SYS_PUNCTUATION_SEMICOLON)[1])) {//处理第二个门，开窗
                                    hasViewPort = true;
                                }
                            }
                        } else {
                            //普通功能段开门只有一个门

                            if (Boolean.parseBoolean(sectionDoorOpendAndViewport.split(SYS_PUNCTUATION_SEMICOLON)[1])) {//处理开窗
                                hasViewPort = true;
                            }
                        }
                        if (hasViewPort)
                            pco.setMemo(String.valueOf(SYS_PANEL_DESC_HASVIEWPORT));

                        retFramePanelCalTemp.add(pco);
                    }
                }

            }

            retCal.addAll(retFramePanelCalTemp);
        }
        return retCal;
    }
    /**
     * XT是计算每个面的总长度，然后除以标准面板宽度900后往上取整所得
     * XT面板只有两种规则，宽度方面或者高度方面，比如XT0608，只有两种规则,一种是高度方面6M*900,宽度方向8M*900
     * 计算面板总数时需要扣除各个位置风阀尺寸
     * 宽度最高是900，因此面板在部分机组是可以水平布置
     * @param series
     * @param retFramePanelCalTemp
     */
    private static void reCalXTPanel(String series, List<PanelCalculationObj> retFramePanelCalTemp) {
        List<PanelCalculationObj> retFramePanelCalFinal = new ArrayList<PanelCalculationObj>();
        PanelCalculationObj commonPanelObj = null;
        PanelCalculationObj commonDoorPanelObj = null;
        String height = String.valueOf(AhuUtil.getHeightOfAHU(series));
        String width = String.valueOf(AhuUtil.getWidthOfAHU(series));
        int totalHeight = 0;
        int totalWidth = 0;
        Map<String,Integer> totalHeightMap = new HashedMap<String,Integer>();//存放顶、底、左、右、前后面总长度
        Map<String,Integer> totalWidthMap = new HashedMap<String,Integer>();//存放顶、底、左、右、前后面总长度
        for (PanelCalculationObj obj : retFramePanelCalTemp) {
            //中框使用老规则结果
            if(obj.getCategory().equals(TYPE_NJG_CATEGORY)
                    || obj.getCategory().equals(TYPE_NJGP_CATEGORY)
                    || obj.getCategory().equals(TYPE_JIAQIANG_CATEGORY)){//加强暂时替代内角钢
                retFramePanelCalFinal.add(obj);//中框XT 忽略，只有前台才会修改彩钢线。。内角钢
            }else{//面板采用（宽*9）（高*9）两种规格重新切割

                //非内角中框不统计
                if(obj.getCategory().equals(TYPE_PUTONG_CATEGORY)
                        || obj.getCategory().equals(TYPE_JIAQIANG_CATEGORY)
                        || obj.getCategory().equals(TYPE_NEI_CATEGORY)
                        || obj.getCategory().equals(TYPE_ZUHE_CATEGORY)){
                    continue;
                }

                //无面板不统计
                if(obj.getCategory().equals(SYS_PANEL_TYPE_WU_CATEGORY)){
                    continue;
                }


                //初始化通用panel,继承所在段编号、所在面等信息
                if(EmptyUtil.isEmpty(commonPanelObj)
                        && obj.getCategory().equals(SYS_PANEL_TYPE_PUTONG_CATEGORY)){
                    commonPanelObj = obj;
                }
                //初始化通用门panel,继承所在段编号、所在面等信息
                if(EmptyUtil.isEmpty(commonDoorPanelObj)
                        && obj.getCategory().equals(SYS_PANEL_TYPE_MEN_CATEGORY)){
                    commonDoorPanelObj = obj;
                }

                //门面板处理门开口规格部件。。
                if(obj.getCategory().equals(SYS_PANEL_TYPE_MEN_CATEGORY) && EmptyUtil.isNotEmpty(obj.getMetaId()) && !"null".equals(obj.getMetaId().toLowerCase())){
                    String partTypeNum = SectionTypeEnum.getSectionTypeFromId(obj.getMetaId()).getCodeNum();
                    SPaneldXtDoor sPaneldXtDoor = AhuMetadata.findOne(SPaneldXtDoor.class, series,String.valueOf(NumberUtil.convertStringToDoubleInt(partTypeNum)));
                    if(EmptyUtil.isNotEmpty(sPaneldXtDoor) && EmptyUtil.isNotEmpty(sPaneldXtDoor.getDoor())){
                        String door = sPaneldXtDoor.getDoor();
                        door = door.replace("M","");
                        PanelCalculationObj doorPanelObj = ObjectUtil.clone(commonDoorPanelObj);//深拷贝
                        doorPanelObj.setPartName(AHUContext.getIntlString(DOOR_PANEL_PARTS, SIMPLIFIED_CHINESE));
                        doorPanelObj.setPartWM(door.split(SYS_PANEL_XT_X)[1]);
                        doorPanelObj.setPartLM(door.split(SYS_PANEL_XT_X)[0]);
                        doorPanelObj.setQuantity("1");
                        retFramePanelCalFinal.add(doorPanelObj);
                    }

                }

                int tempTotal = 0;
                boolean hasAdded = false;
                //段长方向上的面（顶、底、左、右）只处理 getPartLM
                if(obj.getPartWM().equals(height)){
                    tempTotal = (NumberUtil.convertStringToDoubleInt(obj.getPartLM()) * NumberUtil.convertStringToDoubleInt(obj.getQuantity()));
                    hasAdded = true;

                    mergeTotal2Map(obj.getInstPostion(), tempTotal, totalHeightMap);//统计totalheight 、totalwidth
                }
                if(obj.getPartWM().equals(width)){
                    tempTotal = (NumberUtil.convertStringToDoubleInt(obj.getPartLM()) * NumberUtil.convertStringToDoubleInt(obj.getQuantity()));
                    hasAdded = true;

                    mergeTotal2Map(obj.getInstPostion(), tempTotal, totalWidthMap);//统计totalheight 、totalwidth
                }

                //端面或者中间面（左端面、右端面、中间面）附加处理 getPartWM
                if(NumberUtil.convertStringToDoubleInt(obj.getInstPostion()) >= 5 && !hasAdded){
                    if(obj.getPartLM().equals(height)){
                        tempTotal = (NumberUtil.convertStringToDoubleInt(obj.getPartWM()) * NumberUtil.convertStringToDoubleInt(obj.getQuantity()));

                        mergeTotal2Map(obj.getInstPostion(), tempTotal, totalHeightMap);//统计totalheight 、totalwidth
                    }
                    if(obj.getPartLM().equals(width)){
                        tempTotal = (NumberUtil.convertStringToDoubleInt(obj.getPartWM()) * NumberUtil.convertStringToDoubleInt(obj.getQuantity()));

                        mergeTotal2Map(obj.getInstPostion(), tempTotal, totalWidthMap);//统计totalheight 、totalwidth
                    }
                }
            }
        }
        //宽高两种规格普通面板统计
        PanelCalculationObj heightPanelObj = ObjectUtil.clone(commonPanelObj);//深拷贝
        heightPanelObj.setPartWM(SYS_PANEL_XT_SPLIT_BY_M);
        heightPanelObj.setPartLM(height);
        heightPanelObj.setQuantity(SYS_BLANK+getQuantity(totalHeightMap));
        retFramePanelCalFinal.add(heightPanelObj);

        PanelCalculationObj widthPanelObj = ObjectUtil.clone(commonPanelObj);//深拷贝
        widthPanelObj.setPartWM(SYS_PANEL_XT_SPLIT_BY_M);
        widthPanelObj.setPartLM(width);
        widthPanelObj.setQuantity(SYS_BLANK+getQuantity(totalWidthMap));
        retFramePanelCalFinal.add(widthPanelObj);

        retFramePanelCalTemp.clear();
        retFramePanelCalTemp.addAll(retFramePanelCalFinal);
    }

    /**
     * 累加各个面长度
     * @param instPostion
     * @param tempTotal
     * @param totalMap
     */
    private static void mergeTotal2Map(String instPostion, int tempTotal, Map<String, Integer> totalMap) {
        int t = totalMap.get(instPostion)==null ? 0 : totalMap.get(instPostion);
        totalMap.put(instPostion, t+tempTotal);
    }

    /**
     * 获取面板使用个数
     * 1：xt每个非端面按照标准长度960mm的面板进行拼装
     * 2：非端面剩余的切割结余，用于端面。
     * @param totalMap
     * @return
     */
    private static int getQuantity(Map<String,Integer>  totalMap) {
        int finaltotal = 0;
        List<Integer>  totalDivedList = new ArrayList<>();//记录每个面切割后的结余面板长度

        Iterator<String> iterator = totalMap.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            int tempTotal = totalMap.get(key);

            //统计每个面使用标准960mm切割面板数量
            if (NumberUtil.convertStringToDoubleInt(key) < 5) {
                finaltotal += getQuantity(tempTotal);
            }

            //统计每个面使用标准960mm切割,结余面板长度
            int dived = getModulo(tempTotal);
            if(dived > 0)
                totalDivedList.add(dived);
        }

        Collections.sort(totalDivedList, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1>o2 ? 1:-1;
            }
        });

        Iterator<String> iteratorSide = totalMap.keySet().iterator();
        while (iteratorSide.hasNext()) {
            String key = iteratorSide.next();
            int tempTotal = totalMap.get(key);

            if (NumberUtil.convertStringToDoubleInt(key) >= 5) {
                //统计每个面使用标准960mm切割面板数量
                finaltotal += AhuUtil.getRealSize(tempTotal)/SYS_PANEL_XT_SPLIT_BY_MM;

                //是否可以复用960标准切割后的面板
                int dived = getModulo(tempTotal);
                boolean hasUseDived = false;
                for (int i = 0; i < totalDivedList.size(); i++) {
                    if(totalDivedList.get(i) >= dived && dived>0){
                        totalDivedList.remove(i);
                        hasUseDived = true;
                        break;
                    }
                }
                if(!hasUseDived){
                    finaltotal++;
                }
            }
        }
        return finaltotal;
    }
    private static int getModulo(int tempTotal){
        if(AhuUtil.getRealSize(tempTotal)<SYS_PANEL_XT_SPLIT_BY_MM){
            return -1;

        }else{
            return AhuUtil.getRealSize(tempTotal)%SYS_PANEL_XT_SPLIT_BY_MM;
        }
    }
    private static int getQuantity(int total) {
        return getModulo(total)==0
                ?   (AhuUtil.getRealSize(total)/SYS_PANEL_XT_SPLIT_BY_MM)
                : (AhuUtil.getRealSize(total)/SYS_PANEL_XT_SPLIT_BY_MM)+1;
    }

    /**
     * 计算框条 Y 接角 底座逻辑
     * @param retCal
     * @param pos
     */
    public static void calPanelFaceOutsideFrame(List<PanelCalculationObj> retCal, int pos,AhuPartition ap) {

        PanelCalculationObj pcod = new PanelCalculationObj();
        pcod.setCategory(FrameLine.TYPE_BASE_CATEGORY);
        pcod.setPartName(AHUContext.getIntlString(BASE,SIMPLIFIED_CHINESE));//底座
        pcod.setPartLM("0");
        pcod.setPartWM("0");
        pcod.setQuantity("1");
        pcod.setMemo(ap.getBase());
        pcod.setInstSection(""+pos);
        pcod.setInstPostion("");
        if(EmptyUtil.isNotEmpty(ap.getBase()))//底座为空不添加sap excel 记录
            retCal.add(pcod);

        PanelCalculationObj pcoy = new PanelCalculationObj();
        pcoy.setCategory(FrameLine.TYPE_YCROSS_CATEGORY);
        pcoy.setPartName(AHUContext.getIntlString(PANEL_Y,SIMPLIFIED_CHINESE));//Y接角
        pcoy.setPartLM("0");
        pcoy.setPartWM("0");
        pcoy.setQuantity("8");
        pcoy.setInstSection(""+pos);
        pcoy.setInstPostion("");
        retCal.add(pcoy);

        int sectionL = ap.getLength();
        if (sectionL > 100) {
            sectionL = sectionL / 100;
        }
        int width = ap.getWidth();
        int height = ap.getHeight();

        PanelCalculationObj pco = new PanelCalculationObj();
        pco.setCategory(FrameLine.TYPE_FRAME);
        pco.setPartName(AHUContext.getIntlString(FRAME_BAR,SIMPLIFIED_CHINESE));//高框条，统一使用“框条”
        pco.setPartLM(""+height);
        pco.setPartWM("0");
        pco.setQuantity("4");
        pco.setInstSection(""+pos);
        pco.setInstPostion("3");
        retCal.add(pco);

        PanelCalculationObj pcoT = new PanelCalculationObj();
        pcoT.setCategory(FrameLine.TYPE_FRAME);
        pcoT.setPartName(AHUContext.getIntlString(FRAME_BAR,SIMPLIFIED_CHINESE));//顶框条，统一使用“框条”
        pcoT.setPartLM(""+sectionL);
        pcoT.setPartWM("0");
        pcoT.setQuantity("2");
        pcoT.setInstSection(""+pos);
        pcoT.setInstPostion("1");
        retCal.add(pcoT);
        PanelCalculationObj pcoT1 = ObjectUtil.clone(pcoT);
        pcoT1.setPartLM(""+width);
        retCal.add(pcoT1);

        PanelCalculationObj pcoB = new PanelCalculationObj();
        pcoB.setCategory(FrameLine.TYPE_BOTTOMFRAME_CATEGORY);
        pcoB.setPartName(AHUContext.getIntlString(FRAME_B_BAR,SIMPLIFIED_CHINESE));//底框条
        pcoB.setPartLM(""+sectionL);
        pcoB.setPartWM("0");
        pcoB.setQuantity("2");
        pcoB.setInstSection(""+pos);
        pcoB.setInstPostion("2");
        retCal.add(pcoB);
        PanelCalculationObj pcoB1 = ObjectUtil.clone(pcoB);
        pcoB1.setPartLM(""+width);
        retCal.add(pcoB1);


    }

    /**
     * 计算中框面板逻辑
     * @param series
     * @param retCal
     * @param panelFace
     * @param panelType
     * @param pos
     */
    public static void calPanelFaceObjProcess(String series, List<PanelCalculationObj> retCal, PanelFace panelFace, String panelType, int pos){
        LinkedHashMap<String,Integer> calResult = new LinkedHashMap<String,Integer>();
        calPanelFaceObj(panelFace,calResult);//循环二叉树结构完成统计

        //中框
        List<PanelCalculationObj> retCalFrameLine = new ArrayList<PanelCalculationObj>();
        //面板
        List<PanelCalculationObj> retCalPanel = new ArrayList<PanelCalculationObj>();
        int position =  NumberUtil.convertStringToDoubleInt(panelType)+1;
        Iterator iter = calResult.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String key = String.valueOf(entry.getKey());
            String val = String.valueOf(entry.getValue());
            PanelCalculationObj pco = new PanelCalculationObj();
            if(key.startsWith(calPanelFrameLine)){
                String[] keys = key.split(calPanelSplitKey);
                pco.setCategory(getFrameName(series,keys[1])[1]);
                pco.setPartName(getFrameName(series, keys[1])[0]);
                pco.setPartLM(keys[2]);
                pco.setPartWM(keys[3]);
                if(keys.length>=5) {
                    pco.setPro(buildPro(keys[4]));
                }
                pco.setQuantity(val);
                pco.setInstPostion(""+position);
                pco.setInstSection(""+pos);
                retCalFrameLine.add(pco);
            }
            if(key.startsWith(calPanel)){
                String[] keys = key.split(calPanelSplitKey);
                pco.setCategory(getPanelFaceName(keys[1])[1]);
                pco.setPartName(getPanelFaceName(keys[1])[0]);
                pco.setPartLM(keys[2]);
                pco.setPartWM(keys[3]);
                if(keys.length>=5)
                    pco.setPro(buildPro(keys[4]));

                if(keys.length>=6) {
                    pco.setMemo(keys[5]);
                }
                if(keys.length==7) {
                    pco.setMetaId(keys[6]);
                }


                pco.setQuantity(val);
                pco.setInstPostion(""+position);
                pco.setInstSection(""+pos);
                retCalPanel.add(pco);
            }

        }
        retCal.addAll(retCalFrameLine);
        retCal.addAll(retCalPanel);

    }

    /**
     * 重构bom 第三个sheet Pro 列的：0A 为 A0
     * 包含F：0AF 为AF0；
     * @param pro
     * @return
     */
    private static String buildPro(String pro) {
        if(pro.length()==2){
            pro = ""+pro.toCharArray()[1]+pro.toCharArray()[0];
        }else if(pro.length()==3){
            if(pro.indexOf("F") == 1){
                pro = ""+pro.toCharArray()[2]+ pro.toCharArray()[0]+pro.toCharArray()[1];
            }else if(pro.indexOf("F") == 2){
                pro = ""+pro.toCharArray()[1]+pro.toCharArray()[2]+pro.toCharArray()[0];
            }
        }
        return pro;
    }
    /**
     * 获取面板类型，多语言格式
     * 部分参数需要用BOM 特殊名称：
     * 例如：门面板(铭牌) 改为：门面板(MP)
     * @param key
     * @return
     */
    private static String[] getPanelFaceName(String key) {
        if(EmptyUtil.isEmpty(key) || "null".equals(key)){
            return new String[]{AHUContext.getIntlString(GENERAL_PANEL,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_PUTONG_CATEGORY};//普通面板
        }

        int pType = NumberUtil.convertStringToDoubleInt(key);
        if(UtilityConstant.SYS_PANEL_TYPE_WAI == pType){
            return new String[]{AHUContext.getIntlString(OUTSIDE_PANEL,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_WAI_CATEGORY};//外面板
        }else if(UtilityConstant.SYS_PANEL_TYPE_MEN == pType){
            return new String[]{AHUContext.getIntlString(DOOR_PANEL,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_MEN_CATEGORY};//门面板
        }else if(UtilityConstant.SYS_PANEL_TYPE_CHUFENG == pType){
            return new String[]{AHUContext.getIntlString(OUTLET_AIR_PANEL,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_CHUFENG_CATEGORY};//出风面板
        }else if(UtilityConstant.SYS_PANEL_TYPE_WU == pType){
            return new String[]{AHUContext.getIntlString(WITHOUT_PANEL,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_WU_CATEGORY};//无面板
        }else if(UtilityConstant.SYS_PANEL_TYPE_JIASHI == pType){
            return new String[]{AHUContext.getIntlString(HUMIDIFICATION_PANEL,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_JIASHI_CATEGORY};//加湿面板
        }else if(UtilityConstant.SYS_PANEL_TYPE_JIAQIANG == pType){
            return new String[]{AHUContext.getIntlString(STRENGTHEN_PANEL_BOM,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_JIAQIANG_CATEGORY};//加强面板
        }else if(UtilityConstant.SYS_PANEL_TYPE_ZENGYA == pType){
            return new String[]{AHUContext.getIntlString(POSITIVE_PRESSURE_PANEL,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_ZENGYA_CATEGORY};//正压面板
        }else if(UtilityConstant.SYS_PANEL_TYPE_CHAIXIE == pType){
            return new String[]{AHUContext.getIntlString(REMOVABLE_DOOR_PANEL,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_CHAIXIE_CATEGORY};//拆卸式门
        }else if(UtilityConstant.SYS_PANEL_TYPE_MEN_MING == pType){
            return new String[]{AHUContext.getIntlString(DOOR_PANEL_NAMEPLATE_BOM,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_MEN_MING_CATEGORY};//门面板(铭牌)
        }else{
            return new String[]{AHUContext.getIntlString(GENERAL_PANEL,SIMPLIFIED_CHINESE),UtilityConstant.SYS_PANEL_TYPE_PUTONG_CATEGORY};//普通面板
        }
    }

    /**
     * 获取中框多类型，多语言格式
     *
     * @param series
     * @param key
     * @return
     */
    private static String[] getFrameName(String series, String key) {
        if(EmptyUtil.isEmpty(key) || "null".equals(key)){
            return new String[]{AHUContext.getIntlString(GENERAL_MID_FRAME,SIMPLIFIED_CHINESE),TYPE_PUTONG_CATEGORY};//普通中框
        }

        if(FrameLine.TYPE_JIAQIANG.equals(key)){
            if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {// 39XT
                return new String[]{"内角钢", TYPE_NJG_CATEGORY};//内角钢
            }
            return new String[]{AHUContext.getIntlString(STRENGTHEN_MID_FRAME,SIMPLIFIED_CHINESE),TYPE_JIAQIANG_CATEGORY};//加强中框
        }else if(FrameLine.TYPE_NEI.equals(key)){
            if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {// 39XT
                return new String[]{"内角钢+装饰条", TYPE_NJGP_CATEGORY};//内角钢
            }
            return new String[]{AHUContext.getIntlString(INNER_MID_FRAME,SIMPLIFIED_CHINESE),TYPE_NEI_CATEGORY};//内面板框
        }else if(FrameLine.TYPE_ZUHE.equals(key)){
            return new String[]{AHUContext.getIntlString(COMBINED_MID_FRAME,SIMPLIFIED_CHINESE),TYPE_ZUHE_CATEGORY};//组合中框
        }else if(FrameLine.TYPE_NJG.equals(key)){
            return new String[]{AHUContext.getIntlString(COMBINED_MID_FRAME,SIMPLIFIED_CHINESE),TYPE_NJG_CATEGORY};//内角钢
        }else if(FrameLine.TYPE_NJGP.equals(key)){
            return new String[]{AHUContext.getIntlString(INTERIOR_ANGLE_STEEL_DECORATIVE_STRIP,SIMPLIFIED_CHINESE),TYPE_NJGP_CATEGORY};//内角钢+装饰条
        }else{
            return new String[]{AHUContext.getIntlString(GENERAL_MID_FRAME,SIMPLIFIED_CHINESE),TYPE_PUTONG_CATEGORY};//普通中框
        }
    }

    /**
     * 统计面板中框数据
     * @param panelFace
     * @param calResult
     *  key： 类型+名称+长度+宽度;
     *  key： 类型+名称+长度+宽度+连接选项;
     *  value : 数量
     */
    public static void calPanelFaceObj(PanelFace panelFace,LinkedHashMap<String,Integer> calResult) {
        if (null != panelFace) {
            PanelFace[] subPanels = panelFace.getSubPanels();
            //非叶子面板,计算中框
            if (null != subPanels) {
                String frameType = panelFace.getFrameLine().getType();
                String frameConnector = panelFace.getFrameLine().getLineConnector();
                int frameLen = panelFace.getFrameLine().getLineLength();
                /*
                例如：FLINE_0_18_0 表示:
                "frameLine":{
                    "type":"0",
                    "lineLength":18
                }
                */
                String key = calPanelFrameLine + calPanelSplitKey + frameType + calPanelSplitKey + frameLen+calPanelSplitKey + "0" +calPanelSplitKey + frameConnector ;
                if(calResult.containsKey(key)){
                    calResult.put(key,calResult.get(key)+1);
                }else{
                    calResult.put(key,1);
                }

                for (PanelFace pf : subPanels) {
                    calPanelFaceObj(pf,calResult);
                }
            }else{//叶子面板：计算面板
                String panelType = panelFace.getPanelType();
                int panelLen = panelFace.getPanelLength();
                int panelWidth = panelFace.getPanelWidth();
                String pConnector = panelFace.getPanelConnector();
                String pMemo=panelFace.getMemo();
                String pMetaId=panelFace.getSectionMetaId();

                /*例如：PANEL_6_11_18_0A 表示:
                {
                    "direction":0,
                        "panelWidth":18,
                        "panelLength":11,
                        "id":"5_1",
                        "panelType":"6",
                        "cutDirection":"none",
                        "panelConnector":"0A"
                }*/
                String key = calPanel + calPanelSplitKey + panelType + calPanelSplitKey
                        + panelLen + calPanelSplitKey + panelWidth+ calPanelSplitKey
                        + pConnector+ calPanelSplitKey + pMemo+ calPanelSplitKey + pMetaId;
                if(calResult.containsKey(key)){
                    calResult.put(key,calResult.get(key)+1);
                }else{
                    calResult.put(key,1);
                }
            }
        }
    }
    public static Map<String, Object> getPartitionMeta() {
        Map<String, Object> partitionMeta = new HashMap<>();
        partitionMeta.put(Meta.JOINING_KEYS, UNSPLITABLE_SECTION);
        partitionMeta.put(Meta.MAX_LENGTH, PARTITION_MAX_LENGTH);
        partitionMeta.put(Meta.MIN_LENGTH, PARTITION_MIN_LENGTH);
        return partitionMeta;
    }

    /**
     * Return real max partition length in mm.
     *
     * @return
     */
    public static int getMaxPartitionLength() {
        return PARTITION_MAX_LENGTH * UtilityConstant.SYS_INT_MOLD_UNIT_SIZE;
    }

    /**
     * Return real max partition length in mm.
     *
     * @return
     */
    public static int getMinPartitionLength() {
        return PARTITION_MIN_LENGTH * UtilityConstant.SYS_INT_MOLD_UNIT_SIZE;
    }

    /**
     * Return real min access length in mm.
     *
     * @return
     * @param product
     */
    public static int getMinAccessLength(String product) {
        if(SystemCalculateConstants.AHU_PRODUCT_39CQ.equals(product) || SystemCalculateConstants.AHU_PRODUCT_39G.equals(product)){
            return ACCESS_MIN_LENGTH_CQ_G_WITHDOOR * UtilityConstant.SYS_INT_MOLD_UNIT_SIZE;
        }
        return ACCESS_MIN_LENGTH * UtilityConstant.SYS_INT_MOLD_UNIT_SIZE;
    }

    public static int getMinPartitionWithDoorLength() {
        return PARTITION_WITHDOOR_MIN_LENGTH * UtilityConstant.SYS_INT_MOLD_UNIT_SIZE;
    }

    // @formatter:off
    // useless methods
	/*
	protected static void printPODataArray(Integer[] horizontals, Integer[] horizontalTypes, Integer[] verticals,
			Integer[] verticalTypes) {
		StringBuffer sb = new StringBuffer();
		sb.append(printIntegerArray(horizontals));
		sb.append(printIntegerArray(horizontalTypes));
		sb.append(printIntegerArray(verticals));
		sb.append(printIntegerArray(verticalTypes));
		System.out.println(sb.toString());
	}

	private static String printIntegerArray(Integer[] array) {
		StringBuffer sb = new StringBuffer();
		for (Integer i : array) {
			sb.append("," + i);
		}
		return ("[" + sb.toString().substring(1) + "]");
	}

	protected static int[][] processLayout(AhuLayout layout) {

		int style = layout.getStyle();
		int[][] inputLayout = layout.getLayoutData();

		if (style == 21 || style == 22) {
			int[][] outputLayout = new int[3][];
			// 将双层机组的上层进行合并
			// outputLayout[0]=new int[inputLayout[0].length+inputLayout[1].length];
			outputLayout[0] = ArrayUtils.addAll(inputLayout[0], inputLayout[1]);
			// 将双层机组的下层进行合并
			// outputLayout[1]=new int[inputLayout[2].length+inputLayout[3].length];
			outputLayout[2] = ArrayUtils.addAll(inputLayout[2], inputLayout[3]);

			return outputLayout;

		}

		return inputLayout;
	}
	*/

}

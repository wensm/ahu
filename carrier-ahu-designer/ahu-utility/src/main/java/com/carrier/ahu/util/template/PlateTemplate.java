package com.carrier.ahu.util.template;

import static com.carrier.ahu.constant.CommonConstant.METASEXON_AIRDIRECTION;
import static com.carrier.ahu.util.meta.SectionMetaUtils.MetaKey.KEY_RETURN_BACK;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.GroupTypeEnum;
import com.carrier.ahu.common.enums.LayoutStyleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;

/**
 * Created by Braden Zhou on 2018/11/07.
 */
public class PlateTemplate extends TemplateFactory {

    public PlateTemplate(GroupTypeEnum groupType) {
        super(groupType);
    }

    @Override
    public LayoutStyleEnum getSectionLayout() {
        return LayoutStyleEnum.PLATE;
    }

    @Override
    public SectionTypeEnum[] getPreDefinedSectionTypes() {
        return new SectionTypeEnum[] { SectionTypeEnum.TYPE_MIX, SectionTypeEnum.TYPE_COMPOSITE,
                SectionTypeEnum.TYPE_PLATEHEATRECYCLE, SectionTypeEnum.TYPE_SINGLE, SectionTypeEnum.TYPE_MIX,
                SectionTypeEnum.TYPE_FAN, SectionTypeEnum.TYPE_ACCESS, SectionTypeEnum.TYPE_PLATEHEATRECYCLE,
                SectionTypeEnum.TYPE_COLD, SectionTypeEnum.TYPE_HEATINGCOIL, SectionTypeEnum.TYPE_STEAMHUMIDIFIER,
                SectionTypeEnum.TYPE_FAN };
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<Part> preConfigureSections(List<Part> parts) {
        String[] airReturnsOfMix = { KEY_RETURN_BACK, KEY_RETURN_BACK };
        int mixIndex = 0;
        List<Part> updatedParts = new ArrayList<Part>();
        for (Part part : parts) {
            String metaJson = part.getMetaJson();
            Map<String, Object> partMeta = JSONArray.parseObject(metaJson, Map.class);
            SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(part.getSectionKey());
            if (getSectionLayout().isInReturnAirLayer(part.getPosition())) {
                partMeta.put(METASEXON_AIRDIRECTION, AirDirectionEnum.RETURNAIR.getCode());
            } else {
                partMeta.put(METASEXON_AIRDIRECTION, AirDirectionEnum.SUPPLYAIR.getCode());
            }
            if (SectionTypeEnum.TYPE_MIX.equals(sectionType)) {
                setMixAirReturn(partMeta, airReturnsOfMix[mixIndex]);
                mixIndex++;
            }
            part.setMetaJson(JSONArray.toJSONString(partMeta));
            updatedParts.add(part);
        }
        return updatedParts;
    }

}

package com.carrier.ahu.wpd;

import lombok.Data;

/**
 * Created by Braden Zhou on 2019/07/12.
 */
@Data
class TubeDetail {

    private double t_len;
    private double t_id;
    private double w_vel;

    public TubeDetail() {
        this.t_len = 0;
        this.t_id = 0;
        this.w_vel = 0;
    }

}

package com.carrier.ahu.util.meta;

import java.util.ArrayList;
import java.util.List;

import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.coil.SCalSeq;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.vo.SystemCalculateConstants;

/**
 * Utility class for metadata beans in entity.coil package.
 * <p>
 * extract from SCalSeqDao class.
 * <p>
 * Created by Braden Zhou on 2018/06/06.
 */
public class CoilMetaUtils implements SystemCalculateConstants {

    public static List<SCalSeq> getsCalSeqPoListByCFRU(String circuit, int fpi, int row, String unitType, String coil,
                                                       boolean calculateOptimalResults) {
        List<SCalSeq> sCalSeqList = AhuMetadata.findAll(SCalSeq.class);

        List<SCalSeq> sCalSeqPoList = new ArrayList<SCalSeq>();
        Integer fanHeight = Integer.valueOf(unitType.substring(unitType.length() - 4, unitType.length() - 2));// 风机高度
        for (SCalSeq sCalSeqPo : sCalSeqList) {
            if (SystemCountUtil.gteBigUnitHeight(fanHeight)) {// 风机高度大于25查询fanHeight >= 25
                if (waterCoilH.equals(coil)) {// 热水盘管只有2排
                    if (sCalSeqPo.getRow() == 2) {
                        if (HEATINGCOIL_ROWS_AUTO.equals(circuit) && -1 == fpi) {
                            sCalSeqPoList.add(sCalSeqPo);
                        } else if (!HEATINGCOIL_ROWS_AUTO.equals(circuit) && -1 != fpi) {
                            if (circuit.equals(sCalSeqPo.getCircuit()) && fpi == sCalSeqPo.getFpi()) {
                                sCalSeqPoList.add(sCalSeqPo);
                            }
                        } else if (HEATINGCOIL_ROWS_AUTO.equals(circuit) && -1 != fpi) {
                            if (fpi == sCalSeqPo.getFpi()) {
                                sCalSeqPoList.add(sCalSeqPo);
                            }
                        } else if (!HEATINGCOIL_ROWS_AUTO.equals(circuit) && -1 == fpi) {
                            if (circuit.equals(sCalSeqPo.getCircuit())) {
                                sCalSeqPoList.add(sCalSeqPo);
                            }
                        }
                    }
                } else {// 冷水盘管
                    if (sCalSeqPo.getRow() <= 2) {// 大机组过滤2排排数
                        continue;
                    } else {
                        if (EmptyUtil.isNotEmpty(sCalSeqPo.getBigAhu()) && 1 == sCalSeqPo.getBigAhu()) {// 判断数据为大机组标识
                            if (COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 == fpi && -1 == row) {// 全部为AUTO
                                // 最终筛选出大型机组、回路、排数、管径为ALL
                                sCalSeqPoList.add(sCalSeqPo);
                            } else if (COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 == fpi && -1 != row) {
                                if (row == sCalSeqPo.getRow()) {// 根据排数筛选
                                    sCalSeqPoList.add(sCalSeqPo);
                                }
                            } else if (COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 != fpi && -1 != row) {
                                if (fpi == sCalSeqPo.getFpi() && row == sCalSeqPo.getRow()) {// 根据片距、排数筛选
                                    sCalSeqPoList.add(sCalSeqPo);
                                }
                            } else if (COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 != fpi && -1 == row) {
                                if (fpi == sCalSeqPo.getFpi()) {// 根据片距筛选
                                    sCalSeqPoList.add(sCalSeqPo);
                                }
                            } else if (!COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 != fpi && -1 == row) {
                                if (circuit.equals(sCalSeqPo.getCircuit()) && fpi == sCalSeqPo.getFpi()) {// 根据回路、片距筛选
                                    sCalSeqPoList.add(sCalSeqPo);
                                }
                            } else if (!COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 == fpi && -1 == row) {
                                if (circuit.equals(sCalSeqPo.getCircuit())) {// 根据回路筛选
                                    sCalSeqPoList.add(sCalSeqPo);
                                }
                            } else if (!COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 == fpi && -1 != row) {
                                if (circuit.equals(sCalSeqPo.getCircuit()) && row == sCalSeqPo.getRow()) {// 根据回路、排数筛选
                                    sCalSeqPoList.add(sCalSeqPo);
                                }
                            } else if (!COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 != fpi && -1 != row) {
                                if (circuit.equals(sCalSeqPo.getCircuit()) && fpi == sCalSeqPo.getFpi()// 根据回路、片距、排数、筛选
                                        && row == sCalSeqPo.getRow()) {
                                    sCalSeqPoList.add(sCalSeqPo);
                                }
                            }
                        }
                    }
                }
            } else {
                if (COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 == fpi && -1 == row) {// 全部为AUTO
                    sCalSeqPoList.add(sCalSeqPo);// 最终筛选、回路、排数、管径为ALL
                } else if (COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 == fpi && -1 != row) {
                    if (row == sCalSeqPo.getRow()) {
                        sCalSeqPoList.add(sCalSeqPo);// 根据排数筛选
                    }
                } else if (COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 != fpi && -1 != row) {
                    if (fpi == sCalSeqPo.getFpi() && row == sCalSeqPo.getRow()) {
                        sCalSeqPoList.add(sCalSeqPo);// 根据片距、排数筛选
                    }
                } else if (COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 != fpi && -1 == row) {
                    if (fpi == sCalSeqPo.getFpi()) {
                        sCalSeqPoList.add(sCalSeqPo);// 根据片距筛选
                    }
                } else if (!COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 != fpi && -1 == row) {
                    if (circuit.equals(sCalSeqPo.getCircuit()) && fpi == sCalSeqPo.getFpi()) {
                        sCalSeqPoList.add(sCalSeqPo);// 根据回路、片距筛选
                    }
                } else if (!COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 == fpi && -1 == row) {
                    if (circuit.equals(sCalSeqPo.getCircuit())) {
                        sCalSeqPoList.add(sCalSeqPo);// 根据回路筛选
                    }
                } else if (!COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 == fpi && -1 != row) {
                    if (circuit.equals(sCalSeqPo.getCircuit()) && row == sCalSeqPo.getRow()) {
                        sCalSeqPoList.add(sCalSeqPo);// 根据回路、排数筛选
                    }
                } else if (!COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 != fpi && -1 != row) {
                    if (circuit.equals(sCalSeqPo.getCircuit()) && fpi == sCalSeqPo.getFpi()
                            && row == sCalSeqPo.getRow()) {
                        sCalSeqPoList.add(sCalSeqPo);// 根据回路、片距、排数、筛选
                    }
                }
            }
        }

        List<SCalSeq> rowSCalSeqPoList = new ArrayList<SCalSeq>();
        // 根据盘管 hot cold 筛选结果
        for (SCalSeq sCalSeqPo : sCalSeqPoList) {
            // 如果条件都为auto过滤2排计算
            if (COOLINGCOIL_CIRCUIT_AUTO.equals(circuit) && -1 == fpi && -1 == row) {
                if (waterCoilH.equals(coil)) {// 过滤 排数 10、12
                    if (10 != sCalSeqPo.getRow() || 12 != sCalSeqPo.getRow()) {
                        rowSCalSeqPoList.add(sCalSeqPo);
                    }
                } else {
                    if (sCalSeqPo.getRow() > 2) {
                        rowSCalSeqPoList.add(sCalSeqPo);
                    }
                }
            } else {
                if (waterCoilH.equals(coil)) {// 过滤 排数 10、12
                    if (10 != sCalSeqPo.getRow() || 12 != sCalSeqPo.getRow()) {
                        rowSCalSeqPoList.add(sCalSeqPo);
                    }
                } else {
                    rowSCalSeqPoList.add(sCalSeqPo);
                }
            }
        }
        sCalSeqPoList = rowSCalSeqPoList;

        if (calculateOptimalResults) {// 如果TOP5只查询满足条件的5条数据
            List<SCalSeq> seqPoList = new ArrayList<SCalSeq>();
            int i = 0;
            for (SCalSeq sCalSeqPo : sCalSeqPoList) {
                if (i < 5) {
                    seqPoList.add(sCalSeqPo);
                    i++;
                } else {
                    break;
                }
            }
            sCalSeqPoList = new ArrayList<SCalSeq>();
            sCalSeqPoList.addAll(seqPoList);
        }
        return sCalSeqPoList;
    }

}

package com.carrier.ahu.util;

import com.alibaba.fastjson.JSONArray;
import com.carrier.ahu.calculator.ExcelForPriceCodeUtils;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.Partition;
import com.carrier.ahu.common.entity.Project;
import com.carrier.ahu.common.entity.Unit;
import com.carrier.ahu.common.enums.AirDirectionEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.enums.UnitSystemEnum;
import com.carrier.ahu.common.intl.I18NConstants;
import com.carrier.ahu.common.model.partition.AhuPartition;
import com.carrier.ahu.common.util.AhuUtil;
import com.carrier.ahu.common.util.MapValueUtils;
import com.carrier.ahu.common.util.ReflectionUtils;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.engine.cad.CadUtils;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.ReportMetadata;
import com.carrier.ahu.model.calunit.AhuParam;
import com.carrier.ahu.model.calunit.PartParam;
import com.carrier.ahu.model.calunit.UnitUtil;
import com.carrier.ahu.po.meta.unit.UnitConverter;
import com.carrier.ahu.psychometric.PsyCalBean;
import com.carrier.ahu.psychometric.PsychometricDrawer;
import com.carrier.ahu.report.PartPO;
import com.carrier.ahu.report.Report;
import com.carrier.ahu.report.ReportData;
import com.carrier.ahu.report.ReportItem;
import com.carrier.ahu.report.common.ReportConstants;
import com.carrier.ahu.report.content.ReportContent;
import com.carrier.ahu.util.meta.SectionMetaUtils;
import com.carrier.ahu.util.partition.AhuPartitionUtils;
import com.carrier.ahu.vo.FileNamesLoadInSystem;
import com.carrier.ahu.vo.SysConstants;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.*;
import java.util.Map.Entry;

import static com.carrier.ahu.common.configuration.AHUContext.getFontFamily;
import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.constant.CommonConstant.METASEXON_AIRDIRECTION;
import static com.carrier.ahu.constant.CommonConstant.SYS_PATH_EUROLOGOPATH;
import static com.carrier.ahu.constant.CommonConstant.TECHPROJ_TEMPLATE_WORD;
import static com.carrier.ahu.constant.CommonConstant.TECHPROJ_TEMPLATE_WORD_EN;
import static com.carrier.ahu.constant.CommonConstant.TECHPROJ_TEMPLATE_WORD_NOAERO;
import static com.carrier.ahu.constant.CommonConstant.TECHPROJ_TEMPLATE_WORD_NOAERO_EN;
import static com.carrier.ahu.constant.CommonConstant.TEMPLATE_WORD;
import static com.carrier.ahu.constant.CommonConstant.TEMPLATE_WORD_EN;
import static com.carrier.ahu.report.common.ReportConstants.*;
import static com.carrier.ahu.util.DateUtil.HH_MM;
import static com.carrier.ahu.util.DateUtil.YYYY_MM_DD;

@Component
public class Word4ReportUtils {
	protected static Logger logger = LoggerFactory.getLogger(Word4ReportUtils.class);
	
	private static String PatchVersion;
	
	@Value("${ahu.patch.version}")
	private void setPatchVersion(String pv) {
		PatchVersion = pv;
	}
	
	/** WORD类型 - 非标清单 */
	public static final String T_SPECIALLIST = "speciallist";
	/** WORD类型 - 技术说明 （工程版）*/
	public static final String T_TECHPROJ = "techprojWord";

	/**
	 * 非标清单word文件生成入口，
	 * @param file
	 * @param report
	 * @param reportData
	 * @param arrayMap
	 * @param language
	 * @param unitType
	 * @throws Exception
	 */
    public static void genWord4Speciallist(File file, Report report, ReportData reportData, LanguageEnum language,
                                           UnitSystemEnum unitType) throws Exception {
		String reportTemplate = LanguageEnum.Chinese.equals(language)
				? TEMPLATE_WORD
				: TEMPLATE_WORD_EN;
		InputStream is = new FileInputStream(reportTemplate);
		XWPFDocument doc = new XWPFDocument(is);

		/* 1加载模板 */
		//警告，params 的 key 不能拿包含特殊符号必须是一个单词否则 poi截取出现问题
		Map<String, Object> params = getProjectParameters(reportData.getProject());
		XWPFHeader header = doc.getHeaderFooterPolicy().getDefaultHeader();
		/* 2替换模板页眉信息 */
		replaceInHeaderPara(header, params);// 替换header 段落变量
		replaceInHeaderTable(header, params);// 替换header 表格变量
		
		/* 3添加非标具体信息 */
		Project project = reportData.getProject();
		Map<String, Partition> partitionMap = reportData.getPartitionMap();
		for (Unit unit : reportData.getUnitList()) {
			Partition partition = null;
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}

			List<PartPO> partList = new ArrayList<>();
			Boolean isRecycle = false;
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {

                    if(e.getKey().contains(SectionTypeEnum.TYPE_WHEELHEATRECYCLE.getId()) ||
                            e.getKey().contains(SectionTypeEnum.TYPE_PLATEHEATRECYCLE.getId())){
                        if(!isRecycle){
                            partList.add(e.getValue());
                        }
                        isRecycle = true;
                    }else{
                        partList.add(e.getValue());
                    }
				}
			}

			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
			Map<String, String> allInOneMap = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);

			//ahu信息添加
			Map<String, String> ahuMap = allMap.get(UnitConverter.UNIT);
			String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_AHU), reportData.getProject(), unit,
					ahuMap, language, unitType);
			createTable(doc,ParagraphAlignment.LEFT, true, false, ahuContents.length, ahuContents[0].length, ahuContents);

			//总价
			double totalPrice[] = new double[1];
			String enable = String.valueOf(ahuMap.get(UtilityConstant.METANS_NS_AHU_ENABLE));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
				//ahu ns 信息添加


				String[][] ahu2Contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_AHU_NONSTANDARD), reportData.getProject(), unit,
						ahuMap, language, unitType);
				List<Integer> removeIndex = new ArrayList<Integer>();
				removeIndex.add(2);
				//变形
				String deformationEnable = String.valueOf(ahuMap.get(UtilityConstant.METANS_NS_AHU_DEFORMATION));
				if(UtilityConstant.SYS_ASSERT_TRUE.equals(deformationEnable)) {
					if(!UtilityConstant.SYS_BLANK.equals(ahu2Contents[1][7]) && !UtilityConstant.SYS_NOT_NUM.equals(ahu2Contents[1][7])) {
						double price1 = NumberUtil.convertStringToDouble(ahu2Contents[1][7]);
						totalPrice[0] = totalPrice[0] + price1;
					}
				}else{
					removeIndex.add(1);
                    
				}
				
				//槽钢底座
				String nsChannelSteelBaseEnable = String.valueOf(ahuMap.get(UtilityConstant.METANS_NS_AHU_NSCHANNELSTEELBASE));
				if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsChannelSteelBaseEnable)) {
					if(!UtilityConstant.SYS_BLANK.equals(ahu2Contents[3][7]) && !UtilityConstant.SYS_NOT_NUM.equals(ahu2Contents[3][7])) {
						double price2 = NumberUtil.convertStringToDouble(ahu2Contents[3][7]);
						totalPrice[0] = totalPrice[0] + price2;
					}
				}else{
					removeIndex.add(3);
				}

				//没勾选的选项不打印（变形、槽钢底座）
				List<List<String>> nsAhu2ContentsList = new ArrayList<>();
				for (int i = 0; i < ahu2Contents.length; i++) {
					if(!removeIndex.contains(i)){
						List<String> tempList = new ArrayList<>();
						for (String s : ahu2Contents[i]) {
							tempList.add(s);
						}
						nsAhu2ContentsList.add(tempList);
					}
				}
				String[][] fianlAhu2Contents =  new String[nsAhu2ContentsList.size()][nsAhu2ContentsList.get(0).size()];
				for (int i = 0; i < nsAhu2ContentsList.size(); i++) {
					for (int j = 0; j < nsAhu2ContentsList.get(i).size(); j++) {
						fianlAhu2Contents[i][j] = nsAhu2ContentsList.get(i).get(j);
					}
				}
				createTable(doc,ParagraphAlignment.LEFT, true, false, fianlAhu2Contents.length, fianlAhu2Contents[0].length, fianlAhu2Contents);
				
				String[][] ahu3Contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_AHU_NONSTANDARD_MEMO), reportData.getProject(), unit,
						ahuMap, language, unitType);
				createTableNS(doc,ParagraphAlignment.LEFT, false, false, ahu3Contents.length, ahu3Contents[0].length, ahu3Contents);
				
				try {
					double price = NumberUtil.convertStringToDouble(ahu3Contents[0][3]);
					totalPrice[0] = totalPrice[0] + price;

				} catch (Exception e) {
					
				}
			}

			//段信息添加
			String groupCode = unit.getGroupCode();
			if (null  == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
				logger.warn(MessageFormat.format("非标报告-AHU组编码信息不合法，报告忽略声称对应段信息 - unitNo：{0},GroupCode:{1}",
						new Object[] { unit.getUnitNo(), unit.getGroupCode() }));
			} else {
                int order = 1;
                for (PartPO partPO : partList) {
                    Part part = partPO.getCurrentPart();
                    String key = part.getSectionKey();
                    Map<String, String> noChangeMap = allMap.get(UnitConverter.genUnitKey(unit, part));
                    addSection(doc, key, order, totalPrice, noChangeMap, language, unitType, project, unit);
                    order++;
                }
			}
			//总价信息添加
			String[][] totalPriceContents =  new String[1][4];
			totalPriceContents[0][0] = UtilityConstant.SYS_BLANK;
			totalPriceContents[0][1] = UtilityConstant.SYS_BLANK;
			totalPriceContents[0][2] = UtilityConstant.SYS_BLANK;
			totalPriceContents[0][3] = "TOTAL："+totalPrice[0];
			createTable(doc,ParagraphAlignment.RIGHT, true, false, totalPriceContents.length, totalPriceContents[0].length, totalPriceContents);

			//报告尾部信息添加
			String[][] tailContents1 =  new String[1][1];
			tailContents1[0][0] = getIntlString(REPORT_NS_TAIL1);
			createTable(doc,ParagraphAlignment.LEFT, false,true, tailContents1.length, tailContents1[0].length, tailContents1);
			String[][] tailContents2 =  new String[1][2];
			tailContents2[0][0] = getIntlString(REPORT_NS_TAIL2);
			tailContents2[0][1] = getIntlString(REPORT_NS_TAIL3);
			createTable(doc,ParagraphAlignment.LEFT,false, true, tailContents2.length, tailContents2[0].length, tailContents2);

		}

		OutputStream os = new FileOutputStream(file.getPath());
		doc.write(os);
		close(os);
		close(is);
		
	}

    private static Map<String, Object> getProjectParameters(Project project) {
        Map<String, Object> params = new HashMap<String, Object>();
        Date date = new Date();
        params.put(ReportConstants.SPECIALLIST_ATTR_DATE, DateUtil.getDateTimeString(date, YYYY_MM_DD));
        params.put(ReportConstants.SPECIALLIST_ATTR_TIME, DateUtil.getDateTimeString(date, HH_MM));
//        params.put(ReportConstants.SPECIALLIST_ATTR_SYSTEMVERSION, AHUContext.getAhuVersion());
		params.put(ReportConstants.SPECIALLIST_ATTR_SYSTEMVERSION,
				(AHUContext.isExportVersion()
						? (AHUContext.getAhuVersion() + UtilityConstant.SYS_BLANK_SPACE
								+ UtilityConstant.SYS_VERSION_EXPORT + UtilityConstant.SYS_BLANK_SPACE + (PatchVersion.contains("-")?PatchVersion.split("-")[0]:PatchVersion))
						: (AHUContext.getAhuVersion() + UtilityConstant.SYS_BLANK_SPACE + (PatchVersion.contains("-")?PatchVersion.split("-")[0]:PatchVersion))));
        params.put(ReportConstants.SPECIALLIST_ATTR_GZL, StringUtils.EMPTY);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_NO);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_CONTRACT);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_NAME);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_SALER);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_ADDRESS);
        putProperty(params, project, ReportConstants.SPECIALLIST_ATTR_ENQUIRYNO);
        return params;
    }
    
    private static Map<String, Object> getUnitParameters(Unit unit) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(ReportConstants.SPECIALLIST_ATTR_CUSTOMERPO, unit.getCustomerName());
        return params;
    }

    private static void putProperty(Map<String, Object> params, Project project, String name) {
        params.put(name, ReflectionUtils.getProperty(project, name));
    }

	/**
	 * 技术说明（工程版）文件生成入口
	 * @param file
	 * @param report
	 * @param reportData
	 * @param language
	 * @param unitType
	 * @throws Exception
	 */
    public static void genWord4Techproj(File file, Report report, ReportData reportData, LanguageEnum language,
                                        UnitSystemEnum unitType) throws Exception {

		Unit firstUnit = reportData.getUnitList().get(0);
		Map<String, Object> projectParams = getProjectParameters(reportData.getProject());
		String euroLogoPath = "";
		String reportTemplate = LanguageEnum.Chinese.equals(language)
				? TECHPROJ_TEMPLATE_WORD
				: TECHPROJ_TEMPLATE_WORD_EN;
		//只有CQ才打印aero图标
		if(!SystemCountUtil.isAeroUnit(firstUnit)){
			reportTemplate = LanguageEnum.Chinese.equals(language)
					? TECHPROJ_TEMPLATE_WORD_NOAERO
					: TECHPROJ_TEMPLATE_WORD_NOAERO_EN;
		}
		if(SystemCountUtil.isEuroUnit(firstUnit)){//CQ/XT/G有欧标认证
			String series = firstUnit.getSeries();
			if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39CQ) || series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39G)) {// 39CQ 39G
				euroLogoPath = SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR + "Label_ECC_39CQ_ch.png";
			} else if (series.startsWith(UtilityConstant.SYS_UNIT_SERIES_39XT)) {// 39XT
				euroLogoPath = SysConstants.ASSERT_DIR + SysConstants.REPORT_IMG_DIR + "Label_ECC_39XT_ch.png";
			}
		}
		projectParams.put(SYS_PATH_EUROLOGOPATH,euroLogoPath);
		projectParams.put(SYS_WORD_TECH_REPORT_SYSNAME,MessageFormat.format("{0}：CarrierAHU",getIntlString(I18NConstants.SOFTWARE_NAME)));

		InputStream is = new FileInputStream(reportTemplate);
		XWPFDocument doc = new XWPFDocument(is);
		
		/* 1加载模板 */
		XWPFHeader header = doc.getHeaderFooterPolicy().getDefaultHeader();
        /* 2替换模板页眉信息 */
        replaceInHeaderPara(header, projectParams);// 替换header 段落变量
        replaceInHeaderTable(header, projectParams);// 替换header 表格变量

		/* 3添加非标具体信息 */
		Project project = reportData.getProject();
		Map<String, Partition> partitionMap = reportData.getPartitionMap();
		for (int i = 0; i < reportData.getUnitList().size(); i++) {
            Unit unit = reportData.getUnitList().get(i);

            Map<String, Object> unitParams = getUnitParameters(unit);
            replaceInHeaderTable(header, unitParams); // update customer PO number in header

			Partition partition = null;
			if (EmptyUtil.isNotEmpty(partitionMap)) {
				partition = partitionMap.get(unit.getUnitid());
			}
			List<PartPO> partList = new ArrayList<>();
			for (Entry<String, PartPO> e : reportData.getPartMap().entrySet()) {
				if (e.getKey().contains(UnitConverter.genUnitKey(unit, e.getValue().getCurrentPart()))) {
					partList.add(e.getValue());
				}
			}
			
			Map<String, Map<String, String>> allMap = ValueFormatUtil.getAllUnitMetaJsonData(unit, partList);
			Map<String, String> allMap2 = ValueFormatUtil.getAllUnitMetaJsonData2OneMap(unit, partList);
			List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);
			//添加ahu机组信息
			String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_AHU_OVERALL), reportData.getProject(), unit,
					allMap.get(UnitConverter.UNIT), language, unitType);
			ahuContents = SectionContentConvertUtils.getAhuOverall(ahuContents, partition, unit, language, allMap.get(UnitConverter.UNIT));
			WordTableGen.createTable(doc,ParagraphAlignment.LEFT, true, false, ahuContents.length, ahuContents[0].length, ahuContents);

			if (EmptyUtil.isNotEmpty(partition)) {
				// String[][] titleContents = arrayMap.get("TechAhu2");
				String[][] ahuboxContents = SectionContentConvertUtils.getAhu(partition, unit, language , allMap.get(UnitConverter.UNIT));
				String[][] ahu2Contents = ValueFormatUtil.transReport(ahuboxContents, reportData.getProject(), unit,
						allMap.get(UnitConverter.UNIT), language, unitType);
				WordTableGen.createTable(doc, ParagraphAlignment.LEFT, false, false, ahu2Contents.length,
						ahu2Contents[0].length, ahu2Contents);
			}

			//添加ahu非标
			DocTableGen.addCommonF(doc, allMap.get(UnitConverter.UNIT), language, unitType, project, unit,  SectionTypeEnum.TYPE_AHU);
			String enable = String.valueOf(allMap.get(UnitConverter.UNIT).get(UtilityConstant.METANS_NS_AHU_ENABLE));//ahu是否非标
			if (UtilityConstant.SYS_ASSERT_TRUE.equals(enable)) {
				String[][] nsAHUcontents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_NS_AHU_NONSTANDARD), project, unit, allMap.get(UnitConverter.UNIT), language,
						unitType);
				nsAHUcontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(SectionTypeEnum.TYPE_AHU.getId()).getCnName(),language) + nsAHUcontents[0][0];//替换段名称
				nsAHUcontents = SectionContentConvertUtils.getAhuNS(nsAHUcontents, partition, unit, language, allMap.get(UnitConverter.UNIT));
				if(EmptyUtil.isNotEmpty(nsAHUcontents))
				WordTableGen.createTable(doc, ParagraphAlignment.LEFT, false, false, nsAHUcontents.length,
						nsAHUcontents[0].length, nsAHUcontents);
			}
			
			XWPFParagraph p = createParagraph(doc);
			p.setAlignment(ParagraphAlignment.LEFT);
			XWPFRun r = p.createRun();
			r.setText(getIntlString(UNIT_INCLUDE_BASED_ON_AIR_FLOW_DIRECTION));
			AhuParam ahuParam = ValueFormatUtil.transDBData2AhuParam(project, unit, partList, partition);

			//段信息添加
			String groupCode = unit.getGroupCode();
			if (null  == groupCode || groupCode.length() == 0 || groupCode.length() % 3 != 0) {
				logger.warn(MessageFormat.format("非标报告-AHU组编码信息不合法，报告忽略声称对应段信息 - unitNo：{0},GroupCode:{1}",
						new Object[] { unit.getUnitNo(), unit.getGroupCode() }));
			} else {
				int order = 1;

				List<PartPO> orderedPartList = AhuPartitionUtils.getAirflowOrderedPartPOListForReport(unit, partList);
				for (PartPO partPO : orderedPartList) {
					Part part = partPO.getCurrentPart();
					ExcelForPriceCodeUtils.setPartitionOfPart(part, ahuPartitionList);
					String key = part.getSectionKey();
					 Map<String, String> noChangeMap=(allMap.get(UnitConverter.genUnitKey(unit, part)));
					 Map<String, String> cloneMap=UnitUtil.clone(noChangeMap);
					DocTableGen.addSection(doc,key,String.valueOf(part.getPos()),order, noChangeMap,cloneMap, language, unitType, project, unit ,ahuParam);
					order++;
				}
			}
			
			for (ReportItem item : report.getItems()) {
				if (!item.isOutput()) {
					continue;
				}

				if (ReportConstants.CLASSIFY_TVIEW.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						addImage3View(doc, ahuParam, language);
					} catch (Exception e) {
						logger.error("工程版报告》添加三视图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_HANSHI.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						addImagePsychometric(doc, ahuParam,
								language);
					} catch (Exception e) {
						logger.error("工程版报告》添加焓湿图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_FAN.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						AhuParam param = ahuParam;
						List<PartParam> partParams = param.getPartParams();
						int fanCount = 0;
						for (PartParam partParam : partParams) {
							if (SectionTypeEnum.TYPE_FAN.getId().equals(partParam.getKey())) {
								fanCount++;
							}
						}
						for (PartParam partParam : partParams) {
							if (SectionTypeEnum.TYPE_FAN.getId().equals(partParam.getKey())) {
								addImageFan(doc, allMap, language, unitType, project, unit, partParam,fanCount);
							}
						}
					} catch (Exception e) {
						logger.error("工程版报告》添加风机曲线图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_PANEL_SCREENSHOTS.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						addImagePanelScreenshots(doc , unit , partition , language);
					} catch (Exception e) {
						logger.error("工程版报告》添加面板布置图》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_UNIT_NAMEPLATE_DATA.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						//机组段数
						String partitionJson = partition.getPartitionJson();
                        if (EmptyUtil.isNotEmpty(partitionJson)) {
                            allMap2.put(UtilityConstant.INFO_AHU_SECTION_COUNT,
                                    String.valueOf(ahuPartitionList.size()));                        	
                        }else {
                            allMap2.put(UtilityConstant.INFO_AHU_SECTION_COUNT,
                                    String.valueOf(UtilityConstant.SYS_STRING_NUMBER_1));
                        }
						addUnitNameplateData(doc, allMap, allMap2,project,unitType,unit , partition , language);
					} catch (Exception e) {
						logger.error("工程版报告》机组铭牌数据》报错",e);
					}
				}
				int connectionNum=ahuPartitionList.size()-1;
				if (ReportConstants.CLASSIFY_SECTION_CONNECTION_LIST.equals(item.getClassify())) {
					try {
						//第一个段连接处使用页面下拉的类型，后面的段使用“标准”类型
						String paneltype = unit.getPaneltype();
						//最新版本段连接清单逻辑
						if(String.valueOf(paneltype).contains("[")){
							List<String> types = JSONArray.parseArray(paneltype, String.class);
							for (String type : types) {
								String[] tempType = type.split(":");
								String theType = tempType[0];
								int theCount = Integer.parseInt(tempType[1]);
								for(int tIndex=0;tIndex<theCount;tIndex++) {
									addSectionConnectionList(theType, doc, unit, language, allMap.get(UnitConverter.UNIT));
								}
							}

						}else if(connectionNum > 0){
							if (EmptyUtil.isEmpty(paneltype) || AhuUtil.isValidProduct(paneltype.toUpperCase()) || String.valueOf(paneltype).contains("\"") || String.valueOf(paneltype).contains("[]")) {//paneltype为空的时候默认“标准”,panelType 莫名其妙变成39CQ 的情况也处理为默认“标准”
								paneltype = UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD;
							}
							for (int num = 1; num <= connectionNum; num++) {
								if (num > 1) {
									paneltype = UtilityConstant.JSON_UNIT_PANELTYPE_STANDARD;
								}

								XWPFParagraph paragraph = createParagraph(doc);
								paragraph.setPageBreak(true);
								addSectionConnectionList(paneltype, doc, unit, language, allMap.get(UnitConverter.UNIT));
							}
						}
					} catch (Exception e) {
						logger.error("工程版报告》段连接清单》报错",e);
					}
				}
				if (ReportConstants.CLASSIFY_PRODUCT_PACKING_LIST.equals(item.getClassify())) {
					try {
						XWPFParagraph paragraph = createParagraph(doc);
						paragraph.setPageBreak(true);
						addProductPackingList(doc,allMap, allMap2, project, unitType, unit, partition, language, connectionNum,partList);
					} catch (Exception e) {
						logger.error("工程版报告》产品装箱单》报错",e);
					}
				}
			}
            if (i < reportData.getUnitList().size() - 1) {
                WordTableGen.addNewPage(doc);
            }
		}
		OutputStream os = new FileOutputStream(file.getPath());
		doc.write(os);
		close(os);
		close(is);
	}

    private static XWPFParagraph createParagraph(XWPFDocument doc) {
        XWPFParagraph paragraph = doc.createParagraph();
        paragraph.setSpacingAfter(5);
        return paragraph;
    }

	/**
	 * 添加段信息
	 * @param doc
	 * @param key
	 * @param order
	 * @param totalPrice
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @throws Exception
	 */
    private static void addSection(XWPFDocument doc, String key, int order, double[] totalPrice,
                                   Map<String, String> allMap, LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit)
            throws Exception {
        SectionTypeEnum sectionType = SectionTypeEnum.getSectionTypeFromId(key);
        switch (sectionType) {
        case TYPE_COLD:
            addCoolingCoilNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_HEATINGCOIL:
            addHeatingCoilNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_STEAMHUMIDIFIER:
            addSteamHumidifierNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_WETFILMHUMIDIFIER:
            addWetfilmhumidifierNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_SPRAYHUMIDIFIER:
            addSprayHumidifierNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_FAN:
            addFanNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_COMBINEDMIXINGCHAMBER:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_ATTENUATOR:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_DISCHARGE:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_ACCESS:
        	addAccessNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_HEPAFILTER:
        	addHepafilterNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        case TYPE_ELECTRODEHUMIDIFIER:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_ELECTROSTATICFILTER:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_CTR:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_HEATRECYCLE:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_WHEELHEATRECYCLE:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
		case TYPE_PLATEHEATRECYCLE:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_STEAMCOIL:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_ELECTRICHEATINGCOIL:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_DIRECTEXPENSIONCOIL:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_MIX:
			addCommonNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_SINGLE:
        	addSingleNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
			break;
        case TYPE_COMPOSITE:
            addCompositeNS(doc, totalPrice, allMap, language, unitType, project, unit, sectionType);
            break;
        default:
            logger.warn(MessageFormat.format("非标报告-AHU段信息未知，报告忽略声称对应段信息 - unitNo：{0},partTypeCode:{1}",
                    unit.getUnitNo(), key));
        }
    }

	/**
	 * 添加非标信息
	 * @param doc
	 * @param totalPrice
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param sectionType
	 * @throws Exception
	 */
	private static void addCommonNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                    LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
			throws Exception {
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){//启用非标
			String[][] nsCommonMemocontents = ReportContent.getReportContentByLoad(REPORT_NS_COMMON_MEMO);
			nsCommonMemocontents = SectionContentConvertUtils.getNsCommonF(nsCommonMemocontents, allMap, sectionType);
			if (null != nsCommonMemocontents) {
				nsCommonMemocontents = ValueFormatUtil.transReport(nsCommonMemocontents, project, unit, allMap, language,
						unitType);
				nsCommonMemocontents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(), language) + nsCommonMemocontents[0][0];//替换段名称
				createTable(doc, ParagraphAlignment.LEFT, true, false, nsCommonMemocontents.length, nsCommonMemocontents[0].length, nsCommonMemocontents);
				try {
					double price = NumberUtil.convertStringToDouble(nsCommonMemocontents[0][3]);
					totalPrice[0] = totalPrice[0] + price;
				} catch (Exception e) {

				}
			}
		}
	}

	/**
	 * 添加段key信息到contents
	 * @param nsContents
	 */
	public static void changeNsContents2SectionType(String[][] nsContents,SectionTypeEnum sectionType){
		for (int i = 0; i < nsContents.length; i++) {
			String[] nsContent = nsContents[i];
			for (int i1 = 0; i1 < nsContent.length; i1++) {
				String nsKey = nsContent[i1];
				nsContent[i1] = SectionMetaUtils.getNSSectionKey(sectionType.getId(), nsKey.replace("ns.",""));
			}
		}
	}
	/**
	 * 湿膜加湿段非标数据加载
	 * @param doc
	 * @param totalPrice
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param sectionType
	 * @throws Exception
	 */
    private static void addWetfilmhumidifierNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                               LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {


		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){

			createNsCommonTable(doc,project, unit, allMap, language,unitType,sectionType);
			String nsChangeSupplierKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSCHANGESUPPLIER);
			String nsChangeSupplierEnable = String.valueOf(allMap.get(nsChangeSupplierKey));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsChangeSupplierEnable)) {
				String[][] nsWetcontents = ReportContent.getReportContent(REPORT_NS_WETFILMHUMIDIFIER);
				nsWetcontents = ValueFormatUtil.transReport(nsWetcontents, project, unit, allMap, language,
						unitType);
				createTable(doc,ParagraphAlignment.LEFT, false, false, nsWetcontents.length, nsWetcontents[0].length, nsWetcontents);
				if(!UtilityConstant.SYS_BLANK.equals(nsWetcontents[0][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsWetcontents[0][7])) {
					double price1 = NumberUtil.convertStringToDouble(nsWetcontents[0][7]);
						totalPrice[0] = totalPrice[0] + price1;
				}
			}


			String[][] nsWetcontents_memo = ReportContent.getReportContent(REPORT_NS_WETFILMHUMIDIFIER_MEMO);
			nsWetcontents_memo = ValueFormatUtil.transReport(nsWetcontents_memo, project, unit, allMap, language,
					unitType);
			createTableNS(doc,ParagraphAlignment.LEFT, false, false, nsWetcontents_memo.length, nsWetcontents_memo[0].length, nsWetcontents_memo);
			try{
				double price2 = NumberUtil.convertStringToDouble(nsWetcontents_memo[0][3]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}
		}
	}

	/**
	 * 高压喷雾加湿段非标数据加载
	 * @param doc
	 * @param totalPrice
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param sectionType
	 * @throws Exception
	 */
    private static void addSprayHumidifierNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                             LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {

		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			createNsCommonTable(doc,project, unit, allMap, language,unitType,sectionType);
			String nsChangeSupplierKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSCHANGESUPPLIER);
			String nsChangeSupplierEnable = String.valueOf(allMap.get(nsChangeSupplierKey));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsChangeSupplierEnable)) {
				String[][] nsSprcontents = ReportContent.getReportContent(REPORT_NS_SPRAYHUMIDIFIER);
				nsSprcontents = ValueFormatUtil.transReport(nsSprcontents, project, unit, allMap, language,
						unitType);
				createTable(doc,ParagraphAlignment.LEFT, false, false, nsSprcontents.length, nsSprcontents[0].length, nsSprcontents);
				if(!UtilityConstant.SYS_BLANK.equals(nsSprcontents[0][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsSprcontents[0][7])) {
					double price1 = NumberUtil.convertStringToDouble(nsSprcontents[0][7]);
						totalPrice[0] = totalPrice[0] + price1;
				}
			}

			String[][] nsSprcontents_memo = ReportContent.getReportContent(REPORT_NS_SPRAYHUMIDIFIER_MEMO);
			nsSprcontents_memo = ValueFormatUtil.transReport(nsSprcontents_memo, project, unit, allMap, language,
					unitType);
			createTableNS(doc,ParagraphAlignment.LEFT, false, false, nsSprcontents_memo.length, nsSprcontents_memo[0].length, nsSprcontents_memo);
			try{
				double price2 = NumberUtil.convertStringToDouble(nsSprcontents_memo[0][3]);
				totalPrice[0] = totalPrice[0] + price2;

			}catch (Exception e){

			}
		}
	}

	/**
	 * 干蒸加湿段非标数据加载
	 * @param doc
	 * @param totalPrice
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param sectionType
	 * @throws Exception
	 */
    private static void addSteamHumidifierNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                             LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {

		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			createNsCommonTable(doc,project, unit, allMap, language,unitType,sectionType);
			String nsChangeSupplierKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSCHANGESUPPLIER);
			String nsChangeSupplierEnable = String.valueOf(allMap.get(nsChangeSupplierKey));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsChangeSupplierEnable)) {
				String[][] nsStecontents = ReportContent.getReportContent(REPORT_NS_STEAMHUMIDIFIER);
				nsStecontents = ValueFormatUtil.transReport(nsStecontents, project, unit, allMap, language,
						unitType);
				createTable(doc,ParagraphAlignment.LEFT, false, false, nsStecontents.length, nsStecontents[0].length, nsStecontents);
				if(!UtilityConstant.SYS_BLANK.equals(nsStecontents[1][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsStecontents[1][7])) {
					double price1 = NumberUtil.convertStringToDouble(nsStecontents[1][7]);
					totalPrice[0] = totalPrice[0] + price1;
				}
			}

			String[][] nsStecontents_memo = ReportContent.getReportContent(REPORT_NS_STEAMHUMIDIFIER_MEMO);
			nsStecontents_memo = ValueFormatUtil.transReport(nsStecontents_memo, project, unit, allMap, language,
					unitType);
			createTableNS(doc,ParagraphAlignment.LEFT, false, false, nsStecontents_memo.length, nsStecontents_memo[0].length, nsStecontents_memo);
			try {
				double price2 = NumberUtil.convertStringToDouble(nsStecontents_memo[0][3]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}
		}
	}

    private static void addCoolingCoilNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                         LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {


		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			createNsCommonTable(doc,project, unit, allMap, language,unitType,sectionType);
			String nsChangeEliminatorKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSCHANGEELIMINATOR);
			String nsChangeEliminatorEnable = String.valueOf(allMap.get(nsChangeEliminatorKey));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsChangeEliminatorEnable)) {
				/*String[][] nsCoolingCoilcontents = ReportContent.getReportContent(REPORT_NS_COOLINGCOIL);
				nsCoolingCoilcontents = ValueFormatUtil.transReport(nsCoolingCoilcontents, project, unit, allMap, language,
						unitType);
				createTable(doc,ParagraphAlignment.LEFT, false, false, nsCoolingCoilcontents.length, nsCoolingCoilcontents[0].length, nsCoolingCoilcontents);
				if(!UtilityConstant.SYS_BLANK.equals(nsCoolingCoilcontents[1][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsCoolingCoilcontents[1][7])) {
					double price1 = NumberUtil.convertStringToDouble(nsCoolingCoilcontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsCoolingCoilcontents[1][1])) {//变更材质选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}*/
			}

			String[][] nsCoolingCoilcontents_memo = ReportContent.getReportContent(REPORT_NS_COOLINGCOIL_MEMO);
			nsCoolingCoilcontents_memo = ValueFormatUtil.transReport(nsCoolingCoilcontents_memo, project, unit, allMap, language,
					unitType);
			createTableNS(doc,ParagraphAlignment.LEFT, false, false, nsCoolingCoilcontents_memo.length, nsCoolingCoilcontents_memo[0].length, nsCoolingCoilcontents_memo);
			
			try {
				double price2 = NumberUtil.convertStringToDouble(nsCoolingCoilcontents_memo[0][3]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}
		}
	}

    private static void addHeatingCoilNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                         LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {


		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			createNsCommonTable(doc,project, unit, allMap, language,unitType,sectionType);
			String nsChangeEliminatorKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSCHANGEELIMINATOR);
			String nsChangeEliminatorEnable = String.valueOf(allMap.get(nsChangeEliminatorKey));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsChangeEliminatorEnable)) {
				/*String[][] nsHeatingCoilcontents = ReportContent.getReportContent(REPORT_NS_HEATINGCOIL);
				nsHeatingCoilcontents = ValueFormatUtil.transReport(nsHeatingCoilcontents, project, unit, allMap, language,
						unitType);
				createTable(doc,ParagraphAlignment.LEFT, false, false, nsHeatingCoilcontents.length, nsHeatingCoilcontents[0].length, nsHeatingCoilcontents);
				if(!UtilityConstant.SYS_BLANK.equals(nsHeatingCoilcontents[1][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsHeatingCoilcontents[1][7])) {
					double price1 = NumberUtil.convertStringToDouble(nsHeatingCoilcontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsHeatingCoilcontents[1][1])) {//变更材质选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}
				}*/
			}

			String[][] nsHeatingCoilcontents_memo = ReportContent.getReportContent(REPORT_NS_HEATINGCOIL_MEMO);
			nsHeatingCoilcontents_memo = ValueFormatUtil.transReport(nsHeatingCoilcontents_memo, project, unit, allMap, language,
					unitType);
			createTableNS(doc,ParagraphAlignment.LEFT, false, false, nsHeatingCoilcontents_memo.length, nsHeatingCoilcontents_memo[0].length, nsHeatingCoilcontents_memo);

			try {

				double price2 = NumberUtil.convertStringToDouble(nsHeatingCoilcontents_memo[0][3]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}
		}
	}

	/**
	 * 风机段非标数据加载
	 * @param doc
	 * @param totalPrice
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param sectionType
	 * @throws Exception
	 */
    private static void addFanNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                 LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {
		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			try {
				//contents
				createNsCommonTable(doc,project, unit, allMap, language,unitType,sectionType);
				String[][] nsFancontents = ReportContent.getReportContent(REPORT_NS_FAN);
				nsFancontents = ValueFormatUtil.transReport(nsFancontents, project, unit, allMap, language,unitType);
				List<Integer> removeIndex = new ArrayList<Integer>();
				/*if(!UtilityConstant.SYS_BLANK.equals(nsFancontents[1][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsFancontents[1][7])) {
					double price1 = NumberUtil.convertStringToDouble(nsFancontents[1][7]);
					if (UtilityConstant.SYS_ASSERT_TRUE.equals(nsFancontents[1][1])) {//变更供应商选中计算价格
						totalPrice[0] = totalPrice[0] + price1;
					}else{
						removeIndex.add(1);
					}
				}*/

				//变更变更电机功率选中计算价格
				String nsChangeMotorKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSCHANGEMOTOR);
				String nsChangeMotorEnable = String.valueOf(allMap.get(nsChangeMotorKey));
				if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsChangeMotorEnable)) {
					if(!UtilityConstant.SYS_BLANK.equals(nsFancontents[0][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsFancontents[0][7])) {
						double price1 = NumberUtil.convertStringToDouble(nsFancontents[0][7]);
							totalPrice[0] = totalPrice[0] + price1;
					}
				}else{
					removeIndex.add(0);
				}

				//JD弹簧改HD弹簧选中计算价格
				String nsSpringtransformKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSSPRINGTRANSFORM);
				String nsSpringtransformEnable = String.valueOf(allMap.get(nsSpringtransformKey));
				if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsSpringtransformEnable)) {
					if(!UtilityConstant.SYS_BLANK.equals(nsFancontents[1][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsFancontents[1][7])) {
						double price2 = NumberUtil.convertStringToDouble(nsFancontents[1][7]);
							totalPrice[0] = totalPrice[0] + price2;
					}
				}else{
					removeIndex.add(1);
				}

				//没勾选的选项不打印(变更电机功率/JD弹簧改HD弹簧)
				List<List<String>> nsFanContentsList = new ArrayList<>();
				for (int i = 0; i < nsFancontents.length; i++) {
					if(!removeIndex.contains(i)){
						List<String> tempList = new ArrayList<>();
						for (String s : nsFancontents[i]) {
							tempList.add(s);
						}
						nsFanContentsList.add(tempList);
					}
				}
				if(nsFanContentsList.size()>0 && nsFanContentsList.get(0).size()>0){
					String[][] fianlNsContents =  new String[nsFanContentsList.size()][nsFanContentsList.get(0).size()];

					for (int i = 0; i < nsFanContentsList.size(); i++) {
						for (int j = 0; j < nsFanContentsList.get(i).size(); j++) {
							fianlNsContents[i][j] = nsFanContentsList.get(i).get(j);
						}
					}
					createTable(doc,ParagraphAlignment.LEFT, false, false, fianlNsContents.length, fianlNsContents[0].length, fianlNsContents);
				}

				//memo
				String[][] nsFancontents_memo = ReportContent.getReportContent(REPORT_NS_FAN_MEMO);
				nsFancontents_memo = ValueFormatUtil.transReport(nsFancontents_memo, project, unit, allMap, language,unitType);
				createTableNS(doc,ParagraphAlignment.LEFT, false, false, nsFancontents_memo.length, nsFancontents_memo[0].length, nsFancontents_memo);
				double price = NumberUtil.convertStringToDouble(nsFancontents_memo[0][3]);
				totalPrice[0] = totalPrice[0] + price;
			}catch (Exception e){
				e.printStackTrace();
			}

		}
	}
    
    /**
     * 单层过滤段非标数据加载
     * @param doc
     * @param totalPrice
     * @param allMap
     * @param language
     * @param unitType
     * @param project
     * @param unit
     * @param sectionType
     * @throws Exception
     */
    private static void addSingleNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                    LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {

		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			createNsCommonTable(doc,project, unit, allMap, language,unitType,sectionType);
			String nsPressureGuageKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSPRESSUREGUAGE);
			String nsPressureGuageEnable = String.valueOf(allMap.get(nsPressureGuageKey));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsPressureGuageEnable)) {
				String[][] nsFiltercontents = ReportContent.getReportContent(REPORT_NS_FILTER);
				nsFiltercontents = ValueFormatUtil.transReport(nsFiltercontents, project, unit, allMap, language,
						unitType);
				createTable(doc,ParagraphAlignment.LEFT, false, false, nsFiltercontents.length, nsFiltercontents[0].length, nsFiltercontents);

				if(!UtilityConstant.SYS_BLANK.equals(nsFiltercontents[0][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsFiltercontents[0][7])) {
					double price1 = NumberUtil.convertStringToDouble(nsFiltercontents[0][7]);
						totalPrice[0] = totalPrice[0] + price1;
				}
			}

			String[][] nsFiltercontents_memo = ReportContent.getReportContent(REPORT_NS_FILTER_MEMO);
			nsFiltercontents_memo = ValueFormatUtil.transReport(nsFiltercontents_memo, project, unit, allMap, language,
					unitType);
			createTableNS(doc,ParagraphAlignment.LEFT, false, false, nsFiltercontents_memo.length, nsFiltercontents_memo[0].length, nsFiltercontents_memo);
			try {
				double price2 = NumberUtil.convertStringToDouble(nsFiltercontents_memo[0][3]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}

		}
	}

	/**
	 * 添加通用非标清单每个段名称以及顶部线。
	 * @param doc
	 * @param sectionType
	 * @param language
	 * @throws Exception
	 */
	private static void createNsCommonTable(XWPFDocument doc, Project project, Unit unit, Map<String, String> allMap,
                                            LanguageEnum language, UnitSystemEnum unitType, SectionTypeEnum sectionType) throws Exception {
		String[][] nsCommonContents = ReportContent.getReportContent(REPORT_NS_COMMON);
		nsCommonContents = ValueFormatUtil.transReport(nsCommonContents, project, unit, allMap, language,
				unitType);
		nsCommonContents[0][0] = getIntlString(SectionTypeEnum.getSectionTypeFromId(sectionType.getId()).getCnName(),language) + nsCommonContents[0][0];//替换段名称
		createTable(doc,ParagraphAlignment.LEFT, true, false, nsCommonContents.length, nsCommonContents[0].length, nsCommonContents);
	}

	/**
     * 空段非标数据加载
     * @param doc
     * @param totalPrice
     * @param allMap
     * @param language
     * @param unitType
     * @param project
     * @param unit
     * @param sectionType
     * @throws Exception
     */
    private static void addAccessNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                    LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {


		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			createNsCommonTable(doc,project, unit, allMap, language,unitType,sectionType);
			String nsDrainpanKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSDRAINPAN);
			String nsDrainpanEnable = String.valueOf(allMap.get(nsDrainpanKey));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsDrainpanEnable)) {
				String[][] nsHepacontents = ReportContent.getReportContent(REPORT_NS_ACCESS);
				nsHepacontents = ValueFormatUtil.transReport(nsHepacontents, project, unit, allMap, language,
						unitType);
				createTable(doc,ParagraphAlignment.LEFT, false, false, nsHepacontents.length, nsHepacontents[0].length, nsHepacontents);
				if(!UtilityConstant.SYS_BLANK.equals(nsHepacontents[0][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsHepacontents[0][7])) {
					double price1 = NumberUtil.convertStringToDouble(nsHepacontents[0][7]);
						totalPrice[0] = totalPrice[0] + price1;
				}
			}

			String[][] nsHepacontents_memo = ReportContent.getReportContent(REPORT_NS_ACCESS_MEMO);
			nsHepacontents_memo = ValueFormatUtil.transReport(nsHepacontents_memo, project, unit, allMap, language,
					unitType);
			createTableNS(doc,ParagraphAlignment.LEFT, false, false, nsHepacontents_memo.length, nsHepacontents_memo[0].length, nsHepacontents_memo);
			try {
				double price2 = NumberUtil.convertStringToDouble(nsHepacontents_memo[0][3]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}

		}
	}
    
    /**
     * 高效过滤段非标数据加载
     * @param doc
     * @param totalPrice
     * @param allMap
     * @param language
     * @param unitType
     * @param project
     * @param unit
     * @param sectionType
     * @throws Exception
     */
    private static void addHepafilterNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                        LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {


		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			createNsCommonTable(doc,project, unit, allMap, language,unitType,sectionType);
			String nsPressureGuageKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSPRESSUREGUAGE);
			String nsPressureGuageEnable = String.valueOf(allMap.get(nsPressureGuageKey));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsPressureGuageEnable)) {
				String[][] nsHepacontents = ReportContent.getReportContent(REPORT_NS_HEPAFILTER);
				nsHepacontents = ValueFormatUtil.transReport(nsHepacontents, project, unit, allMap, language,
						unitType);
				createTable(doc,ParagraphAlignment.LEFT, false, false, nsHepacontents.length, nsHepacontents[0].length, nsHepacontents);
				if(!UtilityConstant.SYS_BLANK.equals(nsHepacontents[0][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsHepacontents[0][7])) {
					double price1 = NumberUtil.convertStringToDouble(nsHepacontents[0][7]);
						totalPrice[0] = totalPrice[0] + price1;
				}
			}

			String[][] nsHepacontents_memo = ReportContent.getReportContent(REPORT_NS_HEPAFILTER_MEMO);
			nsHepacontents_memo = ValueFormatUtil.transReport(nsHepacontents_memo, project, unit, allMap, language,
					unitType);
			createTableNS(doc,ParagraphAlignment.LEFT, false, false, nsHepacontents_memo.length, nsHepacontents_memo[0].length, nsHepacontents_memo);
			try {

				double price2 = NumberUtil.convertStringToDouble(nsHepacontents_memo[0][3]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}

		}
	}
    
    /**
     * 综合过滤段非标数据加载
     * @param doc
     * @param totalPrice
     * @param allMap
     * @param language
     * @param unitType
     * @param project
     * @param unit
     * @param sectionType
     * @throws Exception
     */
    private static void addCompositeNS(XWPFDocument doc, double[] totalPrice, Map<String, String> allMap,
                                       LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, SectionTypeEnum sectionType)
            throws Exception {

		String nsEnableKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NS_ENABLE);
		String enable = String.valueOf(allMap.get(nsEnableKey));
		if(UtilityConstant.SYS_ASSERT_TRUE.equals(enable)){
			createNsCommonTable(doc,project, unit, allMap, language,unitType,sectionType);
			String nsPressureGuageKey = SectionMetaUtils.getNSSectionKey(sectionType.getId(), SectionMetaUtils.MetaKey.KEY_NSPRESSUREGUAGE);
			String nsPressureGuageEnable = String.valueOf(allMap.get(nsPressureGuageKey));
			if(UtilityConstant.SYS_ASSERT_TRUE.equals(nsPressureGuageEnable)) {
				String[][] nsCompositecontents = ReportContent.getReportContent(REPORT_NS_COMPOSITE);
				nsCompositecontents = ValueFormatUtil.transReport(nsCompositecontents, project, unit, allMap, language,
						unitType);
				createTable(doc,ParagraphAlignment.LEFT, false, false, nsCompositecontents.length, nsCompositecontents[0].length, nsCompositecontents);
				if(!UtilityConstant.SYS_BLANK.equals(nsCompositecontents[0][7]) && !UtilityConstant.SYS_NOT_NUM.equals(nsCompositecontents[0][7])) {
					double price1 = NumberUtil.convertStringToDouble(nsCompositecontents[0][7]);
						totalPrice[0] = totalPrice[0] + price1;
				}
			}
			String[][] nsCompositecontents_memo = ReportContent.getReportContent(REPORT_NS_COMPOSITE_MEMO);
			nsCompositecontents_memo = ValueFormatUtil.transReport(nsCompositecontents_memo, project, unit, allMap, language,
					unitType);
			createTableNS(doc,ParagraphAlignment.LEFT, false, false, nsCompositecontents_memo.length, nsCompositecontents_memo[0].length, nsCompositecontents_memo);
			try {
				double price2 = NumberUtil.convertStringToDouble(nsCompositecontents_memo[0][3]);
				totalPrice[0] = totalPrice[0] + price2;
			}catch (Exception e){

			}

		}
	}
    
	/**
	 * 创建表格
	 * @param doc
	 * @param pa 水平位置
	 * @param hasTopBorder 顶部边框是否存在
	 * @param bothBold 所有列是否都加粗
	 * @param rows
	 * @param cols
	 * @param contents
	 * @throws Exception
	 */
	public static void createTable(XWPFDocument doc, ParagraphAlignment pa, boolean hasTopBorder, boolean bothBold, int rows, int cols, String[][] contents) throws Exception {
		XWPFTable table = doc.createTable(rows, cols);

		setTableProp(hasTopBorder,table);
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			//设置行的高度
//			theRow.setHeight(180);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
				int cellWidth = getCellWidth(cols,j);
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));

				boolean isBold = false;

				if((j+1)%2 != 0) {
					isBold = true;
					setCellText(cell, getFontFamily(), isBold, contents[i][j], pa);
				}
				if((j+1)%2 == 0) {
					if(bothBold){
						isBold = true;
					}
					setCellText(cell, getFontFamily(),isBold,contents[i][j],pa);
				}
			}
		}
		WordTableGen.addBr(doc);
	}
	
	public static void createTableNS(XWPFDocument doc, ParagraphAlignment pa, boolean hasTopBorder, boolean bothBold, int rows, int cols, String[][] contents) throws Exception {
		XWPFTable table = doc.createTable(rows, cols);

		setTableProp(hasTopBorder,table);
		int[] width = {500,6200,800,500};
		for (int i = 0; i < rows; i++) {
			XWPFTableRow theRow = table.getRow(i);
			//设置行的高度
//			theRow.setHeight(180);
			for (int j = 0; j < cols; j++) {
				XWPFTableCell cell = theRow.getCell(j);
				CTTcPr cellPr = cell.getCTTc().addNewTcPr();
				//设置宽度
//				int cellWidth = getCellWidth(cols,j);
				int cellWidth = width[j];
				cellPr.addNewTcW().setW(BigInteger.valueOf(cellWidth));

				boolean isBold = false;

				if((j+1)%2 != 0) {
					isBold = true;
					setCellText(cell, getFontFamily(), isBold, contents[i][j], pa);
				}
				if((j+1)%2 == 0) {
					if(bothBold){
						isBold = true;
					}
					setCellText(cell, getFontFamily(),isBold,contents[i][j],pa);
				}
			}
		}
		WordTableGen.addBr(doc);
	}
	
	/**
	 * 获取列宽
	 * @param cols
	 * @param j
	 * @return
	 */
	private static int getCellWidth(int cols, int j) {
		int cellWidth = 0;
		if(8 == cols) {
			cellWidth = 1000;
			if (j == 0) {
				cellWidth = 1600;
			} else if (j == 7) {
				cellWidth = 400;
			}
		}else{
			cellWidth = 8000/cols;
		}
		return cellWidth;
	}

	/**
	 * 设置单元格内容、样式
	 * @param cell
	 * @param fontFamily
	 * @param isBold
	 * @param text
	 * @param pa
	 */
	private static void setCellText(XWPFTableCell cell ,String fontFamily,boolean isBold,String text,ParagraphAlignment pa){
		XWPFParagraph p1 = cell.getParagraphs().get(0);
		p1.setAlignment(pa);
//		p1.setVerticalAlignment(TextAlignment.TOP);
		XWPFRun r1 = p1.createRun();
		r1.setBold(isBold);
		r1.setText(reCheckText(text));//内容
		r1.setFontFamily(fontFamily);
		r1.setFontSize(8);
	}
	private static String reCheckText(String checkResult){
		String result = checkResult.trim().toLowerCase().toLowerCase();

		if(UtilityConstant.SYS_ASSERT_TRUE.equals(result)){
			return "是";
		}
		if(UtilityConstant.SYS_ASSERT_FALSE.equals(result)){
			return "否";
		}
		return checkResult;
	}
	/**
	 * 设置表格属性
	 * @param hasTopBorder
	 * @param table
	 */
	private static void setTableProp(boolean hasTopBorder, XWPFTable table) {
		CTTblBorders borders = table.getCTTbl().getTblPr().addNewTblBorders();
		CTBorder hBorder = borders.addNewInsideH();
		hBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		hBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		hBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder vBorder = borders.addNewInsideV();
		vBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		vBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		vBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder lBorder = borders.addNewLeft();
		lBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		lBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		lBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder rBorder = borders.addNewRight();
		rBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		rBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		rBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));

		CTBorder tBorder = borders.addNewTop();
		if(hasTopBorder) {
			tBorder.setVal(STBorder.Enum.forString("single"));
			tBorder.setSz(new BigInteger("12"));
			tBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
			tBorder.setColor("000000");
		}else{
			tBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
			tBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
			tBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		}
		CTBorder bBorder = borders.addNewBottom();
		bBorder.setVal(STBorder.Enum.forString(UtilityConstant.SYS_ASSERT_NONE));
		bBorder.setSz(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));
		bBorder.setSpace(new BigInteger(UtilityConstant.SYS_STRING_NUMBER_0));


		CTTbl ttbl = table.getCTTbl();
		CTTblPr tblPr = ttbl.getTblPr() == null ? ttbl.addNewTblPr() : ttbl
				.getTblPr();
		CTTblWidth tblWidth = tblPr.isSetTblW() ? tblPr.getTblW() : tblPr
				.addNewTblW();
		CTJc cTJc = tblPr.addNewJc();
		cTJc.setVal(STJc.Enum.forString("center"));
		tblWidth.setW(new BigInteger("8000"));
		tblWidth.setType(STTblWidth.DXA);
	}

	/**
	 * 替换段落里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
	public static void replaceInPara(XWPFDocument doc, Map<String, Object> params) {
		Iterator<XWPFParagraph> iterator = doc.getParagraphsIterator();
		XWPFParagraph para;
		while (iterator.hasNext()) {
			para = iterator.next();
			replaceInPara(para, params);
		}
	}

	/**
	 * 替换段落里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
    public static void replaceInHeaderPara(XWPFHeader header, Map<String, Object> params) {
        List<XWPFParagraph> headParas = header.getParagraphs();
        for (XWPFParagraph headPara : headParas) {
            replaceInPara(headPara, params);
        }
    }

	/**
	 * 替换段落里面的变量
	 * 
	 * @param para
	 *            要替换的段落
	 * @param params
	 *            参数
	 */
	public static void replaceInPara(XWPFParagraph para, Map<String, Object> params) {
		List<XWPFRun> runs = para.getRuns();
		for (int i = 0; i < runs.size(); i++) {
			XWPFRun run = runs.get(i);
			String runText = run.toString().trim();
			for (Entry<String, Object> e : params.entrySet()) {
				if (runText.contains(e.getKey())) {
					if(runText.contains(SYS_PATH_EUROLOGOPATH)){
						para.removeRun(i);
						XWPFRun r1 = para.createRun();
						try {
							//		r1.addCarriageReturn();
							//		InputStream is = cell.getClass().getResourceAsStream(path);
							File file = new File(String.valueOf(e.getValue()));
							//			InputStream input = new FileInputStream(file);
							//			byte[] byt = new byte[input.available()];
							//			input.read(byt);
							//			InputStream is = new ByteArrayInputStream(byt);
							InputStream is = new FileInputStream(file);
							r1.addPicture(is, XWPFDocument.PICTURE_TYPE_PNG, SYS_PATH_EUROLOGOPATH,
									Units.toEMU((int) (PageSize.A4.getWidth() * 0.20)),
									Units.toEMU((int) (PageSize.A4.getWidth() * 0.07)));
						} catch (InvalidFormatException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}else{
						/*直接调用XWPFRun的setText()方法设置文本时，在底层会重新创建一个XWPFRun，把文本附加在当前文本后面，
						所以我们不能直接设值，需要先删除当前run,然后再自己手动插入一个新的run。*/
						para.removeRun(i);
						XWPFRun paragraphRun = para.createRun();
						paragraphRun.setText(runText.replaceAll(e.getKey(),
								EmptyUtil.isEmpty(e.getValue()) ? StringUtils.EMPTY : String.valueOf(e.getValue())));
					}
                }
			}

		}

	}

	/**
	 * 替换表格里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
    public static void replaceInHeaderTable(XWPFHeader header, Map<String, Object> params) {
        List<XWPFTable> headTables = header.getTables();
		List<XWPFTableRow> rows;
		List<XWPFTableCell> cells;
		List<XWPFParagraph> paras;
		for (XWPFTable table : headTables) {
			rows = table.getRows();
			for (XWPFTableRow row : rows) {
				cells = row.getTableCells();
				for (XWPFTableCell cell : cells) {
					paras = cell.getParagraphs();
					for (XWPFParagraph para : paras) {
						replaceInPara(para, params);
					}
				}
			}
		}
	}

	/**
	 * 替换表格里面的变量
	 * 
	 * @param doc
	 *            要替换的文档
	 * @param params
	 *            参数
	 */
	public static void replaceInTable(XWPFDocument doc, Map<String, Object> params) {
		Iterator<XWPFTable> iterator = doc.getTablesIterator();
		XWPFTable table;
		List<XWPFTableRow> rows;
		List<XWPFTableCell> cells;
		List<XWPFParagraph> paras;
		while (iterator.hasNext()) {
			table = iterator.next();
			rows = table.getRows();
			for (XWPFTableRow row : rows) {
				cells = row.getTableCells();
				for (XWPFTableCell cell : cells) {
					paras = cell.getParagraphs();
					for (XWPFParagraph para : paras) {
						replaceInPara(para, params);
					}
				}
			}
		}
	}

	/**
	 * 关闭输入流
	 * 
	 * @param is
	 */
	public static void close(InputStream is) {
		if (is != null) {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 关闭输出流
	 * 
	 * @param os
	 */
	public static void close(OutputStream os) {
		if (os != null) {
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 添加工程格式报告段名称
	 * @param document
	 * @param partName
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected static void addPartName(XWPFDocument doc, String partName) throws DocumentException, IOException {
		XWPFParagraph p = doc.createParagraph();
		p.setAlignment(ParagraphAlignment.LEFT);
		p.setSpacingAfter(10);
		XWPFRun r = p.createRun();
		r.setFontFamily(getFontFamily());
		r.setBold(true);
		r.setFontSize(11);
		r.setText(partName);
	}
	
	/**
	 * 添加三视图
	 * @param doc
	 * @param param
	 * @param language
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImage3View(XWPFDocument doc, AhuParam param, LanguageEnum language)
			throws DocumentException, IOException {
		logger.info("开始生成三视图：unitid:"+param.getUnitid()+" unitName:"+param.getName());
		String fileName = UtilityConstant.SYS_BLANK+System.currentTimeMillis();
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName();
		}
		String destPath = String.format(
				SysConstants.CAD_DIR + File.separator + "%s" + File.separator + "%s" + File.separator + "%s.bmp", param.getPid(),
				param.getUnitid(), fileName);
		CadUtils.exportBmp(param, destPath);

		XWPFParagraph p = doc.createParagraph();
		XWPFRun r = p.createRun();
		r.setFontFamily(getFontFamily());
		r.setBold(true);
		r.setFontSize(11);
		r.setText(getIntlString(AHU_TRIPLE_VIEW_SEQUENCE_NUMBER) + param.getUnitNo());//添加标题
		r.addCarriageReturn();
		File file = new File(SysConstants.ASSERT_DIR + File.separator + destPath);
		InputStream is = new FileInputStream(file);
		try {
			r.addPicture(is, XWPFDocument.PICTURE_TYPE_BMP, UtilityConstant.SYS_BLANK, Units.toEMU((int) (PageSize.A4.getWidth() * 0.75)),
					Units.toEMU((int) (PageSize.A4.getHeight() * 0.45)));
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}
		WordTableGen.addBr(doc);
	}
	
	/**
	 * 添加焓湿图
	 * @param document
	 * @param param
	 * @param language
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void addImagePsychometric(XWPFDocument doc, AhuParam param, LanguageEnum language)
			throws DocumentException, IOException {
		logger.info("开始生成焓湿图：unitid:"+param.getUnitid()+" unitName:"+param.getName());
		Iterator<PartParam> it = param.getPartParams().iterator();

		List<PsyCalBean> datas = PsychometricDrawer.drawPsy(it);

		String path = PsychometricDrawer.genPsychometric(param.getPid(), param.getUnitid(), datas);

//		Image img = Image.getInstance(SysConstants.ASSERT_DIR + path);
//		img.scalePercent(16);
		XWPFParagraph p = doc.createParagraph();
		XWPFRun r = p.createRun();
		r.setFontFamily(getFontFamily());
		r.setBold(true);
		r.setFontSize(11);
		r.setText(getIntlString(AHU_PSY_CHART_SEQUENCE_NUMBER) + param.getUnitNo());//添加标题
		r.addCarriageReturn();
		File file = new File(SysConstants.ASSERT_DIR + path.substring(0,path.indexOf("?")));
		InputStream is = new FileInputStream(file);
		try {
			r.addPicture(is, XWPFDocument.PICTURE_TYPE_BMP, UtilityConstant.SYS_BLANK, Units.toEMU((int) (PageSize.A4.getWidth() * 0.70)),
					Units.toEMU((int) (PageSize.A4.getHeight() * 0.45)));
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}
		WordTableGen.addBr(doc);
	}
	
	/**
	 * 添加风机曲线图
	 * @param param
	 * @param doc
	 * @param allMap
	 * @param language
	 * @param unitType
	 * @param project
	 * @param unit
	 * @param fanCount
	 * @throws DocumentException
	 * @throws IOException
	 */
    public static void addImageFan(XWPFDocument doc, Map<String, Map<String, String>> allMap,
                                   LanguageEnum language, UnitSystemEnum unitType, Project project, Unit unit, PartParam partParam, int fanCount)
            throws DocumentException, IOException {
		
				Map<String, Object> params = partParam.getParams();
				String path = String.valueOf(params.get(UtilityConstant.METASEXON_FAN_CURVE));
				path = path.replace(UtilityConstant.SYS_FILES_DIR, UtilityConstant.SYS_BLANK);// 页面使用的为files 后端路径去掉files
				if (EmptyUtil.isNotEmpty(path)) {
					Map<String, String> noChangeMap = allMap.get(UnitConverter.genUnitKey(UtilityConstant.SYS_BLANK + partParam.getPosition(), partParam.getUnitid(),
							partParam.getKey()));
					Map<String, String> cloneMap = UnitUtil.clone(noChangeMap);
					Map<String, String> all = SectionDataConvertUtils.getFan(noChangeMap, cloneMap, language, null);
					String[][] contents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_FAN_CURVE),
							project, unit, all, language, unitType);
					Map<String, String> allMap1 = new HashMap<String, String>();
					for (String key : allMap.keySet()) {
						if(key.indexOf(UtilityConstant.METASEXON_FAN)>0) {
							allMap1 = (Map<String, String>)allMap.get(key);
						}
					}
					// 封装风机段特殊字段
//					contents = SectionContentConvertUtils.getFan4(contents, allMap1, language);
					String fanName = getIntlString(SUPPLY_FAN);// 默认送风机
					String airDirection = MapValueUtils.getStringValue(METASEXON_AIRDIRECTION, params);
					if (AirDirectionEnum.RETURNAIR.getCode().equals(airDirection)) {//回风机组
						fanName =getIntlString(RETURN_FAN);
					}

					String fanNameStr = fanCount>1?fanName:"";

                    try {
                        File file = new File(SysConstants.ASSERT_DIR + path);
                        InputStream is = new FileInputStream(file);
                        WordTableGen.genFanCurveTable(unit,doc, contents, is, true,fanNameStr);
                    } catch (InvalidFormatException e) {
                        e.printStackTrace();
                    }
                    WordTableGen.addBr(doc);
                }
			
	}
	
	/**
	 * 添加面板布置图
	 * @param doc
	 * @param unit
	 * @param partition
	 * @param language
	 * @throws DocumentException
	 * @throws IOException
	 */
	private static void addImagePanelScreenshots(XWPFDocument doc, Unit unit, Partition partition, LanguageEnum language) throws DocumentException, IOException {
		logger.info("开始添加面板切割布置图：unitid:"+unit.getUnitid()+" unitName:"+unit.getName());
		String fileName = UtilityConstant.SYS_BLANK+System.currentTimeMillis();
		if(!StringUtils.isEmpty(AHUContext.getUserName())){
			fileName = AHUContext.getUserName();
		}
		
		List<AhuPartition> partitions = JSONArray.parseArray(partition.getPartitionJson(), AhuPartition.class);
		for(int i=0;i<partitions.size();i++) {
			String destPath = MessageFormat.format(FileNamesLoadInSystem.PANEL_IMG_PATH, unit.getUnitid()) + File.separator + partition.getPartitionid() + (i+1) + ".png";
			XWPFParagraph p = doc.createParagraph();
			XWPFRun r = p.createRun();
			r.setFontFamily(getFontFamily());
			r.setBold(true);
			r.setFontSize(11);
			r.setText(getIntlString(AHU_PANEL_SCREENSHOTS_SEQUENCE_NUMBER) + unit.getUnitNo() + "-" + (i+1));//添加标题
			r.addCarriageReturn();
			File file = new File(destPath);
			InputStream is = new FileInputStream(file);
			try {
				r.addPicture(is, XWPFDocument.PICTURE_TYPE_BMP, UtilityConstant.SYS_BLANK, Units.toEMU(PageSize.A4.getWidth() * 0.9),
						Units.toEMU(PageSize.A4.getHeight() * 0.6));
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			r.addBreak(BreakType.PAGE);
		}
		WordTableGen.addBr(doc);
	}
	
	/**
	 * 机组铭牌数据
	 * @param doc
	 * @param arrayMap
	 * @param allMap
	 * @param project
	 * @param unitType
	 * @param unit
	 * @param partition
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
	private static void addUnitNameplateData(XWPFDocument doc, Map<String, Map<String, String>> allMap,
                                             Map<String, String> allMap2, Project project, UnitSystemEnum unitType, Unit unit, Partition partition,
                                             LanguageEnum language)	throws IOException, DocumentException {
		List<AhuPartition> ahuPartitionList = AhuPartitionUtils.parseAhuPartition(unit, partition);

		logger.info("开始添加机组铭牌数据：unitid:" + unit.getUnitid() + " unitName:" + unit.getName());
		
		XWPFParagraph p = doc.createParagraph();
		XWPFRun r = p.createRun();
		r.setFontFamily(getFontFamily());
		r.setBold(true);
		r.setFontSize(11);
		r.setText(getIntlString(UNIT_NAMEPLATE_DATA));//添加标题

		String[][] ahuContents = ValueFormatUtil.transReport(ReportContent.getReportContent(REPORT_TECH_UNITNAMEPLATEDATA), project, unit,
				allMap2, language, unitType);
		ahuContents = SectionContentConvertUtils.getUnitNamePlateData(ahuContents, allMap, allMap2, language);
		ahuContents = SectionContentConvertUtils.getUnitNamePlateDataColdQHeatQ(ahuContents,ahuPartitionList, allMap2);//铭牌冷热量

		WordTableGen.addBr(doc);
		WordTableGen.genTechUnitnameplatedata(ahuContents,doc,true);
		
        String[][] titleContents = ValueFormatUtil.transReport(
                ReportContent.getReportContent(REPORT_TECH_UNITNAMEPLATEDATA_SHAPE), project, unit, allMap2, language,
                unitType);
		String partitionJson = partition.getPartitionJson();
		if (EmptyUtil.isNotEmpty(partitionJson)) {
			String[][] partitionContents = new String[titleContents.length + ahuPartitionList.size()][6];
			partitionContents[0] = titleContents[0];
			partitionContents[1] = titleContents[1];
			int i = 2;
			for (AhuPartition ap : ahuPartitionList) {
				String[] temContents = null;
				temContents = SectionContentConvertUtils.getMPStrs(unit, ap,ahuPartitionList);
				partitionContents[i] = temContents;
				i++;
			}
			WordTableGen.genTechUnitnameplatedata2(partitionContents, doc, false);
		}
		WordTableGen.addBr(doc);
	}

	/**
	 * 段连接清单
	 * @param doc
	 * @param unit
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
	private static void addSectionConnectionList(String paneltype, XWPFDocument doc, Unit unit, LanguageEnum language, Map<String, String> ahuMap)
			throws IOException, DocumentException {
		logger.info("开始添加段连接清单：unitid:" + unit.getUnitid() + " unitName:" + unit.getName());
		String unitSeries = AhuUtil.getUnitSeries(unit.getSeries());
		int unitWidth = AhuUtil.getWidthOfAHU(unit.getSeries());
		int unitHeight = AhuUtil.getHeightOfAHU(unit.getSeries());
		String unitModel = unit.getSeries();

		//面板切割页面变形处理。
		if(EmptyUtil.isNotEmpty(unit.getPanelSeries()) && !unit.getSeries().equals(unit.getPanelSeries())) {
			unitWidth = AhuUtil.getWidthOfAHU(unit.getPanelSeries());
			unitHeight = AhuUtil.getHeightOfAHU(unit.getPanelSeries());
			unitModel = unit.getPanelSeries();
		}
		String inskinm = SectionContentConvertUtils.getConnectionList(ahuMap);
		String[][] sectionConnectionListC = ReportMetadata.getTechSpecSectionConnection(unitSeries, unitModel, unitWidth, unitHeight, paneltype,inskinm);

		XWPFParagraph p = doc.createParagraph();
		XWPFRun r = p.createRun();
		r.setFontFamily(getFontFamily());
		r.setBold(true);
		r.setFontSize(11);
		
		String connectionStr=getIntlString(SECTION_CONNECTION_LIST);
		if(UtilityConstant.JSON_UNIT_PANELTYPE_FOREPART.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONFOREPART);
		}else if(UtilityConstant.JSON_UNIT_PANELTYPE_POSITIVE.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONPOSITIVE);
			
		}else if(UtilityConstant.JSON_UNIT_PANELTYPE_VERTICAL.equals(paneltype)) {
			connectionStr=getIntlString(CONNECTION_CONVERTICAL);
			
		}
		r.setText(getIntlString(UNIT_MODEL_COLON) + unit.getSeries() + " " + connectionStr);// 添加标题

		WordTableGen.genSectionConnectionList(sectionConnectionListC, doc, true, paneltype);
		WordTableGen.addBr(doc);
	}	
	
	/**
	 * 产品装箱单
	 * @param doc
	 * @param arrayMap
	 * @param allMap
	 * @param project
	 * @param unitType
	 * @param unit
	 * @param partition
	 * @param language
	 * @throws IOException
	 * @throws DocumentException
	 */
    private static void addProductPackingList(XWPFDocument doc, Map<String, Map<String, String>> ahuMap, Map<String, String> allMap, Project project,
                                              UnitSystemEnum unitType, Unit unit, Partition partition, LanguageEnum language, int connectionNum, List<PartPO> partList)
            throws IOException, DocumentException {
        logger.info("开始添加产品装箱单：unitid:" + unit.getUnitid() + " unitName:" + unit.getName());
        WordTableGen.addBr(doc);

		String[][] ahuContents = SectionContentConvertUtils.getProductPackingList(ahuMap, allMap, project, unitType, unit, language, connectionNum, partList);
        
        WordTableGen.genTechProductPackingListdata(ahuContents, doc, true);
        WordTableGen.addBr(doc);
    }
    

}

package com.carrier.ahu.calculator;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.util.section.HumidifierUtils;
import com.carrier.ahu.vo.SystemCalculateConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * 重量计算 特殊键值匹配处理工具<br>
 * 在代码重构以后，应该需要遗弃掉该类
 * 
 * @author Simon
 * @since 2018-03-17
 */
@Slf4j
public class CalWeightTool {
	public static String transKey(String prop,Map<String, Object> ahuValueMap) {
		/*外面板材料
			镀锌	1
			彩钢板／冷轧烘漆	2
			不锈钢板	3
			高耐久性聚酯面板	4
			*/
		if(UtilityConstant.METAHU_EXSKINM.equals(prop)){
			String exskinm = String.valueOf(ahuValueMap.get(UtilityConstant.METAHU_EXSKINM));
			if(UtilityConstant.JSON_AHU_EXSKINM_GI.equals(exskinm)){
				return UtilityConstant.SYS_STRING_NUMBER_1;
			}else if(UtilityConstant.JSON_AHU_EXSKINM_PREPAINTEDSTEEL.equals(exskinm) ||"paintedColdRolledSteel".equals(exskinm)){
				return UtilityConstant.SYS_STRING_NUMBER_2;
			}else if(UtilityConstant.JSON_AHU_EXSKINM_STAINLESS.equals(exskinm)){
				return UtilityConstant.SYS_STRING_NUMBER_3;
			}else if(UtilityConstant.JSON_AHU_EXSKINM_HDPPREPAINTEDSTEEL.equals(exskinm)){
				return UtilityConstant.SYS_STRING_NUMBER_4;
			}
		}
		/*外面板厚度
			0.5	:1
			0.8	:2
			1	:3
			1.2	:4
		*/
		if(UtilityConstant.METAHU_EXSKINW.equals(prop)){
			String exskinw = String.valueOf(ahuValueMap.get(UtilityConstant.METAHU_EXSKINW));
			if(UtilityConstant.JSON_AHU_SKINTHICKNESS_0_5MM.equals(exskinw)){
				return UtilityConstant.SYS_STRING_NUMBER_1;
			}else if(UtilityConstant.JSON_AHU_SKINTHICKNESS_0_8MM.equals(exskinw)){
				return UtilityConstant.SYS_STRING_NUMBER_2;
			}else if(UtilityConstant.JSON_AHU_SKINTHICKNESS_1_0MM.equals(exskinw)){
				return UtilityConstant.SYS_STRING_NUMBER_3;
			}else if(UtilityConstant.JSON_AHU_SKINTHICKNESS_1_2MM.equals(exskinw)){
				return UtilityConstant.SYS_STRING_NUMBER_4;
			}
		}
		/*内面板材料
			镀锌	1
			彩钢板／冷轧烘漆	2
			不锈钢板	3
			高耐久性聚酯面板	4
		*/
		if(UtilityConstant.METAHU_INSKINM.equals(prop)){
			String inskinm = String.valueOf(ahuValueMap.get(UtilityConstant.METAHU_INSKINM));
			if("GlSteel".equals(inskinm)){
				return UtilityConstant.SYS_STRING_NUMBER_1;
			}else if(UtilityConstant.JSON_AHU_EXSKINM_PREPAINTEDSTEEL.equals(inskinm) ||"paintedColdRolledSteel".equals(inskinm)){
				return UtilityConstant.SYS_STRING_NUMBER_2;
			}else if(UtilityConstant.JSON_AHU_EXSKINM_STAINLESS.equals(inskinm)){
				return UtilityConstant.SYS_STRING_NUMBER_3;
			}else if(UtilityConstant.JSON_AHU_EXSKINM_HDPPREPAINTEDSTEEL.equals(inskinm)){
				return UtilityConstant.SYS_STRING_NUMBER_4;
			}
		}
		/*内面板厚度
			0.5	:1
			0.8	:2
			1	:3
			1.2	:4
		*/
		if(UtilityConstant.METAHU_INSKINW.equals(prop)){
			String inskinw = String.valueOf(ahuValueMap.get(UtilityConstant.METAHU_INSKINW));
			if(UtilityConstant.JSON_AHU_SKINTHICKNESS_0_5MM.equals(inskinw)){
				return UtilityConstant.SYS_STRING_NUMBER_1;
			}else if(UtilityConstant.JSON_AHU_SKINTHICKNESS_0_8MM.equals(inskinw)){
				return UtilityConstant.SYS_STRING_NUMBER_2;
			}else if(UtilityConstant.JSON_AHU_SKINTHICKNESS_1_0MM.equals(inskinw)){
				return UtilityConstant.SYS_STRING_NUMBER_3;
			}else if(UtilityConstant.JSON_AHU_SKINTHICKNESS_1_2MM.equals(inskinw)){
				return UtilityConstant.SYS_STRING_NUMBER_4;
			}
		}
		return null;
	}
	public static String transValue(String oneProp, Map<String, Object> ahuValueMap, CalculatorSpec spec) {
		StringBuffer rKey = new StringBuffer();
		if (ahuValueMap.containsKey(oneProp)) {
			Object oneValue = ahuValueMap.get(oneProp);
			if (oneProp.equals(UtilityConstant.METASEXON_COMBINEDFILTER_RIMTHICKNESS)) {
				String rimThickness = oneValue.toString();
				if (spec.getName().contains("filter_plate.")) {
					rKey.append(rimThickness.substring(0, 2));
				}
				if (spec.getName().contains("filter_bag")) {
					rKey.append(rimThickness.substring(3, 5));
				}
			}else if (oneProp.equals(UtilityConstant.METASEXON_COMBINEDFILTER_SECTIONL) || oneProp.equals(UtilityConstant.METASEXON_FILTER_SECTIONL)) {
				if(null == oneValue || UtilityConstant.SYS_BLANK.equals(oneValue.toString())){
					rKey.append(UtilityConstant.SYS_BLANK);
				}else{
					int sectionL = Integer.parseInt(oneValue.toString());
					if (sectionL >= 4) {//综合老代码 和老程序 段长大于等于4 改为6计算if (SE_SECTIONL.TEXT='8') OR (SE_SECTIONL.TEXT='9') then   //modified 20170822
						rKey.append(6);
					}
					if(sectionL < 4){//综合老代码老程序，段长小于4，改为：3
						rKey.append(3);
					}
				}
			}  else if (oneProp.equals(UtilityConstant.METASEXON_DXCOIL_ROWS)
					|| oneProp.equals(UtilityConstant.METASEXON_HEATINGCOIL_ROWS)
					|| oneProp.equals(UtilityConstant.METASEXON_COOLINGCOIL_ROWS)) {
				rKey.append(oneValue).append("R");
			} else if (oneProp.equals(UtilityConstant.METASEXON_DXCOIL_FINDENSITY)
					|| oneProp.equals(UtilityConstant.METASEXON_HEATINGCOIL_FINDENSITY)
					|| oneProp.equals(UtilityConstant.METASEXON_COOLINGCOIL_FINDENSITY)) {
				rKey.append(oneValue).append("FPI");
			} else if (oneProp.equals(UtilityConstant.METASEXON_COOLINGCOIL_ELIMINATOR)
					|| oneProp.equals(UtilityConstant.METASEXON_DXCOIL_ELIMINATOR)) {
				String eliminator = oneValue.toString();
				if (eliminator.equals(SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALMESH)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_ALMESH);
				} else if (eliminator.equals(SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_PLASTIC)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_PLASTIC);
				} else if (eliminator.equals(SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_STAINLESS)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_STAINLESSSTEEL);
				} else if (eliminator.equals(SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_GISTEEL)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_GISTEEL);
				} else if (eliminator.equals(SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_ALUMINUM)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_ALUMINUM);
				} else if (eliminator.equals(SystemCalculateConstants.COOLINGCOIL_ELIMINATOR_NONE)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_NONE);
				} else {
					log.warn(String.format("Unknown attribute %s in spec %s", oneProp, spec.getName()));
				}
			} else if (oneProp.equals(UtilityConstant.METASEXON_COOLINGCOIL_BAFFLEMATERIAL)) {
				String eliminator = oneValue.toString();
				if (eliminator.equals(SystemCalculateConstants.COOLINGCOIL_BAFFLEMATERIAL_GISTEEL)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_GISTEEL);
				} else if (eliminator.equals(SystemCalculateConstants.COOLINGCOIL_BAFFLEMATERIAL_STAINLESSSTEEL)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_STAINLESSSTEEL);
				} else if (eliminator
						.equals(SystemCalculateConstants.COOLINGCOIL_BAFFLEMATERIAL_PAINTEDCOLDROLLEDSTEEL)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_PAINTEDCOLDROLLEDSTEEL);
				} else {
					log.warn(String.format("Unknown attribute %s in spec %s", oneProp, spec.getName()));
				}
			} else if (oneProp.equals(UtilityConstant.METASEXON_DXCOIL_BAFFLEMATERIAL)) {
				String eliminator = oneValue.toString();
				if (eliminator.equals(SystemCalculateConstants.DIRECTEXPENSIONCOIL_BAFFLEMATERIAL_GISTEEL)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_GISTEEL);
				} else if (eliminator
						.equals(SystemCalculateConstants.DIRECTEXPENSIONCOIL_BAFFLEMATERIAL_STAINLESSSTEEL)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_STAINLESSSTEEL);
				} else if (eliminator
						.equals(SystemCalculateConstants.DIRECTEXPENSIONCOIL_BAFFLEMATERIAL_PAINTEDCOLDROLLEDSTEEL)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_PAINTEDCOLDROLLEDSTEEL);
				} else {
					log.warn(String.format("Unknown attribute %s in spec %s", oneProp, spec.getName()));
				}
			} else if (oneProp.equals(UtilityConstant.METASEXON_FILTER_RIMTHICKNESS)) {
				String rimThickness = oneValue.toString();
				if (rimThickness.equals(SystemCalculateConstants.FILTER_RIMTHICKNESS_1)) { // sync with old system
				    rKey.append("25");
				} else if (rimThickness.equals(SystemCalculateConstants.FILTER_RIMTHICKNESS_2)) {
				    rKey.append("50");
				} else {
					log.warn(String.format("Unknown attribute %s in spec %s", oneProp, spec.getName()));
					rKey.append(rimThickness);
				}
			} else if (oneProp.equals(UtilityConstant.METASEXON_HEATINGCOIL_BAFFLEMATERIAL)) {
				String baffleMaterial = oneValue.toString();
				if (baffleMaterial.equals(SystemCalculateConstants.HEATINGCOIL_BAFFLEMATERIAL_GISTEEL)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_GISTEEL);
				} else if (baffleMaterial.equals(SystemCalculateConstants.HEATINGCOIL_BAFFLEMATERIAL_STAINLESSSTEEL)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_STAINLESSSTEEL);
				} else if (baffleMaterial
						.equals(SystemCalculateConstants.HEATINGCOIL_BAFFLEMATERIAL_PAINTEDCOLDROLLEDSTEEL)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_PAINTEDCOLDROLLEDSTEEL);
				} else {
					log.warn(String.format("Unknown attribute %s in spec %s", oneProp, spec.getName()));
				}
			} else if (oneProp.equals(UtilityConstant.METASEXON_ELECTRODEHUMIDIFIER_WHUMIDIFICATIONQ)) {
                double maxSeasonHumid = HumidifierUtils
                        .getMaxSeasonHumidification(SectionTypeEnum.TYPE_ELECTRODEHUMIDIFIER, ahuValueMap);
                rKey.append(HumidifierUtils.getHumidQForWeight(maxSeasonHumid));
			} else if (oneProp.equals(UtilityConstant.METASEXON_COMBINEDMIXINGCHAMBER_EXECUTOR1)) {
				String Executor1 = oneValue.toString();
				if (Executor1.equals(SystemCalculateConstants.COMBINEDMIXINGCHAMBER_EXECUTOR1_A)) {
					rKey.append("00000");
				} else if (Executor1.equals(SystemCalculateConstants.COMBINEDMIXINGCHAMBER_EXECUTOR1_C)) {
					rKey.append("220AI");
				} else if (Executor1.equals(SystemCalculateConstants.COMBINEDMIXINGCHAMBER_EXECUTOR1_D)) {
					rKey.append("024AI");
				} else if (Executor1.equals(SystemCalculateConstants.COMBINEDMIXINGCHAMBER_EXECUTOR1_B)) {
					rKey.append("024DI");
				} else {
					log.warn(String.format("Unknown attribute %s in spec %s", oneProp, spec.getName()));
				}
			} else if (oneProp.equals(UtilityConstant.METASEXON_SPRAYHUMIDIFIER_WHUMIDIFICATIONQ)) {
				if (null == oneValue || oneValue.toString().equals("0.0")) {
					log.warn(String.format("Illegal WHumidificationQ %s in spec %s", oneValue, spec.getName()));
					String sOneKey = UtilityConstant.METASEXON_SPRAYHUMIDIFIER_SHUMIDIFICATIONQ;
					if (ahuValueMap.containsKey(sOneKey)) {
						oneValue = ahuValueMap.get(sOneKey);
					}
					Double wHumidificationQ = Double.parseDouble(oneValue.toString());
					rKey.append(getHumidificationQ1(wHumidificationQ));
				} else {
					Double wHumidificationQ = Double.parseDouble(oneValue.toString());
					rKey.append(getHumidificationQ1(wHumidificationQ));
				}
			} else if (oneProp.equals(UtilityConstant.METASEXON_DISCHARGE_AINTERFACE)) {
				String AInterface = oneValue.toString();
				if (AInterface.equals(SystemCalculateConstants.DISCHARGE_AINTERFACE_MANUALDAMPER)) {
					rKey.append("MD");
				} else if (AInterface.equals(SystemCalculateConstants.DISCHARGE_AINTERFACE_ELECTRICDAMPER)) {
					rKey.append("ED");
				} else if (AInterface.equals(SystemCalculateConstants.DISCHARGE_AINTERFACE_FLANGE)) {
					rKey.append("FD");
				} else {
					log.warn(String.format("Unknown attribute %s in spec %s", oneProp, spec.getName()));
				}
			} else if (oneProp.equals(UtilityConstant.METASEXON_DISCHARGE_DAMPEREXECUTOR)) {
				String Executor1 = oneValue.toString();
				if (Executor1.equals(SystemCalculateConstants.DISCHARGE_DAMPEREXECUTOR_1)) {
					rKey.append("00000");
				} else if (Executor1.equals(SystemCalculateConstants.DISCHARGE_DAMPEREXECUTOR_3)) {
					rKey.append("220AI");
				} else if (Executor1.equals(SystemCalculateConstants.DISCHARGE_DAMPEREXECUTOR_4)) {
					rKey.append("024AI");
				} else if (Executor1.equals(SystemCalculateConstants.DISCHARGE_DAMPEREXECUTOR_2)) {
					rKey.append("024DI");
				} else {
					log.warn(String.format("Unknown attribute %s in spec %s", oneProp, spec.getName()));
				}
			} else if (oneProp.equals(UtilityConstant.METASEXON_DISCHARGE_DAMPERMETERIAL)) {
				String Executor1 = oneValue.toString();
				if (Executor1.equals(SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_GL)) {
					rKey.append(UtilityConstant.JSON_AHU_EXSKINM_GI);
				} else if (Executor1.equals(SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_ALUMINUM)) {
					rKey.append("AL");
				} else if (Executor1.equals(SystemCalculateConstants.DISCHARGE_DAMPERMETERIAL_NA)) {
					rKey.append(UtilityConstant.JSON_AHU_MATERIAL_NONE);
				} else {
					log.warn(String.format("Unknown attribute %s in spec %s", oneProp, spec.getName()));
				}
			} else if(oneProp.equals(UtilityConstant.METASEXON_FAN_STARTSTYLE)) {
                if (SystemCalculateConstants.FAN_STARTSTYLE_NO.equals(oneValue)) {
                    rKey.append(UtilityConstant.JSON_AHU_MATERIAL_NONE);
                } else if (SystemCalculateConstants.FAN_STARTSTYLE_DIRECT.equals(oneValue)) {
                    rKey.append("DS");
                } else if (SystemCalculateConstants.FAN_STARTSTYLE_STAR.equals(oneValue)) {
                    rKey.append("SDS");
                } else if (SystemCalculateConstants.FAN_STARTSTYLE_VFD.equals(oneValue)) {
                    rKey.append("VFD");
                }
			} else if(oneProp.equals(UtilityConstant.METASEXON_FAN_SUPPLIER)) {
                if (SystemCalculateConstants.FAN_SUPPLIER_ABB.equals(oneValue)) {
                    rKey.append("A");
                } else if (SystemCalculateConstants.FAN_SUPPLIER_DEFAULT.equals(oneValue)) {
                    rKey.append("B");
                } else if (SystemCalculateConstants.FAN_SUPPLIER_DONGYUAN.equals(oneValue)) {
                    rKey.append("C");
                } else if (SystemCalculateConstants.FAN_SUPPLIER_SIMENS.equals(oneValue)
                        ||SystemCalculateConstants.FAN_SUPPLIER_GE.equals(oneValue)) {
                    rKey.append("S");
                }
            } else if(oneProp.equals(UtilityConstant.METASEXON_FAN_TYPE)) {
                if (SystemCalculateConstants.FAN_TYPE_1.equals(String.valueOf(oneValue))) {
                    rKey.append("E");
                } else if (SystemCalculateConstants.FAN_TYPE_2.equals(String.valueOf(oneValue))) {
                    rKey.append("A");
                } else if (SystemCalculateConstants.FAN_TYPE_3.equals(String.valueOf(oneValue))) {
                    rKey.append("B");
                } else if (SystemCalculateConstants.FAN_TYPE_4.equals(String.valueOf(oneValue))) {
                    rKey.append("C");
                } else if (SystemCalculateConstants.FAN_TYPE_5.equals(String.valueOf(oneValue))) {
                    rKey.append("D");
                } else if (SystemCalculateConstants.FAN_TYPE_6.equals(String.valueOf(oneValue))) {
                    rKey.append("D");
                }
            } else if(oneProp.equals(UtilityConstant.METASEXON_FAN_INSULATION)) {
                rKey.append(String.valueOf(oneValue).replace(",IP", StringUtils.EMPTY));
            } else if(oneProp.equals(UtilityConstant.METASEXON_WETFILMHUMIDIFIER_THICKNESS)) {
                rKey.append(Double.valueOf(NumberUtils.toDouble(String.valueOf(oneValue))).intValue());
            } else {
				rKey.append(oneValue);
			}
			log.info(String.format("Find attribute %s in spec %s", oneProp, spec.getName()));
		} else {
			log.warn(String.format("Unknown attribute %s in spec %s", oneProp, spec.getName()));
			rKey.append(UtilityConstant.SYS_STRING_UNKNOW);
		}
		return rKey.toString();
	}

	private static String getHumidificationQ1(Double wHumidificationQ) {
		int[] qArray = new int[] { 20, 30, 35, 50, 52, 55, 65, 75, 78, 80, 95, 102, 120, 130, 135, 140, 150, 155, 175,
				180, 200, 206, 220, 230, 240, 255, 260, 272, 315, 417, 570, 620 };
		for (int k =  0; k <qArray.length; k++) {
			int theK = qArray[k];
			if (theK >= wHumidificationQ) {
				return String.valueOf(theK);
			}
		}
		return String.valueOf(qArray[0]);
	}

	public static boolean isSprayHumidifierSpec(CalculatorSpec spec) {
		if ("sprayHumidifier".equals(spec.getFile()) && "sprayHumidifier.Humidifier".equals(spec.getName())) {
			return true;
		} else {
			return false;
		}
	}
	public static boolean isCtrSpec(CalculatorSpec spec) {
		if ("ctr".equals(spec.getFile()) && "ctr.ctrwgt1".equals(spec.getName())) {
			return true;
		} else {
			return false;
		}
	}
	public static boolean isPanelParamSpec(CalculatorSpec spec) {
		if ("panel".equals(spec.getFile()) && "partition.parameter".equals(spec.getName())) {
			return true;
		} else {
			return false;
		}
	}
	
    public static boolean isFanOrBeltSpec(CalculatorSpec spec) {
        return "A风机".equals(spec.getTab()) || "I风机保护罩".equals(spec.getTab());
    }

    public static boolean isMotorAndPoleSpec(CalculatorSpec spec) {
        return "B电机".equals(spec.getTab());
    }

    public static boolean isMotorSpec(CalculatorSpec spec) {
        return "C启动方式".equals(spec.getTab());
    }

}

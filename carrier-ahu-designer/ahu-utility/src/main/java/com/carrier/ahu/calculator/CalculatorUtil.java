package com.carrier.ahu.calculator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.metadata.entity.calc.CalculatorSpec;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.util.NumberUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CalculatorUtil {
	public static Map<String, String> nameMap = new HashMap<>();
	
    private static class OP {

        private static String LESS_THAN = "<";
        private static String BIGGER_THAN_OR_EQUAL_TO = ">=";

    }

	static {
		nameMap.put("ahu.package", "包装");
		nameMap.put("ahu.base", "机组底座");
		nameMap.put("ahu.support", "机组支架");
		nameMap.put("filter.general", "过滤器");
		nameMap.put("filter.material.support", "过滤器支架");
		nameMap.put("filter.material.bracket", "过滤器挡风板");
		nameMap.put("mix.damper.top", "上风阀");
		nameMap.put("mix.damper.back", "后风阀");
		nameMap.put("mix.damper.left", "左风阀");
		nameMap.put("mix.damper.right", "右风阀");
		nameMap.put("mix.damper.bottom", "底风阀");
		nameMap.put("fan.motor", "电机");
		nameMap.put("combinedfilter.material.support", "混合过滤器支架");
		nameMap.put("combinedfilter.material.bracket", "混合过滤器挡风板");
		nameMap.put("combinedfilter.general.bag", "混合过滤器（袋式）");
		nameMap.put("combinedfilter.general.panel", "混合过滤器（板式）");
		nameMap.put("coolingcoil.general", "冷水盘管");
		nameMap.put("coolingcoil.accesses.connections", "冷水盘管连接方式");
		nameMap.put("coolingcoil.accesses.eliminator", "冷水盘管挡水器");
		nameMap.put("coolingcoil.accesses.outletpipe", "冷水盘管进出水管");
		nameMap.put("coolingcoil.accesses.watercollection", "冷水盘管集水管");
		nameMap.put("coolingcoil.accesses.drainpantype", "冷水盘管水盘形式");
		nameMap.put("coolingcoil.accessesdrainpanmaterial", "冷水盘管集水管材质");
		nameMap.put("coolingcoil.accesses.bafflematerial", "冷水盘管挡风板材质");
		nameMap.put("coolingcoil.accesses.coilframematerial", "冷水盘管盘管框架材质");
		nameMap.put("coolingcoil.accesses.utrap", "冷水盘管存水弯");
		nameMap.put("coolingcoil.accesses.fintype", "冷水盘管翅片材质");
	}

	public static String calPropertyValues(String propStr, Map<String, Object> ahuValueMap, CalculatorSpec spec) {
		return calPropertyValues(propStr, ahuValueMap, spec, spec.getSection());
	}

	/**
	 * 
	 * @param propStr
	 * @param ahuValueMap
	 * @param spec
	 * @param sectionKey
	 * @return
	 */
	public static String calPropertyValues(String propStr, Map<String, Object> ahuValueMap, CalculatorSpec spec,
			String sectionKey) {
		String calModel = spec.getAhucal();
		StringBuffer buff = new StringBuffer();
		// 依据不同的模式，计算重量值
		String sectionPrefix = UtilityConstant.SYS_BLANK;
		if (StringUtils.isNotBlank(sectionKey)) {
			sectionPrefix = MetaCodeGen.calculateAttributePrefix(sectionKey);
		}
		if (ExcelForPriceUtils.PRICESPEC_AHUCAL_01.equals(calModel)) {
            log.info(String.format("Spec style %s in spec %s, with accessories", spec.getAhucal(), spec.getName()));
            return spec.getProperty();
        } else if (ExcelForPriceUtils.PRICESPEC_AHUCAL_13.equals(calModel)
				|| ExcelForPriceUtils.PRICESPEC_AHUCAL_12.equals(calModel)) {
			log.info(String.format("Spec style %s in spec %s, with section %s", spec.getAhucal(), spec.getName(),
					sectionKey));
			return genKey(propStr, ahuValueMap, spec, sectionPrefix);
		} else if (ExcelForPriceUtils.PRICESPEC_AHUCAL_11.equals(calModel)) {
			log.info(String.format("Spec style %s in spec %s", spec.getAhucal(), spec.getName()));
			return genKey(propStr, ahuValueMap, spec, sectionPrefix);
		} else if (ExcelForPriceUtils.PRICESPEC_AHUCAL_21.equals(calModel)) {
			return sectionKey;
		}

		return buff.toString();
	}

	public static String getPriceName(String item) {
		if (item.startsWith("coolingcoil.accesses")) {
			item = item.substring(0, item.length() - 2);
		}
		if (nameMap.containsKey(item)) {
			return nameMap.get(item);
		} else {
			return item;
		}
	}

	/**
	 * 生成查表的键
	 * 
	 * @param propStr
	 * @param ahuValueMap
	 * @param spec
	 * @param sectionPrefix
	 * @return
	 */
	private static String genKey(String propStr, Map<String, Object> ahuValueMap, CalculatorSpec spec,
			String sectionPrefix) {
		StringBuffer rKey = new StringBuffer();
		String[] ss = propStr.split(UtilityConstant.SYS_PUNCTUATION_SEMICOLON);
		for (int i = 0; i < ss.length; i++) {
			if (i > 0) {
				rKey.append(UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN);
			}
			String oneProp = ss[i];
			if (oneProp.startsWith(UtilityConstant.SYS_PUNCTUATION_LOW_HYPHEN)) {
				rKey.append(oneProp.substring(1, oneProp.length()));
			} else {
				if (oneProp.indexOf(UtilityConstant.SYS_PUNCTUATION_DOT) == -1) {
					oneProp = sectionPrefix + UtilityConstant.SYS_PUNCTUATION_DOT + oneProp;
				}
				rKey.append(CalWeightTool.transValue(oneProp, ahuValueMap, spec));
			}
		}
		return rKey.toString();
	}

	/**
	 * 判断当前Spec是否有效
	 * 
	 * @return
	 */
	public static boolean isSpecEffective(CalculatorSpec spec, Map<String, Object> allMetaValueMap) {
		String[] conds = spec.getName().split(UtilityConstant.SYS_PUNCTUATION_COLON);
		return isSpecEffective(spec, conds, allMetaValueMap);
	}

    public static boolean isSpecEffective(CalculatorSpec spec, String[] conds, Map<String, Object> metaValueMap) {
        if (conds != null && conds.length > 1) {
            String metaKey = conds[0];
            String conditionValue = conds[1];
            int next = 2;
			if ((UtilityConstant.SYS_PUNCTUATION_AND + UtilityConstant.SYS_PUNCTUATION_AND).equals(conds[0])) {
				metaKey = conds[1];
				conditionValue = conds[2];
				next++;
			}
            String metaValue = String.valueOf(metaValueMap.get(metaKey));
            if (assertMetaCondition(metaValue, conditionValue) || metaValue.equals(conditionValue)) {
                log.info(String.format("Hit calculator spec: %s @ %s ", spec.getName(), spec.getTab()));
                return isSpecEffective(spec, Arrays.copyOfRange(conds, next, conds.length), metaValueMap);
            } else if (Arrays.asList(conditionValue.split("/")).contains(metaValue)) {
                log.info(String.format("Hit calculator spec: %s @ %s ", spec.getName(), spec.getTab()));
                return isSpecEffective(spec, Arrays.copyOfRange(conds, next, conds.length), metaValueMap);
            }
            return false;
        }
        return true;
    }

    private static boolean assertMetaCondition(String metaValue, String conditionValue) {
        if(conditionValue.startsWith(OP.BIGGER_THAN_OR_EQUAL_TO)) {
            conditionValue = conditionValue.replace(OP.BIGGER_THAN_OR_EQUAL_TO, StringUtils.EMPTY);
            return NumberUtil.extractInt(metaValue) >= NumberUtils.toInt(conditionValue);
        } else if(conditionValue.startsWith(OP.LESS_THAN)) {
            conditionValue = conditionValue.replace(OP.LESS_THAN, StringUtils.EMPTY);
            return NumberUtil.extractInt(metaValue) < NumberUtils.toInt(conditionValue);
        }
        return false;
    }

}

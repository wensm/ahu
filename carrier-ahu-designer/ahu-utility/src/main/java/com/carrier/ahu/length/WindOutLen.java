package com.carrier.ahu.length;

import java.util.List;

import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.length.util.SystemCountUtil;
import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.cad.DamperDimension;
import com.carrier.ahu.metadata.entity.section.SectionLength;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by liangd4 on 2017/9/11. 出风段
 */
public class WindOutLen {

	// 计算段长 type:机组型号
	// outType:出风形式 T:顶部出风 A:后出风 L:左侧出风 R:右侧出风 F:底部出风
	// doorType：开门方式 DN:开门无观察窗 DY:开门有观察窗 ND:不开门
	public double getLength(String type, String outType, String doorType) {
		// 默认段长为：从表[功能段段长信息]中查找，第N列的数据
		SectionLength length = AhuMetadata.findOne(SectionLength.class, type);
		if (EmptyUtil.isNotEmpty(length)) {
			return length.getN();
		}
		return 0.00;
	}
	
	/**
	 * 计算出风段最小段长
	 * @param type	计算段长 type:机组型号
	 * @param outType	出风形式 T:顶部出风 A:后出风 L:左侧出风 R:右侧出风 F:底部出风
	 * @param doorType	开门方式 DN:开门无观察窗 DY:开门有观察窗 ND:不开门
	 * @param airVolume	风量（已经区分送回风）
	 * @return	最小段长（double）
	 */
	public double getCalLength(String type, String outType, String doorType, int airVolume) {
		String serial = type.substring(0,type.length()-4);
		int height = Integer.valueOf(type.substring(type.length() - 4, type.length() - 2));
		int width = Integer.valueOf(type.substring(type.length()-2,type.length()));
		SectionLength length = AhuMetadata.findOne(SectionLength.class, type);		
		int minsectinl = 5;
		
		if (SystemCountUtil.lteSmallUnitHeight(height)) {
			if (EmptyUtil.isNotEmpty(length)) {
				return length.getN();
			}
		} else {// 如果机组高度大于23 的时候，用以下逻辑
			
			if (UtilityConstant.SYS_ALPHABET_A_UP.equals(outType)) {// 2.后出风
				if (EmptyUtil.isNotEmpty(length)) {
					minsectinl = length.getAMIN();
				}
			}
			if (UtilityConstant.SYS_ALPHABET_T_UP.equals(outType)
					|| UtilityConstant.SYS_ALPHABET_F_UP.equals(outType)) {// 1.顶部5.底部
				int minsectinl1 = calMinSectionl(serial, width, UtilityConstant.SYS_STRING_NUMBER_1, airVolume);
				if (minsectinl1 >= minsectinl) {
					minsectinl = minsectinl1;
				}
			}
			if (UtilityConstant.SYS_ALPHABET_L_UP.equals(outType)
					|| UtilityConstant.SYS_ALPHABET_R_UP.equals(outType)) {// 3.左侧4.右侧
				int minsectinl2 = calMinSectionl(serial, width, UtilityConstant.SYS_STRING_NUMBER_3, airVolume);
				if (minsectinl2 >= minsectinl) {
					minsectinl = minsectinl2;
				}
			}
		}
		return minsectinl;
	}
	
	private static int calMinSectionl(String serial, int width, String mixType, int airVolume) {
		int FC = 0;
		double FA = 0;
		if ("39G".equals(serial)) {
			FC = (width - 1) * 100 + 48 - 72;
		} else if ("39CQ".equals(serial)) {
			FC = (width - 1) * 100 + 128 - 72;
		} else if ("39XT".equals(serial)) {
			FC = (width - 1) * 100 + 76 - 72;
		}
		// 风量注意区分送回风
		FA = airVolume/3600 / (FC / 1000) / 8 * 1000;
		List<DamperDimension> ddList = AhuMetadata.findList(DamperDimension.class, serial);
		DamperDimension finalDD = null;
		for (DamperDimension dd : ddList) {
			if (mixType.equals(dd.getMixType()) && Double.valueOf(dd.getA()) > FA) {
				if (EmptyUtil.isEmpty(finalDD)) {
					finalDD = dd;
				} else if (Double.valueOf(finalDD.getA()) >= Double.valueOf(dd.getA())) {
					finalDD = dd;
				}
			}
		}
		int minsectinl = Integer.valueOf(finalDD.getSectionL());
		return minsectinl;
	}
	
}

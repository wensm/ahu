package com.carrier.ahu.util;

import com.alibaba.fastjson.JSON;
import com.carrier.ahu.common.configuration.AHUContext;
import com.carrier.ahu.common.entity.Part;
import com.carrier.ahu.common.entity.UserConfigParam;
import com.carrier.ahu.common.enums.AhuStatusEnum;
import com.carrier.ahu.common.enums.LanguageEnum;
import com.carrier.ahu.common.enums.RoleEnum;
import com.carrier.ahu.common.enums.SectionTypeEnum;
import com.carrier.ahu.common.intl.I18NBundle;
import com.carrier.ahu.constant.UtilityConstant;
import com.carrier.ahu.po.meta.MetaParameter;
import com.carrier.ahu.report.*;
import com.carrier.ahu.section.meta.AhuSectionMetas;
import com.carrier.ahu.section.meta.MetaCodeGen;
import com.carrier.ahu.section.meta.TemplateUtil;
import com.carrier.ahu.unit.BaseDataUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.Map.Entry;

import static com.carrier.ahu.common.configuration.AHUContext.getIntlString;
import static com.carrier.ahu.common.intl.I18NConstants.*;
import static com.carrier.ahu.constant.CommonConstant.*;
import static com.carrier.ahu.report.common.ReportConstants.BACKUPFILE_EXPORTEXCELGROUP;
import static com.carrier.ahu.report.common.ReportConstants.BACKUPFILE_EXPORTEXCELGROUPBYTEMPLET;
import static com.carrier.ahu.vo.SystemCalculateConstants.COOLINGCOIL_SQUALIFIEDCONDITIONS_1;
import static com.carrier.ahu.vo.SystemCalculateConstants.HEATINGCOIL_WQUALIFIEDCONDITIONS_1;

public class Excel4ModelInAndOutUtils {

	/**
	 * 非标文件导出 add by gaok2
	 * 1.读取导出模板，模板路径asserts/input/template/tempalte.excel.nsfile.xlsx
     * 2.获取数据库数据写入导出文件
	 * @param path
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public static void exportNsExcel(String path, List<ExportUnit> units, LanguageEnum language)
			throws IllegalAccessException, InvocationTargetException, IOException,InvalidFormatException {
        //变量初始化
	    Row row = null;
	    Cell cell = null;
        List<List<String>> exportValues = new ArrayList<List<String>>();
        String sMemo = null;
        String sPrice = null;
        String sSegName = null;
        int start = -1;

		// 生成Excel工作簿>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        //=================1.根据语言，拷贝一个模板=================
        File returnFile = new File(path);
        if ("zh-CN".equals(language.getId())) {
            FileUtils.copyFile(new File(NS_EXPORT_TEMPLATE),
                    returnFile);
        } else {
            FileUtils.copyFile(new File(NS_EXPORT_EN_TEMPLATE),
                    returnFile);
        }

        //=================2.读取模板内容=================
        /* 根据版本选择创建Workbook的方式 */
        boolean isExcel2003 = ExcelUtils.isExcel2003(path);
        Workbook wb = null;
        if (isExcel2003) {
            wb = ExcelUtils.openExcelByPOIFSFileSystem(new FileInputStream(returnFile));
        } else {
            wb = ExcelUtils.openExcelByFactory(new FileInputStream(returnFile));
        }

		// 读取导出模板
        //获取第一个sheet里的内容
		Sheet sheet = wb.getSheetAt(0);
        //获取最后一行行号，继续输出数据
        int rowsNum = sheet.getLastRowNum() + 1;
        //循环数据，准备往excel表里输出数据，这里全部放进exportValues集合里，然后再循环往excel表里插入
		for (int i = 0; i < units.size(); i++) {
            List<String> values = new ArrayList<>();
			ExportUnit eUnit = units.get(i);
            List<ExportPart> parts = eUnit.getExportParts();
            ExportPart ePart = new ExportPart();
            //准备箱体段的数据
            values = new ArrayList<>();
            values.add(eUnit.getUnitNo());
            values.add(eUnit.getSeries());
            values.add(eUnit.getName());
            values.add(eUnit.getMount()+"");
            //箱体段的功能段序号默认为0
            values.add("0");
            //加入一列隐藏列，更新时需要读取位置序号
			values.add("0");
            //获取箱体段的非标内容和非标价格，内容和价格存在metajson中
            String unitMetaJson = eUnit.getMetaJson();
            Map<String, String> umap = new HashMap<String, String>();
            if (StringUtils.isNotBlank(unitMetaJson)) {
                umap = JSON.parseObject(unitMetaJson, Map.class);
            }
            sMemo = umap.get(METANS_AHU_ID_MEMO);
            sPrice = umap.get(METANS_AHU_ID_PRICE);
            //导出时根据界面所选语言，来展示箱体段是中文还是英文
            if ("zh-CN".equals(language.getId())) {
                values.add(AHU_SEG_BOX_CN);
            }else{
                values.add(AHU_SEG_BOX_EN);
            }
            values.add(sMemo);
            values.add(sPrice);

            exportValues.add(values);

            //继续准备AHU功能段信息
            for (int j = 0; j< parts.size(); j++) {
                ePart = parts.get(j);
                List<String> ahuValues = new ArrayList<>();
                ahuValues.add(eUnit.getUnitNo());//机组编号
                ahuValues.add(eUnit.getSeries());//机组型号
                ahuValues.add(eUnit.getName());//机组名称
                ahuValues.add(eUnit.getMount()+"");//机组数量
				ahuValues.add(j+1+"");//功能段序号
                ahuValues.add(ePart.getPosition()+"");//隐藏列
                //功能段名称要转码，例如数据库里存的是ahu.mix,导出文件里要根据所选语言来展示，中文为混合段，英文为MIX
                ahuValues.add(AHUContext.getIntlString(SectionTypeEnum.getSectionTypeFromId(ePart.getSectionKey()).getCnName()));

                //各个功能段的非标内容和价格都在metajson里，且key值都不一样，这里的读取逻辑是，获取ePart.getSectionKey()的.后面的值，再在前面拼上ns.section.
                //在后面拼上Price或者MEMO，这样就组成了MetaJson里的key，可以通过Key值读取对应的属性
                String partSectionKey = ePart.getSectionKey();
                Map<String, String> pmap = JSON.parseObject(ePart.getMetaJson(), Map.class);
                start = partSectionKey.lastIndexOf(UtilityConstant.SYS_PUNCTUATION_DOT);
                sSegName = partSectionKey.substring(start + 1);
                sMemo = pmap.get(METANS_NS_SECTION + sSegName + METACOMMON_POSTFIX_MEMO);
                sPrice = pmap.get(METANS_NS_SECTION + sSegName + METACOMMON_POSTFIX_PRICE);
                ahuValues.add(sMemo);
                ahuValues.add(sPrice);

                exportValues.add(ahuValues);
            }
		}
		//准备好导出的数据后，循环往excel表里写就行
        for (List<String> nsInfoList : exportValues){
		    row = sheet.createRow(rowsNum++);
            int col = 0;
            for (String v : nsInfoList) {
                cell = row.createCell(col++);
                cell.setCellType(CellType.STRING);
                if(v!=null&& v.endsWith(".0")) {
                    v=v.substring(0, v.indexOf(UtilityConstant.SYS_PUNCTUATION_DOT));
                }
                cell.setCellValue(v);
            }
        }

		// 生成Excel工作簿 结束<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		// 写文件>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		File file = new File(path);
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		if (file.exists() && BACKUPFILE_EXPORTEXCELGROUP) {
			String fullPathBack = path + UtilityConstant.SYS_PUNCTUATION_DOT + new Date().getTime() + ".xls";
			FileCopyUtils.copyFileUsingApacheCommonsIO(file, new File(fullPathBack));
		}
		FileOutputStream out = new FileOutputStream(path);
		wb.write(out);
		out.close();
		wb.close();
		// 写文件 结束<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	}



    /**
     * 非标文件导入
     *
     * @param inputStream
     * @param isExcel2003
     * @return
     * @throws IOException
     * @throws EncryptedDocumentException
     * @throws InvalidFormatException
     */
    public static List<ExportNsFile> importnsFileExcel(InputStream inputStream, boolean isExcel2003)
            throws IOException, EncryptedDocumentException, InvalidFormatException {
        /* 根据版本选择创建Workbook的方式 */
        Workbook wb = null;
        if (isExcel2003) {
            wb = ExcelUtils.openExcelByPOIFSFileSystem(inputStream);
        } else {
            wb = ExcelUtils.openExcelByFactory(inputStream);
        }

        List<ExportNsFile> exportNsFileList = readnsFileExcel(wb);
        return exportNsFileList;
    }
    /**
     * 读取导入的非标文件，生成导入对象
     *
     * @param wb
     * @return
     */
    private static List<ExportNsFile> readnsFileExcel(Workbook wb) {
        List<ExportNsFile> exportNsFileList = new ArrayList<ExportNsFile>();
        Sheet sheet = wb.getSheetAt(0);

        for (int i = 3; i < sheet.getLastRowNum() + 1; i++) {
            Row row = sheet.getRow(i);
            if(row==null) {
                continue;
            }
            ExportNsFile exportNsFile = new ExportNsFile();
            exportNsFile.setUnitNo(ExcelCellReader.getCellContext(row.getCell(0)));
            exportNsFile.setUnitSeries(ExcelCellReader.getCellContext(row.getCell(1)));
            exportNsFile.setSegName(ExcelCellReader.getCellContext(row.getCell(2)));
            exportNsFile.setUnitMount(Short.parseShort(ExcelCellReader.getCellContext(row.getCell(3))));
            exportNsFile.setSegNo(ExcelCellReader.getCellContext(row.getCell(5)));
            exportNsFile.setSegName(ExcelCellReader.getCellContext(row.getCell(6)));
            exportNsFile.setNsMemo(ExcelCellReader.getCellContext(row.getCell(7)));
            if(!RoleEnum.Sales.name().equalsIgnoreCase(AHUContext.getUserRole())) {
                exportNsFile.setNsPrice(ExcelCellReader.getCellContext(row.getCell(8)));
            }
            exportNsFileList.add(exportNsFile);
        }
        return exportNsFileList;
    }

	/**
	 * 批量导出[分组]
	 * 
	 * @param path
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public static void exportExcelGroup(String path, ExportProject eProject, LanguageEnum language)
			throws IllegalAccessException, InvocationTargetException, IOException {
		
		// 获取全局配置参数：导出模式 0-全量 1-配置
		String exportMode = new UserConfigParam().getExportMode();
		// 默认为根据配置导出
		if (EmptyUtil.isEmpty(exportMode)) {
			exportMode = UtilityConstant.SYS_STRING_NUMBER_1;
		}
		List<String> list = new ArrayList<>();
		
		// 检查导出的数据源>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		if (EmptyUtil.isEmpty(eProject)) {
			throw new NullPointerException("分组导出时，项目信息缺失");
		}
		List<ExportGroup> groups = eProject.getGroups();
		if (EmptyUtil.isEmpty(groups)) {
			throw new NullPointerException("分组导出时，分组信息缺失");
		}
		ExportGroup eGroup = groups.get(0);
		String groupCode = eGroup.getGroupCode();
		if (EmptyUtil.isEmpty(groupCode)) {
			throw new NullPointerException("分组导出时，分组编码信息缺失");
		}
		List<ExportUnit> units = eGroup.getUnits();
		if (EmptyUtil.isEmpty(units)) {
			throw new NullPointerException("分组导出时，机组信息缺失");
		}
		for (ExportUnit eUnit : units) {
			if (EmptyUtil.isEmpty(eUnit.getMetaJson())) {
				throw new NullPointerException("分组导出时，机组内metaJson信息缺失");
			}
			List<ExportPart> parts = eUnit.getExportParts();
			if (EmptyUtil.isEmpty(parts)) {
				throw new NullPointerException("分组导出时，机组内段信息缺失");
			}
			for (ExportPart ePart : parts) {
				if (EmptyUtil.isEmpty(ePart.getMetaJson())) {
					throw new NullPointerException("分组导出时，机组内段的metaJson信息缺失");
				}
			}
		}
		// 检查导出的数据源 结束<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		// 整理所有性能参数列表>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		Map<String, LinkedList<String>> keyBaseMap = new HashMap<>();// 属性-所有
		Map<String, LinkedList<String>> nameBaseMap = new HashMap<>();// 属性名称-所有
		Map<String, LinkedList<String>> memoBaseMap = new HashMap<>();// 属性中文名称-所有

		Map<String, List<MetaParameter>> map = AhuSectionMetas.getInstance().getPlatMetaWithDetail();
		if(UtilityConstant.SYS_STRING_NUMBER_1.equals(exportMode)) {
			list = TemplateUtil.fiterByCsv(TemplateUtil.getPerformanceExportKeyList());
		}else {
			list = TemplateUtil.getPerformanceExportKeyList();
		}
		List<String> performanceExportKeyList = TemplateUtil.getPerformanceExportKeyList();
		for (Entry<String, List<MetaParameter>> e : map.entrySet()) {
			LinkedList<String> keys = new LinkedList<>();
			LinkedList<String> name = new LinkedList<>();
			LinkedList<String> memo = new LinkedList<>();
			for (MetaParameter mp : e.getValue()) {
				if (mp.isrPerformance()) {
					String key = mp.getKey();
//					if (performanceExportKeyList.contains(key)) {
					if (list.contains(key)) {
						keys.add(key);
						int start = key.lastIndexOf(UtilityConstant.SYS_PUNCTUATION_DOT);
						name.add(key.substring(start + 1));
						if (language == LanguageEnum.English) {
							memo.add(mp.getEmemo());
						} else {
							memo.add(I18NBundle.getString(mp.getMemo(), LanguageEnum.Chinese));
						}
					}
				}
			}
			keyBaseMap.put(e.getKey(), keys);
			nameBaseMap.put(e.getKey(), name);
			memoBaseMap.put(e.getKey(), memo);
		}
		// 整理所有性能参数列表 结束<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		// 获得需要导出的机组、段属性列表>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		Map<String, LinkedList<String>> keyMap = new HashMap<>();// 属性
		Map<String, LinkedList<String>> nameMap = new HashMap<>();// 属性名称
		Map<String, LinkedList<String>> memoMap = new HashMap<>();// 属性中文名称

		LinkedList<String> partKeyList = getPartKeySort4Export(groupCode);// 存放导出段的段序Key
		for (String key : partKeyList) {
			if (nameBaseMap.containsKey(key) && memoBaseMap.containsKey(key)) {
				keyMap.put(key, keyBaseMap.get(key));
				nameMap.put(key, nameBaseMap.get(key));
				memoMap.put(key, memoBaseMap.get(key));
			}
		}
		// 获得需要导出的机组、段属性列表 结束<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		// 整理文档首行需要合并的单元格个数表 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		Map<String, Integer> colMap = new HashMap<>();
		for (Entry<String, LinkedList<String>> e : nameMap.entrySet()) {
			colMap.put(e.getKey(), e.getValue().size());
		}
		// 整理文档首行需要合并的单元格个数表 结束 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		// 生成Excel工作簿>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		XSSFWorkbook wb = new XSSFWorkbook();
		// 创建Header
		Sheet sheet = wb.createSheet("Sheet1");
		Row row0 = sheet.createRow(0);
		Row row1 = sheet.createRow(1);
		Row row2 = sheet.createRow(2);
		Row row3 = sheet.createRow(3);
		Cell cell00 = row0.createCell(0);
		cell00.setCellValue(getIntlString(PROJECT));
		Cell cell1 = row0.createCell(1);
		cell1.setCellValue(getIntlString(DRAWING_NO));
		Cell cell2 = row0.createCell(2);
		cell2.setCellValue(getIntlString(UNIT_SERIES));
		Cell cell3 = row0.createCell(3);
		cell3.setCellValue(getIntlString(UNIT_NAME));
		sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 0));// 设置单元格合并
		sheet.addMergedRegion(new CellRangeAddress(0, 2, 1, 1));// 设置单元格合并
		sheet.addMergedRegion(new CellRangeAddress(0, 2, 2, 2));// 设置单元格合并
		sheet.addMergedRegion(new CellRangeAddress(0, 2, 3, 3));// 设置单元格合并

		int c = 4, c0 = 4, c1 = 4, c2 = 4;
		LinkedList<String> keyList = new LinkedList<>();// 存放导出的属性Key
		for (String key : partKeyList) {
			keyList.addAll(keyMap.get(key));
			LinkedList<String> nameList = nameMap.get(key);
			LinkedList<String> memoList = memoMap.get(key);
			for (String s : nameList) {
				Cell cell0 = row0.createCell(c0++);
				cell0.setCellValue(key);
				Cell cell = row1.createCell(c1++);
				cell.setCellValue(s);
			}
			for (String s : memoList) {
				Cell cell = row2.createCell(c2++);
				cell.setCellValue(s);
			}
			if (colMap.get(key) > 1) {
				sheet.addMergedRegion(new CellRangeAddress(0, 0, c, c - 1 + colMap.get(key)));// 设置单元格合并
			}
			c += colMap.get(key);
		}
		for (int i = 0, rowsNum = 4; i < units.size(); i++) {
			List<String> values = new ArrayList<>();
			ExportUnit eUnit = units.get(i);
			values.add(eProject.getName());
			values.add(eUnit.getDrawingNo());
			values.add(eUnit.getPaneltype());
			values.add(eUnit.getName());

			// 存放所有该机组以及机组下段的metaJson里的数据
			Map<String, String> allValues = new HashMap<>();
			String unitMetaJson = eUnit.getMetaJson();

			if (StringUtils.isNotBlank(unitMetaJson)) {
				Map<String, String> umap = JSON.parseObject(unitMetaJson, Map.class);
				allValues.putAll(umap);
			}
			List<ExportPart> parts = eUnit.getExportParts();

			for (ExportPart ePart : parts) {
				String partMetaJson = ePart.getMetaJson();
				Map<String, String> pmap = JSON.parseObject(partMetaJson, Map.class);
				allValues.putAll(pmap);
			}

			for (String k : keyList) {
				if (allValues.containsKey(k)) {
					values.add(allValues.get(k));
				} else {
					values.add(ExcelCellReader.BLANK);
				}
			}
			Row row = sheet.createRow(rowsNum++);
			int col = 0;
			for (String v : values) {
				Cell cell = row.createCell(col++);
				cell.setCellType(CellType.STRING);
				if(v!=null&& v.endsWith(".0")) {
					v=v.substring(0, v.indexOf(UtilityConstant.SYS_PUNCTUATION_DOT));
				}
				cell.setCellValue(v);
			}
		}
		Row sRow = sheet.getRow(3);//第四行隐藏	
		sRow.setZeroHeight(true);
		// 生成Excel工作簿 结束<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		// 写文件>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		File file = new File(path);
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		if (file.exists() && BACKUPFILE_EXPORTEXCELGROUP) {
			String fullPathBack = path + UtilityConstant.SYS_PUNCTUATION_DOT + new Date().getTime() + ".xls";
			FileCopyUtils.copyFileUsingApacheCommonsIO(file, new File(fullPathBack));
		}
		FileOutputStream out = new FileOutputStream(path);
		wb.write(out);
		out.close();
		wb.close();
		// 写文件 结束<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	}

	@SuppressWarnings("unchecked")
	public static void exportExcelGroupByTemplet(String path, ExportProject eProject, LanguageEnum language)
			throws IllegalAccessException, InvocationTargetException, IOException, EncryptedDocumentException, InvalidFormatException {
		
		// 检查导出的数据源>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				if (EmptyUtil.isEmpty(eProject)) {
					throw new NullPointerException("分组导出时，项目信息缺失");
				}
				List<ExportGroup> groups = eProject.getGroups();
				if (EmptyUtil.isEmpty(groups)) {
					throw new NullPointerException("分组导出时，分组信息缺失");
				}
				ExportGroup eGroup = groups.get(0);
				String groupCode = eGroup.getGroupCode();
				if (EmptyUtil.isEmpty(groupCode)) {
					throw new NullPointerException("分组导出时，分组编码信息缺失");
				}
				List<ExportUnit> units = eGroup.getUnits();
				if (EmptyUtil.isEmpty(units)) {
					throw new NullPointerException("分组导出时，机组信息缺失");
				}
				for (ExportUnit eUnit : units) {
					if (EmptyUtil.isEmpty(eUnit.getMetaJson())) {
						throw new NullPointerException("分组导出时，机组内metaJson信息缺失");
					}
					List<ExportPart> parts = eUnit.getExportParts();
					if (EmptyUtil.isEmpty(parts)) {
						throw new NullPointerException("分组导出时，机组内段信息缺失");
					}
					for (ExportPart ePart : parts) {
						if (EmptyUtil.isEmpty(ePart.getMetaJson())) {
							throw new NullPointerException("分组导出时，机组内段的metaJson信息缺失");
						}
					}
				}
				// 检查导出的数据源 结束<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		
		//=================1.根据语言，拷贝一个模板=================
		File returnFile = new File(path);
		if ("zh-CN".equals(language.getId())) {
			FileUtils.copyFile(new File(SYS_PATH_ASSERTS_TEMPLATE_CN),
					returnFile);
		} else {
			FileUtils.copyFile(new File(SYS_PATH_ASSERTS_TEMPLATE_EN),
					returnFile);
		}
		
		//=================2.读取模板内容=================
		/* 根据版本选择创建Workbook的方式 */
		boolean isExcel2003 = ExcelUtils.isExcel2003(path);
		Workbook wb = null;
		if (isExcel2003) {
			wb = ExcelUtils.openExcelByPOIFSFileSystem(new FileInputStream(returnFile));
		} else {
			wb = ExcelUtils.openExcelByFactory(new FileInputStream(returnFile));
		}
		
		//=================3.根据数据填充模板=================
		Sheet sheet = wb.getSheetAt(0);
		Row row0 = sheet.getRow(0);
		Row row1 = sheet.getRow(1);
		for (int unitCounts = 0; unitCounts < units.size(); unitCounts++) {
			int unitNum = 0;
			for (ExportUnit eUnit : units) {
				Row row = sheet.createRow(unitNum+4);
//				Row row = sheet.getRow(unitNum+4);//数据从第5行开始填充
				//前4列数据
				row.createCell(0).setCellValue(eProject.getName());
				row.createCell(1).setCellValue(eUnit.getDrawingNo());
				row.createCell(2).setCellValue(eUnit.getProduct());
				row.createCell(3).setCellValue(eUnit.getName());
				row.createCell(4).setCellValue(eUnit.getMount());
				
				Map metaJsonMap = new HashMap<>();
				List<ExportPart> parts = eUnit.getExportParts();
				metaJsonMap.putAll(JSON.parseObject(eUnit.getMetaJson(), Map.class));
				for(ExportPart ePart : parts) {
					metaJsonMap.putAll(JSON.parseObject(ePart.getMetaJson(), Map.class));
				}
				
				
				for(int colNum=5; colNum < sheet.getRow(0).getLastCellNum(); colNum++) {
					//拼接key
					String sectionName = sheet.getRow(0).getCell(colNum).getStringCellValue();
					if (sectionName.contains(UtilityConstant.SYS_PUNCTUATION_DOT)) {
						sectionName = sectionName.replace(UtilityConstant.METAHU_AHU_NAME, UtilityConstant.METASEXON_PREFIX) + UtilityConstant.SYS_PUNCTUATION_DOT;
					} else {
						sectionName = UtilityConstant.METAHU_PREFIX;
					}
					String fieldName = sheet.getRow(1).getCell(colNum).getStringCellValue().trim();
					String name = sectionName + fieldName;
					String value = String.valueOf(metaJsonMap.get(name));					
					
					if(EmptyUtil.isEmpty(value)) {
//						row.createCell(colNum);
						continue;
					}else {
						if("null".equals(value)) {
							value = UtilityConstant.SYS_BLANK;
						}
						row.createCell(colNum).setCellValue(value);
					}

				}
				
				unitNum++;
			}
		}
	
		//=================4.输出模板=================
		File file = new File(path);
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		if (file.exists() && BACKUPFILE_EXPORTEXCELGROUPBYTEMPLET) {
			String fullPathBack = path + UtilityConstant.SYS_PUNCTUATION_DOT + new Date().getTime() + ".xls";
			FileCopyUtils.copyFileUsingApacheCommonsIO(file, new File(fullPathBack));
		}
		FileOutputStream out = new FileOutputStream(path);
		wb.write(out);
		out.close();
		wb.close();
	}
	
	/**
	 * 批量导入[分组]
	 * 
	 * @param inputStream
	 * @param isExcel2003
	 * @param groupCode
	 * @return
	 * @throws IOException
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 */
	public static List<ExportUnit> importExcelGroup(InputStream inputStream, boolean isExcel2003, String groupCode)
			throws IOException, EncryptedDocumentException, InvalidFormatException {
		/* 根据版本选择创建Workbook的方式 */
		Workbook wb = null;
		if (isExcel2003) {
			wb = ExcelUtils.openExcelByPOIFSFileSystem(inputStream);
		} else {
			wb = ExcelUtils.openExcelByFactory(inputStream);
		}
		ExportProject eProject = new ExportProject();
		List<ExportGroup> groups = new ArrayList<>();
		ExportGroup eGroup = new ExportGroup();
		List<ExportUnit> units = readExcelGroup(wb, groupCode);
		eGroup.setUnits(units);
		groups.add(eGroup);
		eProject.setGroups(groups);
		return units;
	}

	/**
	 * 读取文档，生成导入对象
	 * 
	 * @param wb
	 * @param groupCode
	 * @return
	 */
	private static List<ExportUnit> readExcelGroup(Workbook wb, String groupCode) {
//		if (EmptyUtil.isEmpty(groupCode)) {
//			throw new NullPointerException("groupCode is ");
//		}
		
		// 获取全局配置参数：导出模式 0-全量 1-配置
		String exportMode = new UserConfigParam().getExportMode();
		// 默认为根据配置导出
		if (EmptyUtil.isEmpty(exportMode)) {
			exportMode = UtilityConstant.SYS_STRING_NUMBER_1;
		}
		
		Sheet sheet = wb.getSheetAt(0);
		Row row0 = sheet.getRow(0);
		Row row1 = sheet.getRow(1);
		LinkedList<String> keyNameList = new LinkedList<>();
		for (int j = 4; j < row0.getLastCellNum(); j++) {
			String k0 = ExcelCellReader.getCellContext(row0.getCell(j));
			String k1 = ExcelCellReader.getCellContext(row1.getCell(j));
			if (SectionTypeEnum.TYPE_AHU.getId().equals(k0)) {
				keyNameList.add("meta." + k0 + UtilityConstant.SYS_PUNCTUATION_DOT + k1);
			} else {
				keyNameList.add(k0.replace(UtilityConstant.METAHU_AHU_NAME, UtilityConstant.METASEXON_PREFIX) + UtilityConstant.SYS_PUNCTUATION_DOT + k1);
			}
		}
		List<ExportUnit> units = new ArrayList<>();
		for (int i = 4; i < sheet.getLastRowNum() + 1; i++) {
			Row row = sheet.getRow(i);
			if(row==null) {
				continue;
			}
			ExportUnit eUnit = new ExportUnit();
			Cell cell=row.getCell(1);
			if(cell==null) {
				continue;
			}
			String dNo = ExcelCellReader.getCellContext(cell);
			if(EmptyUtil.isEmpty(dNo)) {
				continue;
			}
			double dno = Double.parseDouble(dNo);
			eUnit.setDrawingNo(String.valueOf((new Double(dno)).intValue()));//机组编号
			eUnit.setPaneltype(ExcelCellReader.getCellContext(row.getCell(2)));
			eUnit.setProduct(ExcelCellReader.getCellContext(row.getCell(2)));//机组系列
			eUnit.setName(ExcelCellReader.getCellContext(row.getCell(3)));//机组名称
			String amount = ExcelCellReader.getCellContext(row.getCell(4));
			if (EmptyUtil.isNotEmpty(amount)) {
				eUnit.setMount(Short.valueOf(amount));//机组数量				
			}else {
				eUnit.setMount(new Short(UtilityConstant.SYS_STRING_NUMBER_1));
			}
			Map<String, String> keyCouValueMap = new HashMap<>();
			for (int j = 4; j < row.getLastCellNum(); j++) {
				if (EmptyUtil.isEmpty(ExcelCellReader.getCellContext(row.getCell(j)))) {
					continue;
				}
				keyCouValueMap.put(keyNameList.get(j - 4), ExcelCellReader.getCellContext(row.getCell(j)));
			}
//			if (UtilityConstant.SYS_STRING_NUMBER_1.equals(exportMode)) {
//				//全局配置参数为模板，冷水盘管段：限定条件应默认显示为冷量
//				keyCouValueMap.put("meta.section.coolingCoil.SqualifiedConditions", UtilityConstant.SYS_STRING_NUMBER_1);				
//			}

			if (keyCouValueMap.containsKey(METASEXON_COOLINGCOIL_SCOLDQ)) {
				String value = BaseDataUtil.constraintString(keyCouValueMap.get(METASEXON_COOLINGCOIL_SCOLDQ));
				if(!EmptyUtil.isEmpty(value)){
					keyCouValueMap.put(META_COOLINGCOIL_SQUALIFIEDCONDITIONS,COOLINGCOIL_SQUALIFIEDCONDITIONS_1);
				}
			}
			if (keyCouValueMap.containsKey(METASEXON_HEATINGCOIL_WHEATQ)) {
				String value = BaseDataUtil.constraintString(keyCouValueMap.get(METASEXON_HEATINGCOIL_WHEATQ));
				if(!EmptyUtil.isEmpty(value)){
					keyCouValueMap.put(META_HEATINGCOIL_WQUALIFIEDCONDITIONS,HEATINGCOIL_WQUALIFIEDCONDITIONS_1);
				}
			}
			eUnit.setMetaJson(JSON.toJSONString(getPartMetaMaps(SectionTypeEnum.TYPE_AHU, keyCouValueMap)));
			List<ExportPart> parts = new ArrayList<>();
			if(!EmptyUtil.isEmpty(groupCode)) {
				for (int pos = 0; pos < groupCode.length() / 3; pos++) {
					ExportPart ePart = new ExportPart();
					String partCode = groupCode.substring(pos * 3, pos * 3 + 3);
					
					if("011".equals(partCode)) {
						if (keyCouValueMap.containsKey(METAHU_SEXTERNALSTATIC)) {
							keyCouValueMap.put(META_SECTION_FAN_EXTERNALSTATIC,keyCouValueMap.get(METAHU_SEXTERNALSTATIC));
						}
					}
					parsePart(SectionTypeEnum.getSectionTypeByCodeNum(partCode), ePart, keyCouValueMap);
					ePart.setPosition((short) (pos + 1));
					// 设置段的元状态为正在选型
					ePart.setRecordStatus(AhuStatusEnum.SELECTING.getId());
					parts.add(ePart);
				}
			}
			
			eUnit.setExportParts(parts);
			units.add(eUnit);
		}
		return units;
	}

	private static LinkedList<String> getPartKeySort4Export(String groupCode) {
		LinkedList<String> partKeyList = new LinkedList<>();// 存放导出段的段序Key
		partKeyList.add("ahu");
		if (EmptyUtil.isEmpty(groupCode)) {
			return partKeyList;
		}
		int len = groupCode.length();
		if (len == 0 || len % 3 != 0) {
			return partKeyList;
		}
		int k = len / 3;
		for (int i = 0; i < k; i++) {
			String key = groupCode.substring(i * 3, (i + 1) * 3);
			Short value = Short.parseShort(key);
			for (Entry<String, Short> it : MetaCodeGen.KEY_CODE_MAP.entrySet()) {
				if (it.getValue() == value) {
					partKeyList.add(it.getKey());
				}
			}
		}
		return partKeyList;
	}

	private static void parsePart(SectionTypeEnum section, Part p, Map<String, String> allMap) {
		p.setSectionKey(section.getId());
		p.setMetaJson(JSON.toJSONString(getPartMetaMaps(section, allMap)));
	}

	private static Map<String, Object> getPartMetaMaps(SectionTypeEnum section, Map<String, String> allMap) {
		Map<String, Object> resultMap = new HashMap<>();
		for (Entry<String, String> entry : allMap.entrySet()) {
			if (entry.getKey().startsWith(MetaCodeGen.calculateAttributePrefix(section.getId()))) {
				resultMap.put(entry.getKey(), valueFilter(entry.getValue()));
			}
		}
		return resultMap;
	}

	public static Object valueFilter(String value) {
		if (EmptyUtil.isEmpty(value)) {
			return UtilityConstant.SYS_BLANK;
		} else {
			if (UtilityConstant.SYS_ASSERT_FALSE.equalsIgnoreCase(value.trim())) {
				return false;
			} else if (UtilityConstant.SYS_ASSERT_TRUE.equalsIgnoreCase(value.trim())) {
				return true;
			} else {
				return value;
			}
		}
	}

	public static String getStringValue(Object obj) {
		if (EmptyUtil.isEmpty(obj)) {
			return UtilityConstant.SYS_BLANK;
		}
		if (!(obj instanceof String)) {
			return String.valueOf(obj);
		} else {
			return (String) obj;
		}
	}
}

package com.carrier.ahu.util;

import lombok.Data;

import java.util.List;

@Data
public class BookMark {
    private int num;//页码
    private int level;//层级
    private String title;//书签标题
    List<BookMark> kids;//儿子书签
}

package com.carrier.ahu.length;

import com.carrier.ahu.metadata.AhuMetadata;
import com.carrier.ahu.metadata.entity.section.SectionLength;
import com.carrier.ahu.unit.BaseDataUtil;
import com.carrier.ahu.util.EmptyUtil;

/**
 * Created by liangd4 on 2017/9/11. 新回排风段
 */
public class RepeatingWindLen {

	// type:机组型号
	public double getLength(String type) {
		// 从[功能段段长信息]中查找L列的数据
		SectionLength length = AhuMetadata.findOne(SectionLength.class, type);
		if (EmptyUtil.isNotEmpty(length)) {
			return BaseDataUtil.integerConversionDouble(length.getL());
		}
		return 0.00;
	}
}

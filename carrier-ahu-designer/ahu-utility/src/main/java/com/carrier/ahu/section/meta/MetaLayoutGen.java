package com.carrier.ahu.section.meta;

import com.carrier.ahu.metadata.section.MetaValue;
import com.carrier.ahu.metadata.section.ValueOption;
import com.carrier.ahu.po.meta.Meta;
import com.carrier.ahu.po.meta.MetaParameter;
import com.carrier.ahu.po.meta.layout.Group;
import com.carrier.ahu.po.meta.layout.MetaLayout;
import com.carrier.ahu.util.EmptyUtil;
import com.carrier.ahu.util.meta.SectionMetaUtils;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MetaLayoutGen {

	/**
	 *
	 * 这个方法是为了保证旧代码的兼容性，后续可以通过删除这个方法，来清理无用的代码
	 * 
	 * @param layout
	 * @param meta
	 *
	 */
	@Deprecated
	public static void mergeLayoutProperty(MetaLayout layout, Meta meta) {
		mergeLayoutProperty(layout, meta, new HashMap<>(),null);

	}

	/**
	 * merge jsonValue 和xls
	 * @param layout
	 * @param meta
	 * @param cn2Key
	 */
	public static void mergeLayoutProperty(MetaLayout layout, Meta meta, Map<String, String> cn2Key,String unitSeries) {
		List<Group> list = layout.getGroups();
		Iterator<Group> it = list.iterator();
		while (it.hasNext()) {
			Group group = it.next();
			List<String> keys = group.getKeys();
			Iterator<String> it1 = keys.iterator();
			boolean appended = false;
			while (it1.hasNext()) {
				String key = it1.next();
				MetaParameter para = meta.getParameters().get(key);
				if (EmptyUtil.isEmpty(para)) {
					System.out.println("**** " + key);
					continue;
				}
				MetaParameterWithUnit para1 = new MetaParameterWithUnit();
				try {
					BeanUtils.copyProperties(para1, para);
					String memo = para1.getMemo();
					if (cn2Key.containsKey(memo)) {
						para1.setMemo(cn2Key.get(memo));
					}
				} catch (IllegalAccessException | InvocationTargetException e) {
					e.printStackTrace();
				}
				para1.initUnitString(cn2Key,unitSeries);//加载jsonValue
				if(!appended){
					group.getParas().clear();//第一次清空所有历史选项，防止重复加载
					appended = true;//保证清空一次
				}
				group.getParas().add(para1);
			}
		}
	}

	/**
	 * Wrapper to add unit info in meta para result
	 *
	 * @author JL
	 */
	public static class MetaParameterWithUnit extends MetaParameter {
		private ValueOption option;
		private String defaultValue;
		private Map<String, String> defaultValueMap;
		/**
		 * 初始化单位，属性选项值的属性
		 *
		 * @param cn2Key
		 *            在多语言中，保存汉字与intl key的对应关系，以方便程序开发，和元数据编辑
		 */
		public void initUnitString(Map<String, String> cn2Key,String unitSeries) {
			String source = getValueSource();
			MetaValue value = SectionMetaUtils.getSectionMetaValue(source, unitSeries);
			if (EmptyUtil.isNotEmpty(value)) {
				this.option = value.getOption();
				// replace the cn String with the intl key
				if (EmptyUtil.isNotEmpty(this.option)) {
					if (EmptyUtil.isNotEmpty(this.option.getClabels())) {
						String[] tempArr = this.option.getClabels();
						for (int i = 0; i < tempArr.length; i++) {
							if (cn2Key.containsKey(tempArr[i])) {
								this.option.getOptionPairs().get(i).setClabel(cn2Key.get(tempArr[i]));
							}
						}
					}
				}
				this.setDefaultValue(value.getDefaultValue());
				this.setDefaultValueMap(value.getDefaultValueMap());
			}
		}

		public ValueOption getOption() {
			return option;
		}

		public void setOptions(ValueOption option) {
			this.option = option;
		}

		public String getDefaultValue() {
			return defaultValue;
		}

		public void setDefaultValue(String defaultValue) {
			this.defaultValue = defaultValue;
		}

		public Map<String, String> getDefaultValueMap() {
			return defaultValueMap;
		}

		public void setDefaultValueMap(Map<String, String> defaultValueMap) {
			this.defaultValueMap = defaultValueMap;
		}
	}

}

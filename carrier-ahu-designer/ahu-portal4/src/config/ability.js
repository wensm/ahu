import { Ability, AbilityBuilder } from '@casl/ability'

function subjectName(item) {
  if (!item || typeof item === 'string') {
    return item
  }

  return item.__type
}

Ability.addAlias('all', ['GET', 'POST', 'PUT', 'DELETE'])
export default AbilityBuilder.define((can, cannot) => {
})
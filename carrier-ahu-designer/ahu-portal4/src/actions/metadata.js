import http from '../misc/http'

export const RECEIVE_METADATA_LAYOUT = 'RECEIVE_METADATA_LAYOUT'
export const RECEIVE_METADATA_UNIT = 'RECEIVE_METADATA_UNIT'
export const RECEIVE_METADATA_VALIDATION = 'RECEIVE_METADATA_VALIDATION'
export const RECEIVE_METADATA_VARIABLE_VALIDATION = 'RECEIVE_METADATA_VARIABLE_VALIDATION'
export const RECEIVE_METADATA_SERIAL = 'RECEIVE_METADATA_SERIAL'
export const RECEIVE_METADATA_SERIAL_CHANGE = 'RECEIVE_METADATA_SERIAL_CHANGE'
export const GET_VERSION = 'GET_VERSION'
export const SET_PRODUCT = 'SET_PRODUCT'
export const SET_SERIAL = 'SET_SERIAL'


export function fetchLayout(ahuId, callBack) {
  return dispatch => http.get('meta/layout', {unitid:ahuId}).then(data => {
    dispatch(receivedLayout(data))
    callBack && callBack()
  })
}
export function fetchMetaUnit() {
  return (dispatch, getState) => http.get('meta/unit').then(data => {
    let locale = getState().general.user.preferredLocale;
    dispatch(receivedMetaDataUnit(data, locale))

  })
}
export function fetchMetaValidation(ahuId) {
  return (dispatch,getState) => http.get('meta/validations', {unitid:ahuId}).then(data => {
    let {general, metadata, ahu} = getState()
    let {componentValue} = ahu
    let {unitPreferCode, unitPrefer, preferredLocale} = general.user
    let {metaUnit} = metadata
//    console.log('zzfuser', general.user, unitPrefer, metaUnit)
    dispatch(receivedMetaValidations(data, preferredLocale, unitPreferCode, unitPrefer, metaUnit, componentValue))
  })
}
export function fetchMetaVariableValidation(ahuValues, selectedComponent, name, baseSections, locale) {
  return {
    type: RECEIVE_METADATA_VARIABLE_VALIDATION,
    ahuValues,
    selectedComponent,
    selectedName:name,
    baseSections,
    locale
  }
}
function receivedLayout(data) {
  return {
    type: RECEIVE_METADATA_LAYOUT,
    data
  }
}
function receivedMetaDataUnit(data, locale) {
  return {
    type: RECEIVE_METADATA_UNIT,
    data,
    locale
  }
}
function receivedMetaValidations(data, locale, unitPreferCode, unitPrefer, metaUnit, componentValue) {
  
  return {
    type: RECEIVE_METADATA_VALIDATION,
    data,
    locale,
    unitPreferCode,
    unitPrefer,
    metaUnit,
    componentValue

    /*
      test data
    :{
      "meta.section.mix.SInDryBulbT":[
        {"rule":"range","ruleValueSet":["5-100"],"ruleMassage":"警告：值需要在5到100之间",level:"warn"}
      ],

      "meta.section.fan.sectionL":[
        {"rule":"range","ruleValueSet":["5-100"],"ruleMassage":"警告：值需要在5到100之间",level:"error"}
      ],

    }*/
  }
}
export function fetchSerial(ahuId) {
  return (dispatch, getState) => http.get('meta/serial', {unitid:ahuId}).then(data => dispatch(receivedMetaSerial(data, getState().ahu.componentValue)))
   
}
function receivedMetaSerial(data, componentValue){
  return {
    type: RECEIVE_METADATA_SERIAL,
    data,
    componentValue
  }
}
export function changeSeriesCanDropList(data) {
  return {
    type: RECEIVE_METADATA_SERIAL_CHANGE,
    data,
  }
}

export function fetchVersion(data) {
  return dispatch => http.get('help/getSysVersion').then(data => {
    dispatch(getVersion(data.data))
    
  })
   
}
function getVersion(data) {
  return {
    type: GET_VERSION,
    data,
  }
}

export function onSetProduct(product) {
  return {
    type: SET_PRODUCT,
    product,
  }
}


export function onSetSerial(series, product, drawingNo, scrollPosition) {
  return {
    type: SET_SERIAL,
    series,
    product,
    drawingNo,
    scrollPosition
  }
}
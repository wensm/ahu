import {
    OK,
    UPDATE_LANGUAGE_SETTING,
    UPDATE_UNIT_SETTING,
    GET_USER_INFO,
    SIMPLE_CHINESE,
    TRADITIONAL_CHINESE,
    JAPANESE,
    INSTALL_LICENSE_FILE_SUCCESS,
    INSTALL_LICENSE_FILE_FAILED,
    INSTALL_UPGRADE_FILE_SUCCESS,
    INSTALL_UPGRADE_FILE_FAILED,
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import sweetalert from 'sweetalert'
import { hashHistory } from 'react-router'
/**
 * Created by liujianfeng on 2017/9/3.
 */

import $http from '../misc/$http'
export const UPDATE_LANGUAGE = 'UPDATE_LANGUAGE'//{intl.get(UPDATE_LANGUAGE_SETTING)}
export const UPDATE_UNIT = "UPDATE_UNIT"//{intl.get(UPDATE_UNIT_SETTING)}
export const RECEIVE_USERINFO = "RECEIVE_USERINFO"//{intl.get(GET_USER_INFO)}
export const UPLOAD_USER = "UPLOAD_USER"
export const SWITCH_LOCALE = "SWITCH_LOCALE"
export const RECEIVE_USER_ABILITY = "RECEIVE_USER_ABILITY"
export const GET_USER_META = "GET_USER_META"
export const UPLOAD_USER_META = "UPLOAD_USER_META"
export const RECEIVE_PRICE_BASE = "RECEIVE_PRICE_BASE";
export const UPLOAD_IS_EXPORT = "UPLOAD_IS_EXPORT";
export const UPDATESYSPORT = "UPDATESYSPORT";

export const SupportedLocales = [
    {
        name: "English",
        value: "en-US"
    },
    {
        name: intl.get(SIMPLE_CHINESE),
        value: "zh-CN"
    },
    {
        name: intl.get(TRADITIONAL_CHINESE),
        value: "zh-TW"
    },
    {
        name: "français",
        value: "fr-FR"
    },
    {
        name: intl.get(JAPANESE),
        value: "ja-JP"
    },
    {
        name: "Malaysia",
        value: "ms-MY"
    }
]

/**
 * Update language for current site
 * @param language
 */
export const updateLanguage = locale => {
    return {
        type: UPDATE_LANGUAGE,
        locale: locale
    }
}

export const updateUnit = unitPrefer => {
    return {
        type: UPDATE_UNIT,
        unitPrefer
    }
}

/**
 * 更新UnitCode
 * @param unitCode，值为"M" or "B"
 * @returns {{type: string, unitSetting: *}}
 */
export const updateUserUnitCode = (unitPreferCode, unitPrefer) => {
    return {
        type: UPDATE_UNIT,
        unitPrefer,
        unitPreferCode
    }
}

export const updateUserUnitToServer = (unitPrefer) => {
    return dispatch => {
        return $http.post('user', { unitPrefer: unitPrefer }).then(data => dispatch(updateUserInfo(data)))
    }
}

export const updateUserUnitCodeToServer = (unitPreferCode) => {
    return dispatch => {
        return $http.post('user', { unitPreferCode: unitPreferCode }).then(data => dispatch(updateUserInfo(data)))
    }
}

export const fetchUserAbilities = () => {
    return dispatch => {
        return $http.get('abilities').then(data => {
            dispatch(receiveUserAbilities(data));
        })
    }
}

function receiveUserAbilities(data) {
    return {
        type: RECEIVE_USER_ABILITY,
        userAbilities: data.data
    }
}

export const fetchUserInfo = (locale, initIntl) => {
    return dispatch => {
        return $http.get('user', {
        }).then(data => {
            dispatch(fetchUserAbilities());
            dispatch(receiveUserInfo(data));
            if (data.data && data.data.preferredLocale && data.data.preferredLocale != locale) {
                dispatch(switchLocal(data.data.preferredLocale))
                dispatch(fetchIntl(data.data.preferredLocale, initIntl));
            } else {
                // fetch intl data after user info fetched
                dispatch(fetchIntl(locale, initIntl));
            }
        })
    }
}

export const checkSystemUpdate = (lang, showSystemUpdate) => {
    return dispatch => {
        return $http.get('update/check?lang=' + lang, {
        }).then(result => {
            showSystemUpdate(result.data);
        })
    }
}

function fetchIntl(locale, initIntl) {
    return dispatch => {
        return $http.get('i18n/' + locale).then(data => receivedIntl(data, locale, initIntl))
    }
}

function receivedIntl(data, locale, initIntl) {
    let locales = {};
    locales[locale] = JSON.parse(JSON.stringify(data));
    initIntl(locales);
}

//跳转到login
export const fetchtoken = () => {
    let locale = getCookie("lang")

    return dispatch => {
        return $http.get('license').then(data => {
            // console.log(false === data.data.success)
            if (data.data && data.data.success === false) {
                hashHistory.push({
                    pathname: '/login',
                    query: {
                        license: data.data.licenseDat,
                        email: data.data.adminEmail
                    }
                })
            }
        })
    }
}

export const updateUserDefaultParaPreferToServer = (defaultParaPrefer) => {
    return dispatch => {
        return $http.post('user', { defaultParaPrefer: defaultParaPrefer }).then(data => dispatch(updateUserInfo(data)))
    }
}

export const updateUserLocaleToServer = (locale) => {
    return dispatch => {
        return $http.post('user', { preferredLocale: locale }).then(data => {
            dispatch(updateUserInfoLocale(data))
            dispatch(switchLocal(locale))
        })
    }
}

function switchLocal(locale) {
    return {
        type: SWITCH_LOCALE,
        locale: locale
    }
}

function updateUserInfo(data) {
    return {
        type: UPLOAD_USER,
        user: data.data
    }
}
function updateUserInfoLocale(data) {
    return {
        type: UPLOAD_USER,
        user: data.data
    }
}

function receiveUserInfo(data) {
    return {
        type: RECEIVE_USERINFO,
        user: data.data
    }
}

export function fetchPriceBase() {
    return dispatch => {
        return $http.get('meta/price/versions').then(data => dispatch(receivedPriceBase(data)))
    }
}

function receivedPriceBase(data) {
    return {
        type: RECEIVE_PRICE_BASE,
        data: data.data
    }
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        let c_start = document.cookie.indexOf(c_name + "=")
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1
            let c_end = document.cookie.indexOf(";", c_start)
            if (c_end == -1) c_end = document.cookie.length
            return unescape(document.cookie.substring(c_start, c_end))
        }
    }
    return ""
}

export function installLicense(formData) {
    return dispatch => $.ajax({
        url: `${SERVICE_URL}license/install`,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.code == "0") {
                sweetalert({
                    title: intl.get(INSTALL_LICENSE_FILE_SUCCESS), type: 'success',
                    showCancelButton: false,
                    confirmButtonText: intl.get(OK),
                    closeOnConfirm: true
                }, isConfirm => {
                    window.location.href = "/";
                })
            } else {
                sweetalert({ title: data.msg, type: 'error',timer: 1000, showConfirmButton: false });
            }
        },
        error: function () {
            sweetalert({ title: intl.get(INSTALL_LICENSE_FILE_FAILED), type: 'error',timer: 1000, showConfirmButton: false });
        }
    });
}

export function upgrade(formData, finishUpgrade) {
    return dispatch => $.ajax({
        url: `${SERVICE_URL}upgrade`,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.code == "0") {
                sweetalert({
                    title: intl.get(INSTALL_UPGRADE_FILE_SUCCESS), type: 'success',
                    showCancelButton: false,
                    confirmButtonText: intl.get(OK),
                    closeOnConfirm: true
                })
            } else {
                sweetalert({ title: data.msg, type: 'error',timer: 1000, showConfirmButton: false });
            }
            finishUpgrade();
        },
        error: function (data) {
            let message = data.responseJSON ? data.responseJSON.message : intl.get(INSTALL_UPGRADE_FILE_FAILED);
            sweetalert({ title: message, type: 'error',timer: 1000, showConfirmButton: false });
            finishUpgrade();
        }
    });
}

export const DefaultUser = {

    "preferredLocale": "zh-CN",
    "unitPrefer": {
        "AirVolume3": "M",
        "AirVolume2": "M",
        "AirVolume1": "M",
        "HeatQuality": "M",
        "WaterVolume2": "M",
        "Pressure3": "M",
        "WaterVolume1": "M",
        "Pressure1": "M",
        "Pressure2": "M",
        "ColdQuality": "M",
        "Area": "M",
        "AirSpeed": "M",
        "Quality": "M",
        "Tempreture": "M",
        "Length1": "M",
        "Length2": "M",
        "Power": "M",
        "HumidVolume": "M",
        "WaterSpeed": "M"
    }
}

export const fetchUserMeta = () => {
    return dispatch => {
        return $http.get('user/meta').then(data => dispatch(getUserMeta(data)))
    }
}
function getUserMeta(data) {
    return {
        type: GET_USER_META,
        user: data.data.metaData
    }
}

export const updateUserMetau = (params, callback) => {
    return dispatch => {
        return $http.post('user/meta', params).then(data => {
            dispatch(updateUserMeta(params))
            callback()
        })
    }
}
function updateUserMeta(params) {
    return {
        type: UPLOAD_USER_META,
        params: params
    }
}


export const fetchEdition = () => {
    return dispatch => {
        return $http.get('help/isExport').then(data => {
            if(data.code == '0' && data.msg == 'Success'){
                dispatch(updateIsExport(data.data))
            }
            
        })
    }
}

function updateIsExport(params) {
    return {
        type: UPLOAD_IS_EXPORT,
        data: params
    }
}


export const fetchSysPort = () => {
    return dispatch => {
        return $http.get('sys/port').then(data => {
            if(data.code == '0' && data.msg == 'Success'){
                dispatch(updateSysPort(data.data))
            }
            
        })
    }
}
function updateSysPort(params) {
    return {
        type: UPDATESYSPORT,
        data: params
    }
}
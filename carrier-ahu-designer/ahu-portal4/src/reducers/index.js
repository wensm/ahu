import {combineReducers} from 'redux'
import {reducer as formReducer} from 'redux-form'
import {syncHistoryWithStore, routerReducer} from 'react-router-redux'

import todos from './todos'
import visibilityFilter from './visibilityFilter'
import account from './account'
import ahu from './ahu'
import metadata from './metadata'
import projects from './projects'
import groups from './groups'
import pro from './pro'
import partition from './partition'
import general from './general'
// console.log('formReducer', formReducer())
const rootReducer = combineReducers({
    routing: routerReducer,
    form: formReducer,
    todos,
    visibilityFilter,
    account,
    ahu,
    metadata,
    projects,
    groups,
    pro,
    partition,
    general
})

export default rootReducer

import {
  INIT_ACCOUNT
} from '../actions/account'

const account = (state = {
  firstName: 'Jane',
  lastName: 'Doe',
}, action) => {
  switch (action.type) {
    case INIT_ACCOUNT:
      return action.account
    default:
      return state
  }
}

export default account

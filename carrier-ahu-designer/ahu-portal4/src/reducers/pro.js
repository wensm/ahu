import {
  PRO_RECEIVE_PROJECTS,
  PRO_SET_CURRENT_PROJECT_ID,
} from '../actions/pro'

const pro = (state = {
  projects: [],
  projectId: null,
  _id: 0,
}, action) => {
  switch (action.type) {
    case PRO_SET_CURRENT_PROJECT_ID:
      return Object.assign({}, state, {
        projectId: action.projectId,
        _id: state._id + 1,
      })
    case PRO_RECEIVE_PROJECTS:
      return Object.assign({}, state, {
        projects: action.projects
      })
    default:
      return state
  }
}

export default pro

import {
    UPDATE_LANGUAGE,
    RECEIVE_USERINFO,
    UPDATE_UNIT,
    DefaultUser,
    SWITCH_LOCALE,
    RECEIVE_USER_ABILITY,
    updateUserLocaleToServer,
    GET_USER_META,
    UPLOAD_USER_META,
    RECEIVE_PRICE_BASE,
    UPLOAD_IS_EXPORT,
    UPDATESYSPORT
} from '../actions/general'
import ability from '../config/ability';
const general = (state = {
    user: DefaultUser,
    priceBase: [],
    isExport:false,
    fieldValues: {
        COLD_COOLING_SELECTION_SIZE: {
            value: '5'
        },
        EXPORT_MODE: {
            value: '1'
        },
        TEMPERATURE_TRANSMIT: {
            value: 'false'
        },
        CAL_COILFAN_LISTPRICE: {
            value: 'false'
        },

    },
    sysPort:'20000'
}, action) => {
    switch (action.type) {
        case UPDATE_LANGUAGE:
            let newUser = JSON.parse(JSON.stringify(state.user))
            newUser.preferredLocale = action.locale.locale
            return Object.assign({}, state, { user: newUser })
        case RECEIVE_USERINFO:
            return Object.assign({}, state, { user: action.user })
        case RECEIVE_USER_ABILITY:
            ability.update(action.userAbilities)
            return state
        case UPDATE_UNIT:
            newUser = JSON.parse(JSON.stringify(state.user))
            newUser.unitPrefer = action.unitPrefer
            newUser.unitPreferCode = action.unitPreferCode
            return Object.assign({}, state, { user: newUser })
        case SWITCH_LOCALE:
            newUser = JSON.parse(JSON.stringify(state.user))
            newUser.preferredLocale = action.locale
            document.cookie = `lang=${action.locale}`
            // location.search = `?lang=${action.locale}`;
            location.search = `?time=${(new Date()).getTime()}`;
            return Object.assign({}, state, { user: newUser })
        case GET_USER_META:
            let values = {...state.fieldValues}
            for (let key in action.user) {
                values[key] = {
                    value: action.user[key]
                }
            }
            return Object.assign({}, state, { fieldValues: values })
        case UPLOAD_USER_META:
            let values2 = {...state.fieldValues}
            for (let key in state.fieldValues) {
                values2[key] = {
                    value: action.params.metaData[key]
                }
            }
            return Object.assign({}, state, { fieldValues: values2 })
        case RECEIVE_PRICE_BASE:
            return Object.assign({}, state, { priceBase: action.data })
        case UPLOAD_IS_EXPORT:
            return Object.assign({}, state, { isExport: action.data })
        case UPDATESYSPORT:
            return Object.assign({}, state, { sysPort: action.data })
            
        default:
            return state
    }
}

export default general

import {
    UNCLASSIFIED,
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import {
    RECEIVE_GROUP,
    RECEIVE_AHU,
    RECEIVE_GROUP_TYPE_LIST,
    USEFUL_NUMBER,
    CHECK_USEFUL_NM_RES,
    CHECK_GROUP_NAME_RES,
    CAN_MOVE_LIST,
    RECEIVE_AHU_HAS_SEC,
    RECEIVE_GROUP_SEC_LIST,
    RECEIVE_GROUP_CALC_DATA,
    GROUPS_REPORTS,
    CLEAR_REPORTS,
    RECEIVE_UPDATE_GROUP,
    RECEIVE_UPDATE_AHU,
    CANCEL_EDIT_AHU,
    RECEIVE_PAGE_GROUP_FILTERUPDATE,
    RECEIVE_AHU_IN_GROUP_PAGE,
    TabId,
    SYNC_BATCH_VALUES,
    solveLinkage,
    RESET,
    CLEAN_USEFUL_NM,
    CLEANGROUPLIST,
    RECEIVE_AHULIST_FORREPORT,
    UPDATE_UNIT_SERIES
} from '../actions/groups'
const groups = (state = {
    groupList: [],
    ahuGroupList: [],
    ahuList: [],
    usefulNumber: {},
    checkGroupNameRes: {},
    checkUseFulNmRes: {},
    canMoveList: [],
    ahuHasSecList: [],
    groupSecList: [],
    reports: [],
    checkDrawingNoRes: {},
    updateGroup: {},
    updateAhu: {},
    ahuFilterStr: "",
    tabid: "",
    page: 1,
    isEditAhu:'',
    allAhuList:'',
    editSeries:[]
}, action) => {
    let value = null
    let componentValue = null
    switch (action.type) {
        case RECEIVE_GROUP:
            return Object.assign({}, state, { groupList: action.groupsList })
        case RECEIVE_AHU_IN_GROUP_PAGE:
            let newAhuGroupList = JSON.parse(JSON.stringify(state.ahuGroupList))
            var ahus = action.ahuLists
            var tmpGrpId = action.groupId
            let page = action.page
            for (var i = 0; i < newAhuGroupList.length; i++) {
                if (!newAhuGroupList[i].ahus) {
                    newAhuGroupList[i].ahus = []
                }
                if (tmpGrpId == newAhuGroupList[i].groupId) {
                    newAhuGroupList[i].ahus = ahus[0].content
                    Object.assign(newAhuGroupList[i], ahus[0])
                    break
                }
            }

            return Object.assign({}, state, { ahuGroupList: newAhuGroupList, page: page })
        case RECEIVE_AHU:
            var ahuGroups = state.groupList;
            var ahus = action.ahuLists || [];
            //console.log('action.ahuList :'+ahus.length);
            var ahuList = [];
            var ahuGroup = {};

            ahuGroup.groupName = intl.get(UNCLASSIFIED);
            ahuGroup.groupId = null;
            ahuGroup.ahuGroupCode = '';
            ahuGroups.unshift(ahuGroup);
            for (var i = 0; i < ahuGroups.length; i++) {
                ahuGroups[i].ahus = [];
            }
            for (var j = 0; j < ahus.length; j++) {

                ahuList = ahuList.concat(ahus[j].content || []);

                if (ahus[j].numberOfElements == 0) {
                    continue
                }

                let tmpGrpId = ahus[j].content[0].groupId;
                if (tmpGrpId == null || tmpGrpId == '') {
                    ahuGroup.ahus = ahus[j].content;
                    Object.assign(ahuGroup, ahus[j])
                    continue;
                }

                for (var i = 0; i < ahuGroups.length; i++) {
                    if (!ahuGroups[i].ahus) {
                        ahuGroups[i].ahus = [];
                    }
                    if (tmpGrpId == ahuGroups[i].groupId) {
                        ahuGroups[i].ahus = ahus[j].content;
                        Object.assign(ahuGroups[i], ahus[j])
                        break;
                    }
                }
            }

            ahuList.sort((a, b) => {
                let va = Number(a.drawingNo);
                if (va === Number.NaN) {
                    return 1;
                }
                let vb = Number(b.drawingNo);
                if (vb === Number.NaN) {
                    return -1;
                }
                return va - vb;
            });


            return Object.assign({}, state, { ahuGroupList: ahuGroups, ahuList: ahuList, page: action.page });
        case RECEIVE_GROUP_TYPE_LIST:
            return Object.assign({}, state, { groupTypeList: action.groupTypeList });
        case USEFUL_NUMBER:
            let tmpUpdateAhu = JSON.parse(JSON.stringify(state.updateAhu))
            if (!tmpUpdateAhu.unit) {
                tmpUpdateAhu.unit = {}
            }
            tmpUpdateAhu.unit.drawingNo = action.usefulNumber.drawingNo
            tmpUpdateAhu.unit.unitNo = action.usefulNumber.drawingNo
            return Object.assign({}, state, { updateAhu: tmpUpdateAhu });
        case CHECK_USEFUL_NM_RES:
            tmpUpdateAhu = JSON.parse(JSON.stringify(state.updateAhu))

            if (!tmpUpdateAhu.unit) {
                tmpUpdateAhu.unit = {}
            }
            // console.log(Object.assign(tmpUpdateAhu.unit,state.updateAhu.unit))
            if (state.updateAhu.unit) {
                Object.assign(tmpUpdateAhu.unit, state.updateAhu.unit)
            }
            if (action.checkUseFulNmRes.type == 0) {
                tmpUpdateAhu.unit.drawingNo = action.checkUseFulNmRes.drawingNo
                tmpUpdateAhu.unit.unitNo = action.checkUseFulNmRes.drawingNo
            }

            return Object.assign({}, state, { updateAhu: tmpUpdateAhu, checkUseFulNmRes: action.checkUseFulNmRes });
        case CLEAN_USEFUL_NM:
            return Object.assign({}, state, {  checkUseFulNmRes: {} });
        case CHECK_GROUP_NAME_RES:
            return Object.assign({}, state, { checkGroupNameRes: action.checkGroupNameRes });
        case CAN_MOVE_LIST:
            return Object.assign({}, state, { canMoveList: action.canMoveList });
        case RECEIVE_AHU_HAS_SEC:
            let basicGroupSecList = {}
            let groupSecList = {}
            action.ahuHasSecList.forEach(element => {
                let eParse = JSON.parse(element.metaJson)
                basicGroupSecList = { ...basicGroupSecList, ...eParse }
                groupSecList = { ...groupSecList, ...eParse }
            });
            let copygroupSecList = {}
            for (let key in groupSecList) {
                copygroupSecList[key.replace(/\./gi, '_')] = groupSecList[key]
            }
            let copyBasicGroupSecList = {}
            for (let key in basicGroupSecList) {
                copyBasicGroupSecList[key.replace(/\./gi, '_')] = basicGroupSecList[key]
            }
            return Object.assign({}, state, {
                basicGroupSecList: { 0: copyBasicGroupSecList },
                ahuHasSecList: action.ahuHasSecList,
                groupSecList: { 0: copygroupSecList }
            });
        case RECEIVE_GROUP_SEC_LIST:
            // let prokeyObj = JSON.parse(action.groupSecList);
            // let newProkeyObj = {
            //     0:{}
            // };
            // for (let variable in prokeyObj) {
            //     if (prokeyObj.hasOwnProperty(variable)) {
            //         newProkeyObj[0][variable.replace(/\./gi, '_')] = prokeyObj[variable];
            //     }
            // }
            // let newProkeyObj2 = {
            //     0:{}
            // };
            // for (let variable in prokeyObj) {
            //     if (prokeyObj.hasOwnProperty(variable)) {
            //         newProkeyObj2[0][variable.replace(/\./gi, '_')] = prokeyObj[variable];
            //     }
            // }
            return state
        // return Object.assign({}, state, {
        // basicGroupSecList: newProkeyObj2,
        // groupSecList: newProkeyObj
        // });
        case RECEIVE_GROUP_CALC_DATA:
            var calcData = action.calcData;
            return Object.assign({}, state, { calcData: calcData });
        case GROUPS_REPORTS:
            return Object.assign({}, state, { reports: action.reports });
        case CLEAR_REPORTS:
            return Object.assign({}, state, { reports: [] });
        case RECEIVE_UPDATE_GROUP:
            return Object.assign({}, state, { updateGroup: action.updateGroup });
        case RECEIVE_UPDATE_AHU:
            let tmpUserFulNumber = { "drawingNo": action.updateAhu.unit.drawingNo, "unitNo": action.updateAhu.unit.drawingNo }
            let copyUpdateAhu = action.updateAhu
            copyUpdateAhu.unit.unitNo = copyUpdateAhu.unit.drawingNo
            let isEditAhu = copyUpdateAhu.unit.unitNo
            return Object.assign({}, state, { updateAhu: copyUpdateAhu, usefulNumber: tmpUserFulNumber, isEditAhu });
        case CANCEL_EDIT_AHU:
            return Object.assign({}, state, { updateAhu: {}, usefulNumber: { "drawingNo": '', "unitNo": '' } });
        case RECEIVE_PAGE_GROUP_FILTERUPDATE:
            return Object.assign({}, state, { ahuFilterStr: action.filterStr });
        case TabId:
            return Object.assign({}, state, { tabid: action.id });
        case SYNC_BATCH_VALUES:
            // let name = action.name
            let currentValue = { ...state.groupSecList[0] }
            currentValue[action.name] = action.value
            let currentValue2 = {
                0:currentValue
            }
            let currentValue3 = {...currentValue2}
            // currentValue = solveLinkage(action.name, action.value, currentValue)
            return Object.assign({}, state, { groupSecList: currentValue3 });
        case RESET:
            return Object.assign({}, state, { updateGroup: {} });
        case CLEANGROUPLIST:
            return Object.assign({}, state, { ahuGroupList: {} });
        case RECEIVE_AHULIST_FORREPORT:

            return Object.assign({}, state, { allAhuList: action.data });
        case UPDATE_UNIT_SERIES:
            return Object.assign({}, state, { editSeries: action.arr });
            
        default:
            return state
    }
}

export default groups

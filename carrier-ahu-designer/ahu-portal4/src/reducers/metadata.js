import {
  RANGE,
  EQUALS,
  NO_EQUALS
} from '../pages/intl/i18n'

import {
  RECEIVE_METADATA_LAYOUT,
  RECEIVE_METADATA_UNIT,
  RECEIVE_METADATA_VALIDATION,
  RECEIVE_METADATA_VARIABLE_VALIDATION,
  RECEIVE_METADATA_SERIAL,
  RECEIVE_METADATA_SERIAL_CHANGE,
  GET_VERSION,
  SET_PRODUCT,
  SET_SERIAL
} from '../actions/metadata'
import { formatValueForUnit, confirmSection } from '../actions/ahu'

// import {
//     SYNC_AHU_COMPONENT_VALUES
// } from '../actions/ahu'
import { isEmpty } from "lodash";
import intl from 'react-intl-universal';

const metadata = (state = {
  layout: {},
  allLayout: {},
  metaUnit: {},
  metaValidations: {},
  metaWarns: {},
  serial: '',
  series: '39CQ',
  defaultCanDropList: [
    'ahu.mix',
    'ahu.discharge',
    'ahu.access',
    'ahu.combinedMixingChamber',
    'ahu.filter',
    'ahu.combinedFilter',
    'ahu.HEPAFilter',
    'ahu.electrostaticFilter',
    'ahu.coolingCoil',
    'ahu.directExpensionCoil',
    'ahu.heatingCoil',
    'ahu.steamCoil',
    'ahu.electricHeatingCoil',
    'ahu.fan',
    'ahu.ctr',
    'ahu.attenuator',
    'ahu.wetfilmHumidifier',
    'ahu.sprayHumidifier',
    'ahu.steamHumidifier',
    'ahu.electrodeHumidifier',
  ],
  canDropList: [],
  version: '',
  patchVersion: '',
  drawingNo:''
}, action) => {
  switch (action.type) {
    case GET_VERSION:
      let version = action.data.ahuVersion
      let patchVersion = action.data.ahuPatchVersion
      return Object.assign({}, state, { version, patchVersion })

    case RECEIVE_METADATA_LAYOUT:
      let allLayout = {}
      for (let layoutName in action.data) {
        let layout = {}

        for (let section in action.data[layoutName]) {
          if (action.data[layoutName].hasOwnProperty(section)) {
            action.data[layoutName][section].groups.forEach(group => {
              group.paras.forEach(para => {
                layout[para.key] = para
              })
            })
          }
        }
        allLayout[layoutName] = layout
      }
      let layoutCurrent = allLayout[state.series]

      return Object.assign({}, state, { allLayout, layout: layoutCurrent })
    case SET_PRODUCT:
      // return state
      return Object.assign({}, state, { series: action.product, layout: state.allLayout[action.product] })
    case SET_SERIAL:
      if (action.series == '') {
        return Object.assign({}, state, { canDropList: [], drawingNo: action.drawingNo, scrollPosition: action.scrollPosition  })
      }
      // return state
      let canDropListRule = state.allCanDropListRule[action.product]
      let seriesss = action.series
      seriesss = seriesss.substring(seriesss.length - 4)
      // seriesss = seriesss.substring(seriesss.length - 4);
      // // console.log('seriess',seriess)
      // let mins = '', maxs = '';

      // for (let serialName in action.data) {
      let canDropList1 = [...state.defaultCanDropList]
      // console.log('canDropListRule', canDropListRule, action, state)
      for (let key in canDropListRule) {
        let bool = false
        canDropListRule[key].forEach((element) => {
          let sery = element['serial'].split('-')
          mins = sery[0].substring(sery[0].length - 4)
          maxs = sery[1].substring(sery[1].length - 4)
          if (seriesss >= mins && seriesss <= maxs) {
            bool = true
          }
        })
        if (!bool) {
          let index = canDropList1.findIndex(item => {
            // console.log('item',item,key,item == key )
            return item == key
          })
          if (index != '-1') {
            canDropList1.splice(index, 1)
          }
        }
      }
      //   // console.log('canDropList',canDropList,serialName)
      //   allCanDropList[serialName] = canDropList
      // }


      // let canDropListCurrent = allCanDropList[state.series]
      return Object.assign({}, state, { canDropList: canDropList1, drawingNo: action.drawingNo, scrollPosition: action.scrollPosition })

    case RECEIVE_METADATA_UNIT:
      return Object.assign({}, state, {
        metaUnit: action.data.data.unitMeta,
        metaUnitSetting: action.data.data.unitSetting
      })
    case RECEIVE_METADATA_VALIDATION:
      let rules = action.data;
      let validations = {};
      let warns = {};
      let warnsAlarm = {};
      let cname = '';
      let { unitPrefer, metaUnit, componentValue } = action

      for (let name in rules) {

        for (let key in state.layout) {
          if (name == key) {
            cname = action.locale == 'en-US' ? state.layout[key]['ememo'] : state.layout[key]['memo'];
            // console.log('zzf15',action.baseSections[key], rname, name)
          }
        }


        validations[name] = [];
        warns[name] = [];
        warnsAlarm[name] = [];
        rules[name].forEach((config) => {
          // let b = formatValueForUnit(name, componentValue[key][name], unitPrefer, metaUnit, state.layout)//公制切成英制
          // let copyConfig = config
          // let copyConfig = state.layout[name]!='' ? :config
          let copyConfig = { ...config }
          if (action.unitPreferCode == 'B' && state.layout[name] && state.layout[name]['valueSource'] != '' && state.layout[name]['valueSource'].indexOf('.') == '-1') {
            // config.ruleValueSet[0].split('~')[0]
            let left = config.ruleValueSet[0].split('~')[0], right = config.ruleValueSet[0].split('~')[1] || ''
            // console.log('zzff', copyConfig, name, state.layout[name], left, right, unitPrefer['AirSpeed'])
            if (left != '') {
              left = formatValueForUnit(name, left, unitPrefer, metaUnit, state.layout)//公制切成英制
            }
            if (right != '') {
              right = formatValueForUnit(name, right, unitPrefer, metaUnit, state.layout)//公制切成英制
            }
            copyConfig.ruleValueSetB = [`${left}~${right}`]

            // console.log('zzff', copyConfig, name, state.layout[name])
          } else {
            copyConfig.ruleValueSetB = ''
          }


          let v = parseRule(name, cname, copyConfig, undefined, undefined, state.layout, action.locale, action.unitPreferCode, componentValue);
          if (v) {
            if (config.level && config.level === "error") {
              validations[name] = validations[name].concat(v);
              if (config.alarm && config.alarm == 'true') {
                warnsAlarm[name] = warnsAlarm[name].concat(config.alarm);
              }
            } else {
              warns[name] = warns[name].concat(v);
              if (config.alarm && config.alarm == 'true') {

                warnsAlarm[name] = warnsAlarm[name].concat(config.alarm);
              }
            }
          }
        })
      }
      //      console.log('zzflog', warnsAlarm)
      return Object.assign({}, state, {
        metaValidations: validations,
        metaWarns: warns,
        warnsAlarm: warnsAlarm,
        rules
      })
    case RECEIVE_METADATA_VARIABLE_VALIDATION:
      let variableValidations = {};
      let variableWarns = {};
      let rname = '';
      for (let name in state.rules) {
        for (let key in state.layout) {
          if (name == key) {
            rname = action.locale == 'en-US' ? state.layout[key]['ememo'] : state.layout[key]['memo'];
          }
        }

        variableValidations[name] = [];
        variableWarns[name] = [];
        Array.isArray(state.rules[name]) && state.rules[name].forEach((config) => {
          let v = parseRule(name, rname, config, action.ahuValues, action.selectedComponent, state.layout, action.locale);
          if (v) {
            if (config.level && config.level === "error") {
              variableValidations[name] = variableValidations[name].concat(v);
            } else {
              variableWarns[name] = variableWarns[name].concat(v);
            }
          }
        })


      }
      return Object.assign({}, state, {
        metaValidations: variableValidations,
        metaWarns: variableWarns,
      })
    case RECEIVE_METADATA_SERIAL:
      // console.log('action', action)
      let seriess = action.componentValue['0'] && action.componentValue['0']['meta_ahu_serial'] ? action.componentValue['0']['meta_ahu_serial'] : "39CQ0608"
      seriess = seriess.substring(seriess.length - 4);
      let product = action.componentValue['0'] && action.componentValue['0']['meta_ahu_product'] ? action.componentValue['0']['meta_ahu_product'] : "39CQ"
      // console.log('seriess',seriess)
      // let allCanDropList = {}
      let allCanDropListRule = action.data

      let mins = '', maxs = '';
      // for (let serialName in action.data) {
      let canDropList = [...state.defaultCanDropList]

      for (let key in action.data[product]) {
        let bool = false
        action.data[product][key].forEach((element) => {
          let sery = element['serial'].split('-')
          mins = sery[0].substring(sery[0].length - 4)
          maxs = sery[1].substring(sery[1].length - 4)
          if (seriess >= mins && seriess <= maxs) {
            bool = true
          }
        })
        if (!bool) {
          let index = canDropList.findIndex(item => {
            // console.log('item',item,key,item == key )
            return item == key
          })
          // console.log('bool',bool,key,serialName,index,canDropList)
          if (index != '-1') {
            canDropList.splice(index, 1)
          }
        }
      }
      // console.log('canDropList',canDropList,serialName)
      // allCanDropList[serialName] = canDropList
      // }
      // let canDropListCurrent = allCanDropList[state.series]
      return Object.assign({}, state, {
        serial: action.data[state.series],
        allSerial: action.data,
        canDropList: canDropList,
        allCanDropListRule: allCanDropListRule
      })
    case RECEIVE_METADATA_SERIAL_CHANGE:
      let series = action.data.substring(action.data.length - 4);
      let canDropListCopy = [...state.defaultCanDropList]
      let min = '', max = '';
      for (let key in state.serial) {
        let bool = false
        state.serial[key].forEach((element) => {
          let sery = element['serial'].split('-')
          min = sery[0].substring(sery[0].length - 4)
          max = sery[1].substring(sery[1].length - 4)
          if (series >= min && series <= max) {
            bool = true
          }
        })
        if (!bool) {
          let indexCopy = canDropListCopy.findIndex(item => item == key)
          if (indexCopy != '-1') {
            canDropListCopy.splice(indexCopy, 1)
          }
        }
      }
      return { ...state, canDropList: canDropListCopy }
    default:
      return state
  }
}
function getVariableName(name, layout, locale) {
  let language = locale == 'en-US' ? 'ememo' : 'memo';
  name = layout[name][language]
  // console.log('zzf1', name, layout, locale)
  return name
}

// function getVariableValue(key, ahuValues, selectedComponent) {
//   let ahuValuesClone = ahuValues ? { ...ahuValues['0'] } : {}
//   let currentKey = selectedComponent ? selectedComponent.name.split('.')[1] : ''
//   let currentSection = 1;
//   for (let ke in ahuValues) {
//     for (let k in ahuValues[ke]) {
//       if (k.indexOf('meta') != -1 && k.split('meta_section_')[1] && k.split('meta_section_')[1].split('_')[0] == currentKey) {
//         ahuValuesClone = { ...ahuValuesClone, ...ahuValues[ke] }
//         break;
//       }
//     }
//   }
//   return ahuValuesClone[key]
// }


function parseRule(key, name, config, ahuValues, selectedComponent, layout, locale, unitPreferCode, componentValue) {
  // console.log('key, name, config, ahuValues, selectedComponent, layout, locale, unitPreferCode', name, componentValue)
  if (config && config.rule && config.ruleValueSet && config.ruleValueSet.length) {
    switch (config.rule) {
      case "range":
        let list = [];
        let configArr = config.ruleValueSetB && unitPreferCode == 'B' ? config.ruleValueSetB : config.ruleValueSet
        configArr.forEach((rs) => {
          let rangeMsg = config.ruleMassage;
          let rangeMsgType = config.ruleMassage;
          let min = '', max = '', sets = [];
          if (rs.indexOf('~') != -1) {
            sets = rs.split('~');
            min = sets[0], max = sets[1];
          } else {
            sets = rs.split('-');
            min = sets[0], max = sets[1];

          }
          if (min || max) {
            let bool = false
            let rangeValid = (value, values) => {

              bool = false
              if (values == undefined) {
                bool = true
                return rangeMsg;

              }

              let obj = {
                min: min,
                max: max
              }
              let hasLeft = true, hasRight = true
              if ((obj.min + "").indexOf('meta') != -1) {//min是某个段的key值
                let cuValueMin = values ? values[sets[0]] : undefined
                // obj['min'] = !cuValueMin ? -30 : values[sets[0]]//只有温度才是最小-30，最大70
                obj['min'] = !cuValueMin ? '' : values[sets[0]]
              }
              if ((obj.max + "").indexOf('meta') != -1) {//max是某个段的key值
                let cuValueMax = values ? values[sets[1]] : undefined
                // obj['max'] = !cuValueMax ? 70 : values[sets[1]]//只有温度才是最小-30，最大70
                obj['max'] = !cuValueMax ? '' : values[sets[1]]
              }
              if ((obj['min'] != '' && value != '' && value < Number(obj['min'])) || (obj['max'] != '' && value != '' && value > Number(obj['max']))) {
                if (rangeMsgType === "i18n") {
                  rangeMsg = intl.get("rule_message." + key.replace(/\./g, '_'));
                } else {
                  let newrs = `${obj['min']}~${obj['max']}`
                  rangeMsg = [name, intl.get(RANGE), newrs].join(' ')
                }
                return rangeMsg;
              } else {
                rangeMsg = undefined
                return rangeMsg
              }
            };
            !bool && list.push(rangeValid);
          }
        });

        return list;
      case "equals":
        let equalsMsg = config.ruleMassage;
        if (equalsMsg) {
          if (equalsMsg === "default") {
            equalsMsg = [name, intl.get(EQUALS)].join(' ')
          } else if (equalsMsg === "i18n") {
            equalsMsg = [name, intl.get("rule_message." + key.replace(/\./g, '_'))].join(' ')
          }
        }
        // let equalsSelections = config.ruleValueSet;
        let resetArr = config.ruleValueSet[0].indexOf(',') == '-1' ? config.ruleValueSet : config.ruleValueSet[0].split(',')
        const equalsValid = value => {
          if (resetArr.indexOf(`${value}`) !== -1) {
            return equalsMsg;
          }
        };
        return [equalsValid];

      case "noEquals":
        let noEqualsMsg = config.ruleMassage;
        if (noEqualsMsg) {
          if (noEqualsMsg === "default") {
            noEqualsMsg = [name, intl.get(NO_EQUALS)].join(' ')
          } else if (noEqualsMsg === "i18n") {
            noEqualsMsg = [name, intl.get("rule_message." + key.replace(/\./g, '_'))].join(' ')
          }
        }
        let selections = config.ruleValueSet;
        const noEqualsValid = value => {
          if (selections.indexOf(`${value}`) == -1) {
            return noEqualsMsg;
          }
        };
        return [noEqualsValid];
      // case "!range":
      //   let listr = [];
      //   let configArrr = config.ruleValueSetB && unitPreferCode == 'B' ? config.ruleValueSetB : config.ruleValueSet
      //   configArrr.forEach((rs) => {
      //     let rangeMsg = config.ruleMassage;
      //     let rangeMsg0 = rangeMsg[0]
      //     let rangeMsg1 = rangeMsg[1]
      //     let min = '', max = '', sets = [];
      //     if (rs.indexOf('~') != -1) {
      //       sets = rs.split('~');
      //       min = sets[0], max = sets[1];
      //     } else {
      //       sets = rs.split('-');
      //       min = sets[0], max = sets[1];

      //     }
      //     if (min || max) {
      //       let bool = false
      //       let rangeValid = (value, values) => {

      //         bool = false
      //         if (values == undefined) {
      //           bool = true
      //           return rangeMsg;

      //         }
      //         let obj = {
      //           min: min,
      //           max: max
      //         }
      //         if ((obj.min + "").indexOf('meta') != -1) {//min是某个段的key值
      //           let cuValueMin = values ? values[sets[0]] : undefined
      //           obj['min'] = !cuValueMin ? -30 : values[sets[0]]
      //         }
      //         if ((obj.max + "").indexOf('meta') != -1) {//max是某个段的key值
      //           let cuValueMax = values ? values[sets[1]] : undefined
      //           obj['max'] = !cuValueMax ? 70 : values[sets[1]]
      //         }
      //         if (obj['min'] && value < Number(obj['min'])) {
      //           if (!rangeMsg0 || rangeMsg0 === "default") {
      //             let newrs = `${obj['min']}~${obj['max']}`
      //             rangeMsg0 = [name, intl.get(RANGE), newrs].join(' ')
      //           }
      //           if (rangeMsg0 === "i18n") {
      //             rangeMsg0 = intl.get("rule_message." + key.replace(/\./g, '_'));
      //           }
      //           return rangeMsg0;
      //         }
      //         if (obj['max'] && value > Number(obj['max'])) {
      //           if (!rangeMsg1 || rangeMsg1 === "default") {
      //             let newrs = `${obj['min']}~${obj['max']}`
      //             rangeMsg1 = [name, intl.get(RANGE), newrs].join(' ')
      //           }
      //           if (rangeMsg1 === "i18n") {
      //             rangeMsg1 = intl.get("rule_message." + key.replace(/\./g, '_'));
      //           }
      //           return rangeMsg1;
      //         }
      //       };
      //       !bool && listr.push(rangeValid);
      //     }
      //   });

      //   return listr;

    }


  }
}

export default metadata

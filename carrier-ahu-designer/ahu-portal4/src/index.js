/* eslint-disable */
import 'babel-polyfill'
import '../vendors/jquery/jquery.min'
import '../vendors/jquery/jquery.mousewheel.min'
import '../vendors/html2canvas/html2canvas.min'
import '../vendors/bootstrap/js/bootstrap.min'
import '../vendors/datepicker/js/datepicker.min'
import '../vendors/bootstrap-select/js/bootstrap-select.min'
import '../vendors/bootstrap-treeview/bootstrap-treeview.min'
import '../vendors/contextmenu/jquery.contextMenu.min'
// import '../vendors/numberPolyfill/number-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware } from 'redux'
import { Router, Route, hashHistory, IndexRoute, browserHistory} from 'react-router'
import { syncHistoryWithStore, routerMiddleware, push } from 'react-router-redux'
// import '../vendors/antd/antd.min.js'
import ability from './config/ability';

import style from './css/style'
import rootReducer from './reducers'
import AppContainer from './components/AppContainer'
import AppLogin from './components/AppLogin'
import AppUpgrade from './components/AppUpgrade'
import AppWorder from './components/AppWorder'
import ContactPage from './pages/ContactPage'
import AHUPageContainer from './pages/ahu/AHUPageContainer'
import PartitionPageContainer from './pages/partition/PartitionPageContainer'
import ProjectsContainer from './pages/projects/ProjectsContainer'
import Projects from './pages/projects/Projects'
import GroupsContainer from './pages/groups/GroupsContainer'
// import ProPageContainer from './pages/pro/ProPageContainer'
import CalcTreeContainer from './pages/tool/CalcTreeContainer'
import DefaultParameter from './pages/general/DefaultParameterContainer'
import ForTestContainer from './pages/ahu/ForTestContainer'
const loggerMiddleware = createLogger({
  predicate: (getState, action) => {
    if(!action.type) return false
    return !action.type.includes('@@')
  },
  collapsed: (getState, action, logEntry) => !logEntry.error,
})
const middleware = routerMiddleware(hashHistory)

const store = createStore(
  rootReducer,
  applyMiddleware(
    middleware,
    thunkMiddleware, // lets us dispatch() functions
    loggerMiddleware // neat middleware that logs actions
  )
)
window.reduxStore = store
const history = syncHistoryWithStore(hashHistory, store)
window.routerPush = browserHistory;//含有push方法，切换路由
window.middleware2= middleware;
//history.listen(location => console.log(location.pathname))
window.ability = ability

const Root = ({ store }) => (
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={AppContainer} >
        <IndexRoute component={ProjectsContainer} />
        <Route path="projects" component={ProjectsContainer} />
        {/* <Route path="pros" component={ProPageContainer} /> */}{/*没发现有啥用*/}
        <Route path="login" component={AppLogin} />
        <Route path="upgrade" component={AppUpgrade} />
        <Route path="Worder" component={AppWorder} />
        <Route path="tool" component={CalcTreeContainer} />
        <Route path="page2/:projectId" component={GroupsContainer} />
        {/* <Route path="page2/:projectId/:page" component={GroupsContainer} /> */}
        <Route path="projects/:projectId/ahus/:ahuId" component={AHUPageContainer} />
        <Route path="partition/projects/:projectId/ahus/:ahuId" component={PartitionPageContainer} />
        <Route path="panel/projects/:projectId/ahus/:ahuId" component={PartitionPageContainer} />
        <Route path="defaultParameter" component={DefaultParameter} />
        <Route path="forTest" component={ForTestContainer} />
      </Route>
    </Router>
  </Provider>
)

render(<Root store={store} />, document.getElementById('root'))

// console.log(PRODUCTION)
// console.log(SERVICE_URL)
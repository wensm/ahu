
const DateUtils = {

  format(date,format) {  
        var o = {  
            "M+": date.getMonth() + 1,  
            "d+": date.getDate(),  
            "h+": date.getHours(),  
            "m+": date.getMinutes(),  
            "s+": date.getSeconds(),  
            "q+": Math.floor((date.getMonth() + 3) / 3),  
            "S": date.getMilliseconds()  
        }  
        if (/(y+)/.test(format)) {  
            format = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));  
        }  
        for (var k in o) {  
            if (new RegExp("(" + k + ")").test(format)) {  
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));  
            }  
        }  
        return format;  
    },   
    getSmpFormatDate(date, isFull) {  
        var pattern = "";  
        if (isFull == true || isFull == undefined) {  
            pattern = "yyyy-MM-dd hh:mm:ss";  
        } else {  
            pattern = "yyyy-MM-dd";  
        }  
        return this.getFormatDate(date, pattern);  
    },
    getSmpFormatNowDate(isFull) {  
        return this.getSmpFormatDate(new Date(), isFull);  
    } ,
    getSmpFormatDateByLong(l, isFull) {  
        return this.getSmpFormatDate(new Date(l), isFull);  
    },
    getFormatDateByLong(l, pattern) {  
        return this.getFormatDate(new Date(l), pattern);  
    },   
    getFormatDate(date, pattern) {  
        if (date == undefined) {  
            return "";  
        }  
        if (pattern == undefined) {  
            pattern = "yyyy-MM-dd hh:mm:ss";  
        }  
        return this.format(date,pattern);  
    },
    getFormatDateByLongTime(date, pattern){
      if(date == null || date == ''){
        return "";
      }

      if (pattern == undefined) {  
            pattern = "yyyy-MM-dd hh:mm:ss";  
      }  
      return this.format(new Date(date),pattern);
    }

}

export default DateUtils;
import {connect} from 'react-redux'
import CalcTree from './CalcTree'


const mapStateToProps = (state, ownProps) => {

    const calcData = {
      "calType": "ECONOMICAL",
      "calcType": "ECONOMICAL",
      "children": [
        {
          "calType": "ECONOMICAL",
          "calcType": "ECONOMICAL",
          "children": [
            {
              "calType": "ECONOMICAL",
              "calcType": "ECONOMICAL",
              "children": [
                {
                  "children": [ ],
                  "name": "cal.temperature",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.length",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.resistance",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.price",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.weight",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                }
              ],
              "name": "ahu.mix",
              "success": true,
              "text": "PART",
              "value": 0
            },
            {
              "calType": "ECONOMICAL",
              "calcType": "ECONOMICAL",
              "children": [
                {
                  "children": [ ],
                  "name": "cal.filterarrangement",
                  "success": false,
                  "error": "这里发生了错误！",
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.length",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.resistance",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.price",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.weight",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                }
              ],
              "name": "ahu.filter",
              "success": true,
              "text": "PART",
              "value": 0
            },
            {
              "calType": "ECONOMICAL",
              "calcType": "ECONOMICAL",
              "children": [
                {
                  "children": [ ],
                  "name": "cal.coil",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.length",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.resistance",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.price",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.weight",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                }
              ],
              "name": "ahu.coolingCoil",
              "success": true,
              "text": "PART",
              "value": 0
            },
            {
              "calType": "ECONOMICAL",
              "calcType": "ECONOMICAL",
              "children": [
                {
                  "children": [ ],
                  "name": "cal.fan",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.length",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.resistance",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.price",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                },
                {
                  "children": [ ],
                  "name": "cal.weight",
                  "success": true,
                  "text": "ENGINE",
                  "value": 0
                }
              ],
              "name": "ahu.fan",
              "success": true,
              "text": "PART",
              "value": 0
            }
          ],
          "name": "基本型101",
          "success": false,
          "text": "AHU",
          "value": 0
        }
      ],
      "name": "基本型101",
      "success": false,
      "text": "ROOT",
      "value": 0
    }

    return {
      calcData: state.groups.calcData
    }
}

const mapDispatchToProps = dispatch => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CalcTree)

import {
    SUCCESS,
    INCOMPLETE,
    BACK,
    RETURN_PROJECT,
    NO_BATCH_SELECTION_RESULT
} from '../intl/i18n'
import {Link} from 'react-router'

import React from 'react'
import Header from '../ahu/Header'
import style from './CalcTree.css'
import intl from 'react-intl-universal'
import { Tree, Icon } from 'antd';
const TreeNode = Tree.TreeNode;

export default class CalcTree extends React.Component {
  constructor(props) {
    super(props);

    this.renderTreeNode = this.renderTreeNode.bind(this);
    this.renderTree = this.renderTree.bind(this);
  }

  componentDidMount() {


  }

  // itemInnerClick(e) {
  //   $(e.target).parent().siblings('.sub-nodes').toggle();
  //   $(e.target).children('.glyphicon').toggle();

  // }

  //   renderNode(d) {
  //     return (
  //       <div className="inner">
  //         <div className=" list-group-item" style={{cursor: "pointer"}} onClick={this.itemInnerClick}>
  //           <span className="glyphicon glyphicon-minus "></span>
  //           <span className="glyphicon glyphicon-plus " style={{display: 'none'}}></span>
  //           {d.text} <span className="pull-right"
  //                          style={{color: d.success ? "green" : "red"}}> {d.success ? intl.get(SUCCESS) : intl.get(INCOMPLETE)}</span>
  //         </div>
  //         {d.error && (
  //           <div className="error-tip list-group-item" style={{color: "red", marginLeft: "15px"}}>{d.error}</div>)}
  //       </div>
  //     )
  //   }

  //   renderTreeNode(d) {
  //     return d && (<div className=" tree-node ">
  //         {this.renderNode(d)}
  //         <div className="sub-nodes" style={{paddingLeft: "20px"}}>
  //           {
  //             d.children.map((sd) => <div>{this.renderTreeNode(sd)}</div>)
  //           }
  //         </div>
  //       </div>)

  // }



  render() {
    const { calcData } = this.props;

    // function transformData(rawData, level) {
    //   if (!rawData) {
    //     return;
    //   }
    //   let d = {};
    //   if (!level) {
    //     level = 0;
    //   }

    //   d.text = rawData.name;
    //   d.module = rawData.name;
    //   d.icon = "glyphicon glyphicon-stop";
    //   d.color = d.success ? "green" : "red";
    //   d.level = level;
    //   d.error = rawData.error;
    //   d.success = rawData.success;
    //   // d.collapsed = level >= 2;
    //   d.children = [];
    //   if (rawData.children && rawData.children.length) {
    //     rawData.children.forEach((sd) => {
    //       d.children.push(transformData(sd, level + 1))
    //     })
    //   }
    //   return d;
    // }


    // let treeData = transformData(calcData);
    return (
      <div data-name="CalcTree" style={{ minHeight: "calc(100vh - 160px)", padding: "30px 50px", width: "66%" }}>
        <div className="tree-content list-group">
          <div>
          <div>
            <i className = "iconfont icon-fanhui" onClick = {()=>{window.routerPush.goBack()}} style = {{color:'#1890ff',cursor:'pointer'}} >
              {intl.get(BACK)}
            </i>
            {/* <Link className="iconfont icon-fanhui"
                to="/">{intl.get(BACK)}</Link> */}
      
            </div>
            
          {this.renderTree(calcData)}
            </div>
        </div>
      </div>
    )
  }
  renderTree(calcData) {
    if(!calcData) return <div>
      {intl.get(NO_BATCH_SELECTION_RESULT)}
    </div>
   
    let title = calcData ? calcData.name : ''
    let a = ''
    // console.log('calcData', calcData)
    

    
    a = <div><Tree
      showLine
      defaultExpandAll
      showIcon
      defaultExpandedKeys={['0-0-0']}
    // onSelect={this.onSelect}
    >
    
      <TreeNode title={title} >
        {
          this.renderTreeNode(calcData)
        }
      </TreeNode>
    </Tree>
    </div>
    return a
  }
  renderTreeNode(calcData) {
    let arr = calcData.children.map((sd) => {
      let type = sd.success ? "check-circle-o" : "close-circle-o"
      let style = sd.success ? {color:'green'} : {color:'red'}
      return (<TreeNode icon={<Icon type={type} style={style} />} title={sd.name} >{this.renderTreeNode(sd)} </TreeNode>)

    })
    return arr
  }
}

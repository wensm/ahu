import React from 'react';
import classnames from 'classnames';

import style from './helloMessage.css';

import girl from '../images/girl.png';

export default class HelloMessage extends React.Component {
  render() {
    return (
      <div className={style.helloMessage}>
        <h1 className="animated infinite bounce">HelloMessage</h1>
        <i className="fa fa-imdb" style={{ fontSize: '160px' }} />
        <span className={classnames(style.bg, style.red)}>hello {this.props.data}</span>
        <img src={girl} alt="" />

        <button type="button" className="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
          Launch demo modal
        </button>

        <div className="modal fade" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="myModalLabel">Modal title</h4>
              </div>
              <div className="modal-body">
                ...
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Save changes</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

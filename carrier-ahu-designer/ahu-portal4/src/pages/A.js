import React from 'react'

export default class A extends React.Component {
  render() {
    return (
      <div data-name="A">
        A
      </div>
    )
  }
}

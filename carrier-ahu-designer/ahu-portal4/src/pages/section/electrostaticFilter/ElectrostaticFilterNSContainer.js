import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { confirmSection } from '../../../actions/ahu'
import ElectrostaticFilterNS from './ElectrostaticFilterNS'

const mapStateToProps = state => {
  return {
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ElectrostaticFilterNS)

import {
    FILTER_OPTION,
    RESISTANCE,
    FILTER_ARRANGEMENT,
    CONFIRM,
    SPECIFICATION,
    QUANTITY,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

import { required, maxLength5, minValue0 } from '../../ahu/Validate'

class ElectrostaticFilter extends React.Component {

    render() {
        const { isCompleted, onCompleteSection, componentValue, propUnit, unitSetting, metaUnit, metaLayout } = this.props

        let arrList = [];
        if (componentValue) {
            const arr = componentValue['meta_section_electrostaticFilter_FilterArrange']
            if (arr && arr != '')
                arrList = JSON.parse(arr);
        }

        // const bb = JSON.parse(arr)
        // console.log(arrList)
        // console.log(bb)
        return (
            <form data-name="ElectrostaticFilter">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <Group title={intl.get(FILTER_OPTION)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.fitetF" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.filterEfficiency" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.Supplier" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.filterType" />
                        </div>

                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.rimThickness" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.BaffleM" />
                        </div>
                    </Group>
                    <Group title={intl.get(RESISTANCE)} id="group4">
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.dinitialPD" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.deveragePD" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.dfinalPD" />
                        </div>
                    </Group>
                        
                    <Group title={intl.get(RESISTANCE)} id="group4">
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.Weight" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.electrostaticFilter.appendAirVolume" />
                        </div>
                        
                    </Group>
                    <Group title={intl.get(FILTER_ARRANGEMENT)} id="group6">
                        <div className="modal-body">
                            <div style={{ maxHeight: window.innerHeight - 220, overflow: 'auto' }}>
                                <table className="table table-hover table-bordered" style={{ width: '300px' }}>
                                    <thead>
                                        <tr>
                                            <th style={{ textAlign: 'center' }}>{intl.get(SPECIFICATION)}</th>
                                            <th style={{ textAlign: 'center' }}>{intl.get(QUANTITY)}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {arrList && arrList.map((line, index) => <ArrangementLine key={index} index={index} line={line} />)}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="col-lg-12 btnGroup" style={{ paddingBottom: '10px' }}>
                            <Button 
                            // type="button" className="btn btn-primary" 
                            disabled={!this.props.canConfirm || this.props.invalid} 
                            // style={{ marginLeft: '10px' }}
                            size='small' type="primary"
                            style={{ backgroundColor: '#337ab7' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>

                        </div>

                    </Group>
                </div>
            </form>
        )
    }
}

class ArrangementLine extends React.Component {

    render() {
        const { line, index, componentValue } = this.props
        return (
            <tr>
                <td>{line.option}</td>
                <td>{line.count}</td>
            </tr>
        )
    }
}


export default reduxForm({
    form: 'ElectrostaticFilter', // a unique identifier for this form
    enableReinitialize: true,
})(ElectrostaticFilter)

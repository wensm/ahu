import {
    COIL_INFORMATION,
    OPTION,
    WINTER_SLASH_SUMMER,
    WINTER,
    SUMMER,
    LIMITED_CONDITION,
    CALCULATION_CONDITION,
    INLET_WIND_TEMP_WINTER_PAREN,
    WATER_SIDE_PARAMETER_WINTER,
    CALCULATE_WINTER,
    INLET_WIND_TEMP_SUMMER,
    WATER_SIDE_PARAMETER_SUMMER,
    CALCULATE_SUMMER,
    WEIGHT_AND_SECTION_LENGTH,
    OPERATION,
    CONFIRM,
    HEATING,
    HEATING_PARAMETER_INPUT,
    COOLING_PARAMETER_INPUT,
    COIL_MEMO,
    INVALID,
    OPTIMALCALCULATION,
    ALLCALCULATION,
    CALCULATION_RESULT,
    LABEL_TOOLTIP
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required, maxLength15, minLength2, tooHot, tooCold } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip, Button, Radio, Modal, Spin} from 'antd';
import CalculateTable from '../../ahu/calculateTable'
import sweetalert from 'sweetalert'

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class HeatingCoil extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cal: 'false',
            visible: false
        }
    }
    render() {
        const {
            isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            onCalcCoolingCoil,
            unitSetting,
            metaUnit,
            metaLayout,
            propUnit,
            coolingCoilCalcs,
            unitPreferCode,
            onOkCoilCalc,
            tableLoading
        } = this.props
        const { cal } = this.state

        return (
            <form data-name="HeatingCoil">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    {/*盘管信息*/}
                    <Group title={intl.get(COIL_INFORMATION)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.tubeDiameter" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.rows" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.circuit" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.finDensity" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.finType" />
                        </div>
                    </Group>
                    {/*选项*/}
                    <Group title={intl.get(OPTION)} id="group2">
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.connections" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.pipe" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.waterCollection" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.coilFrameMaterial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.baffleMaterial" />
                        </div>
                        {/* <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.uTrap" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.uTrapNum" />
                        </div> */}
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.frostProtectionOption" />
                        </div>
                    </Group>

                    <div className="panel panel-default">
                        {/*<div className="panel-heading">*/}
                        {/*<h3 className="panel-title">{intl.get(WINTER_SLASH_SUMMER)}</h3>*/}
                        {/*</div>*/}
                        <div className="panel-body" style={{ padding: '15px 0' }}>
                            <div>
                                <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                                    <li role="presentation" className="active">
                                        <a href="#winter" aria-controls="winter" role="tab" data-toggle="tab">{intl.get(WINTER)}</a>
                                    </li>
                                    {/*<li role="presentation" >*/}
                                    {/*<a href="#summer" aria-controls="summer" role="tab" data-toggle="tab">{intl.get(SUMMER)}</a>*/}
                                    {/*</li>*/}
                                </ul>
                                <div className="tab-content">
                                    <Group title={intl.get(HEATING)} id="group21" >

                                        <div role="tabpanel" className="tab-pane active" id="winter">
                                            {/*制热*/}
                                            {/*制热参数输入*/}
                                            <Group title={intl.get(HEATING_PARAMETER_INPUT)} showLeftBorder={false}>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WenteringFluidTemperature" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WmaxWPD" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.Wcoolant" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.Wconcentration" />
                                                </div>
                                            </Group>
                                            {/*计算条件*/}
                                            <Group title={intl.get(CALCULATION_CONDITION)} showLeftBorder={false}>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WcalculationConditions" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WWTAScend" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WwaterFlow" />
                                                </div>
                                            </Group>
                                            {/*限定条件*/}
                                            <Group title={intl.get(LIMITED_CONDITION)} showLeftBorder={false}>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WqualifiedConditions" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WheatQ" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WleavingAirDB" />
                                                </div>
                                            </Group>

                                            <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} showLeftBorder={false}>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WInDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_heatingCoil_WInWetBulbT', newValue, 'meta_section_heatingCoil_WInRelativeT', 'meta_section_heatingCoil_WInDryBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field
                                                        id="meta.section.heatingCoil.WInWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_heatingCoil_WInDryBulbT', newValue, 'meta_section_heatingCoil_WInRelativeT', 'meta_section_heatingCoil_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field
                                                        id="meta.section.heatingCoil.WInRelativeT" maxValue={100}
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_heatingCoil_WInDryBulbT', newValue, 'meta_section_heatingCoil_WInWetBulbT', 'meta_section_heatingCoil_WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </Group>
                                            <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} showLeftBorder={false}>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WAmbientDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_heatingCoil_WAmbientWetBulbT', newValue, 'meta_section_heatingCoil_WAmbientRelativeT', 'meta_section_heatingCoil_WAmbientDryBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WAmbientWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_heatingCoil_WAmbientDryBulbT', newValue, 'meta_section_heatingCoil_WAmbientRelativeT', 'meta_section_heatingCoil_WAmbientWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WAmbientRelativeT" maxValue={100}
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_heatingCoil_WAmbientDryBulbT', newValue, 'meta_section_heatingCoil_WAmbientWetBulbT', 'meta_section_heatingCoil_WAmbientRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WSweatCheck" />
                                                </div>
                                            </Group>
                                            <Group title={intl.get(WATER_SIDE_PARAMETER_WINTER)} showLeftBorder={false}>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WOutDryBulbT"
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WOutWetBulbT" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WOutRelativeT" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WAirResistance" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WreturnHeatQ" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WreturnEnteringFluidTemperature" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WwaterResistance" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.WreturnWaterFlow" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.heatingCoil.Wfluidvelocity" />
                                                </div>
                                                <div className="col-lg-3" style={{ paddingTop: '25px' }}>
                                                    {/* <button
                                                    type="button"
                                                    className="btn btn-primary"
                                                    style={{width: '100%'}}
                                                    data-toggle="modal"
                                                    data-target="#coolingCoilModal"
                                                    onClick={() => onCalcCoolingCoil(this.props.componentValues, this.props.selectedComponent, "W", unitSetting, metaUnit, metaLayout, propUnit)}
                                                >
                                                    {intl.get(CALCULATE_WINTER)}
                                                </button> */}
                                                </div>
                                                <div className="col-lg-12">
                                                    {intl.get(COIL_MEMO).split('<_>')[0]}<br />
                                                    {intl.get(COIL_MEMO).split('<_>')[1]}<br />
                                                    {intl.get(COIL_MEMO).split('<_>')[2]}<br />&nbsp;&nbsp;&nbsp;&nbsp;
                                                {intl.get(COIL_MEMO).split('<_>')[3]}<br />
                                                    {intl.get(COIL_MEMO).split('<_>')[4]}
                                                </div>
                                            </Group>
                                        </div>
                                    </Group>

                                    {/*夏季工况*/}
                                    {/*<div role="tabpanel" className="tab-pane" id="summer">
                                        <div style={{ marginBottom: '5px' }}>
                                            <Field id="meta.section.heatingCoil.enableSummer" />
                                        </div>
                                        <Group title={intl.get(COOLING_PARAMETER_INPUT)} id="group13">
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SenteringFluidTemperature" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SmaxWPD" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.Scoolant" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.Sconcentration" />
                                            </div>
                                        </Group>

                                        <Group title={intl.get(LIMITED_CONDITION)} id="group13">
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SqualifiedConditions" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.ScoldQ" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SsensibleCapacity" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SleavingAirDB" />
                                            </div>
                                        </Group>

                                        <Group title={intl.get(CALCULATION_CONDITION)} id="group13">
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.ScalculationConditions" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SWTAScend" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SwaterFlow" />
                                            </div>
                                        </Group>


                                        <Group title={intl.get(INLET_WIND_TEMP_SUMMER)} id="group13">
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SInDryBulbT" validate={[required, tooCold, tooHot]}
                                                />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field
                                                    id="meta.section.heatingCoil.SInWetBulbT"
                                                    validate={[required, tooCold, tooHot]}
                                                    onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_heatingCoil_SInDryBulbT', newValue, 'meta_section_heatingCoil_SInRelativeT')}
                                                />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field
                                                    id="meta.section.heatingCoil.SInRelativeT"
                                                    maxValue={100}
                                                    validate={[required, tooCold, tooHot]}
                                                    onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_heatingCoil_SInDryBulbT', newValue, 'meta_section_heatingCoil_SInWetBulbT')}
                                                />
                                            </div>
                                        </Group>
                                        <Group title={intl.get(WATER_SIDE_PARAMETER_SUMMER)} id="group13">
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SOutDryBulbT"/>
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SOutWetBulbT" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SOutRelativeT" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SAirResistance" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SreturnColdQ" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SreturnEnteringFluidTemperature" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SwaterResistance" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.SreturnWaterFlow" />
                                            </div>
                                            <div className="col-lg-3">
                                                <Field id="meta.section.heatingCoil.Sfluidvelocity" />
                                            </div>
                                            <div className="col-lg-3" style={{ paddingTop: '25px' }}>
                                                <button
                                                    // disabled={!this.props.canConfirm ||this.props.invalid}
                                                    type="button"
                                                    className="btn btn-primary"
                                                    style={{ width: '100%' }}
                                                    data-toggle="modal"
                                                    data-target="#coolingCoilModal"
                                                    onClick={() => { onCalcCoolingCoil(this.props.componentValues, this.props.selectedComponent, "S", unitSetting, metaUnit, metaLayout, propUnit) }}
                                                >
                                                    {intl.get(CALCULATE_SUMMER)}
                                                </button>
                                            </div>
                                            <div className="col-lg-12">
                                                {intl.get(COIL_MEMO).split('<_>')[0]}<br/>
                                                {intl.get(COIL_MEMO).split('<_>')[1]}<br/>
                                                {intl.get(COIL_MEMO).split('<_>')[2]}<br/>&nbsp;&nbsp;&nbsp;&nbsp;
                                                {intl.get(COIL_MEMO).split('<_>')[3]}<br/>
                                                {intl.get(COIL_MEMO).split('<_>')[4]}
                                            </div>
                                        </Group>
                                    </div>*/}

                                </div>
                            </div>
                        </div>
                    </div>
                    <Group title={intl.get(WEIGHT_AND_SECTION_LENGTH)} id="group3">
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.Weight" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.appendAirVolume" />
                        </div>
                        {/*<div className="col-lg-3">
                            <Field id="meta.section.heatingCoil.runningWeight"/>
                        </div>*/}

                    </Group>

                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group3">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button size='small' type="primary" style={{ padding: '0', marginRight: '10px', borderColor: '#e6f7ff' }}>
                                <RadioGroup onChange={(e) => { this.setState({ cal: e.target.value }) }} defaultValue="false" buttonStyle="solid" size='small' type="primary" >
                                    <RadioButton value="false" >{intl.get(OPTIMALCALCULATION)}</RadioButton>
                                    <RadioButton value="true">{intl.get(ALLCALCULATION)}</RadioButton>
                                </RadioGroup>
                            </Button>
                            <Button
                                // type="button"
                                // className="btn btn-primary"
                                style={{ backgroundColor: '#337ab7' }}
                                size='small' type="primary"

                                // data-toggle="modal"
                                // data-target="#coolingCoilModal"
                                onClick={() => { this.setState({ visible: true }); onCalcCoolingCoil(this.props.componentValues, this.props.selectedComponent, "W", unitSetting, metaUnit, metaLayout, propUnit, cal) }}
                            >
                                {intl.get(CALCULATE_WINTER)}
                            </Button>
                            <Button
                                // type="button" className="btn btn-primary"
                                size='small' type="primary"

                                disabled={!this.props.canConfirm || this.props.invalid}
                                style={{ marginLeft: '10px', backgroundColor: '#337ab7' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>

                        </div>
                    </Group>
                </div>
                <Modal
                    title={intl.get(CALCULATION_RESULT)}
                    visible={this.state.visible}
                    onOk={() => { this.handleOk(unitPreferCode, unitSetting, metaUnit, metaLayout, onOkCoilCalc) }}
                    onCancel={this.handleCancel}
                    width='1200px'
                >
                    <Spin spinning={tableLoading}>

                        <CalculateTable ref='calculateHeatCoilTable' datas={coolingCoilCalcs} type='heatingCoil' />
                    </Spin>

                </Modal>
            </form>
        )
    }
    handleCancel = () => {
        this.setState({ visible: false })
    }
    handleOk = (unitPreferCode, unitSetting, metaUnit, metaLayout, onOkCoilCalc) => {
        let { selectedRowKeys, selectedRows } = this.refs.calculateHeatCoilTable.returnState()
        if (selectedRowKeys.length > 0 && selectedRows.length > 0) {

            if (selectedRows[0].ahriMessage) {
                sweetalert({
                    title: intl.get(LABEL_TOOLTIP),
                    text: intl.get(`label.${selectedRows[0].ahriMessage}`),
                    type: 'warning',
                    showConfirmButton: true
                })
            }
            onOkCoilCalc(selectedRows, unitPreferCode, unitSetting, metaUnit, metaLayout)
            this.setState({ visible: false })
        }
    }
}

export default reduxForm({
    form: 'HeatingCoil', // a unique identifier for this form
    enableReinitialize: true,
})(HeatingCoil)

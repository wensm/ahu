import {
    PARAMETER_INPUT,
    RESISTANCE_AND_WEIGHT,
    FILTER_ARRANGEMENT,
    CONFIRM,
    SPECIFICATION,
    QUANTITY,
    INVALID,
    PANEL_RESISTANCE
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Row, Col, Button } from 'antd';

import comb from '../../../images/put_icon8_1.png'
// import { Row } from 'antd/lib/grid';
const MAP = { comb }

class Filter extends React.Component {
    render() {
        const { isCompleted, onCompleteSection, componentValue, propUnit, unitSetting, metaUnit, metaLayout } = this.props
        let arrList = [componentValue];
        if (componentValue) {
            const arr = componentValue['meta_section_filter_FilterArrange']
            if (arr && arr != '')
                arrList = JSON.parse(arr);
        }
        let selectedComponentId = this.props.selectedComponent.id
        let element = $(`[data-section-id='${selectedComponentId}']`)[0]
        const section = $(element).attr('data-section')
        let hasCombine = false;
        let parent = $(`[data-section-id='${selectedComponentId}']`).parent().children('[data-section]')
        parent.each((index, record) => {
            if (record.getAttribute('data-section') == 'ahu.combinedMixingChamber') {
                hasCombine = true//找到新回排
            }
        })
        if ((componentValue.meta_section_doubleReturnAirDirection == "R" || componentValue.meta_section_airDirection == "R")  && hasCombine === false) {
            $(element).css("background-image", 'url(' + MAP.comb + ')')
        }
        return (
            <form data-name="Filter">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <Group title={intl.get(PARAMETER_INPUT)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.fitetF" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.fitlerStandard" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.filterOptions" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.filterEfficiency" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.rimThickness" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.mediaLoading" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.BracketM" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.BaffleM" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.pressureGuage" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.spareFilter" />
                        </div>
                    </Group>
                    <Group title={intl.get(RESISTANCE_AND_WEIGHT)} id="group5">
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.dinitialPD" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.deveragePD" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.dfinalPD" />
                        </div>
                    </Group>
                    <Group title={intl.get(PANEL_RESISTANCE)} id="group5">
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.Weight" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.filter.appendAirVolume" />
                        </div>
                    </Group>

                    <Group title={intl.get(FILTER_ARRANGEMENT)} id="group6">
                        <div className="modal-body">
                            <div style={{ maxHeight: window.innerHeight - 220, overflow: 'auto' }}>
                                <table className="table table-hover table-bordered" style={{ width: '300px' }}>
                                    <thead>
                                        <tr>
                                            <th style={{ textAlign: 'center' }}>{intl.get(SPECIFICATION)}</th>
                                            <th style={{ textAlign: 'center' }}>{intl.get(QUANTITY)}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {arrList && arrList.map((line, index) => <ArrangementLine key={index} index={index} line={line} />)}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="col-lg-12 btnGroup" style={{ paddingBottom: '10px' }}>
                            <Button 
                            // type="button" className="btn btn-primary"
                                disabled={!this.props.canConfirm || this.props.invalid}
                                // style={{  }}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>

                    </Group>

                </div>
            </form>
        )
    }
}
class ArrangementLine extends React.Component {

    render() {
        const { line, index, componentValue } = this.props
        return (
            <tr>
                <td>{line.option}</td>
                <td>{line.count}</td>
            </tr>
        )
    }
}

export default reduxForm({
    form: 'Filter', // a unique identifier for this form
    enableReinitialize: true,
})(Filter)

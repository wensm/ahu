import {
    OUTLET_AIR_FORM,
    AIR_OUTLET_CONNECTOR,
    OPTION,
    OPERATION,
    CONFIRM,
    INVALID,
    RESISTANCE_AND_WEIGHT
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required, maxLength5, minValue0, maxValue100 } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

import mix11 from '../../../images/pro_mix_t.png'
import mix12 from '../../../images/pro_mix_h.png'
import mix13 from '../../../images/pro_mix_r.png'
import mix14 from '../../../images/pro_mix_b.png'
import mix15 from '../../../images/pro_mix_l.png'
import mix16 from '../../../images/pro_mix_ll.png'
const MAP = {
    mix11, mix12, mix13, mix14, mix15, mix16
}

class Discharge extends React.Component {
    render() {
        const {
            isCompleted,
            islayout,
            isid,
            onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout,
            componentValue

        } = this.props
        const ahuDirection = this.props.componentValue
        let letmix
        let selectedComponentId = this.props.selectedComponent.id
        let element = $(`[data-section-id='${selectedComponentId}']`)[0]
        switch (ahuDirection.meta_section_discharge_OutletDirection) {
            case 'T':
                letmix = MAP.mix11
                break;
            case 'A':
                letmix = MAP.mix12
                if (ahuDirection.hasOwnProperty('meta_section_doubleReturnAirDirection')) {
                    if (ahuDirection.meta_section_doubleReturnAirDirection == 'R') {
                        letmix = MAP.mix16
                    }
                } else {
                    if (ahuDirection.meta_section_airDirection == "R") {
                        letmix = MAP.mix16
                    }
                }
                break;
            case 'L':
                letmix = MAP.mix15
                break;
            case 'R':
                letmix = MAP.mix13
                break;
            default:
                letmix = MAP.mix14
                break;
        }
        $(element).css("background-image", 'url(' + letmix + ')')
        // console.log('componentValue', componentValue)
        return (
            <form data-name="Discharge">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    {/* <Group title={intl.get(OUTLET_AIR_FORM)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.damperTopType" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.damperBackType" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.damperLeftType" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.damperRightType" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.damperBottomType" />
                        </div>
                    </Group> */}
                    {/* <Group title={intl.get(OUTLET_AIR_FORM)} id="group1">
                        <div className="col-lg-12" style={{ textAlign: 'center' }}>*/}
                    <div className="col-lg-12">
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.OutletDirection" />
                        </div>
                    </div>
                    {/*<div className="col-lg-2">
                                <div className="col-lg-12" style={{ textAlign: 'center' }}>

                                    A
                            </div>
                                <div className="col-lg-12" style={{ textAlign: 'center' }}>
                                    <Field id="meta.section.discharge.damperTopPosSizeA" />

                                </div>
                            </div>
                            <div className="col-lg-2">
                                B
                            </div>
                            <div className="col-lg-2">
                                C
                            </div>
                            <div className="col-lg-2">
                                D
                            </div>
                            <div className="col-lg-2">

                            </div>
                        </div>
                    </Group> */}

                    <Group title={intl.get(OPTION)} id="group2">
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.AInterface" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.DamperMeterial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.DamperExecutor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.ODoor" />
                        </div>


                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.DoorDirection" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.FixRepairLamp" />
                        </div>
                        <div className="col-lg-3">
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.PositivePressureDoor" />
                        </div>
                    </Group>
                    <Group title={intl.get(RESISTANCE_AND_WEIGHT)} id="group3">
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.AInterfaceR" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.appendAirVolume" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.Weight" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.discharge.sectionL" validate={[maxLength5, minValue0]} />
                        </div>



                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button
                                //  type="button" className="btn btn-primary"
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                disabled={!this.props.canConfirm || this.props.invalid}
                                // style={{  }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'Discharge', // a unique identifier for this form
    enableReinitialize: true,
})(Discharge)

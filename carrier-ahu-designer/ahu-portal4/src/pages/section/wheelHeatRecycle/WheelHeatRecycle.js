import {
    WINTER_SLASH_SUMMER,
    SUMMER,
    WINTER,
    INLET_WIND_TEMP_SUMMER_PAREN,
    INLET_WIND_TEMP_WINTER_PAREN,
    ATTACHMENT,
    OPTION,
    OPERATION,
    WHEEL_SIZE,
    SUPPLY_WIND_SIDE,
    EXHAUST_SIDE,
    HEAT_RECYCLE,
    DB_TEMPERATURE,
    WET_BULB_TEMPERATURE,
    RELATIVE_HUMIDITY,
    AIR_RESISTANCE,
    EFFICIENCY,
    TOTAL_HEATING,
    SHOW_HEAT,
    PRICE,
    PLATE_PRICE,
    CONFIRM,
    SEASON,
    INVALID,
    CALCULATION
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required } from '../../ahu/Validate'
import { Tooltip, Icon, Button, Radio } from 'antd'

class WheelHeatRecycle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form: false
        }
        this.onForm = this.onForm.bind(this)
    }
    onForm() {
        this.setState({
            form: true,
            value:0
        })
    }
    componentWillReceiveProps(nextProps) {
        /*if (nextProps.standard) {
            $("input[name='meta_section_wheelHeatRecycle_Resistance']").attr("disabled", "disabled");//禁用input标签
            $('input[name=meta_section_wheelHeatRecycle_Resistance]').attr("readonly", "readonly")//将input元素设置为readonly
        } else {
            $("input[name='meta_section_wheelHeatRecycle_Resistance']").attr("disabled", false);//禁用input标签
            $('input[name=meta_section_wheelHeatRecycle_Resistance]').removeAttr("readonly");//去除input元素的readonly属性

        }*/
    }
    onChange(e){
//        console.log('radio checked', e.target.value);
        this.setState({
          value: e.target.value,
        });
    }
    render() {
        const {
            isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            WheelHeatRecycleForm,
            componentValue, propUnit, unitSetting, metaUnit, metaLayout
        } = this.props
        let lines = []
        let demoLines = []
        if (componentValue && componentValue.meta_section_wheelHeatRecycle_HeatRecycleDetail != '') {
            if (typeof componentValue.meta_section_wheelHeatRecycle_HeatRecycleDetail === 'string') {

                lines = JSON.parse(componentValue.meta_section_wheelHeatRecycle_HeatRecycleDetail)
                console.log('1')
            } else {
                console.log('2')
                
                lines = componentValue.meta_section_wheelHeatRecycle_HeatRecycleDetail
            }
        }
        let enableWinter = componentValue ? eval(componentValue.meta_section_wheelHeatRecycle_EnableWinter) : false
        // demoLines = [lines, lines, lines]
        demoLines = lines 
        // demoLines[this.state.value]
        return (
            <form data-name="WheelHeatRecycle">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">{intl.get(WINTER_SLASH_SUMMER)}</h3>
                        </div>
                        <div className="panel-body" style={{ padding: '15px 0' }}>
                            <div>
                                <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                                    <li role="presentation" className="active"><a href="#summer" aria-controls="summer"
                                        role="tab" data-toggle="tab">{intl.get(SUMMER)}</a>
                                    </li>
                                    <li role="presentation"><a href="#winter" aria-controls="winter" role="tab"
                                        data-toggle="tab">{intl.get(WINTER)}</a></li>
                                </ul>
                                <div className="tab-content">
                                    <div role="tabpanel" className="tab-pane active" id="summer">
                                        <Group title={intl.get(INLET_WIND_TEMP_SUMMER_PAREN)} id="group1">
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.SNewDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_wheelHeatRecycle_SNewWetBulbT', newValue, 'meta_section_wheelHeatRecycle_SNewRelativeT', 'meta_section_wheelHeatRecycle_SNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.SNewWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_wheelHeatRecycle_SNewDryBulbT', newValue, 'meta_section_wheelHeatRecycle_SNewRelativeT', 'meta_section_wheelHeatRecycle_SNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.SNewRelativeT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_wheelHeatRecycle_SNewDryBulbT', newValue, 'meta_section_wheelHeatRecycle_SNewWetBulbT', 'meta_section_wheelHeatRecycle_SNewRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.SInDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_wheelHeatRecycle_SInWetBulbT', newValue, 'meta_section_wheelHeatRecycle_SInRelativeT', 'meta_section_wheelHeatRecycle_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.SInWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_wheelHeatRecycle_SInDryBulbT', newValue, 'meta_section_wheelHeatRecycle_SInRelativeT', 'meta_section_wheelHeatRecycle_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.SInRelativeT" maxValue={100}
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_wheelHeatRecycle_SInDryBulbT', newValue, 'meta_section_wheelHeatRecycle_SInWetBulbT', 'meta_section_wheelHeatRecycle_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.NAVolume" />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.RAVolume" />
                                                </div>
                                            </div>
                                        </Group>
                                    </div>
                                    <div role="tabpanel" className="tab-pane" id="winter">
                                        <div style={{ marginBottom: '5px' }}>
                                            <Field id="meta.section.wheelHeatRecycle.EnableWinter" />
                                        </div>
                                        {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} id="group4">
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.WNewDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_wheelHeatRecycle_WNewWetBulbT', newValue, 'meta_section_wheelHeatRecycle_WNewRelativeT', 'meta.section.wheelHeatRecycle.WNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.WNewWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_wheelHeatRecycle_WNewDryBulbT', newValue, 'meta_section_wheelHeatRecycle_WNewRelativeT', 'meta.section.wheelHeatRecycle.WNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.WNewRelativeT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_wheelHeatRecycle_WNewDryBulbT', newValue, 'meta_section_wheelHeatRecycle_WNewWetBulbT', 'meta.section.wheelHeatRecycle.WNewRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.WInDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_wheelHeatRecycle_WInWetBulbT', newValue, 'meta_section_wheelHeatRecycle_WInRelativeT', 'meta.section.wheelHeatRecycle.WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.WInWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_wheelHeatRecycle_WInDryBulbT', newValue, 'meta_section_wheelHeatRecycle_WInRelativeT', 'meta.section.wheelHeatRecycle.WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.WInRelativeT" maxValue={100}
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_wheelHeatRecycle_WInDryBulbT', newValue, 'meta_section_wheelHeatRecycle_WInWetBulbT', 'meta.section.wheelHeatRecycle.WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.NAVolume" />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.wheelHeatRecycle.RAVolume" />
                                                </div>
                                            </div>
                                        </Group> : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Group title={intl.get(ATTACHMENT)} id="group8">
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.Brand" />
                        </div>}
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.HeatRecoveryType" />
                        </div>}
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.EfficiencyType" />
                        </div>}
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.Model" />
                        </div>}
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.WheelDepth" />
                        </div>}
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.Regions" />
                        </div>}
                        <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.BracketM" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.Installation" />
                        </div>
                    </Group>
                    <Group title={intl.get(OPTION)} id="group7">
                        <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.HeatRecoveryEfficiency" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.sectionL" />
                        </div>
                        {this.props && this.props.standard && <div className="col-lg-3">
                            <Field id="meta.section.wheelHeatRecycle.Weight" />
                        </div>}
                    </Group>
                    {this.props && this.props.standard && <Group title={intl.get(OPERATION)} id="group10">
                        <Radio.Group onChange={(e)=>{this.onChange(e)}} value={this.state.value}>
                            {demoLines.map((lines, index) => {
                                return (

                                    lines.length > 0 && <div className="col-lg-12" style={{ padding: 0 }}>
                                        <div className="col-lg-1" style={{ padding: 0, textAlign:'center', lineHeight:'260px', maxWidth:'40px'}}>
                                            <Radio value={index}>
                                            </Radio>
                                        </div>
                                        <div className="col-lg-11" style={{ padding: 0 }}>
                                            <div className="modal-body" style={{padding:'0', marginRight:'-15px'}}>
                                                <div style={{ maxHeight: window.innerHeight - 220, overflow: 'auto' }}>
                                                    <table className="table table-hover table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th rowSpan="2"
                                                                    style={{ textAlign: 'center', paddingBottom: '26px' }}>{intl.get(WHEEL_SIZE)}</th>
                                                                <th rowSpan="2"
                                                                    style={{ textAlign: 'center', paddingBottom: '26px' }}>{intl.get(SEASON)}</th>
                                                                <th colSpan="5"
                                                                    style={{ textAlign: 'center' }}>{intl.get(SUPPLY_WIND_SIDE)}</th>
                                                                <th colSpan="5"
                                                                    style={{ textAlign: 'center' }}>{intl.get(EXHAUST_SIDE)}</th>
                                                                <th colSpan="2"
                                                                    style={{ textAlign: 'center' }}>{intl.get(HEAT_RECYCLE)} </th>
                                                                <th colSpan="2"
                                                                    style={{ textAlign: 'center' }}> </th>
                                                            </tr>
                                                            <tr>
                                                                <td> {intl.get(DB_TEMPERATURE)} </td>
                                                                <td> {intl.get(WET_BULB_TEMPERATURE)} </td>
                                                                <td> {intl.get(RELATIVE_HUMIDITY)} </td>
                                                                <td> {intl.get(AIR_RESISTANCE)} </td>
                                                                <td> {intl.get(EFFICIENCY)} </td>

                                                                <td> {intl.get(DB_TEMPERATURE)} </td>
                                                                <td> {intl.get(WET_BULB_TEMPERATURE)} </td>
                                                                <td> {intl.get(RELATIVE_HUMIDITY)} </td>
                                                                <td> {intl.get(AIR_RESISTANCE)} </td>
                                                                <td> {intl.get(EFFICIENCY)} </td>

                                                                <td> {intl.get(TOTAL_HEATING)} </td>
                                                                <td> {intl.get(SHOW_HEAT)} </td>
                                                                <td> {intl.get(PLATE_PRICE)} RMB(LP)</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>{intl.get('meta.moon_intl_str_1145')}</td>

                                                                <td></td>

                                                                <td>{intl.get('meta.moon_intl_str_1013')}</td>
                                                                <td>{intl.get('meta.moon_intl_str_1013')}</td>
                                                                <td>{intl.get('meta.moon_intl_str_1051')}</td>
                                                                <td>{intl.get('meta.moon_intl_str_1024')}</td>
                                                                <td>{intl.get('meta.moon_intl_str_1051')}</td>

                                                                <td>{intl.get('meta.moon_intl_str_1013')}</td>
                                                                <td>{intl.get('meta.moon_intl_str_1013')}</td>
                                                                <td>{intl.get('meta.moon_intl_str_1051')}</td>
                                                                <td>{intl.get('meta.moon_intl_str_1024')}</td>
                                                                <td>{intl.get('meta.moon_intl_str_1051')}</td>

                                                                <td>{intl.get('meta.moon_intl_str_1366')}</td>
                                                                <td>{intl.get('meta.moon_intl_str_1366')}</td>
                                                                <td></td>

                                                            </tr>
                                                            {lines && lines.map((line, index) => <WheelmentLine key={index} index={index} line={line} />)}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                )
                            })}
                        </Radio.Group>
                    </Group>
                    }

                    <Group title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12 btnGroup" style={{ paddingBottom: '10px' }}>
                            <Button disabled={!this.props.canConfirm || this.props.invalid}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => {
                                    onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout, !this.props.standard, true)
                                    this.onForm(this.props)
                                }
                                }>
                                {intl.get(CALCULATION)}
                            </Button>
                            <Button disabled={!this.props.canConfirm || this.props.invalid}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => {
                                    onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout, !this.props.standard, false, demoLines[this.state.value])
                                    this.onForm(this.props)
                                }
                                }>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

class WheelmentLine extends React.Component {

    render() {
        const { line, index } = this.props
        return (
            <tr>
                <td>
                    {line['returnWheelSize']}
                    {
                        line.largeSizeMessage ? <Tooltip placement="topLeft" title={line.largeSizeMessage}>
                            <Icon type="warning" style={{ fontSize: '18px', paddingLeft: '5px', color: '#cab410' }} />
                        </Tooltip> : ''
                    }
                </td>
                <td>{line['returnSeason']}</td>

                <td>{line['returnSDryBulbT']}</td>
                <td>{line['returnSWetBulbT']}</td>
                <td>{line['returnSRelativeT']}</td>
                <td>{line['returnSAirResistance']}</td>
                <td>{line['returnSEfficiency']}</td>

                <td>{line['returnEDryBulbT']}</td>
                <td>{line['returnEWetBulbT']}</td>
                <td>{line['returnERelativeT']}</td>
                <td>{line['returnEAirResistance']}</td>
                <td>{line['returnEEfficiency']}</td>

                <td>{line['returnTotalHeat']}</td>
                <td>{line['returnSensibleHeat']}</td>
                <td>{line['materialPrice']}</td>

            </tr>
        )
    }
}

export default reduxForm({
    form: 'WheelHeatRecycle', // a unique identifier for this form
    enableReinitialize: true,
})(WheelHeatRecycle)

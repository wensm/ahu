import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import {confirmSection, fetchAhuSeries, fetchRAVolume, fetchNAVolume} from '../../../actions/ahu'
import CombinedMixingChamber from './CombinedMixingChamber'

const mapStateToProps = state => {
  return {
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
    isCompleted: state.ahu.componentValue[state.ahu.selectedComponent.id] && state.ahu.componentValue[state.ahu.selectedComponent.id].meta_section_completed,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
   return {
       onCalcRAVolume: (aVolume,naVolume,name, self) => {
           dispatch(fetchRAVolume(aVolume,naVolume,name, self));
       },
       onCalcNAVolume: (aVolume,raVolume,name, self) => {
           dispatch(fetchNAVolume(aVolume,raVolume,name, self));
       }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CombinedMixingChamber)

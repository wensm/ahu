import {
    SUMMER_SLASH_WINTER,
    SUMMER,
    WINTER,
    INLET_WIND_TEMP_SUMMER_PAREN,
    INLET_WIND_TEMP_WINTER_PAREN,
    AIR_OUTLET_CONNECTOR_OPTION,
    PERFORMANCE_AND_WEIGHT,
    OPERATION,
    CONFIRM,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip, Button} from 'antd';

class CombinedMixingChamber extends React.Component {
    render() {
        const {
			isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            componentValues,
            onCalcRAVolume,
            componentValue,
            onCalcNAVolume, propUnit, unitSetting, metaUnit, metaLayout
        } = this.props
        let enableWinter = componentValue ? eval(componentValue.meta_section_combinedMixingChamber_EnableWinter) : false

        return (
            <form data-name="CombinedMixingChamber">
                <div className="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">{intl.get(SUMMER_SLASH_WINTER)}</h3>
                        </div>
                        <div className="panel-body" style={{ padding: '15px 0' }}>
                            <div>
                                <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                                    <li role="presentation" className="active"><a href="#summer" aria-controls="summer"
                                        role="tab" data-toggle="tab">{intl.get(SUMMER)}</a>
                                    </li>
                                    <li role="presentation"><a href="#winter" aria-controls="winter" role="tab"
                                        data-toggle="tab">{intl.get(WINTER)}</a></li>
                                </ul>
                                <div className="tab-content">
                                    <div role="tabpanel" className="tab-pane active" id="summer">
                                        <Group title={intl.get(INLET_WIND_TEMP_SUMMER_PAREN)} id="group1">
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SInDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_combinedMixingChamber_SInWetBulbT', newValue, 'meta_section_combinedMixingChamber_SInRelativeT', 'meta_section_combinedMixingChamber_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    
                                                         />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SInWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_combinedMixingChamber_SInDryBulbT', newValue, 'meta_section_combinedMixingChamber_SInRelativeT', 'meta_section_combinedMixingChamber_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SInRelativeT" maxValue={100}
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_combinedMixingChamber_SInDryBulbT', newValue, 'meta_section_combinedMixingChamber_SInWetBulbT', 'meta_section_combinedMixingChamber_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SNewDryBulbT" 
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_combinedMixingChamber_SNewWetBulbT', newValue, 'meta_section_combinedMixingChamber_SNewRelativeT', 'meta_section_combinedMixingChamber_SNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                        />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SNewWetBulbT"
                                                        maxValue={100}
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_combinedMixingChamber_SNewDryBulbT', newValue, 'meta_section_combinedMixingChamber_SNewRelativeT', 'meta_section_combinedMixingChamber_SNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SNewRelativeT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_combinedMixingChamber_SNewDryBulbT', newValue, 'meta_section_combinedMixingChamber_SNewWetBulbT', 'meta_section_combinedMixingChamber_SNewRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SNAVolume"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRAVolume(componentValues[0].meta_ahu_sairvolume, newValue, ['meta_section_combinedMixingChamber_SRAVolume', 'meta_section_combinedMixingChamber_WRAVolume'], ['meta_section_combinedMixingChamber_SNAVolume', 'meta_section_combinedMixingChamber_WNAVolume'])}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SRAVolume"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcNAVolume(componentValues[0].meta_ahu_sairvolume, newValue, ['meta_section_combinedMixingChamber_SNAVolume', 'meta_section_combinedMixingChamber_WNAVolume'], ['meta_section_combinedMixingChamber_SRAVolume', 'meta_section_combinedMixingChamber_WRAVolume'])}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SOutDryBulbT"  />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SOutWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_combinedMixingChamber_SOutDryBulbT', newValue, 'meta_section_combinedMixingChamber_SOutRelativeT', 'meta_section_combinedMixingChamber_SOutWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.SOutRelativeT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_combinedMixingChamber_SOutDryBulbT', newValue, 'meta_section_combinedMixingChamber_SOutWetBulbT', 'meta_section_combinedMixingChamber_SOutRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                        </Group>
                                    </div>
                                    <div role="tabpanel" className="tab-pane" id="winter">
                                        <div style={{ marginBottom: '5px' }}>
                                            <Field id="meta.section.combinedMixingChamber.EnableWinter" />
                                        </div>
                                        {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} id="group1">
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WInDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_combinedMixingChamber_WInWetBulbT', newValue, 'meta_section_combinedMixingChamber_WInRelativeT', 'meta_section_combinedMixingChamber_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WInWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_combinedMixingChamber_WInDryBulbT', newValue, 'meta_section_combinedMixingChamber_WInRelativeT', 'meta_section_combinedMixingChamber_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WInRelativeT" maxValue={100}
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_combinedMixingChamber_WInDryBulbT', newValue, 'meta_section_combinedMixingChamber_WInWetBulbT', 'meta_section_combinedMixingChamber_WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WNewDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_combinedMixingChamber_WNewWetBulbT', newValue, 'meta_section_combinedMixingChamber_WNewRelativeT', 'meta_section_combinedMixingChamber_WNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    
                                                     />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WNewWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_combinedMixingChamber_WNewDryBulbT', newValue, 'meta_section_combinedMixingChamber_WNewRelativeT', 'meta_section_combinedMixingChamber_WNewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WNewRelativeT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_combinedMixingChamber_WNewDryBulbT', newValue, 'meta_section_combinedMixingChamber_WNewWetBulbT', 'meta_section_combinedMixingChamber_WNewRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WNAVolume"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRAVolume(componentValues[0].meta_ahu_sairvolume, newValue, ['meta_section_combinedMixingChamber_WRAVolume', 'meta_section_combinedMixingChamber_SRAVolume'], ['meta_section_combinedMixingChamber_WNAVolume', 'meta_section_combinedMixingChamber_SNAVolume'])}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WRAVolume"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcNAVolume(componentValues[0].meta_ahu_sairvolume, newValue, ['meta_section_combinedMixingChamber_WNAVolume', 'meta_section_combinedMixingChamber_SNAVolume'], ['meta_section_combinedMixingChamber_WRAVolume', 'meta_section_combinedMixingChamber_SRAVolume'])}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-3">
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WOutDryBulbT" />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WOutWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_combinedMixingChamber_WOutDryBulbT', newValue, 'meta_section_combinedMixingChamber_WOutRelativeT', 'meta_section_combinedMixingChamber_WOutWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-12">
                                                    <Field id="meta.section.combinedMixingChamber.WOutRelativeT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_combinedMixingChamber_WOutDryBulbT', newValue, 'meta_section_combinedMixingChamber_WOutWetBulbT', 'meta_section_combinedMixingChamber_WOutRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </div>
                                        </Group>:''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Group title={intl.get(AIR_OUTLET_CONNECTOR_OPTION)} id="group7">
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.AInterface" />
                        </div>

                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.DamperMeterial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.Executor1" />
                        </div>
                        
                    </Group>
                    <Group title={intl.get('title.moon_intl_str_0343')} id="group7">
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.OSDoor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.ISDoor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.PositivePressureDoor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.FixRepairLamp" />
                        </div>
                    </Group>
                    <Group title={intl.get(PERFORMANCE_AND_WEIGHT)} id="group8">
                    <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.AInterfaceR" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.RResistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.IResistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedMixingChamber.Weight" />
                        </div>
                        
                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button 
                            size='small' type="primary"
                            style={{backgroundColor: '#337ab7'}}
                            // type="button" className="btn btn-primary"
                             disabled={!this.props.canConfirm || this.props.invalid}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>

                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'CombinedMixingChamber', // a unique identifier for this form
    enableReinitialize: true,
})(CombinedMixingChamber)

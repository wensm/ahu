import {
    SUMMER,
    WINTER,
    INLET_WIND_TEMP_SUMMER_PAREN,
    INLET_WIND_TEMP_WINTER_PAREN,
    HUMIDIFIER_OPTION,
    ATTACHMENT,
    PARAMETER,
    OPERATION,
    CONFIRM,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required, maxLength15, minValue0 } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

class WetfilmHumidifier extends React.Component {
    render() {
        const {
            isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            onCalIsoenthalpyJSBHumidificationVolume,
            componentValue,
            onCalIsoenthalpycJSBRelativeHumidity, propUnit, unitSetting, metaUnit, metaLayout

        } = this.props
        let enableWinter = componentValue ? eval(componentValue.meta_section_wetfilmHumidifier_EnableWinter) : false
        let enableSummer = componentValue ? eval(componentValue.meta_section_wetfilmHumidifier_EnableSummer) : false
        let bool = (!enableSummer && enableWinter) ? false : true

        return (
            <form data-name="WetfilmHumidifier">
                <div>
                    <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                        <li role="presentation" className={bool ? "active" : ""}><a href="#summer" aria-controls="summer"
                            role="tab"
                            data-toggle="tab">{intl.get(SUMMER)}</a>
                        </li>
                        <li role="presentation" className={bool ? "" : "active"}><a href="#winter" aria-controls="winter" role="tab"
                            data-toggle="tab">{intl.get(WINTER)}</a>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <div role="tabpanel" className={bool ? "tab-pane active" : "tab-pane "} id="summer">
                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.wetfilmHumidifier.EnableSummer" />
                            </div>
                            {enableSummer ? <Group title={intl.get(INLET_WIND_TEMP_SUMMER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.wetfilmHumidifier.SInDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_wetfilmHumidifier_SInWetBulbT', newValue, 'meta_section_wetfilmHumidifier_SInRelativeT', 'meta_section_wetfilmHumidifier_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.wetfilmHumidifier.SInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_wetfilmHumidifier_SInDryBulbT', newValue, 'meta_section_wetfilmHumidifier_SInRelativeT', 'meta_section_wetfilmHumidifier_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.wetfilmHumidifier.SInRelativeT" maxValue={100}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_wetfilmHumidifier_SInDryBulbT', newValue, 'meta_section_wetfilmHumidifier_SInWetBulbT', 'meta_section_wetfilmHumidifier_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.wetfilmHumidifier.SHumidificationQ"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsoenthalpycJSBRelativeHumidity(
                                                'meta_section_wetfilmHumidifier_SInDryBulbT',
                                                'meta_section_wetfilmHumidifier_SInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_wetfilmHumidifier_SOutDryBulbT',
                                                    'meta_section_wetfilmHumidifier_SOutWetBulbT',
                                                    'meta_section_wetfilmHumidifier_SOutRelativeT',
                                                    'meta_section_wetfilmHumidifier_SJSBRelativeT',
                                                    'meta_section_wetfilmHumidifier_SHumidificationQ',
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outRelativeT', 'humidificationQ'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.wetfilmHumidifier.SJSBRelativeT"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsoenthalpyJSBHumidificationVolume(
                                                'meta_section_wetfilmHumidifier_SInDryBulbT',
                                                'meta_section_wetfilmHumidifier_SInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_wetfilmHumidifier_SOutDryBulbT',
                                                    'meta_section_wetfilmHumidifier_SOutWetBulbT',
                                                    'meta_section_wetfilmHumidifier_SOutRelativeT',
                                                    'meta_section_wetfilmHumidifier_SHumidificationQ',
                                                    'meta_section_wetfilmHumidifier_SJSBRelativeT',
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humidificationQ', 'outRelativeT'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>

                                </div>

                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.wetfilmHumidifier.SOutDryBulbT" />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.wetfilmHumidifier.SOutWetBulbT"

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.wetfilmHumidifier.SOutRelativeT"

                                        />
                                    </div>
                                </div>
                            </Group> : ''}
                        </div>
                        <div role="tabpanel" className={bool ? "tab-pane" : "tab-pane active"} id="winter">
                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.wetfilmHumidifier.EnableWinter" />
                            </div>
                            {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.wetfilmHumidifier.WInDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_wetfilmHumidifier_WInWetBulbT', newValue, 'meta_section_wetfilmHumidifier_WInRelativeT', 'meta_section_wetfilmHumidifier_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.wetfilmHumidifier.WInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_wetfilmHumidifier_WInDryBulbT', newValue, 'meta_section_wetfilmHumidifier_WInRelativeT', 'meta_section_wetfilmHumidifier_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.wetfilmHumidifier.WInRelativeT" maxValue={100}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_wetfilmHumidifier_WInDryBulbT', newValue, 'meta_section_wetfilmHumidifier_WInWetBulbT', 'meta_section_wetfilmHumidifier_WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.wetfilmHumidifier.WHumidificationQ"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsoenthalpycJSBRelativeHumidity(
                                                'meta_section_wetfilmHumidifier_WInDryBulbT',
                                                'meta_section_wetfilmHumidifier_WInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_wetfilmHumidifier_WOutDryBulbT',
                                                    'meta_section_wetfilmHumidifier_WOutWetBulbT',
                                                    'meta_section_wetfilmHumidifier_WOutRelativeT',
                                                    'meta_section_wetfilmHumidifier_WJSBRelativeT',
                                                    'meta_section_wetfilmHumidifier_WHumidificationQ'
                                                ],//zzf 下面2个outRelativeT
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'outRelativeT', 'humidificationQ'], unitSetting, metaUnit, metaLayout, propUnit)}

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field id="meta.section.wetfilmHumidifier.WJSBRelativeT"
                                            onBlurChange={(event, newValue, previousValue) => onCalIsoenthalpyJSBHumidificationVolume(
                                                'meta_section_wetfilmHumidifier_WInDryBulbT',
                                                'meta_section_wetfilmHumidifier_WInWetBulbT',
                                                newValue,
                                                'meta_ahu_eairvolume',
                                                ['meta_section_wetfilmHumidifier_WOutDryBulbT',
                                                    'meta_section_wetfilmHumidifier_WOutWetBulbT',
                                                    'meta_section_wetfilmHumidifier_WOutRelativeT',
                                                    'meta_section_wetfilmHumidifier_WHumidificationQ',
                                                    'meta_section_wetfilmHumidifier_WJSBRelativeT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humidificationQ', 'outRelativeT'], unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.wetfilmHumidifier.WOutDryBulbT"
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.wetfilmHumidifier.WOutWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_wetfilmHumidifier_WOutDryBulbT', newValue, 'meta_section_wetfilmHumidifier_WOutRelativeT', 'meta_section_wetfilmHumidifier_WOutWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.wetfilmHumidifier.WOutRelativeT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_wetfilmHumidifier_WOutDryBulbT', newValue, 'meta_section_wetfilmHumidifier_WOutWetBulbT', 'meta_section_wetfilmHumidifier_WOutRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                            </Group> : ''}

                        </div>
                    </div>
                    <Group title={intl.get(HUMIDIFIER_OPTION)} id="group7">
                        <div className="col-lg-3">
                            <div className="col-lg-12">
                                <Field id="meta.section.wetfilmHumidifier.supplier" />
                            </div>
                            <div className="col-lg-12">
                                <Field id="meta.section.wetfilmHumidifier.wetfilmMaterial" />
                            </div>
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.wetfilmHumidifier.thickness" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.wetfilmHumidifier.material" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.wetfilmHumidifier.WaterQuantity" />
                        </div>
                    </Group>
                    {/* <Group title={intl.get(ATTACHMENT)} id="group7"> */}

                    {/* </Group> */}
                    <Group title={intl.get(PARAMETER)} id="group0">
                        <div className="col-lg-3">
                            <Field id="meta.section.wetfilmHumidifier.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.wetfilmHumidifier.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.wetfilmHumidifier.runningWeight" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.wetfilmHumidifier.Weight" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.wetfilmHumidifier.appendAirVolume" />
                        </div>
                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button
                                type="button" className="btn btn-primary"
                                disabled={!this.props.canConfirm || this.props.invalid}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
    form: 'WetfilmHumidifier', // a unique identifier for this form
    enableReinitialize: true,
})(WetfilmHumidifier)

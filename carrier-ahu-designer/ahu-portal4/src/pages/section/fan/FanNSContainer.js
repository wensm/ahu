import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import { confirmSection, calcComponentValue } from '../../../actions/ahu'
import FanNS from './FanNS'

const mapStateToProps = state => {
  return {
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FanNS)

import {
    FAN_OPTION,
    FAN_PARAMETER,
    ATTACHMENT,
    RESISTANCE_AND_WEIGHT,
    FAN_PERFORMANCE_AND_WEIGHT,
    FAN_CURVE,
    OPERATION,
    CALCULATION,
    CONFIRM,
    INVALID
} from '../../intl/i18n'
import { STYLE_PLATE, STYLE_WHELE } from "../../../actions/ahu"
import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

import fan1 from '../../../images/pro_fan_l1.png'
import fan2 from '../../../images/pro_fan_r2.png'
import fan3 from '../../../images/pro_fan_t1.png'
import fan4 from '../../../images/pro_fan_r1.png'
import fan5 from '../../../images/pro_fan_t2.png'
import fan6 from '../../../images/pro_fan_t3.png'
import plugfan from '../../../images/pro_plugfan.png'
import plugfanL from '../../../images/pro_plugfan_l.png'

const MAP = {
    fan1, fan2, fan3, fan4, fan5, fan6, plugfan, plugfanL
}

class Fan extends React.Component {
    render() {
        const { isCompleted, onCalcFan, onCompleteSection, templateId, formValues, initialValues, componentValue, fanCalcs, propUnit, unitSetting, metaUnit, metaLayout } = this.props
        let fanair = initialValues ? initialValues.meta_section_airDirection : 'S'
        let selectedComponentId = this.props.selectedComponent.id
        let element = $(`[data-section-id='${selectedComponentId}']`)[0]
        const section = $(element).attr('data-section')
        let hasCombine = false;
        let parent = $(`[data-section-id='${selectedComponentId}']`).parent().children('[data-section]')
        parent.each((index, record) => {
            if (record.getAttribute('data-section') == 'ahu.combinedMixingChamber') {
                hasCombine = true//找到新回排
            }
        })

        let letmix
        if (section == 'ahu.fan') {
            switch (componentValue.meta_section_fan_outletDirection) {
                case 'THF':
                    if ((componentValue.meta_section_doubleReturnAirDirection=="R" || componentValue.meta_section_airDirection == "R") && !hasCombine) {
                        letmix = MAP.fan5
                    } else {
                        letmix = MAP.fan1
                    }
                    break;
                case 'BHF':
                    if (componentValue.meta_section_doubleReturnAirDirection=="R" || componentValue.meta_section_airDirection == "R") {
                        letmix = MAP.fan6
                    } else {
                        letmix = MAP.fan2

                    }
                    break;
                case 'UBF':
                    if (componentValue.meta_section_airDirection == "R") {
                        letmix = MAP.fan4
                    } else {
                        letmix = MAP.fan3

                    }
                    break;
                case 'UBR':
                    if (componentValue.meta_section_airDirection == "R") {
                        letmix = MAP.fan3
                    } else {
                        letmix = MAP.fan4
                    }
                    break;
                default:
                    letmix = MAP.fan1
                    break;
            }
            if(componentValue.meta_section_fan_outlet == "wwk") {
                if((this.props.layout.style == STYLE_PLATE || this.props.layout.style == STYLE_WHELE)
                        && (componentValue.meta_section_airDirection == "R")) {
                    letmix = MAP.plugfanL
                } else {
                    letmix = MAP.plugfan
                }
            }
            $(element).css("background-image", 'url(' + letmix + ')')
        }
        return (
            <form data-name="fan">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <Group title={intl.get(FAN_OPTION)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.outletDirection" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.fanSupplier" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.outlet" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.power" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.type" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.supplier" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.pole" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.insulation" />
                        </div>
                    </Group>
                    <Group title={intl.get(FAN_PARAMETER)} id="group3">
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.airVolume" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.sysStatic" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.externalStatic" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.extraStatic" componentValue={componentValue} />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.totalStatic" />
                        </div>
                    </Group>
                    <Group title={intl.get(ATTACHMENT)} id="group5">
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.accessDoor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.motorPosition" />
                        </div>
                        {fanair == "S" ?
                            <div className="col-lg-3">
                                <Field id="meta.section.fan.sendPosition" />
                            </div> :
                            <div className="col-lg-3">
                                <Field id="meta.section.fan.returnPosition" />
                            </div>}
                        {fanair == "S" ?
                            <div className="col-lg-3">
                                <Field id="meta.section.fan.supplyDamper" />
                            </div> :
                            <div className="col-lg-3">
                                <Field id="meta.section.fan.returnDamper" />
                            </div>}
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.PositivePressureDoor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.seismicPringIsolator" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.fixRepairLamp" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.beltGuard" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.doorOnBothSide" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.standbyMotor" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.para13" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.ptc" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.startStyle" />
                        </div>
                    </Group>
                    <Group title={intl.get(RESISTANCE_AND_WEIGHT)} id="group5">
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.sectionL" disabled={true} />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.Weight" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.fan.appendAirVolume" />
                        </div>
                    </Group>

                    <Group title={intl.get(FAN_PERFORMANCE_AND_WEIGHT)} id="group6">
                        <div className="col-lg-5">
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.fanModel" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.outletVelocity" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.efficiency" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.absorbedPower" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.RPM" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.noise" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.motorBaseNo" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.motorPower" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.pole" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.motorEff" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.totalPressure" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.maxAbsorbedPower" />
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.maxRPM" />
                            </div>
                            <div className="col-lg-6">
                                 <Field id="meta.section.fan.frequencyRange"/>
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.hz"/>
                            </div>
                            <div className="col-lg-6">
                                <Field id="meta.section.fan.motorEffPole"/>
                            </div>
                        </div>
                        <div className="col-md-offset col-lg-5">
                            <h5>{intl.get(FAN_CURVE)}</h5>
                            {componentValue && componentValue["meta_section_fan_curve"] && componentValue["meta_section_fan_curve"] != '' &&
                                <div>
                                    <img className="img-responsive" src={componentValue && componentValue["meta_section_fan_curve"]} style={{ width: '100%' }} />
                                </div>
                            }
                        </div>
                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button
                                // disabled={!this.props.canConfirm ||this.props.invalid}
                                // type="button" className="btn btn-primary"
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7' }}
                                data-toggle="modal" data-target="#fanCalcModal"
                                onClick={() => onCalcFan()}
                            >
                                {intl.get(CALCULATION)}
                            </Button>
                            <Button
                                disabled={!this.props.canConfirm || this.props.invalid}
                                // type="button"
                                // className="btn btn-primary"
                                // style={{marginLeft:'10px'}}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => { onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout) }}
                            >
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle"  style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

export default reduxForm({
  form: 'Fan',
  enableReinitialize: true,
})(Fan)

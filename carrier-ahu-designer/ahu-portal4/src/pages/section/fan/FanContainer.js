import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { confirmSection, calcComponentValue, deleComponentValue, calculateUnitValueToMetric2 } from '../../../actions/ahu'
import Fan from './Fan'

const mapStateToProps = state => {
  return {
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
    templateId: state.ahu.templateId,
    isCompleted: state.ahu.componentValue[state.ahu.selectedComponent.id] && state.ahu.componentValue[state.ahu.selectedComponent.id].meta_section_completed,
    fanCalcs : state.ahu.fanCalcs
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onCalcFan: () => {
      let {metaLayout, unitSetting, metaUnit, propUnit} = ownProps
      let copythePropsValues = {...ownProps.componentValues}
            // console.log('zzf222',  projectId, ahuId, componentValue, sectionsNum, layout, bool = true, unitSetting, metaUnit, metaLayout, propUnit)
            if(propUnit == 'B'){//B 是 英制,需要切到公制传给后端
                for(let key in copythePropsValues){
                    let currentSave = {...copythePropsValues[key]}
                    for(let _key in currentSave){
                        if(metaLayout[_key.replace(/\_/gi, '.')] && (metaLayout[_key.replace(/\_/gi, '.')]['valueType']=='DoubleV' || metaLayout[_key.replace(/\_/gi, '.')]['valueType']=='IntV')){
                            let a = calculateUnitValueToMetric2(_key, currentSave[_key],  unitSetting['B'], metaUnit, metaLayout)
                            // console.log('zzf5',  metaLayout[_key.replace(/\_/gi, '.')]['memo'] , a)
                            currentSave[_key] = a
                        }
                    }
                    copythePropsValues[key] = currentSave 
                }
            }
            // console.log('zzff', ownProps , copythePropsValues, ownProps.componentValues)

      const ahu = copythePropsValues[0]
      const newAhu = {}
      for (var variable in ahu) {
        if (ahu.hasOwnProperty(variable)) {
          newAhu[variable.replace(/_/gi, '.')] = ahu[variable]
        }
      }
      const ahuStr = JSON.stringify(newAhu)
      const sections = []
      let position = null


      $('.ahu-section-list').children().each((index, element) => {
        const sectionId = $(element).attr('data-section-id')
        const sectionProps =  copythePropsValues[sectionId]
        if (Number(sectionId) === Number(ownProps.selectedComponent.id)) {
          position = index + 1
        }
        const newSectionProps = {}
        for (var variable in sectionProps) {
          if (sectionProps.hasOwnProperty(variable)) {
            newSectionProps[variable.replace(/_/gi, '.')] = sectionProps[variable]
          }
        }
        // console.log('newSectionProps', newSectionProps)
        const section = {
          pid: ownProps.projectId,
          unitid: ownProps.ahuId,
          key: $(element).attr('data-section'),
          position: index + 1,
          metaJson: JSON.stringify(newSectionProps),
          partid: $(element).attr('data-partid'),
        }
        sections.push(section)
      })

      const sectionStr = JSON.stringify(sections)
      dispatch(deleComponentValue)
      dispatch(calcComponentValue(ownProps.projectId, ownProps.ahuId, ahuStr, sectionStr, position, 'ahu.fan'))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Fan)

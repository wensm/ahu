import { connect } from 'react-redux'
import FanCalcModal from './FanCalcModal'
import { updateFanValue } from '../../../actions/ahu'

const mapStateToProps = state => {
  return {
    fanCalcs: state.ahu.fanCalcs,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onUpdateFanValue: index => {
      dispatch(updateFanValue(index))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FanCalcModal)

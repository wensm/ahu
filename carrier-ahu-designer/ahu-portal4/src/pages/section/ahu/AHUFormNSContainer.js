import React from 'react'
import { connect } from 'react-redux'
import AHUFormNS from './AHUFormNS'
import { reduxForm } from 'redux-form'
import { fetchAhuPartitions } from '../../../actions/ahu'

const mapStateToProps = state => {
  return {
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
    user: state.general.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onFetchAhuPartitions: function (ahuId) {
      dispatch(fetchAhuPartitions(ahuId))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AHUFormNS)

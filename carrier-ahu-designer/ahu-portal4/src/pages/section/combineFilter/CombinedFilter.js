import {
    SPECIFICATION_PANEL,
    QUANTITY_PANEL,
    SPECIFICATION_BAG,
    QUANTITY_BAG,
    PARAMETER_INPUT,
    PANEL_RESISTANCE,
    FILTER_ARRANGEMENT,
    CONFIRM,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

import comb from '../../../images/put_icon2_1.png'
const MAP = { comb }

class CombinedFilter extends React.Component {

    componentWillMount() {
        // this.dragInit()
        const { componentValue } = this.props
        let sectionl = componentValue['meta_section_combinedFilter_sectionL'];
        if (sectionl == null) {
            componentValue['meta_section_combinedFilter_sectionL'] = 3;
        }
    }

    render() {
        const { isCompleted, onCompleteSection, componentValue, propUnit, unitSetting, metaUnit, metaLayout } = this.props
        let arrList = [];
        if (componentValue) {
            const arr = componentValue['meta_section_combinedFilter_FilterArrange']
            if (arr && arr != '')
                arrList = JSON.parse(arr);
        }
        let selectedComponentId = this.props.selectedComponent.id
        let element = $(`[data-section-id='${selectedComponentId}']`)[0]
        const section = $(element).attr('data-section')
        let hasCombine = false;
        let parent = $(`[data-section-id='${selectedComponentId}']`).parent().children('[data-section]')
        parent.each((index, record) => {
            if (record.getAttribute('data-section') == 'ahu.combinedMixingChamber') {
                hasCombine = true//找到新回排
            }
        })
        if ((componentValue.meta_section_doubleReturnAirDirection == "R" || componentValue.meta_section_airDirection == "R")  && hasCombine === false) {
            $(element).css("background-image", 'url(' + MAP.comb + ')')
        }
        return (
            <form data-name="Filter">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <Group title={intl.get(PARAMETER_INPUT)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.fitetF" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.fitlerStandard" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.rimThickness" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.mediaLoading" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.BracketM" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.BaffleM" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.spareFilter" />
                        </div>
                    </Group>
                    <Group title={intl.get(PANEL_RESISTANCE)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.LMaterial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.LMaterialE" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.pressureGuageP" />
                        </div>
                    </Group>
                    <Group title={intl.get(PANEL_RESISTANCE)} id="group15">
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.RMaterial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.RMaterialE" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.pressureGuageB" />
                        </div>
                    </Group>
                    <Group title={intl.get(PANEL_RESISTANCE)} id="group15">
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.initialPDP" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.everagePDP" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.finalPDP" />
                        </div>
                    </Group>
                    <Group title={intl.get(PANEL_RESISTANCE)} id="group15">
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.initialPDB" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.everagePDB" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.finalPDB" />
                        </div>
                    </Group>
                    <Group title={intl.get(PANEL_RESISTANCE)} id="group15">
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.Weight" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.combinedFilter.appendAirVolume" />
                        </div>
                    </Group>
                    <Group title={intl.get(FILTER_ARRANGEMENT)} id="group6">
                        <div className="modal-body">
                            <div style={{ maxHeight: window.innerHeight - 220, overflow: 'auto' }}>
                                <table className="table table-hover table-bordered" style={{ width: '500px' }}>
                                    <thead>
                                        <tr>
                                            <th style={{ textAlign: 'center' }}>{intl.get(SPECIFICATION_PANEL)}</th>
                                            <th style={{ textAlign: 'center' }}>{intl.get(QUANTITY_PANEL)}</th>
                                            <th style={{ textAlign: 'center' }}>{intl.get(SPECIFICATION_BAG)}  </th>
                                            <th style={{ textAlign: 'center' }}>{intl.get(QUANTITY_BAG)}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {arrList && arrList.map((line, index) => <ArrangementLine key={index} index={index} line={line} />)}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="col-lg-12 btnGroup" style={{ paddingBottom: '10px' }}>
                            <Button
                                // type="button" className="btn btn-primary" 
                                disabled={!this.props.canConfirm || this.props.invalid}
                                // style={{ marginLeft: '10px' }}
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
}

class ArrangementLine extends React.Component {

    render() {
        const { line, index, componentValue } = this.props
        return (
            <tr>
                <td>{line.pOption}</td>
                <td>{line.pCount}</td>
                <td>{line.lOption}</td>
                <td>{line.lCount}</td>
            </tr>
        )
    }
}
export default reduxForm({
    form: 'CombinedFilter', // a unique identifier for this form
    enableReinitialize: true,
})(CombinedFilter)

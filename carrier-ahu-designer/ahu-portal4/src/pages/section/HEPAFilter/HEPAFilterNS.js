import {
    NON_STANDARD_OPTION,
    CONFIRM
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button } from 'antd';

class HEPAFilterNS extends React.Component {
  render() {
    const { isCompleted, onCompleteSection, propUnit, unitSetting, metaUnit, metaLayout } = this.props
    return (
      <form data-name="HEPAFilterNS">
        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <Group title={intl.get(NON_STANDARD_OPTION)} id="group1">
            <div className="col-lg-12">
              <Field id="ns.section.HEPAFilter.enable" onChange={(e) => { this.props.onchangeNsEnable(e.target.value) }}/>
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.HEPAFilter.Price"/>
            </div>
            <div className="col-lg-9">
              <Field id="ns.section.HEPAFilter.MEMO"/>
            </div>
             <div className="col-lg-3">
              <Field id="ns.section.HEPAFilter.nsPressureGuage" />
            </div>
            <div className="col-lg-6">
              <Field id="ns.section.HEPAFilter.nsPressureGuageModel" />
            </div>
            <div className="col-lg-3">
              <Field id="ns.section.HEPAFilter.nsPressureGuagePrice" />
            </div>
            <div className="col-lg-12 btnGroup" style={{paddingBottom: '10px'}}>
                        <Button
                        //  type="button" className="btn btn-primary"
                          disabled={!this.props.canConfirm ||this.props.invalid} 
                          // style={{marginLeft: '10px'}}
                          size='small' type="primary"
                            style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout, true)}>
                            {intl.get(CONFIRM)}
                        </Button>
                    </div>
          </Group>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'HEPAFilterNS', // a unique identifier for this form
  enableReinitialize: true,
})(HEPAFilterNS)

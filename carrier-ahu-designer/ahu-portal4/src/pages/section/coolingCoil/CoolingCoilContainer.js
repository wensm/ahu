import { connect } from 'react-redux'
import CoolingCoil from './CoolingCoil'
import { newUpdateCoolingCoilByIndex } from '../../../actions/ahu'

const mapStateToProps = state => {
  return {
    tableLoading: state.ahu.tableLoading,
    coolingCoilCalcs: state.ahu.coolingCoilCalcs,
    unitPreferCode: state.general.user.unitPreferCode,
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
    isCompleted: state.ahu.componentValue[state.ahu.selectedComponent.id] && state.ahu.componentValue[state.ahu.selectedComponent.id].meta_section_completed,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
      onOkCoilCalc :(selectedRows, unitPreferCode, unitSetting, metaUnit, metaLayout)=>{
        dispatch(newUpdateCoolingCoilByIndex(selectedRows, unitPreferCode, unitSetting, metaUnit, metaLayout))
      }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoolingCoil)

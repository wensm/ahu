import {
    COIL_SECTION_CALCULATION_RESULT,
    COIL_INFORMATION,
    INLET_WIND,
    OUTLET_AIR,
    AIR_SIDE,
    WATER_SIDE,
    PERFORMANCE,
    ROW_NUMBER,
    FIN_DISTANCE,
    CIRCUIT,
    WINDWARD_AREA,
    FACE_WIND_SPEED,
    DB_TEMPERATURE,
    WET_BULB_TEMPERATURE,
    RELATIVE_HUMIDITY,
    AIR_RESISTANCE,
    WATER_RESISTANCE,
    WATER_FLOW_VOLUME,
    INLET_WATER_TEMPERATURE,
    WATER_TEMPERATURE_RISE,
    MEDIUM_FLOW_RATE,
    CLOSE,
    OK,
    HEATING_VOLUME,
    PRICE
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { updateCoolingCoilByIndex } from '../../../actions/ahu'
import loadingImg from '../../../images/loding.gif'

class CoolingCoilwModal extends React.Component {
    render() {
        const { coolingCoilCalcs, onSelectCoolingCoilCalc, unitPreferCode, unitSetting, metaUnit, metaLayout} = this.props
        return (
            <div data-name="CoolingCoilwModal">
                <div className="modal fade" id="coolingCoilwModal" tabIndex="-1" role="dialog" aria-labelledby="coolingCoilwModalLabel">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="coolingCoilwModalLabel">{intl.get(COIL_SECTION_CALCULATION_RESULT)}</h4>
                            </div>
                            <div className="modal-body">
                                <div style={{maxHeight:window.innerHeight-220,overflow:'auto'}}>
                                {!coolingCoilCalcs.resultData && <img src={loadingImg} style={{position:'absolute',width:'40px',top:'90%',left:'45%'}}/>}
                                    <table className="table table-hover table-bordered" style={{width:'1200px'}}>
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th colSpan="5" style={{textAlign:'center'}}>{intl.get(COIL_INFORMATION)}</th>
                                            <th colSpan="3" style={{textAlign:'center'}}>{intl.get(INLET_WIND)}</th>
                                            <th colSpan="3" style={{textAlign:'center'}}>{intl.get(OUTLET_AIR)}</th>
                                            <th colSpan="1" style={{textAlign:'center'}}>{intl.get(AIR_SIDE)}</th>
                                            <th colSpan="5" style={{textAlign:'center'}}>{intl.get(WATER_SIDE)}</th>
                                            <th colSpan="1" style={{textAlign:'center'}}>{intl.get(PERFORMANCE)}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td>{intl.get(ROW_NUMBER)}</td>
                                            <td>{intl.get(FIN_DISTANCE)}</td>
                                            <td>{intl.get(CIRCUIT)}</td>
                                            <td>{intl.get(WINDWARD_AREA)}</td>
                                            <td>{intl.get(FACE_WIND_SPEED)}</td>
                                            <td>{intl.get(DB_TEMPERATURE)}</td>
                                            <td>{intl.get(WET_BULB_TEMPERATURE)}</td>
                                            <td>{intl.get(RELATIVE_HUMIDITY)}</td>
                                            <td>{intl.get(DB_TEMPERATURE)}</td>
                                            <td>{intl.get(WET_BULB_TEMPERATURE)}</td>
                                            <td>{intl.get(RELATIVE_HUMIDITY)}</td>
                                            <td>{intl.get(AIR_RESISTANCE)}</td>
                                            <td>{intl.get(WATER_RESISTANCE)}</td>
                                            <td>{intl.get(WATER_FLOW_VOLUME)}</td>
                                            <td>{intl.get(INLET_WATER_TEMPERATURE)}</td>
                                            <td>{intl.get(WATER_TEMPERATURE_RISE)}</td>
                                            <td>{intl.get(MEDIUM_FLOW_RATE)}</td>
                                            <td>{intl.get(HEATING_VOLUME)}</td>
                                        </tr>
                                        <tr>
                                        	<td></td>
                                            <td></td>
                                            <td>{intl.get('title.moon_intl_str_0661')}</td>
                                            <td></td>
                                            <td>{intl.get('title.moon_intl_str_0662')}</td>
                                            <td>{intl.get('title.moon_intl_str_0663')}</td>
                                            <td>{intl.get('title.moon_intl_str_0664')}</td>
                                            <td>{intl.get('title.moon_intl_str_0664')}</td>
                                            <td>{intl.get('title.moon_intl_str_0665')}</td>
                                            <td>{intl.get('title.moon_intl_str_0664')}</td>
                                            <td>{intl.get('title.moon_intl_str_0664')}</td>
                                            <td>{intl.get('title.moon_intl_str_0665')}</td>
                                            <td>{intl.get('title.moon_intl_str_0666')}</td>
                                            <td>{intl.get('title.moon_intl_str_0667')}</td>
                                            <td>{intl.get('title.moon_intl_str_0668')}</td>
                                            <td>{intl.get('title.moon_intl_str_0664')}</td>
                                            <td>{intl.get('title.moon_intl_str_0664')}</td>
                                            <td>{intl.get('title.moon_intl_str_0663')}</td>
                                            <td>{intl.get('title.moon_intl_str_0669')}</td>
                                        </tr>
                                        
                                        {coolingCoilCalcs.resultData && coolingCoilCalcs.resultData.map((coolingCoilCalc, index) =>
                                        	<CoolingCoilCalc key={index} index={index} coolingCoilCalc={coolingCoilCalc} />)}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CLOSE)}</button>
                                <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={() => onSelectCoolingCoilCalc( unitPreferCode, unitSetting, metaUnit, metaLayout)}>{intl.get(OK)}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
class CoolingCoilCalc extends React.Component {
    render() {
        const { coolingCoilCalc, index } = this.props
        return (
            <tr>
                <td>
                    <input type="radio" name="coolingCoilCalcRadio" data-index={index} />
                </td>
                <td>{coolingCoilCalc['rows']}</td>
                <td>{coolingCoilCalc['finDensity']}</td>
                <td>{coolingCoilCalc['circuit']}</td>
                <td>{coolingCoilCalc['area']}</td>
                <td>{coolingCoilCalc['velocity']}</td>
                <td>{coolingCoilCalc['InDryBulbT']}</td>
                <td>{coolingCoilCalc['InWetBulbT']}</td>
                <td>{coolingCoilCalc['InRelativeT']}</td>
                <td>{coolingCoilCalc['OutDryBulbT']}</td>
                <td>{coolingCoilCalc['OutWetBulbT']}</td>
                <td>{coolingCoilCalc['OutRelativeT']}</td>
                <td>{coolingCoilCalc['AirResistance']}</td>
                <td>{coolingCoilCalc['waterResistance']}</td>
                <td>{coolingCoilCalc['returnWaterFlow']}</td>
                <td>{coolingCoilCalc['returnEnteringFluidTemperature']}</td>
                <td>{coolingCoilCalc['WTAScend']}</td>
                <td>{coolingCoilCalc['fluidvelocity']}</td>
                <td>{coolingCoilCalc['returnHeatQ']}</td>
            </tr>
        )
    }
}

const mapStateToProps = state => {
    return {
        coolingCoilCalcs: state.ahu.coolingCoilCalcs,
        unitPreferCode: state.general.user.unitPreferCode,        
        unitSetting: state.metadata.metaUnitSetting,
        metaUnit: state.metadata.metaUnit,
        metaLayout: state.metadata.layout,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onSelectCoolingCoilCalc: ( unitPreferCode, unitSetting, metaUnit, metaLayout) => {
            const index = $('#coolingCoilwModal').find('input[type="radio"]:checked').attr('data-index')
            if (typeof index !== typeof undefined) {
                dispatch(updateCoolingCoilByIndex(index, unitPreferCode, unitSetting, metaUnit, metaLayout))
            }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoolingCoilwModal)

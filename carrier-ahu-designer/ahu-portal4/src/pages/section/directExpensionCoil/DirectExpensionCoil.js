import {
    COIL_INFORMATION,
    COIL_OPTION,
    SUMMER_SLASH_WINTER,
    SUMMER,
    WINTER,
    INLET_WIND_TEMP_SUMMER,
    INLET_WIND_TEMP_WINTER,
    WEIGHT_AND_SECTION_LENGTH,
    OPERATION,
    CONFIRM,
    PARAMETER,
    CALCULATION,
    INVALID,
    CALCULATION_RESULT,
    LABEL_TOOLTIP
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required, tooCold, tooHot } from '../../ahu/Validate'
import { Popover, Tabs, Icon, Tooltip, Button, Modal, Spin } from 'antd';
import CalculateTable from '../../ahu/calculateTable'
import sweetalert from 'sweetalert'

class DirectExpensionCoil extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false
        }
    }
    render() {
        const {
            isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            onCalcCoolingCoil,
            unitSetting,
            metaUnit,
            metaLayout,
            componentValue,
            propUnit,
            coolingCoilCalcs,
            unitPreferCode,
            onOkCoilCalc,
            tableLoading
        } = this.props
        let enableWinter = componentValue ? eval(componentValue.meta_section_directExpensionCoil_EnableWinter) : false

        return (
            <form data-name="directExpensionCoil">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <Group title={intl.get(COIL_INFORMATION)} id="group1">
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.tubeDiameter" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.rows" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.circuit" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.finDensity" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.finType" />
                        </div>
                    </Group>
                    <Group title={intl.get(COIL_OPTION)} id="group3">
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.eliminator" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.pipe" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.waterCollecting" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.drainpanType" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.drainpanMaterial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.baffleMaterial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.coilFrameMaterial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.uTrap" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.uTrapNum" />
                        </div>
                    </Group>
                    <Group title={intl.get(PARAMETER)} id="group3">
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.Cond" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SatCond" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SubClg" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.ACCPressure" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SuperHeat" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.CDU" />
                        </div>
                    </Group>
                    <Group title={intl.get(INLET_WIND_TEMP_SUMMER)} id="group13">

                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{intl.get(SUMMER_SLASH_WINTER)}</h3>
                            </div>
                            <div className="panel-body" style={{ padding: '15px 0px' }}>
                                <div>
                                    <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                                        <li role="presentation" className="active"><a href="#summer" aria-controls="summer" role="tab" data-toggle="tab">{intl.get(SUMMER)}</a></li>
                                        <li role="presentation"><a href="#winter" aria-controls="winter" role="tab" data-toggle="tab">{intl.get(WINTER)}</a></li>
                                    </ul>
                                    <div className="tab-content">
                                        <div role="tabpanel" className="tab-pane active" id="summer">
                                            <Group title={intl.get(INLET_WIND_TEMP_SUMMER)} showLeftBorder={false}>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.directExpensionCoil.SInDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_directExpensionCoil_SInWetBulbT', newValue, 'meta_section_directExpensionCoil_SInRelativeT', 'meta_section_directExpensionCoil_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}

                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field
                                                        id="meta.section.directExpensionCoil.SInWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_directExpensionCoil_SInDryBulbT', newValue, 'meta_section_directExpensionCoil_SInRelativeT', 'meta_section_directExpensionCoil_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field
                                                        id="meta.section.directExpensionCoil.SInRelativeT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_directExpensionCoil_SInDryBulbT', newValue, 'meta_section_directExpensionCoil_SInWetBulbT', 'meta_section_directExpensionCoil_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </Group>

                                            <Group title={intl.get(INLET_WIND_TEMP_WINTER)} showLeftBorder={false}>

                                                <div className="col-lg-3">
                                                    <Field id="meta.section.directExpensionCoil.SOutDryBulbT" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.directExpensionCoil.SOutWetBulbT" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.directExpensionCoil.SOutRelativeT" />
                                                </div>
                                            </Group>
                                        </div>
                                        <div role="tabpanel" className="tab-pane" id="winter">
                                            <div style={{ marginBottom: '5px' }}>
                                                <Field id="meta.section.directExpensionCoil.EnableWinter" />
                                            </div>
                                            {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER)} showLeftBorder={false}>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.directExpensionCoil.WInDryBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_directExpensionCoil_WInWetBulbT', newValue, 'meta_section_directExpensionCoil_WInRelativeT', 'meta_section_directExpensionCoil_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}

                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field
                                                        id="meta.section.directExpensionCoil.WInWetBulbT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_directExpensionCoil_WInDryBulbT', newValue, 'meta_section_directExpensionCoil_WInRelativeT', 'meta_section_directExpensionCoil_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field
                                                        id="meta.section.directExpensionCoil.WInRelativeT"
                                                        onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_directExpensionCoil_WInDryBulbT', newValue, 'meta_section_directExpensionCoil_WInWetBulbT', 'meta_section_directExpensionCoil_WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                                    />
                                                </div>
                                            </Group> : ''}
                                            {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER)} showLeftBorder={false}>

                                                <div className="col-lg-3">
                                                    <Field id="meta.section.directExpensionCoil.WOutDryBulbT" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.directExpensionCoil.WOutWetBulbT" />
                                                </div>
                                                <div className="col-lg-3">
                                                    <Field id="meta.section.directExpensionCoil.WOutRelativeT" />
                                                </div>
                                            </Group> : ''}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Group>

                    <Group title={intl.get(WEIGHT_AND_SECTION_LENGTH)} id="group12">
                        {/* <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SOutDryBulbT" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SOutWetBulbT" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SOutRelativeT" />
                        </div> */}
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SAirResistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SreturnCond" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SreturnSST" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SreturnSatCond" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SUnit" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SreturnColdQ" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.SreturnSensibleCapacity" />
                        </div>
                    </Group>
                    <Group title={intl.get(WEIGHT_AND_SECTION_LENGTH)} id="group12">
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.driftEliminatorResistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.Weight" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.appendAirVolume" />
                        </div>
                        {/* <div className="col-lg-3">
                            <Field id="meta.section.directExpensionCoil.runningWeight" />
                        </div> */}
                    </Group>

                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                        <div className="col-lg-3 btnGroup" style={{ paddingTop: '25px' }}>
                            <Button
                                // type="button"
                                // className="btn btn-primary"
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7' }}
                                // data-toggle="modal"
                                // data-target="#coolingCoilModal"
                                onClick={() => { this.setState({ visible: true }); onCalcCoolingCoil(this.props.componentValues, this.props.selectedComponent, "S", unitSetting, metaUnit, metaLayout, propUnit) }}
                            >
                                {intl.get(CALCULATION)}
                            </Button>
                        </div>
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7', marginLeft: '10px' }}
                                // type="button" 
                                // className="btn btn-primary"
                                disabled={!this.props.canConfirm || this.props.invalid} onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
                <Modal
                    title={intl.get(CALCULATION_RESULT)}
                    visible={this.state.visible}
                    onOk={() => { this.handleOk(unitPreferCode, unitSetting, metaUnit, metaLayout, onOkCoilCalc) }}
                    onCancel={this.handleCancel}
                    width='1200px'
                >
                    <Spin spinning={tableLoading}>

                        <CalculateTable ref='calculateDirectCoilTable' datas={coolingCoilCalcs} type='directExpensionCoil' />
                    </Spin>
                </Modal>
            </form>
        )
    }
    handleCancel = () => {
        this.setState({ visible: false })
    }
    handleOk = (unitPreferCode, unitSetting, metaUnit, metaLayout, onOkCoilCalc) => {
        let { selectedRowKeys, selectedRows } = this.refs.calculateDirectCoilTable.returnState()
        if (selectedRowKeys.length > 0 && selectedRows.length > 0) {

            if (selectedRows[0].ahriMessage) {
                sweetalert({
                    title: intl.get(LABEL_TOOLTIP),
                    text: intl.get(`label.${selectedRows[0].ahriMessage}`),
                    type: 'warning',
                    showConfirmButton: true
                })
            }
            onOkCoilCalc(selectedRows, unitPreferCode, unitSetting, metaUnit, metaLayout)
            this.setState({ visible: false })
        }
    }
}

export default reduxForm({
    form: 'DirectExpensionCoil', // a unique identifier for this form
    enableReinitialize: true,
})(DirectExpensionCoil)

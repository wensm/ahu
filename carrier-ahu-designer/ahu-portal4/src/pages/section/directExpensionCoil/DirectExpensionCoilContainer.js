import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import DirectExpensionCoil from './DirectExpensionCoil'
import { newUpdateCoolingCoilByIndex } from '../../../actions/ahu'

const mapStateToProps = state => {
  return {
    tableLoading: state.ahu.tableLoading,
    coolingCoilCalcs: state.ahu.coolingCoilCalcs,
    unitPreferCode: state.general.user.unitPreferCode,
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
    isCompleted: state.ahu.componentValue[state.ahu.selectedComponent.id] && state.ahu.componentValue[state.ahu.selectedComponent.id].meta_section_completed,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
      onOkCoilCalc :(selectedRows, unitPreferCode, unitSetting, metaUnit, metaLayout)=>{
        dispatch(newUpdateCoolingCoilByIndex(selectedRows, unitPreferCode, unitSetting, metaUnit, metaLayout))
      }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DirectExpensionCoil)

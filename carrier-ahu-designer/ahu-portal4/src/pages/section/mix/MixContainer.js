import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import sweetalert from 'sweetalert'
import {
  OK,
  MODIFY_SUCCESS
} from '../../intl/i18n'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import Mix from './Mix'
import {confirmSection, fetchOutletParams, customDamper, syncAVolume, changeUvLamp} from '../../../actions/ahu'

const mapStateToProps = state => {
  return {
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
    isCompleted: state.ahu.componentValue[state.ahu.selectedComponent.id] && state.ahu.componentValue[state.ahu.selectedComponent.id].meta_section_completed,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onCalcSummerOutletParams: () => {
      const airFlow = ownProps.componentValue.meta_section_mix_NAVolume
      const freshAirRate = ownProps.componentValue.meta_section_mix_NARatio
      const freshAirTIN = ownProps.componentValue.meta_section_mix_SInDryBulbT
      const freshAirTbIN = ownProps.componentValue.meta_section_mix_SInWetBulbT
      const returnAirTIN = ownProps.componentValue.meta_section_mix_SNewDryBulbT
      const returnTbIN = ownProps.componentValue.meta_section_mix_SNewWetBulbT
      dispatch(fetchOutletParams('summer', airFlow, freshAirRate, freshAirTIN, freshAirTbIN, returnAirTIN, returnTbIN))
    },
    onCalcWinterOutletParams: () => {
      const airFlow = ownProps.componentValue.meta_section_mix_NAVolume
      const freshAirRate = ownProps.componentValue.meta_section_mix_NARatio
      const freshAirTIN = ownProps.componentValue.meta_section_mix_WInDryBulbT
      const freshAirTbIN = ownProps.componentValue.meta_section_mix_WInWetBulbT
      const returnAirTIN = ownProps.componentValue.meta_section_mix_WNewDryBulbT
      const returnTbIN = ownProps.componentValue.meta_section_mix_WNewWetBulbT
      dispatch(fetchOutletParams('winter', airFlow, freshAirRate, freshAirTIN, freshAirTbIN, returnAirTIN, returnTbIN))
    },
    onCustomDamper: (dir, param, otherParam, callBack) => {
      //   let param = {
      //     "unit": {
      //         "product": "39CQ",
      //         "series": "39CQ1418",
      //         "metaJson": "{'meta.ahu.eairvolume': '3600', 'meta.ahu.sairvolume': '3600'}"
      //     },
      //     "section": {
      //         "sectionKey": "ahu.mix",
      //         "metaJson": "{'meta.section.airDirection':'S', 'meta.section.mix.NARatio': '20', 'meta.section.mix.damperTopType': 'Fresh', 'meta.section.mix.sectionL': '11','meta.section.mix.damperOutlet': 'FD','meta.section.mix.damperTopModeA': '4', 'meta.section.mix.damperTopModeB': '4', 'meta.section.mix.damperTopModeC': '5', 'meta.section.mix.damperTopModeD': '5','meta.section.mix.damperBackType': 'Return', 'meta.section.mix.damperBackModeA': '2', 'meta.section.mix.damperBackModeB': '2', 'meta.section.mix.damperBackModeC': '2', 'meta.section.mix.damperBackModeD': '2'}"
      //     }
      // }

      dispatch(customDamper(dir, JSON.stringify(param), otherParam, callBack))

    },
    onchangeUvLamp:(value)=>{
        dispatch(changeUvLamp(value))
    },
    onsyncAVolume: () => {
      sweetalert({
        title: intl.get(MODIFY_SUCCESS), 
        type: 'success',
        // showCancelButton: true,
        confirmButtonText: intl.get(OK),
        timer: 2000,

        // closeOnConfirm: true
      }, isConfirm => {
        if(isConfirm){
          dispatch(syncAVolume())
        }
        
      })
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Mix)

import {
    SUMMER,
    WINTER,
    RETURN_AIR_PARAMETER_SUMMER,
    RETURN_AIR_PARAMETER_WINTER,
    MIX_FORM,
    TOP_RETURN_AIR,
    BACK_RETURN_AIR,
    LEFT_SIDE_RETURN_AIR,
    RIGHT_SIDE_RETURN_AIR,
    BOTTOM_RETURN_AIR,
    AIR_VALVE_OPTION,
    OTHER_OPTION,
    RESISTANCE_AND_WEIGHT,
    OPERATION,
    CONFIRM,
    INVALID,
    CALCULATION,
    CLOSE,
    OK,
    Append_Air_Volume
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { Popover, Tabs, Icon, Tooltip, Button, Modal, Row, Col, Checkbox, Select } from 'antd';

const { Option } = Select;
import mix from '../../../images/pro_mix_b.png'
const requireContext = require.context("../../../images/mix", false, /^\.\/.*\.png$/);
const images = requireContext.keys().map(requireContext);
const MAP = {
    mix
}
images.map((v, k) => {
    MAP[`mix${k + 1}`] = v
})
class Mix extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            type: ''
        }
    }
    clickNavi(e) {
        let theLink = e.target
        if (theLink) {
            let theHref = theLink.getAttribute("href")
            theHref = theHref.substr(1)
            const element = document.getElementById(theHref)
            if (element) element.scrollIntoView()
            e.preventDefault()
        }
    }

    render() {
        // console.log('ppppppppppppp', this.props)
        const {
            isCompleted,
            onCalcRelativeHumidity,
            // onCalcWetBulbTemperature,
            onCompleteSection,
            onCalcMixAir1,
            onCalcMixAir2,
            onCalcMixAir4,
            componentValue,
            componentValues,
            invalid, propUnit, unitSetting, metaUnit, metaLayout,
            selectedComponent,
            onsyncAVolume
        } = this.props
        let enableWinter = componentValue ? eval(componentValue.meta_section_mix_EnableWinter) : false
        let enableSummer = componentValue ? eval(componentValue.meta_section_mix_EnableSummer) : false
        let returnBack = componentValue ? eval(componentValue.meta_section_mix_returnBack) : false
        let returnButtom = componentValue ? eval(componentValue.meta_section_mix_returnButtom) : false
        let returnLeft = componentValue ? eval(componentValue.meta_section_mix_returnLeft) : false
        let returnRight = componentValue ? eval(componentValue.meta_section_mix_returnRight) : false
        let returnTop = componentValue ? eval(componentValue.meta_section_mix_returnTop) : false
        const ahuDirection = this.props.componentValue
        let selectedComponentId = this.props.selectedComponent.id;
        let element = $(`[data-section-id='${selectedComponentId}']`)[0]
        const section = $(element).attr('data-section')
        let hasCombine = false;
        let parent = $(`[data-section-id='${selectedComponentId}']`).parent().children('[data-section]')
        parent.each((index, record) => {
            if (record.getAttribute('data-section') == 'ahu.combinedMixingChamber') {
                hasCombine = true//找到新回排
            }
        })
        function imgSet(mix1, mix2, mix3, mix4, mix5, mix6, mix7, mix8, mix9, mix10, mix11, mix12, mix13, mix14, mix15) {
            let letmix
            if (eval(ahuDirection.meta_section_mix_returnTop) == true && eval(ahuDirection.meta_section_mix_returnButtom) == true) {
                letmix = MAP[mix1]
            } else if (eval(ahuDirection.meta_section_mix_returnTop) == true && eval(ahuDirection.meta_section_mix_returnBack) == true) {
                letmix = MAP[mix2]
            } else if (eval(ahuDirection.meta_section_mix_returnTop) == true && eval(ahuDirection.meta_section_mix_returnRight) == true) {
                letmix = MAP[mix3]
            } else if (eval(ahuDirection.meta_section_mix_returnTop) == true && eval(ahuDirection.meta_section_mix_returnLeft) == true) {
                letmix = MAP[mix4]
            } else if (eval(ahuDirection.meta_section_mix_returnRight) == true && eval(ahuDirection.meta_section_mix_returnButtom) == true) {
                letmix = MAP[mix5]
            } else if (eval(ahuDirection.meta_section_mix_returnRight) == true && eval(ahuDirection.meta_section_mix_returnBack) == true) {
                letmix = MAP[mix6]
            } else if (eval(ahuDirection.meta_section_mix_returnLeft) == true && eval(ahuDirection.meta_section_mix_returnButtom) == true) {
                letmix = MAP[mix7]
            } else if (eval(ahuDirection.meta_section_mix_returnLeft) == true && eval(ahuDirection.meta_section_mix_returnBack) == true) {
                letmix = MAP[mix8]
            } else if (eval(ahuDirection.meta_section_mix_returnLeft) == true && eval(ahuDirection.meta_section_mix_returnRight) == true) {
                letmix = MAP[mix9]
            } else if (eval(ahuDirection.meta_section_mix_returnButtom) == true && eval(ahuDirection.meta_section_mix_returnBack) == true) {
                letmix = MAP[mix10]
            } else if (eval(ahuDirection.meta_section_mix_returnTop) == true) {
                letmix = MAP[mix11]
            } else if (eval(ahuDirection.meta_section_mix_returnBack) == true) {
                letmix = MAP[mix12]
            } else if (eval(ahuDirection.meta_section_mix_returnRight) == true) {
                letmix = MAP[mix13]
            } else if (eval(ahuDirection.meta_section_mix_returnButtom) == true) {
                letmix = MAP[mix14]
            } else if (eval(ahuDirection.meta_section_mix_returnLeft) == true) {
                letmix = MAP[mix15]
            }
            else {
                letmix = MAP[mix10]
            }
            return $(element).css("background-image", 'url(' + letmix + ')')
        }
        switch (ahuDirection.meta_section_airDirection) {
            case "R":
                if (hasCombine === true) {
                    imgSet('mix12', 'mix14', 'mix18', 'mix17', 'mix7', 'mix9', 'mix25', 'mix1', 'mix4', 'mix20', 'mix10', 'mix2', 'mix4', 'mix', 'mix22')
                    break;
                }
                imgSet('mix12', 'mix13', 'mix15', 'mix16', 'mix6', 'mix8', 'mix24', 'mix26', 'mix5', 'mix19', 'mix10', 'mix21', 'mix5', 'mix', 'mix23')
                break;
            default:
                imgSet('mix12', 'mix14', 'mix18', 'mix17', 'mix7', 'mix9', 'mix25', 'mix1', 'mix4', 'mix20', 'mix10', 'mix2', 'mix4', 'mix', 'mix22')
                break;
        }
        return (
            <form data-name="Mix">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div>
                        <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                            <li role="presentation" className="active"><a href="#summer" aria-controls="summer"
                                role="tab"
                                data-toggle="tab">{intl.get(SUMMER)}</a>
                            </li>
                            <li role="presentation"><a href="#winter" aria-controls="winter" role="tab"
                                data-toggle="tab">{intl.get(WINTER)}</a>
                            </li>
                        </ul>
                        <div className="tab-content">
                            <div role="tabpanel" className="tab-pane active" id="summer">
                                <div style={{ marginBottom: '5px' }}>
                                    <Field id="meta.section.mix.EnableSummer" />
                                </div>
                                {enableSummer ? <Group title={intl.get(RETURN_AIR_PARAMETER_SUMMER)} id="group6">
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.SInDryBulbT" onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                'meta_section_mix_NARatio',
                                                'meta_section_mix_SNewDryBulbT',
                                                'meta_section_mix_SNewWetBulbT',
                                                'meta_section_mix_SInDryBulbT',
                                                'meta_section_mix_SInWetBulbT',
                                                ['meta_section_mix_SOutDryBulbT',
                                                    'meta_section_mix_SOutWetBulbT',
                                                    'meta_section_mix_SOutRelativeT',
                                                    'meta_section_mix_NAVolume',
                                                    'meta_section_mix_SInRelativeT',
                                                    'meta_section_mix_SNewRelativeT',
                                                    'meta_section_mix_SNewWetBulbT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                newValue, 'InDryBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                            )} />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SInWetBulbT"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewWetBulbT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInWetBulbT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_SInRelativeT',
                                                        'meta_section_mix_SNewRelativeT',
                                                        'meta_section_mix_SNewWetBulbT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                    newValue, 'InWetBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>

                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SInRelativeT"
                                                maxValue={100}
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir2(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewRelativeT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInRelativeT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_SInWetBulbT',
                                                        'meta_section_mix_SNewWetBulbT',
                                                        'meta_section_mix_SInRelativeT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inWetBulbT', 'newWetBulbT', 'inRelativeT'],
                                                    newValue, 'InRelativeT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.SNewDryBulbT" onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                'meta_section_mix_NARatio',
                                                'meta_section_mix_SNewDryBulbT',
                                                'meta_section_mix_SNewWetBulbT',
                                                'meta_section_mix_SInDryBulbT',
                                                'meta_section_mix_SInWetBulbT',
                                                ['meta_section_mix_SOutDryBulbT',
                                                    'meta_section_mix_SOutWetBulbT',
                                                    'meta_section_mix_SOutRelativeT',
                                                    'meta_section_mix_NAVolume',
                                                    'meta_section_mix_SInRelativeT',
                                                    'meta_section_mix_SNewRelativeT',
                                                    'meta_section_mix_SNewWetBulbT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                newValue, 'NewDryBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                            )} />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SNewWetBulbT"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewWetBulbT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInWetBulbT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_SInRelativeT',
                                                        'meta_section_mix_SNewRelativeT',
                                                        'meta_section_mix_SNewWetBulbT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                    newValue, 'NewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SNewRelativeT"
                                                maxValue={100}
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir2(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewRelativeT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInRelativeT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_SInWetBulbT',
                                                        'meta_section_mix_SNewWetBulbT',
                                                        'meta_section_mix_SNewRelativeT',
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inWetBulbT', 'newWetBulbT', 'newRelativeT'],
                                                    newValue, 'NewRelativeT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}

                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.NARatio"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewWetBulbT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInWetBulbT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_SInRelativeT',
                                                        'meta_section_mix_SNewRelativeT',
                                                        'meta_section_mix_NARatio'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'naratio'],
                                                    newValue, 'NARatio', unitSetting, metaUnit, metaLayout, propUnit
                                                )}

                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.NAVolume"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir4(
                                                    'meta_section_mix_NAVolume',
                                                    'meta_section_mix_SNewDryBulbT',
                                                    'meta_section_mix_SNewWetBulbT',
                                                    'meta_section_mix_SInDryBulbT',
                                                    'meta_section_mix_SInWetBulbT',
                                                    ['meta_section_mix_SOutDryBulbT',
                                                        'meta_section_mix_SOutWetBulbT',
                                                        'meta_section_mix_SOutRelativeT',
                                                        'meta_section_mix_NARatio',
                                                        'meta_section_mix_SInRelativeT',
                                                        'meta_section_mix_SNewRelativeT',
                                                        'meta_section_mix_NAVolume'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'naratio', 'inRelativeT', 'newRelativeT', 'navolume'],
                                                    newValue, 'NAVolume', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.SOutDryBulbT" />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SOutWetBulbT"
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.SOutRelativeT"
                                            />
                                        </div>
                                    </div>
                                </Group> : ''}
                            </div>
                            <div role="tabpanel" className="tab-pane" id="winter">
                                <div style={{ marginBottom: '5px' }}>
                                    <Field id="meta.section.mix.EnableWinter" />
                                </div>
                                {enableWinter ? <Group title={intl.get(RETURN_AIR_PARAMETER_WINTER)} id="group6">
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.WInDryBulbT" onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                'meta_section_mix_NARatio',
                                                'meta_section_mix_WNewDryBulbT',
                                                'meta_section_mix_WNewWetBulbT',
                                                'meta_section_mix_WInDryBulbT',
                                                'meta_section_mix_WInWetBulbT',
                                                ['meta_section_mix_WOutDryBulbT',
                                                    'meta_section_mix_WOutWetBulbT',
                                                    'meta_section_mix_WOutRelativeT',
                                                    'meta_section_mix_NAVolume',
                                                    'meta_section_mix_WInRelativeT',
                                                    'meta_section_mix_WNewRelativeT',
                                                    'meta_section_mix_WInWetBulbT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'inWetBulbT'],
                                                newValue, 'InDryBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                            )} />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WInWetBulbT"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewWetBulbT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInWetBulbT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInRelativeT',
                                                        'meta_section_mix_WNewRelativeT',
                                                        'meta_section_mix_WInWetBulbT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'inWetBulbT'],
                                                    newValue, 'InWetBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WInRelativeT" maxValue={100}
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir2(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewRelativeT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInRelativeT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInWetBulbT',
                                                        'meta_section_mix_WNewWetBulbT',
                                                        'meta_section_mix_WInRelativeT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inWetBulbT', 'newWetBulbT', 'inRelativeT'],
                                                    newValue, 'InRelativeT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}

                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.WNewDryBulbT"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewWetBulbT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInWetBulbT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInRelativeT',
                                                        'meta_section_mix_WNewRelativeT',
                                                        'meta_section_mix_WNewWetBulbT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                    newValue, 'NewDryBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                                )} />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WNewWetBulbT"
                                                maxValue={100}
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewWetBulbT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInWetBulbT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInRelativeT',
                                                        'meta_section_mix_WNewRelativeT',
                                                        'meta_section_mix_WNewWetBulbT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'newWetBulbT'],
                                                    newValue, 'NewWetBulbT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}

                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WNewRelativeT"
                                                maxValue={100}
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir2(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewRelativeT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInRelativeT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInWetBulbT',
                                                        'meta_section_mix_WNewWetBulbT',
                                                        'meta_section_mix_WNewRelativeT'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inWetBulbT', 'newWetBulbT', 'newRelativeT'],
                                                    newValue, 'NewRelativeT', unitSetting, metaUnit, metaLayout, propUnit
                                                )}

                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.NARatio"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir1(
                                                    'meta_section_mix_NARatio',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewWetBulbT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInWetBulbT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NAVolume',
                                                        'meta_section_mix_WInRelativeT',
                                                        'meta_section_mix_WNewRelativeT',
                                                        'meta_section_mix_NARatio'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'navolume', 'inRelativeT', 'newRelativeT', 'naratio'],
                                                    newValue, 'NARatio', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.NAVolume"
                                                onBlurChange={(event, newValue, previousValue) => onCalcMixAir4(
                                                    'meta_section_mix_NAVolume',
                                                    'meta_section_mix_WNewDryBulbT',
                                                    'meta_section_mix_WNewWetBulbT',
                                                    'meta_section_mix_WInDryBulbT',
                                                    'meta_section_mix_WInWetBulbT',
                                                    ['meta_section_mix_WOutDryBulbT',
                                                        'meta_section_mix_WOutWetBulbT',
                                                        'meta_section_mix_WOutRelativeT',
                                                        'meta_section_mix_NARatio',
                                                        'meta_section_mix_WInRelativeT',
                                                        'meta_section_mix_WNewRelativeT',
                                                        'meta_section_mix_NAVolume'
                                                    ],
                                                    ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'naratio', 'inRelativeT', 'newRelativeT', 'navolume'],
                                                    newValue, 'NAVolume', unitSetting, metaUnit, metaLayout, propUnit
                                                )}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="col-lg-12">
                                            <Field id="meta.section.mix.WOutDryBulbT" />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WOutWetBulbT"
                                            />
                                        </div>
                                        <div className="col-lg-12">
                                            <Field
                                                id="meta.section.mix.WOutRelativeT"
                                            />
                                        </div>
                                    </div>
                                </Group> : ''}
                            </div>
                        </div>
                    </div>

                    <Group title={intl.get(MIX_FORM)} id="group1">
                        {/* <div className="col-lg-3">
                            <Field id="meta.section.mix.damperTopType" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.damperBackType" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.damperLeftType" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.damperRightType" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.damperBottomType" />
                        </div> */}
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.AAVolume" />
                        </div>
                        <div className="col-lg-3" style={{ marginTop: '24px' }}>
                                <Button
                                    type="button" className="btn btn-primary"
                                    size='small' type="primary"
                                    style={{ backgroundColor: '#337ab7' }}
                                    onClick={() => {
                                        onsyncAVolume()
                                    }}>
                                    {intl.get(Append_Air_Volume)}
                                </Button>
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.appendAirVolume" />
                        </div>
                        
                    </Group>
                    {/* <Group title={intl.get(MIX_FORM)} id="group1">
                        <div className="col-lg-12" style={{ textAlign: 'center' }}>
                            <div className="col-lg-2">

                            </div>
                            <div className="col-lg-2">
                                A
                            </div>
                            <div className="col-lg-2">
                                B
                            </div>
                            <div className="col-lg-2">
                                C
                            </div>
                            <div className="col-lg-2">
                                D
                            </div>
                            <div className="col-lg-2">

                            </div>
                                </div>*/}
                        {/* <div className="col-lg-2"> */}
                        <div className="col-lg-12">
                            <div className="col-lg-12">
                                <Field label={intl.get(TOP_RETURN_AIR)} id="meta.section.mix.returnTop" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(BACK_RETURN_AIR)} id="meta.section.mix.returnBack" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(LEFT_SIDE_RETURN_AIR)} id="meta.section.mix.returnLeft" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(RIGHT_SIDE_RETURN_AIR)} id="meta.section.mix.returnRight" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(BOTTOM_RETURN_AIR)} id="meta.section.mix.returnButtom" />
                            </div>
                        </div>
                        {/*<div className="col-lg-2">
                            <div className="col-lg-12">
                                <Field label={intl.get(TOP_RETURN_AIR)} id="meta.section.mix.damperTopPosSizeA" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(BACK_RETURN_AIR)} id="meta.section.mix.damperBackPosSizeA" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(LEFT_SIDE_RETURN_AIR)} id="meta.section.mix.damperLeftPosSizeA" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(RIGHT_SIDE_RETURN_AIR)} id="meta.section.mix.damperRightPosSizeA" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(BOTTOM_RETURN_AIR)} id="meta.section.mix.damperBottomPosSizeA" />
                            </div>
                        </div>
                        <div className="col-lg-2">
                            <div className="col-lg-12">
                                <Field label={intl.get(TOP_RETURN_AIR)} id="meta.section.mix.damperTopPosSizeB" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(BACK_RETURN_AIR)} id="meta.section.mix.damperBackPosSizeB" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(LEFT_SIDE_RETURN_AIR)} id="meta.section.mix.damperLeftPosSizeB" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(RIGHT_SIDE_RETURN_AIR)} id="meta.section.mix.damperRightPosSizeB" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(BOTTOM_RETURN_AIR)} id="meta.section.mix.damperBottomPosSizeB" />
                            </div>
                        </div>


                        <div className="col-lg-2">
                            <div className="col-lg-12">
                                <Field label={intl.get(TOP_RETURN_AIR)} id="meta.section.mix.damperTopPosSizeC" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(BACK_RETURN_AIR)} id="meta.section.mix.damperBackPosSizeC" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(LEFT_SIDE_RETURN_AIR)} id="meta.section.mix.damperLeftPosSizeC" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(RIGHT_SIDE_RETURN_AIR)} id="meta.section.mix.damperRightPosSizeC" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(BOTTOM_RETURN_AIR)} id="meta.section.mix.damperBottomPosSizeC" />
                            </div>

                        </div>
                        <div className="col-lg-2">
                            <div className="col-lg-12">
                                <Field label={intl.get(TOP_RETURN_AIR)} id="meta.section.mix.damperTopPosSizeD" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(BACK_RETURN_AIR)} id="meta.section.mix.damperBackPosSizeD" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(LEFT_SIDE_RETURN_AIR)} id="meta.section.mix.damperLeftPosSizeD" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(RIGHT_SIDE_RETURN_AIR)} id="meta.section.mix.damperRightPosSizeD" />
                            </div>
                            <div className="col-lg-12">
                                <Field label={intl.get(BOTTOM_RETURN_AIR)} id="meta.section.mix.damperBottomPosSizeD" />
                            </div>

                        </div> */}
                        {/* <div className="col-lg-2">
                            <div className="col-lg-12" style={{ margin: '1px 0' }}>
                                <Button
                                    type="button" className="btn btn-primary"
                                    size='small' type="primary"
                                    style={{ backgroundColor: '#337ab7' }}
                                    disabled={!returnTop}
                                    onClick={() => {
                                        this.setState({ visible: true, type: 'Top' })
                                    }}> */}
                                    {/* {intl.get(CONFIRM)} */}
                                    {/* 自定义顶部风阀
                                </Button>
                            </div>
                            <div className="col-lg-12" style={{ margin: '1px 0' }}>
                                <Button
                                    type="button" className="btn btn-primary"
                                    size='small' type="primary"
                                    style={{ backgroundColor: '#337ab7' }}
                                    disabled={!returnBack}
                                    onClick={() => {
                                        this.setState({ visible: true, type: 'Back' })
                                    }}> */}
                                    {/* {intl.get(CONFIRM)} */}
                                    {/* 自定义后风阀
                                </Button>
                            </div>
                            <div className="col-lg-12" style={{ margin: '1px 0' }}>
                                <Button
                                    type="button" className="btn btn-primary"
                                    size='small' type="primary"
                                    style={{ backgroundColor: '#337ab7' }}
                                    disabled={!returnLeft}
                                    onClick={() => {
                                        this.setState({ visible: true, type: 'Left' })
                                    }}> */}
                                    {/* {intl.get(CONFIRM)} */}
                                    {/* 自定义左侧风阀
                                </Button>
                            </div>
                            <div className="col-lg-12" style={{ margin: '1px 0' }}>
                                <Button
                                    type="button" className="btn btn-primary"
                                    size='small' type="primary"
                                    style={{ backgroundColor: '#337ab7' }}
                                    disabled={!returnRight}
                                    onClick={() => {
                                        this.setState({ visible: true, type: 'Right' })
                                    }}> */}
                                    {/* {intl.get(CONFIRM)} */}
                                    {/* 自定义右侧风阀
                                </Button>
                            </div>
                            <div className="col-lg-12" style={{ margin: '1px 0' }}>
                                <Button
                                    type="button" className="btn btn-primary"
                                    size='small' type="primary"
                                    style={{ backgroundColor: '#337ab7' }}
                                    disabled={!returnButtom}
                                    onClick={() => {
                                        this.setState({ visible: true, type: 'Bottom' })
                                    }}> */}
                                    {/* {intl.get(CONFIRM)} */}
                                    {/* 自定义底部风阀
                                </Button>
                            </div>
                        </div>
                        {this.state.visible ? <Dampermodal
                            visible={this.state.visible}
                            // handleOk={() => { this.handleOk() }}
                            onCustomDamper={this.props.onCustomDamper}
                            handleCancel={(e) => this.handleCancel(e)}
                            componentValue={componentValue}
                            componentValues={componentValues}
                            type={this.state.type}
                            selectedComponent = {selectedComponent}
                        /> : ''}
                    </Group> */}
                    <Group title={intl.get(AIR_VALVE_OPTION)} id="group3">
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.damperOutlet" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.damperMeterial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.damperExecutor" />
                        </div>
                    </Group>
                    <Group title={intl.get(OTHER_OPTION)} id="group3">
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.DoorO" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.DoorDirection" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.uvLamp" onChange={(e) => { this.props.onchangeUvLamp(e.target.value) }}/>
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.uvLampSerial" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.PositivePressureDoor" />
                        </div>
                        <div className="col-lg-9">
                            <Field id="meta.section.mix.fixRepairLamp" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.freshAirHood" />
                        </div>
                    </Group>
                    <Group title={intl.get(RESISTANCE_AND_WEIGHT)} id="group5">
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.outletPressure" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.Resistance" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.sectionL" />
                        </div>
                        <div className="col-lg-3">
                            <Field id="meta.section.mix.Weight" />
                        </div>
                    </Group>
                    <Group className="btnGroup" title={intl.get(OPERATION)} id="group5">
                        <div className="col-lg-12" style={{ paddingBottom: '10px' }}>
                            <Button
                                //  type="button" className="btn btn-primary"
                                size='small' type="primary"
                                style={{ backgroundColor: '#337ab7' }}
                                disabled={!this.props.canConfirm || this.props.invalid}
                                onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                                {intl.get(CONFIRM)}
                            </Button>
                            <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                                <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                            </Tooltip>
                        </div>
                    </Group>
                </div>
            </form>
        )
    }
    handleCancel(e) {
        this.setState({ visible: e })
    }
}

class Dampermodal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            A: 1,
            B: 1,
            C: 1,
            D: 1,
        }
    }
    handleChange(a, b) {
        this.setState({
            [b]: a
        })
    }
    onOk(handleCancel) {
        let { type, onCustomDamper, componentValues, selectedComponent } = this.props
        let {A, B, C, D} = this.state
        let { meta_ahu_product, meta_ahu_serial } = componentValues[0]
        let unitMetaJson = {
            'meta.ahu.eairvolume':componentValues[0]['meta_ahu_eairvolume'],
            'meta.ahu.sairvolume':componentValues[0]['meta_ahu_sairvolume']
        }
        let obj ={
            [`meta.section.mix.damper${type}ModeA`]:A,
            [`meta.section.mix.damper${type}ModeB`]:B,
            [`meta.section.mix.damper${type}ModeC`]:C,
            [`meta.section.mix.damper${type}ModeD`]:D
        }
        let sectionMetaJson = {...componentValues[selectedComponent['id']], ...obj}
        let newObj = {}
        for (let variable in sectionMetaJson) {
            if (sectionMetaJson.hasOwnProperty(variable)) {
                newObj[variable.replace(/_/gi, '.')] = sectionMetaJson[variable]
            }
        }
        let param = {
            "unit": {
                "product": meta_ahu_product,
                "series": meta_ahu_serial,
                "metaJson": JSON.stringify(unitMetaJson)
            },
            "section": {
                "sectionKey": "ahu.mix",
                "metaJson": JSON.stringify(newObj)
            }
        }
        onCustomDamper(type, param, obj, ()=>{handleCancel(false)})
    }
    render() {
        let { visible,
            handleOk,
            handleCancel,
            componentValue,
            componentValues,
            type,
            onCustomDamper
             } = this.props

        let returnBack = componentValue ? eval(componentValue.meta_section_mix_returnBack) : false
        let returnButtom = componentValue ? eval(componentValue.meta_section_mix_returnButtom) : false
        let returnLeft = componentValue ? eval(componentValue.meta_section_mix_returnLeft) : false
        let returnRight = componentValue ? eval(componentValue.meta_section_mix_returnRight) : false
        let returnTop = componentValue ? eval(componentValue.meta_section_mix_returnTop) : false
        let height = componentValues && componentValues[0] && componentValues[0]['meta_ahu_serial'] ? componentValues[0]['meta_ahu_serial'].substr(-4, 2) : '06'
        let width = componentValues && componentValues[0] && componentValues[0]['meta_ahu_serial'] ? componentValues[0]['meta_ahu_serial'].substr(-2, 2) : '08'
        let length = componentValue ? eval(componentValue.meta_section_mix_sectionL) : '2'
            
        let defaultA = componentValue[`meta_section_mix_damper${type}ModeA`]
        let defaultB = componentValue[`meta_section_mix_damper${type}ModeB`]
        let defaultC = componentValue[`meta_section_mix_damper${type}ModeC`]
        let defaultD = componentValue[`meta_section_mix_damper${type}ModeD`]
        let AOption = []
        let BOption = []
        let COption = []
        let DOption = []


        if (type == 'Top' || type == 'Bottom') {
            let AOptionNum = Number(length) / 2
            for (let i = 1; i <= AOptionNum; i++) {
                AOption.push(i)
                BOption.push(i)
            }
            let COptionNum = Number(width) / 2
            for (let i = 1; i <= COptionNum; i++) {
                COption.push(i)
                DOption.push(i)
            }
        }
        if (type == 'Left' || type == 'Right') {
            let AOptionNum = Number(length) / 2
            for (let i = 1; i <= AOptionNum; i++) {
                AOption.push(i)
                BOption.push(i)
            }
            let COptionNum = Number(height) / 2
            for (let i = 1; i <= COptionNum; i++) {
                COption.push(i)
                DOption.push(i)
            }
        }
        if (type == 'Back') {
            let AOptionNum = Number(width) / 2
            for (let i = 1; i <= AOptionNum; i++) {
                AOption.push(i)
                BOption.push(i)
            }
            let COptionNum = Number(height) / 2
            for (let i = 1; i <= COptionNum; i++) {
                COption.push(i)
                DOption.push(i)
            }
        }
        // console.log('visible, handleOk, handleCancel', defaultD, height, width, length, AOption, BOption, COption, DOption)
        return (
            <Modal
                title={intl.get(CALCULATION)}
                // style={{width:'640px'}}
                visible={visible}
                // onOk={() => { this.onOk(handleCancel) }}
                // onCancel={() => handleCancel(false)}
                footer={[
                    <Button key="back" onClick={() => handleCancel(false)}>
                      {intl.get(CLOSE)}
                    </Button>,
                    <Button key="submit" type="primary"  onClick={() => { this.onOk(handleCancel) }}>
                      {intl.get(OK)}
                    </Button>,
                  ]}
            >
                <Row style={{ textAlign: 'center' }}>
                    <Col span={4}></Col>
                    <Col span={5}>A</Col>
                    <Col span={5}>B</Col>
                    <Col span={5}>C</Col>
                    <Col span={5}>D</Col>
                </Row>
                <Row>
                    <Col span={4}>
                        {type == 'Top' ? <Checkbox defaultChecked={returnTop} disabled /> : type == 'Back' ? <Checkbox defaultChecked={returnBack} disabled /> : type == 'Left' ? <Checkbox defaultChecked={returnLeft} disabled /> : type == 'Right' ? <Checkbox defaultChecked={returnRight} disabled /> : type == 'Bottom' ? <Checkbox defaultChecked={returnButtom} disabled /> : ''}
                        {type == 'Top' ? '顶部回风' : type == 'Back' ? '后回风' : type == 'Left' ? '左侧回风' : type == 'Right' ? '右侧回风' : type == 'Bottom' ? '底部回风' : ''}
                    </Col>
                    <Col span={5}>
                        <Select defaultValue={defaultA} style={{ width: '90px' }} onChange={(e) => { this.handleChange(e, 'A') }}>
                            {
                                AOption.map((record, index) => {
                                    return (
                                        <Option value={record}>{record}</Option>
                                    )
                                })
                            }
                        </Select>
                    </Col>
                    <Col span={5}>
                        <Select defaultValue={defaultB} style={{ width: '90px' }} onChange={(e) => { this.handleChange(e, 'B') }}>
                            {
                                BOption.map((record, index) => {
                                    return (
                                        <Option value={record}>{record}</Option>
                                    )
                                })
                            }
                        </Select></Col>
                    <Col span={5}>
                        <Select defaultValue={defaultC} style={{ width: '90px' }} onChange={(e) => { this.handleChange(e, 'C') }}>
                            {
                                COption.map((record, index) => {
                                    return (
                                        <Option value={record}>{record}</Option>
                                    )
                                })
                            }
                        </Select></Col>
                    <Col span={5}>
                        <Select defaultValue={defaultD} style={{ width: '90px' }} onChange={(e) => { this.handleChange(e, 'D') }}>
                            {
                                DOption.map((record, index) => {
                                    return (
                                        <Option value={record}>{record}</Option>
                                    )
                                })
                            }
                        </Select></Col>
                </Row>

            </Modal>
        )
    }

}


export default reduxForm({
    form: 'mix', // a unique identifier for this form
    enableReinitialize: true,
})(Mix)

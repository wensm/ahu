import {
    SUMMER,
    WINTER,
    INLET_WIND_TEMP_SUMMER_PAREN,
    INLET_WIND_TEMP_WINTER_PAREN,
    ELECTRIC_HEATING_OPTION,
    PERFORMANCE,
    OPERATION,
    CONFIRM,
    CHOOSE_MODEL,
    CALCULATION,
    CALCULATION_RESULT,
    OK,
    CANCEL,
    STAGES,
    ROW_NUMBER,
    INVALID
} from '../../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { required, maxLength15, minValue0, maxValue40 } from '../../ahu/Validate'
import { Modal, Table, Button, Spin, Icon, Tooltip } from 'antd'


class ElectricHeatingCoil extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            selectedRowKeys: [],
            selectedRows: [],
            spinning: false
        }
    }
    handleOk(ElectricHeatingCoil) {
        let obj = {}
        for (let key in this.state.selectedRows[0]) {
            obj[`${ElectricHeatingCoil['sectionKey']}.${key}`] = this.state.selectedRows[0][key]
        }
        this.setState({ modal: false }, () => this.props.onhandleOk(obj))
    }
    handleCancel() {
        this.setState({
            modal: false,
            selectedRowKeys: []
        })
    }
    onSelectChange = (selectedRowKeys, selectedRows) => {
        this.setState({
            selectedRowKeys: selectedRowKeys,
            selectedRows
        });
    }
    render() {
        const {
            isCompleted,
            onCalcRelativeHumidity,
            onCalcRelativeHumidity2,
            onCalcWetBulbTemperature,
            onCompleteSection,
            onCalcHumDryBulbT,
            onCalcHeatingQ,
            unitSetting,
            metaUnit,
            metaLayout,
            propUnit,
            onCalculateEHC,
            ElectricHeatingCoil,
            componentValue
        } = this.props
        let enableSummer = componentValue ? eval(componentValue.meta_section_electricHeatingCoil_EnableSummer) : false
        let enableWinter = componentValue ? eval(componentValue.meta_section_electricHeatingCoil_EnableWinter) : false

        let { selectedRowKeys } = this.state
        const columns = [{
            title: intl.get(STAGES),
            dataIndex: 'GroupC',
            key: 'GroupC',
            // render: text => <a href="javascript:;">{text}</a>,
        }, {
            title: intl.get(ROW_NUMBER),
            dataIndex: 'RowNum',
            key: 'RowNum',
        }];
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
            type: 'radio'
        };
        let data = []
        if (typeof ElectricHeatingCoil != 'string') {
            data = ElectricHeatingCoil.resultData
        }

        let bool = (!enableSummer && enableWinter) ? false : true
        return (
            <form data-name="ElectricHeatingCoil">
                <div>
                    <ul className="nav nav-tabs" role="tablist" style={{ marginBottom: '10px' }}>
                        <li role="presentation" className={bool ? "active" : ""}><a href="#summer" aria-controls="summer"
                            role="tab" data-toggle="tab">{intl.get(SUMMER)}</a>
                        </li>
                        <li role="presentation" className={bool ? "" : "active"}><a href="#winter" aria-controls="winter" role="tab"
                            data-toggle="tab">{intl.get(WINTER)}</a></li>
                    </ul>
                    <div className="tab-content">
                        <div role="tabpanel" className={bool ? "tab-pane active" : "tab-pane "} id="summer">

                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.electricHeatingCoil.EnableSummer" />
                            </div>
                            {enableSummer ? <Group title={intl.get(INLET_WIND_TEMP_SUMMER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electricHeatingCoil.SInDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_electricHeatingCoil_SInWetBulbT', newValue, 'meta_section_electricHeatingCoil_SInRelativeT', 'meta_section_electricHeatingCoil_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.SInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_electricHeatingCoil_SInDryBulbT', newValue, 'meta_section_electricHeatingCoil_SInRelativeT', 'meta_section_electricHeatingCoil_SInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.SInRelativeT" maxValue={100}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_electricHeatingCoil_SInDryBulbT', newValue, 'meta_section_electricHeatingCoil_SInWetBulbT', 'meta_section_electricHeatingCoil_SInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.SHeatQ"
                                            onBlurChange={(event, newValue, previousValue) => onCalcHumDryBulbT(
                                                'S',
                                                'meta_section_electricHeatingCoil_SInDryBulbT',
                                                'meta_section_electricHeatingCoil_SInWetBulbT',
                                                'meta_section_electricHeatingCoil_SInRelativeT',
                                                newValue,
                                                ['meta_section_electricHeatingCoil_SOutDryBulbT',
                                                    'meta_section_electricHeatingCoil_SOutWetBulbT',
                                                    'meta_section_electricHeatingCoil_SOutRelativeT',
                                                    'meta_section_electricHeatingCoil_SHumDryBulbT',
                                                    'meta_section_electricHeatingCoil_SHeatQ'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humDryBulbT', 'heatQ'], unitSetting, metaUnit, metaLayout, propUnit
                                            )}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.SHumDryBulbT"
                                            warn={[required, maxValue40, minValue0]}
                                            onBlurChange={(event, newValue, previousValue) => onCalcHeatingQ(
                                                'S',
                                                'meta_section_electricHeatingCoil_SInDryBulbT',
                                                'meta_section_electricHeatingCoil_SInWetBulbT',
                                                'meta_section_electricHeatingCoil_SInRelativeT',
                                                newValue,
                                                ['meta_section_electricHeatingCoil_SOutDryBulbT',
                                                    'meta_section_electricHeatingCoil_SOutWetBulbT',
                                                    'meta_section_electricHeatingCoil_SOutRelativeT',
                                                    'meta_section_electricHeatingCoil_SHeatQ',
                                                    'meta_section_electricHeatingCoil_SHumDryBulbT'
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'heatQ', 'humDryBulbT'], unitSetting, metaUnit, metaLayout, propUnit
                                            )}
                                        />
                                    </div>

                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electricHeatingCoil.SOutDryBulbT" />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.SOutWetBulbT"

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.SOutRelativeT"

                                        />
                                    </div>
                                </div>
                            </Group> : ''}
                        </div>
                        <div role="tabpanel" className={bool ? "tab-pane" : "tab-pane active"} id="winter">
                            <div style={{ marginBottom: '5px' }}>
                                <Field id="meta.section.electricHeatingCoil.EnableWinter" />
                            </div>
                            {enableWinter ? <Group title={intl.get(INLET_WIND_TEMP_WINTER_PAREN)} id="group1">
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electricHeatingCoil.WInDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity2('meta_section_electricHeatingCoil_WInWetBulbT', newValue, 'meta_section_electricHeatingCoil_WInRelativeT', 'meta_section_electricHeatingCoil_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.WInWetBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcRelativeHumidity('meta_section_electricHeatingCoil_WInDryBulbT', newValue, 'meta_section_electricHeatingCoil_WInRelativeT', 'meta_section_electricHeatingCoil_WInWetBulbT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.WInRelativeT" maxValue={100}
                                            onBlurChange={(event, newValue, previousValue) => onCalcWetBulbTemperature('meta_section_electricHeatingCoil_WInDryBulbT', newValue, 'meta_section_electricHeatingCoil_WInWetBulbT', 'meta_section_electricHeatingCoil_WInRelativeT', unitSetting, metaUnit, metaLayout, propUnit)}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.WHeatQ"
                                            validate={[required, maxLength15, minValue0]}
                                            onBlurChange={(event, newValue, previousValue) => onCalcHumDryBulbT(
                                                'W',
                                                'meta_section_electricHeatingCoil_WInDryBulbT',
                                                'meta_section_electricHeatingCoil_WInWetBulbT',
                                                'meta_section_electricHeatingCoil_WInRelativeT',
                                                newValue,
                                                ['meta_section_electricHeatingCoil_WOutDryBulbT',
                                                    'meta_section_electricHeatingCoil_WOutWetBulbT',
                                                    'meta_section_electricHeatingCoil_WOutRelativeT',
                                                    'meta_section_electricHeatingCoil_WHumDryBulbT',
                                                    'meta_section_electricHeatingCoil_WHeatQ',
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'humDryBulbT', 'heatQ'], unitSetting, metaUnit, metaLayout, propUnit
                                            )}
                                        />
                                    </div>

                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.WHumDryBulbT"
                                            onBlurChange={(event, newValue, previousValue) => onCalcHeatingQ(
                                                'W',
                                                'meta_section_electricHeatingCoil_WInDryBulbT',
                                                'meta_section_electricHeatingCoil_WInWetBulbT',
                                                'meta_section_electricHeatingCoil_WInRelativeT',
                                                newValue,
                                                ['meta_section_electricHeatingCoil_WOutDryBulbT',
                                                    'meta_section_electricHeatingCoil_WOutWetBulbT',
                                                    'meta_section_electricHeatingCoil_WOutRelativeT',
                                                    'meta_section_electricHeatingCoil_WHeatQ',
                                                    'meta_section_electricHeatingCoil_WHumDryBulbT',
                                                ],
                                                ['outDryBulbT', 'outWetBulbT', 'outRelativeT', 'heatQ', 'humDryBulbT'], unitSetting, metaUnit, metaLayout, propUnit
                                            )}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4">
                                    <div className="col-lg-12">
                                        <Field id="meta.section.electricHeatingCoil.WOutDryBulbT" />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.WOutWetBulbT"

                                        />
                                    </div>
                                    <div className="col-lg-12">
                                        <Field
                                            id="meta.section.electricHeatingCoil.WOutRelativeT"

                                        />
                                    </div>
                                </div>
                            </Group> : ''}
                        </div>
                    </div>
                </div>
                <Group title={intl.get(ELECTRIC_HEATING_OPTION)} id="group7">
                    <div className="col-lg-3">
                        <Field id="meta.section.electricHeatingCoil.RowNum" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.electricHeatingCoil.GroupC" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.electricHeatingCoil.Material" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.electricHeatingCoil.BaffleM1" />
                    </div>
                </Group>
                <Group title={intl.get(PERFORMANCE)} id="group9">
                    <div className="col-lg-3">
                        <Field id="meta.section.electricHeatingCoil.Resistance" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.electricHeatingCoil.sectionL" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.electricHeatingCoil.Weight" />
                    </div>
                    <div className="col-lg-3">
                        <Field id="meta.section.electricHeatingCoil.appendAirVolume" />
                    </div>
                </Group>
                <Group className="btnGroup" title={intl.get(OPERATION)} id="group10">
                    <div className="col-lg-3">
                        <Button
                            //  type="button" className="btn btn-primary"
                            style={{ backgroundColor: '#337ab7' }}
                            size='small' type="primary"

                            onClick={() => {
                                this.setState({ modal: true, spinning: true }, onCalculateEHC(this.props.componentValues, this.props.selectedComponent, "", unitSetting, metaUnit, metaLayout, propUnit, () => { this.setState({ spinning: false }) }))
                            }}>
                            {intl.get(CALCULATION)}

                        </Button>
                    </div>
                    <div className="col-lg-3" style={{ paddingBottom: '10px' }}>
                        <Button
                            // type="button" className="btn btn-primary" 
                            disabled={!this.props.canConfirm || this.props.invalid}
                            style={{ marginLeft: '10px', backgroundColor: '#337ab7' }}
                            size='small' type="primary"

                            onClick={() => onCompleteSection(this.props, propUnit, unitSetting, metaUnit, metaLayout)}>
                            {intl.get(CONFIRM)}
                        </Button>
                        <Tooltip title={intl.get(INVALID)} placement="bottom" style={{ display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }}>
                            <Icon type="question-circle" style={{ marginLeft: '5px', display: !this.props.canConfirm || this.props.invalid ? 'inline-block' : 'none' }} />
                        </Tooltip>
                    </div>
                </Group>
                <Modal
                    title={intl.get(CALCULATION_RESULT)}
                    visible={this.state.modal}
                    destroyOnClose={true}
                    onOk={() => this.handleOk()}
                    onCancel={() => this.handleCancel()}
                    footer={[
                        <Button key="back" onClick={() => this.handleCancel()}>
                            {intl.get(CANCEL)}
                        </Button>,
                        <Button key="submit" type="primary" onClick={() => this.handleOk(ElectricHeatingCoil)}>
                            {intl.get(OK)}
                        </Button>

                    ]}
                >
                    <Spin spinning={this.state.spinning} style={{ textAlign: 'center' }}>
                        {typeof ElectricHeatingCoil == 'string' && ElectricHeatingCoil != '' ?
                            <div style={{ textAlign: 'center' }}>
                                <span style={{ color: 'red' }}> {intl.get(`label.${ElectricHeatingCoil}`)}</span>
                            </div>
                            : <Table rowSelection={rowSelection} columns={columns} dataSource={data} />}
                    </Spin>
                </Modal>
            </form>
        )
    }
}

export default reduxForm({
    form: 'ElectricHeatingCoil', // a unique identifier for this form
    enableReinitialize: true,
})(ElectricHeatingCoil)

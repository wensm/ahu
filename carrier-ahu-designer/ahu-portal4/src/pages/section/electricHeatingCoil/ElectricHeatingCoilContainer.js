import React from 'react'
import {connect} from 'react-redux'
import { reduxForm } from 'redux-form'
import Group from '../../ahu/Group'
import Field from '../../ahu/Field'
import { confirmSection, handleOk} from '../../../actions/ahu'
import ElectricHeatingCoil from './ElectricHeatingCoil'

const mapStateToProps = state => {
  return {
    ElectricHeatingCoil:state.ahu.ElectricHeatingCoil,
    initialValues: state.ahu.componentValue[state.ahu.selectedComponent.id],
    isCompleted: state.ahu.componentValue[state.ahu.selectedComponent.id] && state.ahu.componentValue[state.ahu.selectedComponent.id].meta_section_completed,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
      onhandleOk:(obj)=>{
        dispatch(handleOk(obj))
      }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ElectricHeatingCoil)

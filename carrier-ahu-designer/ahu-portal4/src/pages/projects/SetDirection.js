import {
    PLEASE_CHOOSE_UNIT,
    PLEASE_CHOOSE_DIRECTION,
    DIRECTION_SETTING,
    PIPE_CONNECTION_DIRECTION,
    PLEASE_CHOOSE,
    TO_LEFT,
    TO_RIGHT,
    ACCESS_DOOR_SIDE,
    CANCEL,
    SAVE,
    SELECT_ALL,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import Header from '../ahu/Header'
import {Link} from 'react-router'
import GroupDiv from '../groups/GroupDiv'
import sweetalert from 'sweetalert'

export default class SetDirection extends React.Component {

  constructor(props) {
    super(props);
    this.setDirection = this.setDirection.bind(this)
    this.toggleSelectAll = this.toggleSelectAll.bind(this)
  }


  setDirection() {
    let ahuIds = []
    //console.log(1)
    $(".setDirAhuCheckBox").each(function () {
      if ($(this).is(':checked')) {
        ahuIds.push($(this).attr("id"))
      }
    })

    let doorOrientation = $(this.refs.doorOrientation).val()
    let pipeOrientation = $(this.refs.pipeOrientation).val()
    let projectId = $('#SetDirection').data('projectId')
    if (projectId && ahuIds.length&&(doorOrientation||pipeOrientation)) {

      this.props.onSetDirection(projectId,ahuIds,doorOrientation,pipeOrientation)
    }else{
    	if(ahuIds.length == 0){
    		sweetalert({title: intl.get(PLEASE_CHOOSE_UNIT), type: 'warning'})
    	}else if(''==doorOrientation || ''==pipeOrientation){
    		sweetalert({title: intl.get(PLEASE_CHOOSE_DIRECTION), type: 'warning'})
    	}
    }
  }
  
	toggleSelectAll(){
        if($('.col-xs-12 [type=checkbox]:checked').length){
          $('.setDirAhuCheckBox').prop('checked',true)
        }else{
          $('.setDirAhuCheckBox').prop('checked',false)
        }
    }

  render() {
    const {ahuGroupList} = this.props;
    return (
      <div className="modal fade" id="SetDirection" data-name="SetDirection" tabIndex="-1" role="dialog"
           aria-labelledby="myModalLabel" data-groupid="">
        <div className="modal-dialog" style={{width: '800px'}} role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
              <h4 className="modal-title" id="myModalLabel"><b>{intl.get(DIRECTION_SETTING)}</b></h4>
            </div>
            <div className="modal-body" style={{height: '780px', overflow: 'auto'}}>
              <div className="row">

                <div className="col-xs-9">
                  {/*<b>{intl.get(DIRECTION_SETTING)}</b>*/}
                  <label style={{marginLeft: '10px'}}>{intl.get(PIPE_CONNECTION_DIRECTION)}</label>
                  <select ref="pipeOrientation"  style={{marginLeft: '10px'}}>
                    <option value="">{intl.get(PLEASE_CHOOSE)}</option>
                    <option value="left">{intl.get(TO_LEFT)}</option>
                    <option value="right">{intl.get(TO_RIGHT)}</option>
                  </select>
                  <label style={{marginLeft: '10px'}}>{intl.get(ACCESS_DOOR_SIDE)}</label>
                  <select ref="doorOrientation"   style={{marginLeft: '10px'}}>
                    <option value="">{intl.get(PLEASE_CHOOSE)}</option>
                    <option value="left">{intl.get(TO_LEFT)}</option>
                    <option value="right">{intl.get(TO_RIGHT)}</option>
                  </select>
                </div>
                <div className="col-xs-3">
                  <button className="btn btn-default" onClick={()=>{
                    $('#SetDirection').modal('hide')
                  }}>{intl.get(CANCEL)}</button>
                  <button className="btn btn-default" onClick={this.setDirection}
                          style={{marginLeft: '10px'}}>{intl.get(SAVE)}</button>
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12" onClick={this.toggleSelectAll}>
                  <input type="checkbox" name=""/>
                  <b style={{marginLeft: '4px'}}>{intl.get(SELECT_ALL)}</b>
                </div>
              </div>

              <div className="row" style={{marginTop: '20px'}}>
                {
                  ahuGroupList && ahuGroupList.map((group, index) => <GroupDiv key={index} {...group} flg="setDir"/>)
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

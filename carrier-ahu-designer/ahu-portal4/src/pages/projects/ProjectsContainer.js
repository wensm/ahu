import {connect} from 'react-redux'
import Projects from './Projects'
import {
    fetchProjects,
    saveBatchImport,
    saveBatchExport,
    saveBatchNSImport,
    saveBatchNSExport,
    saveCreateReport,
    receiveReport,
    clearReports,
} from '../../actions/projects'

const mapStateToProps = (state, ownProps) => {
    return {
        ahuGroupList: state.groups.ahuGroupList,
        projectList:state.projects.projectList,
        archived: state.projects.archived

    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchProjects(param){
            dispatch(fetchProjects(param))
        },
        onSaveBatchImport(formData,projectId, callBack, archived){
            dispatch(saveBatchImport(formData,projectId, callBack, archived));
        },
        onSaveBatchExport(id, fileName){
            dispatch(saveBatchExport(id, fileName));
        },
        onSaveBatchNSImport(formData,projectId, callBack, archived){
            dispatch(saveBatchNSImport(formData,projectId, callBack, archived));
        },
        onSaveBatchNSExport(id, fileName){
            dispatch(saveBatchNSExport(id, fileName));
        },
        onSaveCreateReport(param, startGeneratingReport){
            dispatch(saveCreateReport(param, startGeneratingReport));
        },
        onReportGenerated(reports){
            dispatch(receiveReport(reports));
        },
        onClearReports(){
            dispatch(clearReports());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Projects)

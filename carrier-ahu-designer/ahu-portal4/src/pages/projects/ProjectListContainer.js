import {
    CONFIRM_TO_DELETE_AHU,
    OK,
    CANCEL,
    CONFIRM_TO_ARCHIEVED_AHU,
    ARCHIVED_CANT_RECOVER
} from '../intl/i18n'

import { connect } from 'react-redux'
import ProjectList from './ProjectList'
import sweetalert from 'sweetalert'
import intl from 'react-intl-universal'
import {
    delProject,
    fetchprokey,
    updateProjectPre,
    toArchieved,
    saveAs
} from '../../actions/projects'
import {
    fetchAhuGroupList, clickReport
} from '../../actions/groups'

const mapStateToProps = state => {
    return {
        projectList: state.projects.projectList,
        language: state.general.language,
        projectFilterStr: state.projects.projectFilterStr,
        archived: state.projects.archived

    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDelProject(projectId, archived) {
            sweetalert({
                title: intl.get(CONFIRM_TO_DELETE_AHU),
                text: "Once deleted, you will not be able to recover this project!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: intl.get(OK),
                cancelButtonText: intl.get(CANCEL),
                closeOnConfirm: true
            }, isConfirm => {
                if (isConfirm) {
                    dispatch(delProject(projectId, archived));
                }
            })

        },
        onFetchProkey(projectId) {
            dispatch(fetchprokey(projectId));
        },
        onUpdateProjectPre(projectId) {
            dispatch(updateProjectPre(projectId));
        },
        onFetchAhuGroupList(projectId) {
            dispatch(fetchAhuGroupList(projectId));
        },
        onClickReport(projectId, callBack) {
            dispatch(clickReport(projectId, callBack))
        },
        onToArchieved(projectId, archived) {
            sweetalert({
                title: intl.get(CONFIRM_TO_ARCHIEVED_AHU),
                text: intl.get(ARCHIVED_CANT_RECOVER),
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: intl.get(OK),
                cancelButtonText: intl.get(CANCEL),
                closeOnConfirm: true
            }, isConfirm => {
                if (isConfirm) {
                    dispatch(toArchieved(projectId, archived));

                }
            })
        },
        onSaveAs(projectId, archived, projectName, callBack) {
            console.log('projectId', projectId)
            dispatch(saveAs(projectId, projectName, archived, callBack))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectList)

import intl from 'react-intl-universal'
import React from 'react'
import loadingImg from '../../images/loding.gif'

import {
    CONFIGURING
} from '../intl/i18n'

function WebClient() {
  WebClient.prototype.isopen = false;
  this.webSocket = null;
  this.connectUrl = function (url) {
    this.webSocket = new WebSocket(url);
    this.webSocket.onerror = function (event) {
      WebClient.prototype.callerrorbak(event);
    }
    this.webSocket.onopen = function (event) {
      WebClient.prototype.setConnFlag(true);
      WebClient.prototype.callopenbak(event);
    }
    this.webSocket.onclose = function (event) {
      WebClient.prototype.callclosebak(event);
      WebClient.prototype.unInit();
    }
    this.webSocket.onmessage = function (event) {
      WebClient.prototype.callmsgbak(event.data);
    }
    return true;
  };
  if (typeof WebSocket._callmsgbak == "undefined") {
    WebClient.prototype.callmsgbak = function (data) {

    }
    WebClient._callmsgbak = true;
  }
  if (typeof WebClient._callopenbak == "undefined") {
    WebClient.prototype.callopenbak = function (data) {

    }
    WebClient._callopenbak = true;
  }
  if (typeof WebClient._callerrorbak == "undefined") {
    WebClient.prototype.callerrorbak = function (data) {

    }
    WebClient._callerrorbak = true;
  }
  if (typeof WebClient._callclosebak == "undefined") {
    WebClient.prototype.callclosebak = function (data) {

    }
    WebClient._callclosebak = true;
  }
  if (typeof WebClient._isConn == "undefined") {
    WebClient.prototype.isConn = function (data) {
      return WebClient.prototype.isopen;
    }
    WebClient._isConn = true;
  }
  if (typeof WebClient._setConnFlag == "undefined") {
    WebClient.prototype.setConnFlag = function (flag) {
      WebClient.prototype.isopen = flag;
    }
    WebClient._setConnFlag = true;
  }
  if (typeof WebClient._connect == "undefined") {
    WebClient.prototype.connect = function (url, msgbak, openbak, errorbak, closebak) {
      WebClient.prototype.callmsgbak = msgbak;
      WebClient.prototype.callopenbak = openbak;
      WebClient.prototype.callerrorbak = errorbak;
      WebClient.prototype.callclosebak = closebak;
      if (this.webSocket == null)//表示连接成功
      {
        this.connectUrl(url);
      }
      else if (this.webSocket.readyState == 3) {
        this.webSocket = null;
        this.connectUrl(url);
      }
      if (this.webSocket.readyState != 1) {
        return false;
      }
      return true;
    }
    WebClient._connect = true;
  }

  if (typeof WebClient._sendData == "undefined") {
    WebClient.prototype.sendData = function (data) {
      if (WebClient.prototype.isConn() && this.webSocket != null) {
        this.webSocket.send(data);
        return true;
      }
      else {
        return false;
      }
    }
    WebClient._sendData = true;
  }
  if (typeof WebClient._close == "undefined") {
    WebClient.prototype.close = function (data) {
      this.webSocket.close(1000, "Closeing");
      WebClient.prototype.isopen = false;
      this.webSocket = null;
      return true;
    }
    WebClient._close = true;
  }
  if (typeof WebClient._uninit == "undefined") {
    WebClient.prototype.unInit = function (data) {
      WebClient.prototype.isopen = false;
      this.webSocket = null;
      return true;
    }
    WebClient._uninit = true;
  }
}


export default class SocketPanel extends React.Component {

  constructor(props) {
    super(props);
    this._lastUrl = "";
    this.title = this.props.title || intl.get(CONFIGURING);
  }


  clearContent() {
    $(this.refs.infoContent).html("")
  }


    start(socketUrl, finishMsg) {
        const ignoreMsgs = ["No Data"];
        const RESULT = "RESULT:";
    
        finishMsg = finishMsg || "#OVER#";
        this.clearContent();
        if (!socketUrl || (this._ws && this._lastUrl === socketUrl)) {
          return;
        }
        if (this._ws && this._ws.isopen) {
          this._ws.close()
        }
        clearInterval(this._timer);
        this._lastUrl = socketUrl;
        this._ws = new WebClient();
        this._ws.connect(socketUrl, (msg) => {
            // console.log("handleSocketMsg[" + (this.props.panelName || '') + "]:" + msg);
            if(msg.startsWith(RESULT) && this.props.onResult) {
                this.props.onResult(msg.substring(RESULT.length));
            } else {
                if (this.refs.infoContent) {
                    if (ignoreMsgs.indexOf(msg) === -1) {
                        $(this.refs.infoContent).append('<div class="info-item"> - ' + msg + '</div>')
                    }
                    $(this.refs.infoContent).parent().scrollTop($(this.refs.infoContent).height())
                }
                if (finishMsg && msg === finishMsg && this.props.onGetFinishMsg) {
                  this.props.onGetFinishMsg(msg);
                }
            }
        }, (open) => {
          this._ws.sendData("@heart")
          this.props.onOpen && this.props.onOpen()
          this._timer = setInterval(() => {
            if (this._ws && this._ws.isopen) {
              this._ws.sendData("@heart")
            }
          }, 2000)
        }, (err) => {
          this._ws = null;
          this.props.onError && this.props.onError()
        }, (close) => {
          this._ws = null;
          clearInterval(this._timer);
        });
    }

  stop() {
    clearInterval(this._timer);
    this._ws && this._ws.isopen && this._ws.close();
  }

  render() {

    return (<div className="socket-panel">
      <div className="socket-panel-title" style={{lineHeight:"2.4em",background:'#2461c0', paddingLeft:'15px'}}><img style={{width: "1.6em", marginRight: 10}} src={loadingImg}
                                               alt=""/> {this.title}...
      </div>
      <div style={{overflow:'hidden', border:'#ccc 1px solid', margin:'15px 15px 34px' }}>
	      <div className="socket-panel-content"
	           style={{height: '18em',width: '100%' ,marginLeft:'20px', lineHeight: "1.8em", overflow: "auto", padding: "0 0 1.8em 0", textAlign:'left'}}>
	        <div ref="infoContent">
	
	        </div>
	      </div>
      </div>
    </div>)
  }
}

import {connect} from 'react-redux'

import BatchCalculation from './BatchCalculation'
import {saveQutSet,updateProject,updateProjectCalcData} from '../../actions/groups'

const mapStateToProps = (state, owProps) => {
    return {
        initialValues:{"judge-select":1},
        sysPort: state.general.sysPort,

    }
}

const mapDispatchToProps = (dispatch, owProps) => {
    return {
        onSaveQutSet(groupId, ahuId, selType, isButton, obj, onSuccess, onFailed) {
            dispatch(saveQutSet(owProps.projectId, groupId, ahuId, selType, isButton, obj, onSuccess,onFailed))
        },
        onUpdateGroups(){
          dispatch(updateProject(owProps.projectId))
        },
        onUpdateProjectCalcData(){
          dispatch(updateProjectCalcData(owProps.projectId))
        }

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BatchCalculation)

import {
    MOVE_GROUP,
    CANCEL,
		CONFIRM_MOVE_GROUP,
		WILL_MOVE_TO
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
export default class MoveGroup extends React.Component {
  constructor(props) {
    super(props);
    this.saveMoveGroup = this.saveMoveGroup.bind(this);
  }
  saveMoveGroup(){
  	var ahuId = $("#MoveGroup").data("ahuid");
  	var groupId = $("#groupSelect").val();
  	this.props.onSaveMoveGroup(ahuId,groupId);
  }
  render() {
    const {canMoveList} = this.props;
    return (
			<div className="modal fade" id="MoveGroup" data-name="MoveGroup" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" data-ahuid="">
			  <div className="modal-dialog" role="document">
			    <div className="modal-content">
			      <div className="modal-header">
			        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 className="modal-title" id="myModalLabel"><b>{intl.get(MOVE_GROUP)}</b></h4>
			      </div>
			      <div className="modal-body">
			      	<div className="row">
			      		<div className="col-lg-4">
			      			{<span id="moveGroupAhuName" style = {{paddingRight:'5px'}}></span>} 
									{intl.get(WILL_MOVE_TO)}
									
			      		</div>
			      		<div className="col-lg-8">
			      			<select id="groupSelect" style={{marginLeft:'10px',width:'150px',height:'34px'}}>
				          		{canMoveList.map((group,index) => <option value={group.groupId}>{group.groupName}</option>)}
				            </select>
			      		</div>
			      	</div>
			      </div>
			      <div className="modal-footer">
			        <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CANCEL)}</button>
			        <button type="button" className="btn btn-primary" onClick={this.saveMoveGroup}>{intl.get(CONFIRM_MOVE_GROUP)}</button>
			      </div>
			    </div>
			  </div>
			</div>
    );
  }
}

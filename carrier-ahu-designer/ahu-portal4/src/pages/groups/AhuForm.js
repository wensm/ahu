import {
    ASTERISK_NUMBER_1_999,
    EDIT,
    NEW_UNIT,
    UNIT_NUMBER,
    UNIT_NAME,
    UNIT_QUANTITY,
    CANCEL,
    SAVE,
    UNIT_SERIES,
    TIPS_FOR_2,
    CUSTOMIZE,
    IS_PRINT_DIFF_UNIT_NAME
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import http from '../../misc/http'
import { Field, reduxForm } from 'redux-form'
import DateUtil from '../../misc/DateUtils'
import classnames from 'classnames'
import style from './AhuForm.css'
import { required, maxLength15, minValue0, invalidCharacter, noBlank, tips1, tips2} from '../ahu/Validate'
import { Checkbox, Modal, Button, Row, Col } from 'antd'
import sweetalert from 'sweetalert'

const renderField = ({
    input,
    label,
    type,
    meta: { touched, error, warning}
  }) => {
    return (
        <div>
                <input {...input} type={type} id="unit_name"/>
                {touched && (error && <div id="checkNameMsg" style = {{color:'red', paddingLeft:'5px'}}>{error}</div>)}
                {touched && (warning && <div id="checkNameMsgWarning" style = {{color:'rgb(202, 180, 16)', paddingLeft:'5px'}}>{warning}</div>)}
        </div>
    )
}



class AhuForm extends React.Component {
    constructor(props) {
        super(props)
        this.onCheckUsefulNm = this.onCheckUsefulNm.bind(this)
        this.saveAhu = this.saveAhu.bind(this)
        this.cancelEditAhu = this.cancelEditAhu.bind(this)
        this.state = {
            authFormChecked: true,
            isFirstInto:'Y'
        }
    }

    componentDidMount() {
        const { onCheckUsefulNm, initialValues } = this.props
        // this.getUsefulNm()

    }

    renderTextarea(field) {
        let {
            input,
            label,
            className,
            type,
            readOnly,
            disabled,
            prefix,
            fieldType,
            meta: { touched, error, warning, asyncValidating },
            ManualWarn,
            warnsAlarm,
            toRed,
            from
        } = field
        return (
            <div>
                <textarea {...input} id="unit_name" value={input.value != undefined ? input.value : ''} type={type} disabled={disabled} readOnly={readOnly} cols="34" rows="5" />
                {touched && (error && <div id="checkNameMsg" style = {{color:'red', paddingLeft:'5px'}}>{error}</div>)}
                {touched && (warning && <div id="checkNameMsgWarning" style = {{color:'rgb(202, 180, 16)', paddingLeft:'5px'}}>{warning}</div>)}
            </div>
        )
    }

    onCheckUsefulNm(event) {

        var name = event.target.name
        var value = event.target.value
        if (name == "unit.unitNo") {
            if (value == "") {
                $("#checkNmMsg").text(intl.get(ASTERISK_NUMBER_1_999))
                $("#checkNmMsg").show()
            } else {
                this.props.onCheckUsefulNm(value)
            }
        }
    }

    saveAhu(close) {
        if (!$("#checkNmMsg").is(':visible') && !$("#checkNameMsg").is(':visible')) {
            var ahu = JSON.parse(JSON.stringify(this.props.ahuForm))
            if (this.props.groupid && this.props.groupType != null) {
                ahu.unit['groupId'] = this.props.groupid
                ahu.unit['groupType'] = this.props.groupType
            }
            ahu.unit['unitNo'] = this.props.initialValues.unit.unitNo
            ahu.unit['drawingNo'] = `${Number(this.props.initialValues.unit.unitNo)}`//取消新建页面的图纸编号，但是提交接口的时候，这个参数还要有
            $('#newAhu').data("update", false)
            // console.log($("#newAhu").data("groupid"))
            // console.log($("#newAhu").data("grouptype"))
            // console.log('this.props.initialValues', ahu, this.props)
            let values = {}
            values.name = ahu.unit.name
            values.isPrintDiffUnitName = this.state.authFormChecked
            values.drawingNo = ahu.unit.drawingNo
            values.mount = ahu.unit.mount
            values.unitNo = ahu.unit.unitNo
            values.product = ahu.unit.product
            let unitid = ahu.unit.unitid
            if (unitid) {
                values.unitid = unitid
            }
            let groupId = ahu.unit.groupId
            if (groupId) {
                values.groupId = groupId
            }
            let str = ahu.unit.name
            let count = 0;
            let pos = str.indexOf('&');

            while (pos !== -1) {
            count++;
            pos = str.indexOf('&', pos + 1);
            }
            let canSave = false
            if(this.state.authFormChecked){
                if((ahu.unit.name.indexOf('&')=== -1 && (Number(ahu.unit.mount)==1 ) )|| (count == (Number(ahu.unit.mount) -1 )) ||(ahu.unit.mount==0)){
                    canSave = true;
                }
            }else{
                canSave = true;
            }

            if(canSave){
                this.props.onSaveAhu(values, this.props.page)
                close()
                this.setState({
                    authFormChecked:true,
                    isFirstInto:'Y'
                })
                this.props.onCancelEditAhu()
            }else{
                sweetalert({ 
                    title: intl.get(TIPS_FOR_2),
                     type: 'error',
                    //  timer: 1000, 
                     showConfirmButton: true
                     });

            }

        }
    }

    cancelEditAhu() {
        this.props.onCancelEditAhu()
    }

    onChange(e) {
        // console.log('e', e.target.checked)
        this.setState({
            authFormChecked: e.target.checked
        })
    }
    onChangeForEdit(e) {
        // console.log('e', e.target.checked)
        this.setState({
            authFormChecked: e.target.checked,
            isFirstInto:'N'
        })
    }
    onClose() {
        // console.log('e', e.target.checked)
        this.setState({
            authFormChecked: true,
            isFirstInto:'Y'
        })
    }
    render() {
        const { usefulNumber, checkUseFulNmRes, name, type, visible, close, onCleanUsefulNm, ahuForm, initialValues, isEditAhu, editSeries} = this.props
        // let updating = $('#newAhu').data("update");
        // const renderField = ({
        //     input,
        //     label,
        //     type,
        //     meta: { touched, error, warning }
        // }) => (
        //         <div>
        //             <label>{label}</label>
        //             <input {...input} type={type} />
        //             {
        //                 ((error && <span style={{ color: 'red' }}>{error}</span>) ||
        //                     (warning && <span style={{ color: 'red' }}>{warning}</span>))}

        //         </div>
        //     )
        let groupTypeList = type != 'edit' ? [{id: '39CQ'},{id: '39G'},{id: '39XT'}]:editSeries//新增接口获得编辑的下拉列表
        let { authFormChecked, isFirstInto} = this.state
        if (type == 'edit'){

            if(ahuForm.unit && ahuForm.unit.isPrintDiffUnitName=='true' && isFirstInto=='Y'){
                authFormChecked = true
            }else if (ahuForm.unit && ahuForm.unit.isPrintDiffUnitName=='false' && isFirstInto=='Y'){
                authFormChecked = false
            }

        }
        return (
            <Modal
                visible={visible && name == 'ahu'}
                title={type == 'new' ? intl.get(NEW_UNIT) : intl.get(EDIT)}
                onCancel={() => { this.onClose(); onCleanUsefulNm(); close(); this.props.onCancelEditAhu() }}
                footer={[
                    <Button key="back" onClick={() => { this.onClose();onCleanUsefulNm(); close(); this.props.onCancelEditAhu() }}>{intl.get(CANCEL)}</Button>,
                    <Button key="submit" type="primary" onClick={() => this.saveAhu(close)}>
                        {intl.get(SAVE)}
                    </Button>,
                ]}
            >
                <div
                    // className="collapse"
                    id="newAhu"
                //  data-name="AhuForm"
                // data-groupid=""
                //  data-grouptype=""
                >
                    <form data-name="AhuForm" onChange={this.ahuFormChange} style={{ padding: '0px' }}>
                        <div className={classnames(style.newAhuBody, 'row')}  >
                            {/* <div className="panel-heading"
                                style={{ height: '60px', lineHeight: '40px', backgroundColor: '#4c74ac' }}>
                                <span style={{
                                    fontSize: '16px',
                                    fontWeight: '700',
                                    color: '#ffffff'
                                }}>{updating ? intl.get(EDIT) : intl.get(NEW_UNIT)}</span>
                            </div> */}
                            <div style={{ marginBottom: '10px' }}>
                                <Row>
                                    <Col span={6}>
                                        <label>{intl.get(UNIT_NUMBER)}</label>
                                    </Col>
                                    <Col span={16}>
                                        <Field name="unit.unitNo" id="drawingNo" component="input" type="number"
                                            onChange={(event) => {
                                                if (isEditAhu != event.target.value) {
                                                    this.onCheckUsefulNm(event);
                                                } else {
                                                    onCleanUsefulNm()
                                                }
                                            }}
                                        />
                                        {checkUseFulNmRes.type == "1" &&
                                            <span id="checkNmMsg" style={{
                                                marginLeft: '10px',
                                                color: '#fd4c4a'
                                            }}>* {checkUseFulNmRes.errMsg}</span>
                                        }
                                    </Col>
                                </Row>
                            </div>
                            <div style={{ marginBottom: '10px' }}>
                                <Row>
                                    <Col span={6}>
                                        <label>{intl.get(UNIT_NAME)}</label>
                                    </Col>
                                    <Col span={16}>
                                        {/*<Field name="unit.name" id="name" component={renderField} type="text"*/}
                                        {/*warn={authFormChecked ? [tips1,tips2] : [tips1]}*/}
                                        {/*ref="myName"*/}
                                        {/*validate={[required, invalidCharacter, noBlank]}*/}
                                        {/*/>*/}

                                        <Field
                                        name="unit.name"
                                        id="name"
                                        component={this.renderTextarea}
                                        type="text"
                                        validate={[required, invalidCharacter, noBlank]}
                                        warn={authFormChecked ? [tips1,tips2] : [tips1]}
                                        />
                                    </Col>
                                </Row>
                            </div>
                            <div style={{ paddingLeft: '125px',marginBottom: '10px'}}>
                                <Row>
                                    <Col>
                                        <input type="checkbox" name="unit.isPrintDiffUnitName" id="isPrintDiffUnitName" defaultValue={true} checked={authFormChecked}  onChange={(e) => type != 'edit' ? this.onChange(e):this.onChangeForEdit(e)}/>{intl.get(IS_PRINT_DIFF_UNIT_NAME)}
                                    </Col>
                                </Row>
                            </div>

                            <div style={{ marginBottom: '10px' }}>
                                <Row>
                                    <Col span={6}>
                                        <label>{intl.get(UNIT_QUANTITY)}</label>
                                    </Col>
                                    <Col span={16}>
                                        <Field name="unit.mount" id="mount" type="number" component={renderField}
                                            validate={[required, maxLength15, minValue0]} />
                                    </Col>
                                </Row>
                            </div>
                            <div style={{ marginBottom: '10px' }}>
                                <Row>
                                    <Col span={6}>
                                        <label>{intl.get(UNIT_SERIES)}</label>
                                    </Col>
                                    <Col span={16}>
                                        <Field name="unit.product" id="product" component="select"
                                            //style={{ cursor: type != 'edit' ? 'pointer' : 'not-allowed' }} disabled={type == 'edit' ? true : false}
                                            style={{ cursor: type != 'edit' ? 'pointer' : 'pointer' }} disabled={type == 'edit' ? false : false}
                                        >
                                            {
                                                groupTypeList.map((groupType, index) => {
                                                    return <option key={index} value={groupType.id}>{groupType.id}</option>
                                                })
                                            }
                                        </Field>
                                    </Col>
                                </Row>
                            </div>
                            {/* <div className="panel-body">
                                <div className={classnames(style.newAhuBody, 'row')}> */}
                            {/*<div className="form-group col-md-4 col-sm-6">
                                    <label>{intl.get(UNIT_NUMBER)}</label>
                                    <Field name="unit.unitNo" id="drawingNo" component="input" type="number" style={{color: '#a7a7a7'}}/>
                                </div>*/}
                            {/* <div className="form-group col-md-4 col-sm-6">
                                        <label>{intl.get(UNIT_NUMBER)}</label>
                                        <Field name="unit.unitNo" id="drawingNo" component="input" type="number"
                                            onChange={(event) => this.onCheckUsefulNm(event)}
                                        />
                                        {checkUseFulNmRes.type == "1" &&
                                            <span id="checkNmMsg" style={{
                                                marginLeft: '10px',
                                                color: '#fd4c4a'
                                            }}>* {checkUseFulNmRes.errMsg}</span>
                                        }
                                    </div>
                                    <div className="form-group col-md-4 col-sm-6">
                                        <label>{intl.get(UNIT_NAME)}</label>
                                        <Field name="unit.name" id="name" component="input" type="text" />
                                    </div>
                                    <div className="form-group col-md-4 col-sm-6">
                                        <label>{intl.get(UNIT_QUANTITY)}</label>
                                        <Field name="unit.mount" id="mount" type="number" component="input"
                                            validate={[required, maxLength15, minValue1]} />
                                    </div>
                                    <div className="form-group col-md-4 col-sm-6">
                                        <label>{intl.get(UNIT_SERIES)}</label>
                                        <Field name="unit.product" id="product" component="select">
                                            {
                                                groupTypeList.map((groupType, index) => {
                                                    return <option key={index} value={groupType.id}>{groupType.id}</option>
                                                })
                                            }
                                        </Field>
                                    </div>
                                </div> */}
                            {/* <div className={style.btnGroup}>
                                    <button type="button" className="btn btn-info" role="button" data-toggle="collapse"
                                        href="#newAhu" aria-expanded="false" aria-controls="newAhu"
                                        onClick={this.cancelEditAhu}>{intl.get(CANCEL)}</button>
                                    <button type="button" className="btn btn-primary"
                                        onClick={this.saveAhu}>{intl.get(SAVE)}</button>
                                </div> */}
                            {/* </div> */}
                        </div>
                    </form>
                </div>
            </Modal>
        )
    }
}

export default reduxForm({
    form: 'AhuForm',
    enableReinitialize: true,
})(AhuForm)

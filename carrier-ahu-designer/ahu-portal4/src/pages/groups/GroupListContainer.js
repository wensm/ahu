import {
    CONFIRM_TO_DELETE_GROUP,
    OK,
    CANCEL,
    CONFIRM_TO_DELETE_AHU,
} from '../intl/i18n'

import {connect} from 'react-redux'
import sweetalert from 'sweetalert'
import intl from 'react-intl-universal'
import GroupList from './GroupList'
import {
    delGroup,
    delAhu,
    copyAhu,
    insertAhu,
    fetchCanMoveList,
    fetchGroupSection,
    checkDrawingNo,
    updateGroupPre,
    updateAhuPre,
    cleanGroupList,
    updateUnitSeries
} from '../../actions/groups'


const mapStateToProps = (state, owProps) => {
    return {
        ahuGroupList: state.groups.ahuGroupList,
        projectId: owProps.projectId,
        page:state.groups.page,

    }
}

const mapDispatchToProps = (dispatch, owProps) => {
    return {
        onDelGroup(groupId){
            sweetalert({
                title: intl.get(CONFIRM_TO_DELETE_GROUP),
                text: intl.get('title.moon_intl_str_0607'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: intl.get(OK),
                cancelButtonText: intl.get(CANCEL),
                closeOnConfirm: true
            }, isConfirm => {
                if (isConfirm) {
                    dispatch(delGroup(groupId, owProps.projectId));
                }
            })
        },
        onDelAhu(ahuId){
            sweetalert({
                title: intl.get(CONFIRM_TO_DELETE_AHU),
                text: intl.get('title.moon_intl_str_0606'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: intl.get(OK),
                cancelButtonText: intl.get(CANCEL),
                closeOnConfirm: true
            }, isConfirm => {
                if (isConfirm) {
                    return dispatch(delAhu(ahuId, owProps.projectId));

                }
            })
        },
        onCopyAhu(ahuId){
            dispatch(copyAhu(ahuId, owProps.projectId))
        },
        onInsertAhu(ahuId){
            dispatch(insertAhu(ahuId, owProps.projectId))
        },
        onFetchCanMoveList(ahuId){
            dispatch(fetchCanMoveList(ahuId, owProps.projectId));
        },
        onBatchSelectSet(groupId, ahuId){
            if (groupId) {
                dispatch(fetchGroupSection(owProps.projectId, groupId, ahuId));
            }
        },
        onCheckDrawingNo(ahuId, drawingNo, msgEl, addBox){
            dispatch(checkDrawingNo(ahuId, drawingNo, msgEl, addBox));
        },
        onUpdateGroupPre(groupId){
            dispatch(updateGroupPre(groupId))
        },
        onUpdateAhuPre(AhuId, series){
            dispatch(updateUnitSeries(series))
            dispatch(updateAhuPre(AhuId))
        },
        doCleanGroupList:()=>{
            dispatch(cleanGroupList())
            
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupList)

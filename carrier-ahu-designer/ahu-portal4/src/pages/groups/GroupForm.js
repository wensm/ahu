import {
	LABEL,
	NEW_GROUP,
	GROUP_NAME,
	PLEASE_INPUT_CHINESE_LETTERS_NUMBER_UNDERSCORE,
	MEMO,
	CHOOSE_TYPE,
	PLEASE_CHOOSE,
	CANCEL,
	SAVE,
	EDIT
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react';
import http from '../../misc/http';
import { Field, reduxForm, formValues } from 'redux-form'
import DateUtil from '../../misc/DateUtils';
import classnames from 'classnames'
import style from './GroupForm.css'
import { Modal, Button, Row, Col } from 'antd'
import { GROUP_TYPE_IMAGES } from '../../models/templates.js'

class GroupForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			groupNameCheckNot: false,
			selectedGroup: ""
		}
		this.checkGroupName = this.checkGroupName.bind(this);
		this.saveGroup = this.saveGroup.bind(this);
		// this.cancel = this.cancel.bind(this);
	}

	checkGroupName(event) {
		var name = event.target.name;
		var value = event.target.value;
		var reg = '^[a-zA-Z0-9_\\s\u4e00-\u9fa5]+$';
		if (value.match(reg)) {
			this.setState({ groupNameCheckNot: false });
			this.props.onCheckGroupName(value);
		} else {
			this.setState({ groupNameCheckNot: true });
		}
	}
	saveGroup(close) {
		// console.log('zzf', this.props.reset())
		if (!$("#checkNameMsg").is(':visible')) {
			this.props.onSaveGroup(this.props.groupFrom, this.props.reset);
		}
		close()
		this.setState({selectedGroup:''})
		this.props.onReset()

	}
	// cancel() {
	// 	this.props.onReset()
	// 	$('#newGroup').collapse('hide')
	// }
	render() {
		const { groupTypeList, checkGroupNameRes, name, type, visible, close } = this.props;
		return (
			<Modal
				visible={name == 'groups' && visible}
				title={type == 'new' ? intl.get(NEW_GROUP) : intl.get(EDIT)}
				onCancel={() => {close();this.props.onReset();this.setState({selectedGroup:''})}}
				footer={[
					<Button key="back" onClick={() => {close();this.props.onReset();this.setState({selectedGroup:''})}}>{intl.get(CANCEL)}</Button>,
					<Button key="submit" type="primary" onClick={() => this.saveGroup(close)}>
						{intl.get(SAVE)}
					</Button>,
				]}
			>
				<div
					// className="collapse" 
					id="newGroup"
				// data-name="GroupForm"
				>
					<form data-name="GroupForm" >
						<div className={classnames(style.newGroupBody, 'row')} >
							{/* <div> */}
							{/* <div className="panel-heading" style={{ height: '60px', lineHeight: '40px', backgroundColor: '#4c74ac' }}>
								<span style={{ fontSize: '16px', fontWeight: '700', color: '#ffffff' }}>{intl.get(NEW_GROUP)}</span>
							</div> */}
							<div style={{ marginBottom: '10px' }}>
								<Row>
									<Col span={6}>
										<label>{intl.get(GROUP_NAME)}</label>
									</Col>
									<Col span={16}>
										<Field name="groupName" id="datetimepicker" component="input" type="text" onChange={(event) => this.checkGroupName(event)} />
										{checkGroupNameRes.type == "1" &&
											<span id="checkNameMsg" style={{ marginLeft: '10px', color: '#fd4c4a' }}>* {checkGroupNameRes.errMsg}</span>
										}
										{this.state.groupNameCheckNot && <i style={{ marginLeft: '10px', color: '#fd4c4a' }}>{intl.get(PLEASE_INPUT_CHINESE_LETTERS_NUMBER_UNDERSCORE)}</i>}

									</Col>
								</Row>
							</div>
							<div style={{ marginBottom: '10px' }}>

								<Row>
									<Col span={6}>
										<label>{intl.get(MEMO)}</label>
									</Col>
									<Col span={16}>
										<Field name="groupMemo" id="groupMemo" component="input" type="text" />
									</Col>
								</Row>
							</div>

							<div style={{ marginBottom: '10px' }}>

								<Row>
									<Col span={6}>
										<label>{intl.get(CHOOSE_TYPE)}</label>
									</Col>
									<Col span={16}>
										<Field name="groupType" id="groupType" component="select" style={{ cursor: type != 'edit' ? 'pointer' : 'not-allowed' }}
										    disabled={type == 'edit' ? true : false}
										    onChange={(e) => this.setState({selectedGroup : e.target.value})}>
											<option value="-1"  >--{intl.get(PLEASE_CHOOSE)}--</option>
											{
												groupTypeList.map((groupType, index) => {
													return <option key={index} value={groupType.id}>{intl.get(LABEL + groupType.name)}</option>
												})
											}
										</Field>
									</Col>
								</Row>
							</div>
                            
							<div>
                                <Row>
                                    <Col span={6}>
                                    </Col>
                                    <Col span={16}>
									{name == 'groups' && visible && <img src={GROUP_TYPE_IMAGES[this.props.initialValues&&this.props.initialValues.groupType?this.props.initialValues.groupType:this.state.selectedGroup]} className="img-responsive" />}
                                    </Col>
                                </Row>
							</div>
							{/* <div className="panel-body" style={{ padding: '0px' }}>
								<div className={classnames(style.newGroupBody, 'row')}>
									<div className="form-group col-md-4 col-sm-6">
										<label>{intl.get(GROUP_NAME)}</label>
										<Field name="groupName" id="datetimepicker" component="input" type="text" onChange={(event) => this.checkGroupName(event)} />
										{checkGroupNameRes.type == "1" &&
											<span id="checkNameMsg" style={{ marginLeft: '10px', color: '#fd4c4a' }}>* {checkGroupNameRes.errMsg}</span>
										}
										{this.state.groupNameCheckNot && <i style={{ marginLeft: '10px', color: '#fd4c4a' }}>{intl.get(PLEASE_INPUT_CHINESE_LETTERS_NUMBER_UNDERSCORE)}</i>}
									</div>
									<div className="form-group col-md-4 col-sm-6">
										<label>{intl.get(MEMO)}</label>
										<Field name="groupMemo" id="groupMemo" component="input" type="text" />
									</div>
									<div className="form-group col-md-4 col-sm-6">
										<label>{intl.get(CHOOSE_TYPE)}</label>
										<Field name="groupType" id="groupType" component="select">
											<option value="-1"  >--{intl.get(PLEASE_CHOOSE)}--</option>
											{
												groupTypeList.map((groupType, index) => {
													return <option key={index} value={groupType.id}>{intl.get(LABEL + groupType.name)}</option>
												})
											}
										</Field>
									</div>
								</div>
								{/* <div className={style.btnGroup}>
									<button type="button" className="btn btn-info" onClick={this.cancel} >{intl.get(CANCEL)}</button>
									<button type="button" className="btn btn-primary" onClick={this.saveGroup}>{intl.get(SAVE)}</button>
								</div> 
							</div> */}
						</div>
					</form>
				</div>
			</Modal>

		)
	}
}

export default reduxForm({
	form: 'GroupForm',
	enableReinitialize: true,
})(GroupForm)

import {connect} from 'react-redux'
import MoveGroup from './MoveGroup'
import {
  saveMoveGroup,
} from '../../actions/groups'
const mapStateToProps = (state,owProps) => {
    return {
        canMoveList: state.groups.canMoveList==undefined?{}:state.groups.canMoveList,
    }
}

const mapDispatchToProps = (dispatch,owProps) => {
    return {
    	onSaveMoveGroup(ahuId,groupId){
    		dispatch(saveMoveGroup(ahuId,groupId,owProps.projectId));
    	}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MoveGroup)

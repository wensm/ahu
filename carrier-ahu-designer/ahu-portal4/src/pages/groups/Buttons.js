import {
    BACK,
    NEW_GROUP,
    NEW_UNIT,
    GENERATE_REPORT,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import style from './Buttons.css'
import { Link } from 'react-router'

import { groupPageFilterUpdate, clickReport } from '../../actions/groups'


class Buttons extends React.Component {
    constructor(props) {
        super();
        this.onFilterUpdate = this.onFilterUpdate.bind(this)
    }

    onFilterUpdate(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        clearTimeout(this._onFilterUpdateTimer)
        this._onFilterUpdateTimer = setTimeout(() => {
            this.props.onGroupPageFilterUpdate(value)
        }, 500);
    }

    componentDidMount() {
        $(this.refs.openNewGroup).on('click', () => {
            $("#newAhu").removeClass('in')
        });
        $(this.refs.openNewAhu).on('click', () => {
            $("#newGroup").removeClass('in')
            $(this.refs.buttons).after($("#newAhu"));
        });


    }

    render() {
        let { onClick, onClickReport } = this.props
        return (
            <div className={style.group} ref="buttons">
                <div className="pro_con pull-left">
                    <ul className={style.proCap}>
                        <li>
                            <Link className="iconfont icon-fanhui"
                                to="/">{intl.get(BACK)}</Link>
                        </li>

                        <li className={style.proCapLi} role="button" ref="openNewGroup"
                            // data-toggle="collapse"
                            onClick={() => {
                                onClick('groups', 'new', undefined, undefined, undefined)
                            }}
                        // href="#newGroup"
                        // aria-expanded="false" 
                        // aria-controls="newGroup"
                        >
                            <i className="iconfont icon-xinjiangroup"></i>
                            {intl.get(NEW_GROUP)}
                        </li>

                        <li className={style.proCapLi}
                            // role="button" ref="openNewAhu"
                            // data-toggle="collapse" 
                            // href="#newAhu"
                            onClick={() => {
                                onClick('ahu', 'new', undefined, undefined, undefined)
                            }}
                        // aria-expanded="false"
                        // aria-controls="newAhu"
                        >
                            <i className="iconfont icon-xinzengAHU"></i>
                            {intl.get(NEW_UNIT)}
                        </li>
                        <li className={style.proCapLi}
                            // data-toggle="modal" 
                            // data-target="#CreateReport"
                            onClick={() => {
                                onClickReport(this.props.projectId, () => { $('#CreateReport').modal({ backdrop: 'static', keyboard: false }) })
                            }}
                        >
                            <i className="iconfont icon-baogao"></i>
                            {intl.get(GENERATE_REPORT)}
                        </li>
                    </ul>

                </div>
                <div className="pull-right form-group form-inline">
                    <div className="input-group">
                        <span className="input-group-addon" id="basic-addon1"><i className="fa fa-filter" aria-hidden="true"></i></span>
                        <input type="text" className="form-control" placeholder="Filter String" aria-describedby="basic-addon1" onChange={this.onFilterUpdate} />
                    </div>
                </div>
                <div className={style.clear}></div>
            </div>
        )
    }
}


const mapStateToProps = (state, owProps) => {
    return {
        ahuFilterStr: state.groups.ahuFilterStr
    }
}

const mapDispatchToProps = (dispatch, owProps) => {
    return {
        onGroupPageFilterUpdate(filterStr) {
            dispatch(groupPageFilterUpdate(filterStr))
        },
        onClickReport(projectId, callBack) {
            dispatch(clickReport(projectId, callBack))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Buttons)

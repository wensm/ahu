import {connect} from 'react-redux'
import GroupForm from './GroupForm'
import {
  saveGroup,
	checkGroupName,
	reset
} from '../../actions/groups'

const mapStateToProps = (state,owProps) => {
	 
    return {
    	initialValues: state.groups.updateGroup,
    	groupTypeList:state.groups.groupTypeList==undefined?[]:state.groups.groupTypeList,
    	projectId: owProps.projectId,
    	checkGroupNameRes: state.groups.checkGroupNameRes,
    	groupFrom: state.form.GroupForm == undefined?{}:state.form.GroupForm.values,
    }
}

const mapDispatchToProps = (dispatch,owProps) => {
    return {
    	onSaveGroup(group, callBack){
    		group['projectId']=owProps.projectId;
	      dispatch(saveGroup(group, callBack))
	    },
	    onCheckGroupName(name){
	    	dispatch(checkGroupName(name,owProps.projectId));
		},
		onReset(){
	    	dispatch(reset());
			
		}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupForm)

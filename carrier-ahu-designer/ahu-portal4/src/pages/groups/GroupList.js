import React from 'react'
import classnames from 'classnames'
import {connect} from 'react-redux'
import style from './GroupList.css'

import GroupDiv from './GroupDiv'
export default class SectionList extends React.Component {
    render() {
        const {onCleanGroupList, projectId,ahuGroupList,onDelGroup,onDelAhu,onCopyAhu,onInsertAhu,onFetchCanMoveList,onBatchSelectSet,onCheckDrawingNo,onUpdateGroupPre,onUpdateAhuPre, onClick} = this.props
		return (
        	<div data-name="GroupList" className={classnames(style.projectDiv)}>
        		{
				 ahuGroupList.map((group, index) => <GroupDiv key={index} {...group} 
				 											projectId={projectId} 
				 											onDelGroup={onDelGroup} 
				 											onDelAhu={onDelAhu} 
				 											onCopyAhu={onCopyAhu} 
				 											onInsertAhu={onInsertAhu}
				 											onFetchCanMoveList={onFetchCanMoveList}
				 											onBatchSelectSet={onBatchSelectSet}
				 											onCheckDrawingNo={onCheckDrawingNo}
				 											onUpdateGroupPre={onUpdateGroupPre}
															 onUpdateAhuPre={onUpdateAhuPre}
															 onClick={onClick}
															 onCleanGroupList = {onCleanGroupList}
				 									/>)
				}
        	</div>
        )
    }
}

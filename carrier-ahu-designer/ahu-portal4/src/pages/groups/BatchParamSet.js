import {
    PARAMETER_BATCH_SETTING,
    IMPORT,
    EXPORT,
    CANCEL,
    OPEN_FILE,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
export default class BatchImport extends React.Component {
	constructor(props) {
	    super(props);
	    this.BatchImport = this.BatchImport.bind(this);
	    this.BatchExport = this.BatchExport.bind(this);
	}
	BatchImport(){
		var groupId = $("#BatchParamSet").data("groupid");
		$('#BatchImport').data("groupid",groupId);
		$('#BatchImport').modal('show');
		$("#BatchParamSet").data("groupid","");
		$("#BatchParamSet").modal('hide');
	}
	BatchExport(){
		var groupId = $("#BatchParamSet").data("groupid");
		$('#BatchExport').data("groupid",groupId);
		$('#BatchExport').modal('show');
		$("#BatchParamSet").data("groupid","");
		$("#BatchParamSet").modal('hide');
	}
  render() {
    const data = this.props.data;
    return (
			<div className="modal fade" id="BatchParamSet" data-name="BatchParamSet" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" data-groupid="">
			  <div className="modal-dialog" role="document">
			    <div className="modal-content">
			      <div className="modal-header">
			        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 className="modal-title" id="myModalLabel"><b>{intl.get(PARAMETER_BATCH_SETTING)}</b></h4>
			      </div>
			      <div className="modal-body" style={{height:'300px'}}>
			      	<button type="button" className="btn btn-primary" style={{marginRight:'10px'}} onClick={this.BatchImport}><i className="iconfont icon-daoru" style={{marginRight:'5px'}}></i>{intl.get(IMPORT)}</button>
			      	<button type="button" className="btn btn-primary" onClick={this.BatchExport}><i className="iconfont icon-daochu" style={{marginRight:'5px'}}></i>{intl.get(EXPORT)}</button>
			      </div>
			      <div className="modal-footer">
			        <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CANCEL)}</button>
			        <button type="button" className="btn btn-primary" onClick={this.saveBatchImport}>{intl.get(OPEN_FILE)}</button>
			      </div>
			    </div>
			  </div>
			</div>
    );
  }
}

import { connect } from 'react-redux'
import {
  CANCEL,
  OK,
  CONFIRMCONFIGBOTTOM
} from '../intl/i18n'
import BatchSelectSet from './BatchSelectSet'
import {
  saveGroupOptionForm,
  saveQutSet,
} from '../../actions/groups'
import sweetalert from 'sweetalert'
import React from 'react'
import { Modal } from 'antd';

const confirm = Modal.confirm;

const mapStateToProps = (state, owProps) => {
  return {
    ahuHasSecList: state.groups.ahuHasSecList,
    initialValues: state.groups.groupSecList[0],
    basicGroupSecList: state.groups.basicGroupSecList && state.groups.basicGroupSecList[0],
    // initialValues: {},
    // groupOptionForm: state.form.GroupOptionForm == undefined ? {} : state.form.GroupOptionForm.values,
    batchComponentValue: state.groups.groupSecList[0],
    layout: state.metadata.layout,
    preferredLocale: state.general.user && state.general.user.preferredLocale
    // groupOptionForm: state.groups.groupSecList == undefined?{}:state.groups.groupSecList[0],
  }
}

const mapDispatchToProps = (dispatch, owProps) => {
  return {
    onSaveGroupOptionForm(groupId, groupOptionForm, basicGroupSecList, type, layout, preferredLocale) {
      // console.log('groupOptionForm', groupOptionForm, basicGroupSecList)
      if (type == 'only') {
        let obj = {}
        let content = []

        for (let key in groupOptionForm) {
          if (groupOptionForm[key] != basicGroupSecList[key]) {
            obj[key] = groupOptionForm[key]
            let old = basicGroupSecList[key]
            let newV = groupOptionForm[key]
            let oldValue = old
            let newValue = newV
            let _key = key.replace(/_/gi, '.')
            let currentLayout = layout[_key]
            let language = preferredLocale == "zh-CN" ? 'memo' : 'name'
            let keyName = currentLayout[language]
            let sectionName = key.split('_')
            sectionName = sectionName[sectionName.length - 2].toUpperCase()
            let type = currentLayout['valueType'] == "BooleanV" ? 'checkbox' : currentLayout['valueType'] == "IntV" ? 'input' : 'selected'
            if (type == 'selected') {
              let indexold = ''
              let indexnew = ''
              currentLayout.option.optionPairs.forEach((ele, index) => {
                if (ele.option == old) {
                  indexold = index
                }
                if (ele.option == newV) {
                  indexnew = index
                }
              })
              let opairs = currentLayout.option.optionPairs
              oldValue = preferredLocale == "zh-CN" ? (opairs[indexold] && opairs[indexold]['clabel']) : (opairs[indexold] && opairs[indexold]['label'])
              newValue = preferredLocale == "zh-CN" ? (opairs[indexnew] && opairs[indexnew]['clabel']) : (opairs[indexnew] && opairs[indexnew]['label'])
            } else if (type == 'input') {
              if (old == '') oldValue = preferredLocale == "zh-CN" ? '无' : 'Null'
              if (newV == '') newValue = preferredLocale == "zh-CN" ? '无' : 'Null'
            }
            let differentName = (<div key={key} style={{ marginBottom: '4px' }}><span><span style={{ fontWeight: '900' }}>{keyName}({sectionName}): &nbsp;&nbsp;</span>  From<span style={{ padding: '0 5px' }}>{`${oldValue}`}</span>To<span style={{ paddingLeft: '5px', color: 'red' }}>{`${newValue}`}</span></span></div>)
            content.push(differentName)
          }
        }

        confirm({
          title: intl.get(CONFIRMCONFIGBOTTOM),
          content: content,
          okText: intl.get(OK),
          okType: 'primary',
          cancelText: intl.get(CANCEL),
          zIndex: 2000,
          width: 600,
          onOk() {
            dispatch(saveGroupOptionForm(owProps.projectId, groupId, obj))

          },
          onCancel() {
          },
        });
        //   sweetalert({
        //     title: sp,
        //     // text: sp,
        //     type: "warning",
        //     showCancelButton: true,
        //     confirmButtonColor: "#DD6B55",
        //     confirmButtonText: intl.get(OK),
        //     cancelButtonText: intl.get(CANCEL),
        //     closeOnConfirm: false,
        //     // closeOnCancel: false
        // },
        //     function (isConfirm) {
        //         if (isConfirm) {
        //         } else {
        //         }
        //     })

      } else {
        dispatch(saveGroupOptionForm(owProps.projectId, groupId, groupOptionForm))
      }
    },
    onSaveQutSet(groupId, selType) {
      dispatch(saveQutSet(owProps.projectId, groupId, selType));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BatchSelectSet)

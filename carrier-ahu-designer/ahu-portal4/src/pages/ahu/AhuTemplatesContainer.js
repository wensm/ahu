import { connect } from 'react-redux'
import AhuTemplates from './AhuTemplates'
import { receiveAhuInfo, receiveAhuSections, ahuSerial } from '../../actions/ahu'

const mapStateToProps = (state, ownProps) => {
  return {
    templates: state.ahu.templates,
    projectId: ownProps.projectId,
    page:state.groups.page,
    validation: state.ahu.validation,
    ahuSerial: ahuSerial(state.ahu.componentValue[0]),
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onLoadAhuTemplate: templateId => {
      const templates = ownProps.templates
      const template = templates.find(template => template.id === templateId)
        let tmpAhuObj = JSON.parse(template.ahuContent)
        let tmpSectionsObj = JSON.parse(template.sectionContent)
        let ahu = {unit: tmpAhuObj, parts: tmpSectionsObj, templateId:templateId}
        let {propUnit, unitSetting, metaLayout, metaUnit} = ownProps
      	dispatch(receiveAhuInfo(ahu, unitSetting, metaUnit, metaLayout, propUnit))
      // dispatch(receiveAhuSections(JSON.parse(template.sectionContent)))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AhuTemplates)

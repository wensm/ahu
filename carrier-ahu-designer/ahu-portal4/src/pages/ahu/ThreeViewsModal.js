import {
  RESTORE,
  ENLARGE,
  CLOSE,
  EXPORT_DWG_FILE,
  TRIPLE_VIEW,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import { onThreeViewModalClosed, fetchThreeViewDwg } from '../../actions/ahu'
import loadingImg from '../../images/loding.gif'
class ThreeViewsModal extends React.Component {

  constructor(props) {
    super(props)
    this.clearThreeView = this.clearThreeView.bind(this)
    this.exportDwgFile = this.exportDwgFile.bind(this)

    // this.toggleExpand = this.toggleExpand.bind(this)

    this.state = {
      expanded: false
    }
  }

  clearThreeView() {
    $('#threeViewModal').on('hidden.bs.modal', () => {
      this.props.clearThreeView()
    })
  }

  componentDidMount() {
    this.clearThreeView()
  }
  componentWillReceiveProps() {
    if (!$('#threeViewModal img').attr('hasToBig')) {
    setTimeout(() => {
      $('#threeViewModal img').mousewheel(function (event, delta) {
        let dir = delta > 0 ? 'Up' : 'Down';
        let widthh = $('#threeViewModal img').width() * 1.02
        let widthhh = $('#threeViewModal img').width() * 0.98
        $('#threeViewModal img').css('width', widthh)
        $('#threeViewModal img').css('height', widthh)
        if (dir == 'Up') {
          $('#threeViewModal img').css('width', widthh)
          $('#threeViewModal img').css('height', widthh)
          $('#threeViewModal #threeViewModalWidth').css('width', widthh+100)
        } else {
          $('#threeViewModal img').css('width', widthhh)
          $('#threeViewModal img').css('height', widthhh)
          $('#threeViewModal #threeViewModalWidth').css('width', widthhh+100)
        }
        return false;
      });
      $('#threeViewModal img').attr('hasToBig', true)
    }, 300)
    }
  }

  exportDwgFile() {
    this.props.generateDwg(this.props.ahuId)
  }

  // toggleExpand(){
  //   this.setState({
  //     expanded :!this.state.expanded
  //   })
  // }

  render() {
    const { threeView, threeViewDwg } = this.props
    return (
      <div className="modal fade" id="threeViewModal" tabIndex="-1" role="dialog" aria-labelledby="threeViewModalLabel">
        <div className="modal-dialog modal-lg" id="threeViewModalWidth"  role="document">
          <div className="modal-content">
            <div className="modal-header" style={{textAlign:'center'}}>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              {/* <div className="modal-footer" style={{ padding: "0px", border: "none", paddingRight: "24px", float: "right" }}> */}
                {/* {threeView &&  <button type="button" className="btn btn-default" onClick={this.toggleExpand}> {this.state.expanded?intl.get(RESTORE):intl.get(ENLARGE)}</button>} */}
              <h4 className="modal-title" id="threeViewModalLabel" style={{display:'inline-block', marginRight:'50px'}}>{intl.get(TRIPLE_VIEW)}</h4>
                <button type="button" className="btn btn-default" style={{marginRight:'10px'}} onClick={this.exportDwgFile}>{intl.get(EXPORT_DWG_FILE)}</button>
                <button type="button" className="btn btn-default" style={{marginRight:'20px'}} data-dismiss="modal">{intl.get(CLOSE)}</button>
              {/* </div> */}
            </div>
            <div className="modal-body" >
              <div style={{
                //  maxHeight: window.innerHeight,
                //  overflow: 'scroll' 
                }}>
                {threeView ? <img style={{ 
                  // "height": (this.state.expanded ? "auto" : "-webkit-fill-available"),
                  //  display: "inline" 
                   }} src={threeView} className="img-responsive" /> : <img src={loadingImg} style={{ position: 'absolute',height:'30px', width: '30px', top: '0', left: '45%' }} />}
              </div>
              <div>
                {threeViewDwg && <a href={threeViewDwg} target="blank">{intl.get(EXPORT_DWG_FILE)}</a>}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    threeView: state.ahu.threeView
  }
}

const mapDispatchToProps = dispatch => {
  return {
    clearThreeView() {
      dispatch(onThreeViewModalClosed())
    },
    generateDwg(ahuId) {
      fetchThreeViewDwg(ahuId)
    }
  }
}

// function onThreeViewModalClosed() {
//   return {
//     type: THREE_VIEW_MODAL_CLOSED
//   }
// }

export default connect(mapStateToProps, mapDispatchToProps)(ThreeViewsModal)

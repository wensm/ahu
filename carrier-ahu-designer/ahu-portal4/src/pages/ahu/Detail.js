import {
  LABEL,
  UNIT_INFORMATION,
  UNIT_MODEL,
  AIR_VOLUME,
  EXTERNAL_STATIC_PRESSURE,
  SECTION_INFORMATION,
  PART_NAME,
  SECTION_LENGTH,
  RESISTANCE,
  DB_FRESH_AIR,
  WET_BULB_FRESH_AIR,
  DB_RETURN_AIR,
  WET_BULB_RETURN_AIR,
  DB_OUTLET_AIR,
  WET_BULB_OUTLET_AIR,
  DB_OUTLET_AIR_S,
  DB_OUTLET_AIR_W,
  DB_RETURN_AIR_S,
  DB_RETURN_AIR_W,
  WET_BULB_OUTLET_AIR_S,
  WET_BULB_OUTLET_AIR_W,
  WET_BULB_RETURN_AIR_S,
  WET_BULB_RETURN_AIR_W,
  UNIT_NUMBER,
  UNIT_NAME,
  TOTAL_AIR_VOLUME_COLON
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { formatValueForUnit } from '../../actions/ahu'
export default class Detail extends React.Component {
  UNmetaUnit(name) {
    const { metaUnit, propUnit } = this.props
    let un = ""
    if (metaUnit[name]) {
      if (propUnit[name] == "M") {
        un = metaUnit[name].mlabel
      } else {
        un = metaUnit[name].blabel
      }
    }
    return un
  }
  render() {
    const { sectionName, ahuValue, sectionValue, selectedComponent, propUnit, metaUnit, metaLayout, routeLocation } = this.props
    const { query } = routeLocation
    const name = selectedComponent.name.split('.')[1];
    let sName = `meta_section_${selectedComponent.name.split('.')[1]}_EnableSummer`
    let wName = `meta_section_${selectedComponent.name.split('.')[1]}_EnableWinter`
    let enableSummer = sectionValue && eval(sectionValue[sName])
    let enableWinter = sectionValue && eval(sectionValue[wName])
    let isWheelOrPlate = (sectionName == 'plate_heat_recycle' || sectionName == 'wheel_heat_recycle') ? true : false
    console.log('this', this.props)
    return (
      <div data-name="Detail">
        <div className="panel panel-default">
          <div className="panel-heading">{intl.get(UNIT_INFORMATION)}</div>
          <table className="table">
            <tbody>
              <tr>
                <td>{intl.get(UNIT_NUMBER)}</td>
                <td>{query.drawingNo ? query.drawingNo : '- -'}</td>
              </tr>
              <tr>
                <td>{intl.get(UNIT_NAME)}</td>
                <td><p style={{width:'76px',whitespace: 'nowrap',overflow: 'hidden',textOverflow: 'ellipsis',}} title={query.name ? query.name : '- -'}>{query.name ? query.name : '- -'}</p></td>
              </tr>
              <tr>
                <td>{intl.get(UNIT_MODEL)}</td>
                <td>{ahuValue && ahuValue.meta_ahu_serial ? ahuValue.meta_ahu_serial : '- -'}</td>
              </tr>
              {/* <tr>
                <td>{intl.get(AIR_VOLUME)}</td>
                <td>{ahuValue && ahuValue.meta_ahu_sairvolume ? ahuValue.meta_ahu_sairvolume : '- -'} {this.UNmetaUnit('AirVolume1')}</td>
              </tr>
              <tr>
                <td>{intl.get(EXTERNAL_STATIC_PRESSURE)}</td>
                <td>{ahuValue && ahuValue.meta_ahu_sexternalstatic ? ahuValue.meta_ahu_sexternalstatic : '- -'} {this.UNmetaUnit('Pressure1')}</td>
              </tr> */}
              <tr>
                <td>{intl.get(PART_NAME)}</td>
                <td>{sectionName ? intl.get(LABEL + sectionName) : '- -'}</td>
              </tr>

            </tbody>
          </table>
        </div>
        <div className="panel panel-default">
          <div className="panel-heading">{intl.get(SECTION_INFORMATION)}</div>
          <table className="table">
            <tbody>

              <tr>
                <td>{intl.get(SECTION_LENGTH)}</td>
                <td>

                  {
                    (() => {
                      let lengthV = ''
                      if (sectionValue) {
                        lengthV = sectionValue[`meta_section_${name}_sectionL`]
                      }
                      if (lengthV) {
                        lengthV = Number(lengthV)
                        lengthV = this.getSplit(lengthV)
                      }
                      return lengthV ? `${lengthV}` : '- -'
                    })()
                  }
                </td>
              </tr>
              <tr>
                <td>{intl.get(RESISTANCE)}</td>
                <td>
                  {
                    (() => {
                      let Resistance = ''
                      if (sectionValue) {
                        Resistance = sectionValue[`meta_section_${name}_Resistance`]
                        if (name == 'coolingCoil') {
                          Resistance = Number(Resistance)
                        }
                      }
                      if (Resistance) {
                        Resistance = Number(Resistance)
                        Resistance = this.getSplit(Resistance)
                      }
                      return Resistance ? `${Resistance}` : '- -'
                    })()
                  }
                </td>
              </tr>

              <tr>
                <td>{intl.get(TOTAL_AIR_VOLUME_COLON)}</td>
                <td>
                  {
                    (() => {
                      let AirVolume = ''
                      if (sectionValue) {
                        let _airDirection = sectionValue[`meta_section_airDirection`] == 'S' ? '_sairvolume' : '_eairvolume'
                        AirVolume = Number(sectionValue[`meta_section_${name}_appendAirVolume`]) + Number(ahuValue[`meta_ahu${_airDirection}`])
                      }
                      if (AirVolume) {
                        AirVolume = Number(AirVolume)
                        AirVolume = this.getSplit(AirVolume)
                      }
                      if (isWheelOrPlate) {
                        return '- -'
                      } else {
                        return AirVolume ? `${AirVolume}` : '- -'
                      }
                    })()
                  }
                </td>
              </tr>





              {/* <tr>
                <td>{intl.get(DB_FRESH_AIR)}</td>
                <td>
                  {
                    (() => {
                      if(name == 'fan'){
                        return '- -'
                      }

                      let temperature = ''
                      if (sectionValue) {
                        temperature = sectionValue[`meta_section_${name}_SNewDryBulbT`]
                      }
                      temperature = this.getSplit(Number(temperature))
                      return temperature ? `${temperature} ${this.UNmetaUnit('Tempreture')}` : '- -'
                    })()
                  }
                </td>
              </tr> */}
              {/* <tr>
                <td>{intl.get(WET_BULB_FRESH_AIR)}</td>
                <td>
                  {
                    (() => {
                      if(name == 'fan'){
                        return '- -'
                      }
                      let temperature = ''
                      if (sectionValue) {
                        temperature = sectionValue[`meta_section_${name}_SNewWetBulbT`]
                      }
                      temperature = this.getSplit(Number(temperature))
                      return temperature ? `${temperature} ${this.UNmetaUnit('Tempreture')}` : '- -'
                    })()
                  }
                </td>
              </tr> */}
              {
                enableSummer == true ? <tr>
                  <td>{intl.get(DB_RETURN_AIR_S)}</td>
                  <td>
                    {
                      (() => {
                        if (name == 'fan') {
                          return '- -'
                        }
                        let temperature = ''
                        if (sectionValue) {
                          temperature = sectionValue[`meta_section_${name}_SInDryBulbT`]
                          // if (name == 'heatingCoil') {
                          //   temperature = sectionValue[`meta_section_${name}_WInDryBulbT`]
                          // }
                          // if (!temperature) {
                          //   temperature = sectionValue[`meta_section_${name}_WInDryBulbT`]
                          // }
                          temperature = this.getSplit(Number(temperature))

                        }
                        return temperature ? `${temperature} ${this.UNmetaUnit('Tempreture')}` : '- -'
                      })()
                    }
                  </td>
                </tr> : ''
              }
              {
                enableSummer == true ?
                  <tr>
                    <td>{intl.get(WET_BULB_RETURN_AIR_S)}</td>
                    <td>
                      {
                        (() => {
                          if (name == 'fan') {
                            return '- -'
                          }
                          let temperature = ''
                          if (sectionValue) {
                            temperature = sectionValue[`meta_section_${name}_SInWetBulbT`]
                            // if (name == 'heatingCoil') {
                            //   temperature = sectionValue[`meta_section_${name}_WInWetBulbT`]
                            // }
                            // if (!temperature) {
                            //   temperature = sectionValue[`meta_section_${name}_WInWetBulbT`]
                            // }
                            temperature = this.getSplit(Number(temperature))

                          }
                          return temperature ? `${temperature} ${this.UNmetaUnit('Tempreture')}` : '- -'
                        })()
                      }
                    </td>
                  </tr> : ''
              }
              {
                enableSummer == true ?
                  <tr>
                    <td>{intl.get(DB_OUTLET_AIR_S)}</td>
                    <td>
                      {
                        (() => {
                          if (name == 'fan') {
                            return '- -'
                          }
                          let temperature = ''
                          if (sectionValue) {
                            temperature = sectionValue[`meta_section_${name}_SOutDryBulbT`]
                            // if (name == 'heatingCoil') {
                            //   temperature = sectionValue[`meta_section_${name}_WOutDryBulbT`]
                            // }
                            // if (!temperature) {
                            //   temperature = sectionValue[`meta_section_${name}_WOutDryBulbT`]
                            // }
                            temperature = this.getSplit(Number(temperature))

                          }
                          return temperature ? `${temperature} ${this.UNmetaUnit('Tempreture')}` : '- -'
                        })()
                      }
                    </td>
                  </tr> : ''
              }
              {
                enableSummer == true ?
                  <tr>
                    <td>{intl.get(WET_BULB_OUTLET_AIR_S)}</td>
                    <td>
                      {
                        (() => {
                          if (name == 'fan') {
                            return '- -'
                          }
                          let temperature = ''
                          if (sectionValue) {
                            temperature = sectionValue[`meta_section_${name}_SOutWetBulbT`]
                            // if (name == 'heatingCoil') {
                            //   temperature = sectionValue[`meta_section_${name}_WOutWetBulbT`]
                            // }
                            // if (!temperature) {
                            //   temperature = sectionValue[`meta_section_${name}_WOutWetBulbT`]
                            // }
                            temperature = this.getSplit(Number(temperature))

                          }
                          return temperature ? `${temperature} ${this.UNmetaUnit('Tempreture')}` : '- -'
                        })()
                      }
                    </td>
                  </tr> : ''
              }







              {
                enableWinter == true ?
                  <tr>
                    <td>{intl.get(DB_RETURN_AIR_W)}</td>
                    <td>
                      {
                        (() => {
                          if (name == 'fan') {
                            return '- -'
                          }
                          let temperature = ''
                          if (sectionValue) {
                            temperature = sectionValue[`meta_section_${name}_WInDryBulbT`]
                            // if (name == 'heatingCoil') {
                            //   temperature = sectionValue[`meta_section_${name}_WInDryBulbT`]
                            // }
                            // if (!temperature) {
                            //   temperature = sectionValue[`meta_section_${name}_WInDryBulbT`]
                            // }
                            temperature = this.getSplit(Number(temperature))

                          }
                          return temperature ? `${temperature} ${this.UNmetaUnit('Tempreture')}` : '- -'
                        })()
                      }
                    </td>
                  </tr> : ''
              }
              {
                enableWinter == true ?
                  <tr>
                    <td>{intl.get(WET_BULB_RETURN_AIR_W)}</td>
                    <td>
                      {
                        (() => {
                          if (name == 'fan') {
                            return '- -'
                          }
                          let temperature = ''
                          if (sectionValue) {
                            temperature = sectionValue[`meta_section_${name}_WInWetBulbT`]
                            // if (name == 'heatingCoil') {
                            //   temperature = sectionValue[`meta_section_${name}_WInWetBulbT`]
                            // }
                            // if (!temperature) {
                            //   temperature = sectionValue[`meta_section_${name}_WInWetBulbT`]
                            // }
                            temperature = this.getSplit(Number(temperature))

                          }
                          return temperature ? `${temperature} ${this.UNmetaUnit('Tempreture')}` : '- -'
                        })()
                      }
                    </td>
                  </tr> : ''
              }
              {
                enableWinter == true ?
                  <tr>
                    <td>{intl.get(DB_OUTLET_AIR_W)}</td>
                    <td>
                      {
                        (() => {
                          if (name == 'fan') {
                            return '- -'
                          }
                          let temperature = ''
                          if (sectionValue) {
                            temperature = sectionValue[`meta_section_${name}_WOutDryBulbT`]
                            // if (name == 'heatingCoil') {
                            //   temperature = sectionValue[`meta_section_${name}_WOutDryBulbT`]
                            // }
                            // if (!temperature) {
                            //   temperature = sectionValue[`meta_section_${name}_WOutDryBulbT`]
                            // }
                            temperature = this.getSplit(Number(temperature))

                          }
                          return temperature ? `${temperature} ${this.UNmetaUnit('Tempreture')}` : '- -'
                        })()
                      }
                    </td>
                  </tr> : ''
              }
              {
                enableWinter == true ?
                  <tr>
                    <td>{intl.get(WET_BULB_OUTLET_AIR_W)}</td>
                    <td>
                      {
                        (() => {
                          if (name == 'fan') {
                            return '- -'
                          }
                          let temperature = ''
                          if (sectionValue) {
                            temperature = sectionValue[`meta_section_${name}_WOutWetBulbT`]
                            // if (name == 'heatingCoil') {
                            //   temperature = sectionValue[`meta_section_${name}_WOutWetBulbT`]
                            // }
                            // if (!temperature) {
                            //   temperature = sectionValue[`meta_section_${name}_WOutWetBulbT`]
                            // }
                            temperature = this.getSplit(Number(temperature))

                          }
                          return temperature ? `${temperature} ${this.UNmetaUnit('Tempreture')}` : '- -'
                        })()
                      }
                    </td>
                  </tr> : ''
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
  getSplit = (lengthV) => {
    lengthV = lengthV ? Math.floor(lengthV * 100) / 100 : lengthV
    return lengthV
  }
}
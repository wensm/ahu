import {
    SPLIT_SECTION,
    PANEL_ARRANGEMENT,
    PSYCHROMETRIC_CHART,
    TRIPLE_VIEW,
    CALCULATE_PRICE,
    NON_STANDARD,
    STANDARD,
    SAVE,
    IMPORT,
    EXPORT,
    ONE_CLICK_SELECTION,
    ONE_CLICK_REPORT_ERROR,
    MORE,
    WHETHER_OR_NOT_TO_SAVE,
    OK,
    AHU_NOT_SAVE,
    AHU_NOT_COMPLETE,
    AHU_SAVE_TIPS,
    AHU_NOT_CAL_PRICE
} from '../intl/i18n'

import ability from '../../config/ability';
import sweetalert from 'sweetalert'

import intl from 'react-intl-universal'
import React from 'react'
import { Link, browserHistory } from 'react-router'
import BatchCalculationContainer from '../groups/BatchCalculationContainer'


import { Menu, Dropdown, Button, Icon, message, Tooltip } from 'antd';


import style from './Buttons.css'
export default class Buttons extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {}
        this.handle = this.handle.bind(this)
        this.ImportAhu = this.ImportAhu.bind(this)
        this.reportError = this.reportError.bind(this)
        this.router = context.router;
    }
    handle(param) {
        // e.preventDefault()
        //        console.log('zzf2', routerPush)
        routerPush.push(param)
        // browserHistory.push(param)
    }

    exportAllData(ahuId) {
        // window.location = `${SERVICE_URL}` + 'export/unit/' + ahuId
        let a = `${SERVICE_URL}` + 'export/unit/' + ahuId
        window.open(a)
        // window.location = `${SERVICE_URL}` + 'export/unit/' + ahuId
    }
    ImportAhu(projectId, ahuId) {
        $('#ImportAhu').data("ahuId", ahuId)
        $('#ImportAhu').data("projectId", projectId)
        $('#ImportAhu').modal('show')
    }

    reportError(projectId, ahuId) {
        const { onReportError } = this.props
        onReportError(ahuId)
    }

    onBatchCalculation(groupId, ahuId) {
        $("#BatchCalculation").data('groupid', groupId)
        $("#BatchCalculation").data('ahuId', ahuId)
        $('#BatchCalculation').modal({ backdrop: 'static', keyboard: false })

    }
    handleMenuClick(e) {
        // message.info('Click on menu item.');
        // console.log('click', e);
    }
    getComplete(componentValue) {
        let bool = true
        for (let key in componentValue) {
            if (key > 0) {
                if (componentValue[key]['meta_section_completed'] == '' || componentValue[key]['meta_section_completed'] === 'false') {
                    bool = false
                }
            }
        }
        return bool
    }

    // dotoolTip() {
    //     sweetalert({
    //         title: intl.get(WHETHER_OR_NOT_TO_SAVE),
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonText: intl.get(OK),
    //         closeOnConfirm: true
    //     }, isConfirm => {
    //         if (isConfirm) {
    //             console.log('ahuPropsSave', $('#ahuPropsSave').click())
    //         }
    //         // callBack()
    //         // window.location.href = "/";
    //     })
    // }
    componentWillReceiveProps(nextProps) {
        // console.log('next', this.props.doToolTip, nextProps.doToolTip)

        // if (!this.props.doToolTip && nextProps.doToolTip) {
        //    var bottonSetInterval = setInterval(function () {
        //         let op = $('#ahuPropsSave').css("opacity")
        //         if (op == 1) {
        //             $("#ahuPropsSave").css("opacity", 0);
        //         } else if (op == 0) {
        //             $("#ahuPropsSave").css("opacity", 1);
        //         }
        //     }, 1000);

        // } else if (!nextProps.doToolTip) {
        //     bottonSetInterval && clearInterval(bottonSetInterval)
        // }
    }

    render() {
        const {
            componentValue,
            sectionsNum,
            selectedComponent,
            standard,
            projectId,
            ahuId,
            onSaveAhu,
            onFetchPrice,
            onFetchThreeViews,
            onFetchPsychometric,
            onToggleStandard,
            layout,
            currentUnit,
            validation,
            ahuSerial,
            user,
            groupId,
            unitSetting,
            metaUnit,
            metaLayout,
            propUnit,
            onResetTip,
            ahuStatus,
            doToolTip,
            clearThreeView
        } = this.props
        // let ahuStatus = ""
        // if (currentUnit) {
        //     ahuStatus = currentUnit.recordStatus
        // }
        //根据key值来判断  保存  又没有
        let disabled = false
        // if (user && user.userRole == 'Sales' && !validation.pass || user && user.userRole == 'Sales' && !ahuSerial) {
        if (!validation.pass || !ahuSerial) {
            disabled = true
        }

        //console.log('currentUnit', currentUnit)
        let hasCalPrice =  (currentUnit && currentUnit.price && currentUnit.price != 0)
        //let a = currentUnit.price
            //JSON.parse(currentUnit.metaJson)["meta.ahu.boxPrice"];
        let noCalPrice = false;
        if(hasCalPrice){
            noCalPrice = false
        }else{
            noCalPrice = true
        }

        let needCalPrice =  true
        //console.log('hasCalPrice', hasCalPrice)

        let allComplete = this.getComplete(componentValue)
        // console.log('ahuStatus', ahuStatus,  allComplete)

        let panelDisabled = !standard || ahuStatus == 'selecting' || ahuStatus == 'unsplit' || ahuStatus == 'imported' || ahuStatus == undefined
        if (user && (user.userRole == 'Sales' || user.userRole == 'Engineer')) {//2.销售/工程师:面板布置按钮不可用
            panelDisabled = true
        }
        const menu = (
            <Menu onClick={this.handleMenuClick}>
                <Menu.Item key="1">
                    <div type="primary"
                        onClick={() => this.exportAllData(ahuId)} >{intl.get(EXPORT)}
                    </div>
                </Menu.Item>
                <Menu.Item key="2">
                    <div type="primary"
                        onClick={() => this.ImportAhu(projectId, ahuId)} >{intl.get(IMPORT)}
                    </div>
                </Menu.Item>
                <Menu.Item key="3">
                    {/* <div type="primary"
                        onClick={() => this.ImportAhu(projectId, ahuId)} >{intl.get(IMPORT)}
                    </div> */}
                    <div type="primary" disabled={disabled} style={{ marginRight: '5px' }}
                        onClick={() => { onSaveAhu(projectId, ahuId, componentValue, sectionsNum, layout, false, unitSetting, metaUnit, metaLayout, propUnit, () => { this.onBatchCalculation(groupId, ahuId)}, standard ) }}
                        data-toggle="modal"
                    // data-target="#BatchCalculation"
                    >{intl.get(ONE_CLICK_SELECTION)}
                    </div>
                </Menu.Item>
                <Menu.Item key="4">
                    <div type="primary" onClick={() => this.reportError(projectId, ahuId)}>{intl.get(ONE_CLICK_REPORT_ERROR)}</div>
                </Menu.Item>
            </Menu>
        );
        let dis = !standard || ahuStatus == 'selecting' || ahuStatus == 'imported' || ahuStatus == undefined
        let showDisable = !standard || ahuStatus == 'selecting' || ahuStatus == 'unsplit' || ahuStatus == 'imported' || ahuStatus == undefined

        let title = doToolTip ? intl.get(AHU_NOT_SAVE) : intl.get(AHU_NOT_COMPLETE)
        //standard 判断非标/标准
        let standardButton =  ahuStatus == 'selected' ? true : false
        return (
            <div className={style.Buttons}>

                {/* <Link
                    to={`partition/projects/${projectId}/ahus/${ahuId}`}
                    disabled={dis}
                style={{ cursor: !dis ? 'pointer' : 'not-allowed' }}
                > */}

                {dis ? <Tooltip title={title}>
                    <Button type="primary"
                        onClick={() => {
                            // if (doToolTip) {
                            //     this.dotoolTip()
                            // } else {
                            //     this.router.push(`partition/projects/${projectId}/ahus/${ahuId}`);
                            //     onResetTip()
                            // }

                        }}
                        disabled={dis} style={{ marginRight: '5px', backgroundColor: dis ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff', cursor: !dis ? 'pointer' : 'not-allowed' }}>

                        {intl.get(SPLIT_SECTION)}
                    </Button>
                </Tooltip>
                    :
                    <Button type="primary"
                        onClick={() => {
                            // if (doToolTip) {
                            //     this.dotoolTip()
                            // } else {
                            this.router.push(`partition/projects/${projectId}/ahus/${ahuId}`);
                            onResetTip()
                            // }

                        }}
                        disabled={dis} style={{ marginRight: '5px', backgroundColor: dis ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff', cursor: !dis ? 'pointer' : 'not-allowed' }}>

                        {intl.get(SPLIT_SECTION)}
                    </Button>}
                {/* </Link> */}
                {/* <Link
                    to={`panel/projects/${projectId}/ahus/${ahuId}`}
                    disabled={dis}
                style={{ cursor: !dis ? 'pointer' : 'not-allowed' }}
                    
                // style={{ cursor: !dis ? 'pointer' : 'not-allowed' }}


                > */}
                {panelDisabled ? <Tooltip title={title}>
                    <Button type="primary"
                        onClick={() => {
                            // this.props.onPanelInit(ahuId, () => this.router.push(`panel/projects/${projectId}/ahus/${ahuId}`))

                        }}
                        disabled={panelDisabled} style={{ marginRight: '5px', backgroundColor: panelDisabled ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff', cursor: !panelDisabled ? 'pointer' : 'not-allowed' }}>
                        {intl.get(PANEL_ARRANGEMENT)}
                    </Button>
                </Tooltip>
                    : <Button type="primary"
                        onClick={() => {
                            onFetchThreeViews('panel')
                            this.router.push(`panel/projects/${projectId}/ahus/${ahuId}`)
                            //  window.location.reload();
                            // this.props.onPanelInit(ahuId)
                        }}
                        disabled={panelDisabled} style={{ marginRight: '5px', backgroundColor: panelDisabled ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff', cursor: !panelDisabled ? 'pointer' : 'not-allowed' }}>
                        {intl.get(PANEL_ARRANGEMENT)}
                    </Button>}

                {/* </Link> */}
                {showDisable ? <Tooltip title={title}>

                    <Button type="primary" style={{ marginRight: '5px', backgroundColor: showDisable ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                        disabled={showDisable}
                        data-toggle="modal"
                        data-target="#psychometricModal"
                        onClick={() => {
                            // onFetchPsychometric()
                        }
                        }>{intl.get(PSYCHROMETRIC_CHART)}
                    </Button>
                </Tooltip>
                    : <Button type="primary" style={{ marginRight: '5px', backgroundColor: showDisable ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                        disabled={showDisable}
                        data-toggle="modal"
                        data-target="#psychometricModal"
                        onClick={() => onFetchPsychometric()}>{intl.get(PSYCHROMETRIC_CHART)}
                    </Button>}

                {showDisable ? <Tooltip title={title}>

                    <Button type="primary" style={{ marginRight: '5px', backgroundColor: showDisable ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                        disabled={showDisable}
                        data-toggle="modal"
                        data-target="#threeViewModal"
                        onClick={() => {
                            // onFetchThreeViews()
                        }}>{intl.get(TRIPLE_VIEW)}
                    </Button>
                </Tooltip>
                    : <Button type="primary" style={{ marginRight: '5px', backgroundColor: showDisable ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                        disabled={showDisable}
                        data-toggle="modal"
                        data-target="#threeViewModal"
                        onClick={() => { clearThreeView(); onFetchThreeViews() }}>{intl.get(TRIPLE_VIEW)}
                    </Button>}


                {showDisable ? <Tooltip title={title}>

                    <Button type="primary" style={{ marginRight: '5px', backgroundColor: showDisable ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                        disabled={showDisable}
                        data-toggle="modal"
                        data-target="#priceModal" onClick={() => {
                            // onFetchPrice()
                        }}>{intl.get(CALCULATE_PRICE)}
                    </Button>
                </Tooltip>
                    : <Button type="primary" style={{ marginRight: '5px', backgroundColor: showDisable ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                        disabled={showDisable}
                        data-toggle="modal"
                        data-target="#priceModal" onClick={() => onFetchPrice()}>{intl.get(CALCULATE_PRICE)}
                    </Button>}

                {standard && !noCalPrice &&
                    <Button type="primary" style={{ marginRight: '5px', backgroundColor: !standardButton ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                        disabled={!standardButton}
                        onClick={() => { onToggleStandard() }}>{standard ? intl.get(NON_STANDARD) : intl.get(STANDARD)}</Button>}
                {standard && noCalPrice &&
                    <Tooltip title={intl.get(AHU_NOT_CAL_PRICE)}>
                        <Button type="primary" style={{ marginRight: '5px', backgroundColor: needCalPrice ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                                disabled={needCalPrice}
                                onClick={() => { onToggleStandard() }}>{standard ? intl.get(NON_STANDARD) : intl.get(STANDARD)}</Button>
                    </Tooltip>
                }
                {!standard &&
                    <Button type="primary" style={{ marginRight: '5px', backgroundColor: !standardButton ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                            disabled={!standardButton}
                            onClick={() => { onToggleStandard() }}>{standard ? intl.get(NON_STANDARD) : intl.get(STANDARD)}</Button>}
                {/* <Button type="primary" disabled={disabled} style={{ marginRight: '5px', backgroundColor: '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                    onClick={() => { this.onBatchCalculation(groupId, ahuId); onSaveAhu(projectId, ahuId, componentValue, sectionsNum, layout, false, unitSetting, metaUnit, metaLayout) }}
                    data-toggle="modal"
                    data-target="#BatchCalculation"
                >{intl.get(ONE_CLICK_SELECTION)}
                </Button> */}

                {disabled ? <Tooltip title={ intl.get(AHU_NOT_COMPLETE)}>

                    <Button type="primary" disabled={disabled} style={{ marginRight: '5px', backgroundColor: showDisable ? 'rgba(158, 158, 158, 0.83)' : '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}
                    >{intl.get(SAVE)}
                    </Button>
                </Tooltip>
                    : <Tooltip title={ intl.get(AHU_SAVE_TIPS)}>
                        <Button type="primary" id="ahuPropsSave" disabled={disabled} style={{
                        backgroundColor: '#337ab7', bordercolor: '#2e6da4', color: '#fff',
                        opacity: '1'
                    }}
                        onClick={() => onSaveAhu(projectId, ahuId, componentValue, sectionsNum, layout, true, unitSetting, metaUnit, metaLayout, propUnit, undefined, standard)}>{intl.get(SAVE)}
                    </Button>
                    </Tooltip>}


                <Dropdown overlay={menu}>
                    <span style={{ marginLeft: 60, cursor: 'pointer' }}>
                        {intl.get(MORE)} <Icon type="right" />
                    </span>
                    {/* <Button type='primary' style={{ marginLeft: 18, backgroundColor: '#337ab7', bordercolor: '#2e6da4', color: '#fff' }}> */}

                    {/* </Button> */}
                </Dropdown>
                <BatchCalculationContainer projectId={this.props.projectId} isButton={true} location={this.props.location} />
            </div>
        )
    }
}
Buttons.contextTypes = {
    router: React.PropTypes.object.isRequired
};

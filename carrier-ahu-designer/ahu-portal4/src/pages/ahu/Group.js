import React from 'react'
import classnames from 'classnames'
import {connect} from 'react-redux'
import style from './Form.css'
export default class Group extends React.Component {
    render() {
        const { id, title, children,className, showLeftBorder = true} = this.props
        // console.log('id', id)
        const a = "group1"
        return (
            <div data-name="Group" style={{ borderLeft:showLeftBorder?'10px solid':'none', borderLeftColor: {id}.id == {a}.a?'#11FFEE':"#55AAAA"}}
            className={classnames("panel", "panel-default", style[id]) +' '+ className} id={id}>
                <div id={`collapse${id}`} className="panel-collapse collapse in" role="tabpanel" aria-labelledby={`heading${id}`} aria-expanded="false">
                    <div className="panel-body" style={{padding:'5px 15px'}}>
                        <div className="row">{children}</div>
                    </div>
                </div>
            </div>
        )
    }
}

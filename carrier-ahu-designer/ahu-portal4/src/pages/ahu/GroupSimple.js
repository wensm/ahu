import React from 'react'

import style from './Form.css'
import classnames from 'classnames'

export default class GroupSimple extends React.Component {
    render() {
        const { id, title, children } = this.props
        return (
            <div data-name="Group" className="panel panel-default" id={id}>
              <div className="panel-heading" role="tab" id={`heading${id}`} style={{padding:'5px 15px'}}>
                <h4 className="panel-title">
                  <a role="button" data-toggle="collapse" href={`#collapse${id}`} aria-expanded="false" aria-controls={`collapse${id}`} >
                      {title}
                  </a>
                </h4>
              </div>
              <div id={`collapse${id}`} className="panel-collapse collapse in" role="tabpanel" aria-labelledby={`heading${id}`} aria-expanded="false">
                <div className="panel-body" style={{padding:'10px 15px 0px 15px'}}>
                  <div className="row">{children}</div>
                </div>
              </div>
            </div>
        )
    }
}

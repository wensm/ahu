import {AHU_FLOW_ATTR_ARR, RESISTANCE_ATTR_ARR} from '../../actions/ahu'
import calculate from './ChainCalculator.js'
// newComponentValue:当前AHU和所有 section 的属性值
// selectedComponent:当前选中的 AHU 或 SECTION 的 id 和 name (当id=0时，为删除SECTION)
// sections: 当前 AHU 下段的排列顺序，包含 id 和 name
// defaultValue: AHU 和 SECTION 的默认值
//operation:操作，drop为拖动，remove为删除
export default function syncnewComponentValuesDrop(componentValue, selectedComponent, sections, defaultValue, airDirection, operation, direction, templateId) {
    let newComponentValue = JSON.parse(JSON.stringify(componentValue));
    newComponentValue = calculate(sections, newComponentValue, selectedComponent, undefined, operation, direction, templateId)
    //resistanceCalculate(sections, newComponentValue, selectedComponent)
    
    //Find the closed meaning object;
    let meaningObj = getMeaningObj(selectedComponent, newComponentValue, sections, defaultValue);
    // console.log('zzf8', newComponentValue, direction)
    switch (operation!= 'remove') {
        case selectedComponent.name === 'ahu.mix':
            doPropCopy(meaningObj.name, meaningObj.obj, "ahu.mix", newComponentValue[selectedComponent.id]);
            break;
        case selectedComponent.name == 'ahu.filter':
            break;
        case selectedComponent.name == 'ahu.coolingCoil':
            doPropCopy(meaningObj.name, meaningObj.obj, "ahu.coolingCoil", newComponentValue[selectedComponent.id]);
            break;
        case selectedComponent.name == 'ahu.fan':
            var id = selectedComponent.id;
            var obj = newComponentValue['0'];

            //Assign the object tempreture into the injected object;
            //newComponentValue[id]['meta_section_fan_sysStatic'] = obj['meta_ahu_eexternalstatic'];
            //newComponentValue[id]['meta_section_fan_externalStatic'] = obj['meta_ahu_sexternalstatic'];
            //newComponentValue[id]['meta_section_fan_extraStatic'] = 0;
            if(direction == 'S'){
                newComponentValue[id]['meta_section_fan_airVolume'] = obj['meta_ahu_sairvolume'];
                newComponentValue[id]['meta_section_fan_externalStatic'] = obj['meta_ahu_sexternalstatic'];
                newComponentValue[id]['meta_section_fan_totalStatic'] = parseFloat(newComponentValue[id]['meta_section_fan_extraStatic']) + parseFloat(newComponentValue[id]['meta_section_fan_externalStatic']) + parseFloat(newComponentValue[id]['meta_section_fan_sysStatic']);
                newComponentValue[id]['meta_section_fan_power'] = obj['meta_ahu_voltage'];
            
            }else if(direction == 'R'){
                newComponentValue[id]['meta_section_fan_airVolume'] = obj['meta_ahu_eairvolume'];
                newComponentValue[id]['meta_section_fan_externalStatic'] = obj['meta_ahu_eexternalstatic'];
                newComponentValue[id]['meta_section_fan_totalStatic'] = parseFloat(newComponentValue[id]['meta_section_fan_extraStatic']) + parseFloat(newComponentValue[id]['meta_section_fan_externalStatic']) + parseFloat(newComponentValue[id]['meta_section_fan_sysStatic']);
                newComponentValue[id]['meta_section_fan_power'] = obj['meta_ahu_voltage'];
                
            }
            //Update the following sections into incomplete
            break;
        case selectedComponent.name == 'ahu.combinedfilter':
            break;
        default:

    }
    //Update all the following section's complete state as false
    var i = 0;
    var bol = false;
    for (; i < sections.length; i++) {
        newComponentValue[sections[i].id]['assis_section_position'] = i + 1;
        if (sections[i].id == parseInt(selectedComponent.id)) {
            bol = true;
            continue;
        }
        if (!bol) {
            continue;
        } else {

        }
    }
    if (airDirection) {
        updateAirDirection(newComponentValue, airDirection)
    }
    return newComponentValue;
}

function getMeaningObj(selectedComponent, newComponentValue, sections, defaultValue) {
    var id = selectedComponent.id;
    var obj = {"name" : "ahu", "obj": defaultValue};
    var i = 0;
    for (; i < sections.length; i++) {
        if (sections[i].id == id) {
            break;
        }

        var tmp = newComponentValue[parseInt(sections[i].id)];
        if (sections[i].name == 'ahu.coolingCoil' || sections[i].name == 'ahu.mix') {
            obj = {"name" : sections[i].name, "obj": newComponentValue[sections[i].id]};
        }
    }
    return obj;
}

//TODO here the props has been defined, need a default value object; and replace the splitter from "." to "_"
function doPropCopy(sname, source, tname, target) {
    var ahuPropOut = ["meta_section_coolingCoil_winter_WInDryBulbT", "meta_section_coolingCoil_winter_WInWetBulbT", "meta_section_coolingCoil_winter_WInRelativeT", "meta_section_coolingCoil_summer_SInDryBulbT", "meta_section_coolingCoil_summer_SInWetBulbT", "meta_section_coolingCoil_summer_SInRelative"];
    var mixPropIn = ["meta_section_mix_WInDryBulbT", "meta_section_mix_WInWetBulbT", "meta_section_mix_WInRelativeT", "meta_section_mix_SInDryBulbT", "meta_section_mix_SInWetBulbT", "meta_section_mix_SInRelativeT"];
    var mixPropOut = ["meta_section_mix_WOutDryBulbT", "meta_section_mix_WOutWetBulbT", "meta_section_mix_WOutRelativeT", "meta_section_mix_SOutDryBulbT", "meta_section_mix_SOutWetBulbT", "meta_section_mix_SOutRelativeT"];
    var coolingcoilPropIn = ["meta_section_coolingCoil_winter_WInDryBulbT", "meta_section_coolingCoil_winter_WInWetBulbT", "meta_section_coolingCoil_winter_WInRelativeT", "meta_section_coolingCoil_summer_SInDryBulbT", "meta_section_coolingCoil_summer_SInWetBulbT", "meta_section_coolingCoil_summer_SInRelative"];
    var coolingcoilPropOut = ["meta_section_coolingCoil_winter_WOutDryBulbT", "meta_section_coolingCoil_winter_WOutWetBulbT", "meta_section_coolingCoil_winter_WOutRelativeT", "meta_section_coolingCoil_summer_SOutDryBulbT", "meta_section_coolingCoil_summer_SOutWetBulbT", "meta_section_coolingCoil_summer_SOutRelative"];
    var sourceKeys = [];
    if (sname == "ahu") {
        sourceKeys = ahuPropOut;
    } else if (sname == "ahu.mix") {
        sourceKeys = mixPropOut;
    } else if (sname == "ahu.coolingCoil") {
        sourceKeys = coolingcoilPropOut;
    }
    var targetKeys = [];
    if (tname == "ahu.mix") {
        targetKeys = mixPropIn;
    } else if (tname == "ahu.coolingCoil") {
        targetKeys = coolingcoilPropIn;
    }

    var i = 0;
    for(; i<sourceKeys.length; i++) {
        // target[targetKeys[i]] = source[sourceKeys[i]];
    }
}

/**
 * 同步Component的风向状态
 * @param componentValue
 * @param layoutInfo
 * @param meta_section_airDirection 方向
 * @param meta_section_doubleReturnAirDirection 为双层转向1，双层转向1 特殊增加的参数用来展示段的方向
 */
function updateAirDirection(componentValue, airDirection) {
    for (let id in airDirection) {
        if(typeof airDirection[id] === 'string'){
            componentValue[id]['meta_section_airDirection']=airDirection[id]
        }else{
            componentValue[id]['meta_section_airDirection']=airDirection[id]['airDirection']
            componentValue[id]['meta_section_doubleReturnAirDirection']=airDirection[id]['doubleReturnAirDirection']
            
        }
    }

}
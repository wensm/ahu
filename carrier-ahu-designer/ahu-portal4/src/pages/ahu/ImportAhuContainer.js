import intl from 'react-intl-universal'
import {connect} from 'react-redux'
import ImportAhu from './ImportAhu'
import {
    saveImportAhu
} from '../../actions/ahu'
const mapStateToProps = (state, ownProps) => {
    return {
        sectionsNum: state.ahu.sections.length,
        componentValue: state.ahu.componentValue,
        selectedComponent: state.ahu.selectedComponent,
        standard: state.ahu.standard,
        projectId: ownProps.projectId,
        ahuId: ownProps.ahuId,
        layout: state.ahu.layout,
        currentUnit: state.ahu.currentUnit,
        validation: state.ahu.validation,
        user: state.general.user,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	saveImportAhuData(formData,projectId,ahuId){
            dispatch(saveImportAhu(formData,projectId, ahuId));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ImportAhu)

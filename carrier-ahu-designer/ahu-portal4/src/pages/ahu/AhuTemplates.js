import {
    LABEL,
    INDUSTRY_TYPE,
    RETURN_PROJECT,
    CANCEL,
    OK,
    TEMPLATES_TOOLTIP,
    WHETHER_OR_NOT_TO_SAVE,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import style from './AhuTemplates.css'
import { Link } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import {Tooltip, Icon} from 'antd'
import { GROUP_TYPE_RUNNER, GROUP_TYPE_PLATE, GROUP_TYPE_IMAGES } from '../../models/templates.js'
import sweetalert from 'sweetalert'

class AhuTemplates extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.adoptTempalte = this.adoptTempalte.bind(this);
        this.router = context.router;

    }

    adoptTempalte(values) {
        this.props.onLoadAhuTemplate(values.templateId);
        $('#ahuTemplatesModal').modal('hide');
    }
    dotoolTip(callBack) {
        sweetalert({
            title: intl.get(WHETHER_OR_NOT_TO_SAVE),
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: intl.get(OK),
            closeOnConfirm: true
        }, isConfirm => {
            if (isConfirm) {
                $('#ahuPropsSave').click()
            }else{
                callBack()
            }
            // callBack()
            // window.location.href = "/";
        })
    }

    render() {
        const { templates, onLoadAhuTemplate, projectId, handleSubmit, pristine, reset, submitting, dirty, page, doToolTip, validation, ahuSerial} = this.props
        let disabled = false
        // if (user && user.userRole == 'Sales' && !validation.pass || user && user.userRole == 'Sales' && !ahuSerial) {
        if (!validation.pass || !ahuSerial) {
            disabled = true
        }
        return (
            <div data-name="AhuTemplates" className={style.AhuTemplates}>
                <button type="button" className="btn btn-primary btn-block" data-toggle="modal" style={{ marginBottom: '5px' }}
                    data-target="#ahuTemplatesModal">{intl.get(INDUSTRY_TYPE)}</button>

{/* this.router.push(`partition/projects/${projectId}/ahus/${ahuId}`); */}
                {/* <Link to={{ pathname: '/page2/' + projectId, query: { name: 'pro' } }}
                onClick={()=>{
                    console.log('return', doToolTip)
                    }}> */}
                    <span className="btn btn-default btn-block" onClick={()=>{
                        if (doToolTip && !disabled) {
                            this.dotoolTip(()=>this.router.push(`page2/${projectId}`))
                        } else {
                            this.router.push(`page2/${projectId}`)
                        }
                    }}>
                            {intl.get(RETURN_PROJECT)}
                    </span>
                {/* </Link> */}
                {/*<a   className="btn btn-default btn-block" onClick={()=>{
                    //  this.props.router.push(`page2/${projectId}`)
                     this.props.router.goBack()
                     }}
                   role="button">{intl.get(RETURN_PROJECT)}</a> */}

                <div className="modal fade" id="ahuTemplatesModal" tabIndex="-1" role="dialog"
                    aria-labelledby="ahuTemplatesModalLabel">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title"
                                    id="ahuTemplatesModalLabel">{intl.get(INDUSTRY_TYPE)}</h4>
                            </div>
                            <form data-name="templateSelection" onSubmit={handleSubmit(this.adoptTempalte)}>
                                <div className="modal-body">
                                    <div style={{ maxHeight: window.innerHeight - 220, overflow: 'auto' }}>
                                        <table className="table table-hover" id="ahuTemplates">
                                            <tbody>
                                                {templates.map((template, index) => <Template key={index} warnTip={TEMPLATES_TOOLTIP} {...template} />)}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-default"
                                        data-dismiss="modal">{intl.get(CANCEL)}</button>
                                    <button type="submit" disabled={!dirty} className="btn btn-primary">{intl.get(OK)}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Template extends React.Component {
    render() {
        const { cnName, templateName, id, warnTip} = this.props;
        return (
            <tr>
                <td style={{ verticalAlign: 'middle' }}>
                    <div className="radio">
                        <label>
                            <Field
                                name="templateId"
                                component="input"
                                type="radio"
                                value={id}
                            />{' '}
                            {intl.get(LABEL + cnName)}
                            {
                                id == GROUP_TYPE_RUNNER || id == GROUP_TYPE_PLATE ? <Tooltip placement="topLeft" title={intl.get(warnTip)}>
                                    <Icon type="warning" style={{ fontSize: '18px', paddingLeft: '10px', color: '#cab410' }} />
                                </Tooltip> : ''
                            }
                        </label>
                    </div>
                </td>
                <td>
                    <img src={GROUP_TYPE_IMAGES[id]} className="img-responsive" />
                </td>
            </tr>
        )
    }
}
AhuTemplates.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default reduxForm({
    form: 'templateSelection', // a unique identifier for this form 23:40
    enableReinitialize: true,
})(AhuTemplates)

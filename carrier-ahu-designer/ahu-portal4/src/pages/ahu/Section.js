import {
    LABEL
} from '../intl/i18n'

import React from 'react'
import classnames from 'classnames'
import intl from 'react-intl-universal'
import style from './Section.css'

export default class Section extends React.Component {
  componentDidMount() {
        $('[data-toggle="tooltip"]').tooltip()
    }

  render() {
    const { metaId, cName,name, sectionId, id, canDropList } = this.props;
    let bool = canDropList && canDropList.some((record)=>{
      return record == metaId
    })
    let styleCss = metaId.substr(4);
    if(cName.indexOf("wwk") != -1) {
        styleCss = "wwkFan";
    }
    return (
      <div
        style = {{cursor:bool?'pointer':'not-allowed'}}
        data-section={metaId}
        data-section-id={sectionId}
        data-id={id}
        className={classnames(style.Section, style[styleCss], 'hvr-float-shadow')}
        // className = {`${style.Section} ${style[metaId.substr(4)]} hvr-float-shadow`}
        data-toggle="tooltip"
        data-placement="top"
        title={intl.get(LABEL+cName)}>
      </div>
    )
  }
}

import intl from 'react-intl-universal'
import { connect } from 'react-redux'
import Buttons from './Buttons'
import {
    fetchPrice,
    fetchThreeViews,
    fetchPsychometric,
    saveAhu,
    onToggleStandard,
    saveImportAhu,
    reportError,
    panelInit,
    onThreeViewModalClosed,
    ahuSerial, fetchAhuInfo,fetchAhuJsonInfo
} from '../../actions/ahu'

import {
    reset
} from '../../actions/partition'
import * as metadataAction from "../../actions/metadata";


const mapStateToProps = (state, ownProps) => {
    return {
        sectionsNum: state.ahu.sections.length,
        componentValue: state.ahu.componentValue,
        selectedComponent: state.ahu.selectedComponent,
        standard: state.ahu.standard,
        projectId: ownProps.projectId,
        ahuId: ownProps.ahuId,
        layout: state.ahu.layout,
        currentUnit: state.ahu.currentUnit,
        user: state.general.user,
        validation: state.ahu.validation,
        ahuSerial: ahuSerial(state.ahu.componentValue[0]),
        // ahuStatus : state.ahu.currentUnit ? state.ahu.currentUnit.recordStatus : '',
        ahuStatus: state.ahu.recordStatus,

    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onToggleStandard: () => {
            dispatch(onToggleStandard())
            /*屏蔽切换非标、标准刷新数据库json代码：dispatch(fetchAhuJsonInfo(ownProps.ahuId))*/
        },
        onFetchPrice: () => {
            dispatch(fetchPrice(ownProps.ahuId))
        },
        onFetchThreeViews: (fromWhere) => {
            dispatch(onThreeViewModalClosed())            
            dispatch(fetchThreeViews(ownProps.ahuId, fromWhere))
        },
        onFetchPsychometric: () => {
            dispatch(fetchPsychometric(ownProps.ahuId))
        },
        saveImportAhu(formData, projectId, ahuId) {
            dispatch(saveImportAhu(formData, projectId, ahuId));
        },
        onPanelInit(ahuId, callback) {
            dispatch(panelInit(ahuId, callback));
        },
        onReportError(projectId, ahuId) {
            dispatch(reportError(projectId, ahuId));
        },
        onResetTip: () => {
            dispatch(reset())
        },
        clearThreeView() {
            dispatch(onThreeViewModalClosed())
        },
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Buttons)

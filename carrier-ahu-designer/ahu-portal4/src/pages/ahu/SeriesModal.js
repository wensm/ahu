import {
    UNIT_MODEL,
    BACKGROUND_COLOR,
    WIND_SPEED,
    CHOOSE,
    MODEL,
    WIDTH,
    HEIGHT,
    FACE_WIND_SPEED,
    CLOSE,
    OK,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { selectAhuSeries } from '../../actions/ahu'
import style from './SeriesModal.css'
import { Modal } from 'antd';

export default class SeriesModal extends React.Component {

  constructor(props) {
    super(props)
  }

  componentDidUpdate(prevProps, prevState) {
    const { series, sairvolume, selectedSeries } = prevProps
    $('#seriesModal').find('input[type="radio"]:checked').removeAttr("checked")
    $('#seriesModal').find(`input[data-series='${selectedSeries}']`).prop('checked', true)
  }

  handleSelectSeries() {
    const { onSelectAhuSeries, metaAhuDelivery, isExport } = this.props
    const series = $('#seriesModal').find('input[type="radio"]:checked').attr('data-series')
    const height = $('#seriesModal').find('input[type="radio"]:checked').attr('data-height')


      onSelectAhuSeries(series)
  }

  render() {
    const { series, sairvolume, selectedSeries, unitPreferCode} = this.props
    let copySairvolume = sairvolume
    if(unitPreferCode == 'B'){
      copySairvolume = Number(copySairvolume)*1.6989
      copySairvolume = copySairvolume.toFixed(1)

    }
    return (
      <div className="modal fade" id="seriesModal" tabIndex="-1" role="dialog" aria-labelledby="seriesModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title" id="seriesModalLabel">{intl.get(UNIT_MODEL)}</h4>
            </div>
            <div className="modal-body">
              <table className="table" style={{marginBottom:'0px'}}>
                <thead>
                  <tr>
                    <th>{intl.get(BACKGROUND_COLOR)}</th>
                    <th>{intl.get(WIND_SPEED)} v(m/s)</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div className={classnames(style.indicator, style.white)}></div>
                    </td>
                    <td>{`v <= 2.65m/s`}</td>
                  </tr>
                <tr>
                  <td>
                    <div className={classnames(style.indicator, style.yellow)}></div>
                  </td>
                  <td>{`2.65m/s < v <= 3.5m/s`}</td>
                </tr>
                <tr>
                  <td>
                    <div className={classnames(style.indicator, style.gray)}></div>
                  </td>
                  <td>{`v > 3.5m/s`}</td>
                </tr>
                </tbody>
              </table>
              <div style={{maxHeight:window.innerHeight-380,overflow:'auto'}}>
                <table className="table">
                  <thead>
                    <tr>
                      <th style={{textAlign:'center'}}>{intl.get(CHOOSE)}</th>
                      <th>{intl.get(MODEL)}</th>
                      <th>{intl.get(WIDTH)} (mm)</th>
                      <th>{intl.get(HEIGHT)} (mm)</th>
                      <th>{intl.get(FACE_WIND_SPEED)} (m/s)</th>
                    </tr>
                  </thead>
                  <tbody>
                    {series.map((serie, index) => <Series key={index} {...serie} sairvolume={copySairvolume} unitPreferCode = {unitPreferCode}/>)}
                  </tbody>
                </table>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CLOSE)}</button>
              <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={() => this.handleSelectSeries()}>{intl.get(OK)}</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class Series extends React.Component {
  render() {
    let { ahu, width, height, coilFaceArea, sairvolume, unitPreferCode} = this.props
    const velocity = Number(sairvolume) / (coilFaceArea * 3600)//sairvolume 风量区分了 公英制
    let className = style.white
    switch (true) {
      case velocity <= 2.65:
        className = style.white
        break
      case velocity > 3.5:
        className = style.gray
        break
      default:
        className = style.yellow
    }
    return (
      <tr className={className}>
        <td style={{textAlign:'center'}}>
          <input type="radio" name="ahu-series" data-series={ahu} data-height={height}/>
        </td>
        <td>{ahu}</td>
        <td>{width}</td>
        <td>{height}</td>
        <td>{velocity.toFixed(3)}</td>
      </tr>
    )
  }
}

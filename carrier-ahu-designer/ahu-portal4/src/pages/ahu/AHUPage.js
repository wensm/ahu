import {
    PLEASE_SELECT_THE_DIRECTION_RETURN_AIR
} from '../intl/i18n'
import intl from 'react-intl-universal'
import React from 'react'
import dragula from 'dragula'
import classnames from 'classnames'
import { connect } from 'react-redux'
import Header from './Header'
import AHUContainer from './AHUContainer'
import FormContainer from './FormContainer'
import DetailContainer from './DetailContainer'
import ButtonsContainer from './ButtonsContainer'
import AHUTipsContainer from './AHUTipsContainer'
import SectionListContainer from './SectionListContainer'
import AhuTemplatesContainer from './AhuTemplatesContainer'

import PriceModal from './PriceModal'
import SeriesModalContainer from './SeriesModalContainer'
import ThreeViewsModal from './ThreeViewsModal'
// import CoolingCoilModal from '../section/coolingCoil/CoolingCoilModal'
// import SprayHumidifierModal from '../section/sprayHumidifier/SprayHumidifierModal'
// import SteamHumidifierModal from '../section/steamHumidifier/SteamHumidifierModal'
// import FanCalcModalContainer from '../section/fan/FanCalcModalContainer'
// import PsychometricModal from './PsychometricModal'

import style from './AHUPage.css'
import styleS from './Section.css'
import { isVerticalLine, isSectionPositionFixed, isSingleLine, isTwinLine, getSimpleSectionArr, isReturnLine } from "../../actions/ahu"
import { isEmpty } from 'lodash'
import ImportAhuContainer from './ImportAhuContainer'
import { hasRowOfCoilIn357, hasTopDamper } from '../../models/ahu.js'

export default class AHUPage extends React.Component {

    constructor() {
        super();
        this.dragInit = this.dragInit.bind(this)
        this.isTwinAndRecycle = this.isTwinAndRecycle.bind(this)
        this.copy = this.copy.bind(this)
        this.accepts = this.accepts.bind(this)
        this.invalid = this.invalid.bind(this)
        this.isFixed = this.isFixed.bind(this)
        this.isPlate = this.isPlate.bind(this)
        this.isWheel = this.isWheel.bind(this)
        this.doDrop = this.doDrop.bind(this)

    }

    componentDidMount() {
        // this.dragInit()
        const { ahuId, onFetchAhuInfo, onFetchAhuSections, onGetUserInfo, unitSetting, propUnit, metaUnit, metaLayout } = this.props
        // console.log(' unitSetting, propUnit, metaUnit, metaLayout2222',  unitSetting, propUnit, metaUnit, metaLayout)
        // console.log('this', this.props.router.location.query.product)

        $("#scrollPosition").scrollTop(0);

        onFetchAhuInfo(ahuId, unitSetting, metaUnit, metaLayout, propUnit)//zzf获取初始化信息
        // onFetchAhuSections(ahuId)
        // TODO: need to remove timeout
        // setTimeout(() => {
        //     onFetchAhuInfo(ahuId)
        // }, 5000);
    }

    componentWillReceiveProps(nextProps) {
        const { componentValue, canDropList } = nextProps
        let { isExport } = this.props
        let ahus = [$('#ahu'), $('#ahutl'), $('#ahutr'), $('#ahubl'), $('#ahubr')]
        setTimeout(() => {
            for (var variable in componentValue) {
                if (componentValue.hasOwnProperty(variable)) {
                    ahus.forEach(ahu => {
                        const $section = ahu.find(`[data-section-id='${variable}']`)
                        if (componentValue[variable].meta_section_completed) {
                            $section.attr('data-section-completed', true)
                        } else {
                            $section.removeAttr('data-section-completed')
                        }
                        if ($section.attr('data-section') && canDropList.indexOf($section.attr('data-section')) == -1 && ($section.attr('data-section') != 'ahu.wheelHeatRecycle' && $section.attr('data-section') != 'ahu.plateHeatRecycle')) {
                            $section.attr('data-section-cannotdrop', true)

                        } else if ($section.attr('data-section') && canDropList.indexOf($section.attr('data-section')) != -1) {
                            $section.removeAttr('data-section-cannotdrop')

                        }
                        // if (componentValue[variable].meta_section_completed) {
                        //     $section.attr('data-section-completed', true)
                        // } else {
                        //     $section.removeAttr('data-section-completed')
                        // }
                        let name = ''
                        for (let key in componentValue[variable]) {
                            if (key.split('_').length == 4) {
                                name = key.split('_')[2]
                            }
                        }
                        if (componentValue[variable][`ns_section_${name}_enable`]) {
                            $section.attr('data-section-nsenable', true)
                        } else {
                            $section.removeAttr('data-section-nsenable')
                        }

                    })

                }
            }
            // 盘管段3，5，7排，机组无法变形
            if (hasRowOfCoilIn357(componentValue)) {
                componentValue[0]['meta_ahu_deformation'] = false;
                componentValue[0]['ns_ahu_deformation'] = false;
            } else {
                componentValue[0]['meta_ahu_deformation'] = true;
            }
            // 出口版检查机组是否有顶部风阀
            if (isExport == 'true') {
                if (hasTopDamper(componentValue)) {
                    componentValue[0]['meta_ahu_topDamper'] = true;
                } else {
                    componentValue[0]['meta_ahu_topDamper'] = false;
                }
            }
        }, 100)
    }

    /**
     * 是一个双层段，同时拖动的对象还是板式或者转轮热回收段
     * @param el
     * @returns {boolean}
     */
    isTwinAndRecycle(el) {
        return (isTwinLine(this.props.layout.style))
    }

    /**
     * 如果container是sectionList，执行copy操作
     * @param el
     * @param source
     * @returns {boolean}
     */
    copy(el, source) {
        return source === sections
    }

    /**
     * 以前是如果操作对象是板式或轮式热回收段，而且已经是双层段的时候，返回false，可以拖动
     * 现在是需要和型号联动，来判断是否可以拖动
     * 出口版的控制段不可拖动
     * @param el
     * @param handle
     * @returns {boolean}
     */

    invalid(el, handle) {
        // return true

        let { canDropList, isExport } = this.props
        if (isExport == 'true' && el.getAttribute("data-section") == 'ahu.ctr') {//出口版的控制段不可拖动
            el.setAttribute('style', "cursor: not-allowed;")
            return true
        }

        let isHas = canDropList && canDropList.indexOf(el.getAttribute("data-section")) == -1 ? true : false;
        let bool = isHas || this.isFixed(el)
        // if(bool){
        //     console.log('el', el)
        //     el.setAttribute('style', "cursor: not-allowed;")
        // }else{
        //     el.setAttribute('style', "cursor: pointer;")
        // }
        return bool
        // return this.isWheel(el) || this.isPlate(el)
    }

    isFixed(el) {
        let sectionPosition = el.getAttribute("data-section-id");
        return isSectionPositionFixed(this.props.layout, sectionPosition);
    }

    isPlate(el) {
        let elType = el.getAttribute("data-section")
        return elType == "ahu.plateHeatRecycle"
    }

    isWheel(el) {
        let elType = el.getAttribute("data-section")
        return elType == "ahu.wheelHeatRecycle"
    }

    accepts(el, target, source, sibling) {
        let did = target.getAttribute("id")
        if (this.isWheel(el) || this.isPlate(el)) {
            if (isSingleLine(this.props.layout.style)) {
                return false
            }
        } else {
            if (isTwinLine(this.props.layout.style) && sibling == null && (did === "ahutl" || did === "ahubl")) {
                return false
            }
        }
        // 立式双层机组只允许添加到下层左侧
        if (isVerticalLine(this.props.layout.style)) {
            return did === "ahubl"
        }
        //  转向双层机组只允许添加到上下层左侧
        if (isReturnLine(this.props.layout.style)) {
            return did === "ahubl" || did === "ahutl"
        }
        let enable = did === "ahu" || did === "ahutl" || did === "ahutr" || did === "ahubl" || did === "ahubr"
        return enable
    }

    doDrop(el, target, source, sibling) {
        const name = el.getAttribute('data-section')
        const {
            componentId,
            defaultValue,
            projectId,
            ahuId,
            formValues,
            onAddNewSection,
            onDropSection,
            onValidateAhuSections,
            onSyncComponentValues,
            onSyncDirection,
            propUnit,
            layout
        } = this.props;


        if (target !== source) {
            el.setAttribute('data-section-id', componentId)
            onAddNewSection(name, projectId, ahuId, propUnit)
            $('[data-toggle="tooltip"]').tooltip()
        }
        let combinedMixingChamberIndex = 10000;
        let findWheelHeatRecycle = false;
        let findPlateHeatRecycle = false;
        let direction = "S"
        let isSinggle = isSingleLine(layout.style)
        let currentIndex = 100
        let sections = getSimpleSectionArr();
        let targetId = $(target).attr('id'), id = el.getAttribute('data-section-id')
        sections.forEach(function (section, index, allarr) {
            let a = $(`[data-section-id='${section.id}']`)[0]
            isSinggle && allarr && allarr.length > 14 && $(a).attr('style', 'width:35px;height:35px')
            if (section.id == id) {
                currentIndex = index
            }
            if (section.name === 'ahu.wheelHeatRecycle') {
                findWheelHeatRecycle = true;
            } else if (section.name === 'ahu.plateHeatRecycle') {
                findPlateHeatRecycle = true;

            } else if (section.name === 'ahu.combinedMixingChamber') {
                combinedMixingChamberIndex = index;
            }
        })
        if (findWheelHeatRecycle && (targetId == "ahutl" || targetId == "ahutr")) {
            direction = "R"
        }
        if (findPlateHeatRecycle && (targetId == "ahubl" || targetId == "ahutr")) {
            direction = "R"
        }
        if (combinedMixingChamberIndex != 10000) {
            if (currentIndex < combinedMixingChamberIndex) {
                direction = "R"
            } else if (currentIndex == combinedMixingChamberIndex) {
                direction = "COMBINE"
            }
        }
        $(target).children('[data-section]').each((index, element) => {
            let classSplit = element.getAttribute('class').split(' showBack')[0]
            element.setAttribute('class', classSplit)
            let styleName = `${name.split('.')[1]}showBack`
            if (findWheelHeatRecycle) {//转轮
                if ($(target).attr('id') == 'ahutl' || $(target).attr('id') == 'ahutr') {//换成相反的图片
                    element.setAttribute('class', `${element.getAttribute('class')} ${styleS[styleName]}`)
                }
            } else if (findPlateHeatRecycle) {//板式
                if ($(target).attr('id') == 'ahubl' || $(target).attr('id') == 'ahutr') {//换成相反的图片
                    element.setAttribute('class', `${element.getAttribute('class')} ${styleS[styleName]}`)
                }
            } else if (combinedMixingChamberIndex != 10000) {//新会排不用变
            }
        })
        // const id = el.getAttribute('data-section-id')
        let displayName = el.getAttribute('data-original-title')
        onDropSection(id, name, displayName, formValues)
        onValidateAhuSections(this.props)
        onSyncComponentValues(direction, propUnit)
    }

    dragInit() {
        const ahutl = document.getElementById('ahutl')
        const ahutr = document.getElementById('ahutr')
        const ahubl = document.getElementById('ahubl')
        const ahubr = document.getElementById('ahubr')
        const ahu = document.getElementById('ahu')
        // Skip if the panel is not ready
        if (!ahu && !ahubl) {
            return
        }
        const sections = document.getElementById('sections')
        var drake = dragula([sections, ahu, ahutl, ahutr, ahubl, ahubr], {
            copy: this.copy,
            accepts: this.accepts,
            invalid: this.invalid,
            removeOnSpill: true,
            direction: 'horizontal',
        }).on('drop', this.doDrop)
            .on('remove', (el, container, source) => {

                const {
                    onRemoveAhuSection,
                    onValidateAhuSections,
                    onSyncComponentValues,
                    propUnit
                } = this.props
                let pDomId = source.id;
                let lineIndex = 0;
                let sectionId = el.getAttribute('data-section-id');
                let sectionName = el.getAttribute('data-section');
                if (['ahutl', 'ahutr'].indexOf(pDomId) !== -1) {
                    lineIndex = 0;
                } else if (['ahubl', 'ahubr'].indexOf(pDomId) !== -1) {
                    lineIndex = 1;
                } else {

                }
                let combinedMixingChamberIndex = 10000;
                let findWheelHeatRecycle = false;
                let findPlateHeatRecycle = false;
                let direction = "S"
                let sections = getSimpleSectionArr();
                let targetId = source.id, id = el.getAttribute('data-section-id')
                let currentIndex = 100
                sections.forEach(function (section, index, allarr) {
                    let a = $(`[data-section-id='${section.id}']`)[0]
                    allarr && allarr.length <= 14 && $(a).css('width', '52px')
                    allarr && allarr.length <= 14 && $(a).css('height', '50px')
                    if (section.id == id) {
                        currentIndex = index
                    }
                    if (section.name === 'ahu.wheelHeatRecycle') {
                        findWheelHeatRecycle = true;
                    } else if (section.name === 'ahu.plateHeatRecycle') {
                        findPlateHeatRecycle = true;

                    } else if (section.name === 'ahu.combinedMixingChamber') {
                        combinedMixingChamberIndex = index;
                    }
                })
                if (findWheelHeatRecycle && (targetId == "ahutl" || targetId == "ahutr")) {
                    direction = "R"
                }
                if (findPlateHeatRecycle && (targetId == "ahubl" || targetId == "ahutr")) {
                    direction = "R"
                }
                if (combinedMixingChamberIndex != 10000) {
                    if (currentIndex < combinedMixingChamberIndex) {
                        direction = "R"
                    } else if (currentIndex == combinedMixingChamberIndex) {
                        direction = "COMBINE"
                    }
                }
                if (sectionName == 'ahu.combinedMixingChamber') {
                    direction = "COMBINE"
                }
                $(el).tooltip('destroy')
                onRemoveAhuSection({
                    sectionId,
                    lineIndex,
                    sectionName,
                    direction,
                    propUnit,
                    sectionName
                })
                onValidateAhuSections(this.props)
            })
        return drake
    }
    render() {
        const props = this.props
        const { projectId, ahuId, layout, canDropList, componentValue, selectedComponent, psychometricModalState, location } = this.props;
        let groupId = location.query ? location.query.groupId : ''

        //P00M-000-180509142620-0000/G00M-000-180531171259-0000/1/A00M-000-180531171304-0000
        let defaultMessage = false, defaultType = true
        if (!isEmpty(componentValue)) {
            let arr = []
            arr = this.getDefaultMessage(componentValue[selectedComponent['id']])
            defaultMessage = arr[0]
            defaultType = arr[1]
        }
        return (
            <div className={style.AHU}>
                <div className={classnames('row', style.body)}>
                    <div className="col-lg-4 col-md-4 col-sm-6"></div>
                    <div className="col-lg-4 col-md-4 col-sm-6"></div>
                    <div className="col-lg-4 col-md-4 col-sm-6"></div>
                </div>
                <div className={classnames('row', style.body)}>
                    <div className="col-lg-2 col-md-3 col-sm-4" style={{ position: 'fixed', height: window.innerHeight - 100, maxWidth: '230px' }}>
                        <SectionListContainer />
                    </div>
                    <div style={{ width: '16.66%' }}>
                        <AhuTemplatesContainer {...props} />
                    </div>
                    <div className="col-lg-8 col-md-6 col-sm-4" style={{ marginLeft: '16%' }}>
                        <ButtonsContainer {...props} onSaveAhu={props.onSaveAhu} projectId={props.projectId || ''} ahuId={ahuId} groupId={groupId} />
                        <AHUTipsContainer defaultMessage={defaultMessage} defaultType={defaultType} />
                        <div onClick={e => props.onClickSection(e, props.formValues)}>
                            <AHUContainer dragInit={this.dragInit} />
                        </div>
                        <FormContainer {...props} projectId={projectId} ahuId={ahuId} />
                    </div>
                    <div className="col-lg-2 col-md-3 col-sm-4" style={{ position: 'fixed', right: '0', top: '80px' }}>
                        <DetailContainer routeLocation={location} />
                    </div>
                </div>
                <PriceModal />
                <SeriesModalContainer />
                <ThreeViewsModal ahuId={ahuId} ref="threeViewsModal" />
                {/* <PsychometricModal/>
                <CoolingCoilModal />
                <SprayHumidifierModal />
                <SteamHumidifierModal />
                <FanCalcModalContainer /> */}
                <ImportAhuContainer />
            </div>
        )
    }
    getDefaultMessage = (values) => {
        let bool = false, arr = []
        if (values.hasOwnProperty('meta_section_mix_returnTop')) {//提示，回风方向未勾选
            const list = [
                'meta_section_mix_returnTop',
                'meta_section_mix_returnBack',
                'meta_section_mix_returnLeft',
                'meta_section_mix_returnRight',
                'meta_section_mix_returnButtom',
            ]
            list.forEach((record) => {
                if (values[record]) {
                    bool = true
                }
            })

        } else {
            bool = true
        }
        if (!bool) {
            arr = [intl.get(PLEASE_SELECT_THE_DIRECTION_RETURN_AIR), false]
        } else {
            arr = [false, true]
        }
        return arr
    }
}



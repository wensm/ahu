import {
  PROMPT_DRAG_OUT_SECTION_TO_DELETE,
  PLEASE_SELECT_THE_UNIT_MODEL_FIRST,
  ALERT_COLON,
  CONDENSER_INLET_AIR_TEMPERATURE,
  SATURATED_CONDENSATION_TEMPERATURE,
  SATURATED_SUCTION_TEMPERATURE,
  WATER_RESISTANCE,
  WATER_FLOW_VOLUME,
  INLET_WATER_TEMPERATURE,
  WATER_TEMPERATURE_RISE,
  MEDIUM_FLOW_RATE,
  COIL_SECTION_CALCULATION_RESULT,
  COIL_INFORMATION,
  INLET_WIND,
  OUTLET_AIR,
  AIR_SIDE,
  WATER_SIDE,
  PERFORMANCE,
  ROW_NUMBER,
  FIN_DISTANCE,
  CIRCUIT,
  WINDWARD_AREA,
  FACE_WIND_SPEED,
  DB_TEMPERATURE,
  WET_BULB_TEMPERATURE,
  RELATIVE_HUMIDITY,
  AIR_RESISTANCE,
  REQUEST_DATA_IS_ZERO,
  CLOSE,
  OK,
  COOLING_VOLUME,
  SHOW_COLD,
  HEATING_VOLUME,
  PRICE,
  COIL_NOT_CERTIFIED_AHRI,
  LABEL_TOOLTIP,
  WHEELHEATRECYCLE_TOOLTIP
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import classnames from 'classnames'
import { Table } from 'antd';
export default class calculateTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedRowKeys: [],
      selectedRows: ''
    }
  }
  componentDidMount(){
    $(".ant-table-thead > tr > th.ant-table-column-has-actions.ant-table-column-has-sorters").css('cssText','padding-right: 5px !important')
    $(".ant-table-thead > tr > th.ant-table-column-has-actions.ant-table-column-has-filters").css('cssText','padding-right: 5px !important')
  }
  returnState = () => {
    return this.state
  }
  render() {
    let { selectedRowKeys } = this.state
    let { datas, type } = this.props
    let lseason = 'S'
    if (datas) {
      lseason = datas.season
    }
    // console.log('datas', datas)
    <div><div>{}</div><div>{}</div></div>
    const columns = [
      {
        title: intl.get(COIL_INFORMATION),
        fixed: 'left',
        children: [
          {
            title: intl.get(ROW_NUMBER),
            dataIndex: 'rows',
            key: 'rows',
            width: 70,
            sorter: (a, b) => a.rows - b.rows,


          },
          {
            title: <div><div>{intl.get(FIN_DISTANCE)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0661')}</div></div>,
            title: intl.get(FIN_DISTANCE),
            dataIndex: 'finDensity',
            key: 'finDensity',
            width: 50,
            // sorter: (a, b) => a.finDensity - b.finDensity,


          },
          {
            title: intl.get(CIRCUIT),
            dataIndex: 'circuit',
            key: 'circuit',
            width: 50,

          },
          
        ],
      },
      {
        title: intl.get(PERFORMANCE),
        children: lseason == "S" && type != "ahu.directExpensionCoil" ? [
          {
            title: <div><div>{intl.get(COOLING_VOLUME)}</div><div style={{textAlign:'center'}}>{intl.get('meta.moon_intl_str_1366')}</div></div>,
            dataIndex: 'returnColdQ',
            key: 'returnColdQ',
            width: 70,
            sorter: (a, b) => a.returnColdQ - b.returnColdQ,

          },
          {
            title: <div><div>{intl.get(SHOW_COLD)}</div><div style={{textAlign:'center'}}>{intl.get('meta.moon_intl_str_1366')}</div></div>,
            dataIndex: 'returnSensibleCapacity',
            key: 'returnSensibleCapacity',
            width: 70,
            sorter: (a, b) => a.returnSensibleCapacity - b.returnSensibleCapacity,

          },
          // {
          //   title: intl.get('meta.moon_intl_str_2046'),
          //   dataIndex: 'returnBypass',
          //   key: 'returnBypass',
          //   width: 110,
          //   sorter: (a, b) => a.returnBypass - b.returnBypass,
          // },
        ] : lseason == "W" && type != "ahu.directExpensionCoil" ? [
          {
            title: <div><div>{intl.get(HEATING_VOLUME)}</div><div style={{textAlign:'center'}}>{intl.get('meta.moon_intl_str_1366')}</div></div>,
            dataIndex: 'returnHeatQ',
            key: 'returnHeatQ',
            width: 100,
            sorter: (a, b) => a.returnHeatQ - b.returnHeatQ,

          }
        ] : [
              {
                title: intl.get(COOLING_VOLUME),
                dataIndex: 'returnColdQ',
                key: 'returnColdQ',
                width: 160,
                sorter: (a, b) => a.returnColdQ - b.returnColdQ,

              },
              {
                title: intl.get(SHOW_COLD),
                dataIndex: 'returnSensibleCapacity',
                key: 'returnSensibleCapacity',
                width: 160,
                sorter: (a, b) => a.returnSensibleCapacity - b.returnSensibleCapacity,

              }
            ],
      },
      {
        title: intl.get(AIR_SIDE),
        // dataIndex: 'air_side',
        // key: 'air_side',
        // width: 160,

        children: [
          {
            title: <div><div>{intl.get(AIR_RESISTANCE)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0666')}</div></div>,
            dataIndex: 'AirResistance',
            key: 'AirResistance',
            width: 100,
            sorter: (a, b) => a.AirResistance - b.AirResistance,

          }
        ],
        // width: 80,
        // fixed: 'right',
      },
      {

        title: intl.get(WATER_SIDE),
        children: type == "ahu.directExpensionCoil" ? [
          {
            title: intl.get(CONDENSER_INLET_AIR_TEMPERATURE),
            dataIndex: 'returnCond',
            key: 'returnCond',
            width: 160,
            sorter: (a, b) => a.returnCond - b.returnCond,

          },
          {
            title: intl.get(SATURATED_CONDENSATION_TEMPERATURE),
            dataIndex: 'returnSatCond',
            key: 'returnSatCond',
            width: 160,
            sorter: (a, b) => a.returnSatCond - b.returnSatCond,

          },
          {
            title: intl.get(SATURATED_SUCTION_TEMPERATURE),
            dataIndex: 'returnSST',
            key: 'returnSST',
            width: 160,
            sorter: (a, b) => a.returnSST - b.returnSST,

          },
          {
            title: intl.get('title.moon_intl_str_0698'),
            dataIndex: 'Unit',
            key: 'Unit',
            width: 160,
            sorter: (a, b) => a.Unit - b.Unit,

          },
        ]
          : [
            {
              title: <div><div>{intl.get(WATER_RESISTANCE)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0667')}</div></div>,
              dataIndex: 'waterResistance',
              key: 'waterResistance',
              width: 90,
              sorter: (a, b) => a.waterResistance - b.waterResistance,

            },
            {
              title: <div><div>{intl.get(WATER_FLOW_VOLUME)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0668')}</div></div>,
              dataIndex: 'returnWaterFlow',
              key: 'returnWaterFlow',
              width: 90,
              sorter: (a, b) => a.returnWaterFlow - b.returnWaterFlow,
            },         
            {
              title: <div><div>{intl.get(MEDIUM_FLOW_RATE)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0663')}</div></div>,
              dataIndex: 'fluidvelocity',
              key: 'fluidvelocity',
              width: 100,
              sorter: (a, b) => a.fluidvelocity - b.fluidvelocity,

            },
          ],
      },

      {
        title: intl.get(OUTLET_AIR),
        // width: 160,

        children: [
          {
            title: <div><div>{intl.get(DB_TEMPERATURE)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0664')}</div></div>,
            dataIndex: 'OutDryBulbT',
            key: 'OutDryBulbT',
            // width: 160,
            width: 100,
            sorter: (a, b) => a.OutDryBulbT - b.OutDryBulbT,

          },
          {
            title: <div><div>{intl.get(WET_BULB_TEMPERATURE)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0664')}</div></div>,
            dataIndex: 'OutWetBulbT',
            key: 'OutWetBulbT',
            width: 100,
            sorter: (a, b) => a.OutWetBulbT - b.OutWetBulbT,

          },
          {
            title: <div><div>{intl.get(RELATIVE_HUMIDITY)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0665')}</div></div>,
            dataIndex: 'OutRelativeT',
            key: 'OutRelativeT',
            width: 100,
            sorter: (a, b) => a.OutRelativeT - b.OutRelativeT,

          },
        ],
      },






      {
        title: '',
        children: [
          {
            title: <div><div>{intl.get(WINDWARD_AREA)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0662')}</div></div>,
            dataIndex: 'area',
            key: 'area',
            width: 80,
            // sorter: (a, b) => a.area - b.area,


          },
          {
            title:<div><div>{intl.get(FACE_WIND_SPEED)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0663')}</div></div>,
            dataIndex: 'velocity',
            key: 'velocity',
            width: 70,
            // sorter: (a, b) => a.velocity - b.velocity,


          },
        ]        
      },
      {
        title: intl.get(INLET_WIND),
        // width: 160,

        children: [
          {
            title: <div><div>{intl.get(DB_TEMPERATURE)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0664')}</div></div>,
            dataIndex: 'InDryBulbT',
            key: 'InDryBulbT',
            width: 80,
            // sorter: (a, b) => a.InDryBulbT - b.InDryBulbT,
          },
          {
            title: <div><div>{intl.get(WET_BULB_TEMPERATURE)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0664')}</div></div>,
            dataIndex: 'InWetBulbT',
            key: 'InWetBulbT',
            width: 80,
            // sorter: (a, b) => a.InWetBulbT - b.InWetBulbT,


          },
          {
            title: <div><div>{intl.get(RELATIVE_HUMIDITY)}</div><div style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0665')}</div></div>,
            dataIndex: 'InRelativeT',
            key: 'InRelativeT',
            width: 80,
            // sorter: (a, b) => a.InRelativeT - b.InRelativeT,


          },
        ],
      },
      {
        title: '',
        children: [
          {
            title:  <div><div>{intl.get(INLET_WATER_TEMPERATURE)}</div><div  style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0664')}</div></div>,
            dataIndex: 'returnEnteringFluidTemperature',
            key: 'returnEnteringFluidTemperature',
            width: 80,
            // sorter: (a, b) => a.returnEnteringFluidTemperature - b.returnEnteringFluidTemperature,
    
          },
          
          {
            title: <div><div>{intl.get(WATER_TEMPERATURE_RISE)}</div><div  style={{textAlign:'center'}}>{intl.get('title.moon_intl_str_0664')}</div></div>,
            dataIndex: 'returnWTAScend',
            key: 'returnWTAScend',
            width: 70,
            // sorter: (a, b) => a.returnWTAScend - b.returnWTAScend,
    
          }
        ]        
      },
      {
        title: <div><div>{intl.get(PRICE)}</div><div>{'RMB(LP)'}</div></div>,
        dataIndex: 'price',
        key: 'price',
        // width: 120,
        sorter: (a, b) => a.price - b.price,
        // fixed: 'right',
      },
    ];

    const data = datas != 'clean' ? datas.resultData : []
    let error = typeof datas == 'string' && datas != 'clean' ? true : false
    let rowSelection = {
      selectedRowKeys,
      type: 'radio',
      onChange: (selectedRowKeys, selectedRows) => { this.onSelectChange(selectedRowKeys, selectedRows) },
    };
    let scroll = { x: 1670, y: 400 }
    if (lseason == 'W') {
      scroll = { x: 1630, y: 400 }
    }
    return (
      error ? <div style={{
        minHeight: '200px',
        lineHeight: '200px',
        textAlign: 'center',
        color: 'red'
      }}><span>{datas}</span></div>
        : <Table
          columns={columns}
          dataSource={data}
          rowSelection={rowSelection}
          bordered
          size="default "
          size="small"
          pagination={false}
          scroll={scroll}
        />
    )
  }
  onSelectChange = (selectedRowKeys, selectedRows) => {
    // console.log('selectedRowKeys changed: ', selectedRowKeys, selectedRows);
    this.setState({ selectedRowKeys, selectedRows });
  };
}

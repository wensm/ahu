import { connect } from 'react-redux'
import Detail from './Detail'

function getSectionName(baseSections, selectedComponent) {
  const baseSection = baseSections.find(baseSection => baseSection.metaId === selectedComponent.name)
  return baseSection && baseSection.cName
}

const mapStateToProps = state => {
  return {
    ahuValue: state.ahu.componentValue[0],
    selectedComponent: state.ahu.selectedComponent,
    sectionValue: state.ahu.componentValue[state.ahu.selectedComponent.id],
    sectionName: getSectionName(state.ahu.baseSections, state.ahu.selectedComponent),
    metaUnit: state.metadata.metaUnit,
    propUnit: state.general.user && state.general.user.unitPrefer,
    metaUnit: state.metadata.metaUnit,
    metaLayout: state.metadata.layout
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // onTodoClick: id => {
    //   dispatch(toggleTodo(id))
    // }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail)

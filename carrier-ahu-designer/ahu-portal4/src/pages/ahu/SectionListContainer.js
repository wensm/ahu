import {connect} from 'react-redux'

import SectionList from './SectionList'

const mapStateToProps = state => {
  return {
    sections: state.ahu.baseSections,
    forDirection: state.ahu.forDirection,
    canDropList:state.metadata.canDropList,

  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionList)

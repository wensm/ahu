import {
    REQUIRED,
    MUST_LESS_THAN_OR_EQUAL,
    MORE_THAN_ALERT,
    PLEASE_INPUT_CHINESE_LETTERS_NUMBER_UNDERSCORE,
    TIPS_FOR_1,
    TIPS_FOR_2
} from '../intl/i18n'

import intl from 'react-intl-universal'
export function required(value) {
    return (value || value == 0 ? undefined : intl.get(REQUIRED))
     
}
export const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined
export const maxLength15 = maxLength(15)
export const maxLength5 = maxLength(5)

// export const positiveNumber = posnum => value =>
//     value && value > posnum ? `必须为大于${posnum}的数 ` : undefined
// export const positiveNumber0 = positiveNumber(0)

export const minLength = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined
export const minLength2 = minLength(2)
export const number = value =>
    value && isNaN(Number(value)) ? 'Must be a number' : undefined
export const minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined

export const minValue18 = minValue(18)
export const minValue0 = minValue(0)
export const minValue1 = minValue(1)
export const maxValue = (max,intlStr) => value =>
    value && value > max ? `${intl.get(intlStr)} ${max}` : undefined

export const maxValue100 = maxValue(100,MUST_LESS_THAN_OR_EQUAL)
export const maxValue40 = maxValue(40,MORE_THAN_ALERT)

export const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Invalid email address'
        : undefined
export const tooOld = value =>
    value && value > 65 ? 'You might be too old for this' : undefined

export const tooCold = value =>
    value && value < -999 ? 'Too Cold!' : undefined

export const tooHot = value =>
    value && value > 999 ? 'Too Hot!' : undefined

export const aol = value =>
    value && /.+@aol\.com/.test(value)
        ? 'Really? You still use AOL for your email?'
        : undefined
export const alphaNumeric = value =>
    value && /[^a-zA-Z0-9 ]/i.test(value)
        ? 'Only alphanumeric characters'
        : undefined
export const phoneNumber = value =>
    value && !/^(0|[1-9][0-9]{9})$/i.test(value)
        ? 'Invalid phone number, must be 10 digits'
        : undefined
export const invalidCharacter = value =>{

  return  value && new RegExp("[`~!@^*()=|{}':;'\\[\\].<>?~！@￥……*（）——|{}【】‘；：”“'。、？]").test(value) ?  intl.get(PLEASE_INPUT_CHINESE_LETTERS_NUMBER_UNDERSCORE)  : undefined
}
export const noBlank = value =>{

    return  value && /\s+/.test(value) ?  intl.get(PLEASE_INPUT_CHINESE_LETTERS_NUMBER_UNDERSCORE)  : undefined
}
export const tips1 = value =>{

    return  value && new RegExp("/").test(value) ?  intl.get(TIPS_FOR_1)  : undefined
}
export const tips2 = value =>{

    return  value && new RegExp("&").test(value) ?  intl.get(TIPS_FOR_2)  : undefined
}


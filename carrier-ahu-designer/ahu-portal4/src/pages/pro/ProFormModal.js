import {
    MODIFY_PROJECT,
    NEW_PROJECT,
    PROJECT_NO,
    PROJECT_NAME,
    DATE,
    SALES,
    PROJECT_ADDRESS,
    NON_STANDARD_QUOTATION_NO,
    CONTRACT_NO,
    PRICE_BASELINE,
    CUSTOMER_NAME,
    CANCEL,
    SAVE,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { Field, reduxForm } from 'redux-form'

class ProFormModal extends React.Component {

  componentDidMount() {
    $('#proDate').datepicker({
      autoclose: true,
      todayHighlight: true
    }).on('changeDate', e => {
      this.props.onSynProDate(e.target.value)
    })
  }

  componentWillReceiveProps(nextProps) {
    $('#proDate').val(nextProps.pro.projectDate)
  }

  render() {
    const {
      pro,
      projectId,
      onSave
    } = this.props
    return (
      <div data-name="ProFormModal">
        <div className="modal fade" id="proFormModal" tabIndex="-1" role="dialog" aria-labelledby="proFormModalLabel">
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="proFormModalLabel">{projectId ? intl.get(MODIFY_PROJECT) :intl.get(NEW_PROJECT)}</h4>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-lg-4">
                    <div className="form-group">
                      <label>{intl.get(PROJECT_NO)}</label>
                      <Field name="no" className="form-control" component="input" type="text" />
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="form-group">
                      <label>{intl.get(PROJECT_NAME)}</label>
                      <Field name="name" className="form-control" component="input" type="text" />
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="form-group">
                      <label>{intl.get(DATE)}</label>
                      <Field name="projectDate" className="form-control" component="input" type="text" id="proDate" />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-4">
                    <div className="form-group">
                      <label>{intl.get(SALES)}</label>
                      <Field name="saler" className="form-control" component="input" type="text" />
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="form-group">
                      <label>{intl.get(PROJECT_ADDRESS)}</label>
                      <Field name="address" className="form-control" component="input" type="text" />
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="form-group">
                      <label>{intl.get(NON_STANDARD_QUOTATION_NO)}</label>
                      <Field name="enquiryNo" className="form-control" component="input" type="text" />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-4">
                    <div className="form-group">
                      <label>{intl.get(CONTRACT_NO)}</label>
                      <Field name="contract" className="form-control" component="input" type="text" />
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="form-group">
                      <label>{intl.get(PRICE_BASELINE)}</label>
                      <Field name="priceBase" className="form-control" component="input" type="text" />
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="form-group">
                      <label>{intl.get(CUSTOMER_NAME)}</label>
                      <Field name="customerName" className="form-control" component="input" type="text" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">{intl.get(CANCEL)}</button>
                <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={() => onSave(pro)}>{intl.get(SAVE)}</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default reduxForm({
  form: 'pro',
  enableReinitialize: true,
})(ProFormModal)

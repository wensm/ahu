import { connect } from 'react-redux'
import ProFormModal from './ProFormModal'
import {
  saveProject
} from '../../actions/pro'

function getPorject(projectId, projects, _id) {
  if (projectId) {
    const project = projects.find(project => project.pid === projectId)
    return project ? project : {}
  } else {
    return { _id }
  }
}

const mapStateToProps = state => {
  return {
    initialValues: getPorject(state.pro.projectId, state.pro.projects, state.pro._id),
    pro: state.form.pro,
    projectId: state.pro.projectId,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSave: pro => {
      if (pro && pro.values) {
        dispatch(saveProject(pro.values))
      }
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProFormModal)

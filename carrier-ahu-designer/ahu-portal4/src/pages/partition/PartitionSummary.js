import {
    ONE_CELL_EDIT_WINDOW,
    GROUP_NAME,
    EXPLANATION,
    QUANTITY,
    BASE,
    PANELY,
    FRAME_BAR,
    GENERAL_MID_FRAME,
    GENERAL_PANEL,
    DOOR_PANEL,
    WITHOUT_PANEL,
    SIDE,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { isArray } from 'lodash'
import style from './Panel.css'

import {
    PANEL_KEYS,
    PANEL_NAMES,
    PANEL_NAMES_G,
    PANEL_NAMES_XT,
    LINE_KEYS,
    LINE_NAMES
} from './DivideConstants'

//{intl.get(ONE_CELL_EDIT_WINDOW)}
export default class PartitionSummary extends React.Component {
    constructor(props) {
        super(props)
        this.getPanelInfo = this.getPanelInfo.bind(this)
    }

    getPanelTypeName(type, product) {
        let name = PANEL_NAMES.PUTONG;
        if (PANEL_KEYS[type] && product == '39CQ') {
            name = PANEL_NAMES[PANEL_KEYS[type]]
        }
        if (PANEL_KEYS[type] && product == '39G') {
            name = PANEL_NAMES_G[PANEL_KEYS[type]]
        }
        if (PANEL_KEYS[type] && product == '39XT') {
            name = PANEL_NAMES_XT[PANEL_KEYS[type]]
        }
        return name;
    }



    getDivideTypeName(type) {
        let name = LINE_NAMES.PUTONG;
        if (LINE_KEYS[type]) {
            name = LINE_NAMES[LINE_KEYS[type]]
        }
        return name;
    }
    recursion(param, arr, bool = false) {
        // console.log('param', param)
        isArray(param) && param.forEach(element => {
            if (!isArray(element)) {
                arr.push(element)
            } else {
                this.recursion(element, arr)

            }
        });
        !isArray(param) && arr.push(param)
        if (bool) {
            return arr
        }

    }

    getPanelInfo(p, key2, product) {
        let res = []
        let count = 0
        let namelist = {
            '0': '侧面（非操作面）板',
            '1': '侧面（操作面）板',
            '2': '端面板',
            '3': '右面板',
            '4': '顶面板',
            '5': '底面板'
        }
        for (let key in p) {

            for (let size in p[key][0]) {
                if (!isArray(p[key][0])) {
                    let kvs = size.split("_");
                    // let type = p[key][0][size][0]['id'] ? `${namelist[p[key][0][size][0]['id'].split('_')[0]]}` : ''
                    let type = p[key][0][size][0]['id'] ? Number(p[key][0][size][0]['id'].split('_')[0])+1 : ''
                    let name = p[key][0][size][0]['panelConnector'] ? `(${p[key][0][size][0]['panelConnector'].split('_')[0]})` : ''

                    res.push({
                        name: count == 0 ? this.getPanelTypeName(key2,product) : "",
                        size: [kvs[1], "M*", kvs[0], "M", name],
                        count: p[key][0][size].length,
                        type: type
                    })
                    count++;
                } else {
                    let result = p[key][0]
                    result = this.recursion(p[key][0][size], [], true)
                    result.forEach((element, index) => {

                        for (let _key in element) {
                            let kvs = _key.split("_");
                            // let type = element[_key][0]['id'] ? `${namelist[element[_key][0]['id'].split('_')[0]]}` : ''
                            let type = element[_key][0]['id'] ? Number(element[_key][0]['id'].split('_')[0]) + 1 : ''
                            let name = element[_key][0]['panelConnector'] ? `(${element[_key][0]['panelConnector']})` : ''
                            let has = false
                            res.forEach((_element, _index) => {
                                if (_element.keyName == _key) {
                                    has = true
                                    res[_index]['count'] = res[_index]['count'] + 1
                                }
                            })
                            
                            if (!has) {
                                res.push({
                                    keyName: _key,
                                    name: count == 0 ? this.getPanelTypeName(key2, product) : "",
                                    size: [kvs[1], "M*", kvs[0], "M", name],
                                    count: 1,
                                    type: type
                                })
                                count++;
                            }
                        }
                    })
                }
            }
        }


        return res

    }

    getDivideInfo(p, key) {//key代表框的类型
        let res = []
        let count = 0
        let namelist = {
            '0': '侧面（非操作面）板',
            '1': '侧面（操作面）板',
            '2': '端面板',
            '3': '右面板',
            '4': '顶面板',
            '5': '底面板'
        }

        for (let size in p) {
            // let type = p[size][0]['id'] ? `${namelist[p[size][0]['id'].split('_')[0]]}` : ''
            for(let _size in p[size]){

                let type = namelist[size]
                let typeSize = Number(size) + 1
                let name = p[size][_size][0]['frameLine']['lineConnector'] ? `(${p[size][_size][0]['frameLine']['lineConnector']})` : ''
                
                // console.log('ppppp', p, key, p[size], _size)
                res.push({
                    name: count == 0 ? this.getDivideTypeName(key) : "",
                    size: _size.split('_')[1] + "M" + name,
                    count: p[size][_size].length,
                    type: typeSize
                    
                })
                count++;
            }
        }

        return res

    }
    getframeBar(props){
        let {currentPartition} = props
        let width= currentPartition ? currentPartition.width : ''
        let length= currentPartition ? Number(currentPartition.length)/100 : ''
        let height= currentPartition ? currentPartition.height : ''
        return {
            length,
            width,
            height
        }
    }


    render() {
        const { currentPartition, onChangeBase} = this.props;
        const { summaries, product } = this.props;

        const { panels, summaryPanel ,dividers } = summaries;
        let {
            length,
            width,
            height
        } =  this.getframeBar(this.props)



        return (
            <div className="row">
                <div className="col-lg-12" style={{height:'940px', overflow:'scroll'}}>
                    <table className="table table-striped table-hover table-condensed"
                        style={{ border: "1px solid #f1f1f1" }}>
                        <thead>
                            <tr>
                                <th>{intl.get(GROUP_NAME)}</th>
                                <th>{intl.get(EXPLANATION)}</th>
                                <th>{intl.get(QUANTITY)}</th>
                                <th>{intl.get(SIDE)}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            {currentPartition.base != '' ?
                                <tr>
                                    <td>{intl.get(BASE)}</td>
                                    <td><input style={{width:'100%', border:'1px solid rgba(204,204,204,0.7)'}} title={currentPartition.base} key={currentPartition.pos} defaultValue = {currentPartition.base} onChange={(e)=>{

                                        onChangeBase(e.target.value, currentPartition.pos)
                                    }}/></td>
                                    <td>1</td>
                                    <td></td>
                                </tr>
                                :
                                ''
                            }
                            <tr>
                                <td>{intl.get(PANELY)}</td>
                                <td></td>
                                <td>8</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>{intl.get(FRAME_BAR)}</td>
                                <td>{`${length}M(B)`}</td>
                                <td>2</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>{`${width}M(B)`}</td>
                                <td>2</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>{`${length}M(H)`}</td>
                                <td>4</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>{`${length}M(T)`}</td>
                                <td>2</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>{`${width}M(T)`}</td>
                                <td>2</td>
                                <td></td>
                            </tr>

                            {product != '39XT' ?
                                $.map(dividers, (p, key) => {
                                    let pInfo = this.getDivideInfo(p, key, product)
                                    // console.log(dividers)
                                    return pInfo.map((info, index) => <tr key={`${key}_${index}`}>
                                        <td>{info.name}</td>
                                        <td>{info.size}</td>
                                        <td>{info.count}</td>
                                        <td>{info.type}</td>

                                    </tr>)
                                }):''
                            }
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            {product != '39XT'?
                                $.map(panels, (p, key) => {
                                    let pInfo = this.getPanelInfo(p, key, product)
                                    return pInfo.map((info, index) => <tr key ={`${key}_${index}`}>
                                        <td>{info.name}</td>
                                        <td>{info.size}</td>
                                        <td>{info.count}</td>
                                        <td>{info.type}</td>
                                    </tr>)
                                }):''
                            }
                            {product == '39XT'?
                                summaryPanel.map((obj, index) =>
                                        <tr key ={`${index}`}>
                                            <td>{obj.partName}</td>
                                            <td>{obj.partWM?obj.partLM+'M*'+obj.partWM+'M':''}</td>
                                            <td>{obj.quantity}</td>
                                            <td></td>
                                        </tr>
                                    ):''
                            }
                            {/*<tr>
                         <td>{intl.get(BASE)}</td>
                         <td>Q239CQ1480035L</td>
                         <td>1</td>
                         </tr>
                         <tr>
                         <td>{intl.get(FRAME_BAR)}</td>
                         <td>9M*6M</td>
                         <td>1</td>
                         </tr>
                         <tr>
                         <td>{intl.get(GENERAL_MID_FRAME)}</td>
                         <td>9M*6M</td>
                         <td>1</td>
                         </tr>
                         <tr>
                         <td>{intl.get(GENERAL_PANEL)}</td>
                         <td>9M*6M</td>
                         <td>1</td>
                         </tr>
                         <tr>
                         <td></td>
                         <td>9M*6M</td>
                         <td>1</td>
                         </tr>
                         <tr>
                         <td></td>
                         <td>9M*6M</td>
                         <td>1</td>
                         </tr>
                         <tr>
                         <td></td>
                         <td>9M*6M</td>
                         <td>1</td>
                         </tr>
                         <tr>
                         <td>{intl.get(DOOR_PANEL)}</td>
                         <td>5M*11M</td>
                         <td>1</td>
                         </tr>
                         <tr className="danger">
                         <td>{intl.get(WITHOUT_PANEL)}</td>
                         <td>17M*6M</td>
                         <td>1</td>
                         </tr>
                         <tr className="danger">
                         <td></td>
                         <td>17M*6M</td>
                         <td>1</td>
                         </tr>*/}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

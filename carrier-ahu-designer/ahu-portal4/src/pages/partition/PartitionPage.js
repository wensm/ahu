import {
    TO_LEFT,
    TO_RIGHT,
    HOME,
    SPLIT_SECTION,
    PANEL_ARRANGEMENT,
    UNIT_MODEL,
    PIPE_CONNECTION_DIRECTION,
    OPEN_DOOR_DIRECTION,
    CAN_EDIT_SECTION_BY_RIGHT_CLICK_MENU,
    TRANSFORMER,
    TRANSFORMERTIP,
    PROJECT
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { Link, browserHistory } from 'react-router'
import Header from '../../components/Header'
import AHUContainer from './AHUContainer'
import PartitionDetailContainer from './PartitionDetailContainer'
import ButtonsContainer from './ButtonsContainer'
import PartitionTipsContainer from './PartitionTipsContainer'
// import setupAhuLine from '../globalTools'
import SectionListContainer from '../ahu/SectionListContainer'
import { Input, Button, Modal } from 'antd';

export default class PartitionPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            serieState: undefined
        }
    }
    componentDidMount() {
        const { onFetchAhuInfo, onFetchAhuSections, onFetchAhuPartitions, onFetchPartitionMeta, params, onResetTip, onFetchThreeViews, location, route, panelSeries, onFetchCheck } = this.props;
        let type = route && route.path.split('/')[0] == 'partition' ? 'partition' : 'panel'
        if (type == 'partition') {
            onFetchPartitionMeta()
            onFetchAhuInfo(params.ahuId)
            onFetchAhuPartitions(params.ahuId)
        } else if (type == 'panel') {
            this.props.onPanelInit(params.ahuId, () => {
                onFetchAhuInfo(params.ahuId)
                onFetchAhuPartitions(params.ahuId)
                onFetchCheck(params.ahuId)
                // onFetchThreeViews(params.ahuId, 'panel')
            // }, panelSeries)
            }, '')
            // onResetTip()
        }
        let copyProps = { ...this.props }
        copyProps.layout = copyProps.ahuLayout
        // setupAhuLine(copyProps)

    }
    componentWillUnmount() {
        this.props.onClean()
    }


    getDirectionStr(dir) {
        if (dir == 'left') {
            return intl.get(TO_LEFT)
        } else if (dir == 'right') {
            return intl.get(TO_RIGHT)
        } else {
            return ""
        }
    }
    doGoBack() {
        this.props.onResetTip();
        browserHistory.goBack();
    }
    doTransformer = (param) => {
        // console.log('tt', this)
        let { serieState } = this.state
        let serie = serieState === undefined ? param: serieState
        if ((serie && serie.length !== 4)||serie === '') {

            Modal.error({
                // title: 'This is a notification message',2.25
                content: (
                    <div style={{ marginTop: '-8px' }}>
                        <p>{intl.get(TRANSFORMERTIP)}</p>
                    </div>
                ),
                onOk() { },
            });
        } else {
            const { params, onFetchAhuInfo, onFetchAhuPartitions, panelSeries } = this.props
            let copyPanelSeries = panelSeries

            let product = copyPanelSeries.substring(0, copyPanelSeries.length - 4)
            let transformer = `${product}${serie}`
            this.props.onPanelInit(params.ahuId, () => {
                onFetchAhuInfo(params.ahuId)
                onFetchAhuPartitions(params.ahuId)
                // onFetchThreeViews(params.ahuId, 'panel')
            }, transformer)
        }
    }

    render() {
        const projectId = this.props.params.projectId
        const ahuId = this.props.params.ahuId
        const { serieState } = this.state
        let {
            editingAHUProps,
            currentPartitions,
            currentPanelIndex,
            location,
            tips,
            locale,
            panelConfirmTip,
            panelSeries
        } = this.props;
        let copyPanelSeries = panelSeries
        let defaultType = false
        if (tips == 'default') {
            tips = intl.get(CAN_EDIT_SECTION_BY_RIGHT_CLICK_MENU);
            defaultType = true
        }
        let product = copyPanelSeries ? copyPanelSeries.substring(0, copyPanelSeries.length - 4) : ''
        let serie = copyPanelSeries ? copyPanelSeries.substr(copyPanelSeries.length - 4) : ''
        return (

            <div data-name="PartitionPage">
                <ol className="breadcrumb" style={{ marginTop: '20px' }}>
                    <li><Link to="/">{intl.get(HOME)}</Link></li>
                    <li><Link to={`page2/${projectId}`}>{intl.get(PROJECT)}</Link></li>
                    <li><Link to={`projects/${projectId}/ahus/${ahuId}`}>AHU</Link></li>
                    {!location.pathname.startsWith("/panel") && <li className="active">{intl.get(SPLIT_SECTION)}</li>}
                    {location.pathname.startsWith("/panel") && <li className="active">{intl.get(PANEL_ARRANGEMENT)}</li>}
                </ol>

                <div className="row">
                    <div className="col-lg-2"></div>
                    {location.pathname.startsWith("/partition") && <div className="col-lg-3">{intl.get(UNIT_MODEL)} : {(editingAHUProps["meta.ahu.serial"])}</div>}
                    {location.pathname.startsWith("/panel") &&<div className="col-lg-3">{intl.get(UNIT_MODEL)} :<span>{product}</span><Input size="small" style={{ display: 'inline-block', width: '100px' }} placeholder="" onChange={(e) => { this.setState({ serieState: e.target.value }) }} defaultValue={serieState === undefined ? serie: serieState} value={serieState  === undefined ? serie : serieState} />
                        <Button type="primary" size="small" style={{ display: 'inline-block', marginLeft: '10px' }} onClick={() => this.doTransformer(serie)}>{intl.get(TRANSFORMER)}</Button>
                    </div>}
                    <div className="col-lg-3">{intl.get(OPEN_DOOR_DIRECTION)} : {this.getDirectionStr(editingAHUProps["meta.ahu.doororientation"])}</div>
                    <div className="col-lg-3">{intl.get(PIPE_CONNECTION_DIRECTION)} : {this.getDirectionStr(editingAHUProps["meta.ahu.pipeorientation"])}</div>
                </div>

                {(location.pathname.startsWith("/partition") || location.pathname.startsWith("/panel")) && <AHUContainer ahuId={ahuId} {...this.props} isPartition={location.pathname.startsWith("/partition")} />}
                <PartitionTipsContainer panelConfirmTip={panelConfirmTip} defaultMessage={tips} defaultType={defaultType} />
                {
                    location.pathname.startsWith("/partition") && <ButtonsContainer  {...this.props} onGoBack={() => {
                        this.doGoBack()
                    }}

                    />}
                {location.pathname.startsWith("/panel") && <PartitionDetailContainer {...this.props} onGoBack={() => {
                    this.doGoBack()
                }} />}
            </div>
        )
    }
}

import {connect} from 'react-redux'
import PartitionDetail from './PartitionDetail'
import {PARTITION_FACE_SWITCHED, PARTITION_PANEL_RECT_UPDATE_CONFIRM,PARTITION_PANEL_SELECTED, getPanelCaption,savePartitionImage, savePartitions,
    spin, syncAB
} from '../../actions/partition'
import {
    panelInit
} from '../../actions/ahu'
const mapStateToProps = state => {

    var s = {
        currentPartitions: state.partition.editingAHUPartitions,
        currentPanelIndex: state.partition.currentPanelIndex,
        currentPartitionIndex: state.partition.currentPartitionIndex,
        caption: getPanelCaption(state.partition.currentPanelIndex),
        currentPartitionPObj:state.partition.currentPartitionPObj,
        currentPanel: state.partition.currentEPanel,
        panel: state.partition.currentESubPanel,
        parentPanel: state.partition.currentEParentPanel,
        show: state.partition.show,
        user: state.general.user,
        spinning:state.partition.spinning,
        threeView: state.ahu.threeView,
        checkImg:state.partition.checkImg
    }
    return s;
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onFaceChanged: (currentPartitions, currentPartitionIndex, face) => {
            dispatch(faceChanged(currentPartitions, currentPartitionIndex, face))
        },
        onPartitionImageSave: (currentPartitionPObj,imgData, show, callBack) => {
            dispatch(savePartitionImage(currentPartitionPObj, imgData, show, callBack))
        },
        onShowPanelIndex: () => {
            dispatch(panelSelected(null))
        },
        onSpin:(bool)=>{
            dispatch(spin(bool))
        },
        onDoConfirm: () => {
            dispatch(doConfirm())
            dispatch(spin(true))
        },
        onSavePartitoins: (currentPartitionPObj, partitions, num) => {
            dispatch(savePartitions(currentPartitionPObj, partitions, num))
        },
        onSyncAB:(panels)=>{
            dispatch(syncAB(panels))
        },
        
    }
}

function doConfirm() {
    return {
        type: PARTITION_PANEL_RECT_UPDATE_CONFIRM,
    }
}


function panelSelected(currentPanel) {
    return {
        type: PARTITION_PANEL_SELECTED,
        currentPanel: currentPanel
    }
}

function faceChanged(currentPartitions, currentPartitionIndex, face) {
    return {
        type: PARTITION_FACE_SWITCHED,
        currentPanel: currentPartitions[currentPartitionIndex].panels[face], face: face
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PartitionDetail)

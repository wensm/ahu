import {
    ONE_CELL_EDIT_WINDOW,
    LENGTH,
    WIDE,
    HIGH,
    TYPE,
    PANEL,
    MEMO,
    TYPECATEGORY
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import style from './Panel.css'
import classnames from 'classnames'

import {
    PANEL_KEYS,
    PANEL_TYPES,
    PANEL_NAMES,
    PANEL_BGS,

    LINE_KEYS,
    LINE_TYPES,
    LINE_NAMES,
    LINE_BGS,
    LINE_NAMES_XT,
    LINE_TYPES_XT,
    PANEL_NAMES_G
} from './DivideConstants'
import { Row, Col } from 'antd'

//{intl.get(ONE_CELL_EDIT_WINDOW)}
export default class RectEditor extends React.Component {
    constructor(props) {
        super(props)
        this.valueChanged = this.valueChanged.bind(this)
        this.typeChange = this.typeChange.bind(this)
        this.lineChange = this.lineChange.bind(this)
        this.state = {
            num: 10
        }
        this._lastChangeTimer = null
    }

    valueChanged(event, nameType, min, max) {
        // console.log('event, nameType', event, nameType)
        if (this._lastChangeTimer) {
            clearTimeout(this._lastChangeTimer)
            this._lastChangeTimer = null
        }
        const target = event.target;

        this._lastChangeTimer = setTimeout(() => {

            let value = target.type === 'checkbox' ? target.checked : target.value;
            if(min){
                if(Number(value)<min) value = 1
                if(Number(value)>(Number(max)-1)) value = Number(max)-1
            }
            const name = target.name;
            this.props.onSubPanelChanged(this.props.currentPanel, this.props.parentPanel, this.props.panel, value, name, this.props.currentPartitionIndex)

            this._lastChangeTimer = null
        }, 400)

    }

    typeChange(event) {

        const target = event.target;
        const value = target.value;
        // console.log("typeChange " + value)
        this.props.onSubPanelTypeChanged(this.props.currentPanel, this.props.parentPanel, this.props.panel, value, this.props.currentPartitionIndex)
    }

    lineChange(event) {

        const target = event.target;
        const value = target.value;
        // console.log("lineChange " + value)
        this.props.onSubPanelLineChanged(this.props.currentPanel, this.props.parentPanel, this.props.panel, value)
    }


    render() {
        const {
            currentPanel, panel, parentPanel, panelPos, product
        } = this.props
        let label = (panelPos == '0' || panelPos == '1' || panelPos == '2' || panelPos == '3') ? intl.get(LENGTH) : intl.get(WIDE)
        let disableLabel = (panelPos == '0' || panelPos == '1') ? intl.get(WIDE) : intl.get(HIGH)

        let num = panel.panelLength
        let disableNum = panel.panelWidth
        let max = parentPanel.panelLength
        let direction = parentPanel.direction;
        let arr = []
        let type = panel.panelConnector
        let memo = panel.memo
        if (parentPanel) {
            if (parentPanel.direction == 1) {
                label = (panelPos == '0' || panelPos == '1') ? intl.get(WIDE) : intl.get(HIGH)
                disableLabel = (panelPos == '0' || panelPos == '1' || panelPos == '2' || panelPos == '3') ? intl.get(LENGTH) : intl.get(WIDE)
                num = panel.panelWidth
                disableNum = panel.panelLength
                max = parentPanel.panelWidth
            }
        }

        const hasParent = parentPanel && parentPanel.id != null && panel && parentPanel.id !== panel.id
        const hasSub = panel.subPanels && panel.subPanels.length > 0

        let panelTypes = [
            { text: PANEL_NAMES.PUTONG, id: PANEL_TYPES.PUTONG },
            { text: PANEL_NAMES.WU, id: PANEL_TYPES.WU },
            { text: PANEL_NAMES.WAI, id: PANEL_TYPES.WAI },
            { text: PANEL_NAMES.MEN, id: PANEL_TYPES.MEN },
            { text: PANEL_NAMES.CHUFENG, id: PANEL_TYPES.CHUFENG },
            { text: PANEL_NAMES.JIASHI, id: PANEL_TYPES.JIASHI },
            { text: PANEL_NAMES.JIAQIANG, id: PANEL_TYPES.JIAQIANG },
            { text: PANEL_NAMES.ZHENGYA, id: PANEL_TYPES.ZHENGYA },
            { text: PANEL_NAMES.CHAIXIE, id: PANEL_TYPES.CHAIXIE },
            { text: PANEL_NAMES.MEN_MING, id: PANEL_TYPES.MEN_MING }
        ]
        let lineTypes = [
            { text: LINE_NAMES.PUTONG, id: LINE_TYPES.PUTONG },
            { text: LINE_NAMES.JIAQIANG, id: LINE_TYPES.JIAQIANG },
            { text: LINE_NAMES.NEI, id: LINE_TYPES.NEI },
            { text: LINE_NAMES.ZUHE, id: LINE_TYPES.ZUHE },
        ]
        if(product == '39G'){
            panelTypes = [
                { text: PANEL_NAMES_G.PUTONG, id: PANEL_TYPES.PUTONG },
                { text: PANEL_NAMES_G.WU, id: PANEL_TYPES.WU },
                { text: PANEL_NAMES_G.WAI, id: PANEL_TYPES.WAI },
                { text: PANEL_NAMES_G.MEN, id: PANEL_TYPES.MEN },
                { text: PANEL_NAMES_G.CHUFENG, id: PANEL_TYPES.CHUFENG },
                { text: PANEL_NAMES_G.JIASHI, id: PANEL_TYPES.JIASHI },
                { text: PANEL_NAMES_G.JIAQIANG, id: PANEL_TYPES.JIAQIANG },
                { text: PANEL_NAMES_G.ZHENGYA, id: PANEL_TYPES.ZHENGYA },
                { text: PANEL_NAMES_G.CHAIXIE, id: PANEL_TYPES.CHAIXIE },
                { text: PANEL_NAMES_G.MEN_MING, id: PANEL_TYPES.MEN_MING }
            ]
            lineTypes = [
                { text: LINE_NAMES.PUTONG, id: LINE_TYPES.PUTONG },
                { text: LINE_NAMES.JIAQIANG, id: LINE_TYPES.JIAQIANG },
                { text: LINE_NAMES.NEI, id: LINE_TYPES.NEI },
                { text: LINE_NAMES.ZUHE, id: LINE_TYPES.ZUHE },
            ]
        }else if(product == '39XT'){
            panelTypes = [
                { text: PANEL_NAMES.PUTONG, id: PANEL_TYPES.PUTONG },
                { text: PANEL_NAMES.WU, id: PANEL_TYPES.WU },
                { text: PANEL_NAMES.MEN, id: PANEL_TYPES.MEN },
            ]
            lineTypes = [
                { text: LINE_NAMES.PUTONG, id: LINE_TYPES.PUTONG },                
                { text: LINE_NAMES_XT.NEIJIAOGANG, id: LINE_TYPES_XT.NEIJIAOGANG },
                { text: LINE_NAMES_XT.NEIJIAOGANGZHUANGSHI, id: LINE_TYPES_XT.NEIJIAOGANGZHUANGSHI },
            ]
        }

        let frameLine = panel && panel.frameLine
        let frameLineConnector = frameLine ? frameLine.lineConnector : ''
        let frameLineLength = frameLine ? Number(frameLine.lineLength) : ''
        // console.log('frameLine', panel, hasSub, frameLine, frameLineLength, frameLineConnector )

        return (
            <div className="row" style={{margin:0}}>
                {/* <fieldset onSubmit={this.handleSubmit}> */}
                {currentPanel && hasParent && !hasSub && <div className="input-group input-group-md col-lg-12">
                    <span className="input-group-addon" id="sizing-addon1"
                        style={{ minWidth: "5em" }}>&nbsp;{label}&nbsp;</span>
                    <input type="number"
                        className="form-control"
                        style={{ minWidth: "6em", width: '50px' }}
                        ref={(el) => {
                            $(el).val(num)
                        }
                        }
                        // onChange={(e) => this.valueChanged(e, 'num', 1, max)} 
                        onBlur = {(e) => this.valueChanged(e, 'num', 1, max)}
                        min="1"
                        name="num"
                        max={max}
                        defaultValue={num}
                        aria-describedby="sizing-addon1" />



                </div>}
                {currentPanel && hasParent && !hasSub && <div className="input-group input-group-md col-lg-12" style={{ margin: '0 0 5px 0' }}>
                    <span className="input-group-addon" id="sizing-addon1"
                        style={{ minWidth: "5em" }}>&nbsp;{disableLabel}&nbsp;</span>
                    <input type="number"
                        disabled
                        className="form-control"
                        style={{ minWidth: "6em", width: '50px' }}
                        ref={(el) => {
                            $(el).val(disableNum)
                        }
                        }
                        onChange={this.valueChanged} min="1"
                        name="disableNum"
                        max={max}
                        defaultValue={disableNum}
                        aria-describedby="sizing-addon1" />
                </div>}
                {
                    //框
                }
                {currentPanel && hasSub && <div className="input-group input-group-md col-lg-12">
                    <span className="input-group-addon" id="sizing-addon1"
                        style={{ minWidth: "5em" }}>&nbsp;{label}&nbsp;</span>
                    <input type="number"
                        disabled
                        className="form-control"
                        style={{ minWidth: "6em", width: '50px' }}
                        ref={(el) => {
                            $(el).val(frameLineLength)
                        } }
                        name="frameLineLength"
                        defaultValue={frameLineLength}
                        aria-describedby="sizing-addon1" />



                </div>}
                {
                    //类别
                }
                {!hasSub && <div className="input-group input-group-md col-lg-12" style={{ margin: '0 0 5px 0' }}>
                    <span className="input-group-addon" id="sizing-addon1"
                        style={{ minWidth: "5em" }}>&nbsp;{intl.get(TYPECATEGORY)}&nbsp;</span>
                    <input type="text"
                        disabled={product == '39CQ' ? false : true}
                        className="form-control"
                        style={{ minWidth: "6em", width: '50px' }}
                        ref={(el) => {
                            $(el).val(type)
                        }
                        }
                        onChange={(e) => this.valueChanged(e, 'type')}
                        name="type"
                        defaultValue={type}
                        aria-describedby="sizing-addon1" />
                </div>}

                {
                    //类别
                }
                {hasSub && <div className="input-group input-group-md col-lg-12" style={{ margin: '0 0 5px 0' }}>
                    <span className="input-group-addon" id="sizing-addon1"
                        style={{ minWidth: "5em" }}>&nbsp;{intl.get(TYPECATEGORY)}&nbsp;</span>
                    <input type="text"
                        disabled={product == '39CQ' ? false : true}
                        className="form-control"
                        style={{ minWidth: "6em", width: '50px' }}
                        ref={(el) => {
                            $(el).val(frameLineConnector)
                        }}
                        onChange={(e) => this.valueChanged(e, 'lineConnector')}
                        name="lineConnector"
                        defaultValue={frameLineConnector}
                        aria-describedby="sizing-addon1" />
                </div>}

                {currentPanel && !panel.subPanels &&
                    <div className="input-group input-group-md col-lg-12"> 
                        <span className="input-group-addon" id="sizing-addon2" style={{ minWidth: "5em"}}>&nbsp;{intl.get(PANEL)}</span>

                        <select ref={(el) => {
                            $(el).val(panel.panelType)
                            $(el).selectpicker({
                                width: "100%"
                            }).selectpicker('val', panel.panelType || 0);

                        }}
                            aria-describedby="sizing-addon2"

                            className="selectpicker" defaultValue={panel.panelType} onChange={this.typeChange} >

                            {
                                panelTypes.map((pt) => <option key={pt.id} value={pt.id}>{pt.text}</option>)
                            }
                        </select>

                    </div>
                }
                {!hasSub && <div className="input-group input-group-md col-lg-12">
                    <span className="input-group-addon" id="sizing-addon1"
                        style={{ minWidth: "5em" }}>&nbsp;{intl.get(MEMO)}&nbsp;</span>
                    <input type="type"
                        className="form-control"
                        style={{ minWidth: "6em", width: '50px' }}
                        ref={(el) => {
                            $(el).val(memo)
                        }
                        }
                        onChange={(e) => this.valueChanged(e, 'memo')}
                        name="memo"

                        defaultValue={memo}
                        aria-describedby="sizing-addon1" />



                </div>}
                {hasSub &&
                    <div className="input-group input-group-md col-lg-12">
                        <span className="input-group-addon" id="sizing-addon3" style={{ minWidth: "5em" }}>切割线</span>

                        <select ref={(el) => {
                            $(el).selectpicker({
                                width: "100%"
                            }).selectpicker('val', (panel.frameLine && panel.frameLine.type) || 0);

                        }} className="selectpicker" defaultValue={panel.frameLine && panel.frameLine.type} onChange={this.lineChange}>

                            {
                                lineTypes.map((pt) => <option key={pt.id} value={pt.id}>{pt.text}</option>)
                            }
                        </select>

                    </div>
                }


                {/* </fieldset> */}

            </div>
        )
    }
}

RectEditor.defaultProps = {
    panel: {},
    parentPanel: {}
};

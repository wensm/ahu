import {
    GENERAL_PANEL,
    WITHOUT_PANEL,
    OUTSIDE_PANEL,
    DOOR_PANEL,
    OUTLET_AIR_PANEL,
    HUMIDIFICATION_PANEL,
    GENERAL_MID_FRAME,
    STRENGTHEN_MID_FRAME,
    COMBINED_MID_FRAME,
    INSIDE_ANGLE_STEEL,
    INTERIOR_ANGLE_STEEL_DECORATIVE_STRIP
} from '../intl/i18n'

/**
 * Created by zhouzechen on 2018/3/29.
 */
import intl from 'react-intl-universal'

import panel_bg_chufeng from '../../images/divide/im_panel_chufeng.png'
import panel_bg_jiashi from '../../images/divide/im_panel_jiashi.png'
import panel_bg_men from '../../images/divide/im_panel_men.png'
import panel_bg_wai from '../../images/divide/im_panel_wai.png'
import panel_bg_wu from '../../images/divide/im_panel_wu.png'
import panel_bg_zhengya from '../../images/divide/im_panel_zhengya.png'
import panel_bg_jiaqiangdi from '../../images/divide/im_panel_jiaqiangdi.png'
import panel_bg_chaixie from '../../images/divide/im_panel_chaixie.png'

import im_line_jiaqiang from '../../images/divide/im_line_jiaqiang.png'
import im_line_nei from '../../images/divide/im_line_nei.png'
import im_line_zuhe from '../../images/divide/im_line_zuhe.png'
import im_line_pt from '../../images/divide/im_line_pt.png'

import im_line_jiaqiang_v from '../../images/divide/im_line_jiaqiang_v.png'
import im_line_nei_v from '../../images/divide/im_line_nei_v.png'
import im_line_zuhe_v from '../../images/divide/im_line_zuhe_v.png'
import im_line_pt_v from '../../images/divide/im_line_pt_v.png'


const PANEL_TYPES = {
    "PUTONG": 0,
    "WAI": 1,
    "MEN": 2,
    "CHUFENG": 3,
    "WU": 4,
    "JIASHI": 5,
    "JIAQIANG": 6,
    "ZHENGYA": 7,
    "CHAIXIE": 8,
    "MEN_MING": 9,
};

//逆
let PANEL_KEYS = {}
for (let id in PANEL_TYPES) {
    PANEL_KEYS[PANEL_TYPES[id]]=id;
}

const PANEL_NAMES = {
    "PUTONG": GENERAL_PANEL,//"普通面板",
    "WU": WITHOUT_PANEL,//"无面板",
    "WAI": OUTSIDE_PANEL,//"外面板",
    "MEN": DOOR_PANEL,//"门面板",
    "CHUFENG": OUTLET_AIR_PANEL,//"出风面板",
    "JIASHI": HUMIDIFICATION_PANEL,//"加湿面板",
    "JIAQIANG": "title.moon_intl_str_0809",//"加强底面",
    "ZHENGYA": "title.moon_intl_str_0810",//"正压门",
    "CHAIXIE": "title.moon_intl_str_0811",//"拆卸式门",
    "MEN_MING": "title.moon_intl_str_0812",//"门面板（铭牌）",
};
const PANEL_NAMES_G = {
    "PUTONG": GENERAL_PANEL,//"普通面板",
    "WU": WITHOUT_PANEL,//"无面板",
    "WAI": OUTSIDE_PANEL,//"外面板",
    "MEN": DOOR_PANEL,//"门面板",
    "CHUFENG": OUTLET_AIR_PANEL,//"出风面板",
    "JIASHI": HUMIDIFICATION_PANEL,//"加湿面板",
    "JIAQIANG": "title.moon_intl_str_0817",//"加强底面",
    "ZHENGYA": "title.moon_intl_str_0810",//"正压门",
    "CHAIXIE": "title.moon_intl_str_0811",//"拆卸式门",
    "MEN_MING": "title.moon_intl_str_0812",//"门面板（铭牌）",
    "JIAQIANG2": "title.moon_intl_str_0817",//
    
};
const PANEL_NAMES_XT = {
    "PUTONG": GENERAL_PANEL,//"普通面板",
    "WU": WITHOUT_PANEL,//"无面板",
    "WAI": OUTSIDE_PANEL,//"外面板",
    "MEN": DOOR_PANEL,//"门面板",
    "CHUFENG": OUTLET_AIR_PANEL,//"出风面板",
    "JIASHI": HUMIDIFICATION_PANEL,//"加湿面板",
    "JIAQIANG": "title.moon_intl_str_0809",//"加强底面",
    "ZHENGYA": "title.moon_intl_str_0810",//"正压门",
    "CHAIXIE": "title.moon_intl_str_0811",//"拆卸式门",
    "MEN_MING": "title.moon_intl_str_0812",//"门面板（铭牌）",
};

const PANEL_BGS = {
    "PUTONG": "",
    "WU": panel_bg_wu,
    "WAI": panel_bg_wai,
    "MEN": panel_bg_men,
    "CHUFENG": panel_bg_chufeng,
    "JIASHI": panel_bg_jiashi,
    "JIAQIANG": panel_bg_jiaqiangdi,
    "ZHENGYA": panel_bg_zhengya,
    "CHAIXIE": panel_bg_chaixie,
    "MEN_MING": panel_bg_men
};
const PANEL_BGS_G = {
    "PUTONG": "",
    "WU": panel_bg_wu,
    "WAI": panel_bg_wai,
    "MEN": panel_bg_men,
    "CHUFENG": panel_bg_chufeng,
    "JIASHI": panel_bg_jiashi,
    "JIAQIANG": "",
    "ZHENGYA": panel_bg_zhengya,
    "CHAIXIE": panel_bg_chaixie,
    "MEN_MING": panel_bg_men
};
const PANEL_BGS_XT = {
    "PUTONG": "",
    "WU": panel_bg_wu,
    "MEN": panel_bg_men,
};



const LINE_TYPES = {
    "PUTONG": 0,
    "JIAQIANG": 1,
    "NEI": 2,
    "ZUHE": 3
};
const LINE_TYPES_XT = {
    "NEIJIAOGANG": 1,
    "NEIJIAOGANGZHUANGSHI": 2,
};


//逆
let LINE_KEYS = {}
for (let id in LINE_TYPES) {
    LINE_KEYS[LINE_TYPES[id]]=id;
}

const LINE_NAMES = {
    "PUTONG": GENERAL_MID_FRAME,// "普通中框",
    "JIAQIANG": STRENGTHEN_MID_FRAME,//"加强中框",
    "NEI": "title.moon_intl_str_0815",//"内面板框",
    "ZUHE": COMBINED_MID_FRAME,//"组合中框"
};
const LINE_NAMES_XT = {
    "NEIJIAOGANG": INSIDE_ANGLE_STEEL,
    "NEIJIAOGANGZHUANGSHI": INTERIOR_ANGLE_STEEL_DECORATIVE_STRIP,
};
const LINE_BGS = {
    "PUTONG": [im_line_pt, im_line_pt_v],
    "JIAQIANG": [im_line_jiaqiang, im_line_jiaqiang_v],
    "NEI": [im_line_nei, im_line_nei_v],
    "ZUHE": [im_line_zuhe, im_line_zuhe_v]
};
const LINE_BGS_XT = {
    "NEIJIAOGANG": [im_line_jiaqiang, im_line_jiaqiang_v],
    "NEIJIAOGANGZHUANGSHI": [im_line_nei, im_line_nei_v],
};


export {

    PANEL_KEYS,
    PANEL_TYPES,
    PANEL_NAMES,
    PANEL_NAMES_G,
    PANEL_NAMES_XT,
    PANEL_BGS,
    LINE_KEYS,
    LINE_TYPES,
    LINE_NAMES,
    LINE_BGS,
    PANEL_BGS_G,
    PANEL_BGS_XT,
    LINE_NAMES_XT,
    LINE_BGS_XT,
    LINE_TYPES_XT
}


import {connect} from 'react-redux'
import AHU from './AHU'

const mapStateToProps = state => {
    return {
        currentPartitionIndex: state.partition.currentPartitionIndex,
        partitions: state.partition.editingAHUPartitions,
        layout: state.partition.layout,
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {

    }
}



export default connect(mapStateToProps, mapDispatchToProps)(AHU)
import { connect } from 'react-redux'
import AHUTips from '../ahu/AHUTips'

function ahuSerial(ahu) {
  if (ahu && ahu.meta_ahu_serial) {
    return true
  }
  return false
}

const mapStateToProps = state => {
  return {
    validation: state.ahu.validation,
    ahuSerial: ahuSerial(state.ahu.componentValue[0]),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // onTodoClick: id => {
    //   dispatch(toggleTodo(id))
    // }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AHUTips)

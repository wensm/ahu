import {
    PREVIOUS,
    NEXT,
    SPLIT,
    SAVE,
    BACK,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'

import style from './Buttons.css'
import { Spin } from 'antd'

export default class Buttons extends React.Component {

    constructor(props) {
        super()
        this.onSavePartition = this.onSavePartition.bind(this)
        this.forward = this.forward.bind(this)
        this.backward = this.backward.bind(this)
        this.split = this.split.bind(this)
        this.goBack = this.goBack.bind(this)
    }
    componentDidMount(){
        this.props.onSpin(false)
    }
    onSavePartition() {
        const {
            currentPartitionPObj,
            editingAHUPartitions
        } = this.props
        currentPartitionPObj.saveFrom = this.props.route.path.split('/')[0]

        this.props.onSavePartitoins(currentPartitionPObj,
            editingAHUPartitions)
    }

    forward() {
        this.props.onForward(this.props.currentPartitionIndex, this.props.layout)
    }

    backward() {
        this.props.onBackward(this.props.currentPartitionIndex, this.props.layout)
    }

    split() {
        this.props.onSplit(this.props.currentPartitionIndex, this.props.layout)
    }

    goBack() {
        this.props.onGoBack()
    }

    render() {
        let { spinning } = this.props
        return (
            <div className={style.Buttons}>
                <Spin spinning={spinning} size="large" style={{left:0}}>
                    <div>
                        <button type="button" className="btn btn-default" style={{ marginRight: 5 }}
                            onClick={this.forward}>{intl.get(PREVIOUS)}
                        </button>
                        <button type="button" className="btn btn-default" style={{ marginRight: 5 }}
                            onClick={this.backward}>{intl.get(NEXT)}
                        </button>
                        <button type="button" className="btn btn-default" style={{ marginRight: 5 }}
                            onClick={this.split}>{intl.get(SPLIT)}
                        </button>
                        <button type="button" className="btn btn-primary" style={{ marginRight: 5 }}
                            onClick={this.onSavePartition}>{intl.get(SAVE)}
                        </button>
                        <button type="button" className="btn btn-primary"
                            onClick={this.goBack}>{intl.get(BACK)}
                        </button>
                    </div>
                </Spin>
            </div>
        )
    }
}

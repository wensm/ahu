import React from 'react'
import classnames from 'classnames'

import style from '../ahu/Section.css'
import styleS from './Section.css'
import {
  CAL_COILFAN_LISTPRICE_DESC
} from '../intl/i18n'


import {
  isSingleLine, isTwinLine, STYLE_CHAMBER, STYLE_COMMON, STYLE_PLATE, STYLE_WHELE,
  STYLE_VERTICAL_1, STYLE_VERTICAL_2, STYLE_DOUBLE_RETURN_1, STYLE_DOUBLE_RETURN_2,
  STYLE_SIDE_BY_SIDE_RETURN, STYLE_SIDE_BY_SIDE_RETURN_2
} from "../../actions/ahu"

import mix from '../../images/pro_mix_b.png'
const requireContext = require.context("../../images/mix", false, /^\.\/.*\.png$/);
const images = requireContext.keys().map(requireContext);
import { Tooltip } from 'antd'
import fan1 from '../../images/pro_fan_l1.png'
import fan2 from '../../images/pro_fan_r2.png'
import fan3 from '../../images/pro_fan_t1.png'
import fan4 from '../../images/pro_fan_r1.png'
import fan5 from '../../images/pro_fan_t2.png'
import plugfan from '../../images/pro_plugfan.png'
import plugfanL from '../../images/pro_plugfan_l.png'

import comb from '../../images/put_icon8_1.png'
import comb2 from '../../images/put_icon2_1.png'
const MAP = {
  mix,
  fan1, fan2, fan3, fan4, fan5, comb, comb2, plugfan, plugfanL
}
images.map((v, k) => {
  MAP[`mix${k + 1}`] = v
})




export default class Section extends React.Component {


  // componentDidMount() {
  //   let{metaId, index, loc, nextProps}=this.props
  //   let id = `${loc}_${metaId}_${index}`

  //   // console.log('did', $(`[data-name-id='${id}']`),index)
  //   $('[data-toggle="tooltip"]').tooltip()
  //   let hasCombine = false
  //   let letmix = null
  //   // let asection = $(`[data-section='${section.sectionKey}']`).attr("data-section")
  //   let asection = this.props.metaId
  //   // console.log(asection)
  //   let ahuDirection = JSON.parse(nextProps.sections[index].metaJson)
  //   switch (asection) {
  //     case 'ahu.fan':
  //       // nextProps.sections.map((k, index) => {
  //         // if (index == nextProps.sections[index].position - 1) {
  //           switch (ahuDirection['meta.section.fan.outletDirection']) {
  //             case 'BHF':
  //               letmix = MAP.fan2
  //               break;
  //             case 'UBF':
  //               letmix = MAP.fan3
  //               break;
  //             case 'UBR':
  //               letmix = MAP.fan4
  //               break;
  //             default:
  //               if (ahuDirection['meta.section.airDirection'] == "R" && hasCombine === false) {
  //                 letmix = MAP.fan5
  //               } else {
  //                 letmix = MAP.fan1
  //               }
  //               break;
  //           }
  //           if (ahuDirection['meta.section.fan.outlet'] == "wwk") {
  //             if ((nextProps.layout.style == STYLE_PLATE || nextProps.layout.style == STYLE_WHELE)
  //               && (ahuDirection['meta.section.airDirection'] == "R")) {
  //               letmix = MAP.plugfanL
  //             } else {
  //               letmix = MAP.plugfan
  //             }
  //           }
  //           $(`[data-name-id='${id}']`).css("background-image", 'url(' + letmix + ')')
  //         // }
  //       // })
  //       break;
  //     case 'ahu.mix':
  //       // nextProps.sections.map((k, index) => {
  //       //   if (index == nextProps.sections[index].position - 1) {
  //           // let ahuDirection = JSON.parse(nextProps.sections[index].metaJson)
  //           // console.log('ahuDirection',ahuDirection)
  //           function imgSet(mix1, mix2, mix3, mix4, mix5, mix6, mix7, mix8, mix9, mix10, mix11, mix12, mix13, mix14, mix15) {
  //             if (eval(ahuDirection['meta.section.mix.returnTop']) == true && eval(ahuDirection['meta.section.mix.returnButtom']) == true) {
  //               letmix = MAP[mix1]
  //             } else if (eval(ahuDirection['meta.section.mix.returnTop']) == true && eval(ahuDirection['meta.section.mix.returnBack']) == true) {
  //               letmix = MAP[mix2]
  //             } else if (eval(ahuDirection['meta.section.mix.returnTop']) == true && eval(ahuDirection['meta.section.mix.returnRight']) == true) {
  //               letmix = MAP[mix3]
  //             } else if (eval(ahuDirection['meta.section.mix.returnTop']) == true && eval(ahuDirection['meta.section.mix.returnLeft']) == true) {
  //               letmix = MAP[mix4]
  //             } else if (eval(ahuDirection['meta.section.mix.returnRight']) == true && eval(ahuDirection['meta.section.mix.returnButtom']) == true) {
  //               letmix = MAP[mix5]
  //             } else if (eval(ahuDirection['meta.section.mix.returnRight']) == true && eval(ahuDirection['meta.section.mix.returnBack']) == true) {
  //               letmix = MAP[mix6]
  //             } else if (eval(ahuDirection['meta.section.mix.returnLeft']) == true && eval(ahuDirection['meta.section.mix.returnButtom']) == true) {
  //               letmix = MAP[mix7]
  //             } else if (eval(ahuDirection['meta.section.mix.returnLeft']) == true && eval(ahuDirection['meta.section.mix.returnBack']) == true) {
  //               letmix = MAP[mix8]
  //             } else if (eval(ahuDirection['meta.section.mix.returnLeft']) == true && eval(ahuDirection['meta.section.mix.returnRight']) == true) {
  //               letmix = MAP[mix9]
  //             } else if (eval(ahuDirection['meta.section.mix.returnBack']) == true && eval(ahuDirection['meta.section.mix.returnButtom']) == true) {
  //               letmix = MAP[mix10]
  //             } else if (eval(ahuDirection['meta.section.mix.returnTop']) == true) {
  //               letmix = MAP[mix11]
  //             } else if (eval(ahuDirection['meta.section.mix.returnBack']) == true) {
  //               letmix = MAP[mix12]
  //             } else if (eval(ahuDirection['meta.section.mix.returnRight']) == true) {
  //               letmix = MAP[mix13]
  //             } else if (eval(ahuDirection['meta.section.mix.returnButtom']) == true) {
  //               letmix = MAP[mix14]
  //             } else if (eval(ahuDirection['meta.section.mix.returnLeft']) == true) {
  //               letmix = MAP[mix15]
  //             } else {
  //               letmix = MAP[mix10]
  //             }
  //           }
  //           switch (ahuDirection['meta.section.airDirection']) {
  //             case 'R':
  //               if (hasCombine === true) {
  //                 imgSet('mix12', 'mix14', 'mix18', 'mix17', 'mix7', 'mix9', 'mix25', 'mix1', 'mix4', 'mix20', 'mix10', 'mix2', 'mix4', 'mix', 'mix22')
  //               } else {
  //                 imgSet('mix12', 'mix13', 'mix15', 'mix16', 'mix6', 'mix8', 'mix24', 'mix26', 'mix5', 'mix19', 'mix10', 'mix21', 'mix5', 'mix', 'mix23')
  //               }
  //               break;
  //             default:
  //               imgSet('mix12', 'mix14', 'mix18', 'mix17', 'mix7', 'mix9', 'mix25', 'mix1', 'mix4', 'mix20', 'mix10', 'mix2', 'mix4', 'mix', 'mix22')
  //               break;
  //           }



  //           $(`[data-name-id='${id}']`).css("background-image", 'url(' + letmix + ')')
  //         // }
  //       // })
  //       break;
  //     case 'ahu.discharge':
  //       // nextProps.sections.map((k, index) => {
  //       //   if (index == nextProps.sections[index].position - 1) {
  //           // let ahuDirection = JSON.parse(nextProps.sections[index].metaJson)
  //           switch (ahuDirection['meta.section.discharge.OutletDirection']) {
  //             case 'T':
  //               letmix = MAP.mix10
  //               break;
  //             case 'A':
  //               if (ahuDirection['meta.section.airDirection'] == "R" && hasCombine === false) {
  //                 letmix = MAP.mix2
  //               } else {
  //                 letmix = MAP.mix21
  //               }
  //               break;
  //             case 'L':
  //               letmix = MAP.mix22
  //               break;
  //             case 'R':
  //               letmix = MAP.mix17
  //               break;
  //             default:
  //               letmix = MAP.mix4
  //               break;
  //           }
  //           $(`[data-name-id='${id}']`).css("background-image", 'url(' + letmix + ')')
  //         // }
  //       // })
  //       break;
  //     case 'ahu.filter':
  //       // nextProps.sections.map((k, index) => {
  //         // if (index == nextProps.sections[index].position - 1) {
  //           // let ahuDirection = JSON.parse(nextProps.sections[index].metaJson)
  //           if (ahuDirection['meta.section.airDirection'] == "R" && hasCombine === false) {
  //             $(`[data-name-id='${id}']`).css("background-image", 'url(' + MAP.comb + ')')
  //           }
  //         // }
  //       // })
  //       break;
  //     case 'ahu.combinedFilter':
  //       // nextProps.sections.map((k, index) => {
  //         // if (index == nextProps.sections[index].position - 1) {
  //           // let ahuDirection = JSON.parse(nextProps.sections[index].metaJson)
  //           if (ahuDirection['meta.section.airDirection'] == "R" && hasCombine === false) {
  //             $(`[data-name-id='${id}']`).css("background-image", 'url(' + MAP.comb2 + ')')
  //           }
  //         // }
  //       // })
  //       break;
  //     default:
  //       break;
  //   }
  // }

  render() {
    const { metaId, cName, sectionId, id, sectionL, metaJson, key, index, loc, editingAHUProps, length, sectionsNumber, changeSmall} = this.props
    const meta = JSON.parse(metaJson);
    let styleCss = metaId.substr(4);
    let isFan = metaId.substr(4) == 'fan' ? true : false
    if (meta['meta.section.fan.outlet'] == "wwk") {
      styleCss = "wwkFan";
    }
    styleCss = intl.get(`label.${styleCss}3`)

    let is39G = editingAHUProps['meta.ahu.product'] == '39G' ? true : false
    // console.log('sectionL', sectionL)
    let width = 80 * sectionL / 600
    if(changeSmall) width = 60 * sectionL / 600
    if(sectionL == 0){
      width = 20
    }
    let isLastOne = sectionsNumber-1 == index ? true : false
    let styl = {
      width: `${width}px`,
      borderRight: isLastOne ? 'none' : '1px solid darkcyan'
    }
    if (isFan && is39G) {
      let title = `${meta['meta.section.fan.fanModel']}${meta['meta.section.fan.outletDirection']}${editingAHUProps['meta.ahu.pipeorientation']}`
      return (<Tooltip title={title} placement="top">
        <div
          data-section={metaId}
          data-section-id={sectionId}
          data-name-id={`${loc}_${metaId}_${index}`}
          className={classnames(
            // style.Section,
            styleS.Section,
            // style[styleCss]
          )}
          data-toggle="tooltip"
          data-placement="top"
          title={styleCss}
          style={styl}

        >
          <p style={{marginBottom:'1px'}}>{sectionL}</p>
          <p style={{padding:'0 5px', margin:'12px 0 1px 0', textAlign: 'left'}}>{styleCss}</p>
          
        </div>
      </Tooltip>)
    }

    return (
      <div
        data-section={metaId}
        data-section-id={sectionId}
        data-name-id={`${loc}_${metaId}_${index}`}
        className={classnames(
          // style.Section,
          styleS.Section,
          // style[styleCss]
        )}
        data-toggle="tooltip"
        data-placement="top"
        title={styleCss}
        style={styl}
      >
          <p style={{marginBottom:'1px'}}>{sectionL}</p>      
        <p style={{padding:'0 5px', margin:'12px 0 1px 0', textAlign: 'left'}}>{styleCss}</p>
      </div>
    )
  }
}

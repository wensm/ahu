import {
    OK_TO_SAVE_IT,
    OK,
    CANCEL,
} from '../intl/i18n'

import intl from 'react-intl-universal'
import {connect} from 'react-redux'
import sweetalert from 'sweetalert'
import Buttons from './Buttons'
import {
    PARTITION_PARTITIONS_SAVE, savePartitions, spin
} from '../../actions/partition'
const mapStateToProps = (state, ownProps) => {
    return {
        currentPartitionPObj: state.partition.currentPartitionPObj,
        editingAHUPartitions: state.partition.editingAHUPartitions,
        currentPartitionIndex: state.partition.currentPartitionIndex,
        spinning:state.partition.spinning

    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onSavePartitoins: (currentPartitionPObj, partitions) => {
            dispatch(spin(true))
            dispatch(savePartitions(currentPartitionPObj, partitions, undefined))
            // sweetalert({
            //     title: intl.get(OK_TO_SAVE_IT),
            //     text:"",
            //     type: 'warning',
            //     showCancelButton: true,
            //     confirmButtonText: intl.get(OK),
            //     cancelButtonText: intl.get(CANCEL),
            //     closeOnConfirm: true
            // }, isConfirm => {
            //     if (isConfirm) {
            //         dispatch(savePartitions(currentPartitionPObj, partitions))
            //     }
            // })
        },
        onSpin:(bool)=>{
            dispatch(spin(bool))
        },
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Buttons)

import React from 'react'
import classnames from 'classnames'
import Section from './Section'
import style from './Partition.css'

export default class Partition extends React.Component {

    constructor(props) {
        super()
        this.partitionSelected = this.partitionSelected.bind(this)

    }

    partitionSelected(show) {
        const {
            pindex, currentPartitionIndex
        } = this.props
        if (pindex != currentPartitionIndex) {
            this.props.onPartitionSelected(pindex, show)
        }
    }

    render() {
        const {
            pindex, sections, currentPartitionIndex, loc, partition,ahuSections,ahuLayout,editingAHUProps, changeSmall = false

        } = this.props
        let show = pindex
        let length = partition.length+(partition.casingWidth || 0)
        let istooLong = Number(length) > 2800 ? true :false
        let obj = {
            layout:ahuLayout,
            sections:ahuSections
        }
        return (

            <div onClick={()=>{this.partitionSelected(show)}} id={pindex}
                 className={classnames(style.Partition, 'hvr-float-shadow', 'with-cool-menu')}>
                {(loc=="00" || loc=="01") && <div className={style.PartitionMetrics}>{partition.length+(partition.casingWidth || 0)}</div>}
                {(loc=="00" || loc=="01") && <div className={style.PartitionMetricsBottom}> &nbsp; </div>}
                <div style={{ border:istooLong ? '2px solid red':'2px solid darkcyan'}} className={classnames((currentPartitionIndex == pindex) ? style.partitionSelectedBody : style.PartitionBody) }>
                    <div className={style.PartitionNumberDiv} style={{"left":'50%'
                    //  28* sections.length + "px"
                     }}><span className={style.PartitionNumber}>{partition.pos + 1}</span></div>
                    {sections.map((section, index) => <Section  length={partition.length} sectionsNumber ={sections.length} metaId={section.metaId} sectionL={section.sectionL} {...section} key={index} index={index} loc={loc} nextProps={obj} editingAHUProps={editingAHUProps} changeSmall={changeSmall}/>)}
                </div>
                {(loc=="10" || loc=="11") && <div className={style.PartitionMetricsBottom}>&nbsp;</div>}
                {(loc=="10" || loc=="11") && <div className={style.PartitionMetrics}> {partition.length+(partition.casingWidth || 0)} </div>}
            </div>
        )
    }
}

import {
    HORIZONTAL_SPLIT,
    SPLIT_VERTICALLY,
    DELETE,
    SIDE_PANEL,
    SIDE_OPERATION_PANEL,
    TERM_PANEL,
    RIGHT_PANEL,
    TOP_PANEL,
    BOTTOM_PANEL,
    ROOTPANEL,
    LOCKED,
    MERGE,
    RESETAB
} from '../intl/i18n'
import intl from 'react-intl-universal'
import React from 'react'
import RectContainer from './RectContainer'
import style from './Panel.css'
import Rectstyle from './Rect.css'
import classnames from 'classnames'
import RectEditorContainer from './RectEditorContainer'
import {
    generatePanelRect,
    translatePanelForRender,
    searchParentPanel,
    changeSubPanelSize,
    innerSearchToDevide
} from '../../actions/partition'
import { message } from 'antd';

import im_sec_bottom_1 from '../../images/divide/im_sec_bottom_1.png'
import im_sec_bottom_2 from '../../images/divide/im_sec_bottom_2.png'
import im_sec_top_1 from '../../images/divide/im_sec_top_1.png'
import im_sec_top_2 from '../../images/divide/im_sec_top_2.png'
import im_sec_top_back_1 from '../../images/divide/im_sec_top_back_1.png'
import im_sec_top_back_2 from '../../images/divide/im_sec_top_back_2.png'
import im_sec_top_forward_1 from '../../images/divide/im_sec_top_forward_1.png'
import im_sec_top_forward_2 from '../../images/divide/im_sec_top_forward_2.png'
import im_sec_top_left_1 from '../../images/divide/im_sec_top_left_1.png'
import im_sec_top_left_2 from '../../images/divide/im_sec_top_left_2.png'
import im_sec_top_right_1 from '../../images/divide/im_sec_top_right_1.png'
import im_sec_top_right_2 from '../../images/divide/im_sec_top_right_2.png'
import im_sec_right_inside_1 from '../../images/divide/im_sec_right_inside_1.png'
import im_sec_right_inside_2 from '../../images/divide/im_sec_right_inside_2.png'
let _this = null


export default class PanelClass extends React.Component {

    constructor(props) {
        super(props)
        this.horizontalCut = this.horizontalCut.bind(this)
        this.verticalCut = this.verticalCut.bind(this)
        this.deleteSubPanel = this.deleteSubPanel.bind(this)
        this.state = {
            ShowPanelIndex: false
        }
        _this = this

    }


    horizontalCut(key, opt, selector) {
        let subPanelId = opt.$trigger.context.id
        let id = subPanelId.split('_')[0]
        let ulTop = ''

        $('.context-menu-list.context-menu-root').length > 0 && $('.context-menu-list.context-menu-root').each((index)=>{

            if($('.context-menu-list.context-menu-root')[index].style.top != '' && $('.context-menu-list.context-menu-root')[index].style.top != 'auto'){
                ulTop = $('.context-menu-list.context-menu-root')[index].style.top.replace('px', '')
            }
        })
        // let mouseDown = ''
        // $(document).mousedown(function (e) {
        //     // $("p").text("X:" + e.pageX + "   Y:" + e.pageY);
        //     console.log('eee', e, e.pageY)
        //     mouseDown = e.pageY
        // });
        ulTop = window.offY ? window.offY : ulTop
        // $('.context-menu-list.context-menu-root').css('top').replace('px', '')
        let divHeight = $(`#collapsegroup7 #${subPanelId}`).css('height').replace('px', '')
        let percent = (Number(ulTop) - Number($(`#collapsegroup7 #${subPanelId}`).offset().top)) / Number(divHeight)

        // console.log('ccc', mouseDown)
        let {editingAHUPartitions, currentPartitionIndex, product} = _this.props
        if (Number.isFinite(percent)) {
            this.props.onDevidePanel(editingAHUPartitions[currentPartitionIndex]['panels'][id], subPanelId, 1, product, percent, currentPartitionIndex)
        }else{
            this.props.onDevidePanel(editingAHUPartitions[currentPartitionIndex]['panels'][id], subPanelId, 1, product, '50%', currentPartitionIndex)
        }

    }

    verticalCut(key, opt, selector) {
        let subPanelId = opt.$trigger.context.id
        let id = subPanelId.split('_')[0]
        let ulLeft = ''
        //  $('.context-menu-list.context-menu-root').css('left').replace('px', '')
         $('.context-menu-list.context-menu-root').length > 0 && $('.context-menu-list.context-menu-root').each((index)=>{
            if($('.context-menu-list.context-menu-root')[index].style.left != '' && $('.context-menu-list.context-menu-root')[index].style.left != 'auto'){
                ulLeft = $('.context-menu-list.context-menu-root')[index].style.left.replace('px', '')
            }
        })
        // let mouseDown = ''
        // $(document).mousedown(function (e) {
        //     // $("p").text("X:" + e.pageX + "   Y:" + e.pageY);
        //     console.log('eeea', e, e.pageX)
        //     mouseDown = e.pageX
        // });
        ulLeft = window.offX ? window.offX : ulLeft
        let divLeft = $(`#collapsegroup7 #${subPanelId}`).css('width').replace('px', '')
        let percent = (Number(ulLeft) - Number($(`#collapsegroup7 #${subPanelId}`).offset().left)) / Number(divLeft)
        let {editingAHUPartitions, currentPartitionIndex, product} = _this.props
        
        if (Number.isFinite(percent)) {
            this.props.onDevidePanel(editingAHUPartitions[currentPartitionIndex]['panels'][id], subPanelId, 2, product, percent, currentPartitionIndex)
        }else{
            this.props.onDevidePanel(editingAHUPartitions[currentPartitionIndex]['panels'][id], subPanelId, 2, product, '50%', currentPartitionIndex)
        }
    }

    deleteSubPanel(key, opt, selector) {
        let subPanelId = opt.$trigger.context.id
        let id = subPanelId.split('_')[0]
        let {editingAHUPartitions, currentPartitionIndex} = _this.props
        
        this.props.onDeleteSubPanel(editingAHUPartitions[currentPartitionIndex]['panels'][id], subPanelId, currentPartitionIndex)
    }
    showParent(key, opt, selector) {
        let subPanelId = opt.$trigger.context.id
        let id = subPanelId.split('_')[0]
        let subPanelIdSplit = subPanelId.split('_')
        //        console.log('id', subPanelId, _this)
        if (subPanelIdSplit.length == 1) {
            $(`#collapsegroup7 #${subPanelIdSplit}`).css('border', '2px solid red')
            this._changeBorder = setTimeout(() => {
                $(`#collapsegroup7 #${subPanelIdSplit}`).css('border', '1px solid #000')
            }, 2000)

        } else if (subPanelIdSplit.length > 1) {
            subPanelIdSplit.pop()
            let arr = subPanelIdSplit.join('_')
            $(`#collapsegroup7 #${arr}`).css('border', '2px solid red')
            this._changeBorder = setTimeout(() => {
                $(`#collapsegroup7 #${arr}`).css('border', '1px solid #000')
            }, 2000)
        }
        // this.props.onDeleteSubPanel(_this.props.editingAHUPartitions[_this.props.currentPartitionIndex]['panels'][id], subPanelId)
    }
    findSubPanle(id, root, obj) {
        // console.log('digui', id, root, obj)
        root && root.forEach((sub) => {
            if (sub.id == id) {
                obj.thisP = sub
            } else {
                _this.findSubPanle(id, sub.subPanels, obj)
            }
        })
    }
    locking(key, opt, selector) {
        let subPanelId = opt.$trigger.context.id
        let id = subPanelId.split('_')[0]
        // let subPanelIdSplit = subPanelId.split('_')
        let subs = _this.props.editingAHUPartitions[_this.props.currentPartitionIndex]['panels'][id]['subPanels']
        let thisPanel = {
            thisP: ''
        }
        _this.findSubPanle(subPanelId, subs, thisPanel)
        this.props.onLocking(thisPanel.thisP)

    }
    merge(key, opt, selector) {
        let subPanelId = opt.$trigger.context.id
        let id = subPanelId.split('_')[0]
        // let subPanelIdSplit = subPanelId.split('_')
        let {editingAHUPartitions, currentPartitionIndex, product, isLockedPanel} = _this.props
        
        let subs = editingAHUPartitions[currentPartitionIndex]['panels'][id]['subPanels']
        let thisPanel = {
            thisP: ''
        }
        _this.findSubPanle(subPanelId, subs, thisPanel)

        // subs && subs.forEach((sub) => {
        //     if (sub.id == subPanelId) thisPanel = sub
        // })
//        console.log('isLockedPanel', isLockedPanel, thisPanel.thisP)
        // if (isLockedPanel == '') {
        //     message.error('请先锁定要合并的第一个面');
        // } else if (isLockedPanel != '' && thisPanel.thisP && (thisPanel.thisP.panelWidth != isLockedPanel.panelWidth)) {
        //     message.error('非法合并');

        // } else if (isLockedPanel != '' && thisPanel.thisP && (thisPanel.thisP.id.substr(0, 1) != isLockedPanel.id.substr(0, 1))) {
        //     message.error('非同一个面');
        // } else {
            // console.log('aaaa', product)
            this.props.onMerge(isLockedPanel.id, thisPanel.thisP.id, editingAHUPartitions[currentPartitionIndex]['panels'][id], product, currentPartitionIndex)
        // }
        // console.log('_this', thisPanel, isLockedPanel)
    }

    componentWillReceiveProps() {
        $(this.refs.panelParams).on('mouseenter', (e) => {
            $(this.refs.panelParams2).css('opacity', '1')

        })
        $(this.refs.panelParams).on('mouseleave', (e) => {
            $(this.refs.panelParams2).css('opacity', '0')
            // $(this.refs.panelParams).removeClass(classnames(Rectstyle.HasGap))
        })
    }
    componentDidMount() {
        $(document).mousedown(function (e) {
            // $("p").text("X:" + e.pageX + "   Y:" + e.pageY);
            if(e.which == 3){
                window.offX = e.pageX
                window.offY = e.pageY
            }
        });

        $.contextMenu({
            // define which elements trigger this menu
            selector: 'div[data-rect]',
            // define the elements of the menu
            items: {
                item1: {
                    name: intl.get(HORIZONTAL_SPLIT), callback: (key, opt, selector) => this.horizontalCut(key, opt, selector)
                },
                item2: {
                    name: intl.get(SPLIT_VERTICALLY), callback: (key, opt, selector) => this.verticalCut(key, opt, selector)
                },
                item3: {
                    name: intl.get(DELETE), callback: (key, opt, selector) => this.deleteSubPanel(key, opt, selector)
                },
                item4: {
                    name: intl.get(ROOTPANEL), callback: (key, opt, selector) => this.showParent(key, opt, selector)
                },
               item5: {
                   name: intl.get(LOCKED), callback: (key, opt, selector) => this.locking(key, opt, selector)
               },
               item6: {
                   name: intl.get(MERGE), callback: (key, opt, selector) => this.merge(key, opt, selector)
               }

            },
            events: {
                contextMenu: (e) => {
                    if (e.$trigger.attr("data-divide")) {
                        return false
                    }
                    return true
                },
                show: (e) => {
                    if (e.$trigger.attr("data-divide")) {
                        return
                    }
                    // 手动触发一下面板选择
                    e.$trigger.trigger("click")
                    //为以后扩展面板多切（已切割的可以再选择）做准备
                    //现在样式屏蔽
                    $(".panel-area").addClass(classnames(Rectstyle.HasGap))
                },
                hide: () => {
                    $(".panel-area").removeClass(classnames(Rectstyle.HasGap))
                }
            }
            // there's more, have a look at the demos and docs...
        })

        $('.panel-area').on('mousemove', '[data-rect]', (e) => {
            if (e.currentTarget == e.target) {
                $(e.target).addClass("hover")

            }
        });
        $(this.refs.panel).click((e) => {
            if (e.target == this.refs.panel) {
                let panel = this.props.currentPartitions[this.props.currentPartitionIndex].panels[this.props.panelPos]
                this.props.onToggleSelectPanel(panel)
            }
        })


        $(this.refs.panel).on('mousemove', (e) => {
            $(this.refs.panel).addClass(classnames(Rectstyle.HasGap))
        })
        $(this.refs.panel).on('mouseleave', (e) => {
            $(this.refs.panel).removeClass(classnames(Rectstyle.HasGap))
        })
        $('.panel-area').on('mouseout', '[data-rect]', (e) => {
            if (e.currentTarget == e.target) {
                // e.preventDefault()
                // e.stopPropagation()
                $(e.target).removeClass("hover")
            }
        })


    }


    getSubPanelDescList(panel) {
        let list = []
        if (panel.subPanels) {
            panel.subPanels.forEach((sp) => {
                list = list.concat(this.getSubPanelDescList(sp))
            })
        } else {
            list.push({ id: panel.subPanelIndex, size: panel.panelWidth + "*" + panel.panelLength, panelConnector: panel.panelConnector }, )
        }
        return list;
    }
    getSectionName(thisPartion, fromMouseToLeft) {
        let currentLength = Number(fromMouseToLeft) * (thisPartion.length)
        let allLength = 0
        let name = ''
        let bool = false
        thisPartion.sections.forEach((section) => {
            let beforeLength = allLength
            allLength = allLength + section.sectionL
            // console.log('aaaa', allLength)
            if (!bool && currentLength < allLength && beforeLength < currentLength) {
                bool = true;
                name = section.metaId

            }
        })
        // console.log('thisPartion, fromMouseToLeft', name)
        return name
    }


    render() {
        const {
            currentSubPanel, currentParentPanel, ratio, panelPos, currentPartitions, currentPartitionIndex, currentPanel, product, fromMouseToLeft, ahuSections, panelSeries, panelIndex
        } = this.props
        let panel = currentPartitions[currentPartitionIndex].panels[panelPos]
        let panellabels = {
            0: intl.get(TOP_PANEL),//顶面板
            1: intl.get(BOTTOM_PANEL),//底面板
            2: intl.get(SIDE_PANEL),//非操作面
            3: intl.get(SIDE_OPERATION_PANEL),//操作面
            4: intl.get(TERM_PANEL),//端面板
            5: intl.get(RIGHT_PANEL)//右面板
        }


        let panelDescList = this.getSubPanelDescList(panel)
        // console.log('panelDescList',panel, panelDescList)
        // panelDescList.sort((a, b) => {
        //         return b.id - a.id
        //     })

        let legendImage = [
            [im_sec_top_1, im_sec_top_2],
            [im_sec_bottom_1, im_sec_bottom_2],
            [im_sec_top_back_1, im_sec_top_back_2],
            [im_sec_top_forward_1, im_sec_top_forward_2],
            [im_sec_top_left_1, im_sec_top_left_2],
            [im_sec_top_right_1, im_sec_top_right_2],
            [im_sec_right_inside_1, im_sec_right_inside_2]
        ]

        // let rects = translatePanelForRender(panel, ratio)

        let mRect = generatePanelRect(panel, null, ratio)
        let hasCurrentEditPanel = currentSubPanel && currentSubPanel.id && innerSearchToDevide(panel, currentSubPanel.id)
        let img = legendImage[panelPos] && legendImage[panelPos][0]
        if (panelPos >= 6) {
            img = legendImage[6][0]
        }
        let isEditing = this.props.currentPanel && panel && (this.props.currentPanel.id == panel.id)
        // let currentSerial = this.props.editingAHUProps ? this.props.editingAHUProps['meta.ahu.serial'] : ''

        // let currentHeight = currentSerial ? currentSerial.substring(currentSerial.length - 4) : ''
        // currentHeight = (panelPos == 0 || panelPos == 1) ? currentHeight.substring(2, 4) : currentHeight.substring(0, 2)

        let currentHeight = panelSeries ? panelSeries.substring(panelSeries.length - 4) : ''
        currentHeight = (panelPos == 0 || panelPos == 1) ? currentHeight.substring(2, 4) : currentHeight.substring(0, 2)
        

        let thisPartion = this.props.editingAHUPartitions[currentPartitionIndex]
        let thisSection = this.getSectionName(thisPartion, fromMouseToLeft)
        let thisSectionName = thisSection ? intl.get(`label.${thisSection.split('.')[1]}3`) : ''
        let isDuanmian = Number(this.props.currentPanelIndex) <= 3 ? false : true
        // console.log('thissss', this.props.currentPanelIndex)
        // console.log('thisSection', thisSection, thisSectionName)
        /*1366 * 768*/
//        console.log('isEditing', isEditing, this.props.currentPanel , panel, currentPartitions[currentPartitionIndex])
        // console.log('product', fromMouseToLeft,thisPartion, thisSection, isEditing,this.props.editingAHUPartitions,currentPartitionIndex)
//        console.log('this', panel.panelLength)
        let showSectionL = panel ? panel.panelLength : 0//底面板显示段长
        return (
            <div className={classnames(style.PanelRow, "row", isEditing ? style.PanelEditing : "")} ref="panelParams">
                {/*<div style={{paddingLeft: '50px'}}><h4>{panellabels[panelPos]}</h4></div>*/}
                <div className="col-lg-2" style={{ position: 'absolute',fontWeight:'bold' }}>
                    {Number(panelPos) + 1} {isEditing && !isDuanmian ? `-- ${thisSectionName}` : ''}
                </div>
                <div className={classnames(style.PanelLegend, "col-lg-3 col-lg-offset-1")} title={panellabels[panelPos]} style={{ padding: 0 }}
                    ref={(el) => {
                        $(el).click((e) => {
                            this.props.onToggleSelectPanel(panel)
                        })
                        $(el).css("background-image", "url(" + img + ")")
                    }}
                >
                </div>
                <div className={classnames(style.PanelArea, "col-lg-8", "panel-area")}>
                    <div className={classnames(style.rects, "col-lg-6", hasCurrentEditPanel ? "has-gap" : "")}
                        ref="panel">

                        <div style={{
                            padding: '20px 10px 0 0'
                            // transform: 'rotate(-90deg)',webkitTransform: 'rotate(-90deg)'
                        }}>{currentHeight}M</div>
                        <RectContainer
                            product={product}
                            rect={mRect}
                            direction={panel.direction}
                            selectedSubPanel={currentSubPanel}
                        />
                        {panelPos == 1 ? <div style={{
                            padding: '0',
                            margin:'-20px 0 0 0',
                            position:'absolute',
                            top:'0',
                            left:'130px'
                            // transform: 'rotate(-90deg)',webkitTransform: 'rotate(-90deg)'
                        }}><span>{ showSectionL}M</span></div>:''}
                    </div>
                    
                    <div className={"col-lg-1"} />
                    {isEditing && <div className={"col-lg-5"} ref="panelParams2" style={{padding:0,}}>

                        {isEditing && <RectEditorContainer panel={currentSubPanel} panelPos={panelPos} product={product}
                            parentPanel={currentParentPanel} />}

                    </div>}
                    {!isEditing && <div className={"col-lg-6"}>
                        {!isEditing && <ul className={classnames(style.PanelDescList)}>
                            {panelDescList.map((des) => {
                                // console.log('li', des)
                            return <li key={des.id}><span
                            className={classnames(style.PanelIndexTip)} style={{ fontSize: '21px' }}>{des.id}</span> {des.size}{des.panelConnector ? `(${des.panelConnector})` : ''}</li>})}
                        </ul>}
                    </div>}

                </div>
            </div>
        )
    }
}
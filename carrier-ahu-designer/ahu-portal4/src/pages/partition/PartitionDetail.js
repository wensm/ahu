import {
    PANEL_EDIT,
    EDIT,
    CONFIRM,
    CASING_PARTS_LIST,
    Partition,
    RESETAB,
    BACK,
    WHETHER_OR_NOT_TO_SAVE,
    OK
} from '../intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import classnames from 'classnames'
import PanelContainer from './PanelContainer'
import Group from '../ahu/Group'
import style from './Partition.css'
import panelStyle from './Panel.css'
import PartitionSummary from "./PartitionSummaryContainer"
import PanelLegend from "./PanelLegend"
import sweetalert from 'sweetalert'
import sImg from '../../images/sImg.png';
import rImg from '../../images/rImg.png';
import { Spin } from 'antd'
window.intl = intl

export default class PartitionDetail extends React.Component {

    constructor(props) {
        super()
        this.selectPanel = this.selectPanel.bind(this)
        this.confirm = this.confirm.bind(this)
        this.syncAB = this.syncAB.bind(this)
        this.goBack = this.goBack.bind(this)
        this.showPanelIndex = this.showPanelIndex.bind(this)
        this.saveCapture = this.saveCapture.bind(this)

        this.state = {
            ShowPanelIndex: false,
            enlarge: false
        }
    }
    componentDidMount() {
        this.props.onSpin(false)



    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.threeView != '' && !$('#group111').attr('hasToBig')) {
            setTimeout(() => {
                $('#group111').mousewheel(function (event, delta) {
                    let dir = delta > 0 ? 'Up' : 'Down';
                    let widthh = $('#group111').width() * 1.02
                    let widthhh = $('#group111').width() * 0.98
                    $('#group111').css('width', widthh)
                    $('#group111').css('height', widthh)
                    if (dir == 'Up') {
                        $('#group111').css('width', widthh)
                        $('#group111').css('height', widthh)
                    } else {
                        $('#group111').css('width', widthhh)
                        $('#group111').css('height', widthhh)
                    }
                    return false;
                });
                $('#group111').attr('hasToBig', true)
            }, 300)
        }
    }

    selectPanel(face) {
        this.props.onFaceChanged(this.props.currentPartitions, this.props.currentPartitionIndex, face)
        // console.log("Selected face: " + face)
    }

    confirm() {
        this.props.onDoConfirm()
        // console.log("save ...")
        this.saveCapture()
    }
    syncAB() {
        //    console.log('this', this.props.currentPartitions[this.props.currentPartitionIndex])
        this.props.onSyncAB(this.props.currentPartitions[this.props.currentPartitionIndex])
    }
    goBack() {
        //    console.log('this', this.props.currentPartitions[this.props.currentPartitionIndex])
        // this.props.onSyncAB(this.props.currentPartitions[this.props.currentPartitionIndex])
        let arr = ''
        for(let key in this.props.checkImg){
            if(this.props.checkImg[key] != 'true'){
                arr = arr + '分段' + key + ','
            }
        }
        if(arr.length > 0){
            sweetalert({
                // title: intl.get(WHETHER_OR_NOT_TO_SAVE),
                title: `${arr} 尚未保存，是否仍要返回`,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: intl.get(OK),
                closeOnConfirm: true
            }, isConfirm => {
                if (isConfirm) {
                    this.props.onGoBack()
                    
                }
                // callBack()
                // window.location.href = "/";
            })
        }else{
            this.props.onGoBack()
        }

    }
    showPanelIndex() {
        //this.props.onShowPanelIndex()
        this.setState({
            ShowPanelIndex: !this.state.ShowPanelIndex
        });

        if (this.state.ShowPanelIndex) {
            this.props.onShowPanelIndex()
        }
    }

    saveCapture() {

        var cntElem = $("#group77")[0];

        var shareContent = cntElem;//需要截图的包裹的（原生的）DOM 对象
        var width = shareContent.offsetWidth; //获取dom 宽度
        var height = shareContent.offsetHeight; //获取dom 高度
        var canvas = document.createElement("canvas"); //创建一个canvas节点
        var scale = 1; //定义任意放大倍数 支持小数
        canvas.width = width * scale; //定义canvas 宽度 * 缩放
        canvas.height = height * scale; //定义canvas高度 *缩放
        canvas.getContext("2d").scale(scale, scale); //获取context,设置scale
        var opts = {
            scale: scale, // 添加的scale 参数
            canvas: canvas, //自定义 canvas
            // logging: true, //日志开关，便于查看html2canvas的内部执行流程
            width: width, //dom 原始宽度
            height: height,
            useCORS: true // 【重要】开启跨域配置
        };


        html2canvas(cntElem, opts).then(canvas => {
            var context = canvas.getContext('2d');
            // 【重要】关闭抗锯齿
            context.mozImageSmoothingEnabled = false;
            context.webkitImageSmoothingEnabled = false;
            context.msImageSmoothingEnabled = false;
            context.imageSmoothingEnabled = false;

            let src = canvas.toDataURL()
            const {params, onPartitionImageSave, currentPartitionPObj, currentPartitions, currentPartitionIndex, onChangeCheck} = this.props
            onPartitionImageSave(currentPartitionPObj, src, Number(currentPartitions[currentPartitionIndex].pos) + 1, () => { this.onSavePartition(Number(currentPartitions[currentPartitionIndex].pos) + 1); onChangeCheck(Number(currentPartitions[currentPartitionIndex].pos), 'true') })
            // console.log(src)
            // $("#info_capture").attr("src", src)
            // $("#captureModal").modal("show")
             //document.body.appendChild(canvas)
            // document.body.appendChild($("<img src="+canvas.toDataURL()+"/>"))
        });

    }
    onSavePartition(num) {
        const {
            currentPartitionPObj,
            currentPartitions
        } = this.props
        currentPartitionPObj.saveFrom = this.props.route.path.split('/')[0]
        this.props.onSavePartitoins(currentPartitionPObj, currentPartitions, num)
    }
    getArr(obj) {
        let arr = []
        for (let key in obj) {
            arr.push(obj[key])
        }
        return arr
    }

    render() {
        const {
            currentPartitions, currentPartitionIndex, currentSubPanel, currentParentPanel, parentPanel, currentPanel, user, params, spinning, editingAHUProps, threeView, panelSeries
        } = this.props

        let ratio = 2

        //设定显示需要显示的面板
        let panelIndexArray = []

        panelIndexArray.push(0)
        panelIndexArray.push(1)
        // if (this.props.currentPartitionIndex == 0) {
        // }
        panelIndexArray.push(2)
        // if (this.props.currentPartitions.length - 1 == this.props.currentPartitionIndex) {
        panelIndexArray.push(3)
        panelIndexArray.push(4)
        panelIndexArray.push(5)
        // }
        let panelArr = []
        let currentPartitionsCurrentPartitionIndex = currentPartitions[currentPartitionIndex]
        if (currentPartitionsCurrentPartitionIndex && currentPartitionsCurrentPartitionIndex['panels']) {
            panelArr = this.getArr(currentPartitionsCurrentPartitionIndex['panels'])
        }
        //        console.log('panelArr', panelArr)
        let arr = []
        currentPartitionsCurrentPartitionIndex && currentPartitionsCurrentPartitionIndex.sections.forEach((ele, index)=>{
            
            arr.push(JSON.parse(ele.metaJson)['meta.section.airDirection'])
        })
        let boolDir = 'both'
        let isS = arr.every((ele)=>{
            return ele === 'S'
        })
        let isR = arr.every((ele)=>{
            return ele === 'R'
        })
        if(isS){
            boolDir = 'S'
        }else if(isR){
            boolDir = 'R'
            
        }
        // console.log('currentPartitionsCurrentPartitionIndex', boolDir, currentPartitionsCurrentPartitionIndex && currentPartitionsCurrentPartitionIndex.sections)
        // let backgroundImageUrl = `url('/files/output/cad/${params ? params.projectId : ""}/${params ? params.ahuId : ""}/${user.userId}.bmp')`
        // let backgroundImageUrl = `url(${threeView})`
        let backgroundImageUrl = threeView ? "url(' " + threeView + " ')" : ''
        //        console.log('this2',this.props.editingAHUProps['meta.ahu.product'])

        let syncABDisabled = false
        if (panelSeries && panelSeries.indexOf('CQ') == -1) {//非CQ禁用重置AB
            syncABDisabled = true
        }

        return (
            <div>
                <Spin spinning={spinning} size="large">
                    <div style={{ textAlign: 'center', margin: '10px 0' }}>
                        <button className="btn btn-primary"
                            style={{ marginRight: 10 }}
                            onClick={this.confirm}>{intl.get(CONFIRM)}</button>
                        <button className="btn btn-primary"
                            style={{ marginRight: 10 }}
                            onClick={this.showPanelIndex}>{intl.get(this.state.ShowPanelIndex ? "title.moon_intl_str_0801" : "title.moon_intl_str_0802")}</button>
                        <button className="btn btn-primary"
                                style={{ marginRight: 10 }}
                                disabled={syncABDisabled} onClick={this.syncAB}>{intl.get(RESETAB)}</button>
                        <button type="button" className="btn btn-primary"
                            onClick={this.goBack}>{intl.get(BACK)}
                        </button>
                    </div>
                </Spin>
                <Group title={intl.get(PANEL_EDIT)} id="group7">
                    <div style={{ marginTop: '20px' }} id="panel_detail">

                        <div className="col-lg-12">
                            <div className="row">
                                <div className="col-lg-10" id="group77">
                                    <div className="row">

                                        <div className="col-lg-8" style={{ height: 'inherit', padding: 0 }}>
                                            <div className="panel panel-default">
                                                <div className="panel-heading">
                                                    <h3 className="panel-title">AirFlow
                                                        {boolDir === 'S' ? <img src={sImg} style={{ width: '20px', marginLeft:'10px'}}/> : ''}
                                                        {boolDir === 'S' ? <img src={sImg} style={{ width: '20px', marginLeft:'10px'}}/> : ''}
                                                        {boolDir === 'R' ? <img src={rImg} style={{ width: '20px', marginLeft:'10px'}}/> : ''}
                                                        {boolDir === 'R' ? <img src={rImg} style={{ width: '20px', marginLeft:'10px'}}/> : ''}
                                                        {boolDir === 'both' ? <img src={rImg} style={{ width: '20px', marginLeft:'10px'}}/> : ''}
                                                        {boolDir === 'both' ? <img src={sImg} style={{ width: '20px', marginLeft:'10px'}}/> : ''}
                                                    </h3>
                                                    <h3 className="panel-title">{this.props.caption}
                                                    </h3>
                                                </div>
                                                <div
                                                    className={classnames((this.state.ShowPanelIndex ? panelStyle.ShowPanelIndex : ""), "panel-body")}
                                                    style={{ padding: 0 }}>
                                                    {panelArr && panelArr.length > 0 ? <div className="tab-content">
                                                        {panelArr.map((panelPos, index) => {
                                                            return <PanelContainer
                                                                // {...this.props}
                                                                product={editingAHUProps['meta.ahu.product']}
                                                                key={`${currentPartitionIndex}${panelPos.id}`}
                                                                panelPos={panelPos.id}
                                                                // panelPos={index}
                                                                ratio={ratio}
                                                                currentPartitions={currentPartitions}
                                                                panelSeries={panelSeries}
                                                            // currentPanel={currentPanel}
                                                                // currentPartitionIndexfromProps={currentPartitionIndex}
                                                            />
                                                        })}

                                                    </div> : ''}
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-lg-4 " style={{ color: 'black', height: 'inherit' }}>
                                            <div className="panel panel-default">
                                                <div className="panel-heading">
                                                    <h3 className="panel-title">{intl.get(CASING_PARTS_LIST)}--{intl.get(Partition)}{`${Number(currentPartitionsCurrentPartitionIndex ? currentPartitionsCurrentPartitionIndex.pos : 0) + 1}`}
                                                    </h3>
                                                </div>
                                                <div className="panel-body">
                                                    {panelArr && panelArr.length > 0 &&
                                                        <PartitionSummary product={editingAHUProps['meta.ahu.product']} />}
                                                </div>
                                            </div>
                                            <PanelLegend product={this.props.editingAHUProps['meta.ahu.product']} />

                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-2" >
                                    {backgroundImageUrl && <div style={{
                                        width: 'inherit',
                                        height: '300px',
                                        backgroundImage: backgroundImageUrl,
                                        backgroundSize: '100% 100%',
                                        // backgroundColor: 'red',
                                        position: 'fixed',
                                        right: 0,
                                        top: '20%'
                                    }}
                                        id="group111"
                                        onClick={() => {
                                            let widthh = $('#group111').width() * 2.5
                                            let widthhh = $('#group111').width() / 2.5
                                            if (!this.state.enlarge) {

                                                $('#group111').css('width', widthh)
                                                $('#group111').css('height', widthh)
                                            } else {

                                                $('#group111').css('width', widthhh)
                                                $('#group111').css('height', widthhh)
                                            }
                                            this.setState({
                                                enlarge: !this.state.enlarge
                                            })
                                        }}
                                    >

                                    </div>}

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="modal fade" id="captureModal" tabIndex="-1" role="dialog"
                        aria-labelledby="coolingCoilwModalLabel">
                        <div className="modal-dialog modal-lg" role="document">
                            <img id="info_capture" src="" alt="" style={{ width: "100%" }} />
                        </div>
                    </div>
                </Group>
            </div>

        )
    }
}

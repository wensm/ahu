import {connect} from 'react-redux'
import PartitionSummary from './PartitionSummary'
import {isEmpty} from 'lodash'
import {
    changeBase, changeCheckImg
} from '../../actions/partition'


const mapStateToProps = (state, ownProps) => {
    let partition = state.partition.editingAHUPartitions[state.partition.currentPartitionIndex];
    let summaryJson = state.partition.currentPartitionPObj.summaryJson;
    function getSummaryInfoFromPanel(panel, obj, arr, bool=false) {
        // console.log('11', panel)
        let panels={}
        let dividers ={}
        if(panel.subPanels && panel.subPanels.length>0){
            let d_key = [(panel.frameLine && panel.frameLine.type)||0].join("_");
            let d_key1 = panel.direction === 2 ?panel.panelWidth:panel.panelLength
            let p_key2 = panel.id.split('_')[0]
            
            if(!dividers[d_key]){
                dividers[d_key] = {}
            }

            if (!dividers[d_key][d_key1]){
                dividers[d_key][d_key1] = []
            }
            // console.log('current pa', panel)
            dividers[d_key][d_key1].push(panel)
            arr.push(panel)


            panel.subPanels.forEach(function (subPanel) {

                
                // console.log('subPanel', subPanel)

                let res = getSummaryInfoFromPanel(subPanel, obj, arr, false)//zzf daixiugai
        // console.log('22', subPanel)
                
                // console.log('res', res)
                if (res && res.panels){
                    for (let p_key in res.panels){
                        if(!panels[p_key]){
                            panels[p_key] = {}
                        }
                        for(let p_key1 in res.panels[p_key]){
                            if (!panels[p_key][p_key1]){
                                panels[p_key][p_key1] = []
                            }
                            panels[p_key][p_key1].push(res.panels[p_key][p_key1])
                        }
                    }
                }
                // if (res && !isEmpty(res.dividers)){
                //     console.log('res2', res)
                //     for(let d_key in res.dividers){
                //         if(!dividers[d_key]){
                //             dividers[d_key] = {}
                //         }
                //         for(let d_key1 in res.dividers[d_key]) {
                //             if (!dividers[d_key][d_key1]) {
                //                 dividers[d_key][d_key1] = []
                //             }
                //             dividers[d_key][d_key1].push(panel)
                //         }
                //     }
                // }
                // console.log('zzf chushihua', subPanel, panels)
            });

        }else{
            // console.log("find panel", panel)
            let p_key = (panel.panelType||0)//组名称 ，对应普通面板，外面板等
            let _id = panel.id.split('_')[0]
            let p_key1 = [panel.panelLength,panel.panelWidth, panel.panelConnector || '--', _id].join("_")//说明列
            let p_key2 = panel.id.split('_')[0]//面名称，顶面，侧面等

            if(!panels[p_key]){//zzf 这里多加一层用来区分哪个面，比如顶面 底面  端面等,该层可以为p_key2
                panels[p_key] = {[p_key2]:{}}
            }
            
            if (!panels[p_key][p_key2][p_key1]){
                panels[p_key][p_key2][p_key1] = []
            }

            panels[p_key][p_key2][p_key1].push(panel)

        }

        return {
            panels:panels,
            dividers:arr
        }
    }

    function getSummaryInfoFromPartition(partition){
        let panels={}
        let dividers ={}
        let obj = {}
        for(let pNo in partition.panels){
            let panel = partition.panels[pNo];
            if(!panel){
                continue;
            }
            let arr = []
            let res = getSummaryInfoFromPanel(panel, obj, arr, true)
            res.dividers.length>0 && res.dividers.forEach((element)=>{
                // let d_key = [(element.frameLine && element.frameLine.type)||0].join("_");
                let d_key = `${(element.frameLine && element.frameLine.type)?element.frameLine.type:0}`
                let d_key1 = element.direction === 2 ?element.panelWidth:element.panelLength
                let d_key2 = element.id.split('_')[0]//面名称，顶面，侧面等
                let key = `${d_key}_${d_key1}_${(element.frameLine && element.frameLine.lineConnector)?element.frameLine.lineConnector:0}`

                if(!obj[d_key]){
                    obj[d_key] = {}
                }
                if(!obj[d_key][d_key2]){
                    obj[d_key][d_key2] = {}
                }
                if(!obj[d_key][d_key2][key]){
                    obj[d_key][d_key2][key] = []
                }
                obj[d_key][d_key2][key].push(element)
                
            })
           

            if (res && res.panels){
                for (let p_key in res.panels){
                    if(!panels[p_key]){
                        panels[p_key] = {}
                    }

                    for(let p_key1 in res.panels[p_key]){
                        if (!panels[p_key][p_key1]){
                            panels[p_key][p_key1] = []
                        }
                        panels[p_key][p_key1].push(res.panels[p_key][p_key1])
                    }
                }
            }
            // if (res && res.dividers){
            // console.log('zzfres', res)
                
                // for(let d_key in res.dividers){
                //     if(!dividers[d_key]){
                //         dividers[d_key] = {}
                //     }
                //     for(let d_key1 in res.dividers[d_key]) {
                //         if (!dividers[d_key][d_key1]) {
                //             dividers[d_key][d_key1] = []
                //         }
                //         dividers[d_key][d_key1].push(panel)
                //     }
                // }
            // }
        }
        // console.log('obj', obj)
        return {
            panels:panels,
            summaryPanel:JSON.parse(summaryJson),
            dividers:obj
        }

    }

    let summaries = getSummaryInfoFromPartition(partition)

    // console.log(summaries)

    return {
        currentPartition:partition,
        summaries:summaries,

    }
}

const mapDispatchToProps = dispatch => {
    return {
        onChangeBase:(value, pos)=>{
            dispatch(changeCheckImg(Number(pos)+1))

            dispatch(changeBase(value, pos))
        }
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(PartitionSummary)

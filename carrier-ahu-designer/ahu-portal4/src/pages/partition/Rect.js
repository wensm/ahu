import React from 'react'
import style from './Rect.css'
import panelStyle from './Panel.css'
import classnames from 'classnames'


import RectContainer from './RectContainer'

import {
    generatePanelRect
} from '../../actions/partition'

import {
    PANEL_KEYS,
    PANEL_TYPES,
    PANEL_NAMES,
    PANEL_BGS,

    LINE_KEYS,
    LINE_TYPES,
    LINE_NAMES,
    LINE_BGS,
    PANEL_BGS_G,
    PANEL_BGS_XT
} from './DivideConstants'
import { Icon, Tooltip } from 'antd'

export default class Rect extends React.Component {

    constructor(props) {
        super(props)
    }

    rectSelected(e, clickMouseId) {
        e.preventDefault();
        e.stopPropagation()

        // if (!this.props.rect.panel.subPanels) {
        // console.log('this.props.rect.panel, this.props.currentPanel', this.props.rect.panel, this.props.currentPanel)
        let parentId = clickMouseId ? clickMouseId.split('_')[0] : ''
        let fromMouseToLeft = 0
        // console.log('e',clickMouseId)
        if (parentId != '') {
            fromMouseToLeft = (Number(e.pageX) - Math.round($(`#collapsegroup7 #${parentId}`).offset().left)) / 216
            console.log('fromMouseToLeft',Number(e.pageX), Math.round($(`#collapsegroup7 #${parentId}`).offset().left))
        }
        this.props.onRectSelected(this.props.rect.panel, this.props.currentPanel, fromMouseToLeft.toFixed(2))
        // }

    }

    getPanelTypeName(type) {
        let name = PANEL_NAMES.PUTONG;
        if (PANEL_KEYS[type]) {
            name = PANEL_NAMES[PANEL_KEYS[type]]
        }
        return name;
    }

    getDivideTypeBg(rect) {
        if (rect.panel.frameLine == null) {
            rect.panel.frameLine = {
                type: 0
            }
        }
        if (rect.panel.frameLine && rect.panel.subPanels) {
            if (rect.panel.direction == 2) {
                return (LINE_BGS[LINE_KEYS[rect.panel.frameLine.type]] || [])[0]
            } else if (rect.panel.direction == 1) {
                return (LINE_BGS[LINE_KEYS[rect.panel.frameLine.type]] || [])[1]
            } else {
                rect.panel.direction = 0;
                return (LINE_BGS[LINE_KEYS[rect.panel.frameLine.type]] || [])[1]
            }

        }

    }


    getPanelTypeBg(rect, product) {
        let panelType = rect.panel.panelType == null ? 0 : rect.panel.panelType
        if (product == '39G') {

            return PANEL_BGS_G[PANEL_KEYS[panelType]];

        } else if (product == '39XT') {

            return PANEL_BGS_XT[PANEL_KEYS[panelType]];

        } else {

            return PANEL_BGS[PANEL_KEYS[panelType]];
        }

    }


    initDrag(el, direction) {
        // $(el).draggable({
        //     axis: direction === 1 ?"x":"y"
        // })
    }

    render() {
        let {
            rect, selectedSubPanel, product, isLockedPanel
        } = this.props
        let cssObj = { height: rect.height, width: rect.width, overflow:'hidden'}


        let directionClass = rect.panel.direction === 1 ? style.RectVerticalContainer : style.RectHorizonContainer;
        let hasSub = rect.panel.subPanels && rect.panel.subPanels.length === 2;
        let divideStyle = {
            backgroundSize: '90% 90%'
        }
        if (hasSub) {
            let firstRect = generatePanelRect(rect.panel.subPanels[0], rect.panel)
            if (rect.panel.direction === 1) {
                divideStyle.top = firstRect.height
            } else {
                divideStyle.left = firstRect.width
            }
        }
        let bg_panel = !hasSub && this.getPanelTypeBg(rect, product);
        let bg_divide = this.getDivideTypeBg(rect);
        let is_top = !this.props.parentPanel
        let name = rect.panel.panelConnector
        let splitl = '', splitR = ''
        if (name) {

            splitl = name.split('')[0]
            splitR = name.split('')[1]
            if (name.split('').length === 3 && name.split('')[2] == 'F') {
                splitR = `${name.split('')[1]}${name.split('')[2]}`
                if (name.split('')[1] == '0') {
                    splitR = `${name.split('')[2]}`
                }
            }
            if (name.split('').length === 3 && name.split('')[1] == 'F') {
                splitR = name.split('')[2]
                splitl = `${name.split('')[0]}${name.split('')[1]}`
                if (name.split('')[0] == '0') {
                    splitl = `${name.split('')[1]}`
                }
            }
        }
        let memo = rect.panel.memo
        // console.log('this', this.props)
        let isLockPanel = isLockedPanel && isLockedPanel.id == this.props.rect.id ? true :false 
        return (
            <div id={rect.id}
                key={this.props.key}
                data-w={rect.width}
                data-h={rect.height}
                ref={(el) => {
                    if (el && !hasSub && bg_panel) {
                        // let p = Math.min($(el).height(), $(el).width()) / Math.max($(el).height(), $(el).width()) * 100
                        let p = $(el).height() / $(el).width() * 100
                        // console.log("getBg size[" + rect.panel.id + "]:" + p);
                        $(el).css({

                            "background-image": "url(" + bg_panel + ")",
                            // "background-size": p + "%",
                            // "background-size": ($(el).height() - $(el).width()) > 0 ? "100%" : p + "%",
                            "background-repeat": "no-repeat",
                            "background-size": "100% 100%",
                        })
                        // console.log("draw "+rect.id)
                    } else {
                        $(el).css({
                            "background-image": "none"
                        })
                    }
                }}
                className={classnames(style.Rect,
                    (selectedSubPanel && (selectedSubPanel.id == rect.panel.id)) && style.RectSelected,
                    is_top && style.TopRect,
                    directionClass)}
                style={cssObj}
                onClick={(e)=>this.rectSelected(e, rect.id)}
                data-rect>
                {!rect.panel.subPanels &&
                    <span className={classnames(style.RectTip, panelStyle.PanelRectTip)} style={{maxHeight:'100%'}} onClick={(e)=>this.rectSelected(e, rect.id)} >
                        {rect.panel.panelWidth}
                        {splitl != '0' ? splitl : ''}
                        *
                        {rect.panel.panelLength}
                        {splitR != '0' ? splitR : ''}
                        {memo ? <Tooltip title={memo} placement="bottom" >
                            <Icon key={this.props.key} type="warning" style={{ color: 'red' }} />
                        </Tooltip>
                            : ''}
                    </span>
                }
                {isLockPanel && <Icon type="lock" onClick={()=>{this.props.onLocking('')}} style={{margin:'auto', fontSize:'20px', color:'#555'}}/> }


                {!rect.panel.subPanels && <span style={{ marginTop: 2, color: 'black', borderColor: 'black', fontSize: '13px', fontWeight:'bold'}} className={classnames(panelStyle.PanelIndexTip)} >{rect.panel.subPanelIndex}</span>}

                {/*{!rect.panel.subPanels && <span className={classnames(style.RectTip)}
                 style={{paddingTop: "1em"}}>{this.getPanelTypeName(rect.panel.panelType)}</span>}*/}
                {/*{JSON.stringify(rect)}*/}

                {rect.panel.subPanels && rect.panel.subPanels.map((subPanel, index) => {
                    const subRect = generatePanelRect(subPanel, rect.panel)
                    return <RectContainer key={subRect.id} rect={subRect}
                        product={product}
                        parentPanel={rect.panel}
                        selectedSubPanel={selectedSubPanel} />
                })}
                {rect.panel.subPanels && <div data-divide="true" ref={(el) => {
                    this.initDrag(el, rect.panel.direction)
                    // console.log("change bg_divide "+rect.panel.id)
                    $(el).css("background-image", "url(" + bg_divide + ")")
                }} className={classnames(style.RectDivider)} style={divideStyle}>

                </div>}
            </div>
        )
    }
}
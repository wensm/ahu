import {
    LICENSE_EXPIRED_WARNING_MESSAGE,
    LICENSE_APPLICATION_MESSAGE,
    DOWNLOAD_APPLICATION,
    APPLY_LICENSE,
    INSTALL_LICENSE_MESSAGE,
    INSTALL_LICENSE,
    CHOOSE_LICENSE_FILE,
    APPLY_LICENSE_EMAIL_SUBJECT,
    APPLY_LICENSE_KEY_TITLE,
} from '../pages/intl/i18n'

import {
    installLicense
} from '../actions/general';
import React from 'react';
import { connect } from 'react-redux';
import $http from '../misc/$http'
import intl from 'react-intl-universal'
import style from './app.css';
import { Row, Col, Icon} from 'antd'


class AppLogin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            license: '',
            choseFileName: ''
        }
        this.onApply = this.onApply.bind(this)
        this.onInstall = this.onInstall.bind(this)
        this.onFileChange = this.onFileChange.bind(this)
        this.onDownload = this.onDownload.bind(this)
    }
    onApply() {
        let mailTo = "mailto:" + this.state.email;
        mailTo += "?subject=" + intl.get(APPLY_LICENSE_EMAIL_SUBJECT);
        mailTo += "&body=" + intl.get(APPLY_LICENSE_KEY_TITLE) + this.state.license;
        window.location.href = mailTo;
    }
    onInstall() {
        var formData = new FormData();
        formData.append("licenseFile", document.getElementById("licenseFile").files[0]);
        this.props.installLicense(formData);
    }
    onFileChange() {
        this.setState({
            choseFileName: $("#licenseFile").val()
        })
    }
    onDownload() {
        window.location.href = `${SERVICE_URL}` + 'license/application/download';
    }
    componentWillMount() {
        this.setState({
            license: this.props.location.query.license,
            email: this.props.location.query.email
        })
    }
    render() {
        let licenseAppMessage = intl.get(LICENSE_APPLICATION_MESSAGE);
        licenseAppMessage = licenseAppMessage.replace("ADMIN_EMAIL", this.state.email);
        let licenseAppMessageA = licenseAppMessage.substring(0, licenseAppMessage.indexOf("#"));
        let licenseAppMessageB = licenseAppMessage.substring(licenseAppMessage.lastIndexOf("#") + 1)
        return (
            <div className={style.login}>
                <div style={{ width: '700px', height: '420px', margin: '-210px 0 0 -350px', position: 'absolute', top: '50%', left: '50%', color: 'white' }}>
                    <div className={style.systemLogin} />
                    <Row type="flex" justify="center">
                        <Col span={12}>
                            <p>
                                {intl.get(LICENSE_EXPIRED_WARNING_MESSAGE)}
                            </p>
                            <p>
                                {licenseAppMessageA}
                                <a onClick={() => this.onDownload()}>{intl.get(DOWNLOAD_APPLICATION)}</a>
                                {licenseAppMessageB}
                            </p>
                        </Col>
                        <Col span={8}>
                            <Row>
                                {/* <button className="btn btn-primary" style={{marginLeft:'40px', marginBottom:'15px'}} onClick={() => this.onInstall()}>{intl.get(INSTALL_LICENSE)}</button> */}
                                <button className="btn btn-primary" style={{ marginLeft: '50px', marginBottom: '15px' }} onClick={() => this.onApply()}>{intl.get(APPLY_LICENSE)}</button>
                            </Row>
                            <Row>
                                {/* <button className="btn btn-primary" style={{ marginLeft: '40px', marginBottom: '15px' }} onClick={() => this.onDownload()}>{intl.get(DOWNLOAD_APPLICATION)}</button> */}
                                {/* <a onClick={() => this.onDownload()}>{intl.get(DOWNLOAD_APPLICATION)}</a> */}
                                <button className="btn btn-primary" style={{ marginLeft: '50px', marginBottom: '15px' }}  onClick={()=>{$('#licenseFile').click()}}> 
                                    <input type="file" name="licenseFile" id="licenseFile" className={style.inputfile} accept=".dat" onChange={() => this.onFileChange()}></input>
                                    <label htmlFor="licenseFile2" style={{marginBottom:'0', cursor:'pointer'}}>
                                        {/* <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                                        </svg> */}
                                        <Icon type="download" style={{paddingRight:'5px'}} />
                                        <span>{intl.get(CHOOSE_LICENSE_FILE)}</span>
                                    </label>
                                    <br />
                                    <span style={{ color: "#fff" }}>{this.state.choseFileName}</span>
                                </button>

                            </Row>
                            <Row>
                                <button className="btn btn-primary" style={{ marginLeft: '50px' }} onClick={() => this.onInstall()}>{intl.get(INSTALL_LICENSE)}</button>
                            </Row>

                        </Col>
                    </Row>


                    {/* <p style={{ color: "red", fontWeight: "bold", fontSize: "20px", textAlign: "center" }}>{intl.get(LICENSE_EXPIRED_WARNING_MESSAGE)}</p>

                <p style={{ color: "#fff", textAlign: "center" }}>
                    {licenseAppMessageA}
                    <a onClick={() => this.onDownload()}>{intl.get(DOWNLOAD_APPLICATION)}</a>
                    {licenseAppMessageB}
                </p> */}
                    {/* <br /> */}
                    {/* <div style={{ textAlign: "center" }}>
                    <button className="btn btn-primary" onClick={() => this.onApply()}>{intl.get(APPLY_LICENSE)}</button>
                    <br /><br /><br />
                    <p style={{ color: "#fff" }}>{intl.get(INSTALL_LICENSE_MESSAGE)}</p>
                    <div>
                        <input type="file" name="licenseFile" id="licenseFile" className={style.inputfile} accept=".dat" onChange={() => this.onFileChange()}></input>
                        <label htmlFor="licenseFile">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                            </svg>
                            <span>{intl.get(CHOOSE_LICENSE_FILE)}</span>
                        </label>
                        <br />
                        <span style={{ color: "#fff" }}>{this.state.choseFileName}</span>
                    </div>
                    <br />
                    <button className="btn btn-primary" onClick={() => this.onInstall()}>{intl.get(INSTALL_LICENSE)}</button>
                </div> */}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
    }
}

const mapDispatchToProps = dispatch => {
    return {
        installLicense(formData) {
            dispatch(installLicense(formData));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AppLogin)
import {
    PLEASE_CHOOSE_UNIT,
    PROJECT_CHECKLIST,
    PRIMARY_PARAMETER_LIST,
    LONG_LEAD_TIME_OPTION,
    ARUP_REPORT,
    BAICHENG_ENGINEERING_REPORT,
    CUSTOMIZE_REPORT_FORMAT,
    TECHNICAL_SPECIFICATION,
    TRIPLE_VIEW,
    FAN_CURVE,
    PSYCHROMETRIC_CHART,
    SALES_VERSION,
    GENERATE_FAILED,
    GENERATE_SUCCESS,
    GENERATE_REPORT,
    THIRD_PARTY_INFO_DOC,
    ENGINEER_VERSION,
    RELATED_FILE,
    SELECT_ALL,
    UNIT_NUMBER,
    DRAWING_NO,
    UNIT_NAME,
    UNIT_MODEL,
    STATE,
    CANCEL,
    SAVE,
    REPORT_GENERATING,
    NON_STANDARD_FILE,
    NON_STANDARD_LIST,
    CRM_FILE,
    CRM_LIST,
    SAP_FILE,
    SAP_LIST,
    PANEL_SCREENSHOTS,
    UNIT_NAMEPLATE_DATA,
    PRODUCT_PACKING_LIST,
    SECTION_CONNECTION_LIST,
    PRINT,
    REPORT_CONTENT,
    VIEW_REPORT,
    VIEW_REPORT_WORD,
    REPORTDESC,
    CAD_DRAWING,
    DWG_FILE,
    PLEASE_CONTINUOUS_UNIT
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import { reduxForm } from 'redux-form'
import classnames from 'classnames'
import style from './CreateReport.css'
import Field from '../pages/ahu/Field'
import DateUtil from '../misc/DateUtils'
import AhuDiv from '../pages/groups/AhuDiv'
import style1 from '../pages/groups/GroupDiv.css'
import loadingImg from '../images/loding.gif'
import sweetalert from 'sweetalert'
import SocketPanel from '../pages/groups/SocketPanel'
import { Icon,Tooltip} from 'antd';

export const PROJECT_REPORTS = 'PROJECT_REPORTS'

class CreateReport extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            reportForm: {},
            reports: [],
            defaultChecked: true,
            'tView': false,
            'fan': false,
            'hanshi': false,
            'panelScreenshots': false,
            'unitNameplateData': false,
            'productPackingList': false,
            'sectionConnectionList': false,
            techproj: true,
            techprojPdf: true,
            isPdf: true,
            isWord: false,
            prolist: false,
            crmlist: false,
            saplist:false,
            dwglist:false
        }
        this.saveCreateReport = this.saveCreateReport.bind(this)
        this.onResult = this.onResult.bind(this);
        this.onCreateReportFinish = this.onCreateReportFinish.bind(this);
        this.removeReport = this.removeReport.bind(this)
        this.ReportFormChange = this.ReportFormChange.bind(this)
        // this.startGeneratingReport = this.startGeneratingReport.bind(this);
    }

    componentDidMount() {
        this.removeReport()
        this.initCheckSelect()
    }

    initCheckSelect() {
        $('[data-name="CreateReport"]').on('click', '.selAhuCheckBox', () => {

            var l = $('.selAhuCheckBox').length,
                lc = $('.selAhuCheckBox:checked').length,
                all = l && l === lc;
            $('.select-all [type=checkbox]').prop('checked', all);
        })
        //        console.log('this', this.props.user.userRole)
        if (this.props.user && this.props.user.userRole == 'Factory') {
            this.setState({
                // 'prolist': true,
                'saplist': true,
                'tView': true,
                'panelScreenshots': true,
                'unitNameplateData': true,
                'productPackingList': true,
                'sectionConnectionList': true,
            })
        }
    }

    toggleSelectAll() {
        if ($('.select-all [type=checkbox]:checked').length) {
            //全选时  AHU状态 为正在选型的数据不要勾选
            $('.selAhuCheckBox.selected').prop('checked', true)
        } else {
            $('.selAhuCheckBox.selected').prop('checked', false)
        }

    }

    removeReport() {
        $('#CreateReport').on('hidden.bs.modal', (e) => {
            if (e.target.id == 'CreateReport') {
                this.props.onClearReports()
            }

        })
    }

    ReportFormChange(event) {
        var name = event.target.name
        var value = event.target.checked
        // console.log(value + ',' +name);
        if (value && (name == "tView" || name == "fan" || name == "hanshi"
            || name == "panelScreenshots" || name == "unitNameplateData"
            || name == "productPackingList" || name == "sectionConnectionList")) {
            //            $("input[name=techproj]").prop("checked", "checked")
            this.setState({ 'techproj': true });
        }
    }

    saveCreateReport() {
        this.setState({ 'generating': true })
        let isPdf = this.state.isPdf
        let isWord = this.state.isWord
        var obj = {
            list:[]
        }
        obj.ahuIds = []
        if (this.props.flg == "groups"||this.props.flg == "projects") {
            $(".selAhuCheckBox").each(function (index, record) {
                if ($(this).is(':checked')) {
                    obj.ahuIds.push($(this).attr("id"))
                    obj.list.push($(this).attr("data-id"))
                }
                // if(index == 0){
                //     obj.first = $(this).attr('data-id')
                // }
                // if(index == ($(".selAhuCheckBox").length-1)){
                //     obj.last = $(this).attr('data-id')
                // }
            })
        }
        if ((this.props.flg == "groups"||this.props.flg == "projects") && obj.ahuIds.length == 0) {//项目里面的生成报告 如果没有勾选，则给提示，不做提交。
            sweetAlert(intl.get(PLEASE_CHOOSE_UNIT), "", "error");
            return;
        }
        if (this.props.flg != "groups" || obj.ahuIds.length != 0 || true) {
            let param = {
                "ahuIds": obj.ahuIds,
                "projectId": this.props.projectId,
                "reports": [{
                    "type": "excel",
                    "name": intl.get(PROJECT_CHECKLIST),
                    "output": false,
                    "path": "/asserts/report/pro1/" + intl.get(PROJECT_CHECKLIST) + ".xlsx",
                    "items": [],
                    "classify": "prolist",
                }, {
                    "type": "excel",
                    "name": intl.get(PRIMARY_PARAMETER_LIST),
                    "output": false,
                    "path": "/asserts/report/pro1/" + intl.get(PRIMARY_PARAMETER_LIST) + ".xlsx",
                    "items": [],
                    "classify": "paralist",
                }, {
                    "type": "excel",
                    "name": intl.get(LONG_LEAD_TIME_OPTION),
                    "output": false,
                    "path": "/asserts/report/pro1/" + intl.get(LONG_LEAD_TIME_OPTION) + ".xlsx",
                    "items": [],
                    "classify": "delivery",
                }, {
                    "type": "pdf",
                    "name": intl.get(ARUP_REPORT),
                    "output": false,
                    "path": "/asserts/report/proj1/" + intl.get(ARUP_REPORT) + ".pdf",
                    "classify": "aruppf",
                    "items": []
                }, {
                    "type": "pdf",
                    "name": intl.get(BAICHENG_ENGINEERING_REPORT),
                    "output": false,
                    "path": "/asserts/report/proj1/" + intl.get(BAICHENG_ENGINEERING_REPORT) + ".pdf",
                    "classify": "bochengpf",
                    "items": []
                }/*, {
                    "type": "pdf",
                    "name": intl.get(CUSTOMIZE_REPORT_FORMAT),
                    "output": false,
                    "path": "/asserts/report/proj1/" + intl.get(CUSTOMIZE_REPORT_FORMAT) + ".pdf",
                    "classify": "custompf",
                    "items": []
                }*/, {
                    "type": isPdf ? "pdf" : "",
                    "name": intl.get(TECHNICAL_SPECIFICATION),
                    "output": false,
                    "path": isPdf ? "/asserts/report/proj1/" + intl.get(TECHNICAL_SPECIFICATION) + ".pdf" : "",
                    "classify": "techprojPdf",
                    "items": [
                        { "name": intl.get(TRIPLE_VIEW), "classify": "tView", "output": false },
                        { "name": intl.get(FAN_CURVE), "classify": "fan", "output": false },
                        { "name": intl.get(PSYCHROMETRIC_CHART), "classify": "hanshi", "output": false },
                        { "name": intl.get(PANEL_SCREENSHOTS), "classify": "panelScreenshots", "output": false },
                        { "name": intl.get(UNIT_NAMEPLATE_DATA), "classify": "unitNameplateData", "output": false },
                        { "name": intl.get(PRODUCT_PACKING_LIST), "classify": "productPackingList", "output": false },
                        { "name": intl.get(SECTION_CONNECTION_LIST), "classify": "sectionConnectionList", "output": false }
                    ]
                }, {
                    "type": !isWord ? "" : "word",
                    "name": intl.get(TECHNICAL_SPECIFICATION),
                    "output": false,
                    "path": !isWord ? "" : "/asserts/report/proj1/" + intl.get(TECHNICAL_SPECIFICATION) + ".docx",
                    "classify": "techprojWord",
                    "items": [
                        { "name": intl.get(TRIPLE_VIEW), "classify": "tView", "output": false },
                        { "name": intl.get(FAN_CURVE), "classify": "fan", "output": false },
                        { "name": intl.get(PSYCHROMETRIC_CHART), "classify": "hanshi", "output": false },
                        { "name": intl.get(PANEL_SCREENSHOTS), "classify": "panelScreenshots", "output": false },
                        { "name": intl.get(UNIT_NAMEPLATE_DATA), "classify": "unitNameplateData", "output": false },
                        { "name": intl.get(PRODUCT_PACKING_LIST), "classify": "productPackingList", "output": false },
                        { "name": intl.get(SECTION_CONNECTION_LIST), "classify": "sectionConnectionList", "output": false }
                    ]
                }, {
                    "type": "pdf",
                    "name": intl.get(SALES_VERSION),
                    "output": false,
                    "path": "/asserts/report/proj1/" + intl.get(SALES_VERSION) + ".pdf",
                    "classify": "techsaler",
                    "items": []
                }, {
                    "type": "word",
                    "name": intl.get(NON_STANDARD_LIST),
                    "output": false,
                    "path": "/asserts/report/proj1/" + intl.get(NON_STANDARD_LIST) + ".docx",
                    "classify": "speciallist",
                    "items": []
                }, {
                    "type": "excel",
                    "name": intl.get(CRM_LIST),
                    "output": false,
                    "path": "/asserts/report/proj1/" + intl.get(CRM_LIST) + ".docx",
                    "classify": "crmlist",
                    "items": []
                }, {
                    "type": "excel",
                    "name": intl.get(SAP_LIST),
                    "output": false,
                    "path": "/asserts/report/proj1/" + intl.get(SAP_LIST) + ".zip",
                    "classify": "saplist",
                    "items": []
                }, {
                    "type": "excel",
                    "name": intl.get(DWG_FILE),
                    "output": false,
                    "path": "/asserts/report/proj1/" + intl.get(DWG_FILE) + ".zip",
                    "classify": "dwglist",
                    "items": []
                }]
            }
            let canSave = true
            param.reports.forEach(report => {
                report.items.forEach(item => {
                    if ($("input[name=" + item.classify + "]").is(":checked")) {
                        item.output = true
                    }
                })
                if ($("input[name=" + report.classify + "]").is(":checked")) {
                    report.output = true
                }
                if(report.classify == "crmlist" && report.output == true){
                    // console.log('obj', obj)
                    let count = obj.list.length
                    let first = Number(obj.list[0])
                    let last = Number(obj.list[obj.list.length-1])
                    if((last-first + 1) == count){ 
                    }else{
                        canSave = false                            
                    }
                }
            })
            let tmpProjectId = $('#CreateReport').data('projectId')
            if (!tmpProjectId) {
                tmpProjectId = this.props.projectId
            }
            if (!tmpProjectId) {
                console.error("Quit reporte generation with empty projectId")
                return
            }
            param.projectId = tmpProjectId
            if(canSave){
                this.props.onSaveCreateReport(param, ()=>{this.startGeneratingReport(this.props.sysPort)})
            }else{
                sweetAlert(intl.get(PLEASE_CONTINUOUS_UNIT), "", "error")
            }
        }
    }
    click = (e) => {
        let name = e.target.name
        let { tView,
            fan,
            hanshi,
            panelScreenshots,
            unitNameplateData,
            productPackingList,
            sectionConnectionList,
            techproj,
            prolist,
            crmlist,
            saplist,
            dwglist
        } = this.state;
        if (name == 'prolist') {
            this.setState({
                'prolist': !prolist
            })
        }
        if (name == 'crmlist') {
            this.setState({
                'crmlist': !crmlist
            })
        }
        if (name == 'saplist') {
            this.setState({
                'saplist': !saplist
            })
        }
        if (name == 'dwglist') {
            this.setState({
                'dwglist': !dwglist
            })
        }
        if (name == 'tView') {
            this.setState({
                'tView': !tView
            })
        }
        if (name == 'fan') {
            this.setState({
                'fan': !fan
            })
        }
        if (name == 'hanshi') {
            this.setState({
                'hanshi': !hanshi
            })
        }
        if (name == 'panelScreenshots') {
            this.setState({
                'panelScreenshots': !panelScreenshots
            })
        }
        if (name == 'unitNameplateData') {
            this.setState({
                'unitNameplateData': !unitNameplateData
            })
        }
        if (name == 'productPackingList') {
            this.setState({
                'productPackingList': !productPackingList
            })
        }
        if (name == 'sectionConnectionList') {
            this.setState({
                'sectionConnectionList': !sectionConnectionList
            })
        }
    }

    startGeneratingReport(sysPort) {
        let tmpProjectId = $('#CreateReport').data('projectId')
        if (!tmpProjectId) {
            tmpProjectId = this.props.projectId
        }
        let socketUrl = "ws://" + window.location.hostname + ":"+sysPort+"/websocket/" + tmpProjectId;
        this.refs.reportSocketPanel.start(socketUrl)
    }

    onCreateReportError() {
        $("#generatingReport").modal("hide");
        sweetalert({ title: intl.get(GENERATE_FAILED), text: '', type: 'error', timer: 1000, showConfirmButton: false });
    }

    onResult(result) {
        var response = JSON.parse(result);
        // console.log(response);
        this.props.onReportGenerated(response.reports);
    }

    onCreateReportFinish() {
        $("#generatingReport").modal("hide");
        this.refs.reportSocketPanel.stop();
        sweetalert({ title: intl.get(GENERATE_SUCCESS), type: 'success', timer: 1000, showConfirmButton: false });
    }

    render() {
        const { peojectReports, groupReports, flg, projectId, ahuList, user, allAhuList } = this.props
        let { defaultChecked,
            tView,
            fan,
            hanshi,
            panelScreenshots,
            unitNameplateData,
            productPackingList,
            sectionConnectionList,
            techproj,
            isPdf,
            isWord,
            crmlist,
            prolist,
            saplist,
            dwglist
        } = this.state;
        let sanfangPower = true
        if (user && user.userRole == 'Factory') {
            sanfangPower = false
        }
        let panelWord = true
        if (user && (user.userRole == 'Sales' || user.userRole == 'Engineer')) {
            panelWord = false
        }
        return (
            <div className="modal fade" data-name="CreateReport" id="CreateReport" tabIndex="-1" role="dialog"
                aria-labelledby="myModalLabel" data-projectid={projectId}>
                <div className="modal-dialog" role="document" >
                    <div className="modal-content" style={{ width: '700px', height: '504px' }}>
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title" id="myModalLabel"><b>{intl.get(GENERATE_REPORT)}</b>
                            </h4>
                        </div>
                        <div className="modal-body" style={{ maxHeight: '700px', overflow: 'hidden', padding: '0px', minHeight: '380px' }}>

                            {peojectReports && groupReports && (peojectReports.length == 0 && groupReports.length == 0) ? <form data-name="CreateReport" onChange={(event) => this.ReportFormChange(event)}>
                                <div style={{ width: '698px', height: '380px', overflow: 'auto' }}>
                                    <div>

                                        <div style={{ marginTop: '10px', marginBottom: '10px' }}>

                                            <div style={{ marginLeft: '30px', float: 'left' }}>
                                                {sanfangPower ? <div>
                                                    <div style={{
                                                        width: '150px',
                                                        color: '#505050',
                                                        fontWeight: 'bold',
                                                        fontSize: '12px',
                                                        margin: '0px 0px 15px 0px'
                                                    }}>{intl.get(THIRD_PARTY_INFO_DOC)}</div>
                                                    <label className="checkbox-inline"
                                                        style={{ marginLeft: '30px', color: '#666666' }}>
                                                        <input type="checkbox" name="aruppf"
                                                            style={{ height: '13px' }} /> {intl.get(ARUP_REPORT)}
                                                    </label>
                                                    <label className="checkbox-inline" style={{ color: '#666666' }}>
                                                        <input type="checkbox" name="bochengpf"
                                                            style={{ height: '13px' }} /> {intl.get(BAICHENG_ENGINEERING_REPORT)}
                                                    </label>
                                                </div> : ''}

                                                {/*<label className="checkbox-inline" style={{color: '#666666'}}>
                                                    <input type="checkbox" name="custompf"
                                                           style={{height: '13px'}}/> {intl.get(CUSTOMIZE_REPORT_FORMAT)}
                                                </label>*/}
                                                <div style={{
                                                    width: '150px',
                                                    color: '#505050',
                                                    fontWeight: 'bold',
                                                    fontSize: '12px',
                                                    margin: '10px 0px 15px 0px'
                                                }}>{intl.get(TECHNICAL_SPECIFICATION)}</div>
                                                {/* <label className="checkbox-inline" style={{
                                                    marginLeft: '30px',
                                                    fontWeight: 'bold',
                                                    fontSize: '12px',
                                                    color: '#505050'
                                                }}> */}
                                                <input type="checkbox" name="techprojPdf" value="pdf" checked={isPdf} onClick={(e) => {
                                                    let sta = e.target.checked
                                                    if (sta) {
                                                        this.setState({
                                                            'isPdf': sta,
                                                            'tView': sta,
                                                            'panelScreenshots': sta,
                                                            'unitNameplateData': sta,
                                                            'productPackingList': sta,
                                                            'sectionConnectionList': sta,
                                                        })
                                                    } else if (!sta && !this.state.isWord) {
                                                        this.setState({
                                                            'isPdf': sta,
                                                            'tView': sta,
                                                            'panelScreenshots': sta,
                                                            'unitNameplateData': sta,
                                                            'productPackingList': sta,
                                                            'sectionConnectionList': sta,
                                                        })
                                                    } if (!sta && this.state.isWord) {
                                                        this.setState({
                                                            'isPdf': sta,
                                                        })
                                                    }
                                                }} style={{ marginLeft: '30px' }} />
                                                {intl.get(ENGINEER_VERSION)}
                                                {panelWord ? <input type="checkbox" name="techprojWord" value="word" checked={isWord} onClick={(e) => {
                                                    let sta = e.target.checked
                                                    if (sta) {

                                                        this.setState({
                                                            'isWord': sta,
                                                            'tView': sta,
                                                            'panelScreenshots': sta,
                                                            'unitNameplateData': sta,
                                                            'productPackingList': sta,
                                                            'sectionConnectionList': sta,
                                                        })
                                                    } else if (!sta && !this.state.isPdf) {
                                                        this.setState({
                                                            'isWord': sta,
                                                            'tView': sta,
                                                            'panelScreenshots': sta,
                                                            'unitNameplateData': sta,
                                                            'productPackingList': sta,
                                                            'sectionConnectionList': sta,
                                                        })
                                                    } else if (!sta && this.state.isPdf) {
                                                        this.setState({
                                                            'isWord': sta,
                                                        })
                                                    }
                                                }} style={{ marginLeft: '10px' }} /> : ''}
                                                {panelWord ? intl.get(VIEW_REPORT_WORD) : ''}

                                                {/* <input type="checkbox" name="techproj"
                                                        defaultChecked={defaultChecked}
                                                        checked={techproj}
                                                        onClick={this.click}
                                                        style={{ height: '13px' }} /> {intl.get(ENGINEER_VERSION)}
                                                    <input type="checkbox" name="techproj2"
                                                        defaultChecked={defaultChecked}
                                                        checked={techproj}
                                                        onClick={this.click}
                                                        style={{ height: '13px' }} /> {intl.get(ENGINEER_VERSION)} */}
                                                {/* </label> */}
                                                <div style={{
                                                    marginLeft: '60px',
                                                    marginBottom: '16px',
                                                    marginTop: '16px'
                                                }}>
                                                    <label className="checkbox-inline" style={{ color: '#666666' }}>
                                                        <input type="checkbox" name="tView"
                                                            defaultChecked={!defaultChecked}
                                                            checked={tView}
                                                            onClick={this.click}
                                                            style={{ height: '13px' }} /> {intl.get(TRIPLE_VIEW)}
                                                    </label>
                                                    <label className="checkbox-inline" style={{ color: '#666666' }}>
                                                        <input type="checkbox" name="fan"
                                                            defaultChecked={!defaultChecked}
                                                            checked={fan}
                                                            onClick={this.click}
                                                            style={{ height: '13px' }} /> {intl.get(FAN_CURVE)}
                                                    </label>
                                                    <label className="checkbox-inline" style={{ color: '#666666' }}>
                                                        <input type="checkbox" name="hanshi"
                                                            defaultChecked={!defaultChecked}
                                                            checked={hanshi}
                                                            onClick={this.click}
                                                            style={{ height: '13px' }} /> {intl.get(PSYCHROMETRIC_CHART)}
                                                    </label>


                                                    {/*面板布置图 机组铭牌数据 产品装箱单 段连接清单*/<br />}
                                                    {panelWord ? <label className="checkbox-inline" style={{ color: '#666666' }}>
                                                        <input type="checkbox" name="panelScreenshots"
                                                            defaultChecked={!defaultChecked}
                                                            checked={panelScreenshots}
                                                            onClick={this.click}
                                                            style={{ height: '13px' }} /> {intl.get(PANEL_SCREENSHOTS)}
                                                    </label> : ''}
                                                    {panelWord ? <label className="checkbox-inline" style={{ color: '#666666' }}>
                                                        <input type="checkbox" name="unitNameplateData"
                                                            defaultChecked={!defaultChecked}
                                                            checked={unitNameplateData}
                                                            onClick={this.click}
                                                            style={{ height: '13px' }} /> {intl.get(UNIT_NAMEPLATE_DATA)}
                                                    </label> : ''}
                                                    {<br />}
                                                    {panelWord ? <label className="checkbox-inline" style={{ color: '#666666' }}>
                                                        <input type="checkbox" name="productPackingList"
                                                            defaultChecked={!defaultChecked}
                                                            checked={productPackingList}
                                                            onClick={this.click}
                                                            style={{ height: '13px' }} /> {intl.get(PRODUCT_PACKING_LIST)}
                                                    </label> : ''}
                                                    {panelWord ? <label className="checkbox-inline" style={{ color: '#666666' }}>
                                                        <input type="checkbox" name="sectionConnectionList"
                                                            defaultChecked={!defaultChecked}
                                                            checked={sectionConnectionList}
                                                            onClick={this.click}
                                                            style={{ height: '13px' }} /> {intl.get(SECTION_CONNECTION_LIST)}
                                                    </label> : ''}
                                                    {/*面板布置图 机组铭牌数据 产品装箱单 段连接清单*/}


                                                </div>
                                                <label className="checkbox-inline" style={{
                                                    marginLeft: '30px',
                                                    fontWeight: 'bold',
                                                    fontSize: '12px',
                                                    color: '#505050'
                                                }}>
                                                    <input type="checkbox" name="techsaler"
                                                        style={{ height: '13px' }} /> {intl.get(SALES_VERSION)}
                                                </label>
                                                <div style={{
                                                    width: '100px',
                                                    color: '#505050',
                                                    fontWeight: 'bold',
                                                    fontSize: '12px',
                                                    margin: '10px 0px 15px 0px'
                                                }}>{intl.get(RELATED_FILE)}</div>
                                                <label className="checkbox-inline"
                                                    style={{ marginLeft: '30px', color: '#666666' }}>
                                                    <input type="checkbox" name="prolist" checked={prolist} onClick={this.click}
                                                        style={{ height: '13px' }} /> {intl.get(PROJECT_CHECKLIST)}
                                                </label>
                                                <label className="checkbox-inline" style={{ color: '#666666' }}>
                                                    <input type="checkbox" name="paralist"
                                                        style={{ height: '13px' }} /> {intl.get(PRIMARY_PARAMETER_LIST)}
                                                </label>
                                                <label className="checkbox-inline" style={{ color: '#666666' }}>
                                                    <input type="checkbox" name="delivery"
                                                        style={{ height: '13px' }} /> {intl.get(LONG_LEAD_TIME_OPTION)}
                                                </label>
                                                <div style={{
                                                    width: '100px',
                                                    color: '#505050',
                                                    fontWeight: 'bold',
                                                    fontSize: '12px',
                                                    margin: '10px 0px 15px 0px'
                                                }}>{intl.get(NON_STANDARD_FILE)}
                                                </div>
                                                <label className="checkbox-inline"
                                                    style={{ marginLeft: '30px', color: '#666666' }}>
                                                    <input type="checkbox" name="speciallist"
                                                        style={{ height: '13px' }} /> {intl.get(NON_STANDARD_LIST)}
                                                </label>

                                                <div style={{
                                                    width: '100px',
                                                    color: '#505050',
                                                    fontWeight: 'bold',
                                                    fontSize: '12px',
                                                    margin: '10px 0px 15px 0px'
                                                }}>{intl.get(CRM_FILE)}
                                                </div>
                                                <label className="checkbox-inline"
                                                    style={{ marginLeft: '30px', color: '#666666' }}>

                                                    <input type="checkbox" name="crmlist" checked={crmlist} onClick={this.click}
                                                        style={{ height: '13px' }} /> {intl.get(CRM_LIST)}
                                                </label>

                                                {panelWord ? <div>
                                                    <div style={{
                                                        width: '100px',
                                                        color: '#505050',
                                                        fontWeight: 'bold',
                                                        fontSize: '12px',
                                                        margin: '10px 0px 15px 0px'
                                                    }}>{intl.get(SAP_FILE)}
                                                    </div>
                                                    <label className="checkbox-inline"
                                                        style={{ marginLeft: '30px', color: '#666666' }}>
                                                        <input type="checkbox" name="saplist" checked={saplist} onClick={this.click}
                                                            style={{ height: '13px' }} /> {intl.get(SAP_LIST)}
                                                    </label>
                                                </div> : ''}
                                                
                                                <div style={{
                                                    width: '100px',
                                                    color: '#505050',
                                                    fontWeight: 'bold',
                                                    fontSize: '12px',
                                                    margin: '10px 0px 15px 0px'
                                                }}>{intl.get(CAD_DRAWING)}
                                                </div>
                                                <label className="checkbox-inline"
                                                    style={{ marginLeft: '30px', color: '#666666' }}>

                                                    <input type="checkbox" name="dwglist" checked={dwglist} onClick={this.click}
                                                        style={{ height: '13px' }} /> {intl.get(DWG_FILE)}
                                                </label>
                                            </div>
                                            <div style={{ clear: 'both' }}></div>
                                        </div>
                                    </div>
                                    {(flg == "groups" || flg == "projects" ) &&
                                        <div style={{
                                            padding: '10px',
                                            borderTop: '1px solid #F0F0F0',
                                            backgroundColor: '#F9FAFC'
                                        }}>


                                            <div className={classnames(style1.groupDiv, 'pro_cont clear com_cont')}>
                                                {
                                                    !this.state.collapse &&
                                                    <table ref={(c) => { this.listTable = c; }} className="table table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th><label className="select-all"
                                                                    onClick={() => this.toggleSelectAll(allAhuList)}
                                                                    style={{ margin: '0', color: '#666666' }}>
                                                                    <input type="checkbox"
                                                                        style={{ height: '13px' }} /> {intl.get(SELECT_ALL)}
                                                                </label> </th>
                                                                <th>{intl.get(UNIT_NUMBER)}</th>
                                                                {/* <th>{intl.get(DRAWING_NO)}</th> */}
                                                                <th>{intl.get(UNIT_NAME)}</th>
                                                                <th>{intl.get(UNIT_MODEL)}</th>
                                                                <th style={{ width: '9%' }}>
                                                                    AHU{intl.get(STATE)}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {
                                                                allAhuList && allAhuList.map((ahu, index) => {
                                                                    return <AhuDiv flg={"report"} key={index} data={ahu} />
                                                                })
                                                            }
                                                        </tbody>
                                                    </table>
                                                }
                                            </div>
                                        </div>
                                    }
                                </div>

                            </form> : <div style={{ margin: "30px 0 0 20px" }}>
                                            <a style={{ color: '#666666' }}>{intl.get(VIEW_REPORT)} : </a>
                                            <Tooltip title={intl.get(REPORTDESC)} placement="right">
                                                <Icon type="question-circle" />
                                            </Tooltip>
                                        </div>
                            }

                            {peojectReports.map(report => <div style={{ margin: "3px 0 0 50px" }}><a href={report.path} target="_blank"
                            >{report.name}</a></div>)}
                            {groupReports.map(report => <div style={{ margin: "3px 0 0 50px" }}><a href={report.path} target="_blank"
                            >{report.name}</a></div>)}
                        </div>

                        <div className="modal-footer">
                            <button type="button" className="btn btn-default"
                                data-dismiss="modal">{intl.get(CANCEL)}</button>
                            <button type="button" className="btn btn-primary"
                                onClick={this.saveCreateReport}>{intl.get(PRINT)}</button>
                            {/* <span className="pull-left"> */}
                            {/* <a style={{ color: '#666666' }}>{intl.get(VIEW_REPORT)} : </a>
                                {peojectReports.map(report => <button><a href={report.path} target="_blank"
                                    style={{ marginRight: '10px' }}>{report.name}</a></button>)}
                                {groupReports.map(report => <button><a href={report.path} target="_blank"
                                    style={{ marginRight: '10px' }}>{report.name}</a></button>)} */}
                            {/* </span> */}
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="generatingReport" data-name="isSetIng" tabIndex="-1" role="dialog"
                    aria-labelledby="myModalLabel" data-groupid="">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <SocketPanel ref="reportSocketPanel" panelName="genReport"
                                    title={intl.get(REPORT_GENERATING)}
                                    onError={this.onCreateReportError}
                                    onResult={this.onResult}
                                    onGetFinishMsg={this.onCreateReportFinish} />
                                <div className="row text-center">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default reduxForm({
    form: 'CreateReport', // a unique identifier for this form
    enableReinitialize: true,
})(CreateReport)
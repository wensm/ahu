import {connect} from 'react-redux'
import AhuInit from './AhuInit'
import {
  fetchprokey,
  saveAhuInit,
} from '../actions/projects'

const mapStateToProps = state => {
    return {
        initialValues: state.projects.prokey,
        projectId: state.projects.projectId,
        AhuInitForm: state.form.AhuInitForm == undefined?{}:state.form.AhuInitForm.values,
    }
}

const mapDispatchToProps = dispatch => {
    return {
			onSaveAhuInit(projectId,AhuInitValues){
				dispatch(saveAhuInit(projectId,AhuInitValues));
			}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AhuInit)

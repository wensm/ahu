import { connect } from 'react-redux'
import React, { Component } from 'react'
import { Field, reduxForm, change } from 'redux-form'

class ContactForm extends Component {
  render() {
    const { handleSubmit, onClick } = this.props
    return (
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="firstName" onClick={() => onClick()}>First Name</label>
          <Field name="firstName" component="input" type="text"/>
        </div>
        <div>
          <label htmlFor="lastName">Last Name</label>
          <Field name="lastName" component="input" type="text"/>
        </div>
        <div>
          <label htmlFor="email">Email</label>
          <Field name="email" component="input" type="email"/>
        </div>
        <button type="submit">Submit</button>
      </form>
    );
  }
}

// Decorate the form component
ContactForm = reduxForm({
  form: 'contact' // a unique name for this form
})(ContactForm)

const mapStateToProps = (state, ownProps) => {
  return {
    initialValues: state.account
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => {
      dispatch(change('contact', 'firstName', 'Bob'))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactForm)

import React from 'react'
import LinkContainer from './LinkContainer'

const Footer = () => (
  <p data-step="4" data-intro="filter todo list">
    Show:
    {" "}
    <LinkContainer filter="SHOW_ALL">
      All
    </LinkContainer>
    {", "}
    <LinkContainer filter="SHOW_ACTIVE">
      Active
    </LinkContainer>
    {", "}
    <LinkContainer filter="SHOW_COMPLETED">
      Completed
    </LinkContainer>
  </p>
)

export default Footer

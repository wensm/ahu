import {connect} from 'react-redux'
import CreateReport from './CreateReport'
// import {  无用代码
	// saveCreateReport,
	// clearReports
// } from '../actions/projects'


const mapStateToProps = (state,owProps) => {
    return {
       peojectReports:state.projects.reports == undefined? []:state.projects.reports,
       groupReports:state.groups.reports == undefined? []:state.groups.reports,
       projectId: owProps.projectId ==  undefined? "":owProps.projectId,
       flg: owProps.flg,
       ahuList: state.groups.ahuList == undefined? []:state.groups.ahuList,
       user: state.general.user,
       allAhuList: state.groups.allAhuList,
       sysPort: state.general.sysPort,

    }
}

const mapDispatchToProps = (dispatch,owProps) => {
    return {
		onSaveCreateReport(param, startGeneratingReport){
			owProps.onSaveCreateReport(param, startGeneratingReport);
		},
		onClearReports(){
			owProps.onClearReports()
		},
		onReportGenerated(reports){
            owProps.onReportGenerated(reports);
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateReport)

import {
  PREVIOUS_PAGE,
  BACK,
  NEXT_PAGE,
  CURRENT_PAGE_NUMBER,
} from '../pages/intl/i18n'

import React, { Component } from 'react';
import PDF from 'react-pdf-js';
import intl from 'react-intl-universal'
import style from './app.css'
import { hashHistory } from 'react-router'
import $http from '../misc/$http'
import { Pagination } from 'antd';

export default class AppWorder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      href: null
    }
  }
  componentWillMount() {

  }
  onDocumentComplete = (pages) => {
    this.setState({ page: 1, pages });
  }
  onPageComplete = (page) => {
    this.setState({ page });
  }

  handlePrevious = () => {
    this.setState({ page: this.state.page - 1 });
  }

  handleNext = () => {
    this.setState({ page: this.state.page + 1 });
  }
  black = () => {
    // console.log('====================================')
    // console.log("我要返回了")
    // console.log('====================================')
    hashHistory.go(-1)
  }

  onChange = (page) => {
    this.setState({ page: page });

  }
  renderPagination = (page, pages) => {
    let previousButton = <li className="previous" onClick={this.handlePrevious}><a><i className="fa fa-arrow-left"></i>{intl.get(PREVIOUS_PAGE)}</a></li>;
    if (page === 1) {
      previousButton = <li className="previous disabled"><a onClick={this.black}><i className="fa fa-arrow-left"></i>{intl.get(BACK)}</a></li>;
    }
    let nextButton = <li className="next" onClick={this.handleNext}><a>{intl.get(NEXT_PAGE)}<i className="fa fa-arrow-right"></i></a></li>;
    if (page === pages) {
      nextButton = <li className="next disabled"><a onClick={this.black}>{intl.get(BACK)}<i className="fa fa-arrow-right"></i></a></li>;
    }
    return (
      <Pagination defaultCurrent={1} current={this.state.page} onChange={this.onChange} total={pages * 10} style={{ marginBottom: '10px' }} />
      // <nav className={style.nav}>
      //    <span className={style.span}>{intl.get(CURRENT_PAGE_NUMBER)}{page}/{pages}</span>
      //   <ul className="pager">
      //     {previousButton}
      //     {nextButton}
      //   </ul> 
      // </nav>
    );
  }
  render() {
    let pagination = null;
    let href = this.props.location.query.href
    //    console.log('zzf', href)
    if (this.state.pages) {
      pagination = this.renderPagination(this.state.page, this.state.pages);
    }
    return (
      <div style={{ 'textAlign': 'center' }}>
        <PDF
          file={href}
          onDocumentComplete={this.onDocumentComplete}
          onPageComplete={this.onPageComplete}
          // scale={1.2}
          page={this.state.page}
        />
        {pagination}
        <button className="btn btn-primary" style={{ marginBottom: '10px' }} onClick={()=>{hashHistory.go(-1)}}>
          {intl.get(BACK)}
        </button>

      </div>
    )
  }
}

import React from 'react'
import { Field, reduxForm } from 'redux-form'

export default class DatePicker extends React.Component {

  componentDidMount() {
    const {
      name,
      onSynDateToReduxState,
    } = this.props
    $(`input[name='${name}']`).datepicker({
      autoclose: true,
      todayHighlight: true
    }).on('changeDate', e => {
      onSynDateToReduxState(name, e.target.value)
    })
  }

  componentWillReceiveProps(nextProps) {
    const {
      value,
    } = this.props
    $(`input[name='${name}']`).val(value)
  }

  render() {
    const {
      label,
      name,
    } = this.props
    return (
      <div data-name="DatePicker" className="form-group">
        <label>{label}</label>
        <Field name={name} className="form-control" component="input" type="text" />
      </div>
    )
  }
}

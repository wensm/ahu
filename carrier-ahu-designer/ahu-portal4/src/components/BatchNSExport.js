import {
    PARAMETER_BATCH_EXPORT,
    PARAMETER_BATCH_EXPORT_WARNING,
    FILE_NAME,
    CANCEL,
    EXPORT,
    EXPORT_NS_FILE
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'

export default class BatchNSExport extends React.Component {
    constructor(props) {
        super(props);
        this.exportFile = this.exportFile.bind(this);
        this.cancel = this.cancel.bind(this);

    }

    componentDidMount() {
        $('#BatchNSExport').on('hide.bs.modal', () => {
            setTimeout(() => {
                $("#nsFileName").val("")
            }, 200)
        })
    }

    exportFile() {
        let nsFileName = $("#nsFileName").val();

        if (this.props.onExportFile) {
            this.props.onExportFile(nsFileName)
            return;
        }

        let projectId = $("#BatchNSExport").data("projectId");
        this.props.saveBatchNSExport(projectId, nsFileName);
    }

    cancel() {
        $('#BatchNSExport').modal('hide');

    }

    render() {
        const {data} = this.props;
        return (
            <div className="modal fade" id="BatchNSExport" data-name="BatchNSExport" tabIndex="-1" role="dialog"
                 aria-labelledby="myModalLabel" data-projectid="" data-groupid="">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title" id="myModalLabel"><b>{intl.get(EXPORT_NS_FILE)}</b>
                            </h4>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-lg-2">
                                    <label style={{lineHeight: '32px'}}>{intl.get(FILE_NAME)}</label>
                                </div>
                                <div className="col-lg-8">
                                    <input className="form-control" id="nsFileName" name="trueattachment" type="text"/>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default"
                                    onClick={this.cancel}>{intl.get(CANCEL)}</button>
                            <button type="button" className="btn btn-primary"
                                    onClick={this.exportFile}>{intl.get(EXPORT)}</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

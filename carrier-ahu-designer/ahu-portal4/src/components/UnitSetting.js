import {
    METRIC,
    IMPERIAL,
    UNIT_SETTING,
    CANCEL,
    CONFIRM,
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import {Field, reduxForm, formValues} from 'redux-form'
import classnames from 'classnames'

class UnitSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {unitPrefer: JSON.parse(JSON.stringify(props.unitPrefer))}
        this.saveUnitSetting = this.saveUnitSetting.bind(this);
    }

    saveUnitSetting(values) {
        this.props.onUpdateUnitPrefer(values);
        $('#UnitSetting').modal('hide');
    }

    //Render a single unit field
    renderField = (unit, index) =>
        <div className="checkbox" key={index}>
            <label
                style={{width: '110px'}}>{unit.key}{this.props.metadata.metaUnit[unit.key] && this.props.metadata.metaUnit[unit.key].name}</label>
            <label style={{width: '200px'}}>
                <Field
                    name={unit.key}
                    component="input"
                    type="radio"
                    value="M"
                />{' '}
                {intl.get(METRIC) + "(" + (this.props.metadata.metaUnit[unit.key] && this.props.metadata.metaUnit[unit.key].mlabel) + ')'}
            </label>
            <label style={{width: '200px'}}>
                <Field
                    name={unit.key}
                    component="input"
                    type="radio"
                    value="B"
                />{' '}
                {intl.get(IMPERIAL) + "(" + (this.props.metadata.metaUnit[unit.key] && this.props.metadata.metaUnit[unit.key].blabel) + ')'}
            </label>
        </div>


    render() {
        const {handleSubmit, pristine, reset, submitting, dirty, onUpdateUnitPrefer} = this.props
        const {unitPrefer} = this.state
        return (
            <div className="modal fade" data-name="UnitSetting" id="UnitSetting" tabIndex="-1" role="dialog"
                 aria-labelledby="myModalLabel">
                <div className="modal-dialog" role="document">
                    <div className="modal-content" style={{width: '600px'}}>
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title" id="myModalLabel"><b>{intl.get(UNIT_SETTING)}</b></h4>
                        </div>
                        <form data-name="UnitSetting" onSubmit={handleSubmit(this.saveUnitSetting)}>
                            <div className="modal-body"
                                 style={{overflow: 'hidden', padding: '0px'}}>

                                <div style={{height: '400px', overflow: 'auto'}}>
                                    <div style={{padding: '20px'}}>
                                        <div style={{marginTop: '20px', marginBottom: '20px'}}>
                                            {unitPrefer.map((unit, index) => this.renderField(unit, index)
                                            )}

                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default"
                                        data-dismiss="modal">{intl.get(CANCEL)}</button>
                                <button type="submit" disabled={!dirty}
                                        className="btn btn-primary">{intl.get(CONFIRM)}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default reduxForm({
    form: 'unit', // a unique identifier for this form 23:40
    enableReinitialize: true,
})(UnitSetting)

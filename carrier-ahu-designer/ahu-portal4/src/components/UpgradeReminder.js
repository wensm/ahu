import {
    SYSTEM_UPDATE,
    UPGRADE,
    CLOSE,
    INSTALLATION,
    DOWNLOAD,
    INSTALL_SUCCESS,
    DOWNLOAD_SUCCESS,
    INSTALL_FAILED,
    DOWNLOAD_FAILED,
    DOWNLOAD_PROGRESS,
    CURRENT_SYSTEM_VERSION,
    FOUND_NEW_UPDATE_VERSION,
    MESSAGE_RESTART_AFTER_UPGRADE,
    INSTALL_PROGRESS
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import {reduxForm} from 'redux-form'
import Field from '../pages/ahu/Field'
import style1 from '../pages/groups/GroupDiv.css'
import loadingImg from '../images/loding.gif'
import sweetalert from 'sweetalert'
import $http from '../misc/$http'
import { Line } from 'rc-progress';
import Websocket from 'react-websocket';

const PROGRESS = "PROGRESS";
const DONE = "DONE";
const START = "START";
const ERROR = "ERROR";

class UpgradeReminder extends React.Component {
    constructor(props) {sysPort
        super(props)
        this.state = {
            progress: "",
            progressLabel: intl.get(DOWNLOAD_PROGRESS),
            inprogress: false,
            wsurl: 'ws://'+window.location.hostname+ ':'+ props.sysPort ? props.sysPort:'20000'+ '/websocket/UPGRADE'
        }
        this.onUpgrade = this.onUpgrade.bind(this)
        this.onDownload = this.onDownload.bind(this)
        this.onProgress = this.onProgress.bind(this)
    }

    componentDidMount() {
        if(this.props && this.props.updateInfo && this.props.updateInfo.updateVersion
                && this.props.updateInfo.updateVersion != this.props.updateInfo.ahuVersion) {
            if(this.props.updateInfo.downloaded) {
                this.setState({
                    downloadFlag: DONE,
                    percent: 100,
                    progress: intl.get(DOWNLOAD_SUCCESS),
                    inprogress: false,
                })
            }
            $('#UpgradeReminder').modal('show')
        }
    }

    onDownload(){
        let ahuMajorVersion = parseInt(this.props.updateInfo.ahuVersion.charAt(0));
        let updateMajorVersion = parseInt(this.props.updateInfo.updateVersion.charAt(0));
        this.setState({
            downloadFlag: START,
            inprogress: true
        })
        let currentLocale = intl.determineLocale({
            urlLocaleKey: "lang",
            cookieLocaleKey: "lang"
        });
        if(ahuMajorVersion < updateMajorVersion) {
            $http.get('update/download?release=MAJOR&lang=' + currentLocale + '&version=' + this.props.updateInfo.updateVersion);
        } else {
            $http.get('update/download?release=MINOR&lang=' + currentLocale + '&version=' + this.props.updateInfo.updateVersion);
            this.setState({
                progress: ""
            })
        }
    }

    onUpgrade(){
        let ahuMajorVersion = parseInt(this.props.updateInfo.ahuVersion.charAt(0));
        let updateMajorVersion = parseInt(this.props.updateInfo.updateVersion.charAt(0));
        this.setState({
            upgradeFlag: START
        })
        if(ahuMajorVersion < updateMajorVersion) {
            $http.get('update/upgrade?release=MAJOR&version=' + this.props.updateInfo.updateVersion);
        } else {
            this.setState({
                progress: "",
                inprogress: true,
                progressLabel: intl.get(INSTALL_PROGRESS)
            })
            $http.get('update/upgrade?release=MINOR&version=' + this.props.updateInfo.updateVersion);
        }
    }

    onProgress(msg) {
        if(msg.startsWith(PROGRESS)) {
            let value = msg.substring(PROGRESS.length);
            this.setState({
                percent: value,
                progress: value + "%"
            })
        } else if(msg.startsWith(DONE)) {
            if(this.state.upgradeFlag == START) {
                this.setState({
                    percent: 100,
                    upgradeFlag: DONE,
                    progress: intl.get(INSTALL_SUCCESS)
                })
                sweetalert({title: intl.get(MESSAGE_RESTART_AFTER_UPGRADE), type: 'success', timer: 4000});
            } else {
                this.setState({
                    percent: 100,
                    downloadFlag: DONE,
                    progress: intl.get(DOWNLOAD_SUCCESS),
                    inprogress: false
                })
            }
        } else if(msg.startsWith(ERROR)) {
            this.setState({
                progress: msg.substring(ERROR.length),
                inprogress: false
            })
        } else {
            this.setState({
                progress: msg
            })
        }
    }

    render() {
        return (
            <div className="modal fade" data-name="UpgradeReminder" id="UpgradeReminder" 
                tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div className="modal-dialog" role="document">
                    <div className="modal-content" style={{width: '650px',height:'304px'}}>
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 className="modal-title" id="myModalLabel">
                                <b>{intl.get(SYSTEM_UPDATE)}</b>
                            </h4>
                        </div>
                        <div className="modal-body" style={{maxHeight: '500px', overflow: 'hidden', padding: '0px'}}>
                            <form data-name="UpgradeReminder">
                                <div style={{width: '648px', height: '180px', overflow: 'auto'}}>
                                    <div style={{marginLeft: '30px'}}>
                                        <div style={{width: '150px', color: '#505050', fontWeight: 'bold',
                                            fontSize: '12px', margin: '10px 0px 15px 0px' }}>
                                            {intl.get(FOUND_NEW_UPDATE_VERSION)}
                                            {(this.props.updateInfo && this.props.updateInfo.updateVersion) ? this.props.updateInfo.updateVersion : ""}
                                        </div>
                                        <div style={{width: '150px', color: '#505050', fontWeight: 'bold',
                                            fontSize: '12px', margin: '10px 0px 15px 0px' }}>
                                            {intl.get(CURRENT_SYSTEM_VERSION)}
                                            {(this.props.updateInfo && this.props.updateInfo.ahuVersion) ? this.props.updateInfo.ahuVersion : ""}
                                        </div>
                                        { this.state.downloadFlag &&
                                            <div style={{marginRight: '30px'}}>
                                                <Line percent={this.state.percent} style={{width: '100%', height: '5px'}} strokeWidth="1" strokeColor="#337ab7" />
                                                <Websocket url={this.state.wsurl} onMessage={this.onProgress}/>
                                                <div>
                                                {this.state.progressLabel + this.state.progress}
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal">
                                {intl.get(CLOSE)}
                            </button>
                            { this.state.downloadFlag != DONE  &&
                                <button type="button" disabled={this.state.inprogress} className="btn btn-primary" onClick={this.onDownload}>
                                    {intl.get(DOWNLOAD)}
                                </button>
                            }
                            { this.state.downloadFlag == DONE  &&
                                this.state.upgradeFlag != DONE && 
                                <button type="button" disabled={this.state.inprogress} className="btn btn-primary" onClick={this.onUpgrade}>
                                    {intl.get(INSTALLATION)}
                                </button>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default reduxForm({form: 'UpgradeReminder', enableReinitialize: true})(UpgradeReminder)

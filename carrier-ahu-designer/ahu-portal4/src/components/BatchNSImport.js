import {
  PLEASE_CHOOSE,
  IMPORT_UNIT,
  IMPORT_PROJECT,
  CHOOSE_FILE,
  BROWSING,
  OVERRIDE_EXISTING_PARAMETER,
  OVERRIDE_EXISTING_PROJECT,
  CANCEL,
  IMPORT,
  IMPORT_NS_FILE
} from '../pages/intl/i18n'

import intl from 'react-intl-universal'
import React from 'react'
import Select from 'react-select';
import SelectStyle from 'react-select/dist/react-select.min.css';
import { Spin } from 'antd'


export default class BatchNSImport extends React.Component {
  constructor(props) {
    super(props);
    this.selFileBtn = this.selFileBtn.bind(this);
    this.selFileChange = this.selFileChange.bind(this);
    this.saveBatchNSImport = this.saveBatchNSImport.bind(this);

    this.state = {
      nsFileName: "",
      imported: false,
      msg: ""
    };
  }
  componentDidMount() {
    if (this.state.imported) {
      this.setState({
        imported: false
      })
    }
    $('#BatchNSImport').on('hide.bs.modal', () => {
      setTimeout(() => {
        this.setState({
          msg: ''
        })
      }, 200)
    })
  }
  selFileBtn() {
    $("#nsConfigInputFile").click();
  }

  selFileChange() {
    var value = $("#nsConfigInputFile").val() || '';
    var indenNum = value.lastIndexOf('\\');
    var nsFileName = value.substring((indenNum + 1));
    //$("#fileInput").val(fileName);
    this.setState({
      nsFileName: nsFileName
    });
    return true;
  }

  saveBatchNSImport() {
    var projectId = $("#BatchNSImport").data("projectId");
    var formData = new FormData();
    formData.append("myfile", document.getElementById("nsConfigInputFile").files[0]);
    this.setState({
      imported: true
    })
    this.props.saveBatchNSImport(formData, projectId, (msg) => {
      this.setState({
        imported: false,
        msg: msg
      })
      if (!msg) {
        $('#BatchNSImport').modal('hide')
      }
    });
  }



  render() {
    let { imported } = this.state


    return (
      <div className="modal fade" id="BatchNSImport" data-name="BatchNSImport" tabIndex="-1" role="dialog"
        aria-labelledby="myModalLabel" data-groupid="">
        <div className="modal-dialog" role="document">
          <Spin
            spinning={imported}>
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                  aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="myModalLabel"><b>{intl.get(IMPORT_NS_FILE)}</b>
                </h4>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-lg-3">
                    <label style={{ lineHeight: '32px' }}>{intl.get(CHOOSE_FILE)}</label>
                  </div>
                  <div className="col-lg-6">
                    <input className="form-control" id="nsFileInput" readOnly value={this.state.nsFileName} name="trueattachment"
                      type="text" />
                    <input style={{ display: 'none' }} type="file" id="nsConfigInputFile"
                      onChange={this.selFileChange} />
                  </div>
                  <div className="col-lg-2">
                    <button className="btn btn-default" type="button"
                      onClick={this.selFileBtn}>{intl.get(BROWSING)}</button>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <div className="col-lg-8" style={{ textAlign: 'left' }}>
                  <label style={{ lineHeight: '32px' }}>{this.state.msg}</label>
                </div>
                <div>
                  <button type="button" className="btn btn-default"
                    data-dismiss="modal">{intl.get(CANCEL)}</button>
                  <button type="button" className="btn btn-primary"
                    onClick={this.saveBatchNSImport}>{intl.get(IMPORT)}</button>
                </div>
              </div>
            </div>
          </Spin>
        </div>
      </div>
    );
  }
}

import {connect} from 'react-redux'
import LanguageSetting from './LanguageSetting'
import {
    updateLanguage,
    updateUserLocaleToServer
}
    from
        '../actions/general'


const mapStateToProps = (state, owProps) => {
    return {
        initialValues: {locale: state.general.user.preferredLocale}
    }
}

const mapDispatchToProps = (dispatch, owProps) => {
    return {
        onUpdateLanguage(param) {
            dispatch(updateLanguage(param))
            dispatch(updateUserLocaleToServer(param))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(LanguageSetting)

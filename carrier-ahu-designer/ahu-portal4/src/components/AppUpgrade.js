import {
    INSTALL_UPGRADE,
    INSTALL_UPGRADE_MESSAGE,
    CHOOSE_UPGRADE_FILE,
    CLOSE,
    UPGRADE_FILE_IS_NOT_CHOSE,
    RESTART_MESSAGE_AFTER_UPGRADE
} from '../pages/intl/i18n'

import {
    upgrade
} from '../actions/general';
import React from 'react';
import { connect } from 'react-redux';
import intl from 'react-intl-universal'
import style from './app.css';
import loadingImg from '../images/loding.gif'
import sweetalert from 'sweetalert'
import { Row, Col, Icon } from 'antd'

class AppUpgrade extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: ""
        }
        this.onUpgrade = this.onUpgrade.bind(this)
        this.onClose = this.onClose.bind(this)
        this.onFinishUpgrade = this.onFinishUpgrade.bind(this)
    }
    onUpgrade() {
        if (document.getElementById("upgradeFile").files.length > 0
            && document.getElementById("upgradeFile").files[0].name.endsWith(".zip")) {
            var formData = new FormData();
            formData.append("upgradeFile", document.getElementById("upgradeFile").files[0]);
            this.props.upgrade(formData, this.onFinishUpgrade);
            this.setState({
                status: "upgrading"
            });
        } else {
            sweetalert({ title: intl.get(UPGRADE_FILE_IS_NOT_CHOSE), type: 'error',timer: 1000, showConfirmButton: false });
        }
    }
    onFinishUpgrade() {
        this.setState({
            status: "",
            choseFileName: ""
        })
        $("#upgradeFile").val("")
    }
    onFileChange() {
        this.setState({
            choseFileName: $("#upgradeFile").val()
        })
    }
    onClose() {
        history.go(-1);
    }
    componentWillMount() {
    }
    render() {
        return (
            <div className={style.login}>
                <div style={{ width: '700px', height: '420px', margin: '-230px 0 0 -350px', position: 'absolute', top: '50%', left: '50%', color: 'white' }}>
                    <div className={style.systemUpdate} />
                    <Row type="flex" justify="center">
                        <Col span={8} offset={2}>

                            <p style={{ color: "#fff", textAlign: 'center' }}>{intl.get(INSTALL_UPGRADE_MESSAGE)}</p>
                            <p style={{ color: "yellow", textAlign: 'center', fontSize:'16px' }}>{intl.get(RESTART_MESSAGE_AFTER_UPGRADE)}</p>

                            {/* <div style={{textAlign:'center'}}> */}
                        </Col>
                        <Col span={8}>
                            <button  className="btn btn-primary" style={{ marginLeft: '85px' }} onClick={()=>{$('#upgradeFile').click()}}>

                                <input type="file" name="upgradeFile" id="upgradeFile" className={style.inputfile} accept=".zip" onChange={() => this.onFileChange()} ></input>
                                {/* <input type="file" name="licenseFile" id="licenseFile" className={style.inputfile} accept=".dat" onChange={() => this.onFileChange()}></input> */}

                                <label htmlFor="upgradeFile2" style={{ cursor: 'pointer' }}>
                                    {/* <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                               <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                           </svg>  */}
                                    <Icon type="download" style={{ paddingRight: '5px' }} />

                                    <span>{intl.get(CHOOSE_UPGRADE_FILE)}</span>
                                </label>
                                <br />
                                <span style={{ color: "#fff" }}>{this.state.choseFileName}</span>
                            </button >

                            <br />
                            {this.state.status == 'upgrading' && <img src={loadingImg} style={{ width: '40px', top: '90%', left: '45%' }} />}
                            
                            <br />
                            <button className="btn btn-primary" style={{ marginLeft: '85px', width:'130px' }} onClick={() => this.onUpgrade()}>{intl.get(INSTALL_UPGRADE)}</button>
                            {/* <br /><br /> */}
                        </Col>
                    </Row>
                    <Row>
                    
                            <button className="btn btn-primary" style={{ margin: '20px 0 0 335px', width:'70px'}} onClick={() => this.onClose()}>{intl.get(CLOSE)}</button>
                            </Row>

                    {/* </div> */}

                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
    }
}

const mapDispatchToProps = dispatch => {
    return {
        upgrade(formData, finishUpgrade) {
            dispatch(upgrade(formData, finishUpgrade));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AppUpgrade)
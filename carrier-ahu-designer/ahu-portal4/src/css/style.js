// import '!style-loader!css-loader!normalize.css';
// import 'bootstrap/dist/css/bootstrap.css'
import 'animate.css/animate.css'
import 'hover.css/css/hover.css'
import 'sweetalert/dist/sweetalert.css'
import 'dragula/dist/dragula.css'
import 'react-dates/lib/css/_datepicker.css'
// import 'nprogress/nprogress.css'

import './font-awesome.css'
import './common.css'
import './iconfont.css'

// import '../fonts/iconfont.css'

// copy css
import '../../vendors/bootstrap/css/bootstrap.min.css'
import '../../vendors/bootstrap-select/css/bootstrap-select.min.css'
import '../../vendors/bootstrap/css/bootstrap-slate.min.css'
import '../../vendors/bootstrap-treeview/bootstrap-treeview.min.css'
import '../../vendors/datepicker/css/datepicker.min.css'
import '../../vendors/contextmenu/jquery.contextMenu.min.css'
import '../../vendors/numberPolyfill/number-polyfill.css'
import '../../vendors/nprogress/nprogress.css'

//antd
// import '../../vendors/antd/antd.min.css'

// copy fonts
import '../../vendors/bootstrap/fonts/glyphicons-halflings-regular.eot'
import '../../vendors/bootstrap/fonts/glyphicons-halflings-regular.svg'
import '../../vendors/bootstrap/fonts/glyphicons-halflings-regular.ttf'
import '../../vendors/bootstrap/fonts/glyphicons-halflings-regular.woff'
import '../../vendors/bootstrap/fonts/glyphicons-halflings-regular.woff2'

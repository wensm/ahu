import {
    STYLE_COMMON,
    STYLE_CHAMBER,
    STYLE_WHELE,
    STYLE_PLATE,
    STYLE_VERTICAL_1,
    STYLE_VERTICAL_2,
    STYLE_DOUBLE_RETURN_1,
    STYLE_DOUBLE_RETURN_2,
    STYLE_SIDE_BY_SIDE_RETURN,
    STYLE_SIDE_BY_SIDE_RETURN_2
} from '../actions/ahu'

const TOP_LEFT = 0;
const TOP_RIGHT = 1;
const BOTTOM_LEFT = 2;
const BOTTOM_RIGHT = 3;

// reorder partition position for double layer AHUs
export function reorderPartition(partitions, layout) {
    
    if(layout && (layout.style == STYLE_WHELE ||
            layout.style ==STYLE_DOUBLE_RETURN_1 ||
            layout.style ==STYLE_DOUBLE_RETURN_2 ||
            layout.style == STYLE_SIDE_BY_SIDE_RETURN ||
            layout.style == STYLE_SIDE_BY_SIDE_RETURN_2)) {
        reorderPartitionForWheel(partitions, layout);
    } else if(layout && layout.style == STYLE_PLATE) {
        reorderPartitionForPlate(partitions, layout);
    } else if(layout && (layout.style == STYLE_VERTICAL_1 || layout.style == STYLE_VERTICAL_2 )) {
        reorderPartitionForVertical(partitions, layout);
    }
}

function reorderPartitionForWheel(partitions, layout) {
    let posOfPartition = 0;
    // sort from bottom
    let bottomSections = getBottomLayerSections(layout);
    for (let i = 0; i < partitions.length; i++) {
        let partition = partitions[i];
        let sections = partition.sections;
        if (bottomSections.includes(sections[0].pos + 1)) {
            partition.pos = posOfPartition++;
        }
    }
    // sort from top
    posOfPartition = partitions.length - 1;
    let topSections = getTopLayerSections(layout);
    for (let i = 0; i < partitions.length; i++) {
        let partition = partitions[i];
        let sections = partition.sections;
        if (topSections.includes(sections[0].pos + 1)) {
            partition.pos = posOfPartition--;
        }
    }
}

function reorderPartitionForPlate(partitions, layout) {
    let reversePartitions = [];
    reversePartitions = reversePartitions.concat(partitions);
    reversePartitions.reverse();

    let posOfPartition = 0;
    // sort from bottom
    let bottomSections = getBottomLayerSections(layout);
    for (let i = 0; i < reversePartitions.length; i++) {
        let partition = reversePartitions[i];
        let sections = partition.sections;
        if (bottomSections.includes(sections[0].pos + 1)) {
            partition.pos = posOfPartition++;
        }
    }
    // sort from top
    posOfPartition = partitions.length - 1;
    let topSections = getTopLayerSections(layout);
    for (let i = 0; i < partitions.length; i++) {
        let partition = partitions[i];
        let sections = partition.sections;
        if (topSections.includes(sections[0].pos + 1)) {
            partition.pos = posOfPartition--;
        }
    }
}

function reorderPartitionForVertical(partitions, layout) {
    let posOfPartition = 0;
    // sort from bottom
    let bottomSections = getBottomLayerSections(layout);
    for (let i = 0; i < partitions.length; i++) {
        let partition = partitions[i];
        let sections = partition.sections;
        if (bottomSections.includes(sections[0].pos + 1)) {
            partition.pos = posOfPartition++;
        }
    }
    // sort from top
    let topSections = getTopLayerSections(layout);
    for (let i = 0; i < partitions.length; i++) {
        let partition = partitions[i];
        let sections = partition.sections;
        if (topSections.includes(sections[0].pos + 1)) {
            partition.pos = posOfPartition++;
        }
    }
}

function getTopLayerSections(layout) {
    let newLayout = [];
    newLayout = newLayout.concat(layout.layoutData[TOP_LEFT]);
    newLayout = newLayout.concat(layout.layoutData[TOP_RIGHT]);
    return newLayout;
}

function getBottomLayerSections(layout) {
    let newLayout = [];
    newLayout = newLayout.concat(layout.layoutData[BOTTOM_LEFT]);
    newLayout = newLayout.concat(layout.layoutData[BOTTOM_RIGHT]);
    return newLayout;
}

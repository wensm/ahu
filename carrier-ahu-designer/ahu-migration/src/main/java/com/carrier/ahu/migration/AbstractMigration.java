package com.carrier.ahu.migration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Wen zhengtao on 2017/4/3.
 */
public abstract class AbstractMigration {
    protected static Logger logger = LoggerFactory.getLogger(AbstractMigration.class);
    @Autowired
    JdbcTemplate jdbcTemplate;
}

package com.carrier.ahu.migration;

import com.carrier.ahu.api.Api;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 迁移项目数据
 * Created by Wen zhengtao on 2017/4/2.
 */
@Component
public class Migration extends AbstractMigration {
    public void migrationProject() {
        String sql = "select * from p_project";
        List<Map<String,Object>> projectList = jdbcTemplate.queryForList(sql);
        if (CollectionUtils.isNotEmpty(projectList)) {
            for (Map<String,Object> project : projectList) {
                System.out.println(project);
//                Map<String,Object> formMap = new HashedMap();
//                formMap.put("pid",project.get("pid"));
//                formMap.put("no",project.get("no"));
//                formMap.put("name",project.get("name"));
//                formMap.put("address",project.get("address"));
//                formMap.put("contract",project.get("contract"));
//                formMap.put("saler",project.get("saler"));
//                formMap.put("memo",project.get("memo"));
                Api.importData("/project/add",project);
                //{pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, no=, name=20170227, address=, contract=, saler=, inputdate=2017-02-27 08:00:00.0, memo=}
            }
        }
    }

    public void migrationAhu() {
        String sql = "select * from p_unit";
        List<Map<String,Object>> unitList = jdbcTemplate.queryForList(sql);
        if (CollectionUtils.isNotEmpty(unitList)) {
            for (Map<String,Object> unit : unitList) {
                System.out.println(unit);
                Api.importData("/ahu/save",unit);
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, series=001, product=39CQ1420, mount=1, name=AHU-5-01, weight=0.0, price=0.0, ModifiedNo=59, paneltype=1, nsprice=0.0, COST=0.0, LB=0.0, DRAWING=null}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={CF008BEC-37BD-465E-9739-D894094FAB9E}, series=002, product=AHU, mount=1, name=, weight=0.0, price=0.0, ModifiedNo=0, paneltype=0, nsprice=0.0, COST=0.0, LB=0.0, DRAWING=null}
            }
        }
    }

    public void migrationSection() {
        String sql = "select * from p_part";
        List<Map<String,Object>> partList = jdbcTemplate.queryForList(sql);
        if (CollectionUtils.isNotEmpty(partList)) {
            for (Map<String,Object> part : partList) {
                System.out.println(part);
                Api.importData("/section/save",part);
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={180E3250-98EA-4CCC-8F98-0945EE6D6200}, orders=11, types=13, position=0, setup=1, pic=M, groupi=4, COST=3679.76, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={18B6875A-2011-456C-AB4E-C895C3887A45}, orders=1, types=1, position=0, setup=1, pic=a11000, groupi=1, COST=1044.83, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={19F5A474-3397-4A52-BA10-3CA3DCF2ABC3}, orders=4, types=6, position=0, setup=1, pic=F, groupi=2, COST=0.0, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={2A68CC1F-B92F-49AE-A410-EE794E9BA99D}, orders=5, types=7, position=0, setup=1, pic=G, groupi=2, COST=0.0, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={32236410-03AA-4B2C-8040-0E9547A8CD92}, orders=6, types=15, position=0, setup=1, pic=O, groupi=2, COST=0.0, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={431EC6ED-9003-49E1-A591-5C99C26FF7CE}, orders=2, types=3, position=0, setup=1, pic=C, groupi=1, COST=1977.09, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={5EB75305-E93B-49CB-A7F7-2F3837F438B2}, orders=14, types=14, position=0, setup=1, pic=n2, groupi=5, COST=522.42, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={6981254A-A6FA-4B88-9EA4-5551E4AF557E}, orders=7, types=5, position=0, setup=1, pic=E, groupi=3, COST=3381.21, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={A14CC26E-718F-4A15-8FEE-B6CB11E324EF}, orders=12, types=18, position=0, setup=1, pic=R, groupi=4, COST=0.0, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={A36D4D38-2D9F-4EAA-A8E0-1881426C9705}, orders=13, types=22, position=0, setup=1, pic=vv, groupi=5, COST=6631.12, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={B17725E0-CB5B-42ED-9D8D-4A012288151C}, orders=3, types=16, position=0, setup=1, pic=P, groupi=2, COST=0.0, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={C14B5B42-FFAE-41DA-89DE-3FF2BC48A56C}, orders=8, types=8, position=0, setup=1, pic=H, groupi=3, COST=1917.14, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={D466D8AE-87F8-489E-9D2E-044A558B8E24}, orders=10, types=15, position=0, setup=1, pic=O, groupi=4, COST=0.0, AVDIRECTION=1}
//                {pid={46FA7006-B03E-4E0A-BABF-5E3510D5E0BE}, unitid={8999461E-20AC-44DB-AE04-472D7E9855EE}, partid={FAD6E04E-D7B6-47B3-932C-68819BB07887}, orders=9, types=11, position=0, setup=1, pic=k_BHF, groupi=3, COST=6435.73, AVDIRECTION=1}
            }
        }
    }
}

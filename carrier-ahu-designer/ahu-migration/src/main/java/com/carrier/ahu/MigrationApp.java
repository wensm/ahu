package com.carrier.ahu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Wen zhengtao on 2017/4/3.
 */
@SpringBootApplication
public class MigrationApp {
    public static void main(String[] args) {
        SpringApplication.run(MigrationApp.class,args);
    }
}
